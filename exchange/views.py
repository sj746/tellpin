#-*- coding: utf-8 -*-
###############################################################################
# filename    : exchange > views.py
# description : exchange views ( language exchange )
# author      : khyun@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 khyun 최초 작성
#
#
###############################################################################
import json


from django.http.response import HttpResponse, JsonResponse,\
    HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.template.context import RequestContext

from common.common import tellpin_login_required
from exchange.UserInfoService import UserInfoService
from django.views.decorators.csrf import csrf_exempt
from exchange.service import UserService
from dashboard.service import DashboardService
from community.service import Community
from common import common
from django.core.serializers.json import DjangoJSONEncoder

from tutor.service import TutorService
from people.service import PeopleService
from tutoring.service import ReservationService

# userInfoService = UserInfoService( object )
ts = UserService(object)


# Create your views here.
# 유저 프로필 상세
def detailUser(request, user_id):
    """
    @summary: exchange detail(상세보기) function
    @author: khyun
    @param request
    @param user_id : user id
    @return: url - exchange/user_info/user_info.html
    """
    # response json
    resData = {}
    resData["isSuccess"] = 1
    my_id = request.user.id
    user_utc = request.session.get('utc_delta', 0)
    print "detailUserdetailUserdetailUser"

    user_detail = TutorService.get_user_detail(user_id, user_utc)

    # 기본 정보
    resData["profile"] = user_detail['profile']
    cur_page = 1
    # 유저 statistics
    resData["statistics"] = user_detail['statistics']
    # 자기 자신 확인
    resData["mypage"] = TutorService.check_mine(user_id, my_id)

    resData["tutor"] = PeopleService.getTutorList(user_id)
    resData["tutee"] = PeopleService.getTutees(user_id)
    # 친구목록

    resData["friends"] = PeopleService.getFriends(int(user_id))
    # 친구 요청 여부
    resData['isFriend'] = PeopleService.isRequestFriend(user_id, my_id)

    # Feedback
    resData["feedback"] = user_detail['feedback']
    # 튜터 북마크
#     resData['bookmark'] = PeopleService.getBookmarkList(user_id)


    # follow 목록
#     resData['follow'] = userInfoService.getFollowList(user_id)
    # Club & Community Activities
#     resData['activity'] = userInfoService.getActivity(user_id)

#     resData["languageInfo"] = ds.get_language_info()
    # community activities
    resData["LBs"], resData["LBShowMore"] = DashboardService.get_language_board(cur_page, user_id, 5)
    resData["TPs"], resData["TPShowMore"] = DashboardService.get_translation_proofreading(cur_page, user_id, 5)
    resData["LQs"], resData["LQShowMore"] = DashboardService.get_language_question(cur_page, user_id, 5)
    resData["all"], resData["AllShowMore"] = DashboardService.get_all_community(cur_page, user_id, 5)
    resData['page'] = cur_page

    return render_to_response("exchange/user_info/user_info.html",
                              {
                                  'data': json.dumps(resData, cls=DjangoJSONEncoder),
                                  'title': resData['profile']
                              }, RequestContext(request))


@csrf_exempt
def moreCommunity(request):
    """
    @summary: exchange detail(상세보기) more community function
    @author: khyun
    @param request
    @return: json
    """
    resData = {
        'isSuccess': True
    }
    post = json.loads(request.body)
    page = post['page']
    writer_id = post['id']
    community_type = post['type']
    resData['page'] = page

    if community_type == 'all':
        resData['list'], resData['AllShowMore'] = DashboardService.get_all_community(page, writer_id, 5)
    elif community_type == 'LB':
        resData['list'], resData['LCShowMore'] = DashboardService.get_language_board(page, writer_id, 5)
    elif community_type == 'TP':
        resData['list'], resData['TPShowMore'] = DashboardService.get_translation_proofreading(page, writer_id, 5)
    else:
        resData['list'], resData['LCShowMore'] = DashboardService.get_language_question(page, writer_id, 5)

    return JsonResponse(resData)


# 친구 신청
@csrf_exempt
@tellpin_login_required
def ajax_newFriend(request):
    """
    @summary: exchange new friend function
    @author: khyun
    @param request
    @return: json
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    dict = json.loads(request.body)
    response_id = dict['response_id']
    message = dict['message']
    resData['isSuccess'], friend = PeopleService.addFriend(user_id, response_id, message)
    Community.set_notification_exchange_add_friend(user_id, response_id, friend)
    
    return JsonResponse(resData)


# 메세지 보내기
@csrf_exempt
@tellpin_login_required
def ajax_newMessage(request):
    """
    @summary: exchange new message function
    @author: khyun
    @param request
    @return: json
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = ts.get_user_id(request.session["email"])
    dict = json.loads(request.body)
    to_user_id = dict['to_user_id']
    from_user_id = user_id
    message = dict['message']
    resData['message'] = userInfoService.sendMessage(to_user_id, from_user_id, message)

    return JsonResponse(resData)

# 메세지 목록
@csrf_exempt
@tellpin_login_required
def ajax_getMessage(request):
    """
    @summary: exchange get message
    @author: khyun
    @param request
    @return: json
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = ts.get_user_id(request.session["email"])
    dict = json.loads( request.body )
    to_user_id = dict['to_user_id']
    resData['message'] = userInfoService.getMessage(user_id, to_user_id)

    return JsonResponse(resData)


#############################################################################
# user 검색, 리스트 페이지 호출
#  
# url : tutor/findtutor/
# respose data: json 
###############################################################################
def exchange_list(request):
    """
    @summary: exchange exchange list function
    @author: khyun
    @param request
    @return: url - exchange/exchange_user_find.html
    """
    # resoponse json
    resData = {
        'isSuccess': 1,
        'search_data': TutorService.get_search_data()
    }
    jsonData = json.dumps(resData)

    return render_to_response("exchange/exchange_user_find.html", {'data': jsonData}, RequestContext(request))


###############################################################################
# user 검색 필터 처리
#  
# url : tutor/search_tutor/
# respose : json 
###############################################################################
@csrf_exempt
def exchange_finder_search(request):
    """
    @summary: exchange new friend function
    @author: khyun
    @param request
    @return: json
    """
    # resoponse json
    resData = {
        'isSuccess': 1
    }
    user_id = request.user.id

    # get request parameter
    post_dict = json.loads(request.body)
    user_id_list = TutorService.filter_listup_tutor(None, post_dict, 'tutee')
    resData["user_list"], resData["page_info"] = TutorService.listup_user(user_id, user_id_list, post_dict['page'])
    resData['me'] = user_id

    return JsonResponse(resData)


# 해당 리스트 더 가져오기
@csrf_exempt
def ajax_moreList(request):
    """
    @summary: exchange more list
    @author: khyun
    @param request
    @return: json
    """
    resData = {}
    resData['isSuccess'] = 1
    req_dict = json.loads(request.body)
    req_type = req_dict['type']
    uid = req_dict['uid']

    if req_type == 'Tutors':
        resData['list'] = userInfoService.moreBookmarkList(uid)
    elif req_type == 'Friends':
        resData['list'] = userInfoService.moreFriendsList(uid)
    else:
        resData['list'] = userInfoService.moreFollowList(uid)

    return JsonResponse(resData)


# 피드백 페이징
@csrf_exempt
def page_moreFeedback(request):
    """
    @summary: exchange get more feedback function ( paging )
    @author: khyun
    @param request
    @return: json
    """
    resData = {}
    resData['isSuccess'] = 1
    req_dict = json.loads(request.body)
    page = req_dict['page']
    _user_id = req_dict['_user_id']
    resData['feedback'] = ReservationService.getFeedbackList(_user_id, page)

    return JsonResponse(resData)


# 피드백더가져오기
@csrf_exempt
def moreFeedback(request):
    """
    @summary: exchange get more feedback fucntion
    @author: khyun
    @param none
    @return: json
    """
    resData = {}
    resData['isSuccess'] = 1

    if request.session.get('email', False):
        user_id = ts.get_user_id(request.session["email"])
    else:
        user_id = None

    dict = json.loads(request.body)
    revid = dict['revid']
    tutor_id = dict['tutor_id']
    tutee_id = dict['tutee_id']
    resData['moreFeedback'] = userInfoService.moreFeedback(revid, tutor_id, tutee_id, user_id)

    return JsonResponse(resData)
