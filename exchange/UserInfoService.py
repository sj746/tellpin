#-*- coding: utf-8 -*-
###############################################################################
# filename    : exchange > UserInfoService.py
# description : exchange UserInfoService ( language exchange info)
# author      : khyun@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 khyun 최초 작성
#
#
###############################################################################
# from common.models import Reservation, NationInfo, CityInfo, Friend, Feedback,\
#     Follow, Message
# import datetime
# from datetime import timedelta
# from common.models import TellpinUser, DjangoSession, Language
# from django.contrib.sessions.models import Session
# from django.db.models import Q
# from django.db.models.aggregates import Count
# from common.models import LqQuestion, LqAnswer, LqComment
# from tutor.service import TutorService
# from django.core.paginator import Paginator
# from common.models import Timezone
from common.common import get_login_users
from django.db import connection
from common import common
from dateutil.relativedelta import relativedelta

class UserInfoService( object ):
    NATION_FLAG_FILE_DIR = "/static/img/nations"
    PROFILE_IMAGE_FILE_DIR = "/static/img/upload/profile"
    def __init__(self, params):
        '''
        Constructor
        '''
#     def getUserProfile(self, _user_id ):
#         """
#         @summary: exchange get user profile function
#         @author: khyun
#         @param none
#         @return: object data
#         """
#         obj = {}
#         try :
#             #online
#             online_list = []
#             
#             online_list = get_login_users()    
#                 
#             userInfo = TellpinUser.objects.get( id = _user_id )
#             obj["tid"] = userInfo.id
#             obj["display_name"] = userInfo.name
#             obj["profile_photo"] = self.PROFILE_IMAGE_FILE_DIR + "/" +userInfo.profile_photo
#             obj["is_tutor"] = userInfo.is_tutor
#             obj['intro'] = userInfo.long_intro
#             if userInfo.email in online_list :
#                 obj["is_login"] = 1
#             else :
#                 obj["is_login"] = 0
#             try :
#                 obj["nation_from"] = NationInfo.objects.get( nid = userInfo.from_field ).name_en
#             except :
#                 obj["nation_from"] = ""
#             try:
#                 obj["city_from"] = CityInfo.objects.get(city_id=userInfo.from_city).name
#             except:
#                 obj["city_from"] = ""
#             try :
#                 obj["nation_live"] = NationInfo.objects.get(nid=userInfo.livein).name_en
#             except : 
#                 obj['nation_live'] = ""
#             try:
#                 obj["city_live"] = CityInfo.objects.get(city_id=userInfo.livein_city).name
#             except:
#                 obj["city_live"] = ""
#             try : 
#                 obj["nation_flag"] = self.NATION_FLAG_FILE_DIR + "/" + NationInfo.objects.get( nid = userInfo.from_field ).image
#             except : 
#                 obj['nation_flag'] = ""
#             # speak, teach
#             my_speak_list = []
#             if userInfo.native1 is not None and userInfo.native1 != 0:
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get(id=userInfo.native1).language1
#                 spk_item["level"] = 7
#                 my_speak_list.append(spk_item)
#             if userInfo.native2 is not None and userInfo.native2 != 0:
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get(id=userInfo.native2).language1
#                 spk_item["level"] = 7
#                 my_speak_list.append(spk_item)
#             if userInfo.native3 is not None and userInfo.native3 != 0:
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get(id=userInfo.native3).language1
#                 spk_item["level"] = 7
#                 my_speak_list.append(spk_item)
#             if userInfo.other1 is not None and userInfo.other1 != 0:
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get(id=userInfo.other1).language1
#                 spk_item["level"] = userInfo.other1_skill_level
#                 my_speak_list.append(spk_item)
#             if userInfo.other2 is not None and userInfo.other2 != 0:
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get(id=userInfo.other2).language1
#                 spk_item["level"] = userInfo.other2_skill_level
#                 my_speak_list.append(spk_item)
#             if userInfo.other3 is not None and userInfo.other3 != 0:
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get(id=userInfo.other3).language1
#                 spk_item["level"] = userInfo.other1_skill_level
#                 my_speak_list.append(spk_item)
#             obj["speaks"] = my_speak_list
#             my_learn_list = []
#             if userInfo.learning1 is not None and userInfo.learning1 != 0 :
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get( id = userInfo.learning1 ).language1
#                 spk_item["level"] = userInfo.learning1_skill_level
#                 my_learn_list.append( spk_item )
#             if userInfo.learning2 is not None and userInfo.learning2 != 0 :
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get( id = userInfo.learning2 ).language1
#                 spk_item["level"] = userInfo.learning1_skill_level
#                 my_learn_list.append( spk_item )
#             if userInfo.learning3 is not None and userInfo.learning3 != 0 :
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get( id = userInfo.learning3 ).language1
#                 spk_item["level"] = userInfo.learning1_skill_level    
#                 my_learn_list.append( spk_item )
#             obj["learn"] = my_learn_list
#             if userInfo.is_tutor == 1 :
#                 my_teach_list = []
#                 if userInfo.teaching1 is not None and userInfo.teaching1 != 0:
#                     tch_item = {}
#                     tch_item["name"] = Language.objects.get(id=userInfo.teaching1).language1
#                     tch_item["level"] = 7
#                     my_teach_list.append(tch_item)
#                 if userInfo.teaching2 is not None and userInfo.teaching2 != 0:
#                     tch_item = {}
#                     tch_item["name"] = Language.objects.get(id=userInfo.teaching2).language1
#                     tch_item["level"] = 7
#                     my_teach_list.append(tch_item)
#                 if userInfo.teaching3 is not None and userInfo.teaching3 != 0:
#                     tch_item = {}
#                     tch_item["name"] = Language.objects.get(id=userInfo.teaching3).language1
#                     tch_item["level"] = 7
#                     my_teach_list.append(tch_item)
#                 obj["teaches"] = my_teach_list
#         except Exception as e :
#             print str( e )
#             obj = {}
#             obj['message'] = str( e )
#         finally:
#             return obj
# 
#     def excludeLoginProfile(self, _user_id ):
#         """
#         @summary: exchange get user profile
#         @author: khyun
#         @param _user_id : user id
#         @return: object data
#         """
#         obj = {}
#         try :
#             userInfo = TellpinUser.objects.get( id = _user_id )
#             obj["tid"] = userInfo.id
#             obj["display_name"] = userInfo.name
#             obj["profile_photo"] = self.PROFILE_IMAGE_FILE_DIR + "/" +userInfo.profile_photo
#             obj["is_tutor"] = userInfo.is_tutor
#             obj['intro'] = userInfo.long_intro
#             try :
#                 obj["nation_from"] = NationInfo.objects.get( nid = userInfo.from_field ).name_en
#             except :
#                 obj["nation_from"] = ""
#             try:
#                 obj["city_from"] = CityInfo.objects.get(city_id=userInfo.from_city).name
#             except:
#                 obj["city_from"] = ""
#             try :
#                 obj["nation_live"] = NationInfo.objects.get(nid=userInfo.livein).name_en
#             except : 
#                 obj['nation_live'] = ""
#             try:
#                 obj["city_live"] = CityInfo.objects.get(city_id=userInfo.livein_city).name
#             except:
#                 obj["city_live"] = ""
#             try : 
#                 obj["nation_flag"] = self.NATION_FLAG_FILE_DIR + "/" + NationInfo.objects.get( nid = userInfo.from_field ).image
#             except : 
#                 obj['nation_flag'] = ""
#             # speak, teach
#             my_speak_list = []
#             if userInfo.native1 is not None and userInfo.native1 != 0:
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get(id=userInfo.native1).language1
#                 spk_item["level"] = 7
#                 my_speak_list.append(spk_item)
#             if userInfo.native2 is not None and userInfo.native2 != 0:
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get(id=userInfo.native2).language1
#                 spk_item["level"] = 7
#                 my_speak_list.append(spk_item)
#             if userInfo.native3 is not None and userInfo.native3 != 0:
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get(id=userInfo.native3).language1
#                 spk_item["level"] = 7
#                 my_speak_list.append(spk_item)
#             if userInfo.other1 is not None and userInfo.other1 != 0:
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get(id=userInfo.other1).language1
#                 spk_item["level"] = userInfo.other1_skill_level
#                 my_speak_list.append(spk_item)
#             if userInfo.other2 is not None and userInfo.other2 != 0:
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get(id=userInfo.other2).language1
#                 spk_item["level"] = userInfo.other2_skill_level
#                 my_speak_list.append(spk_item)
#             if userInfo.other3 is not None and userInfo.other3 != 0:
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get(id=userInfo.other3).language1
#                 spk_item["level"] = userInfo.other1_skill_level
#                 my_speak_list.append(spk_item)
#             obj["speaks"] = my_speak_list
#             my_learn_list = []
#             if userInfo.learning1 is not None and userInfo.learning1 != 0 :
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get( id = userInfo.learning1 ).language1
#                 spk_item["level"] = userInfo.learning1_skill_level
#                 my_learn_list.append( spk_item )
#             if userInfo.learning2 is not None and userInfo.learning2 != 0 :
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get( id = userInfo.learning2 ).language1
#                 spk_item["level"] = userInfo.learning1_skill_level
#                 my_learn_list.append( spk_item )
#             if userInfo.learning3 is not None and userInfo.learning3 != 0 :
#                 spk_item = {}
#                 spk_item["name"] = Language.objects.get( id = userInfo.learning3 ).language1
#                 spk_item["level"] = userInfo.learning1_skill_level    
#                 my_learn_list.append( spk_item )
#             obj["learn"] = my_learn_list
#             if userInfo.is_tutor == 1 :
#                 my_teach_list = []
#                 if userInfo.teaching1 is not None and userInfo.teaching1 != 0:
#                     tch_item = {}
#                     tch_item["name"] = Language.objects.get(id=userInfo.teaching1).language1
#                     tch_item["level"] = 7
#                     my_teach_list.append(tch_item)
#                 if userInfo.teaching2 is not None and userInfo.teaching2 != 0:
#                     tch_item = {}
#                     tch_item["name"] = Language.objects.get(id=userInfo.teaching2).language1
#                     tch_item["level"] = 7
#                     my_teach_list.append(tch_item)
#                 if userInfo.teaching3 is not None and userInfo.teaching3 != 0:
#                     tch_item = {}
#                     tch_item["name"] = Language.objects.get(id=userInfo.teaching3).language1
#                     tch_item["level"] = 7
#                     my_teach_list.append(tch_item)
#                 obj["teaches"] = my_teach_list
#         except Exception as e :
#             print str( e )
#             obj = {}
#             obj['message'] = str( e )
#         finally:
#             return obj

#     def get_tutee_statistics(self, _user_id):
#         """
#         @summary: exchange get tutee statistics
#         @author: khyun
#         @param _user_id : user id
#         @return: object data
#         """
#         obj = {}
#         
#         item_obj = {}
# 
#         print "get_tutee_statistics"
# 
#         #Attendance : (신청완료 - dipute) / 신청완료
#         #last_month
#         now = datetime.datetime.now()
#         timegap = timedelta(days=30) #날짜 차이
#         term = now - timegap
#         
#         str_startday = str(term.year) + term.strftime("%m%d")
#         str_endday = str(now.year) + now.strftime("%m%d")
#         stat_range = []
#         for i in range(int(str_startday), int(str_endday)):
#             stat_range.append(i+1)
#         
#         rv_list = Reservation.objects.filter(tutee_id=_user_id, user_confirm__gt=0, tutor_confirm=1)
#         rv_count = 0
#         for item in rv_list:
#             str_sc_time = item.rv_time
#             str_sc_time.strip()
#             timelist = str_sc_time.split('$')
#             b_checked = False
#             for t in timelist:
#                 rv_date = t[:8]
#                 if rv_date in stat_range:
#                     b_checked = True
#                     break
#             if b_checked:
#                 rv_count += 1
#                 
#         rv_list = Reservation.objects.filter(tutee_id=_user_id, user_confirm__gt=2, tutor_confirm=1)
#         rv_dispute = 0
#         for item in rv_list:
#             str_sc_time = item.rv_time
#             str_sc_time.strip()
#             timelist = str_sc_time.split('$')
#             b_checked = False
#             for t in timelist:
#                 rv_date = t[:8]
#                 if rv_date in stat_range:
#                     b_checked = True
#                     break
#             if b_checked:
#                 rv_dispute += 1
#         
#         if rv_count > 0:
#             item_obj["last_month"] = str(((rv_count - rv_dispute) / float(rv_count) ) * 100) + " %"
#         else:
#             item_obj["last_month"] = None
#             
#         rv_count = Reservation.objects.filter(tutee_id=_user_id, user_confirm__gt=0, tutor_confirm=1).count()
#         rv_dispute = Reservation.objects.filter(tutee_id=_user_id, user_confirm__gt=1, tutor_confirm=1).count()
# 
#         if rv_count > 0:
#             item_obj["all_time"] = str(((rv_count - rv_dispute) / float(rv_count) ) * 100) + " %"
#             
#         else:
#             item_obj["all_time"] = None    
#             
#         obj["attendance"] = item_obj
#         
#         item_obj = {}
#         #Disputes : user_confirm > 1
#         item_obj["last_month"] = rv_dispute
#         item_obj["all_time"] = rv_dispute
#         obj["disputes"] = item_obj
#         
#         #confirmed lessons
#         item_obj = {}
#         now = datetime.datetime.now()
#         timegap = timedelta(days=30) #날짜 차이
#         term = now - timegap
#         
#         str_startday = str(term.year) + term.strftime("%m%d")
#         str_endday = str(now.year) + now.strftime("%m%d")
# 
#         rv_list = Reservation.objects.filter(tutee_id=_user_id, rv_status = 5)
#         
#         last_month_confirmed_count = 0
# 
#         for item in rv_list:
#             str_sc_time = item.rv_time
#             str_sc_time.strip()
#             timelist = str_sc_time.split('$')
#             for t in timelist:
#                 rv_date = t[:8]
#                 if int(rv_date) >= int(str_startday):
#                     last_month_confirmed_count += 1
# 
#         item_obj["last_month"] = last_month_confirmed_count
#         all_time_confirmed_count = Reservation.objects.filter(tutee_id=_user_id, rv_status = 5).count()
# 
#         item_obj["all_time"] = all_time_confirmed_count
#         obj["confirmed_lesson"] = item_obj
# 
#         return obj

    # 친구 목록
    def getFriendsList(self, _user_id ):
        """
        @summary: exchange get friends list
        @author: khyun
        @param _user_id : user id
        @return: object data
        """
        result = {}
        friendList = []
        try :
            friends = Friend.objects.filter( Q( request_id = _user_id ) | Q( response_id = _user_id ), state = 3 ).order_by( '-responsed_date' )[:4]
            #print friends.query
            for friend in friends :
                obj = {}
                if friend.request_id == int( _user_id ) :  
                    friendId = friend.response_id
                else :
                    friendId = friend.request_id
                friendModel = TellpinUser.objects.get( id = friendId )
                obj["friendId"] = friendModel.id
                obj["profile_photo"] = '/static/img/upload/profile/' + friendModel.profile_photo
                obj["friendName"] = friendModel.name
                friendList.append( obj )
            result['friend'] = friendList
            result['count'] = Friend.objects.filter( Q( request_id = _user_id ) | Q( response_id = _user_id ), state = 3 ).order_by( '-responsed_date' ).count()
        except Exception as e :
            result = {}
            result["message"] = str( e )
        finally:
            return result
    # 전체 친구 목록
    def moreFriendsList(self, _user_id ):
        """
        @summary: exchange get all friends list
        @author: khyun
        @param _user_id : user id
        @return: object data
        """
        friendList = []
        try :
            friends = Friend.objects.filter( Q( request_id = _user_id ) | Q( response_id = _user_id ), state = 3 ).order_by( '-responsed_date' )
            for friend in friends :
                obj = {}
                if friend.request_id == _user_id :  
                    friendId = friend.response_id
                else :
                    friendId = friend.request_id
                friendModel = TellpinUser.objects.get( id = friendId )
                obj["tid"] = friendModel.id
                obj["profile_photo"] = '/static/img/upload/profile/' + friendModel.profile_photo
                obj["friendName"] = friendModel.name
                friendList.append( obj )
        except Exception as e :
            obj = {}
            obj["message"] = str( e )
            friendList.append( obj )
        finally:
            return friendList
    
    # Bookmark 목록
    def getBookmarkList(self, _user_id ):
        """
        @summary: exchange get mookmark list
        @author: khyun
        @param _user_id : user id
        @return: object data
        """
        result = {}
        bookmarkList = []
        try :
            bookmarks = Follow.objects.filter( follower_id = _user_id, division = 1 ).order_by( '-created_time' )[0:4]
            for bookmark in bookmarks :
                obj = self.excludeLoginProfile( bookmark.followee_id )
                bookmarkList.append( obj )
            result['bookmark'] = bookmarkList
            result['count'] = Follow.objects.filter( follower_id = _user_id, division = 1 ).count()
        except Exception as e :
            result = {}
            result['message'] = str( e )
        finally :
            return result

    # 전체 Bookmark 목록
    def moreBookmarkList(self, _user_id ):
        """
        @summary: exchange get all mookmark list
        @author: khyun
        @param _user_id : user id
        @return: object data
        """
        bookmarkList = []
        try :
            bookmarks = Follow.objects.filter( follower_id = _user_id, division = 1 ).order_by( '-created_time' )
            for bookmark in bookmarks :
                obj = self.excludeLoginProfile( bookmark.followee_id )
                bookmarkList.append( obj )
        except Exception as e :
            obj = {}
            obj['message'] = str( e )
            bookmarkList.append( obj )
        finally :
            return bookmarkList

    # Follow 목록
    def getFollowList(self, _user_id ):
        """
        @summary: exchange get follow list
        @author: khyun
        @param _user_id : user id
        @return: object data
        """
        result = {}
        followList = []
        try : 
            #follows = Follow.objects.filter( follower_id = _user_id, division = 2 ).order_by( '-created_time' )
            # follow에서 Tutee로 변경
            cursor = connection.cursor()
            sql = '''
                SELECT id
                FROM user_auth_tellpinuser
                WHERE id IN ( 
                                SELECT DISTINCT tutee_id
                                FROM reservation
                                WHERE tutor_id = %s
                            )
                ORDER BY last_login DESC
                LIMIT 0, 4
            '''
            cursor.execute( sql, [ _user_id ] )
            tuteeList = common.dictfetchall( cursor )
            for tutee in tuteeList :
                print 'tutee', tutee['id']
                obj = self.excludeLoginProfile( tutee['id'] )
                followList.append( obj )
            result['follow'] = followList
            sql = '''
                SELECT COUNT(*)
                FROM user_auth_tellpinuser
                WHERE id IN ( 
                                SELECT DISTINCT tutee_id
                                FROM reservation
                                WHERE tutor_id = %s
                            )
            '''
            cursor.execute( sql, [ _user_id ] )
            result['count'] = cursor.fetchone()[0]
        except Exception as e :
            result = {}
            result['message'] = str( e )
        finally :
            cursor.close()
            return result

    # 전체 Follow 목록
    def moreFollowList(self, _user_id ):
        """
        @summary: exchange get all follow list
        @author: khyun
        @param _user_id : user id
        @return: object data
        """
        followList = []
        try : 
            #follows = Follow.objects.filter( follower_id = _user_id, division = 2 ).order_by( '-created_time' )
            cursor = connection.cursor()
            sql = '''
                SELECT id
                FROM user_auth_tellpinuser
                WHERE id IN ( 
                                SELECT DISTINCT tutee_id
                                FROM reservation
                                WHERE tutor_id = %s
                            )
                ORDER BY last_login DESC
            '''
            cursor.execute( sql, [ _user_id ] )
            tuteeList = common.dictfetchall( cursor )
            for tutee in tuteeList:
                obj = self.excludeLoginProfile( tutee['id'] )
                followList.append( obj )
        except Exception as e :
            obj = {}
            obj['message'] = str( e )
            followList.append( obj )
        finally :
            cursor.close()
            return followList

    # Club Posts 목록
    def getPostList(self, _user_id ):
        """
        @summary: get club post list
        @author: khyun
        @param _user_id : user id
        @return: object data
        """
        postList = []
        try :
            # 클럽 포스팅 글 목록
            obj = {}
        except Exception as e :
            obj = {}
            obj['message'] = str( e )
            postList.append( obj )
        finally :
            return postList

    # Translation & Proofreading 목록
    def getTransList(self, _user_id ):
        """
        @summary: get translation & proofreading list
        @author: khyun
        @param _user_id : user id
        @return: object data
        """
        transList = []
        try :
            # Translation & Proofreading 목록
            obj = {}
        except Exception as e :
            obj = {}
            obj['message'] = str( e )
            transList.append( obj )
        finally :
            return transList

    # Language Q&A 목록
    def getLanguageQNAList(self, _user_id ):
        """
        @summary: get Q&A list
        @author: khyun
        @param _user_id : user id
        @return: object data
        """
        qnaList = []
        try :
            # 질문목록
            questions = LqQuestion.objects.filter( user_id=_user_id ).order_by( '-updated_time' )
            for question in questions :
                obj = {}
                obj['title'] = question.title
                obj['content'] = question.content
                obj['updated_time'] = str( question.updated_time )
                obj['qid'] = question.qid
                obj['answerCount'] = LqAnswer.objects.filter( qid=question.qid ).count()
                obj['commentCount'] = LqComment.objects.filter( type='Q', ref_id=question.qid ).count()
                qnaList.append(obj)
        except Exception as e :
            obj = {}
            obj['message'] = str(e)
            qnaList.append(obj)
        finally :
            return qnaList

    # Community Activity 목록
    def getActivity(self, _user_id, page = 1 ):
        """
        @summary: get activity list
        @author: khyun
        @param _user_id : user id
        @param page : page number
        @return: object data
        """
        activityList = {}
        per_page = 5
        try :
            # Club Post
            activityList['post'] = self.getPostList(_user_id)
            # Translation
            activityList['trans'] = self.getTransList(_user_id)
            # Q&A list
            activityList['qna'] = self.getLanguageQNAList(_user_id)
            list = activityList['post'] + activityList['trans'] + activityList['qna']
            p = Paginator( list, per_page )
            activity = p.page( page )
            activityList = {
                'item' : activity.object_list,
                'pageRange' : p.page_range,
                'page' : page,
                'has_next' : activity.has_next(),
                'has_prev' : activity.has_previous(),
                'all' : len( list ),
                'post' : len( activityList['post'] ),
                'trans' : len( activityList['trans'] ),
                'qna' : len( activityList['qna'] )
            }
        except Exception as e :
            activityList = {}
            activityList['message'] = str( e )
        finally :
            return activityList

    # 친구 요청
    def addFriend(self, request_id, response_id, message ):
        """
        @summary: add friend function
        @author: khyun
        @param request_id : request id
        @param response_id : response number
        @param message : request message
        @return: result state, object data
        """
        result = 1
        try : 
            if request_id == response_id :
                return 5
            '''
            1 : 요청
            2 : 취소
            3 : 수락
            4 : 거절
            5 : 삭제
            '''
            if Friend.objects.filter( request_id = response_id, response_id = request_id ).count() > 0 :
                result = 2
            else :
                friend, created = Friend.objects.get_or_create( request_id = request_id,
                                                       response_id = response_id,
                                                       message = message,
                                                       state = 1,
                                                       responsed_date = None, req_del=0, res_del=0 )
        except Exception as e :
            result = 0
            print str( e )
        finally : 
            return result, friend

    # 친구 요청 여부
    def getRequestFriend(self, request_id, response_id ):
        """
        @summary: 친구요청 여부 확인
        @author: khyun
        @param request_id : request id
        @param response_id : response number
        @return: result state
        """
        result = 1
        try : 
            if Friend.objects.filter( request_id = response_id, response_id = request_id, state = 1 ).count() > 0 :
                result = 2 # 이미 요청받은 친구
            else :
                result = Friend.objects.filter( request_id = request_id, response_id = response_id, state = 1 ).count()
        except Exception as e :
            result = 0
            print str( e )
        finally :
            return result

    # 메세지 보내기
    def sendMessage(self, to_user_id, from_user_id, _message ):
        """
        @summary: send message function
        @author: khyun
        @param to_user_id : to user
        @param from_user_id : from user
        @param _message : message
        @return: object data
        """
        result = []
        try :
            message = Message()
            message.to_user_id = to_user_id
            message.from_user_id = from_user_id
            message.message = _message
            message.save()
            result = self.getMessage( from_user_id, to_user_id )
        except Exception as e :
            result = []
            obj = {}
            obj['log'] = str( e )
            result.append( obj )
        finally:
            return result

    # 메세지 목록
    def getMessage(self, user_id, to_user_id ):
        """
        @summary: get message function
        @author: khyun
        @param user_id : user id
        @param to_user_id : user id
        @return: object data
        """
        result = []
        try : 
            list = Message.objects.filter( Q( to_user_id = user_id, from_user_id = to_user_id ) | Q( from_user_id = user_id, to_user_id = to_user_id ) ).order_by( 'created_time' )
            utcId = TellpinUser.objects.get( id = user_id ).timezone
            utc = Timezone.objects.get( id = utcId ).utc_time
            utcTime = int( utc[:3])
            if utc[4:] == '30' :
                utcTime += 0.5
            for i in list :
                timeDay = str( i.created_time ).split()
                i.created_time += timedelta( hours = utcTime )
                print timeDay
                flag = True
                for j in result :
                    if j['day'] == timeDay[0] :
                        flag = False
                        break
                    flag = True
                if flag : # 배열에 존재하지 않을 경우
                    obj = {}
                    obj['day'] = timeDay[0]
                    obj['message'] = []
                    result.append( obj )
                for j in result :
                    if j['day'] == timeDay[0] :                         
                        messageObj = {}
                        messageObj['mid'] = i.mid
                        messageObj['to_user_id'] = i.to_user_id
                        messageObj['from_user_id'] = i.from_user_id
                        messageObj['message'] = i.message
                        messageObj['user_id'] = user_id
                        messageObj['time'] = i.created_time
                        if user_id == i.to_user_id : # 받은 메세지
                            messageObj['type'] = 'receive'
                        else :
                            messageObj['type'] = 'send'
                        j['message'].append( messageObj )
        except Exception as e :
            obj = {}
            obj['message'] = str( e )
            result.append( obj )
        finally :
            return result
    
    # 해당 유저 피드백 더 가져오기
    def moreFeedback(self, revid, tutor_id, tutee_id, user_id):
        """
        @summary: get more feedback function function
        @author: khyun
        @param revid : feedback id
        @param tutor_id : tutor id
        @param tutee_id : tutee id
        @param user_id
        @return: object data
        """
        # 해당 revid제외하고 가져오기
        result = []
        try:
            userInfo = TellpinUser.objects.filter(
                id__in=[tutor_id, user_id]
            ).values('timezone', 'name', 'id')

            if user_id is None:
                userTimezone = userInfo[0].timezone
                tutorName = userInfo[0].name
            else:
                for i in userInfo:
                    if i['id'] == user_id:
                        userTimezone = i['timezone']
                    else:
                        tutorName = i['name']

            userUTC = Timezone.objects.get(id=userTimezone).utc_time
            userTime = int(userUTC[:3])

            if userUTC[4] == '3':
                if userUTC[0] == '-':
                    userTime -= 0.5
                else:
                    userTime += 0.5

            # print 'utc', userTime
            list = Feedback.objects.filter(tutor_id=tutor_id, tutee_id=tutee_id
                                           ).exclude(revid=revid)
            # print list.query
            for i in list:
                obj = {}
                user = TellpinUser.objects.get(id=i.tutor_id)
                obj['revid'] = i.revid
                obj['tutor_id'] = i.tutor_id
                obj['tutor_name'] = tutorName
                obj['profile_photo'] = self.PROFILE_IMAGE_FILE_DIR + '/' + user.profile_photo
                obj['tutee_id'] = i.tutee_id
                obj['lid'] = i.tutee_id
                obj['comment'] = i.comment
                obj['created_date'] = i.created_date + relativedelta(hours=userTime)
                result.append(obj)
        except Exception as e:
            obj = {}
            obj['message'] = str(e)
            result.append(obj)
        finally:
            return result
