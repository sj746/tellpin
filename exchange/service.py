#-*- coding: utf-8 -*-
'''
Created on 2016. 1. 13.

@author: unichal
'''
###############################################################################
# filename    : exchange > service.py
# description : exchange service ( language exchange )
# author      : khyun@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 khyun 최초 작성
#
#
###############################################################################
from django.contrib.sessions.models import Session
from django.core.paginator import EmptyPage
from django.db import connection
from django.db.models import Q
from django.db.models.aggregates import Max, Min

# from common.models import SkillLevel
# from common.models import TutorHide, NationInfo, CityInfo, Lesson, Ratings, \
#     Specialtag, SpecialtagInfo, Profile, Follow, Tools, ToolsInfo
# from common.models import DjangoSession, TellpinUser, Language
# from MySQLdb.constants.FIELD_TYPE import NULL
# from common.common import get_login_users
# from tutor.service import TutorService
# from common import common
# from common.models import LeFriend


class UserService(object):
    """
    @summary: exchange user class
    @author: khyun
    @param none
    @return: url - none
    """
    NATION_FLAG_FILE_DIR = "/static/img/nations"
    PROFILE_IMAGE_FILE_DIR = "/static/img/upload/profile"
    
    VL_PERPAGE = 3
    RT_PERPAGE = 5
    


    def __init__(self, params):
        '''
        Constructor
        '''

    def dictfetchall(self, cursor):
        """
        @summary: exchange data 구조 변경 함수
        @author: khyun
        @param cursor : db connection cursor
        @return: dict data
        """
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ] 
    
    
    def get_city_name(self, _nid):
        """
        @summary: exchange get city name
        @author: khyun
        @param _nid : city id
        @return: object data
        """
        nat_obj={}
        nat_obj["citylist"] = []
        for city in CityInfo.objects.filter(nid=_nid):
            city_obj ={}
            city_obj["city_id"] = city.city_id
            city_obj["city"] = city.name
            nat_obj["citylist"].append(city_obj)
    
        return nat_obj
    
    #검색 데이터
    def get_search_data(self):
        """
        @summary: exchange 검색조건 데이터 검색 함수
        @author: khyun
        @param none
        @return: object data
        """
        try:
            result = []
            obj = {}
            
            obj["language_list"] = []
            for item in Language.objects.all():
                item_obj ={}
                item_obj["id"] = item.id
                item_obj["language"] = item.language1
                obj["language_list"].append(item_obj)
            
            obj["nation_info"] = []
            for nation in NationInfo.objects.all():
                nat_obj ={}
                nat_obj["nation"] = nation.name_en
                nat_obj["nation_id"]= nation.nid
                nat_obj["citylist"] =[]
                obj["nation_info"].append(nat_obj)
                
            
            obj["tool_list"] = []
            for item in ToolsInfo.objects.all().values("tool_name"):
                obj["tool_list"].append(item["tool_name"])
                
                
            obj["tag_list"] = []
            for item in SpecialtagInfo.objects.all():
                item_obj = {}
                item_obj["stname"] = item.stname
                item_obj["stid"] = item.stid
                obj["tag_list"].append(item_obj) 
                
            result.append(obj)
        except Exception as err:
            print str(err)
            
        return result
    #튜터 리스트업
    def listup_tutors(self, userID, search_keys=[], _type=0, _data=[], cur_page=0):
        """
        @summary: exchange get user list ( exchange 목록페이지 데이터 검색 ) function
        @author: khyun
        @param userID : user id
        @param search_keys : filtered user list
        @param _type : 1:검색리스트 조회, 0:초기페이지 호출 
        @param _data : 온라인상태 유저만 검색
        @param cur_page : 페이지 수
        @return: object data
        """
        from django.core.paginator import Paginator
        online_list = []
        
        online_list = get_login_users()
        if cur_page == 0:
            itemCount = 20
        else:
            itemCount = 20
        '''
        for item in DjangoSession.objects.all():
            session = Session.objects.get(session_key=item.session_key)
            uid = session.get_decoded().get('email')
            if uid:
                online_list.append(uid)
        print online_list
        print 'test _data'
        print _data
        if len(_data) > 0 :
            print _data[0]['is_login']
            print _data[0]['is_native']
        return
        '''
        print 'listsup_tutors2'
        hide_list = []
        friend_list = []
        friendState = [ 1, 3 ] # 요청, 수락
        if userID is not None:
            for h in TutorHide.objects.filter(tutee_id=userID):
                hide_list.append(h.tutor_id) 
            for i in LeFriend.objects.filter( ( Q( request_id = userID ) | Q( response_id = userID ) ), state__in = friendState  ) :
                if i.request_id == userID and i.req_del == 0:
                    friend_list.append( i.response_id )
                elif i.response_id == userID and i.res_del == 0:
                    friend_list.append( i.request_id )
                else:
                    continue
        
        print len(search_keys)
        print search_keys
        print _data
        try:
            if cur_page == 0:
                if _type == 1:
                    if len(_data) > 0 :
                        if _data[0]['is_login'] :
                            print 'is_login'
                            #if TellpinUser.objects.get(id=item.id).email not in online_list:
                            tutorlist = TellpinUser.objects.filter(id__in = search_keys, email__in=online_list ).exclude(id__in=hide_list).order_by("updated_time")
                        else:    
                            print 'all user'
                            tutorlist = TellpinUser.objects.filter(id__in = search_keys).exclude(id__in=hide_list).order_by("updated_time")
                    else: 
                        print 'no filter'
                        tutorlist = TellpinUser.objects.filter(id__in = search_keys).exclude(id__in=hide_list).order_by("updated_time")
                    
                    print tutorlist
                    print 'before length :', len(tutorlist)
                    #정보를 입력하지 않은 사용자 검색 제외
                    tutorlist = tutorlist.exclude(from_field__in = [NULL])
                    print ' exchange list : '
                    print tutorlist
                    for index, t in enumerate(tutorlist):
                        print index, t.id, t.name
                    #print "total" , Tutor.objects.filter(tid__in = search_keys).exclude(tid__in=hide_list).count()
                    p = Paginator(tutorlist, itemCount)
                    last_page = p.num_pages
                    try:
                        tutorlist = p.page(1)
                    except EmptyPage:
                        tutorlist = p.page(p.num_pages)
                else:
                    tutorlist = TellpinUser.objects.all().exclude(id__in=hide_list).order_by("updated_time")
                    #print "total" , Tutor.objects.all().exclude(tid__in=hide_list).count()
                    
                    p = Paginator(tutorlist, itemCount)
                    last_page = p.num_pages
                    try:
                        tutorlist = p.page(cur_page+1)
                    except EmptyPage:
                        tutorlist = p.page(p.num_pages)
            else:
                if _type == 1:
                    if len(_data) > 0 :
                        if _data[0]['is_login'] :
                            #if TellpinUser.objects.get(id=item.id).email not in online_list:
                            tutorlist = TellpinUser.objects.filter(id__in = search_keys, email__in=online_list ).exclude(id__in=hide_list).order_by("updated_time")
                        else:    
                            tutorlist = TellpinUser.objects.filter(id__in = search_keys).exclude(id__in=hide_list).order_by("updated_time")
                    else: 
                        tutorlist = TellpinUser.objects.filter(id__in = search_keys).exclude(id__in=hide_list).order_by("updated_time")
                    
                    print tutorlist
                    tutorlist = tutorlist.exclude(from_field__in = [NULL])
                    
                    print ' exchange list : '
                    print tutorlist
                    #print "total" , Tutor.objects.filter(tid__in = search_keys).exclude(tid__in=hide_list).count()
                    p = Paginator(tutorlist, itemCount )
                    last_page = p.num_pages
                    try:
                        tutorlist = p.page(cur_page+1)
                    except EmptyPage:
                        tutorlist = p.page(p.num_pages)
                else:
                    tutorlist = TellpinUser.objects.all().exclude(id__in=hide_list).order_by("updated_time")
                    #print "total" , Tutor.objects.all().exclude(tid__in=hide_list).count()
                    p = Paginator(tutorlist, itemCount )
                    last_page = p.num_pages
                    try:
                        tutorlist = p.page(cur_page+1)
                    except EmptyPage:
                        tutorlist = p.page(p.num_pages)
                    
            result = []
            print 'length: '+str(len(tutorlist))
            for item in tutorlist:
                print item.id, item.name
            for item in tutorlist:
                #if len(_data) > 0 :
                    #if _data[0]['is_login'] and TellpinUser.objects.get(id=item.user_id).email in online_list:
                        obj = {}
                        
                        #print 'test2'
                        #print TellpinUser.objects.get(id=item.user_id).email
                        #print online_list
                        
                        '''
                        if len(_data) > 0 :
                            if _data[0]['is_login'] :
                                if TellpinUser.objects.get(id=item.id).email not in online_list:
                                    continue
                        '''
                        obj["tid"] = item.id
                        obj['is_friend'] = item.id in friend_list
                        obj["name"] = item.name
                        obj["birth"] = item.birth
                        if TellpinUser.objects.get(id=item.id).email in online_list: 
                            obj["is_login"] = 1
                        else:
                            obj["is_login"] = 0
                        obj["profile_photo"] = self.PROFILE_IMAGE_FILE_DIR + "/" +item.profile_photo
                        print 'from'
                        print item.from_field
                        print item.from_city
                        print item.livein
                        print item.livein_city
                        try:
                            obj["nation_from"] = NationInfo.objects.get(nid=item.from_field).name_en
                        except:
                            obj["nation_from"] = ""
                        try:
                            obj["city_from"] = CityInfo.objects.get(city_id=item.from_city).name
                        except:
                            print '??'
                            obj["city_from"] = ""
                        try:
                            obj["nation_live"] = NationInfo.objects.get(nid=item.livein).name_en
                        except:
                            print '???'
                            obj["nation_live"] = ""
                        try:
                            obj["city_live"] = CityInfo.objects.get(city_id=item.livein_city).name
                        except:
                            print '????'
                            obj["city_live"] = ""
                        try:
                            obj["nation_flag"] = self.NATION_FLAG_FILE_DIR + "/" + NationInfo.objects.get(nid=item.from_field).image
                        except:
                            obj["nation_flag"] = ""
                        
                        #speak, teach
                        my_speak_list = []
                        if item.native1 is not None:
                            spk_item = {}
                            try:
                                spk_item["name"] = Language.objects.get(id=item.native1).language1
                            except:
                                spk_item["name"] = ""
                            spk_item["is_native"] = 1;
                            spk_item["level"] = 7
                            my_speak_list.append(spk_item)
                        if item.native2 is not None:
                            spk_item = {}
                            try:
                                spk_item["name"] = Language.objects.get(id=item.native2).language1
                            except:
                                spk_item["name"] = ""
                            spk_item["is_native"] = 1;
                            spk_item["level"] = 7
                            my_speak_list.append(spk_item)
                        if item.native3 is not None:
                            spk_item = {}
                            try:
                                spk_item["name"] = Language.objects.get(id=item.native3).language1
                            except:
                                spk_item["name"] = ""
                            spk_item["is_native"] = 1;
                            spk_item["level"] = 7
                            my_speak_list.append(spk_item)
                        
                        tutorService = TutorService( self )    
                        if item.other1 is not None:
                            spk_item = {}
                            try:
                                spk_item["name"] = Language.objects.get(id=item.other1).language1
                            except:
                                spk_item["name"] = ""
                            spk_item["is_native"] = 0;
                            spk_item["level"] = item.other1_skill_level
                            spk_item["level_name"] = tutorService.get_skill_level_name( item.other1_skill_level )
                            my_speak_list.append(spk_item)
                        if item.other2 is not None:
                            spk_item = {}
                            try:
                                spk_item["name"] = Language.objects.get(id=item.other2).language1
                            except:
                                spk_item["name"] = ""
                            spk_item["is_native"] = 0;
                            spk_item["level"] = item.other2_skill_level
                            spk_item["level_name"] = tutorService.get_skill_level_name( item.other2_skill_level )
                            my_speak_list.append(spk_item)
                        if item.other3 is not None:
                            spk_item = {}
                            try:
                                spk_item["name"] = Language.objects.get(id=item.other3).language1
                            except:
                                spk_item["name"] = ""
                            spk_item["is_native"] = 0;
                            spk_item["level"] = item.other1_skill_level
                            spk_item["level_name"] = tutorService.get_skill_level_name( item.other3_skill_level )
                            my_speak_list.append(spk_item)
                        
                        obj["speaks"] = my_speak_list    
                            
                            
                        '''    
                        for lang in Speaks.objects.filter(tid=tutor_info.tid):
                            if lang.language_id > 11 or lang.language_id < 1: continue
                            spk_item = {}
                            spk_item["name"] = Language.objects.get(id=lang.language_id).language1
                            spk_item["level"] = lang.skill_level
                            my_speak_list.append(spk_item)
                        obj["speaks"] = my_speak_list
                        '''
                            
                        my_teach_list = []
                        if item.teaching1 is not None:
                            tch_item = {}
                            
                            try:
                                tch_item["name"] = Language.objects.get(id=item.teaching1).language1
                            except:
                                tch_item["name"] = ""
                            tch_item["is_native"]=0;
                            if item.teaching1 == item.native1 or item.teaching1 == item.native2 or item.teaching1 == item.native3:
                                tch_item["is_native"] = 1;
                            tch_item["level"] = 7
                            tch_item["level_name"] = "native"
                            if item.teaching1 == item.other1:
                                tch_item["level"] = item.other1_skill_level
                                tch_item["level_name"] = tutorService.get_skill_level_name( item.other1_skill_level )
                            elif item.teaching1 == item.other2:
                                tch_item["level"] = item.other2_skill_level
                                tch_item["level_name"] = tutorService.get_skill_level_name( item.other2_skill_level )
                            elif item.teaching1 == item.other3:
                                tch_item["level"] = item.other3_skill_level
                                tch_item["level_name"] = tutorService.get_skill_level_name( item.other3_skill_level )
                            my_teach_list.append(tch_item)
                        if item.teaching2 is not None:
                            tch_item = {}
                            try:
                                tch_item["name"] = Language.objects.get(id=item.teaching2).language1
                            except:
                                tch_item["name"] = ""
                            tch_item["is_native"]=0;
                            if item.teaching2 == item.native1 or item.teaching2 == item.native2 or item.teaching2 == item.native3:
                                tch_item["is_native"] = 1;
                            tch_item["level"] = 7
                            tch_item["level_name"] = "native"
                            if item.teaching2 == item.other1:
                                tch_item["level"] = item.other1_skill_level
                                tch_item["level_name"] = tutorService.get_skill_level_name( item.other1_skill_level )
                            elif item.teaching2 == item.other2:
                                tch_item["level"] = item.other2_skill_level
                                tch_item["level_name"] = tutorService.get_skill_level_name( item.other2_skill_level )
                            elif item.teaching2 == item.other3:
                                tch_item["level"] = item.other3_skill_level
                                tch_item["level_name"] = tutorService.get_skill_level_name( item.other3_skill_level )
                            my_teach_list.append(tch_item)
                        if item.teaching3 is not None:
                            tch_item = {}
                            try:
                                tch_item["name"] = Language.objects.get(id=item.teaching3).language1
                            except:
                                tch_item["name"] = ""
                            tch_item["is_native"]=0;
                            if item.teaching3 == item.native1 or item.teaching3 == item.native2 or item.teaching3 == item.native3:
                                tch_item["is_native"] = 1;
                            tch_item["level"] = 7
                            if item.teaching3 == item.other1:
                                tch_item["level"] = item.other1_skill_level
                                tch_item["level_name"] = tutorService.get_skill_level_name( item.other1_skill_level )
                            elif item.teaching3 == item.other2:
                                tch_item["level"] = item.other2_skill_level
                                tch_item["level_name"] = tutorService.get_skill_level_name( item.other2_skill_level )
                            elif item.teaching3 == item.other3:
                                tch_item["level"] = item.other3_skill_level
                                tch_item["level_name"] = tutorService.get_skill_level_name( item.other3_skill_level )
                            my_teach_list.append(tch_item)
                        
                        obj["teaches"] = my_teach_list
                        if self.is_tutor(item.id):
                            obj["is_tutor"] = 1
                        else:
                            obj["is_tutor"] = 0
                        #learning
                        my_learning_list = []
                        if item.learning1 is not None:
                            learning_item = {}
                            learning_item["name"] = Language.objects.get(id=item.learning1).language1
                            learning_item["is_native"] = 0
                            if item.learning1_skill_level:
                                learning_item["level"] = item.learning1_skill_level
                                learning_item["level_name"] = tutorService.get_skill_level_name( item.learning1_skill_level )
                            else:
                                learning_item["level"] = 1
                                learning_item["level_name"] = SkillLevel.objects.get(level_id=1).level_name[3:]
                            
                            my_learning_list.append(learning_item)
                        if item.learning2 is not None:
                            learning_item = {}
                            learning_item["name"] = Language.objects.get(id=item.learning2).language1
                            learning_item["is_native"] = 0
                            if item.learning2_skill_level:
                                learning_item["level"] = item.learning2_skill_level
                                learning_item["level_name"] = tutorService.get_skill_level_name( item.learning2_skill_level )
                            else:
                                learning_item["level"] = 1
                                learning_item["level_name"] = SkillLevel.objects.get(level_id=1).level_name[3:]
                            
                            my_learning_list.append(learning_item)
                        if item.learning3 is not None:
                            learning_item = {}
                            learning_item["name"] = Language.objects.get(id=item.learning3).language1
                            learning_item["is_native"] = 0
                            if item.learning3_skill_level:
                                learning_item["level"] = item.learning3_skill_level
                                learning_item["level_name"] = tutorService.get_skill_level_name( item.learning3_skill_level )
                            else:
                                learning_item["level"] = 1
                                learning_item["level_name"] = SkillLevel.objects.get(level_id=1).level_name[3:]
                            
                            my_learning_list.append(learning_item)
                        
                        obj["learnings"] = my_learning_list    
                        '''
                        for lang in Teaches.objects.filter(tid=tutor_info.tid):
                            if lang.language_id > 11 or lang.language_id < 1: continue
                            tch_item = {}
                            tch_item["name"] = Language.objects.get(id=lang.language_id).language1
                            tch_item["level"] = lang.skill_level
                            my_teach_list.append(tch_item)
                        obj["teaches"] = my_teach_list
                        '''
#                          
#                         #rating 정보 가져오기
#                         total_rating = 0
#                         total_num = 0
#                         my_lesson_list = Lesson.objects.filter(user_id=item.id).values('lid')
#                         for lesson in my_lesson_list:
#                             num = Ratings.objects.filter(lid=lesson['lid']).count()
#                             if num < 1: continue
#                             for rating in Ratings.objects.filter(lid=lesson['lid']).values('rating'):
#                                 total_rating += rating['rating']
#                             total_num += num
#                         if total_num < 1:
#                             ave_rating = 0
#                         else:
#                             ave_rating = total_rating / float(total_num)
#                          
#                         obj["ave_rating"] = ave_rating
                        '''
                        #lessons, tutee 정보 가져오기
                        Reservation.objects.filter(tid=item.tid)
                         
                        obj["tutees"] = Reservation.objects.filter(tid=item.tid).values('user_id').annotate(Count('user_id'))
                        print obj["tutees"]
                        obj["ave_rating"] = ave_rating
                         
                        '''
                         
#                         #tag 가져오기
#                         my_tag_list = []
#                         for tag in Specialtag.objects.filter(user_id=item.id):
#                             item_list = {}
#                             try:
#                                 item_list["stname"] = SpecialtagInfo.objects.get(stid=tag.stid).stname
#                                 item_list["stid"] = SpecialtagInfo.objects.get(stid=tag.stid).stid
#                                 my_tag_list.append(item_list);
#                             except:
#                                 continue
#                         obj["tag_list"] = my_tag_list
                         
                     
                        #description
                        
                        obj["description"] = item.quick_intro
#                         obj["video_intro"] = my_profile.video_intro
                         
#                         #follow info
#                         obj["follow_cnt"] = Follow.objects.filter(followee_id=item.id).count()
#                         try:
#                             Follow.objects.get(followee_id=item.id, follower_id=userID)
#                             obj["is_follower"] = 1
#                         except:
#                             obj["is_follower"] = 0
#                              
                             
#                         #tools list
#                         tool_list = []
#                         my_tools = Tools.objects.get(user_id=item.id)
#                         tool_list.append({"offline":my_tools.offline})
#                         tool_list.append({"audio":my_tools.audio})
#                         tool_list.append({"skype":my_tools.skype})
#                         tool_list.append({"hangout":my_tools.hangout})
#                         tool_list.append({"facetime":my_tools.facetime})
#                         tool_list.append({"qq":my_tools.qq})
#                         tool_list.append({"wechat":my_tools.wechat})
#                         obj["tool_list"] = tool_list
#                         obj["tool_comment"] = my_tools.comment
#                          
#                         #lesson price 
#                         #trial
#                         try:
#                             obj["trial_price"] = Lesson.objects.get(user_id=item.id, type=0).online_30m_1times_cost
#                         except Exception as err:
#                             #print "not trial ", str(err)
#                             obj["trial_price"] = 0
#                         if obj["trial_price"] is not None :
#                             obj["trial_price_ex"] = int(obj["trial_price"]) * 100
#                          
#                          
#                         #lesson_price = Lesson.objects.filter(tid=item.tid, type=1).aggregate(Min('online_60m_1times_cost'),Max('online_60m_1times_cost'))
#                          
#                         cost_obj = Lesson.objects.filter(user_id=item.id, type=1, del_yn=0).aggregate(Max('online_30m_1times_cost'),Max('online_60m_1times_cost'),Max('offline_60m_1times_cost'),Max('offline_90m_1times_cost'),Max('offline_120m_1times_cost'))
#                         if cost_obj["online_30m_1times_cost__max"] is None:
#                             cost_obj["online_30m_1times_cost__max"] = 0
#                          
#                         if cost_obj["offline_90m_1times_cost__max"] is None:
#                             cost_obj["offline_90m_1times_cost__max"] = 0
#                         if cost_obj["offline_120m_1times_cost__max"] is None:
#                             cost_obj["offline_120m_1times_cost__max"] = 0
#                         cost_list = [ cost_obj["online_30m_1times_cost__max"] * 2, cost_obj["online_60m_1times_cost__max"], cost_obj["offline_60m_1times_cost__max"], (cost_obj["offline_90m_1times_cost__max"] * 2 / 3), cost_obj["offline_120m_1times_cost__max"] / 2 ] 
#                         obj["lesson_max_price"] = max(cost_list)
#                          
#                          
#                         cost_list = [0, 0, 0, 0, 0]
#                         cost_obj = Lesson.objects.filter(user_id=item.id, type=1, del_yn=0, online_30m_1times_cost__gt=0).aggregate(Min('online_30m_1times_cost'))
#                         if cost_obj["online_30m_1times_cost__min"] is not None:
#                             cost_list[0] = (cost_obj["online_30m_1times_cost__min"] * 2)
#                         else :
#                             cost_list[0] = 0
#                         cost_obj = Lesson.objects.filter(user_id=item.id, type=1, del_yn=0, online_60m_1times_cost__gt=0).aggregate(Min('online_60m_1times_cost'))
#                         if cost_obj["online_60m_1times_cost__min"] is not None:
#                             cost_list[1] = (cost_obj["online_60m_1times_cost__min"])
#                         else: 
#                             cost_list[1] = 0
#                         cost_obj = Lesson.objects.filter(user_id=item.id, type=1, del_yn=0, offline_60m_1times_cost__gt=0).aggregate(Min('offline_60m_1times_cost'))
#                         if cost_obj["offline_60m_1times_cost__min"] is not None:
#                             cost_list[2] = (cost_obj["offline_60m_1times_cost__min"])
#                         else:
#                             cost_list[2] = 0
#                         cost_obj = Lesson.objects.filter(user_id=item.id, type=1, del_yn=0, offline_90m_1times_cost__gt=0).aggregate(Min('offline_90m_1times_cost'))
#                         if cost_obj["offline_90m_1times_cost__min"] is not None:
#                             val = cost_obj["offline_90m_1times_cost__min"] / float(3) * 2
#                             cost_list.append(int(val))
#                         else:
#                             val = 0
#                             cost_list.append(int(val))
#                         cost_obj = Lesson.objects.filter(user_id=item.id, type=1, del_yn=0, offline_120m_1times_cost__gt=0).aggregate(Min('offline_120m_1times_cost'))
#                         if cost_obj["offline_120m_1times_cost__min"] is not None:
#                             cost_list.append(cost_obj["offline_120m_1times_cost__min"] / 2)
#                         else:
#                             cost_list.append(0)
#                              
#                         res = [num for num in cost_list if num > 0]
#                         if res :
#                             obj["lesson_min_price"] = min(res)
#                         else :
#                             obj["lesson_min_price"] = 0
#                          
#                          
#                         #if lesson_price["online_60m_1times_cost__min"]:
#                         #    obj["lesson_min_price"] = lesson_price["online_60m_1times_cost__min"]
#                         #else:
#                         #    obj["lesson_min_price"] = 0
#                         obj["lesson_min_price_ex"] = int(obj["lesson_min_price"]) * 100
#                              
#                         #if lesson_price["online_60m_1times_cost__max"]:    
#                         #    obj["lesson_max_price"] = lesson_price["online_60m_1times_cost__max"]
#                         #else:
#                         #    obj["lesson_max_price"] = 0
#                         obj["lesson_max_price_ex"] = int(obj["lesson_max_price"]) * 100
#                          
#                         obj["price_unit"] = "krw"
                        
                        result.append(obj)
                
                    
        except Exception as err:
            print str(err)
        
        last_page -= 1
        page_info = { "page": cur_page, "last_page": last_page}
        
        return result, page_info
    
    #필터 튜터 
    def ajax_listup_tutors(self, user_id, data):
        """
        @summary: exchange user filtering function
        @author: khyun
        @param user_id : user id
        @param data : filtering 조건
        @return: user list
        """
        result = {}
        _type = 1;
        _data = []
        
        #page info
        cur_page = data["page"]
        
        #print data
        sql_string = "select distinct id from v_searchfilter_user where 1=1 "
        
        print 'ajax_listup_tutors'
        print user_id
        print data
        try:
            #search name
            if data['type'] == 'name':
                #TODO
                #name search
                sql_string += "and name like '%" + data['name'] + "%' group by id"
            else:
                #TODO
                #filter search
                speaks = data['speaks'] #
                learning = data['learning'] #
                gender = data['gender'] #
                age = data['age'] #
                nation_from = data['nation_from']#
                city_from = data['city_from']
                livein = data['livein']#
                liveincity = data['liveincity']#
                native = data['native']
                online = data['online']
            
                if(speaks != "all"):
                    sql_string += " and ( native1=" + speaks + " or native2=" + speaks + " or native3=" + speaks + " or other1=" + speaks + " or other2=" + speaks + " or other3=" + speaks + " ) "
                if(learning != "all"):
                    sql_string += " and ( learning1=" + learning + " or learning2=" + learning + " or learning3=" + learning + " ) "
                if(gender != "all"):
                    sql_string += " and gender=" + gender + " "
                if(age != "all"):
                    if age == '1':
                        sql_string += " and age < 20 "
                    else:
                        sql_string += " and (age >= " + age + " and age < " + str(int(age)+10) + ") "
                if(nation_from != "all"):
                    sql_string += " and nation_from_id=" + nation_from + " "
                if(city_from != "all"):
                    sql_string += " and city_from_id=" + city_from + " "
                if(livein != "all"):
                    sql_string += " and nation_live_id=" + livein + " "
                if(liveincity != "all"):
                    sql_string += " and city_live_id=" + liveincity + " "
                
                                    
                
                #native_only
                if native:
                    sql_string += " and (native1="+speaks+" or native2="+speaks+" or native3="+speaks+ " )"
                
                    
                sql_string += " group by id"
                
                print sql_string
                list = {}
                list["is_login"] = online
                list["is_native"] = native
                _data.append(list)
        except Exception as e:
            print 'errrror'
            print e
        
        print sql_string
        tutor_list = [];
        cursor = connection.cursor()
        cursor.execute(sql_string)
        result = self.dictfetchall(cursor)
        
        for tutor in result:
            tutor_list.append(tutor['id'])
        
        
        return self.listup_tutors(user_id , tutor_list, _type, _data, cur_page)
