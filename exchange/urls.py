from django.conf.urls import url
import views

urlpatterns = [
    # url(r'^$', views.detailUser),
    url(r'^user/(?P<user_id>\d+)/$', views.detailUser, name='detailUser'),
    url(r'^exchange/more/community/$', views.moreCommunity),
    url(r'^exchange/newfriend/$', views.ajax_newFriend),
    url(r'^exchange/newmessage/$', views.ajax_newMessage),
    url(r'^exchange/getmessage/$', views.ajax_getMessage),
    url(r'^exchange/$', views.exchange_list),
    url(r'^exchange/search_tutor/$', views.exchange_finder_search),
    url(r'^exchange/morelist/$', views.ajax_moreList),
    url(r'^exchange/pagefeedback/$', views.page_moreFeedback),
    url(r'^exchange/morecomment/$', views.moreFeedback),
]
