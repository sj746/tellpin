from django.conf.urls import url
import views

urlpatterns = [
               url( r'^tutor/$', views.myTutor ),
               url( r'^friend/$', views.myFriend ),
               url( r'^friend/request/$', views.getRequestFriends ),
               url( r'^friend/receive/$', views.getReceiveFriends ),
               url( r'^friend/accept/$', views.acceptFriends ),
               url( r'^friend/decline/$', views.declineFriend ),
               url( r'^friend/delete/$', views.deleteFriend ),
               url( r'^friend/cancel/$', views.cancelFriend ),
               url( r'^tutee/$', views.myTutee ),
               url( r'^tutor/hide/$', views.hideUser ),
]