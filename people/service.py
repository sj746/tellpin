#-*- coding: utf-8 -*-

###############################################################################
# filename    : people > peopleService.py
# description : people app
# author      : msjang@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 msjang 최초 작성
#
#
###############################################################################

from datetime import datetime

from django.db import connection
from django.db.models import Q

from common import common
from common.common import get_login_users
from common.service import CommonService
from dashboard.models import Message
from tutor.models import Bookmark, Tutor, Friend, Tutee, FriendHide
from tutoring.models import LessonReservation
from user_auth.models import TellpinUser
from django.forms.models import model_to_dict
from django.db.models.aggregates import Max


class PeopleService(object):

    def __init__(self):
        pass

    @staticmethod
    def getTutorList(_user_id):
        """
        @summary: Tutor list
        @author: msjang
        @param user_id: 유저 id 번호
        @return: result( list )
        """
        result = []
#         try:#bookmark
#             tutorList = Bookmark.objects.filter(user_id=_user_id).order_by("-created_time")
#             for myTutor in tutorList:
#                 myTutor_dict = model_to_dict(myTutor, exclude=['created_time'])
#                 tutor = Tutor.objects.get(user_id=myTutor_dict["tutor"])
# 
#                 myTutor_dict = model_to_dict(tutor,
#                                                     exclude=['created_time', 'updated_time', 'del_by_user', 'del_by_admin',
#                                                              'is_available', 'has_schedule', 'has_course', 'schedule_comment',
#                                                              'gender', 'birthdate', 'how_to_meet', 'skype_id', 'facetime_id', 'hangout_id',
#                                                              'qq_id', 'has_trial', 'trial_price', 'total_balance', 'available_balance',
#                                                              'pending_balance', 'withdrawal_pending', 'certificate1', 'certificate2',
#                                                              'certificate3', 'education1', 'education2', 'education3', 'paymentinfo',
#                                                              'work1', 'work2', 'work3', 'audio_only' ])
# 
#                 if tutor.from_country_id:
#                     myTutor_dict['country_filename'] = '%s/%s' % (CommonService.NATION_FILE_DIR,
#                                                                    tutor.from_country_id.flag_filename)
#                 else:
#                     myTutor_dict['country_filename'] = '%s' % (CommonService.NATION_FILE_DIR)
# 
#                 if tutor.photo_filename:
#                     myTutor_dict['photo_filename'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
#                                                                  tutor.photo_filename)
#                 else:
#                     myTutor_dict['photo_filename'] = '%s' % (CommonService.PROFILE_IMAGE_FILE_DIR)
# 
#                 myTutor_dict['rate'] = 5
#                 myTutor_dict['type'] = 1
# 
# #                 if tutor.email in online_list:
# #                     myTutor_dict["is_login"] = 1
# #                 else:
# #                     myTutor_dict["is_login"] = 0
#                 result.append(myTutor_dict)
# 
#         except Exception as e:
#             print '@@@@@PeopleService getTutorList11 : ', str(e)
        try:#tutor
            lessonList = LessonReservation.objects.filter(tutee=_user_id, pkg_order=1).values('tutor')\
                                        .annotate(lastest_created_time=Max('created_time'))\
                                        .values_list('tutor', flat=True)
            for myTutor in lessonList:
#                 myTutor_dict = model_to_dict(myTutor, exclude=['created_time'])
                tutor = Tutor.objects.get(user_id=myTutor)
                myTutor_dict = model_to_dict(tutor,
                                                    exclude=['created_time', 'updated_time', 'del_by_user', 'del_by_admin',
                                                             'is_available', 'has_schedule', 'has_course', 'schedule_comment',
                                                             'gender', 'birthdate', 'how_to_meet', 'skype_id', 'facetime_id', 'hangout_id',
                                                             'qq_id', 'has_trial', 'trial_price', 'total_balance', 'available_balance',
                                                             'pending_balance', 'withdrawal_pending', 'certificate1', 'certificate2',
                                                             'certificate3', 'education1', 'education2', 'education3', 'paymentinfo',
                                                             'work1', 'work2', 'work3', 'audio_only' ])

                if tutor.from_country_id:
                    myTutor_dict['country_filename'] = '%s/%s' % (CommonService.NATION_FILE_DIR,
                                                                   tutor.from_country_id.flag_filename)
                else:
                    myTutor_dict['country_filename'] = '%s' % (CommonService.NATION_FILE_DIR)

                if tutor.photo_filename:
                    myTutor_dict['photo_filename'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                                 tutor.photo_filename)
                else:
                    myTutor_dict['photo_filename'] = '%s' % (CommonService.PROFILE_IMAGE_FILE_DIR)

                myTutor_dict['rate'] = 5
                myTutor_dict['type'] = 2

                result.append(myTutor_dict)

        except Exception as e:
            print '@@@@@PeopleService getTutorList : ', str(e)
#         try:#contact
#             messageList = Message.objects.filter(sender_id = _user_id).order_by("-created_time")
#             for myTutor in messageList:
#                 myTutor_dict = model_to_dict(myTutor, exclude=['created_time'])
#                 tutor = Tutor.objects.get(user_id=myTutor_dict["tutor"])
#                 myTutor_dict = model_to_dict(tutor,
#                                                     exclude=['created_time', 'updated_time', 'del_by_user', 'del_by_admin',
#                                                              'is_available', 'has_schedule', 'has_course', 'schedule_comment',
#                                                              'gender', 'birthdate', 'how_to_meet', 'skype_id', 'facetime_id', 'hangout_id',
#                                                              'qq_id', 'has_trial', 'trial_price', 'total_balance', 'available_balance',
#                                                              'pending_balance', 'withdrawal_pending', 'certificate1', 'certificate2',
#                                                              'certificate3', 'education1', 'education2', 'education3', 'paymentinfo',
#                                                              'work1', 'work2', 'work3', 'audio_only' ])
# 
#                 if tutor.from_country_id:
#                     myTutor_dict['country_filename'] = '%s/%s' % (CommonService.NATION_FILE_DIR,
#                                                                    tutor.from_country_id.flag_filename)
#                 else:
#                     myTutor_dict['country_filename'] = '%s' % (CommonService.NATION_FILE_DIR)
# 
#                 if tutor.photo_filename:
#                     myTutor_dict['photo_filename'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
#                                                                  tutor.photo_filename)
#                 else:
#                     myTutor_dict['photo_filename'] = '%s' % (CommonService.PROFILE_IMAGE_FILE_DIR)
#                 myTutor_dict['rate'] = 5
#                 myTutor_dict['type'] = 3
# 
#         except Exception as e:
#             print '@@@@@PeopleService getTutorList33 : ', str(e)
        finally:
            return result

    # 튜티 목록 가져오기
    @staticmethod
    def getTutees(user_id):
        """
        @summary: 튜티 목록
        @author: msjang
        @param user_id: 사용자 id 번호
        @return: result( list )
        """
        result = []

        try:#tutor
            tuteeList = LessonReservation.objects.filter(tutor=user_id, pkg_order=1).values('tutee')\
                                        .annotate(lastest_created_time=Max('created_time'))\
                                        .values_list('tutee', flat=True)
#             print tuteeList
            for myTutee in tuteeList:
                tutee = Tutee.objects.get(user_id=myTutee)
                myTutee_dict = model_to_dict(tutee,
                            exclude=['created_time', 'updated_time',
                                     'del_by_user', 'del_by_admin',
                                     'gender', 'birthdate', 'how_to_meet',
                                     'skype_id', 'facetime_id', 'hangout_id',
                                     'qq_id', 'has_trial', 'trial_price',
                                     'total_balance', 'available_balance',
                                     'pending_balance', 'withdrawal_pending'])

                if tutee.from_country_id:
                    myTutee_dict['country_filename'] = '%s/%s' % (CommonService.NATION_FILE_DIR,
                                                                  tutee.from_country_id.flag_filename)
                else:
                    myTutee_dict['country_filename'] = '%s' % (CommonService.NATION_FILE_DIR)

                if tutee.photo_filename:
                    myTutee_dict['photo_filename'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                                          tutee.photo_filename)
                else:
                    myTutee_dict['photo_filename'] = '%s' % (CommonService.PROFILE_IMAGE_FILE_DIR)

                if LessonReservation.objects.filter(tutor=user_id, tutee=myTutee, lesson_status__in=[3, 4, 5, 7]).exists():
                    myTutee_dict['stat'] = 0
                else:
                    myTutee_dict['stat'] = 1

                result.append(myTutee_dict)

        except Exception as e:
            print '@@@@@PeopleService getTutees : ', str(e)
        finally:
            return result

    @staticmethod
    def getBookmarkList(user_id):
        """
        @summary: Tutor list
        @author: msjang
        @param user_id: 유저 id 번호
        @return: result( list )
        """
        result = []
#         online_list = CommonService.get_login_users()

        try :#bookmark
            tutorList = Bookmark.objects.filter(user_id=user_id).order_by("-created_time")
            for myTutor in tutorList:
                myTutor_dict = model_to_dict(myTutor, exclude=['created_time'])

                tutor = Tutor.objects.get(user_id=myTutor_dict["tutor"])

#                 tutor = Tutor.objects.select_related(
#                                                     'from_country_id', 'from_city_id', 'livingin_country_id', 'livingin_city_id',
#                                                     'timezone_id', 'currency_id',
#                                                     'native1_id', 'native2_id', 'native3_id',
#                                                     'lang1_id', 'lang2_id', 'lang3_id', 'lang4_id', 'lang5_id', 'lang6_id',
#                                                     'teaching1_id', 'teaching2_id', 'teaching3_id',
#                                                      ).get(user_id=myTutor_dict["tutor"])
                myTutor_dict = model_to_dict(tutor,
                                                    exclude=['created_time', 'updated_time', 'del_by_user', 'del_by_admin',
                                                             'is_available', 'has_schedule', 'has_course', 'schedule_comment',
                                                             'gender', 'birthdate', 'how_to_meet', 'skype_id', 'facetime_id', 'hangout_id',
                                                             'qq_id', 'has_trial', 'trial_price', 'total_balance', 'available_balance',
                                                             'pending_balance', 'withdrawal_pending', 'certificate1', 'certificate2',
                                                             'certificate3', 'education1', 'education2', 'education3', 'paymentinfo',
                                                             'work1', 'work2', 'work3', 'audio_only' ])

                if tutor.from_country_id:
                    myTutor_dict['country_filename'] = '%s/%s' % (CommonService.NATION_FILE_DIR,
                                                                   tutor.from_country_id.flag_filename)
                else:
                    myTutor_dict['country_filename'] = '%s' % (CommonService.NATION_FILE_DIR)

                if tutor.photo_filename:
                    myTutor_dict['photo_filename'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                                 tutor.photo_filename)
                else:
                    myTutor_dict['photo_filename'] = '%s' % (CommonService.PROFILE_IMAGE_FILE_DIR)

                myTutor_dict['rate'] = 5
                myTutor_dict['type'] = 1

                result.append(myTutor_dict)

        except Exception as e:
            print '@@@@@PeopleService getBookmarkList : ', str(e)

        finally:
            return result

    # 내 친구목록
    @staticmethod
    def getFriends(user_id):
        """
        @summary: 친구목록
        @author: msjang
        @param user_id: 사용자 id 번호
        @return: result( list )
        """
        '''
            내가 요청보내거나 요청받은 것중에 수락된것만
        '''
        result = []

        try:
            friendsList = Friend.objects.filter(Q(req_user=user_id, status=2) | Q(res_user=user_id, status=2)).order_by("created_time")
            for myFriend in friendsList:
                myFriend_dict = model_to_dict(myFriend, exclude=['created_time'])

                if myFriend_dict["req_user"] == user_id:
                    if Tutor.objects.filter(user=myFriend_dict["res_user"]).exists():
                        friend = Tutor.objects.get(user=myFriend_dict["res_user"])
                    else:
                        friend = Tutee.objects.get(user=myFriend_dict["res_user"])
                    myFriend_list = model_to_dict(friend,
                                                        exclude=['created_time', 'updated_time', 'del_by_user', 'del_by_admin',
                                                                 'is_available', 'has_schedule', 'has_course', 'schedule_comment',
                                                                 'gender', 'birthdate', 'how_to_meet', 'skype_id', 'facetime_id', 'hangout_id',
                                                                 'qq_id', 'has_trial', 'trial_price', 'total_balance', 'available_balance',
                                                                 'pending_balance', 'withdrawal_pending', 'certificate1', 'certificate2',
                                                                 'certificate3', 'education1', 'education2', 'education3', 'paymentinfo',
                                                                 'work1', 'work2', 'work3', 'audio_only' ])
                elif myFriend_dict["res_user"] == user_id:
                    if Tutor.objects.filter(user=myFriend_dict["req_user"]).exists():
                        friend = Tutor.objects.get(user=myFriend_dict["req_user"])
                    else:
                        friend = Tutee.objects.get(user=myFriend_dict["req_user"])
                    myFriend_list = model_to_dict(friend,
                                                        exclude=['created_time', 'updated_time', 'del_by_user', 'del_by_admin',
                                                                 'gender', 'birthdate', 'how_to_meet', 'skype_id', 'facetime_id', 'hangout_id',
                                                                 'qq_id', 'has_trial', 'trial_price', 'total_balance', 'available_balance',
                                                                 'pending_balance', 'withdrawal_pending', ])

                if friend.from_country_id:
                    myFriend_list['country_filename'] = '%s/%s' % (CommonService.NATION_FILE_DIR,
                                                                    friend.from_country_id.flag_filename)
                else:
                    myFriend_list['country_filename'] = '%s' % (CommonService.NATION_FILE_DIR)

                if friend.photo_filename:
                    myFriend_list['photo_filename'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                                  friend.photo_filename)
                else:
                    myFriend_list['photo_filename'] = '%s' % (CommonService.PROFILE_IMAGE_FILE_DIR)

                myFriend_list['is_tutor'] = friend.user.is_tutor

                result.append(myFriend_list)

        except Exception as e:
            print '@@@@@PeopleService getFriends : ', str(e)
        finally:
            return result

    @staticmethod
    def getReceiveFriends(user_id):
        """
        @summary: 내가 요청받은 친구목록
        @author: msjang
        @param user_id: 사용자 id 번호
        @return: result( list )
        """
        result = []

        try:
            requestFriendsList = Friend.objects.filter(res_user=user_id, status=1).order_by("-created_time")
            for myRequest in requestFriendsList:
                myRequest_dict = model_to_dict(myRequest, exclude=['created_time'])

                if Tutor.objects.filter(user=myRequest_dict["req_user"]).exists():
                    friends = Tutor.objects.get(user=myRequest_dict["req_user"])
                    myRequest_dict = model_to_dict(friends,
                                                        exclude=['created_time', 'updated_time', 'del_by_user', 'del_by_admin',
                                                                 'is_available', 'has_schedule', 'has_course', 'schedule_comment',
                                                                 'gender', 'birthdate', 'how_to_meet', 'skype_id', 'facetime_id', 'hangout_id',
                                                                 'qq_id', 'has_trial', 'trial_price', 'total_balance', 'available_balance',
                                                                 'pending_balance', 'withdrawal_pending', 'certificate1', 'certificate2',
                                                                 'certificate3', 'education1', 'education2', 'education3', 'paymentinfo',
                                                                 'work1', 'work2', 'work3', 'audio_only' ])
                else:
                    friends = Tutee.objects.get(user=myRequest_dict["req_user"])
                    myRequest_dict = model_to_dict(friends,
                                                        exclude=['created_time', 'updated_time', 'del_by_user', 'del_by_admin',
                                                                 'gender', 'birthdate', 'how_to_meet', 'skype_id', 'facetime_id', 'hangout_id',
                                                                 'qq_id', 'has_trial', 'trial_price', 'total_balance', 'available_balance',
                                                                 'pending_balance', 'withdrawal_pending', ])

                if friends.from_country_id:
                    myRequest_dict['country_filename'] = '%s/%s' % (CommonService.NATION_FILE_DIR,
                                                                     friends.from_country_id.flag_filename)
                else:
                    myRequest_dict['country_filename'] = '%s' % (CommonService.NATION_FILE_DIR)
                if friends.photo_filename:
                    myRequest_dict['photo_filename'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                                          friends.photo_filename)
                else:
                    myRequest_dict['photo_filename'] = '%s' %(CommonService.PROFILE_IMAGE_FILE_DIR)

                myRequest_dict['is_tutor'] = friends.user.is_tutor
                myRequest_dict['message'] = myRequest.message

                result.append(myRequest_dict)

        except Exception as e:
            print '@@@@@PeopleService getReceiveFriends : ', str(e)
        finally:
            return result

    # 내가 친구 요청 보낸 유저목록 가져오기
    @staticmethod
    def getRequestFriends(user_id):
        """
        @summary: 내가 친구 요청 보낸 유저목록
        @author: msjang
        @param user_id: 사용자 id 번호
        @return: result( list )
        """
        result = []
        try:
            requestFriendsList = Friend.objects.filter(req_user=user_id, status=1)\
                                                        .order_by("-created_time")

            for myRequest in requestFriendsList:
                myRequest_dict = model_to_dict(myRequest, exclude=['created_time'])

                if Tutor.objects.filter(user=myRequest_dict["res_user"]).exists():
                    friends = Tutor.objects.get(user=myRequest_dict["res_user"])
                    myRequest_dict = model_to_dict(friends,
                                exclude=['created_time', 'updated_time',
                                         'del_by_user', 'del_by_admin',
                                         'is_available', 'has_schedule',
                                         'has_course', 'schedule_comment',
                                         'gender', 'birthdate', 'how_to_meet',
                                         'skype_id', 'facetime_id', 'hangout_id',
                                         'qq_id', 'has_trial', 'trial_price',
                                         'total_balance', 'available_balance',
                                         'pending_balance', 'withdrawal_pending',
                                         'certificate1', 'certificate2',
                                         'certificate3', 'education1', 'education2',
                                         'education3', 'paymentinfo',
                                         'work1', 'work2', 'work3', 'audio_only' ])
                else:
                    friends = Tutee.objects.get(user=myRequest_dict["res_user"])
                    myRequest_dict = model_to_dict(friends,
                                                        exclude=['created_time', 'updated_time', 'del_by_user', 'del_by_admin',
                                                                 'gender', 'birthdate', 'how_to_meet', 'skype_id', 'facetime_id', 'hangout_id',
                                                                 'qq_id', 'has_trial', 'trial_price', 'total_balance', 'available_balance',
                                                                 'pending_balance', 'withdrawal_pending', ])

                if friends.from_country_id:
                    myRequest_dict['country_filename'] = '%s/%s' % (CommonService.NATION_FILE_DIR,
                                                            friends.from_country_id.flag_filename)
                else:
                    myRequest_dict['country_filename'] = '%s' % (CommonService.NATION_FILE_DIR)
                if friends.photo_filename:
                    myRequest_dict['photo_filename'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                                          friends.photo_filename)
                else:
                    myRequest_dict['photo_filename'] = '%s' %(CommonService.PROFILE_IMAGE_FILE_DIR)

                myRequest_dict['is_tutor'] = friends.user.is_tutor
                myRequest_dict['message'] = myRequest.message

                result.append(myRequest_dict)

        except Exception as e:
            print '@@@@@PeopleService getRequestFriends : ', str(e)
        finally:
            return result

    # 친구 요청 여부
    @staticmethod
    def isRequestFriend(request_id, response_id):
        """
        @summary: 친구요청 여부 확인
        @author: khyun
        @param request_id : request id
        @param response_id : response number
        @return: result state
        """
        result = 1
        try:
            if Friend.objects.filter( req_user = response_id, res_user = request_id, status = 1 ).count() > 0 :
                result = 2 # 이미 요청받은 친구
            else:
                result = Friend.objects.filter( req_user = request_id, res_user = response_id, status = 1 ).count()
        except Exception as e:
            result = 0
            print '@@@@@PeopleService isRequestFriend : ', str(e)
        finally:
            return result

    # 친구 요청 수락
    @staticmethod
    def acceptFriends(user_id, req_id, requesttype):
        """
        @summary: 친구 요청 수락
        @author: msjang
        @param id: 해당 친구요청 id
        @param requesttype: 수락 방법( 전체수락, 개별수락 )
        @return: 1( success ), 0( fail )
        """
        result = 1
        try:
            # 개별 친구 요청 수락
            if requesttype != 'all':
                friend = Friend.objects.get(res_user=user_id, req_user=req_id, status=1)
                friend.status = 2
                friend.res_time = datetime.now()
                friend.save()
            else:
                friendList = Friend.objects.filter(res_user=id, status=1).order_by("-created_time")
                for friend in friendList:
                    try:
                        friend.status = 2
                        friend.res_time = datetime.now()
                        friend.save()
                    except:
                        result = 0

        except Exception as e:
            result = 0
            print '@@@@@PeopleService acceptFriends : ', str(e)
        finally:
            return result

    # 친구 요청 거절
    @staticmethod
    def declineFriend(user_id, req_id, requesttype):
        """
        @summary: 친구 요청 거절
        @author: msjang
        @param fr_id: 친구요청 id
        @param requesttype: 거절 방법( 전체거절, 개별거절 )
        @return: 
        """
        result = 1
        try:
            # 개별 친구 요청 거절
            if requesttype != 'all':
                friend = Friend.objects.get(req_user=req_id, res_user=user_id, status=1)
                friend.status = 4
                friend.res_time = datetime.now()
                friend.save()
            else:
                friendList = Friend.objects.filter(res_user=user_id, status=1).order_by("-created_time")
                for friend in friendList:
                    try:
                        friend.status = 4
                        friend.res_time = datetime.now()
                        friend.save()
                    except:
                        result = 0

        except Exception as e:
            print '@@@@@PeopleService declineFriend : ', str(e)
            result = 0
        finally:
            return result

    # 내가 보낸 친구 요청 취소
    @staticmethod
    def cancelFriend(user_id, res_id):
        """
        @summary: 내가 보낸 친구요청 취소
        @author: msjang
        @param id: friend table res_user
        @return: 1( success ), 0( fail )
        """
        result = 1
        try:
                friend = Friend.objects.get(req_user=user_id, res_user=res_id, status=1)
                friend.status = 3
                friend.res_time = datetime.now()
                friend.save()
        except Exception as e:
            result = 0
            print '@@@@@PeopleService cancelFriend : ', str(e)
        finally:
            return result

    # 친구 삭제
    @staticmethod
    def deleteFriend(fr_id, user_id):
        """
        @summary: 친구삭제( 상대방 친구목록에선 보임 )
        @author: msjang
        @param fr_id: le_friend table pk
        @param user_id: 사용자 id 번호
        @return: 1( success ), 0( fail )
        """
        result = 1
        try:
            c = connection.cursor()
            sql = '''
                UPDATE le_friend
                SET req_del = ( SELECT CASE WHEN request_id = %s THEN 1 ELSE 0 END )
                  , res_del = ( SELECT CASE WHEN request_id = %s THEN 0 ELSE 1 END )
                WHERE fr_id = %s
            '''
            c.execute(sql, [user_id, user_id, fr_id])
        except Exception as e:
            result = 0
            print '@@@@@PeopleService deleteFriend : ', str(e)
        finally:
            c.close()
            return result

    # 유저숨김
    @staticmethod
    def hideUser(hide_id, user_id):
        """
        @summary: 유저 차단
        @author: msjang
        @param hide_id: 차단할 유저 id 번호
        @param user_id: 사용자 id 번호
        @return: 1( success ), 0( fail )
        """
        # tutor_hide 테이블에 데이터추가( get_or_create )
        result = 1
        try:
            friendHide, created = FriendHide.objects.get_or_create(hidden_user=hide_id, user=user_id)
        except Exception as e:
            result = 0
            print '@@@@@PeopleService hideUser : ', str(e)
        finally:
            return result

    # 친구 요청
    @staticmethod
    def addFriend(request_id, response_id, message):
        """
        @summary: add friend function
        @author: khyun
        @param request_id : request id
        @param response_id : response number
        @param message : request message
        @return: result state, object data
        """
        result = 1
        try:
            '''
            1 : 요청
            2 : 수락
            3 : 취소
            4 : 거절
            '''
#             if Friend.objects.filter(res_user_id=response_id, req_user_id=request_id).exists():
#                 friend = Friend.objects.get(res_user_id=response_id, req_user_id=request_id)
#             else:
            friend, created = Friend.objects.get_or_create(req_user_id=request_id,
                                                           res_user_id=response_id, message=message,
                                                           status=1)
        except Exception as e:
            result = 0
            print '@@@@@PeopleService addFriend : ', str(e)
        finally:
            return result, friend

    @staticmethod
    def getOnOffcheck(user_id, rs):
        """
        @summary: login 유무
        @author: msjang
        @param user_id: 사용자 id
        @param rs: 로그인 확인 할 친구 목록
        @return: 1( success ), 0( fail )
        """
        email_list = [user for user in TellpinUser.objects.filter(user_id__in=[obj['user'] for obj in rs] ).values('email', 'user_id')]
        online_list = CommonService.get_login_users()

        for item in email_list:
            if item['email'] in online_list:
                item['is_login'] = 1
            else:
                item['is_login'] = 0

        return email_list
