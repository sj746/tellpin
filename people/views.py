# -*- encoding: utf-8 -*-

###############################################################################
# filename    : people > views.py
# description : people 관련 views.py
# author      : msjang@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 whcho 최초 작성
#
#
###############################################################################

import json

from django.http.response import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt

from common.common import tellpin_login_required
from community.service import Community
from people.service import PeopleService


# from community.service import Community
@tellpin_login_required
def myTutor(request):
    """
    @summary: my tutor page
    @author: msjang
    @param request
    @return: url - people/my_tutor.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    resData['tutor'] = PeopleService.getTutorList(user_id)
    resData['onoff_check'] = PeopleService.getOnOffcheck(user_id, resData['tutor'])

    return render_to_response('people/my_tutor.html', {'data': json.dumps(resData)}, RequestContext(request))


@tellpin_login_required
def myFriend(request):
    """
    @summary: my friend page
    @author: msjang
    @param request
    @return: url - people/my_friend.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    resData['friends'] = PeopleService.getFriends(user_id)  # 내친구목록
    resData['onoff_friends'] = PeopleService.getOnOffcheck(user_id, resData['friends'])
    resData['requestFriends'] = PeopleService.getReceiveFriends(user_id)  # 요청받은목록
    resData['onoff_requestFriends'] = PeopleService.getOnOffcheck(user_id, resData['requestFriends'])

    return render_to_response('people/my_friend.html', {'data': json.dumps(resData)}, RequestContext(request))


@tellpin_login_required
def myTutee(request):
    """
    @summary: my tutee page
    @author: msjang
    @param request
    @return: url - people/my_tutee.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    resData['tutees'] = PeopleService.getTutees(user_id)
    resData['onoff_check'] = PeopleService.getOnOffcheck(user_id, resData['tutees'])

    return render_to_response('people/my_tutee.html', {'data': json.dumps(resData)}, RequestContext(request))


# 유저숨기기( My tutor 에서)
@csrf_exempt
@tellpin_login_required
def hideUser(request):
    """
    @summary: my tutor 에서 유저 숨기기
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    hide_id = post['hide_id']
    resData['isSuccess'] = PeopleService.hideUser(hide_id, user_id)

    if resData['isSuccess'] == 1:
        resData['tutor'] = PeopleService.getTutorList(user_id)
        resData['onoff_check'] = PeopleService.getOnOffcheck(user_id, resData['tutor'])

    return JsonResponse(resData)


# 내가 요청 보낸 친구목록 가져오기
@csrf_exempt
@tellpin_login_required
def getRequestFriends(request):
    """
    @summary: 내가 요청 보낸 친구목록 가져오기
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    resData['requestFriends'] = PeopleService.getRequestFriends(user_id)
    resData['onoff_requestFriends'] = PeopleService.getOnOffcheck(user_id, resData['requestFriends'])

    return JsonResponse(resData)


# 내가 요청 받은 친구목록 가져오기
@csrf_exempt
@tellpin_login_required
def getReceiveFriends(request):
    """
    @summary: 내가 요청 받은 친구목록 가져오기
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    resData['receiveFriends'] = PeopleService.getReceiveFriends(user_id)
    resData['onoff_receiveFriends'] = PeopleService.getOnOffcheck(user_id, resData['receiveFriends'])

    return JsonResponse(resData)


# 친구요청 수락하기
@csrf_exempt
@tellpin_login_required
def acceptFriends(request):
    """
    @summary: 친구 요청 수락하기
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    type = post['type']

    if type == 'all':
        id = user_id
    else:
        id = post['id']

    resData['isSuccess'] = PeopleService.acceptFriends(user_id, id, type)
    Community.set_notification_exchange_approve_friend(user_id, id, type)
    if resData['isSuccess'] == 1:
        resData['friends'] = PeopleService.getFriends(user_id)
        resData['onoff_friends'] = PeopleService.getOnOffcheck(user_id, resData['friends'])
        resData['requestFriends'] = PeopleService.getReceiveFriends(user_id)
        resData['onoff_requestFriends'] = PeopleService.getOnOffcheck(user_id, resData['requestFriends'])

    return JsonResponse(resData)


# 친구요청 거절하기
@csrf_exempt
@tellpin_login_required
def declineFriend(request):
    """
    @summary: 친구 요청 거절하기
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    type = post['type']

    if type == 'all':
        id = user_id
    else:
        id = post['fr_id']

    resData['isSuccess'] = PeopleService.declineFriend(user_id, id, type)

    if resData['isSuccess'] == 1:
        resData['requestFriends'] = PeopleService.getReceiveFriends(user_id)
        resData['onoff_requestFriends'] = PeopleService.getOnOffcheck(user_id, resData['requestFriends'])

    return JsonResponse(resData)


# 친구삭제
@csrf_exempt
@tellpin_login_required
def deleteFriend(request):
    """
    @summary: 친구 삭제
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    fr_id = post['fr_id']
    resData['isSuccess'] = PeopleService.deleteFriend(fr_id, user_id)

    if resData['isSuccess'] == 1:
        resData['friends'] = PeopleService.getFriends(user_id)
        resData['onoff_friends'] = PeopleService.getOnOffcheck(user_id, resData['friends'])

    return JsonResponse(resData)


# 내가 보낸 친구 요청 취소
@csrf_exempt
def cancelFriend(request):
    """
    @summary: 내가 보낸 친구 요청 취소
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    fr_id = post['fr_id']
    resData['isSuccess'] = PeopleService.cancelFriend(user_id, fr_id)

    if resData['isSuccess'] == 1:
        resData['requestFriends'] = PeopleService.getRequestFriends(user_id)
        resData['onoff_requestFriends'] = PeopleService.getOnOffcheck(user_id, resData['requestFriends'])

    return JsonResponse(resData)
