"""
WSGI py for tellpin project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os, sys
sys.path.append('/opt/djangostack-1.8.3-0/apps/django/django_projects/tellpin')
os.environ.setdefault("PYTHON_EGG_CACHE", "/opt/djangostack-1.8.3-0/apps/django/django_projects/tellpin/egg_cache")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tellpin.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
