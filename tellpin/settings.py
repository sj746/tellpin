"""
Django settings for tellpin project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
from ConfigParser import RawConfigParser
import os

from django.conf.global_settings import SESSION_EXPIRE_AT_BROWSER_CLOSE, \
    EMAIL_BACKEND

from auth_config import *
from django.utils.translation import ugettext_lazy as _


BASE_DIR = os.path.dirname(os.path.dirname(__file__)).replace("\\", "/")

config = RawConfigParser()
config.read(BASE_DIR+'/tellpin/settings.ini')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '54yjf!5azl%#)m0p7go#m3pk2px+pacs8_7ro0#d8vx59xpjfx'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['www.tellpin.com', 'tellpin.com']
#ALLOWED_HOSTS = ['localhost', '127.0.0.1']
ROOT_URLCONF = 'tellpin.urls'
# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'social.apps.django_app.default',
    'django.contrib.humanize',
    'wallet',
    'common',
    'community',
    'dashboard',
    'exchange',
    'mycontent',
    'mypage',
    'people',
    'settings',
    'support',
    'django_extensions',
    'tools',
    'tutor',
    'tutoring',
    'user_auth',
    'tellpin_admin',
    'ws4redis',
    'chatserver',
#     'main',
#    'writingform',
#     'debug_toolbar',
    )

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'social.apps.django_app.middleware.SocialAuthExceptionMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
#     'debug_toolbar.middleware.DebugToolbarMiddleware',
)


#SESSION_COOKIE_AGE = 8*60*60
SESSION_SAVE_EVERY_REQUEST = True
SESSION_EXPIRE_AT_BROWSER_CLOSE = True

#WEBSOCKET_URL = '/ws/'

#WS4REDIS_EXPIRE = 1
#WS4REDIS_PREFIX = 'ws'
#WS4REDIS_SUBSCRIBER = 'myapp.redis_store.RedisSubscriber'
WSGI_APPLICATION = 'tellpin.wsgi.application'
#WSGI_APPLICATION = 'ws4redis.django_runserver.application'

#SESSION_ENGINE = 'redis_sessions.session'
#SESSION_REDIS_PREFIX = 'session'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR,"templates")],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social.apps.django_app.context_processors.backends',
                'social.apps.django_app.context_processors.login_redirect',
                # and add request if you didn't do so already
                'django.core.context_processors.request',
                'django.template.context_processors.i18n',
                # websocket
                'django.contrib.auth.context_processors.auth',
                'django.core.context_processors.static',
                'django.core.context_processors.request',
                'ws4redis.context_processors.default',
            ],
        },
    },
]

AUTH_USER_MODEL = 'user_auth.TellpinAuthUser'

AUTHENTICATION_BACKENDS = (
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.google.GoogleOAuth2',
    'social.backends.twitter.TwitterOAuth',
    'social.backends.email.EmailAuth',
    'django.contrib.auth.backends.ModelBackend',
)

DATABASES = {
    'default': {
        'ENGINE': config.get('DATABASE', 'DATABASE_ENGINE'),
        'NAME': config.get('DATABASE', 'DATABASE_NAME'),
        'HOST': config.get('DATABASE', 'DATABASE_HOST'),
        'PORT': config.get('DATABASE', 'DATABASE_PORT'),
        'USER': config.get('DATABASE', 'DATABASE_USER'),
        'PASSWORD': config.get('DATABASE', 'DATABASE_PASSWORD'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/
# https://msdn.microsoft.com/en-us/library/ms533052(v=vs.85).aspx

LANGUAGES = (
    ('en-us', _('English (United States)')),
    ('ko', _('Korean')),
    ('zh-cn', _('Chinese (PRC)')),
    ('es', _('Spanish (Spain)')),
    ('ja', _('Japanese')),
)

LANGUAGE_CODE = 'en-us'
# SOLID_I18N_USE_REDIRECTS = False
LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


#TELLPIN_HOME = 'http://localhost:8000/'
TELLPIN_HOME = 'http://www.tellpin.com/'
#TELLPIN_HOME = 'http://unichal.dlinkddns.com:8000/'
INTEREST_IMG_URL = 'http://www.tellpin.com/static/img/mypage/interest/'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
STATIC_ROOT = os.path.join(BASE_DIR,'static')
STATIC_URL = '/static/'
STATICFILES_DIRS =(
    ('css', os.path.join(STATIC_ROOT,'css')),
    ('img', os.path.join(STATIC_ROOT,'img')),
    ('js', os.path.join(STATIC_ROOT,'js')),
    ('html', os.path.join(STATIC_ROOT,'html')),
    ('font', os.path.join(STATIC_ROOT,'font')),
)

DEFAULT_FROM_EMAIL = 'tellpinadmin@unichal.com'

EMAIL_HOST = 'mail.unichal.com'
EMAIL_PORT = 25
EMAIL_HOST_USER = 'tellpinadmin@unichal.com'
EMAIL_HOST_PASSWORD = 'yk7582'
#EMAIL_USE_TLS = True
#EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_BACKEND = 'user_auth.smtp.EmailBackend'


SERVER_EMAIL = 'tellpinadmin@unichal.com'

ADMINS = (
    ('tellpinadmin', 'tellpinadmin@unichal.com'),
)


LOGGING = {
           'version': 1,
           'disable_existing_loggers': False,
           'filters': {
                       'require_debug_false': {
                                               '()': 'django.utils.log.RequireDebugFalse'
                                               },
                       'require_debug_true': {
                                              '()': 'django.utils.log.RequireDebugTrue'
                                              }
                       },
           'formatters': {
                          'main_formatter': {
                                             'format': '[(%(asctime)s ) %(levelname)s: %(pathname)s %(filename)s %(lineno)d] %(message)s ',
                                             'datefmt': "%Y-%m-%d %H:%M:%S",
                                             },
                          },
           'handlers': {
                        'debug_file': {
                                 'level': 'DEBUG',
                                 #'class': 'logging.handlers.TimedRotatingFileHandler',
                                 'class': 'tellpin.ParallelTimedRotatingFileHandler.ParallelTimedRotatingFileHandler',
                                 'filename': os.path.dirname(os.path.dirname(__file__)) + '/logs/debug',
                                 'formatter': 'main_formatter',
                                 'when': 'midnight',
                                 'interval' : 1,
                                 'backupCount' : 0,
                                 'filters': ['require_debug_true'],
                                 },
                        'console':{
                                   'level': 'DEBUG',
                                   'filters': ['require_debug_false'],
                                   'class': 'logging.StreamHandler',
                                   'formatter': 'main_formatter',
                                   },
                        'mail_admins': {
                                        'level': 'ERROR',
                                        'class': 'django.utils.log.AdminEmailHandler',
                                        'include_html': True
                                        }
                        },
           'loggers': {
                       'django': {
                                  #'handlers': ['debug_file', 'mail_admins',],
                                  'handlers': ['debug_file', 'console'],
                                  'propagate': False,
                                  'level': 'DEBUG',
                                  },
                       'mylogger': {
                                    'handlers': ['debug_file', 'console'],
                                    'level': 'DEBUG',
                                    'propagate': False,
                                    },
                       'main': {
                                    'handlers': ['debug_file', 'console'],
                                    'level': 'DEBUG',
                                    'propagate': False,
                                    },
                       }
           }


PAYPAL_SELLER = config.get('PAYPAL', 'PAYPAL_SELLER')
