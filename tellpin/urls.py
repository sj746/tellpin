from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.debug import default_urlconf
from django.views.generic import RedirectView
from tellpin.settings import TELLPIN_HOME

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'tellpin.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^favicon\.ico$', RedirectView.as_view(url=TELLPIN_HOME + 'static/img/tellpin_com_favicon.png')),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^$', include('user_auth.urls')),
    url(r'^user_auth/', include('user_auth.urls')),
    url(r'^tools/', include('tools.urls')),
    url(r'^tellpinsearch/', include('tools.urls')),
    url(r'^', include('community.urls')),
    url(r'^tutor/', include('tutor.urls')),
    url(r'^', include('tutor.urls')),
    url(r'^tutoring/', include('tutoring.urls')),
    url(r'^tutorlessons/', include('tutoring.urls')),
    url(r'^tuteelessons/', include('tutoring.urls')),
    url(r'^', include('tutoring.urls')),
    url(r'^wallet/', include('wallet.urls')),
    url(r'^', include('exchange.urls')),
    url(r'^dashboard/', include('dashboard.urls')),
    url(r'^settings/', include('settings.urls')),
    url(r'^people/', include('people.urls')),
    url(r'^sample/', include('sample.urls')),
    url(r'^mycontent/', include('mycontent.urls')),
    url(r'^help/', include('support.urls')),
    url(r'^admin/', include('tellpin_admin.urls')),
#     url(r'^main/', include('main.urls')),
#     url(r'^imgoptim/', include('imgoptim.urls')),
    url(r'^mypage/', include('mypage.urls')),
    url(r'^common/', include('common.urls')),
    url(r'^chatserver/', include('chatserver.urls')),
)

'''
if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
'''