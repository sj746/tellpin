#-*- coding: utf-8 -*-

###############################################################################
# filename    : tutoring > NewLessonService.py
# description : 새로운 강의 등록 시 필요한 함수들을 구성하는 파일
# author      : kihyun@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 j746 최초 작성
#
#
###############################################################################

# from common.models import SkillLevel
# from common.models import Lesson, Reservation
# from django.core import serializers
# from common.models import Language, TellpinUser
from django.forms.models import model_to_dict
from django.db import connection
from common import common


class NewLesson(object):
    # get Language skill level
    def getSkillLevel(self):
        """
        @summary: 언어의 레벨을 구하는 함수 (1: beginner, 2 : elementary, 3: pre-intermediate, 4: intermediate, 5: upper-intermediate, 6: advanced, 7: native)
        @author: sj746
        @return: skillLevelList
        """

        skillLevelList = []
        try :
            skillList = SkillLevel.objects.all().exclude( level_id = 7 )
            for skill in skillList :
                obj = {}
                obj["level_id"] = skill.level_id
                obj["level_name"] = skill.level_name
                skillLevelList.append(obj)
        except Exception as e :
            obj = {}
            obj["message"] = str(e)
            skillLevelList.append(obj)
        finally :
            return skillLevelList

    # get speak list
    def getSpeakList(self, userId):
        """
        @summary: 등록된 사용자가 가르치는 언어를 구하는 함수
        @author: sj746
        @param param1 : userId :사용자 id
        @return: skillLevelList
        """

        speakResult = []
        try:
            # get tid
            # userTid = Tutor.objects.get( user_id = userId ).tid
            userTid = userId
            # get speak table language
            tutorModel = TellpinUser.objects.get( id = userTid )
            languageList = []
            if tutorModel.teaching1 :
                languageList.append( tutorModel.teaching1 )
            if tutorModel.teaching2 :
                languageList.append( tutorModel.teaching2 )
            if tutorModel.teaching3 :
                languageList.append( tutorModel.teaching3 )
            for languageId in languageList :
                obj = {}
                language = Language.objects.get( id = languageId ).language1
                obj["id"] = languageId
                obj["language"] = language
                speakResult.append( obj )
        except Exception as e :
            obj = {}
            obj["message"] = str( e )
            speakResult.append( obj )
        finally:
            print speakResult
            return speakResult
    # save lesson
    def saveLesson(self, lesson, userId, type ):
        """
        @summary: 강의를 등록하는 함수
        @author: sj746
        @param param1 : userId :사용자 id
        @param param2 : type :강의 type (trial, else)
        @return: result(1:성공, 0 : 실패)
        """

        result = 0
        if lesson :
            try :
                # print lesson
                # get tid
                # userTid = Tutor.objects.get( user_id = userId ).tid
                userTid = userId
                if type == "trial" :
                    lessonModel, created = Lesson.objects.get_or_create( user_id = userTid, type = 0 )
                    
                    if created == False :
                        if lessonModel.del_yn == 0 :
                            print "trialdelete"
                            lessonModel.del_yn = 1
                        else :
                            print "addtrial"
                            lessonModel.del_yn = 0
                    lessonModel.language = TellpinUser.objects.get( id = userId ).native1
                else :
                    # 최대강의 6개
                    if Lesson.objects.filter( user_id = userId, type = 1, del_yn = 0 ).count() >= 6 :
                        result = 2
                        return result
                    if lesson["lid"] != 0 :
                        print lesson["lid"]
                        lessonModel = Lesson.objects.get( lid = lesson["lid"] )                        
                    else :
                        lessonModel = Lesson()
                    lessonModel.language = lesson["language"]
                    lessonModel.del_yn = 0
                print 'del_yn'
                print lessonModel.del_yn
                lessonModel.user_id = userTid
                lessonModel.title = lesson["title"]
                lessonModel.description = lesson["description"]
                lessonModel.type = lesson["type"]
                lessonModel.from_level = lesson["from_level"]
                lessonModel.to_level = lesson["to_level"]
                lessonModel.display_color = lesson["display_color"]
                lessonModel.has_online = lesson["has_online"]
                lessonModel.has_offline = lesson["has_offline"]
                lessonModel.online_30m = lesson["online_30m"]
                lessonModel.online_60m = lesson["online_60m"]
                lessonModel.offline_60m = lesson["offline_60m"]
                lessonModel.offline_90m = lesson["offline_90m"]
                lessonModel.offline_120m = lesson["offline_120m"]
                lessonModel.online_1times = lesson["online_1times"]
                lessonModel.online_5times = lesson["online_5times"]
                lessonModel.online_10times = lesson["online_10times"]
                lessonModel.offline_1times = lesson["offline_1times"]
                lessonModel.offline_5times = lesson["offline_5times"]
                lessonModel.offline_10times = lesson["offline_10times"]
                lessonModel.online_30m_1times_cost = lesson["online_30m_1times_cost"]
                lessonModel.online_30m_5times_cost = lesson["online_30m_5times_cost"] * 5
                lessonModel.online_30m_10times_cost = lesson["online_30m_10times_cost"] * 10
                lessonModel.online_60m_1times_cost = lesson["online_60m_1times_cost"]
                lessonModel.online_60m_5times_cost = lesson["online_60m_5times_cost"] * 5
                lessonModel.online_60m_10times_cost = lesson["online_60m_10times_cost"] * 10
                lessonModel.offline_60m_1times_cost = lesson["offline_60m_1times_cost"]
                lessonModel.offline_60m_5times_cost = lesson["offline_60m_5times_cost"] * 5
                lessonModel.offline_60m_10times_cost = lesson["offline_60m_10times_cost"] * 10
                lessonModel.offline_90m_1times_cost = lesson["offline_90m_1times_cost"]
                lessonModel.offline_90m_5times_cost = lesson["offline_90m_5times_cost"] * 5
                lessonModel.offline_90m_10times_cost = lesson["offline_90m_10times_cost"] * 10
                lessonModel.offline_120m_1times_cost = lesson["offline_120m_1times_cost"]
                lessonModel.offline_120m_5times_cost = lesson["offline_120m_5times_cost"] * 5
                lessonModel.offline_120m_10times_cost = lesson["offline_120m_10times_cost"] * 10
                lessonModel.save()
                result = 1
            except Exception as e:
                print str( e )
            finally :
                return result
        else :
            return result
        
    # save lesson
    def changeLesson(self, lesson, userId, type ):
        """
        @summary: 등롣한 강의를 편집하는 함수
        @author: sj746
        @param lesson : lesson :등록된 강의 정보
        @param param2 : userId :사용자 id
        @param param3 : type :강의 type (trial, else)
        @return: result(1:성공, 0 : 실패)
        """

        result = 0;
        if lesson :
            try :
                #print lesson
                # get tid
                #userTid = Tutor.objects.get( user_id = userId ).tid
                userTid = userId
                if type == "trial" :
                    lessonModel, created = Lesson.objects.get_or_create( user_id = userTid, type = 0 )
                    lessonModel.language = TellpinUser.objects.get( id = userId ).native1
                if lessonModel.del_yn == 1 :
                    lessonModel.del_yn = 0
                print lessonModel.del_yn
                lessonModel.user_id = userTid
                lessonModel.title = lesson["title"]
                lessonModel.description = lesson["description"]
                lessonModel.type = lesson["type"]
                lessonModel.from_level = lesson["from_level"]
                lessonModel.to_level = lesson["to_level"]
                lessonModel.display_color = lesson["display_color"]
                lessonModel.has_online = lesson["has_online"]
                lessonModel.has_offline = lesson["has_offline"]
                lessonModel.online_30m = lesson["online_30m"]
                lessonModel.online_60m = lesson["online_60m"]
                lessonModel.offline_60m = lesson["offline_60m"]
                lessonModel.offline_90m = lesson["offline_90m"]
                lessonModel.offline_120m = lesson["offline_120m"]
                lessonModel.online_1times = lesson["online_1times"]
                lessonModel.online_5times = lesson["online_5times"]
                lessonModel.online_10times = lesson["online_10times"]
                lessonModel.offline_1times = lesson["offline_1times"]
                lessonModel.offline_5times = lesson["offline_5times"]
                lessonModel.offline_10times = lesson["offline_10times"]
                lessonModel.online_30m_1times_cost = lesson["online_30m_1times_cost"];
                lessonModel.online_30m_5times_cost = lesson["online_30m_5times_cost"]
                lessonModel.online_30m_10times_cost = lesson["online_30m_10times_cost"]
                lessonModel.online_60m_1times_cost = lesson["online_60m_1times_cost"]
                lessonModel.online_60m_5times_cost = lesson["online_60m_5times_cost"]
                lessonModel.online_60m_10times_cost = lesson["online_60m_10times_cost"]
                lessonModel.offline_60m_1times_cost = lesson["offline_60m_1times_cost"]
                lessonModel.offline_60m_5times_cost = lesson["offline_60m_5times_cost"]
                lessonModel.offline_60m_10times_cost = lesson["offline_60m_10times_cost"]
                lessonModel.offline_90m_1times_cost = lesson["offline_90m_1times_cost"]
                lessonModel.offline_90m_5times_cost = lesson["offline_90m_5times_cost"]
                lessonModel.offline_90m_10times_cost = lesson["offline_90m_10times_cost"]
                lessonModel.offline_120m_1times_cost = lesson["offline_120m_1times_cost"]
                lessonModel.offline_120m_5times_cost = lesson["offline_120m_5times_cost"]
                lessonModel.offline_120m_10times_cost = lesson["offline_120m_10times_cost"]
                lessonModel.save()
                result = 1
            except Exception as e :
                print str( e )
            finally :
                return result
        else :
            return result

    # isTrial check
    def isTrial(self, o_tid ):
        """
        @summary: trial 강의 정보를 구하는 함수(강의타입(0:trial, 1:정식강의)
        @author: sj746
        @param param1 : o_tid :사용자 id
        @return: trialResult : trial 강의 정보 리스트
        """

        trialResult = {}
        try :
            # get tid
            trial = Lesson.objects.filter( user_id = o_tid, type = 0 ).count()
            print trial
            trialResult["isTrial"] = trial
            if ( trial != 0 ) :
                trial = Lesson.objects.get( user_id = o_tid, type = 0 )
                if trial.del_yn == 0 :
                    trialResult["trialCost"] = trial.online_30m_1times_cost
                else :
                    trialResult["isTrial"] = 0
                    trialResult["trialCost"] = trial.online_30m_1times_cost
            else :
                trialResult["trialCost"] = 0
            trialResult["trialCost_ex"] = int(trialResult["trialCost"]) * 100
        except Exception as e :
            trialResult["message"] = str( e )
        finally :
            return trialResult
    
    # get lesson
    def getLesson(self, o_lid ):
        """
        @summary: 등록된 강의의 정보를 가져오는 함수
        @author: sj746
        @param param1 : o_tid :강의 id
        @return: lessonObj : 강의 정보
        """

        lessonObj = {}
        try :
            cursor = connection.cursor()
            sql = '''
                SELECT lid, user_id, title, ul.language1, description, l.language
                     , from_level, has_offline
                     , to_level
                     , ( SELECT CONCAT( ( CASE WHEN c.type = 1 THEN '/static/img/course_bgimg/Business/'
                                          WHEN c.type = 2 THEN '/static/img/course_bgimg/National flags/'
                                          WHEN c.type = 3 THEN '/static/img/course_bgimg/People/'
                                          WHEN c.type = 4 THEN '/static/img/course_bgimg/Scenery/'
                                          ELSE '/static/img/course_bgimg/Themes/' END ), filepath )
                         FROM course_bg_info c
                         WHERE id = l.display_color ) AS display_color
                     , has_online, online_30m, online_60m, online_1times, online_5times, online_10times
                     , online_30m_1times_cost, online_30m_5times_cost, online_30m_10times_cost
                     , online_60m_1times_cost, online_60m_5times_cost, online_60m_10times_cost
                FROM lesson l, user_auth_language ul
                WHERE l.language = ul.id
                  AND l.lid = %s
            '''
            cursor.execute( sql, [ o_lid ] )
            lessonObj = common.dictfetchall( cursor )[0]
            
            '''
            lessonModel = Lesson.objects.get( lid = o_lid )
            lessonDict = model_to_dict( lessonModel )
            for field in lessonModel._meta.get_all_field_names() :
                lessonObj[field] = lessonDict[field]
            '''
        except Exception as e :
            lessonObj["message"] = str( e )
        finally :
            cursor.close()
            return lessonObj
        
    # delete lesson
    def deleteLesson(self, o_lid ):
        """
        @summary: 등록된 강의를 삭제하는 함수
        @author: sj746
        @param param1 : o_tid :강의 id
        @return: result : 강의 삭제 여부 (1:강의 삭제, 0:강의 삭제 X)
        """

        result = 0
        try :
            resCount = Reservation.objects.filter( lid = o_lid ).count()
            if ( resCount == 0 ) :
                lesson = Lesson.objects.get( lid = o_lid )
                lesson.del_yn = 1
                lesson.save()
                result = 1
            else :
                result = 0
        except Exception as e :
            print str( e )
            result = 0
        finally :
            print result
            return result
    
    # get Course Image
    def getCourseImg(self, type = 1):
        """
        @summary: 강의 등록 시 배경이미지를 설정하는데 해당 이미지를 가져오는 함수
        @author: sj746
        @param param1 : type : 강의 배경이미지 타입 (1: Business, 2: National flags, 3: People, 4: Scenery, else: Themes)
        @return: result : 강의 배경 이미지
        """

        result = []
        try:
            cursor = connection.cursor()
            fd = ''
            if type == 1:
                fd = 'Business'
            elif type == 2:
                fd = 'National flags'
            elif type == 3:
                fd = 'People'
            elif type == 4:
                fd = 'Scenery'
            else:
                fd = 'Themes'
            src = '/static/img/course_bgimg/%s/' % fd # img src
            sql = '''
                SELECT id, CONCAT( %s, filepath ) AS src
                FROM course_bg_info
                WHERE TYPE = %s
            '''
            cursor.execute( sql, [ src, type ] )
            result = common.dictfetchall( cursor )
        except Exception as e:
            print str( e )
        finally:
            cursor.close()
            return result
