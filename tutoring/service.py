# -*- coding: utf-8 -*-
###############################################################################
# filename    : tutoring > service.py
# description : 새로운 강의 등록 시 필요한 함수들을 구성하는 파일
# author      : kihyun@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 j746 최초 작성
#
#
###############################################################################

"""
Created on 2016. 1. 22.

@author: unichal
"""
import calendar
import json
from datetime import datetime, timedelta, date
from time import strftime

from dateutil.relativedelta import relativedelta
from django.core.paginator import Paginator
from django.db import connection, transaction
from django.db.models.aggregates import Count
from django.db.models.query_utils import Q
from django.forms.models import model_to_dict
from django.shortcuts import get_object_or_404
from django.utils import timezone

from common import common
from common.models import TimezoneInfo
from common.service import CommonService
from dashboard.models import Notification
from models import Lesson
from tutor.models import Tutor, Tutee
from tutor.service import TutorService
from tutoring.models import TutorCourse, TutorCourseBgImg, TutorScheduleWeek, TutorScheduleCalendar, LessonReservation, \
    LessonMessage, LessonFeedback, LessonHistory, LessonClaim, LessonHistoryText, \
    LessonReservationStatusText, LessonReservationTemp, LESSON_STATUS
from user_auth.models import TellpinUser
from wallet.models import TutorWalletHistory, TuteeWalletHistory
from wallet.service import WalletService


SUFFIXES = {1: 'st', 2: 'nd', 3: 'rd'}
# class TutoringService(object):
#     '''
#     classdocs
#     '''
# 
#     def __init__(self, params):
#         '''
#         Constructor
#         '''
# 
#     def get_tutor_week_schedule(self, _tid, _utc_time):
#         """
#         @summary: 튜터의 주간 레슨 스케쥴을 구하는 함수
#         @author: sj746
#         @param param1 : _tid : 사용자 id
#         @param param2 : _utc_time : 사용자의 현지 시간
#         @return: 주간 스케쥴
#         """
# 
#         schedule_list = WeekSchedule.objects.filter(user_id=_tid).order_by("day_of_week")  # (1: monday, 7 : sunday)
#         list = []
#         for schedule in schedule_list:
#             scd = model_to_dict(schedule, exclude="date")
#             list.append(scd)
#         client_list = common.applyUserUTCTime(list, _utc_time)
#         return client_list
# 
#     def delete_range_schedule_for_mobile(self, _tid, _from_date, _to_date):
#         """
#         @summary: 사용자의 특정 시간의 일정을 지우는 함수
#         @author: sj746
#         @param param1 : _tid : 사용자 id
#         @param param2 : _from_time : 주간 스케쥴 시작 시간
#         @param param3 : _to_time : 주간 스케줄 종료 시간
#         @return: 월간 일정 리스트
#         """
# 
#         from_date = datetime.strptime(_from_date, "%Y%m%d").date()
#         to_date = datetime.strptime(_to_date, "%Y%m%d").date()
#         for single_date in self.daterange(from_date, to_date):
#             Schedule.objects.filter(user_id=_tid, delta_time__contains=single_date.strftime("%Y%m%d")).delete();
#             day_of_week = single_date.weekday() + 1
#             week_schedule = WeekSchedule.objects.get(user_id=_tid, day_of_week=day_of_week)
#             for idx in range(48):
#                 index_to_time = self.set_index_to_time(idx)
#                 if getattr(week_schedule, index_to_time) == 1:
#                     delta_string = single_date.strftime("%Y%m%d") + self.set_index_to_alphabet(
#                         day_of_week) + index_to_time[4:8]
#                     Schedule(user_id=_tid,
#                              delta_time=delta_string,
#                              type=2).save()
# 
#     def daterange(self, start_date, end_date):
#         """
#         @summary: 입력 날짜의 차이를 계산하는 함수
#         @author: sj746
#         @param param1 : start_date : 시작 날짜
#         @param param2 : end_date : 종료 날짜
#         @return: none
#         """
# 
#         for n in range(int((end_date - start_date).days) + 1):
#             yield start_date + timedelta(n)
# 
#     def set_time_list(self, from_time, to_time):
#         """
#         @summary: 입력 날짜의 차이로 시간을 스트링으로 변환
#         @author: sj746
#         @param param1 : from_time : 시작 날짜
#         @param param2 : to_time : 종료 날짜
#         @return: 변환된 시간 스트링 리스트
#         """
# 
#         list = []
#         for idx in range(from_time, to_time):
#             time_string = self.set_index_to_time(idx)
#             list.append(time_string)
#         return list
# 
#     def set_index_to_time(self, index):
#         """
#         @summary: 입력된 시간을 단위로 나누는 함수(00, 30분 단위 등등)
#         @author: sj746
#         @param param1 : index : 시간 인덱스
#         @param param2 : to_time : 종료 날짜
#         @return: 시간 문자열
#         """
# 
#         time_string = "time"
#         if index % 2 == 0:
#             if index / 2 < 10:
#                 time_string += "0" + str(index / 2) + "00"
#             else:
#                 time_string += str(index / 2) + "00"
#         else:
#             if (index - 1) / 2 < 10:
#                 time_string += "0" + str((index - 1) / 2) + "30"
#             else:
#                 time_string += str((index - 1) / 2) + "30";
#         return time_string
# 
#     def set_alphabet_to_index(self, alphabet):
#         """
#         @summary: 입력된 알파벳의 인덱스 를 리턴하는 함수
#         @author: sj746
#         @param param1 : alphabet : 알파벳 입력
#         @return: 입력된 알파벳의 인덱스
#         """
# 
#         alphabet_list = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
#         return alphabet_list.index(alphabet) + 1
# 
#     def set_index_to_alphabet(self, index):
#         """
#         @summary: 입력된 인덱스로 알파벳을 구하는 함수
#         @author: sj746
#         @param param1 : index : 알고자하는 알파벳의 인덱스
#         @return: 입력된 인덱스의 알파벳
#         """
# 
#         alphabet_list = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
#         return alphabet_list[index - 1]


class CourseService(object):
    COURSE_IMG_PATH = '/static/img/course_bgimg/'

    def __init__(self):
        pass

    @staticmethod
    def course_bgimg_path(image_type):
        """
        @summary: 해당 이미지 타입에 따른 폴더 경로
        @param image_type:
        @return: 해당 이미지 파일 폴더 경로
        """
        dir_path = CourseService.COURSE_IMG_PATH
        if image_type == 1:
            dir_path += 'Business/'
        elif image_type == 2:
            dir_path += 'National flags/'
        elif image_type == 3:
            dir_path += 'People/'
        elif image_type == 4:
            dir_path += 'Scenery/'
        else:
            dir_path += 'Themes/'

        return dir_path

    @staticmethod
    def course_bgimg(image_type=1):
        """
        @summary: 튜터 강의 등록 시 강의 배경 이미지 정보
        @param image_type:
        @return: list
        """
        result = list()
        try:
            dir_path = CourseService.course_bgimg_path(image_type)
            queryset = TutorCourseBgImg.objects.filter(type=image_type).values('id', 'type', 'filepath')
            result = list(queryset)
            for item in result:
                item['filepath'] = '%s%s' % (dir_path, item['filepath'])
        except Exception as err:
            print str(err)
            result = None
        finally:
            return result

    @staticmethod
    def course_count(tutor_id):
        """
        @summary: 튜터가 등록한 강의 수
        @param tutor_id: 튜터 id
        @return: 강의 수
        """
        result = None
        try:
            result = TutorCourse.objects.filter(tutor_id=tutor_id,
                                                del_by_admin=0,
                                                del_by_user=0).count()
        except Exception as err:
            print str(err)
            result = None
        finally:
            return result

    @staticmethod
    def create_trial(tutor_id, has_trial, trial_price):
        """
        @summary: 튜터 시범 강의 등록
        @param tutor_id: 튜터 id
        @param has_trial: 시범강의 등록인지 변경인지
        @param trial_price: 시범강의 정보
        @return: True, False
        """
        result = {
            'isSuccess': True,
            'message': 'Success'
        }
        try:
            tutor_obj = Tutor.objects.get(user_id=tutor_id)
            tutor_obj.has_trial = has_trial
            tutor_obj.trial_price = trial_price
            tutor_obj.save()
        except Exception as err:
            print str(err)
            result['message'] = str(err)
            result['isSuccess'] = False
        finally:
            return result

    @staticmethod
    def get_trial(tutor_id):
        """
        @summary: 튜터 trial course 정보
        @param tutor_id: 튜터 id
        @return: trial course
        """
        result = {
            'isSuccess': True,
            'message': 'Success'
        }
        try:
            tutor_obj = Tutor.objects.get(user_id=tutor_id)
            result['has_trial'] = tutor_obj.has_trial
            result['trial_price'] = tutor_obj.trial_price
        except Exception as err:
            print str(err)
            result['isSuccess'] = False
            result['message'] = str(err)
        finally:
            return result

    @staticmethod
    @transaction.atomic
    def create_course(tutor_id, course):
        """
        @summary: 튜터 강의 등록
        @author: msjang
        @param tutor_id: tutor id
        @param course: course 정보
        @return True, False
        """
        result = {
            'isSuccess': True,
            'message': 'Success'
        }
        try:
            if CourseService.course_count(tutor_id) < 7:
                if 'id' in course:
                    tutor_course = TutorCourse.objects.get(id=course['id'])
                else:
                    tutor_course = TutorCourse()
                foreign_keys = [
                    'course_lang', 'course_level_from', 'course_level_to',
                    'course_bgimg', 'tutor'
                ]
                tutor_course_keys = tutor_course._meta.get_all_field_names()
                for key, value in course.items():
                    if key in tutor_course_keys:
                        if key in foreign_keys:
                            key += '_id'
                        setattr(tutor_course, key, value)
                tutor_course.tutor_id = tutor_id
                tutor_course.save()
                if not tutor_course.tutor.has_course:
                    tutor_course.tutor.has_course = True
                    tutor_course.tutor.save()
            else:
                result['isSuccess'] = 2
                result['message'] = '강의개수초과'
        except Exception as err:
            result['isSuccess'] = False
            result['message'] = str(err)
        finally:
            return result

    @staticmethod
    def edit_course(course_id, tutor_id, course):
        """
        @summary: 튜터 강의 수정
        @param course_id: course id
        @param tutor_id: tutor id
        @param course: course 정보
        @return: True, False
        """
        result = {
            'isSuccess': True,
            'message': 'Success'
        }
        try:
            tutor_course = TutorCourse.objects.select_related(
                'tutor_id'
            ).get(id=course_id)
            course_keys = tutor_course._meta.get_all_field_names()
            foreign_keys = [
                'course_lang', 'course_level_from', 'course_level_to',
                'course_bgimg'
            ]
            if tutor_id == tutor_course.tutor_id:
                for key, value in course.dict():
                    if key in course_keys:
                        if key in foreign_keys:
                            key += '_id'
                        setattr(tutor_course, key, value)
                tutor_course.save()
            else:
                result['isSuccess'] = False
                result['message'] = '유저 불일치'
        except Exception as err:
            result['isSuccess'] = False
            result['message'] = str(err)
        finally:
            return result

    @staticmethod
    def course_list(tutor_id):
        """
        @summary: 튜터 강의목록
        @param tutor_id: 튜터 id
        @return: list
        """
        course_list = list()
        try:
            queryset = TutorCourse.objects.select_related(
                'course_lang', 'course_level_from', 'course_level_to',
                'course_bgimg'
            ).filter(tutor_id=tutor_id,
                     del_by_user=0,
                     del_by_admin=0).defer('created_time')
            for course in queryset:
                course_dict = model_to_dict(course)
                course_dict['filepath'] = '%s%s' % (CourseService.course_bgimg_path(course.course_bgimg.type),
                                                    course.course_bgimg.filepath)
                course_dict['from_level'] = course.course_level_from.level[3:]
                course_dict['to_level'] = course.course_level_to.level[3:]
                course_dict['language'] = course.course_lang.lang_english
                course_list.append(course_dict)
        except Exception as err:
            print str(err)
            course_list = None
        finally:
            return course_list

    @staticmethod
    @transaction.atomic
    def delete_course(course_id, tutor_id):
        """
        @summary: tutor course 삭제
        @param course_id: course id
        @param tutor_id: 튜터 id
        @return: True, False
        """
        result = True
        try:
            tutor_course = TutorCourse.objects.get(id=course_id, tutor_id=tutor_id)
            tutor_course.del_by_user = 1
            tutor_course.save()
            if CourseService.course_count(tutor_id) == 0:
                tutor_course.tutor.has_course = False
                tutor_course.tutor.save()
        except Exception as err:
            print str(err)
            result = False
        finally:
            return result

    @staticmethod
    def get_course(course_id):
        """
        @summary: 튜터 강의 정보
        @author: msjang
        @param course_id: course id
        @return object
        """
        tutor_course = dict()
        try:
            queryset = TutorCourse.objects.select_related(
                'course_bgimg', 'course_level_from', 'course_level_to',
                'course_lang'
            ).get(id=course_id, del_by_user=0, del_by_admin=0)
            tutor_course = model_to_dict(queryset,
                                         exclude=[
                                             'del_by_user',
                                             'del_by_admin'
                                         ])
            tutor_course['from_level'] = queryset.course_level_from.level[3:]
            tutor_course['to_level'] = queryset.course_level_to.level[3:]
            tutor_course['filepath'] = '%s%s' % (CourseService.course_bgimg_path(queryset.course_bgimg.type),
                                                 queryset.course_bgimg.filepath)
        except Exception as err:
            print str(err)
            tutor_course = None
        finally:
            return tutor_course

    @staticmethod
    def teaching_list(tutor_id):
        """
        @summary: 튜터가 가르칠 수 있는 언어 종류
        @param tutor_id: 튜터 id
        @return: list
        """
        result = list()
        try:
            tutor_obj = Tutor.objects.get(user_id=tutor_id)
            if tutor_obj.teaching1:
                obj = dict()
                obj['id'] = tutor_obj.teaching1_id_id
                obj['language'] = tutor_obj.teaching1
                result.append(obj)
            if tutor_obj.teaching2:
                obj = dict()
                obj['id'] = tutor_obj.teaching2_id_id
                obj['language'] = tutor_obj.teaching2
                result.append(obj)
            if tutor_obj.teaching3:
                obj = dict()
                obj['id'] = tutor_obj.teaching3_id_id
                obj['language'] = tutor_obj.teaching3
                result.append(obj)
        except Exception as err:
            print str(err)
            result = None
        finally:
            return result

    @staticmethod
    def get_course_price(course_id, bundle, duration):
        """
        @summary: 패키지, 시간에 따른 강의 가격
        @param course_id: course id
        @param bundle: package 여부
        @param duration: 강의 시간
        @return: 강의 가격
        """
        price_dict = {
            'per_price': 0,
            'total_price': 0
        }
        try:
            bundle_type = int(bundle)
            duration_type = int(duration)
            course_obj = TutorCourse.objects.get(id=course_id)
            if bundle_type == 1:
                if duration_type == 30:
                    price_dict['per_price'] = course_obj.sg_30min_price
                    price_dict['total_price'] = course_obj.sg_30min_price
                elif duration_type == 60:
                    price_dict['per_price'] = course_obj.sg_60min_price
                    price_dict['total_price'] = course_obj.sg_60min_price
                else:
                    price_dict['per_price'] = course_obj.sg_90min_price
                    price_dict['total_price'] = course_obj.sg_90min_price
            else:
                if duration_type == 30:
                    price_dict['per_price'] = course_obj.pkg_30min_price
                    price_dict['total_price'] = course_obj.pkg_30min_price * bundle_type
                elif duration_type == 60:
                    price_dict['per_price'] = course_obj.pkg_60min_price
                    price_dict['total_price'] = course_obj.pkg_60min_price * bundle_type
                else:
                    price_dict['per_price'] = course_obj.pkg_90min_price
                    price_dict['total_price'] = course_obj.pkg_90min_price * bundle_type
        except Exception as err:
            print '@@@@@get_course_price : ', str(err)
            price_dict['err'] = str(err)
        finally:
            return price_dict


class ScheduleService(object):
    def __init__(self):
        pass

    @staticmethod
    def get_year_month_day(user_utc):
        """
        @summary: 유저 현지시간에 맞는 년월일
        @param user_utc:
        @return: 날짜 ( ex. YYYYMMDD )
        """
        utc_now = datetime.utcnow() + timedelta(days=1)

        return datetime.strftime(utc_now + timedelta(hours=user_utc), '%Y%m%d')

    @staticmethod
    def get_utc_date_range(from_date, to_date, utc_time):
        """
        @summary: utc 적용 날짜범위
        @author:
        @param from_date: 시작날짜
        @param to_date: 종료날짜
        @param utc_time: utc 시간
        @return: range_list( list )
        """
        try:
            utc_index = int(utc_time * 2)
            if utc_index % 2 == 0:
                utc_timedelta = timedelta(hours=-(utc_index / 2))
            else:
                if utc_index < 0:
                    utc_timedelta = timedelta(hours=-(utc_index / 2), minutes=30)
                else:
                    utc_timedelta = timedelta(hours=-(utc_index / 2), minutes=-30)
            range_list = []
            start_date = datetime.strptime(from_date, "%Y%m%d")
            end_date = datetime.strptime(to_date, "%Y%m%d")
            for single_day in range(int((end_date - start_date).days) + 1):
                for idx in range(48):
                    single_date = start_date + timedelta(single_day)
                    single_date_string = strftime(
                        "%Y%m%d", single_date.timetuple()
                    ) + ScheduleService.index_to_time(idx)[4:8]
                    utc_time = datetime.strptime(single_date_string, "%Y%m%d%H%M") + utc_timedelta
                    time_string = strftime("%Y%m%d%H%M", utc_time.timetuple())
                    range_list.append(time_string[0:8]
                                      + ScheduleService.set_index_to_alphabet(utc_time.isoweekday())
                                      + time_string[8:12])
            return range_list
        except Exception as e:
            print str(e)

    @staticmethod
    def set_utc_time_list(date_info, time_list, utc_time):
        """
        @summary: time list utc 적용
        @author:
        @param date_info: 날짜정보
        @param time_list: 시간 정보
        @param utc_time: utc 시간
        @return: utc_time_list( list )
        """
        utc_time_list = []
        utc_index = int(utc_time * 2)
        if utc_index % 2 == 0:
            utc_timedelta = timedelta(hours=-(utc_index / 2))
        else:
            if utc_index < 0:
                utc_timedelta = timedelta(hours=-(utc_index / 2), minutes=30)
            else:
                utc_timedelta = timedelta(hours=-(utc_index / 2), minutes=-30)
        for column in time_list:
            utc_time = datetime.strptime(date_info[0:8] + column[4:8], "%Y%m%d%H%M") + utc_timedelta
            time_string = strftime("%Y%m%d%H%M", utc_time.timetuple())
            utc_time_list.append(time_string[0:8]
                                 + ScheduleService.set_index_to_alphabet(utc_time.isoweekday())
                                 + time_string[8:12])

        return utc_time_list

    @staticmethod
    def set_index_to_time(index):
        """
        @summary: 입력된 시간을 단위로 나누는 함수(00, 30분 단위 등등)
        @author: sj746
        @param index : 시간 인덱스
        @return: 시간 문자열
        """
        time_string = "time"
        if index % 2 == 0:
            if index / 2 < 10:
                time_string += "0" + str(index / 2) + "00"
            else:
                time_string += str(index / 2) + "00"
        else:
            if (index - 1) / 2 < 10:
                time_string += "0" + str((index - 1) / 2) + "30"
            else:
                time_string += str((index - 1) / 2) + "30"
        return time_string

    @staticmethod
    def set_time_list(from_time, to_time):
        """
        @summary: 입력 날짜의 차이로 시간을 스트링으로 변환
        @author: sj746
        @param from_time : 시작 날짜
        @param to_time : 종료 날짜
        @return: 변환된 시간 스트링 리스트
        """

        time_list = []
        for idx in range(from_time, to_time):
            time_string = ScheduleService.set_index_to_time(idx)
            time_list.append(time_string)
        return time_list

    @staticmethod
    def set_alphabet_to_index(alphabet):
        """
        @summary: 입력된 알파벳의 인덱스 를 리턴하는 함수
        @author: sj746
        @param alphabet : 알파벳 입력
        @return: 입력된 알파벳의 인덱스
        """

        alphabet_list = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
        return alphabet_list.index(alphabet) + 1

    @staticmethod
    def set_index_to_alphabet(index):
        """
        @summary: 입력된 인덱스로 알파벳을 구하는 함수
        @author: sj746
        @param index : 알고자하는 알파벳의 인덱스
        @return: 입력된 인덱스의 알파벳
        """

        alphabet_list = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
        return alphabet_list[index - 1]

    @staticmethod
    def apply_schedule_utc(server_list, utc_time):
        """
        @summary: 사용자가 설정한 utc 적용
        @author:
        @param server_list: 서버저장 시간
        @param utc_time: utc 정보
        @return: client_list( list )
        """
        client_list = []
        utc_index = int(utc_time * 2)
        for dayIndex, list_item in enumerate(server_list):  # for index(dayIndex : 0~6), value in enumerate(list)
            schedule_obj = {}
            for idx in range(48):
                index = idx - utc_index  # utc_index = 10
                if index < 0:
                    if dayIndex - 1 < 0:
                        schedule_obj[ScheduleService.index_to_time(idx)] = server_list[6][
                            ScheduleService.index_to_time(48 + index)]
                    else:
                        schedule_obj[ScheduleService.index_to_time(idx)] = server_list[dayIndex - 1][
                            ScheduleService.index_to_time(48 + index)]
                elif index > 47:
                    if dayIndex + 1 > 6:
                        schedule_obj[ScheduleService.index_to_time(idx)] = server_list[0][
                            ScheduleService.index_to_time(index - 48)]
                    else:
                        schedule_obj[ScheduleService.index_to_time(idx)] = server_list[dayIndex + 1][
                            ScheduleService.index_to_time(index - 48)]
                else:
                    schedule_obj[ScheduleService.index_to_time(idx)] = server_list[dayIndex][
                        ScheduleService.index_to_time(index)]
            schedule_obj["day_of_week"] = server_list[dayIndex]["day_of_week"]
            client_list.append(schedule_obj)
        return client_list

    @staticmethod
    def utc_to_index(utc_time):
        """
        @summary: utc -> index
        @author:
        @param utc_time: utc 시간
        @return: int
        """
        utc_index = int(utc_time[1:3]) * 2  # utc_time : +05:00

        if int(utc_time[4:6]) == 30:
            utc_index += 1

        if utc_time[0] == "-":
            utc_index = -utc_index
        return utc_index

    @staticmethod
    def index_to_time(index):
        """
        @summary: index -> time
        @author:
        @param index: index정보
        @return: string
        """
        time_string = "time"
        if (index % 2) == 0:
            if index / 2 < 10:
                time_string += "0" + str(index / 2) + "00"
            else:
                time_string += str(index / 2) + "00"
        else:
            if (index - 1) / 2 < 10:
                time_string += "0" + str((index - 1) / 2) + "30"
            else:
                time_string += str((index - 1) / 2) + "30"
        return time_string  # time1900

    @staticmethod
    def is_default_week_schedule(tutor_id):
        """
        @summary: 기본 스케줄 데이터가 존재 유무
        @param tutor_id: tutor id
        @return: True, False
        """
        return TutorScheduleWeek.objects.filter(tutor_id=tutor_id).exists()

    @staticmethod
    def make_default_week_schedule(tutor_id):
        """
        @summary: 처음 기본 스케줄 설정
        @param tutor_id: tutor id
        @return: True, False
        """
        result = True
        try:
            TutorScheduleWeek.objects.bulk_create([
                TutorScheduleWeek(tutor_id=tutor_id, day_of_week=1),
                TutorScheduleWeek(tutor_id=tutor_id, day_of_week=2),
                TutorScheduleWeek(tutor_id=tutor_id, day_of_week=3),
                TutorScheduleWeek(tutor_id=tutor_id, day_of_week=4),
                TutorScheduleWeek(tutor_id=tutor_id, day_of_week=5),
                TutorScheduleWeek(tutor_id=tutor_id, day_of_week=6),
                TutorScheduleWeek(tutor_id=tutor_id, day_of_week=7)
            ])
        except Exception as err:
            print str(err)
            result = False
        finally:
            return result

    @staticmethod
    def get_week_schedule(tutor_id, utc_time):
        """
        @summary: 튜터 주간 스케줄
        @param tutor_id: 튜터 id
        @param utc_time: 사용자 현지 시간
        @return: 주간 스케줄
        """
        result = list()
        try:
            queryset = TutorScheduleWeek.objects.filter(
                tutor_id=tutor_id
            ).order_by('day_of_week').defer('created_time', 'updated_time').values()
            schedule_list = list(queryset)
            result = ScheduleService.apply_schedule_utc(schedule_list, utc_time)

        except Exception as err:
            print str(err)
            result = None
        finally:
            return result

    @staticmethod
    def apply_time_list(day_of_week, from_time, to_time, utc_time):
        """
        @summary: utc 적용 리스트
        @param day_of_week: 요일정보
        @param from_time: 시작날짜
        @param to_time: 종료날짜
        @param utc_time: utc 시간
        @return: list
        """
        time_list = []
        try:
            utc_index = int(utc_time * 2)
            for time_index in range(from_time, to_time):
                time_obj = {}
                index = time_index - utc_index
                if index < 0:
                    if day_of_week - 1 < 1:
                        time_obj["day_of_week"] = 7
                    else:
                        time_obj["day_of_week"] = day_of_week - 1
                    time_obj["time"] = ScheduleService.index_to_time(48 + index)
                elif index > 47:
                    if day_of_week + 1 > 7:
                        time_obj["day_of_week"] = 1
                    else:
                        time_obj["day_of_week"] = day_of_week + 1
                    time_obj["time"] = ScheduleService.index_to_time(index + 47)

                else:
                    time_obj["day_of_week"] = day_of_week
                    time_obj["time"] = ScheduleService.index_to_time(index)
                time_list.append(time_obj)
        except Exception as err:
            print '@@@@@apply_time_list : ', str(err)
            time_list = None
        finally:
            return time_list

    @staticmethod
    @transaction.atomic
    def save_week_schedule(tutor_id, day_of_week, from_time, to_time, fn, utc_time):
        """
        @summary: 주간 단위 스케줄 저장 함수
        @param tutor_id: 튜터 id
        @param day_of_week: 날짜
        @param from_time: 시작 시간
        @param to_time: 완료 시간
        @param fn: 튜티 id
        @param utc_time: 헌지시간
        @return: True False
        """
        if not ScheduleService.is_default_week_schedule(tutor_id):
            ScheduleService.make_default_week_schedule(tutor_id)
        time_list = ScheduleService.apply_time_list(day_of_week, from_time, to_time, utc_time)
        if fn == 0:
            try:
                if time_list[0]["day_of_week"] == time_list[-1]["day_of_week"]:
                    day_schedule_0 = TutorScheduleWeek.objects.get(tutor_id=tutor_id,
                                                                   day_of_week=time_list[0]["day_of_week"])
                    for time in time_list:
                        time_string = ScheduleService.set_index_to_alphabet(time["day_of_week"]) + time["time"][4:8]
                        setattr(day_schedule_0, time["time"], 0)
                        delta_time = TutorScheduleCalendar.objects.filter(tutor_id=tutor_id,
                                                                          schedule_time__contains=time_string)
                        if delta_time:
                            for d_time in delta_time:
                                if d_time.type == 2:
                                    d_time.delete()
                    day_schedule_0.save()
                else:
                    day_schedule_0 = TutorScheduleWeek.objects.get(tutor_id=tutor_id,
                                                                   day_of_week=time_list[0]["day_of_week"])
                    day_schedule_1 = TutorScheduleWeek.objects.get(tutor_id=tutor_id,
                                                                   day_of_week=time_list[-1]["day_of_week"])
                    for time in time_list:
                        time_string = ScheduleService.set_index_to_alphabet(time["day_of_week"]) + time["time"][4:8]
                        if time_list[0]["day_of_week"] == time_list[-1]["day_of_week"]:
                            setattr(day_schedule_0, time["time"], 0)
                        else:
                            if time["day_of_week"] == time_list[0]["day_of_week"]:
                                setattr(day_schedule_0, time["time"], 0)
                            else:
                                setattr(day_schedule_1, time["time"], 0)
                        delta_time = TutorScheduleCalendar.objects.filter(tutor_id=tutor_id,
                                                                          schedule_time__contains=time_string)
                        if delta_time:
                            for d_time in delta_time:
                                if d_time.type == 2:
                                    d_time.delete()
                    day_schedule_0.save()
                    day_schedule_1.save()
            except Exception as err:
                print err
        else:
            try:
                if time_list[0]["day_of_week"] == time_list[-1]["day_of_week"]:
                    day_schedule_0 = TutorScheduleWeek.objects.get(tutor_id=tutor_id,
                                                                   day_of_week=time_list[0]["day_of_week"])
                    for time in time_list:
                        setattr(day_schedule_0, time["time"], 1)
                    day_schedule_0.save()
                else:
                    day_schedule_0 = TutorScheduleWeek.objects.get(tutor_id=tutor_id,
                                                                   day_of_week=time_list[0]["day_of_week"])
                    day_schedule_1 = TutorScheduleWeek.objects.get(tutor_id=tutor_id,
                                                                   day_of_week=time_list[-1]["day_of_week"])
                    for time in time_list:
                        if time["day_of_week"] == time_list[0]["day_of_week"]:
                            setattr(day_schedule_0, time["time"], 1)
                        else:
                            setattr(day_schedule_1, time["time"], 1)

                    day_schedule_0.save()
                    day_schedule_1.save()
            except Exception as err:
                print '@@@@@save_week_schedule : ', str(err)
        ScheduleService.check_schedule(tutor_id)

        return ScheduleService.get_week_schedule(tutor_id, utc_time)

    @staticmethod
    def check_schedule(tutor_id):
        """
        @summary: 튜터 스케줄 존재여부
        @param tutor_id: 튜터 id
        @return: None
        """
        week_list = list()
        has_calendar = True
        has_week = True
        try:
            queryset = TutorScheduleWeek.objects.filter(tutor_id=tutor_id)
            tutor_obj = Tutor.objects.get(user_id=tutor_id)
            for week in queryset:
                week_dict = model_to_dict(week, exclude=['id, tutor', 'day_of_week'])
                week_list.extend(week_dict.values())

            if True not in week_list:
                has_week = False

            if has_week:
                if not tutor_obj.has_schedule:
                    tutor_obj.has_schedule = True
                    tutor_obj.save()
            else:
                has_calendar = TutorScheduleCalendar.objects.filter(tutor_id=tutor_id, type=1).exists()
                if has_calendar:
                    if not tutor_obj.has_schedule:
                        tutor_obj.has_schedule = True
                        tutor_obj.save()
                else:
                    if tutor_obj.has_schedule:
                        tutor_obj.has_schedule = False
                        tutor_obj.save()
        except Exception as err:
            print str(err)

    @staticmethod
    def get_date():
        """
        @summary: 현재 년도와 월 정보
        @return: 현재 년월, 현재 년도, 월 정보(ex. 201609, 2016, 9)
        """
        today = datetime.today()
        return today.strftime("%Y%m%d"), today.year, today.month, today.day

    @staticmethod
    def get_month_info(year, month):
        """
        @summary: 년, 월 로 해당 달의 정보를 구하는 함수
        @author: sj746
        @param year: 년
        @param month: 월
        @return: 해당 달의 정보
        """

        calendar.setfirstweekday(6)
        month_list = calendar.monthcalendar(year, month)

        return month_list

    @staticmethod
    def get_month_schedule(tutor_id, year_month, utc_time):
        """
        @summary: 사용자의 해당 달의 레슨 가능한 시간을 구하는 함수
        @param tutor_id: 튜터 id
        @param year_month: 헤당 년/달
        @param utc_time: 현지 시간
        @return: 월간 일정 리스트
        """
        year = int(year_month[0:4])
        month = int(year_month[4:6])
        yearmonth = year_month[0:6]
        lastday = date(year, month, 1) - relativedelta(days=1)  # 2016-05-31
        firstday = date(year, month, 1) + relativedelta(months=1)  # 2016-07-01
        month_list = []
        try:
            queryset = TutorScheduleCalendar.objects.filter(
                Q(schedule_time__contains=yearmonth) |
                Q(schedule_time__contains=date.strftime(lastday, '%Y%m%d')) |
                Q(schedule_time__contains=date.strftime(firstday, '%Y%m%d')),
                tutor_id=tutor_id).defer('created_time', 'updated_time').values()
            for time in queryset:  # time -> month_schedule로 이름 바꾸기
                obj = dict()
                obj["schedule_time"] = ScheduleService.apply_utc_month_schedule(time["schedule_time"], utc_time)
                obj["date"] = int(obj["schedule_time"][6:8])
                # day -> dayofweek 으로 이름 바꾸기
                obj["day"] = ScheduleService.set_alphabet_to_index(obj["schedule_time"][8:9])
                obj["time"] = obj["schedule_time"][9:13]
                obj["type"] = time["type"]
                month_list.append(obj)

        except Exception as err:
            print err
            month_list = None
        finally:
            return month_list

    @staticmethod
    def apply_utc_month_schedule(schedule_time, utc_time):
        """
        @summary: utc 적용 월별 스케줄
        @param schedule_time: 시간 정보
        @param utc_time: utc 시간
        @return: string
        """
        utc_index = int(utc_time * 2)
        if utc_index % 2 == 0:
            utc_timedelta = timedelta(hours=(utc_index / 2))
        else:
            if utc_index < 0:
                utc_timedelta = timedelta(hours=(utc_index / 2), minutes=-30)
            else:
                utc_timedelta = timedelta(hours=(utc_index / 2), minutes=30)
        utc_time = datetime.strptime(schedule_time[0:8] + schedule_time[9:13],
                                     "%Y%m%d%H%M") + utc_timedelta  # deltatime =20160623D0100
        time_string = strftime("%Y%m%d%H%M", utc_time.timetuple())
        return time_string[0:8] + ScheduleService.set_index_to_alphabet(utc_time.isoweekday()) + time_string[8:12]

    @staticmethod
    @transaction.atomic
    def add_month_schedule(tutor_id, day, from_time, to_time, utc_time):
        """
        @summary: 사용자의 월간 일정 중 특정한 날의 수업 가능한 레슨 시간 저장
        @param tutor_id: 튜터 id
        @param day: 요일(A: 월 ~ G: 일)
        @param from_time: 월간 레슨 가능 시작 시간
        @param to_time: 월간 레슨 가능 종료 시간
        @param utc_time: 현지 시간
        @return: 월간 일정 리스트
        """

        try:
            day_schedule = TutorScheduleWeek.objects.filter(tutor_id=tutor_id).order_by("day_of_week")  # 주간 레슨 스케쥴
            time_list = ScheduleService.set_time_list(from_time, to_time)  # ['time0000', 'time0030']
            all_time_list = ScheduleService.set_utc_time_list(day, time_list,
                                                              utc_time)  # ['20160626G1500', '20160626G1530']
            for index, column in enumerate(all_time_list):
                delta_time = TutorScheduleCalendar.objects.filter(tutor_id=tutor_id, schedule_time=column)  # 월간 레슨 스케쥴
                if delta_time:
                    if delta_time[0].type == 2:
                        delta_time[0].delete()
                else:
                    if getattr(day_schedule[ScheduleService.set_alphabet_to_index(column[8])],
                               time_list[index]) == 0:  # 해당 요일에 일정이 없으켠 새로운 월간 레슨 가능 시간을 저장
                        TutorScheduleCalendar(tutor_id=tutor_id, schedule_time=column, type=1).save()  # 월간 레슨 스케쥴 저장
            month_list = ScheduleService.get_month_schedule(tutor_id, day[0:6], utc_time)
            ScheduleService.check_schedule(tutor_id)

            return month_list
        except Exception as err:
            print err

    @staticmethod
    def delete_month_schedule(tutor_id, day, from_time, to_time, utc_time):
        """
        @summary: 사용자의 월간 일정을 삭제하는 함수
        @param tutor_id: 튜터 id
        @param day: 스케줄 일정
        @param from_time: 주간 스케줄 시작 시간
        @param to_time: 주간 스케줄 종료 시간
        @param utc_time: 현지 시간
        @return: 월간 일정 리스트
        """
        day_schedule = TutorScheduleWeek.objects.filter(tutor_id=tutor_id).order_by("day_of_week")
        time_list = ScheduleService.set_time_list(from_time, to_time)  # ['time0000']
        all_time_list = ScheduleService.set_utc_time_list(day, time_list, utc_time)  # ['20160626G1500']
        for index, column in enumerate(all_time_list):
            # [<Schedule: Schedule object>]
            # delta_time = TutorScheduleCalendar.objects.filter(tutor_id=tutor_id, schedule_time=column)
            if getattr(day_schedule[ScheduleService.set_alphabet_to_index(column[8])], "time" + column[9:]) == 1:
                TutorScheduleCalendar(tutor_id=tutor_id, schedule_time=column, type=2).save()
            else:
                delta_time = TutorScheduleCalendar.objects.get(tutor_id=tutor_id, schedule_time=column)
                delta_time.delete()
        month_list = ScheduleService.get_month_schedule(tutor_id, day[0:6], utc_time)
        ScheduleService.check_schedule(tutor_id)

        return month_list

    @staticmethod
    def delete_range_schedule(tutor_id, year, month, from_date, to_date, utc_time):
        """
        @summary: 사용자의 특정 기간의 일정 지우는 함수
        @param tutor_id: 튜터 id
        @param year: 튜터 현지 시간의 년도
        @param month: 튜터 현지 시간의 월
        @param from_date: 주간 스케줄 시작 시간
        @param to_date: 주간 스케줄 종료 시간
        @param utc_time: 현지시간
        @return: 월간 일정 리스트
        """
        range_list = ScheduleService.get_utc_date_range(from_date, to_date, utc_time)
        day_schedule = TutorScheduleWeek.objects.filter(tutor_id=tutor_id).order_by("day_of_week")
        for time in range_list:
            if getattr(day_schedule[ScheduleService.set_alphabet_to_index(time[8]) - 1], "time" + time[9:]) == 1:
                TutorScheduleCalendar(
                    tutor_id=tutor_id,
                    schedule_time=time,
                    type=2
                ).save()
            else:
                schedule = TutorScheduleCalendar.objects.filter(tutor_id=tutor_id, schedule_time=time, type=1)
                if schedule:
                    schedule[0].delete()
        month_list = ScheduleService.get_month_schedule(tutor_id,
                                                        date(year, month, 1).strftime("%Y%m%d")[0:6],
                                                        utc_time)
        ScheduleService.check_schedule(tutor_id)

        return month_list

    @staticmethod
    def get_course_available(tutor_id):
        """
        @summary: 튜터 강의 가능 여부
        @param tutor_id: 튜터 id
        @return: dict
        """
        result = {
            'isSuccess': True,
            'message': 'Success'
        }
        try:
            tutor_obj = Tutor.objects.get(user_id=tutor_id)
            result['schedule_comment'] = tutor_obj.schedule_comment
            result['is_available'] = tutor_obj.is_available
        except Exception as err:
            print str(err)
            result['isSuccess'] = False
            result['message'] = str(err)
        finally:
            return result

    @staticmethod
    def set_course_available(tutor_id, is_available):
        """
        @summary: 강의 가능 여부 설정
        @param tutor_id: 튜터 id
        @param is_available: 가능 여부
        @return: True, False
        """
        result = True
        try:
            tutor_obj = Tutor.objects.get(user_id=tutor_id)
            tutor_obj.is_available = is_available
            tutor_obj.save()
        except Exception as err:
            print str(err)
            result = False
        finally:
            return result

    @staticmethod
    def set_schedule_comment(tutor_id, schedule_comment):
        """
        @summary: schedule comment 설정
        @param tutor_id: 튜터 id
        @param schedule_comment: 스케줄 코멘트
        @return: True, False
        """
        result = True
        try:
            tutor_obj = Tutor.objects.get(user_id=tutor_id)
            tutor_obj.schedule_comment = schedule_comment
            tutor_obj.save()
        except Exception as err:
            print str(err)
            result = False
        finally:
            return result

    @staticmethod
    def apply_user_utc(server_list, utc_time):
        """
        @summary: 사용자가 설정한 utc 적용
        @param server_list: 서버 저장 시간
        @param utc_time: utc 정보
        @return: list
        """
        client_list = list()
        try:
            utc_index = int(utc_time * 2)
            for dayIndex, list_item in enumerate(server_list):  # for index(dayIndex : 0~6), value in enumerate(list)
                schedule_obj = {}
                for idx in range(48):
                    index = idx - utc_index  # utc_index = 10
                    if index < 0:
                        if dayIndex - 1 < 0:
                            schedule_obj[
                                ScheduleService.index_to_time(idx)
                            ] = server_list[6][ScheduleService.index_to_time(48 + index)]
                        else:
                            schedule_obj[
                                ScheduleService.index_to_time(idx)
                            ] = server_list[dayIndex - 1][ScheduleService.index_to_time(48 + index)]
                    elif index > 47:
                        if dayIndex + 1 > 6:
                            schedule_obj[
                                ScheduleService.index_to_time(idx)
                            ] = server_list[0][ScheduleService.index_to_time(index - 48)]
                        else:
                            schedule_obj[
                                ScheduleService.index_to_time(idx)
                            ] = server_list[dayIndex + 1][ScheduleService.index_to_time(index - 48)]
                    else:
                        schedule_obj[
                            ScheduleService.index_to_time(idx)
                        ] = server_list[dayIndex][ScheduleService.index_to_time(index)]
                schedule_obj["day_of_week"] = server_list[dayIndex]["day_of_week"]
                client_list.append(schedule_obj)
        except Exception as err:
            print '@@@@@apply_user_utc : ', str(err)
            client_list = None
        finally:
            return client_list

    @staticmethod
    def get_tutor_schedule(tutor_id, user_utc, now=datetime.utcnow()):
        """
        @summary: 튜터 스케줄 정보
        @param tutor_id: 튜터 id
        @param user_utc: 접속 유저 utc ㅓ정보
        @param now: 현재 시각
        @return: object
        """
        res_obj = dict()
        week_list = list()
        dow_list = list()
        obj = dict()
        try:
            user_now = datetime.strptime(
                (now + timedelta(hours=user_utc)).strftime('%Y-%m-%d %H:%M:%S'),
                '%Y-%m-%d %H:%M:%S'
            )
            # user_now = datetime.strptime(str(now + timedelta(hours=user_utc)), "%Y-%m-%d %H:%M:%S")
            currunt_day_of_weak = int(user_now.weekday())

            dow_Str = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]
            for i in range(0, 7):
                timegap = timedelta(days=i)
                d = currunt_day_of_weak + i
                if d > 6:
                    d -= 7
                obj[dow_Str[d]] = str((user_now + timegap).year) + str((user_now + timegap).strftime("%m%d"))
                dow_list.append(dow_Str[d])
                week_list.append(obj[dow_Str[d]])

            res_obj["mark_day"] = str(user_now.year) + user_now.strftime("%m%d")
            # week_list ['20160623', '20160624', '20160625', '20160626', '20160627', '20160628', '20160629']
            res_obj["week_list"] = week_list
            res_obj["dow_list"] = dow_list  # [0: "thu", 1:"fri"]
            sc_list = []
            dow_sign = ["A", "B", "C", "D", "E", "F", "G"]

            schedule_list = TutorScheduleWeek.objects.filter(tutor_id=tutor_id).order_by("day_of_week")
            temp_list = []
            for schedule in schedule_list:
                scd = model_to_dict(schedule)
                temp_list.append(scd)
            client_list = ScheduleService.apply_user_utc(temp_list, user_utc)
            for schd in client_list:
                dow_sign[schd["day_of_week"] - 1]
                for field_name in schd:
                    if field_name != "day_of_week":
                        if schd[field_name] == 1:
                            sc_list.append(dow_sign[schd["day_of_week"] - 1] + str(field_name)[4:])

            res_obj["available_time"] = sc_list  # 0:"A2000" , 9:"B1930"

            # calendar 날짜별 입력된 내용
            start_day = str(user_now.year) + user_now.strftime("%m%d")  # 20160623
            timegap = timedelta(days=7)
            end_day = str((user_now + timegap).year) + str((user_now + timegap).strftime("%m%d"))  # 20160630
            cld_list = []
            for cld_schd in TutorScheduleCalendar.objects.filter(tutor_id=tutor_id):
                delta_time = ScheduleService.apply_utc_month_schedule(cld_schd.schedule_time, user_utc)
                cld_obj = {}
                if start_day <= delta_time[:8] <= end_day:
                    cld_obj["delta_time"] = delta_time[8:]
                    cld_obj["type"] = cld_schd.type
                    cld_list.append(cld_obj)

            res_obj["appointed"] = cld_list  # 월간일정
        except Exception as err:
            print '@@@@@get_tutor_schedule : ', str(err)
            res_obj['err'] = str(err)
        finally:
            return res_obj

    @staticmethod
    def user_utc_rv_time(rv_time, user_utc):
        """
        @summary: 사용자 utc 적용 예약 함수
        @author:
        @param rv_time: 예약 시간
        @param user_utc: 사용자 utc 시간
        @return: datetime
        """
        utc_index = int(user_utc * 2)
        time = datetime.strptime(rv_time[0:8] + rv_time[9:13], "%Y%m%d%H%M")
        if utc_index % 2 == 0:
            utc_timedelta = timedelta(hours=(utc_index / 2))
        else:
            if utc_index < 0:
                utc_timedelta = timedelta(hours=(utc_index / 2), minutes=-30)
            else:
                utc_timedelta = timedelta(hours=(utc_index / 2), minutes=30)
        utc_rv_time = time + utc_timedelta
        time_string = strftime("%Y%m%d%H%M", utc_rv_time.timetuple())

        return time_string[0:8] + ScheduleService.set_index_to_alphabet(utc_rv_time.isoweekday()) + time_string[8:12]

    @staticmethod
    def get_reserved_schedule(tutor_id, user_utc, now=datetime.utcnow()):
        """
        @summary: 예약된 시간표 얻기
        @param tutor_id: 튜터 id
        @param user_utc: user utc 정보
        @param now: 현재 시각
        @return: reserved_list
        """
        print "get_reserved_schedule"
        reserved_list = []
        user_utc = user_utc * (-1)
        try:
            user_now = now + timedelta(hours=user_utc)
            start_day = user_now.strftime('%Y%m%d')
            timegap = timedelta(days=7)
            end_day = (user_now + timegap).strftime('%Y%m%d')
            this_week = []
            for i in range(0, 7):
                timegap = timedelta(days=i)
                this_week.append(
                    (user_now + timegap).strftime('%Y%m%d')
                )

            queryset = LessonReservation.objects.select_related(
                'lesson'
            ).filter(tutor_id=tutor_id)
            for rv in queryset:
                rv_dict = {}
                if rv.rv_time:
                    utc_rv_time = ScheduleService.user_utc_rv_time(rv.rv_time, user_utc)
                    if int(start_day) <= int(utc_rv_time[:8]) <= int(end_day):
                        rv_dict['rv_time'] = utc_rv_time[8:]
                        if rv.lesson.lesson_type == 1:
                            rv_dict['duration'] = 30
                        elif rv.lesson.lesson_type == 2:
                            rv_dict['duration'] = rv.lesson.sg_min
                        else:
                            rv_dict['duration'] = rv.lesson.pkg_min
                        reserved_list.append(rv_dict)

        except Exception as err:
            print '@@@@@get_reserved_schedule : ', str(err)
            reserved_list = []
        finally:
            return reserved_list


class ReservationService(object):
    RESERVATION_STATUS = {
        9999: 'None',
        1: 'Requested',
        2: 'Approved/Upcoming',
        3: 'Problem reported',
        4: 'In dispute',
        5: 'In progress',
        6: 'Need confirmation',
        7: 'Confirmed',
        8: 'Canceled',
        9: 'Refunded',
        10: 'Expired',
        21: 'Approved/Upcoming - Request to change',
        22: 'Approved/Upcoming - Change date/time',
        23: 'Approved/Upcoming - Cancel',
    }

    RESERVATION_STATUS_TYPE = {
        0: 'None',
        1: 'Action required',
        2: 'Waiting for action'
    }

    PACKAGE_ID = 'P12345678'
    LESSON_ID = 'L1234567890'

    def __init__(self):
        pass

    @staticmethod
    def set_package_id(package_id):
        """
        @summary: package 아이디 변환
        @param package_id: 변환할 package id
        @return: 변환된 package id (ex. P12345678)
        """
        result = None
        try:
            result = ReservationService.PACKAGE_ID[0] + str(int(ReservationService.PACKAGE_ID[1:]) + int(package_id))
        except Exception as err:
            print '@@@@@set_package_id : ', str(err)
            result = None
        finally:
            return result

    @staticmethod
    def get_package_id(package_id):
        """
        @summary: 변환된 package id를 원래 id로 변환
        @param package_id: 변환된 package id
        @return: 원래 package id (ex. 1)
        """
        result = None
        try:
            result = int(package_id[1:]) - int(ReservationService.PACKAGE_ID[1:])
        except Exception as err:
            print '@@@@@get_package_id : ', str(err)
            result = None
        finally:
            return result

    @staticmethod
    def set_lesson_id(lesson_id):
        """
        @summary: package 아이디 변환
        @param lesson_id: 변환할 lesson id
        @return: 변환된 lesson id (ex. L1234567890)
        """
        result = None
        try:
            result = ReservationService.LESSON_ID[0] + str(int(ReservationService.LESSON_ID[1:]) + int(lesson_id))
        except Exception as err:
            print '@@@@@set_lesson_id : ', str(err)
            result = None
        finally:
            return result

    @staticmethod
    def get_lesson_id(lesson_id):
        """
        @summary: 변환된 lesson id를 원래 id로 변환
        @param lesson_id: 변환된 lesson id
        @return: 원래 lesson id (ex. 1)
        """
        result = None
        try:
            result = int(lesson_id[1:]) - int(ReservationService.LESSON_ID[1:])
        except Exception as err:
            print '@@@@@get_lesson_id : ', str(err)
            result = None
        finally:
            return result

    @staticmethod
    def set_status_history(reservation, history_type, data=None):
        """
        @summary: lesson detail 에 history 추가
        @param reservation: 해당 reservation id
        @param history_type: history 종류
        @param data: 신고 정보
        @return: true, false
        """
        result = True

        try:
            if history_type == 'Request':
                history_id = 1
            elif history_type == 'Approve':
                history_id = 2
                from_user = reservation.tutor_id
                to_user = reservation.tutee_id
            elif history_type == 'Decline':
                history_id = 3
            elif history_type == 'Confirm':
                history_id = 7
            elif history_type == 'Confirm2':
                history_id = 12
            elif history_type == 'Unschedule':
                history_id = 4
            elif history_type == 'Reschedule':
                history_id = 5
            elif history_type == 'Schedule':
                history_id = 1
            elif history_type == 'Problem':
                if reservation.tutor_id == data['reportor']:
                    history_id = 9
                    from_user = reservation.tutor_id
                    to_user = reservation.tutee_id
                elif reservation.tutee_id == data['reportor']:
                    history_id = 8
            elif history_type == 'Refund':
                history_id = 11
                from_user = reservation.tutor_id
                to_user = reservation.tutee_id
            else:
                print '@@@@@set_status_history : else : '

            lesson_history = LessonHistory(
                lesson_reservation_id=reservation.id,
                history_text_id=history_id

            )
            lesson_history.save()

            # TODO
            # lesson notification 추가해야함
            #TutorService.set_lesson_notification(1, from_user, to_user, reservation.id, 1)
        except Exception as err:
            print '@@@@@set_status_history : ', str(err)
            result = False
        finally:
            return result

    @staticmethod
    def update_reservation(user_utc, data):
        """
        @summary: 추가 강의 예약
        @param user_utc: 유저 utc 정보
        @param data: 예약 정보
        @return: True, False
        """
        result = True
        try:
            time_list = [None for _ in range(int(data['bundle']))]

            for i, t in enumerate(data['rv_time'].split('$')):
                if i >= len(time_list):
                    break
                time_list[i] = ScheduleService.user_utc_rv_time(t, user_utc)

            rv_list = data['rvid']

            for i in range(len(rv_list)):
                rv = LessonReservation.objects.get(id=rv_list[i])
                rv.rv_time = time_list[i]
                if not rv.rv_time:
                    rv.lesson_status = 0
                else:
                    if data['command'] == 'Schedule' or data['command'] == 'Reschedule':
                        rv.lesson_status = 1
                    rv.message = data['message']
                rv.save()
        except Exception as err:
            print '@@@@@update_reservation : ', str(err)
            result = False
        finally:
            return rv

    @staticmethod
    @transaction.atomic
    def set_reservation(user_id, user_utc, data):
        """
        @summary: 강의 예약 처리
        @param user_id: 유저 id
        @param user_utc: 유저 utc 정보
        @param data: 예약 정보
        @return: list
        """
        rv_list = []
        try:
            time_list = [None for _ in range(int(data['bundle']))]
            course_obj = TutorCourse.objects.select_related(
                'tutor'
            ).get(id=data['lid'])
            lesson_type = 2  # single course
            trial_price = 0
            lesson_obj = Lesson(
                course_id=data['lid'],
                tutor_id=data['tid'],
                tutee_id=user_id
            )
            lesson_duration = int(data['duration'])
            lesson_times = int(data['bundle'])

            if lesson_times != 1:
                lesson_type = 3
                lesson_obj.sg_min = None
            else:
                lesson_obj.pkg_min = None

            if int(data['accessType']) == 2:
                lesson_type = 1

            if lesson_type == 1:
                trial_price = course_obj.tutor.trial_price

            lesson_obj.trial_price = trial_price
            lesson_obj.lesson_type = lesson_type
            lesson_obj.pkg_times = lesson_times

            if lesson_type == 1 or lesson_type == 2:  # single course
                lesson_obj.sg_min = lesson_duration
                if lesson_duration == 30:
                    lesson_obj.sg_price = course_obj.sg_30min_price
                elif lesson_duration == 60:
                    lesson_obj.sg_price = course_obj.sg_60min_price
                else:
                    lesson_obj.sg_price = course_obj.sg_90min_price
            else:  # package course
                lesson_obj.pkg_min = lesson_duration
                lesson_obj.pkg_times = lesson_times
                if lesson_duration == 30:
                    lesson_obj.pkg_price = course_obj.pkg_30min_price
                elif lesson_duration == 60:
                    lesson_obj.pkg_price = course_obj.pkg_60min_price
                else:
                    lesson_obj.pkg_price = course_obj.pkg_90min_price
                lesson_obj.pkg_total_price = lesson_obj.pkg_price * lesson_times

            lesson_obj.save()
            lesson_id = lesson_obj.pk

            for i, t in enumerate(data['rv_time'].split('$')):
                if i >= len(time_list):
                    break
                time_list[i] = ScheduleService.user_utc_rv_time(t, -user_utc)

            reservation_list = [
                LessonReservation(
                    lesson_id=lesson_id,
                    course_id=course_obj.pk,
                    tutor_id=data['tid'],
                    tutee_id=user_id,
                    pkg_order=i + 1,
                    rv_time=time,
                    messenger_type=data['tool'],
                    messenger_id=data['r_tool_info']
                )
                for i, time in enumerate(time_list)
            ]

            setattr(reservation_list[0], 'message', data['message'])

            for i in reservation_list:
                if i.rv_time:
                    i.lesson_status = 1

            LessonReservation.objects.bulk_create(reservation_list)
            rv_list = reservation_list
            first_reservation = LessonReservation.objects.filter(lesson_id=lesson_id).order_by('id').first()

            LessonMessage(
                lesson_reservation_id=first_reservation.pk,
                sender_id=user_id,
                receiver_id=data['tid'],
                message=data['message']
            ).save()

        except Exception as err:
            print '@@@@@set_reservation : ', str(err)
        finally:
            return first_reservation

    # 추가 스케쥴 예약
    @staticmethod
    def add_schedule_reserve(_rvid, req):
        """
        @summary: 추가 스케줄 예약
        @author: msjang
        @param _rvid: 강의 예약 id
        @param req: http request
        @return: result_list( list )
        """
        print 'add_schedule_reserve'
#         post_dict = json.loads(req.body)
        # print post_dict['command']
        try:
            rv = LessonReservation.objects.select_related(
                'lesson', 'lesson__course').get(id=_rvid)
            print "rv"
            print rv.id
            #             rv.rv_time=None
            #             rv.status=0
            #             rv.user_confirm=None
            #             rv.tutor_confirm=None
            #             rv.save()
            add_rv_list = []
            add_rv_list.append(rv.id)
        # for item in Reservation.objects.filter(bundle_rvid=rv.bundle_rvid, rv_status=0):
        #                 add_rv_list.append(item.rvid)
        except:
            print "exception 1111111111111"
            return {"add_schedule": "/tutoring/"}
        # 예약정보 세션에 저장

        print 'add_schedule_reserve2'
        # req.session['reservation']
        post_dict = json.loads(req.body)
        obj = {}
        obj["rvid"] = add_rv_list
        obj["tid"] = rv.tutor_id
        obj["accessType"] = 3
        obj["lid"] = rv.course.id
        if rv.lesson.lesson_type == 2:
            obj["duration"] = rv.lesson.sg_min
        else:
            obj["duration"] = rv.lesson.pkg_min
        obj["bundle"] = rv.lesson.pkg_times
        obj["orders"] = rv.pkg_order
        obj["command"] = post_dict["command"]
        obj["tools"] = TutorService.get_tutor_tools(rv.tutor_id, None)
#         if rv.online == 1:
#             obj["online_type"] = 1
# 
#         if rv.offline == 1:
#             obj["online_type"] = 2

        print 'add_schedule_reserve3'
        print obj
        req.session['reservation'] = obj

        print 'add_schedule_reserve4'
        return {"add_schedule": "/tutor/reservation/how/"}

    @staticmethod
    def get_tutor_reservations(tutor_id, utc_time=0, status=None, page=1, filter_type=None,
                               search_name=None):
        """
        @summary: 튜터 강의 신청 받은 내역
        @param tutor_id: 튜터 id
        @param utc_time: utc time 정보
        @param status: 강의 예약 타입
        @param page: page 번호
        @param filter_type: 강의 리스트 필터
        @param search_name: 검색 이름
        @return: 강의예약 리스트
        """
        reservation_list = []
        result = {}
        try:
            queryset = LessonReservation.objects.select_related(
                'lesson', 'lesson__course', 'tutee'
            ).filter(tutor_id=tutor_id)

            if status:
                if type(status) is int:
                    queryset = queryset.filter(lesson_status=status)
                elif status == 'New Message':
                    message_query = LessonMessage.objects.select_related(
                        'lesson_reservation'
                    ).filter(
                        receiver_id=tutor_id,
                        is_read=False
                    ).order_by(
                        'created_time'
                    )
                    queryset = []
                    for mq in message_query:
                        queryset.append(mq.lesson_reservation)
                else:
                    queryset = queryset.filter(lesson_status__in=status)
            else:
                if search_name:
                    queryset = queryset.filter(tutee__name__contains=search_name)

            if not status and status != 'New Message':
                queryset = queryset.order_by('lesson_status', '-updated_time', 'id')

            paginator = Paginator(queryset, 10)
            page_item = paginator.page(page)
            page_list = paginator.page_range

            for rv in page_item.object_list:
                rv_dict = {
                    'rvid': rv.id,
                    'tutor_id': rv.tutor_id,
                    'tutee_id': rv.tutee_id,
                    'lesson_number': ReservationService.set_lesson_id(rv.id),
                    'lesson_title': rv.lesson.course.course_name,
                    'orders': rv.lesson.pkg_times,
                    'order': rv.pkg_order
                }
                if rv.rv_time:
                    lesson_date = datetime.strptime(rv.rv_time[:8] + rv.rv_time[9:],
                                                    '%Y%m%d%H%M') + timedelta(hours=utc_time)
                    lesson_time = datetime.strptime(str(lesson_date)[11:13]
                                                    + ':' + str(lesson_date)[14:16],
                                                    '%H:%M')
                    rv_dict['time'] = lesson_time.strftime('%I:%M%p') + \
                        (lesson_time + timedelta(minutes=rv.lesson.sg_min or rv.lesson.pkg_min)).strftime('%I:%M%p')
                    rv_dict['date_time'] = lesson_date.strftime('%Y/%m/%d') + ', ' + \
                        lesson_time.strftime('%I:%M%p') + '-' + \
                        (lesson_time + timedelta(minutes=rv.lesson.sg_min or rv.lesson.pkg_min)).strftime('%I:%M%p')
                    rv_dict['time_format'] = lesson_time.strftime('%H%M')
                else:
                    rv_dict['time'] = None
                    rv_dict['date'] = None
                rv_dict['duration'] = rv.lesson.sg_min or rv.lesson.pkg_min
                rv_dict['name'] = rv.tutee.name
                rv_dict['tutee_image'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                    rv.tutee.photo_filename)
                rv_dict['status'] = ReservationService.RESERVATION_STATUS[rv.lesson_status]
                current_time = datetime.utcnow()
                ####### LESSON STATUS ############
                if rv_dict['status'] == 4:
                    # in dispute
                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                elif rv_dict['status'] == 3:
                    # problem reported
                    firstLessonClaim = LessonClaim.objects.select_related('lesson_reservation').get(id = rv.id).order_by('created_time')[0]
                    lastLessonClaim = LessonClaim.objects.select_related('lesson_reservation').get(id = rv.id).order_by('-created_time')[0]
                    if current_time >= firstLessonClaim.created_time + timedelta(hours=72):
                        # change status to in dispute
                        #lh.history_text_id = LessonHistoryText.objects.get(id = 33)
                        LessonHistory(history_text_id = 30, lesson_reservation_id = rv_dict['rvid']).save()
                        rv.lesson_status = 4
                        rv.save()
                    else:
                        if lastLessonClaim.reporter_id == tutor_id:
                        # I was the last one to report
                            rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                        else:
                            rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                elif rv_dict['status'] == 7:
                    # confirmed
                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[0]
                elif rv_dict['status'] == 6:
                    # need confirmation
                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                elif rv_dict['status'] == 2 or rv_dict['status'] == 21:
                    #approved/upcoming or approved/upcoming - request to change
                    lesson_date = datetime.strptime(rv.rv_time[:8] + rv.rv_time[9:],
                                                    '%Y%m%d%H%M')
                    tmplesson = queryset.filter(id = rv.id)
                    if tmplesson.lesson.pkg_times == 1:
                        duration = tmplesson.lesson.sg_min
                    else:
                        duration = tmplesson.lesson.pkg_min
                    if current_time >= lesson_date and current_time < lesson_date + timedelta(minutes=duration):
                        rv_dict['status'] = 5
                        rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[0]
                    elif current_time > lesson_date + timedelta(minutes=duration):
                        # change to need confirmation but it's waiting for action since this is for the tutor
                        rv_dict['status'] = 6
                        rv.lesson_status = 6
                        rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                        rv.save()
                elif rv_dict['status']  == 22 or rv_dict['status'] == 23:
                    # one side needs to have action required
                    lm = LessonMessage.objects.get(lesson_reservation_id = rv.id)
                    if lm.sender_id == tutor_id:
                        # I as the tutor sent the request
                        rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                    else:
                        rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                else:
                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                ######### REVISED LINE #########################
                rv_dict['status_id'] = rv.lesson_status
                """
                if rv.rv_time:
                    lesson_time = datetime(int(rv.rv_time[:4]), int(rv.rv_time[4:6]), int(rv.rv_time[6:8]),
                                           int(rv.rv_time[9:11]), int(rv.rv_time[11:13]))
                    if rv.lesson_status == 2 \
                            and lesson_time < timezone.now() < lesson_time + timedelta(
                                minutes=rv.lesson.sg_min or rv.lesson.pkg_min):
                        rv_dict['status'] = 'In Progress'
                """
                reservation_list.append(rv_dict)

            result['total'] = paginator.num_pages
            result['entries'] = reservation_list
            result['page'] = page
            result['pgGroup'] = CommonService.paging_list(page_list, page, 5)
        except Exception as err:
            print '@@@@@get_tutor_reservation : ', str(err)
            result = {}
        finally:
            return result

    @staticmethod
    def get_tutee_reservations(tutee_id, utc_time=0, status=None, page=1, filter_type=None,
                               search_name=None):
        """
        @summary: 튜티 강의 신청 목록
        @param tutee_id: 튜티 id
        @param utc_time: utc time
        @param status: 강의 예약 타입
        @param page: page 번호
        @param filter_type: 강의 신청 목록 필터
        @param search_name: 이름검색
        @return:
        """
        reservation_list = []
        result = {}
        try:
            queryset = LessonReservation.objects.select_related(
                'lesson', 'lesson__course', 'tutor'
            ).filter(tutee_id=tutee_id)

            if status:
                if type(status) is int:
                    queryset = queryset.filter(lesson_status=status)
                elif status == 'New Message':
                    message_query = LessonMessage.objects.select_related(
                        'lesson_reservation'
                    ).filter(
                        receiver_id=tutee_id,
                        is_read=False
                    ).order_by(
                        'created_time'
                    )
                    queryset = []
                    for mq in message_query:
                        queryset.append(mq.lesson_reservation)
                else:
                    queryset = queryset.filter(lesson_status__in=status)
            else:
                if search_name:
                    queryset = queryset.filter(tutor__name__contains=search_name)

            if not status and status != 'New Message':
                queryset = queryset.order_by('lesson_status', '-updated_time', 'id')

            paginator = Paginator(queryset, 10)
            page_item = paginator.page(page)
            page_list = paginator.page_range
            current_time = datetime.utcnow()
            for rv in page_item.object_list:
                rv_dict = {
                    'rvid': rv.id,
                    'tutor_id': rv.tutor_id,
                    'tutee_id': rv.tutee_id,
                    'lesson_number': ReservationService.set_lesson_id(rv.id),
                    'lesson_title': rv.lesson.course.course_name,
                    'orders': rv.lesson.pkg_times,
                    'order': rv.pkg_order
                }
                if rv.rv_time:
                    lesson_date = datetime.strptime(rv.rv_time[:8] + rv.rv_time[9:],
                                                    '%Y%m%d%H%M') + timedelta(hours=utc_time)
                    lesson_time = datetime.strptime(str(lesson_date)[11:13]
                                                    + ':' + str(lesson_date)[14:16],
                                                    '%H:%M')
                    rv_dict['time'] = lesson_time.strftime('%I:%M%p') + \
                        (lesson_time + timedelta(minutes=rv.lesson.sg_min or rv.lesson.pkg_min)).strftime('%I:%M%p')
                    rv_dict['date_time'] = lesson_date.strftime('%Y/%m/%d') + ', ' + \
                        lesson_time.strftime('%I:%M%p') + '-' + \
                        (lesson_time + timedelta(minutes=rv.lesson.sg_min or rv.lesson.pkg_min)).strftime('%I:%M%p')
                    rv_dict['time_format'] = lesson_time.strftime('%H%M')
                else:
                    rv_dict['time'] = None
                    rv_dict['date'] = None
                rv_dict['duration'] = rv.lesson.sg_min or rv.lesson.pkg_min
                rv_dict['name'] = rv.tutor.name
                rv_dict['tutor_image'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                    rv.tutor.photo_filename)
                rv_dict['status'] = ReservationService.RESERVATION_STATUS[rv.lesson_status]
                
                ####### LESSON STATUS ############
                if rv_dict['status'] == 4:
                    # in dispute
                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                elif rv_dict['status'] == 3:
                    firstLessonClaim = LessonClaim.objects.select_related('lesson_reservation').get(id = rv.id).order_by('created_time')[0]
                    lastLessonClaim = LessonClaim.objects.select_related('lesson_reservation').get(id = rv.id).order_by('-created_time')[0]
                    if current_time >= firstLessonClaim.created_time + timedelta(hours=72):
                        # change status to in dispute
                        #lh.history_text_id = LessonHistoryText.objects.get(id = 33)
                        LessonHistory(history_text_id = 30, lesson_reservation_id = rv_dict['rvid']).save()
                        rv.lesson_status = 4
                        rv.save()
                    else:
                        if lastLessonClaim.reporter_id == tutee_id:
                        # I was the last one to report
                            rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                        else:
                            rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                elif rv_dict['status'] == 7:
                    # confirmed
                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[0]
                elif rv_dict['status'] == 1:
                    # requested
                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                elif rv_dict['status'] == 2 or rv_dict['status'] == 21:
                    #approved/upcoming or approved/upcoming - request to change
                    lesson_date = datetime.strptime(rv.rv_time[:8] + rv.rv_time[9:],
                                                    '%Y%m%d%H%M')
                    tmplesson = queryset.filter(id = rv.id)
                    if tmplesson.lesson.pkg_times == 1:
                        duration = tmplesson.lesson.sg_min
                    else:
                        duration = tmplesson.lesson.pkg_min
                    if current_time >= lesson_date and current_time < lesson_date + timedelta(minutes=duration):
                        rv_dict['status'] = 5
                        rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[0]
                    elif current_time > lesson_date + timedelta(minutes=duration):
                        # change to need confirmation but action is required since this is for the tutee
                        rv_dict['status'] = 6
                        rv.lesson_status = 6
                        rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                        rv.save()
                elif rv_dict['status']  == 22 or rv_dict['status'] == 23:
                    # one side needs to have action required
                    lm = LessonMessage.objects.get(lesson_reservation_id = rv.id)
                    if lm.sender_id == tutee_id:
                        # I as the tutee sent the request
                        rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                    else:
                        rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                else:
                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                ####### REVISED LINE #################
                rv_dict['status_id'] = rv.lesson_status
                """
                if rv.rv_time:
                    lesson_time = datetime(int(rv.rv_time[:4]), int(rv.rv_time[4:6]), int(rv.rv_time[6:8]),
                                           int(rv.rv_time[9:11]), int(rv.rv_time[11:13]))
                    if rv.lesson_status == 2 \
                            and lesson_time < timezone.now() < lesson_time + timedelta(
                                minutes=rv.lesson.sg_min or rv.lesson.pkg_min):
                        rv_dict['status'] = 'In Progress'
                """
                reservation_list.append(rv_dict)

            result['total'] = paginator.num_pages
            result['entries'] = reservation_list
            result['page'] = page
            result['pgGroup'] = CommonService.paging_list(page_list, page, 5)
        except Exception as err:
            print '@@@@@get_tutee_reservation : ', str(err)
            result = {}
        finally:
            return result

    @staticmethod
    def get_reservation_detail(user_id, reservation_id, utc_time):
        """
        @summary: Lesson Reservation detail
        @param user_id: 유저 id
        @param reservation_id: reservatoin id
        @param utc_time: utc time 정보
        @return: lesson reservation 정보
        """
        print "Starting get reservation detail"
        rv_dict = {}
        utc_time = utc_time * (-1)
        try:
            rv_obj = get_object_or_404(
                LessonReservation.objects.select_related(
                    'lesson', 'course', 'tutor', 'tutee',
                    'tutor__from_country_id', 'tutee__from_country_id'
                ), pk=reservation_id
            )
            current_time = datetime.utcnow()
            rv_dict['price'] = rv_obj.lesson.sg_price or rv_obj.lesson.pkg_price
            rv_dict['expired'] = (rv_obj.expired_time + timedelta(hours=utc_time)).strftime('%Y-%m-%d %H:%M:%S')
            rv_dict['user'] = user_id

            # rv_dict['is_tutor'] for checking if this user is the tutor
            if rv_obj.tutor_id == user_id:
                print "This user is the tutor of the lesson"
                rv_dict['is_tutor'] = True
            elif rv_obj.tutee_id == user_id:
                print "This user is the tutee of the lesson"
                rv_dict['is_tutor'] = False
            else:
                print "This user is not related to this lesson in any way"
                rv_dict['is_tutor'] = 'redirect'

            if not rv_dict['is_tutor'] == 'redirect':
                rv_dict['rvid'] = rv_obj.id
                rv_dict['reservation_number'] = ReservationService.set_lesson_id(rv_obj.id)
                rv_dict['lesson_name'] = rv_obj.course.course_name
                rv_dict['orders'] = rv_obj.lesson.pkg_times
                rv_dict['order'] = rv_obj.pkg_order

                if rv_dict['orders'] != 1:
                    rv_dict['package_id'] = ReservationService.set_package_id(rv_obj.lesson.id)

                if rv_obj.rv_time:
                    print "Checked if this lesson has a scheduled lesson time"
                    reservation_time = datetime.strptime(
                        str(rv_obj.rv_time[:8]) + str(rv_obj.rv_time[9:]),
                        '%Y%m%d%H%M'
                    )
                    user_rv_time = reservation_time + timedelta(hours=utc_time)

                    rv_dict['before_24_hours'] = reservation_time - current_time > timedelta(hours=24)
                    rv_dict['between_24_hours'] = timedelta() < reservation_time - current_time \
                        <= timedelta(hours=24)
                    rv_dict['between_72_hours'] = reservation_time < current_time \
                        < reservation_time + timedelta(hours=72)
                    rv_dict['after_72_hours'] = current_time - reservation_time > timedelta(hours=72)
                    rv_dict['date'] = user_rv_time.strftime('%Y-%m-%d')
                    rv_dict['time'] = user_rv_time.strftime('%H%M')
                    rv_dict['from_time'] = user_rv_time.strftime('%H:%M')
                    rv_dict['to_time'] = (
                        user_rv_time + timedelta(minutes=rv_obj.lesson.sg_min or rv_obj.lesson.pkg_min)
                    ).strftime('%H:%M')
                    rv_dict['from_date_time'] = user_rv_time.strftime('%Y-%m-%d %H:%M:%S')
                    rv_dict['duration'] = rv_obj.lesson.sg_min or rv_obj.lesson.pkg_min
                    rv_dict['day'] = user_rv_time.strftime('%A')
                else:
                    rv_dict['before_24_hours'] = False
                    rv_dict['between_24_hours'] = False
                    rv_dict['between_72_hours'] = False
                    rv_dict['after_72_hours'] = False
                    rv_dict["date"] = None
                    rv_dict["time"] = None
                    rv_dict["from_time"] = None
                    rv_dict["to_time"] = None
                    rv_dict["from_date_time"] = None
                    rv_dict["duration"] = None
                    rv_dict['day'] = None

                rv_dict['tutor_name'] = rv_obj.tutor.name
                rv_dict['tutor_id'] = rv_obj.tutor_id
                rv_dict['tutor_id_format'] = 'TU' + str('%011d' % int(rv_obj.tutor_id))
                rv_dict['tutor_img'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                  rv_obj.tutor.photo_filename)
                if getattr(rv_obj.tutor, 'from_country_id'):
                    rv_dict['tutor_flag'] = '%s/%s' % (CommonService.NATION_FILE_DIR,
                                                       rv_obj.tutor.from_country_id.flag_filename)
                else:
                    rv_dict['tutor_flag'] = None

                rv_dict['tutee_name'] = rv_obj.tutee.name
                rv_dict['tutee_id'] = rv_obj.tutee_id
                rv_dict['tutee_id_format'] = 'TE' + str('%011d' % int(rv_obj.tutee_id))

                if getattr(rv_obj.tutee, 'photo_filename'):
                    rv_dict['tutee_img'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                      rv_obj.tutee.photo_filename)
                else:
                    rv_dict['tutee_img'] = None

                if getattr(rv_obj.tutee, 'from_country_id'):
                    rv_dict['tutee_flag'] = '%s/%s' % (CommonService.NATION_FILE_DIR,
                                                       rv_obj.tutee.from_country_id.flag_filename)

                tool_list = CommonService.tool_list()
                rv_dict['tool_id'] = rv_obj.messenger_type
                rv_dict['tool_name'] = tool_list[rv_obj.messenger_type]['name']
                rv_dict['tutee_tool_id'] = rv_obj.messenger_id
                if rv_obj.messenger_type == 1:
                    rv_dict['tutor_tool_id'] = rv_obj.tutor.skype_id
                elif rv_obj.messenger_type == 2:
                    rv_dict['tutor_tool_id'] = rv_obj.tutor.hangout_id
                elif rv_obj.messenger_type == 3:
                    rv_dict['tutor_tool_id'] = rv_obj.tutor.facetime_id
                else:
                    rv_dict['tutor_tool_id'] = rv_obj.tutor.qq_id
                print "LessonFeedback"
                if LessonFeedback.objects.filter(lesson_reservation_id=rv_obj.id).exists():
                    feedback_obj = LessonFeedback.objects.filter(lesson_reservation_id=rv_obj.id)
                    for feedback in feedback_obj:
                        if feedback.tutor_feedback is not None:
                            rv_dict['tutor_feedback'] = feedback.tutor_feedback or '-'
                            rv_dict['feedback_tutor_id'] = feedback.tutor_id
                            rv_dict['feedback_date'] = (
                                feedback.updated_time + timedelta(hours=utc_time)
                            ).strftime('%Y-%m-%d %H:%M:%S')
                        if feedback.tutee_feedback is not None:
                            rv_dict['feedback_tutee_id'] = feedback.tutee_id or '-'
                            rv_dict['feedback2_date'] = (
                                feedback.created_time + timedelta(hours=utc_time)
                            ).strftime('%Y-%m-%d %H:%M:%S')
                            rv_dict['feedback2_rate'] = feedback.ratings
                else:
                    rv_dict['tutor_feedback'] = '-'
                    rv_dict['feedback_date'] = None
                    rv_dict['feedback_tutee_id'] = '-'
                    rv_dict['feedback2_date'] = None
                    rv_dict['feedback2_rate'] = None

                rv_dict['status'] = rv_obj.lesson_status
                rv_dict['status_name'] = ReservationService.RESERVATION_STATUS[rv_obj.lesson_status]
                rv_dict['action1'] = None
                rv_dict['action2'] = None
                rv_dict['action3'] = None
                if rv_obj.rv_time:
                    reservation_time = datetime.strptime(
                        str(rv_obj.rv_time[:8]) + str(rv_obj.rv_time[9:]),
                        '%Y%m%d%H%M'
                    )
                    ################### TUTOR LESSON STATUS DETAIL ######################
                    print "Calculating the status_type of this lesson; action required, waiting for action, or none"
                    if rv_obj.tutor_id == user_id:
                        if rv_obj.lesson_status == 4:
                            # in dispute
                            print "This lesson is in dispute state"
                            rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                        elif rv_obj.lesson_status == 3:
                            # problem reported
                            print "This lesson is in problem reported state"
                            firstLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).order_by('created_time')[0]
                            lastLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0]
                            if current_time >= datetime.strptime(str(firstLessonClaim.created_time)[:19], '%Y-%m-%d %H:%M:%S') + timedelta(hours=72):
                                # change status to in dispute
                                print "more than 72 hours passed since the first lesson claim"
                                LessonHistory(history_text_id = 30, lesson_reservation_id = rv_dict['rvid']).save()
                                firstLessonClaim.status = 5
                                firstLessonClaim.save()
                                rv_dict['status'] = 4
                                rv_obj.lesson_status = 4
                                rv_obj.save()
                                rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                            else:
                                # 72 hours didn't pass since the first lesson claim
                                print "less than 72 hours passed since the first lesson claim"
                                if lastLessonClaim.reportor_id == user_id:
                                # I was the last one to report
                                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                                else:
                                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]  
                        elif rv_obj.lesson_status == 7:
                            # confirmed
                            print "This lesson is confirmed2"
                            rv_dict['between_72_hours'] = False
                            rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[0]
                        elif rv_obj.lesson_status == 6:
                            # need confirmation
                            print "This lesson needs confirmation"
                            rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                        elif rv_obj.lesson_status == 2:
                            print "This lesson is approved/upcoming"
                            # approved/upcoming or approved/upcoming - request to change
                            # check if this is a single lesson
                            if rv_obj.lesson.pkg_times == 1:
                                duration = rv_obj.lesson.sg_min
                            else:
                                duration = rv_obj.lesson.pkg_min

                            # there is still time till the lesson time
                            if current_time <= reservation_time:
                                # check if there is more than a day left till the lesson starts
                                if reservation_time >= current_time + timedelta(hours=24):
                                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                                else:
                                    # there is less than 24 hours till the lesson_date time so there is no action to take
                                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[0]
                            else: # here it is assumed that [current time > lesson date]
                                # lesson date-------current time-------lesson date + duration of lesson
                                if current_time < reservation_time + timedelta(minutes=duration):
                                    print "This lesson is in progress"
                                    rv_dict['status'] = 5
                                    rv_dict['status_name'] = "In Progress"
                                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[0]
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 22).status_english
                                else:
                                    # change to need confirmation but it's waiting for action since this is for the tutor
                                    print "This lesson is in need of confirmation"
                                    rv_dict['status'] = 6
                                    rv_obj.lesson_status = 6
                                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                                    rv_obj.save()
                        elif rv_obj.lesson_status == 22 or rv_obj.lesson_status == 23:
                            print "This lesson has a request for changing the date/time or cancelling"
                            # one side needs to have action required
                            # currently the lesson status is approved/upcoming - change date/time, cancel
                            # based on the premise that a lesson message is sent when trying to send a change request
                            lm = LessonMessage.objects.filter(lesson_reservation_id = rv_obj.id, msg_type = 1).order_by('-created_time')[0]
                            if lm.sender_id is user_id:
                                # I as the tutor sent the request
                                print "the tutor sent the request to change"
                                rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                            else:
                                print "the tutee sent the request to change"
                                rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                        elif rv_obj.lesson_status == 1:
                            print "This lesson is requested"
                            # if the current_time is after the start time and the lesson status is still requested, then
                            # this means the tutor ignored the request
                            if current_time > reservation_time:
                                rv_dict['status'] = 8
                                rv_obj.lesson_status = 8
                                rv_obj.save()
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 21).status_english
                                rv_dict['status_name'] = ReservationService.RESERVATION_STATUS[rv_obj.lesson_status]
                                rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[0]
                            else:
                                rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                        else:
                            rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                    ###############################################################
                    ########## Done setting the 'status_type' and 'status_string'##
                    ##################### now onto setting the actions ############
                        print "calculating the actions available for this lesson"
                        if rv_obj.lesson_status == 1:
                            # there is a premise that the current_time <= reservation_time
                            # Requested - TUTOR
                            print "A tutee requested this lesson"
                            rv_dict['action1'] = 'Approve'
                            rv_dict['action2'] = 'Decline'
                            createtime = LessonReservation.objects.get(id = rv_dict['rvid']).created_time
                            # createtime------createtime+24hours---reservation_time-----createtime+48hour
                            if reservation_time >= datetime.strptime(str(createtime)[:19], '%Y-%m-%d %H:%M:%S') + timedelta(hours=24)\
                            and reservation_time < datetime.strptime(str(createtime)[:19], '%Y-%m-%d %H:%M:%S') + timedelta(hours=48):
                                print "created_time+24h <= reservation_time < created_time+48h"
                                rv_dict['status_time'] = reservation_time.strftime('%Y-%m-%d, %I:%M %p')
                            # createtime+48hours-----reservation_time---------createtime+720hours    
                            elif reservation_time >= datetime.strptime(str(createtime)[:19], '%Y-%m-%d %H:%M:%S') + timedelta(hours=48)\
                            and reservation_time < datetime.strptime(str(createtime)[:19], '%Y-%m-%d %H:%M:%S') + timedelta(hours=720):
                                print "created_time+48h <= reservation_time + created_time+720h"
                                rv_dict['status_time'] = (LessonReservation.objects.get(id = rv_dict['rvid']).created_time + timedelta(hours=48)).strftime('%Y-%m-%d, %I:%M %p')
                            # at this point the status time is already set
                            # single lesson 
                            if rv_dict['orders'] == 1:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 1).status_english % {'start_date':str(rv_dict['status_time']), 'newline': '\n'}
                            else:
                                # package lesson
                                # need to check on which lesson it is
                                if rv_dict['order'] == 1:
                                    # this is the first lesson of the pkg
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 65).status_english % {'start_date':str(rv_dict['status_time']), 'newline': '\n'}
                                else:
                                    # this is not the first lesson of the pkg
                                    if rv_obj.decline_time is 2:
                                        # the tutor already declined twice
                                        print "The tutor already declined this lesson twice"
                                        rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 67).status_english % {'start_date':str(rv_dict['status_time']), 'newline': '\n'}
                                    else:
                                        rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 66).status_english % {'start_date':str(rv_dict['status_time']), 'newline': '\n'}
                        elif rv_obj.lesson_status == 2 and rv_dict['status'] == 2:
                            # the reason we check both lesson_status and rv_dict['status'] here is to make sure
                            # that the lesson is not in progress or confirmed due to time calculations
                            # Approved/Upcoming
                            print "This lesson is in approved/upcoming state"
                            lastLessonHistory = LessonHistory.objects.filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0]
                            rv_dict['status_string'] = ""
                            if lastLessonHistory.history_text_id in (3, 13, 36):
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 2).status_english
                            elif lastLessonHistory.history_text_id == 7:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 7).status_english
                            elif lastLessonHistory.history_text_id == 8:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 8).status_english
                            elif lastLessonHistory.history_text_id == 9:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 10).status_english
                            elif lastLessonHistory.history_text_id == 10:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 9).status_english
                            elif lastLessonHistory.history_text_id == 12:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 12).status_english
                            elif lastLessonHistory.history_text_id == 15:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 13).status_english
                            elif lastLessonHistory.history_text_id == 16:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 14).status_english
                            elif lastLessonHistory.history_text_id == 17:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 15).status_english % {'newline': '\n'}
                            elif lastLessonHistory.history_text_id == 6:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 6).status_english
                            if reservation_time >= current_time + timedelta(hours=24):

                                rv_dict['action1'] = 'Request to change'
                                rv_dict['status_string'] += "\n" + LessonReservationStatusText.objects.get(id = 3).status_english
                        elif rv_obj.lesson_status == 22: # Change date/time
                            print "This lesson is in Change date/time state"
                            lastLessonRequest = LessonMessage.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id, msg_type = 1).order_by('-created_time')[0]
                            tempLessonReservation = LessonReservationTemp.objects.filter(id = rv_obj.id).order_by('-created_time')[0]
                            if tempLessonReservation.rv_time is not None:
                                new_date = datetime.strptime(tempLessonReservation.rv_time[:8] + tempLessonReservation.rv_time[9:], '%Y%m%d%H%M')
                            else:
                                # there is no rv_time in tempLessonReservation
                                new_date = 'New date'

                            if lastLessonRequest.sender_id == user_id:
                                print "This user sent the lesson request to change date/time"
                                rv_dict['action1'] = 'Edit request'
                                rv_dict['action2'] = 'Undo request'
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 4).status_english % {'newline':'\n', 'start_date': reservation_time, 'change_date': new_date}
                            else:
                                print "The tutee of the lesson requested to change date/time"
                                if rv_obj.decline_time == 2:
                                    print "This tutor already declined twice"
                                    # already decliend twice
                                    rv_dict['action1'] = 'Give a refund'
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 67).status_english % {'newline':'\n', 'start_date': reservation_time}
                                else:
                                    rv_dict['action1'] = 'Accept'
                                    rv_dict['action2'] = 'Decline'
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 5).status_english % {'newline':'\n', 'start_date': reservation_time, 'change_date':new_date}
                        elif rv_obj.lesson_status == 23: # Cancel
                            print "the tutee of the lesson requested to cancel the lesson"
                            rv_dict['action1'] = 'Accept'
                            rv_dict['action2'] = 'Decline'
                            rv_dict['status_string'] = LessonReservationStatusText.objects.get(id=11).status_english % {'newline':'\n', 'start_date': reservation_time}
                        elif rv_obj.lesson_status == 3:
                            print "This lesson has a problem reported2"
                            firstLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).order_by('created_time')[0]
                            lastLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0]
                            rv_dict['status_time'] = (firstLessonClaim.created_time + timedelta(hours=72)).strftime('%Y-%m-%d, %I:%M %p')
                            if datetime.strptime(str(firstLessonClaim.created_time)[:19], '%Y-%m-%d %H:%M:%S') + timedelta(hours=72) > current_time:
                                if lastLessonClaim.reportor_id == user_id:
                                    rv_dict['action1'] = 'Edit statement'
                                    rv_dict['action2'] = 'Undo report'
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 26).status_english % {'newline':'\n', 'report_exp_date': rv_dict['status_time']}
                                else:
                                    rv_dict['action1'] = 'Accept'
                                    rv_dict['action2'] = 'Make statement'
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 27).status_english % {'newline':'\n', 'report_exp_date': rv_dict['status_time']}
                        elif rv_obj.lesson_status == 6:
                            # need confirmation
                            print "This lesson needs confirmation"
                            rv_dict['status_time'] = (LessonReservation.objects.get(id = rv_dict['rvid']).expired_time + timedelta(hours=72)).strftime('%Y-%m-%d, %I:%M %p')
                            lastLessonHistory = LessonHistory.objects.filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0]
                            if (reservation_time
                                    + timedelta(minutes=rv_obj.lesson.sg_min or rv_obj.lesson.pkg_min)
                                    + timedelta(hours=72) >= current_time):
                                print "It's before 72 hours after this lesson ended"
                                rv_dict['action2'] = 'Report a problem'
                                # need to check if there is a feedback for this lesson
                                if not LessonFeedback.objects.filter(lesson_reservation_id = rv_obj.id).exists():
                                    rv_dict['action1'] = 'Leave Feedback'

                                if lastLessonHistory.history_text_id == 25:
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 23).status_english % {'confirm_exp_date': rv_dict['status_time']}
                                elif lastLessonHistory.history_text_id == 26:
                                    #tutor undid report
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 24).status_english % {'confirm_exp_date': rv_dict['status_time']}
                                elif lastLessonHistory.history_text_id == 27:
                                    #tutee undid report
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 25).status_english % {'confirm_exp_date': rv_dict['status_time']}
                            else:
                                print "More than 72 hours passed since the end of the lesson"
                                print "changing the status of this lesson to confirmed"
                                rv_dict['status'] = 7
                                rv_obj.lesson_status = 7
                                rv_obj.save()
                                # need to set the appropriate status strings and shit
                                if rv_obj.lesson.pkg_times == 1:
                                    duration = rv_obj.lesson.sg_min
                                else:
                                    duration = rv_obj.lesson.pkg_min
                                rv_dict['status_time'] = reservation_time + timedelta(minutes=duration)
                                # check if there is a feedback from the tutee
                                if LessonFeedback.objects.filter(lesson_reservation_id = rv_obj.id).exists():
                                    if LessonFeedback.objects.filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0].tutor_feedback:
                                        # tutor was the one to make feedback
                                        rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 32).status_english
                                    elif LessonFeedback.objects.filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0].tutee_feedback:
                                        # tutee was the only one to make feedback
                                        rv_dict['action1'] = 'Leave Feedback'
                                        rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 32).status_english
                                else:
                                    rv_dict['action1'] = 'Leave Feedback'
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 31).status_english
                        elif rv_obj.lesson_status == 4: # in dispute
                            print "This lesson is in dispute"
                            # there should be a lesson claim for this lesson reservation
                            firstLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).order_by('created_time')[0]
                            rv_dict['status_time'] = (firstLessonClaim.created_time + timedelta(hours=72)).strftime('%Y-%m-%d, %I:%M %p')
                            rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 28).status_english % {'report_exp_date': rv_dict['status_time']}
                        elif rv_obj.lesson_status == 7: # confirmed
                            print "This lesson is confirmed!"
                            if rv_obj.lesson.pkg_times == 1:
                                duration = rv_obj.lesson.sg_min
                            else:
                                duration = rv_obj.lesson.pkg_min
                            rv_dict['status_time'] = reservation_time + timedelta(minutes=duration)
                            if current_time < rv_dict['status_time'] + timedelta(hours=72):
                                if LessonFeedback.objects.filter(lesson_reservation_id=rv_obj.id).count() > 1:
                                    if rv_obj.tutor_id == user_id:#tutor
                                        rv_dict['status_string'] = LessonReservationStatusText.objects.get(id=30).status_english
                                    else:
                                        rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 32).status_english
                                else:
                                    feedback_obj = LessonFeedback.objects.get(lesson_reservation_id=rv_obj.id)
                                    if rv_obj.tutor_id == user_id:#tutor
                                        if feedback_obj.tutor_feedback is not None:
                                            rv_dict['status_string'] = LessonReservationStatusText.objects.get(id=30).status_english
                                        else:
                                            rv_dict['action1'] = 'Leave Feedback'
                                            rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 29).status_english
                                    else:
                                        if feedback_obj.tutee_feedback is not None:
                                            rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 32).status_english
                                        else:
                                            rv_dict['action1'] = 'Leave Feedback'
                                            rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 32).status_english
                            else:
                                if LessonFeedback.objects.filter(lessone_reservation_id=rv_obj.id).count() > 1:
                                    if rv_obj.tutor_id == user_id:#tutor
                                        rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 32).status_english
                                    else:
                                        rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 32).status_english
                                else:
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 32).status_english
                            # check if there was a lesson claim resolution that caused this situation.
                            if LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).exists():
                                lastLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0]
                                if lastLessonClaim.resolve_type in ('6', '9'):
                                    tmpprice = rv_obj.lesson.sg_price
                                    if tmpprice is 0:
                                        # package lesson
                                        tmpprice = rv_obj.lesson.pkg_price
                                    rv_dict['status_string']= LessonReservationStatusText.objects.get(id = 33).status_english % {'tutor_refund': tmpprice}       
                        elif rv_obj.lesson_status == 8: # cancelled
                            # need to check if the tutee cancelled, or I was ignorant, or 
                            lastLessonHistory = LessonHistory.objects.filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0]
                            if lastLessonHistory.history_text_id == 20: # tutee cancelled lesson request
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 18).status_english
                            elif lastLessonHistory.history_text_id == 21: # accepted cancel request of tutee
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 19).status_english
                            elif lastLessonHistory.history_text_id == 22: # tutor decline lesson request
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 20).status_english
                            elif lastLessonHistory.history_text_id == 23: # no action from tutor within deadline
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 21).status_english
                        elif rv_obj.lesson_status == 9:
                            # refunded
                            lastLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0]
                            if lastLessonClaim.resolve_type in ('4', '8'):
                                tmpprice = rv_obj.lesson.sg_price
                                if tmpprice is  0:
                                    # package lesson
                                    tmpprice = rv_obj.lesson.pkg_price
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 16).status_english % {'tutee_refund': tmpprice}
                            elif lastLessonClaim.resolve_type == '7':
                                tutee_refund = TuteeWalletHistory.objects.get(type = 5, type_ref = rv_obj.id).pending_delta
                                tutor_refund = TutorWalletHistory.objects.get(type = 5, type_ref = rv_obj.id).pending_delta
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 49).status_english % {'tutee_refund': tutee_refund, 'tutor_refund': tutor_refund}
                    #################### TUTEE LESSON STATUS DETAIL #########################
                    else:
                        print "tutee : Calculating the status_type of this lesson"
                        ####### LESSON STATUS ############
                        if rv_dict['status'] == 4:
                            # in dispute
                            print "This lesson is in dispute"
                            rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                        elif rv_dict['status'] == 3:
                            print "This lesson has a problem reported3"
                            firstLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).order_by('created_time')[0]
                            lastLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0]
                            if current_time >= datetime.strptime(str(firstLessonClaim.created_time)[:19], '%Y-%m-%d %H:%M:%S') + timedelta(hours=72):
                                # change status to in dispute
                                LessonHistory(history_text_id = 30, lesson_reservation_id = rv_dict['rvid']).save()
                                rv_dict['status'] = 4
                                rv_obj.lesson_status = 4
                                rv_obj.save()
                                rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                            else:
                                if lastLessonClaim.reportor_id == user_id:
                                # I was the last one to report
                                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                                else:
                                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                        elif rv_dict['status'] == 7:
                            # confirmed
                            print "This lesson is confirmed1"
                            rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[0]
                        elif rv_dict['status'] == 1:
                            # requested
                            print "This lesson is requested"
                            if current_time > reservation_time:
                                rv_dict['status'] = 8
                                rv_obj.lesson_status = 8
                                rv_obj.save()
                                LessonHistory(history_text_id=21, lesson_reservation_id=rv_obj.id).save()
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 21).status_english
                                rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[0]
                            else:
                                rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                        elif rv_dict['status'] == 2:
                            #approved/upcoming or approved/upcoming - request to change
                            if rv_obj.lesson.pkg_times == 1:
                                duration = rv_obj.lesson.sg_min
                            else:
                                duration = rv_obj.lesson.pkg_min
                            if current_time <= reservation_time:
                                if reservation_time >= current_time + timedelta(hours=24):
                                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                                else:
                                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[0]
                            else:
                                if current_time < reservation_time + timedelta(minutes=duration):
                                    rv_dict['status'] = 5
                                    rv_dict['status_name'] = "In Progress"
                                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[0]
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 22).status_english
                                else:
                                    # change to need confirmation but it's action is required since this is for the tutee
                                    rv_dict['status'] = 6
                                    rv_obj.lesson_status = 6
                                    rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                                    rv_obj.save()
                        elif rv_dict['status'] == 22 or rv_dict['status'] == 23:
                            # one side needs to have action required
                            lastLessonRequest = LessonMessage.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id, msg_type = 1).order_by('-created_time')[0]
                            if lastLessonRequest.sender_id == user_id:
                                # I as the tutee sent the request
                                rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[2]
                            else:
                                rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                        else:
                            rv_dict['status_type'] = ReservationService.RESERVATION_STATUS_TYPE[1]
                        ##############################################
                        print "Calculating the lesson_status to set the status string of the lesson"
                        if rv_obj.lesson_status == 1:
                            print "This lesson is requested"
                            # Requested
                            rv_dict['action1'] = 'Change date/time'
                            rv_dict['action2'] = 'Cancel'
                            createtime = LessonReservation.objects.get(id = rv_dict['rvid']).created_time
                            if reservation_time >= datetime.strptime(str(createtime)[:19], '%Y-%m-%d %H:%M:%S') + timedelta(hours=24)\
                            and reservation_time < datetime.strptime(str(createtime)[:19], '%Y-%m-%d %H:%M:%S') + timedelta(hours=48):
                                rv_dict['status_time'] = reservation_time.strftime('%Y-%m-%d, %I:%M %p')
                            elif reservation_time >= datetime.strptime(str(createtime)[:19], '%Y-%m-%d %H:%M:%S') + timedelta(hours=48)\
                            and reservation_time < datetime.strptime(str(createtime)[:19], '%Y-%m-%d %H:%M:%S') + timedelta(hours=720):
                                rv_dict['status_time'] = (LessonReservation.objects.get(id = rv_dict['rvid']).created_time + timedelta(hours=48)).strftime('%Y-%m-%d, %I:%M %p')
                            # status time should be set
                            if rv_dict['orders'] == 1:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 1).status_english % {'start_date':str(rv_dict['status_time']), 'newline': '\n'}
                            else:
                                # package lesson
                                # need to check on which lesson it is
                                if rv_dict['order'] == 1:
                                    # this is the first lesson of the pkg
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 77).status_english % {'start_date':str(rv_dict['status_time']), 'newline': '\n'}
                                else:
                                    # this is not the first lesson of the pkg
                                    if rv_obj.decline_time == 2:
                                        rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 79).status_english % {'start_date':str(rv_dict['status_time']), 'newline': '\n'}
                                    else:
                                        rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 78).status_english % {'start_date':str(rv_dict['status_time']), 'newline': '\n'}
                        elif rv_obj.lesson_status == 2 and rv_dict['status'] == 2:
                            # Approved/Upcoming
                            print "This lesson is in approved/upcoming state"
                            lastLessonHistory = LessonHistory.objects.filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0]
                            rv_dict['status_string'] = ""
                            if lastLessonHistory.history_text_id in (3, 13, 36):
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 35).status_english
                            elif lastLessonHistory.history_text_id == 7:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 39).status_english
                            elif lastLessonHistory.history_text_id == 8:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 40).status_english
                            elif lastLessonHistory.history_text_id == 9:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 42).status_english
                            elif lastLessonHistory.history_text_id == 10:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 41).status_english
                            elif lastLessonHistory.history_text_id == 12:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 44).status_english
                            elif lastLessonHistory.history_text_id == 15:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 45).status_english
                            elif lastLessonHistory.history_text_id == 16:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 46).status_english
                            elif lastLessonHistory.history_text_id == 17:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 47).status_english % {'newline': '\n'}
                            elif lastLessonHistory.history_text_id == 5:
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 35).status_english
                            if reservation_time >= current_time + timedelta(hours=24):
                                rv_dict['action1'] = 'Request to change'
                                rv_dict['status_string'] += "\n" + LessonReservationStatusText.objects.get(id = 3).status_english
                        elif rv_obj.lesson_status == 22:
                            print "This lesson is approved/upcoming - change date/time state"
                            lastLessonRequest = LessonMessage.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id, msg_type = 1).order_by('-created_time')[0]
                            tempLessonReservation = LessonReservationTemp.objects.filter(id = rv_obj.id).order_by('-created_time')[0]
                            if tempLessonReservation.rv_time is not None:
                                new_date = datetime.strptime(tempLessonReservation.rv_time[:8] + tempLessonReservation.rv_time[9:],
                                                                '%Y%m%d%H%M')
                            else:
                                new_date = "New Date"
                            if lastLessonRequest.sender_id == user_id:
                                rv_dict['action1'] = 'Edit request'
                                rv_dict['action2'] = 'Undo request'
                                if rv_obj.decline_time == 2:
                                    # already declined twice
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 79).status_english % {'newline':'\n', 'start_date': reservation_time}
                                else:
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 37).status_english % {'newline':'\n', 'start_date': reservation_time, 'change_date':new_date}
                            else:
                                rv_dict['action1'] = 'Accept'
                                rv_dict['action2'] = 'Decline'
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 36).status_english % {'newline':'\n', 'start_date': reservation_time, 'change_date':new_date}
                        elif rv_obj.lesson_status == 23:
                            print "this lesson is in approved/upcoming - request to cancel"
                            lastLessonRequest = LessonMessage.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id, msg_type = 1).order_by('-created_time')[0]
                            rv_dict['action1'] = 'Undo request'
                            rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 43).status_english % {'newline':'\n', 'start_date': rv_obj.rv_time}
                        elif rv_obj.lesson_status == 3:
                            # Problem reported
                            print "This lesson has a problem reported1"
                            firstLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).order_by('created_time')[0]
                            lastLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0]
                            rv_dict['status_time'] = (firstLessonClaim.created_time + timedelta(hours=72)).strftime('%Y-%m-%d, %I:%M %p')
                            if datetime.strptime(str(firstLessonClaim.created_time)[:19], '%Y-%m-%d %H:%M:%S') + timedelta(hours=72) > current_time:
                                if lastLessonClaim.reportor_id == user_id:
                                    rv_dict['action1'] = 'Edit statement'
                                    rv_dict['action2'] = 'Undo report'
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 59).status_english % {'newline':'\n', 'report_exp_date': rv_dict['status_time']}
                                else:
                                    rv_dict['action1'] = 'Accept'
                                    rv_dict['action2'] = 'Make statement'
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 58).status_english % {'newline':'\n', 'report_exp_date': rv_dict['status_time']}
                        elif rv_obj.lesson_status == 6:
                            # need confirmation
                            print "This lesson needs confirmation"
                            rv_dict['status_time'] = (LessonReservation.objects.get(id = rv_dict['rvid']).expired_time + timedelta(hours=72)).strftime('%Y-%m-%d, %I:%M %p')
                            lastLessonHistory = LessonHistory.objects.filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0]
                            if (reservation_time
                                    + timedelta(minutes=rv_obj.lesson.sg_min or rv_obj.lesson.pkg_min)
                                    + timedelta(hours=72) >= current_time):
                                rv_dict['action2'] = 'Report a problem'
                                rv_dict['action1'] = 'Confirm'
                                if lastLessonHistory.history_text_id == 25:
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 55).status_english % {'confirm_exp_date' : rv_dict['status_time']}
                                elif lastLessonHistory.history_text_id == 26:
                                    # tutor undid report
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 56).status_english % {'confirm_exp_date': rv_dict['status_time']}
                                elif lastLessonHistory.history_text_id == 27:
                                    # tutee undid report
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 57).status_english % {'confirm_exp_date': rv_dict['status_time']}
                            else:
                                rv_dict['status'] = 7
                                rv_obj.lesson_status = 7
                                rv_obj.save()
                                if LessonFeedback.objects.filter(lesson_reservation_id = rv_obj.id).exists():
                                    if LessonFeedback.objects.filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0].tutor_feedback:
                                        # tutor was the one to make feedback
                                        if LessonFeedback.objects.filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0].tutee_feedback:
                                            rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 63).status_english
                                        else:
                                            rv_dict['action1'] = 'Leave Feedback'
                                            rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 62).status_english
                                    elif LessonFeedback.objects.filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0].tutee_feedback:
                                        rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 63).status_english
                                else:
                                    rv_dict['action1'] = 'Leave Feedback'
                                    rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 62).status_english
                        elif rv_obj.lesson_status == 4: # in dispute
                            print "This lesson is in dispute"
                            firstLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).order_by('created_time')[0]
                            lastLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0]
                            rv_dict['status_time'] = (firstLessonClaim.created_time + timedelta(hours=72)).strftime('%Y-%m-%d, %I:%M %p')
                            rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 60).status_english % {'report_exp_date': rv_dict['status_time']}
                        elif rv_obj.lesson_status == 7: # confirmed
                            print "This lesson is confirmed3"
                            lesson_date = datetime.strptime(rv_obj.rv_time[:8] + rv_obj.rv_time[9:],
                                                            '%Y%m%d%H%M')
                            if rv_obj.lesson.pkg_times == 1:
                                duration = rv_obj.lesson.sg_min
                            else:
                                duration = rv_obj.lesson.pkg_min
                            rv_dict['status_time'] = lesson_date + timedelta(minutes=duration)
                            if current_time < rv_dict['status_time'] + timedelta(hours=72):
                                # before the 3 day limit after the course ended
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 61).status_english
                            else:
                                print "This lesson's deadline for confirmation passed"
                                #passed the 72 hours - but how do I know that the tutee confirmed this lesson?
                                if rv_obj.tutor_id == user_id:
                                    if LessonFeedback.objects.get(lesson_reservation_id=rv_obj.id).tutor_feedback is not None:
                                        rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 63).status_english
                                    else:
                                        rv_dict['action1'] = 'Leave Feedback'
                                        rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 62).status_english
                                else:#tutee
                                    print rv_obj.id
                                    if LessonFeedback.objects.get(lesson_reservation_id=rv_obj.id).tutee_feedback is not None:
                                        rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 63).status_english
                                    else:
                                        rv_dict['action1'] = 'Leave Feedback'
                                        rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 62).status_english
                            # check if there was a lesson claim resolution that caused this situation.
                            if LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).exists():
                                lastLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0]
                                if lastLessonClaim.resolve_type in ('6', '9'):
                                    tmpprice = rv_obj.lesson.sg_price
                                    if tmpprice is  0:
                                        # package lesson
                                        tmpprice = rv_obj.lesson.pkg_price
                                    rv_dict['status_string']= LessonReservationStatusText.objects.get(id = 64).status_english % {'tutor_refund': tmpprice}
                        elif rv_obj.lesson_status == 8: # cancelled
                            # need to check if the tutee cancelled, or I was ignorant, or 
                            lastLessonHistory = LessonHistory.objects.filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0]
                            if lastLessonHistory.history_text_id == 20: # tutee cancelled lesson request
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 50).status_english
                            elif lastLessonHistory.history_text_id == 21: # accepted cancel request of tutee
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 51).status_english
                            elif lastLessonHistory.history_text_id == 22: # tutor decline lesson request
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 52).status_english
                            elif lastLessonHistory.history_text_id == 23: # no action from tutor within deadline
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 53).status_english   
                        elif rv_obj.lesson_status == 9:
                            # refunded
                            # need to get the appropriate transaction - for the tutee
                            lastLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv_obj.id).order_by('-created_time')[0]
                            if lastLessonClaim.resolve_type in ('4', '8'):
                                tmpprice = rv_obj.lesson.sg_price
                                if tmpprice is  0:
                                    # package lesson
                                    tmpprice = rv_obj.lesson.pkg_price
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 48).status_english % {'tutee_refund': tmpprice}
                            elif lastLessonClaim.resolve_type == '7':
                                tutee_refund = TuteeWalletHistory.objects.get(type = 5, type_ref = rv_obj.id).pending_delta
                                tutor_refund = TutorWalletHistory.objects.get(type = 5, type_ref = rv_obj.id).pending_delta
                                rv_dict['status_string'] = LessonReservationStatusText.objects.get(id = 49).status_english % {'tutee_refund': tutee_refund, 'tutor_refund': tutor_refund}
            print "Finished setting all the status_type, status_string, status_time for the lesson"
            history = LessonHistory.objects.select_related('history_text').filter(lesson_reservation = rv_obj.id).order_by('created_time')
            history_list = []
            # need to make sure that there is a way to make the TC values contained inside
            for i in history:
                if i.history_text_id in (18, 19):
                    tmpstring = str(i.history_text.history_english)
                    tuteeamnt = LessonClaim.objects.filter(lesson_reservation_id=rv_obj.id).order_by('created_time')[0].resolve_tc
#                     tuteeamnt = TuteeWalletHistory.objects.get(type=5, type_ref=rv_obj.id).pending_delta
                    if "$$$" in tmpstring:
                        # this means some has been returned to the tutor and some to the tutee
                        tutoramnt = TutorWalletHistory.objects.get(type=5, type_ref=rv_obj.id).pending_delta
                        tmpstring = tmpstring.replace("$$$", str(tutoramnt))
                    tmpstring = tmpstring.replace("###", str(tuteeamnt))
                    history_dict = {
                        'comment': tmpstring,
                        'reg_date': (i.created_time + timedelta(hours=utc_time)).strftime(
                            '%B %d %H:%M'
                        )
                    }
                elif i.history_text_id == 35:
                    tmpstring = str(i.history_text.history_english)
                    if "$$$" in tmpstring:
                        # this means some has been returned to the tutor and some to the tutee
                        tutoramnt = LessonClaim.objects.filter(lesson_reservation_id=rv_obj.id).order_by('created_time')[0].resolve_tc
#                         tutoramnt = TutorWalletHistory.objects.get(type=5, type_ref=rv_obj.id).pending_delta
                        tmpstring = tmpstring.replace("$$$", str(tutoramnt))
                    history_dict = {
                        'comment': tmpstring,
                        'reg_date': (i.created_time + timedelta(hours=utc_time)).strftime(
                            '%B %d %H:%M'
                        )
                    }
                else:
                    history_dict = {
                        'comment': i.history_text.history_english,
                        'reg_date': (i.created_time + timedelta(hours=utc_time)).strftime(
                            '%B %d %H:%M'
                        )
                    }
                history_list.append(history_dict)

            rv_dict['histories'] = history_list
        except Exception as err:
            print '@@@@@get_reservation_detail : ', str(err)
            rv_dict = {}
        finally:
            return rv_dict

    @staticmethod
    def get_tutor_package_lesson(tutor_id, utc_time=0, status=None, page=1, filter_type=None,
                               search_name=None):
        """
        @summary: 튜터 강의 신청 받은 package 내역
        @param tutor_id: 튜터 id
        @param utc_time: utc time 정보
        @param status: 강의 예약 타입
        @param page: page 번호
        @param filter_type: 강의 리스트 필터
        @param search_name: 검색 이름
        @return: 강의예약 리스트
        """
        reservation_list = []
        result = {}
        print "filter_type"
        print filter_type
        try:
#             queryset = LessonReservation.objects.select_related(
#                 'lesson', 'lesson__course', 'tutee'
#             ).filter(tutor_id=tutor_id)

            queryset = Lesson.objects.filter(tutor_id=tutor_id, pkg_times__gte=2)
            if status:
                if type(status) is int:
                    queryset = queryset.filter(lesson_status=status)
                else:
                    queryset = queryset.filter(lesson_status__in=status)
            else:
                if search_name:
                    queryset = queryset.filter(tutee__name__contains=search_name)

            paginator = Paginator(queryset, 10)
            page_item = paginator.page(page)
            page_list = paginator.page_range

            for pkg in page_item.object_list:
                pkg_dict = {
                    'pkg_id': pkg.id,
                    'tutor_id': pkg.tutor_id,
                    'tutee_id': pkg.tutee_id,
                    'package_number': ReservationService.set_package_id(pkg.id),
                    'package_title': pkg.course.course_name,
                    'orders': pkg.pkg_times,
                }
                pkg_dict['exp_data'] = pkg.expired_time.strftime('%Y-%m-%d')

                pkg_dict['name'] = pkg.tutee.name
                pkg_dict['tutee_image'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                    pkg.tutee.photo_filename)

                status_list = {}
                status_list['Not_scheduled'] = LessonReservation.objects.filter(
                                lesson_id=pkg.id, lesson_status=9999).count()
                status_list['Requested'] = LessonReservation.objects.filter(
                                lesson_id=pkg.id, lesson_status=1).count()
                status_list['Scheduled'] = LessonReservation.objects.filter(
                                lesson_id=pkg.id, lesson_status=2).count()
                pkg_dict['status'] = status_list

                if LessonReservation.objects.filter(lesson_id=pkg.id, lesson_status=7).exists():
                    pkg_dict['pkg_status'] = 'Completed'
                elif LessonReservation.objects.filter(lesson_id=pkg.id, lesson_status=9).exists():
                    pkg_dict['pkg_status'] = 'Refunded'
                elif LessonReservation.objects.filter(lesson_id=pkg.id, lesson_status=10).exists():
                    pkg_dict['pkg_status'] = 'Expired'
                else:
                    pkg_dict['pkg_status'] = 'In progress'
                
#                 year_month, now_year, now_month, now_day = ScheduleService.get_date()
#                 pkg_dict['today'] = str(now_year)+"-"+str(now_month)+"-"+str(now_day)

                reservation_list.append(pkg_dict)

            result['total'] = paginator.num_pages
            result['entries'] = reservation_list
            result['page'] = page
            result['pgGroup'] = CommonService.paging_list(page_list, page, 5)
        except Exception as err:
            print '@@@@@get_tutor_package_lesson : ', str(err)
            result = {}
        finally:
            return result

    @staticmethod
    def get_tutee_package_lesson(tutee_id, utc_time=0, status=None, page=1, filter_type=None,
                               search_name=None):
        """
        @summary: 튜터 강의 신청 받은 package 내역
        @param tutor_id: 튜터 id
        @param utc_time: utc time 정보
        @param status: 강의 예약 타입
        @param page: page 번호
        @param filter_type: 강의 리스트 필터
        @param search_name: 검색 이름
        @return: 강의예약 리스트
        """
        reservation_list = []
        result = {}
        try:
            queryset = Lesson.objects.filter(tutee_id=tutee_id, pkg_times__gte=2)

            if status:
                if type(status) is int:
                    queryset = queryset.filter(lesson_status=status)
                else:
                    queryset = queryset.filter(lesson_status__in=status)
            else:
                if search_name:
                    queryset = queryset.filter(tutor__name__contains=search_name)

            paginator = Paginator(queryset, 10)
            page_item = paginator.page(page)
            page_list = paginator.page_range

            for pkg in page_item.object_list:
                pkg_dict = {
                    'pkg_id': pkg.id,
                    'tutor_id': pkg.tutor_id,
                    'tutee_id': pkg.tutee_id,
                    'package_number': ReservationService.set_package_id(pkg.id),
                    'package_title': pkg.course.course_name,
                    'orders': pkg.pkg_times,
                }
                pkg_dict['exp_data'] = pkg.expired_time.strftime('%Y-%m-%d')

                pkg_dict['name'] = pkg.tutor.name
                pkg_dict['tutee_image'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                    pkg.tutor.photo_filename)
                status_list = {}
                status_list['Not_scheduled'] = LessonReservation.objects.filter(
                                lesson_id=pkg.id, lesson_status=9999).count()
                status_list['Requested'] = LessonReservation.objects.filter(
                                lesson_id=pkg.id, lesson_status=1).count()
                status_list['Scheduled'] = LessonReservation.objects.filter(
                                lesson_id=pkg.id, lesson_status=2).count()
                pkg_dict['status'] = status_list
                if LessonReservation.objects.filter(lesson_id=pkg.id, lesson_status=7).exists():
                    pkg_dict['pkg_status'] = 'Completed'
                elif LessonReservation.objects.filter(lesson_id=pkg.id, lesson_status=9).exists():
                    pkg_dict['pkg_status'] = 'Refunded'
                elif LessonReservation.objects.filter(lesson_id=pkg.id, lesson_status=10).exists():
                    pkg_dict['pkg_status'] = 'Expired'
                else:
                    pkg_dict['pkg_status'] = 'In progress'
#                 year_month, now_year, now_month, now_day = ScheduleService.get_date()
#                 pkg_dict['today'] = str(now_year)+"-"+str(now_month)+"-"+str(now_day)

                reservation_list.append(pkg_dict)

            result['total'] = paginator.num_pages
            result['entries'] = reservation_list
            result['page'] = page
            result['pgGroup'] = CommonService.paging_list(page_list, page, 5)
        except Exception as err:
            print '@@@@@get_tutee_package_lesson : ', str(err)
            result = {}
        finally:
            return result

    @staticmethod
    def ordinal(num):
    # I'm checking for 10-20 because those are the digits that
    # don't follow the normal counting scheme. 
        if 10 <= num % 100 <= 20:
            suffix = 'th'
        else:
            # the second parameter is a default.
            suffix = SUFFIXES.get(num % 10, 'th')
        return str(num) + suffix

    @staticmethod
    def get_reservation_pkg_detail(user_id, reservation_id, utc_time):
        """
        @summary: Lesson Reservation detail
        @param user_id: 유저 id
        @param reservation_id: reservatoin id
        @param utc_time: utc time 정보
        @return: lesson reservation 정보
        """
        current_time = datetime.utcnow()
        rv_dict = {}
        utc_time = utc_time * (-1)
        try:
#             LessonHistory(lesson_reservation_id=113, history_text_id=3).save()
            pkgLesson = Lesson.objects.select_related('course', 'tutor', 'tutee').get(id=reservation_id)
            rv_dict['rvid'] = reservation_id
            rv_dict['package_id'] = ReservationService.set_package_id(reservation_id)
            rv_dict['lesson_name'] = pkgLesson.course.course_name
            rv_dict['duration'] = pkgLesson.pkg_min
            rv_dict['orders'] = pkgLesson.pkg_times
            rv_dict['pkg_price'] = pkgLesson.pkg_total_price
            rv_dict['expired'] = (pkgLesson.expired_time + timedelta(hours=utc_time)).strftime('%Y-%m-%d %H:%M:%S')

            if pkgLesson.tutor_id == user_id:
                rv_dict['is_tutor'] = True
            elif pkgLesson.tutee_id == user_id:
                rv_dict['is_tutor'] = False
            else:
                rv_dict['is_tutor'] = 'redirect'

            if not rv_dict['is_tutor'] == 'redirect':
                rv_dict['tutor_name'] = pkgLesson.tutor.name
                rv_dict['tutor_id'] = pkgLesson.tutor_id
                rv_dict['tutor_id_format'] = 'TU' + str('%011d' % int(pkgLesson.tutor_id))
                rv_dict['tutor_img'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                  pkgLesson.tutor.photo_filename)
                rv_dict['tutee_name'] = pkgLesson.tutee.name
                rv_dict['tutee_id'] = pkgLesson.tutee_id
                rv_dict['tutee_id_format'] = 'TE' + str('%011d' % int(pkgLesson.tutee_id))

                if getattr(pkgLesson.tutee, 'photo_filename'):
                    rv_dict['tutee_img'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                      pkgLesson.tutee.photo_filename)
                else:
                    rv_dict['tutee_img'] = None

            status_list = {}
            status_list['Not_scheduled'] = LessonReservation.objects.filter(
                            lesson_id=reservation_id, lesson_status=9999).count()
            status_list['Requested'] = LessonReservation.objects.filter(
                            lesson_id=reservation_id, lesson_status=1).count()
            status_list['Scheduled'] = LessonReservation.objects.filter(
                            lesson_id=reservation_id, lesson_status=2).count()
            rv_dict['status'] = status_list

            if pkgLesson.pkg_times is 5:
                expirationdelta = 90*24
            elif pkgLesson.pkg_times is 10:
                expirationdelta = 120*24
            elif pkgLesson.pkg_times is 20:
                expirationdelta = 150*24
            elif pkgLesson.pkg_times is 30:
                expirationdelta = 180*24
            firstLesson = LessonReservation.objects.get(lesson_id = reservation_id, pkg_order = 1)

            if current_time > datetime.strptime(str(firstLesson.created_time)[:19], '%Y-%m-%d %H:%M:%S') + timedelta(hours=expirationdelta):
                rv_dict['pkg_status'] = 'Expired'
                pkgLesson.pkg_status = 0
            elif LessonReservation.objects.filter(lesson_id=reservation_id, lesson_status=9).exists():
                rv_dict['pkg_status'] = 'Refunded'
                pkgLesson.pkg_status = 1
            elif LessonReservation.objects.filter(lesson_id=reservation_id, lesson_status=7).exists():
                rv_dict['pkg_status'] = 'Completed'
                pkgLesson.pkg_status = 2
            elif LessonReservation.objects.filter(lesson_id = reservation_id, pkg_order=1, lesson_status=9999).exists():
                rv_dict['pkg_status'] = 'Canceled'
                pkgLesson.pkg_status = 3
            else:
                rv_dict['pkg_status'] = 'In progress'
                pkgLesson.pkg_status = 4
            print "Package Status is " + rv_dict['pkg_status']
            pkgLesson.save()
            queryset = LessonReservation.objects.select_related(
                    'lesson', 'course', 'tutor', 'tutee',
                    'tutor__from_country_id', 'tutee__from_country_id'
                ).filter(lesson_id=reservation_id)

            if rv_dict['pkg_status'] is 'In progress':
                if rv_dict['is_tutor']: 
                    rv_dict['pkg_status_string'] = LessonReservationStatusText.objects.get(id = 80).status_english % {'exp_date': pkgLesson.expired_time.strftime('%Y-%m-%d %H:%M:%S')}
                else:
                    rv_dict['pkg_status_string'] = LessonReservationStatusText.objects.get(id = 86).status_english % {'exp_date': pkgLesson.expired_time.strftime('%Y-%m-%d %H:%M:%S')}
            elif rv_dict['pkg_status'] is 'Canceled':
                if rv_dict['is_tutor']:
                    rv_dict['pkg_status_string'] = LessonReservationStatusText.objects.get(id = 81).status_english
                else:
                    rv_dict['pkg_status_string'] = LessonReservationStatusText.objects.get(id = 87).status_english
            elif rv_dict['pkg_status'] is 'Completed':
                rv_dict['pkg_status_string'] = LessonReservationStatusText.objects.get(id = 82).status_english
            elif rv_dict['pkg_status'] is 'Expired':
                rv_dict['pkg_status_string'] = LessonReservationStatusText.objects.get(id = 85).status_english
            pkg_list = []
            reserve_schedule = False
            first = True
            pkg_history_list = []
            num_of_lessons = queryset[0].lesson.pkg_times # number of lessons to calculate how many lessons get refunded
            refunded = False
            for lesson in queryset:
                # for every lesson inside the package:
                # 1. check if they are 24 hours or less till the rv_time
                # 2. in progress, need confirmation, problem reported, in dispute
                lessonStatus = lesson.lesson_status
                if first:
                    ############# FIRST LESSON OF THE PACKAGE ##################
                    if lesson.rv_time:
                        reservation_time = datetime.strptime(
                            str(lesson.rv_time[:8]) + str(lesson.rv_time[9:]),
                            '%Y%m%d%H%M'
                        )
                        pkg_list_dict = {
                        'start_date': (reservation_time + timedelta(hours=utc_time))
                        .strftime('%Y-%m-%d, %H:%M:%S'),
                        'reservation_number': ReservationService.set_lesson_id(lesson.id),
                        'order': lesson.pkg_order,
                        'lesson_status': ReservationService.RESERVATION_STATUS[lesson.lesson_status],
                        'lesson_id': lesson.id
                        }
                        # need to add that the request was sent
                        pkg_history_dict = {
                            'comment': str(LessonHistoryText.objects.get(id = 2).history_english),
                            'reg_date': (lesson.created_time + timedelta(hours=utc_time)).strftime(
                                '%B %d %H:%M'
                            )
                        }
                        pkg_history_list.append(pkg_history_dict)

                        if lessonStatus not in (1, 9999):
                            ## tutor accepted the package request - ONLY FOR FIRST LESSON OF PACKAGE
                            pkg_history_dict = {
                                'comment': str(LessonHistoryText.objects.get(id = 36).history_english),
                                'reg_date': (lesson.created_time + timedelta(hours=utc_time)).strftime(
                                    '%B %d %H:%M'
                                )
                            }
                            pkg_history_list.append(pkg_history_dict)

                            if lessonStatus == 7:
                                # confirmed - FIRST LESSON
                                tmpstring = str(LessonHistoryText.objects.get(id = 38).history_english)
                                tmpstring = tmpstring.replace("###", "1st")
                                pkg_history_dict = {
                                    'comment': tmpstring,
                                    'reg_date': (lesson.updated_time + timedelta(hours=utc_time)).strftime(
                                        '%B %d %H:%M'
                                    )
                                }
                                pkg_history_list.append(pkg_history_dict)
                                num_of_lessons -= 1
                            elif lessonStatus == 9:
                                # refunded
                                # need to find out who requested the refund
                                refunded = True
                                lastLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id = lesson.id).order_by('-created_time')[0]
                                if lastLessonClaim.reportor_id == pkgLesson.tutor_id:
                                    # tutor requested the refund - the number of refunds is the same as the size of package
                                    tmpstring = str(LessonHistoryText.objects.get(id = 40).history_english)
                                    tmpstring = tmpstring.replace("X", str(num_of_lessons))
                                    pkg_history_dict = {
                                        'comment': tmpstring,
                                        'reg_date': (lesson.updated_time + timedelta(hours=utc_time)).strftime(
                                            '%B %d %H:%M'
                                        )
                                    }
                                    pkg_history_list.append(pkg_history_dict)
                                else:
                                    # tutee requested the refund - the number of refunds is the same as the size of package
                                    tmpstring = str(LessonHistoryText.objects.get(id = 42).history_english)
                                    tmpstring = tmpstring.replace("X", str(num_of_lessons))
                                    pkg_history_dict = {
                                        'comment': tmpstring,
                                        'reg_date': (lesson.created_time + timedelta(hours=utc_time)).strftime(
                                            '%B %d %H:%M'
                                        )
                                    }
                                    pkg_history_list.append(pkg_history_dict)
                                num_of_lessons -= 1
                            elif lessonStatus == 2:
                                # need to check if the lesson is in time 
                                if reservation_time >= current_time + timedelta(hours=24):
                                    pass
                                else:
                                    # the class should not be considered for refund
                                    num_of_lessons -= 1
                        elif lessonStatus == 9999:
                            # tutor declined the package request
                            pkg_history_dict = {
                                'comment': str(LessonHistoryText.objects.get(id = 37).history_english),
                                'reg_date': (lesson.updated_time + timedelta(hours=utc_time)).strftime(
                                    '%B %d %H:%M'
                                )
                            }
                            pkg_history_list.append(pkg_history_dict)
                    else:
                        ## Shouldn't come here for the first lesson because it is mandatory that you 
                        ## set the time for the first lesson of the package
                        if reserve_schedule is False:
                            pkg_list_dict = {
                                'reserve_schedule': True,
                                'order': lesson.pkg_order,
                                'pkg_comment': "Tutee will choose a date and time for this lesson.",
                                'lesson_id': lesson.id
                            }
                        else:
                            pkg_list_dict = {
                                'reserve_schedule': False,
                                'order': lesson.pkg_order,
                                'pkg_comment': "Tutee will choose a date and time for this lesson.",
                                'lesson_id': lesson.id
                            }
                        reserve_schedule = True
                else:
                    if refunded:
                        break
                    else:
                        ############ NOT THE FIRST LESSON
                        ############ all the other lessons after the first
                        if lesson.rv_time:
                            reservation_time = datetime.strptime(
                                str(lesson.rv_time[:8]) + str(lesson.rv_time[9:]),
                                '%Y%m%d%H%M'
                            )
                            pkg_list_dict = {
                            'start_date': (reservation_time + timedelta(hours=utc_time))
                            .strftime('%Y-%m-%d, %H:%M:%S'),
                            'reservation_number': ReservationService.set_lesson_id(lesson.id),
                            'order': lesson.pkg_order,
                            'lesson_status': ReservationService.RESERVATION_STATUS[lesson.lesson_status],
                            'lesson_id': lesson.id
                            }
                            #######################################
                            if lessonStatus not in (1, 9999):
                                if lessonStatus == 7:
                                    # confirmed
                                    package = Lesson.objects.get(id = lesson.lesson_id)
                                    if lesson.pkg_order is package.pkg_times:
                                        tmpstring = LessonHistoryText.objects.get(id = 39).history_english
                                    else:
                                        tmpstring = LessonHistoryText.objects.get(id = 38).history_english
                                    ordinalString = ReservationService.ordinal(lesson.pkg_order)
                                    print tmpstring
                                    tmpstring = tmpstring.replace("###", str(ordinalString))
#                                     print tmpstring
#                                     print ordinalString
                                    pkg_history_dict = {
                                        'comment': tmpstring,
                                        'reg_date': (lesson.created_time + timedelta(hours=utc_time)).strftime(
                                            '%B %d %H:%M'
                                        )
                                    }
                                    pkg_history_list.append(pkg_history_dict)
                                    num_of_lessons -= 1
                                elif lessonStatus == 9:
                                    # refunded
                                    # need to find out who requested the refund
                                    refunded = True
                                    lastLessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id=lesson.id).order_by('-created_time')[0]
                                    if lastLessonClaim.reportor_id == pkgLesson.tutor_id:
                                        # tutor requested the refund - the number of refunds is the same as remainder of package
                                        tmpstring = LessonHistoryText.objects.get(id = 40).history_english
                                        tmpstring = tmpstring.replace("X", str(num_of_lessons))
                                        pkg_history_dict = {
                                            'comment': tmpstring,
                                            'reg_date': (lesson.created_time + timedelta(hours=utc_time)).strftime(
                                                '%B %d %H:%M'
                                            )
                                        }
                                        pkg_history_list.append(pkg_history_dict)
                                    else:
                                        # tutee requested the refund - the number of refunds is the same as remainder of package
                                        tmpstring = LessonHistoryText.objects.get(id = 42).history_english
                                        tmpstring = tmpstring.replace("X", str(num_of_lessons))
                                        pkg_history_dict = {
                                            'comment': tmpstring,
                                            'reg_date': (lesson.created_time + timedelta(hours=utc_time)).strftime(
                                                '%B %d %H:%M'
                                            )
                                        }
                                        pkg_history_list.append(pkg_history_dict)
                                        num_of_lessons -= 1
                                elif lessonStatus == 2:
                                    # need to check if the lesson is in time 
                                    if reservation_time >= current_time + timedelta(hours=24):
                                        pass
                                    else:
                                        # the class should not be considered for refund
                                        num_of_lessons -= 1
                            elif lessonStatus == 9999:
                                # tutor declined the package request
                                pkg_history_dict = {
                                    'comment': str(LessonHistoryText.objects.get(id = 37).history_english),
                                    'reg_date': (lesson.updated_time + timedelta(hours=utc_time)).strftime(
                                        '%B %d %H:%M'
                                    )
                                }
                                pkg_history_list.append(pkg_history_dict)
                        else:
                            ### no reserve_time
                            if reserve_schedule is False:
                                pkg_list_dict = {
                                    'reserve_schedule': True,
                                    'order': lesson.pkg_order,
                                    'pkg_comment': "Tutee will choose a date and time for this lesson.",
                                    'lesson_id': lesson.id
                                }
                            else:
                                pkg_list_dict = {
                                    'reserve_schedule': False,
                                    'order': lesson.pkg_order,
                                    'pkg_comment': "Tutee will choose a date and time for this lesson.",
                                    'lesson_id': lesson.id
                                }
                            reserve_schedule = True

                first = False
                pkg_list.append(pkg_list_dict)

                tool_list = CommonService.tool_list()
                rv_dict['tool_id'] = lesson.messenger_type
                rv_dict['tool_name'] = tool_list[lesson.messenger_type]['name']
                rv_dict['tutee_tool_id'] = lesson.messenger_id
                if lesson.messenger_type == 1:
                    rv_dict['tutor_tool_id'] = lesson.tutor.skype_id
                elif lesson.messenger_type == 2:
                    rv_dict['tutor_tool_id'] = lesson.tutor.hangout_id
                elif lesson.messenger_type == 3:
                    rv_dict['tutor_tool_id'] = lesson.tutor.facetime_id
                else:
                    rv_dict['tutor_tool_id'] = lesson.tutor.qq_id
                '''    
                history = lesson.lessonhistory_set.select_related(
                    'history_text').all().order_by('created_time')
                history_list = []
                for i in history:
                    history_dict = {
                        'comment': i.history_text.history_english,
                        'reg_date': (i.created_time + timedelta(hours=utc_time)).strftime(
                            '%B %d %H:%M'
                        )
                    }
                    history_list.append(history_dict)

                rv_dict['histories'] = history_list
                '''
            rv_dict['pkg_list'] = pkg_list
            rv_dict['pkg_histories'] = pkg_history_list
            print rv_dict['pkg_histories']
        except Exception as err:
            print '@@@@@get_reservation_pkg_detail : ', str(err)
            rv_dict = {}
        finally:
            return rv_dict

    # Feedback 목록
    @staticmethod
    def getFeedbackList(self, _user_id, page=1):
        """
        @summary: exchange get feedback list
        @author: khyun
        @param _user_id : user id
        @param page : page number
        @return: object data
        """
        feedbackList = []
        try:
            per_page = 5
            start = ( page - 1 ) * per_page
            end = start + per_page
            totalCount = 0
            query = 'SELECT T1.revid, COUNT(*) AS count FROM ( SELECT *, COUNT(*) AS count FROM feedback WHERE tutee_id = %s GROUP BY tutor_id ) T1'
            for p in LessonFeedback.objects.raw( query, [_user_id] ) :
                totalCount = p.count
            print totalCount
            pg = TutorService( object ).paging( page, totalCount )
            query = 'SELECT *, COUNT(*) AS count FROM feedback WHERE tutee_id = %s GROUP BY tutor_id ORDER BY created_date DESC LIMIT %s, %s'
            tutors = LessonFeedback.objects.raw( query, [ _user_id, start, end ])
            userTimezone = TellpinUser.objects.get( id = _user_id ).timezone
            userUTC = TimezoneInfo.objects.get( id = userTimezone ).utc_time
            userTime = int( userUTC[:3] )
            if userUTC[4] == '3' :
                if userUTC[0] == '-':
                    userTime -= 0.5
                else:
                    userTime += 0.5
            print 'utc', userTime
            #tutors = Feedback.objects.filter( tutee_id = _user_id ).values( 'tutor_id' ).order_by( '-created_date' ).annotate( count = Count('tutor_id'))[ start : end ]
            for tutor in tutors :
                obj = {}
                user = TellpinUser.objects.get( id = tutor.tutor_id )
                obj['revid'] = tutor.revid
                obj['tutor_id'] = tutor.tutor_id
                obj['tutor_name'] = user.name
                obj['profile_photo'] = self.PROFILE_IMAGE_FILE_DIR + '/' + user.profile_photo
                obj['tutee_id'] = tutor.tutee_id
                obj['lid'] = tutor.tutee_id
                obj['comment'] = tutor.comment
                obj['created_date'] = tutor.created_date + relativedelta( hours = userTime )
                obj['count'] = tutor.count
                obj['moreList'] = []
                feedbackList.append( obj )
            result = {
                'total' : totalCount,
                'item' : feedbackList,
                'page' : page,
                'startPage' : pg['startPage'],
                'endPage' : pg['endPage'],
                'has_prev' : pg['has_prev'],
                'hast_next' : pg['has_next'],
                'last_page' : pg['last_page'],
                'page_range' : range( pg['startPage'], pg['endPage'] + 1 )
            }
        except Exception as e:
            result = {}
            result['message'] = str(e)
        finally:
            return result

    # 강의 status 처리
    @staticmethod
    def lesson_status_commit(user_id, _rvid, status, action, data=None, new_date=None):
        """
        @summary: 강의 상태 처리
        @author: msjang, shkim
        @param _rvid: 강의 예약 id
        @param status: 강의 상태
        @param action: 튜터/튜티가 취한 행동
        @param _data: 강의정보
        @return: True, False( boolean )
        """
        rv = LessonReservation.objects.select_related('lesson', 'course', 'tutee', 'tutor').get(id=_rvid)
#         lh = LessonHistory.objects.select_related('history_text', 'lesson_reservation').get(lesson_reservation = _rvid)
        comment_id = 0
        rvid = int(_rvid)
        current_time = datetime.utcnow()
        print "<<LESSON_STATUS_COMMIT>>"
        print "CURRENT LESSON STATUS IS " + status
        print "RECEIVED ACTION IS " + action
        if status == "Requested":
            # history text = Tutee sent a lesson request
            if action == "Change date/time":
                # no need for history
                return True
            elif action == "Decline":
                if rv.lesson.pkg_times == 1:
                    # single lesson -> set status to cancelled
                    # Tutor declined the lesson request.
                    LessonHistory(history_text_id = 22, lesson_reservation_id = rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 22)
                    rv.lesson_status = 8
                    tmpprice = rv.lesson.sg_price
                    if tmpprice is  0:
                        # package lesson
                        tmpprice = rv.lesson.pkg_price
                    WalletService.setRefund(tmpprice, rv.tutor_id, rv.tutee_id, rvid)
                else:
                    # package lesson
                    # need to check if this is the first lesson
                    if rv.pkg_order == 1:
                        # the first lesson of the pkg
                        LessonHistory(history_text_id = 37, lesson_reservation_id = rvid).save()
                        TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 37)
                        # need to change the status of the package
                        rv.lesson.pkg_status = 3
                        rv.lesson.save()
                    else:
                        rv.lesson.decline_time += 1
                        LessonHistory(history_text_id = 22, lesson_reservation_id = rvid).save()
                        TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 22)
                    rv.lesson_status = 9999
                    rv.rv_time = None
            elif action == "Approve":
                # change status to Approve/Upcoming
                if rv.lesson.pkg_times == 1:
                    # single lesson
                    # The lesson is scheduled.
                    LessonHistory(history_text_id=3, lesson_reservation_id = rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 3)
                else:
                    # package lesson
                    # Tutor accepted the package request.
                    if rv.pkg_order == 1:
                        rv.lesson.pkg_status = 4
                        rv.lesson.save()
                    LessonHistory(history_text_id = 36, lesson_reservation_id = rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 36)
                rv.lesson_status = 2
            elif action == "Cancel":
                # change status to Canceled
                # Tutee cancelled the lesson request.
                LessonHistory(history_text_id = 20, lesson_reservation_id = rvid).save()
                TutorService.set_lesson_notification(1, rv.tutee_id, rv.tutor_id, rvid, 20)
                rv.lesson_status = 8
            rv.save()
            return True
        elif status == "Approved/Upcoming":
            if action == "Change date/time":
                rv.lesson_status = 22 # Approved/Upcoming - change date/time
                if rv.tutor_id == user_id:
                    # if the user is the tutor
                    LessonHistory(history_text_id = 4, lesson_reservation_id = rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 4)
                    LessonMessage(message = data['comment'], msg_type = 1, lesson_reservation_id = rvid, sender_id = user_id, receiver_id = rv.tutee_id).save()
                else:
                    # if the user is the tutee of the lesson
                    LessonHistory(history_text_id = 5, lesson_reservation_id = rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutee_id, rv.tutor_id, rvid, 5)
                    LessonMessage(message = data['comment'], msg_type = 1, lesson_reservation_id = rvid, sender_id = user_id, receiver_id = rv.tutor_id).save()
                LessonReservationTemp(id = rv.id, lesson_id = rv.lesson_id, course_id = rv.course_id, tutor_id = rv.tutor_id, tutee_id = rv.tutee_id, 
                                        pkg_order = rv.pkg_order, rv_time = new_date, messenger_type = rv.messenger_type, messenger_id = rv.messenger_id,
                                        message = rv.message, lesson_status = rv.lesson_status, ticket_status = rv.ticket_status, expired_time = rv.expired_time,
                                        reservation_status_text_id = rv.reservation_status_text_id, del_by_user = rv.del_by_user, del_by_admin = rv.del_by_admin).save()
            elif action == "Canceled":
                # single lessons only - only the tutee can do this action
                # tutee requested to cancel the lesson.
                rv.lesson_status = 23
                LessonHistory(history_text_id = 11, lesson_reservation_id = rvid).save()
                TutorService.set_lesson_notification(1, rv.tutee_id, rv.tutor_id, rvid, 11)
                LessonMessage(message = data['comment'], msg_type = 1, lesson_reservation_id = rvid, sender_id = user_id, receiver_id = rv.tutor_id).save()

            elif action == "Request to change":
                rv.lesson_status = 22
                if rv.tutor_id == user_id:
                    LessonHistory(history_text_id = 4, lesson_reservation_id = rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 4)
                    LessonMessage(message = data['comment'], msg_type = 1, lesson_reservation_id = rvid, sender_id = user_id, receiver_id = rv.tutee_id).save()
                else:
                    LessonHistory(history_text_id = 5, lesson_reservation_id = rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutee_id, rv.tutor_id, rvid, 5)
                    LessonMessage(message = data['comment'], msg_type = 1, lesson_reservation_id = rvid, sender_id = user_id, receiver_id = rv.tutor_id).save()
                LessonReservationTemp(id = rv.id, lesson_id = rv.lesson_id, course_id = rv.course_id, tutor_id = rv.tutor_id, tutee_id = rv.tutee_id, 
                                        pkg_order = rv.pkg_order, rv_time = new_date, messenger_type = rv.messenger_type, messenger_id = rv.messenger_id,
                                        message = rv.message, lesson_status = rv.lesson_status, ticket_status = rv.ticket_status, expired_time = rv.expired_time,
                                        reservation_status_text_id = rv.reservation_status_text_id, del_by_user = rv.del_by_user, del_by_admin = rv.del_by_admin).save()
            rv.save()
            return True
        elif status == "Approved/Upcoming - Change date/time":
            if action == "Accept":
                # lesson is changed
                LessonHistory(history_text_id = 6, lesson_reservation_id = rvid).save()
                lr = LessonReservationTemp.objects.get(id = rv.id)
                rv.rv_time = lr.rv_time
                rv.save()
                LessonReservationTemp.objects.get(id = rv.id).delete()
                rv.lesson_status = 2
                lastLessonRequest = LessonMessage.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv.id, msg_type = 1).order_by('-created_time')[0]
                if lastLessonRequest.sender_id == rv.tutor_id:
                    TutorService.set_lesson_notification(1, rv.tutee_id, rv.tutor_id, rvid, 6)
                else:
                    TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 6)
            elif action == "Decline":
                lastLessonRequest = LessonMessage.objects.select_related('lesson_reservation').filter(lesson_reservation_id = rv.id, msg_type = 1).order_by('-created_time')[0]
                if lastLessonRequest.sender_id == rv.tutor_id:
                    # tutor sent the request, but the action was decline - tutee declined the request
                    LessonHistory(history_text_id = 7, lesson_reservation_id = rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutee_id, rv.tutor_id, rvid, 7)
                else:
                    LessonHistory(history_text_id = 8, lesson_reservation_id = rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 8)
                rv.lesson_status = 2
            elif action == "Undo request":
                if rv.tutor_id == user_id:
                    # if the user is the tutor
                    LessonHistory(history_text_id = 15, lesson_reservation_id = rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 15)
                    LessonMessage(message = "Tutor undid the request.", msg_type = 1, lesson_reservation_id = rvid, sender_id = user_id, receiver_id = rv.tutee_id).save()
                else:
                    # if the user is the tutee of the lesson
                    LessonHistory(history_text_id = 16, lesson_reservation_id = rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutee_id, rv.tutor_id, rvid, 16)
                    LessonMessage(message = "Tutee undid the request.", msg_type = 1, lesson_reservation_id = rvid, sender_id = user_id, receiver_id = rv.tutor_id).save()
                LessonReservationTemp.objects.get(id = rv.id).delete()
                rv.lesson_status = 2
            elif action == "Edit request":
                return True
            elif action == "Request to change":
                if rv.tutor_id == user_id:
                    LessonHistory(history_text_id = 4, lesson_reservation_id = rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 4)
                    LessonMessage(message = data['comment'], msg_type = 1, lesson_reservation_id = rvid, sender_id = user_id, receiver_id = rv.tutee_id).save()
                else:
                    LessonHistory(history_text_id = 5, lesson_reservation_id = rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutee_id, rv.tutor_id, rvid, 5)
                    LessonMessage(message = data['comment'], msg_type = 1, lesson_reservation_id = rvid, sender_id = user_id, receiver_id = rv.tutor_id).save()
                if LessonReservationTemp.objects.filter(id = rv.id, lesson_id = rv.lesson_id).exists():
                    lrt = LessonReservationTemp.objects.filter(id = rv.id, lesson_id = rv.lesson_id).order_by('-created_time')[0]
                    lrt.rv_time = new_date
                    lrt.save()
                else:
                    LessonReservationTemp(id = rv.id, lesson_id = rv.lesson_id, course_id = rv.course_id, tutor_id = rv.tutor_id, tutee_id = rv.tutee_id, 
                                        pkg_order = rv.pkg_order, rv_time = new_date, messenger_type = rv.messenger_type, messenger_id = rv.messenger_id,
                                        message = rv.message, lesson_status = rv.lesson_status, ticket_status = rv.ticket_status, expired_time = rv.expired_time,
                                        reservation_status_text_id = rv.reservation_status_text_id, del_by_user = rv.del_by_user, del_by_admin = rv.del_by_admin).save()
            rv.save()
            return True
        elif status == "Approved/Upcoming - Cancel":
            if action == "Accept":
                LessonHistory(history_text_id = 21, lesson_reservation_id = rvid).save()
                TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 21)
                rv.lesson_status = 8
            elif action == "Decline":
                LessonHistory(history_text_id = 12, lesson_reservation_id = rvid).save()
                TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 12)
                rv.lesson_status = 2
            elif action == "Undo request":
                if rv.tutor_id == user_id:
                    # if the user is the tutor
                    LessonHistory(history_text_id = 15, lesson_reservation_id = rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 15)
                    LessonMessage(message = "Tutor undid the request.", msg_type = 1, lesson_reservation_id = rvid, sender_id = user_id, receiver_id = rv.tutee_id).save()
                else:
                    # if the user is the tutee of the lesson
                    LessonHistory(history_text_id = 16, lesson_reservation_id = rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutee_id, rv.tutor_id, rvid, 16)
                    LessonMessage(message = "Tutee undid the request.", msg_type = 1, lesson_reservation_id = rvid, sender_id = user_id, receiver_id = rv.tutor_id).save()
                rv.lesson_status = 2
            rv.save()
            return True

        elif status == "Problem reported":
            # need to find out when the claim was first reported
            # the claim will hold the desired resolve type
            firstLessonClaim = LessonClaim.objects.filter(lesson_reservation_id=rvid).order_by('created_time')[0]
#             firstLessonClaim = LessonClaim.objects.select_related('lesson_reservation').get(id = rv.id).order_by('created_time')[0]
            if action == "Accept":
                # get the claim type and resolve type
                if rv.lesson.sg_min is not None:#single lesson
                    refund_tc = rv.lesson.sg_price
                else:#pkg_lesson
                    refund_tc = rv.lesson.pkg_price
                firstLessonClaim.status = 2
                if firstLessonClaim.claim_text_id == 1:
                    # tutor was absent
                    if firstLessonClaim.resolve_type == 4:
                        # return all credits of the lesson fee to the tutee
                        LessonHistory(history_text_id=18, lesson_reservation_id=rvid).save()
                        WalletService.setRefund(refund_tc, rv.tutor_id, rv.tutee_id, rvid)
                        rv.lesson_status = 9
                    elif firstLessonClaim.resolve_type == 5:
                        # reschedule the lesson
                        LessonHistory(history_text_id=17, lesson_reservation_id=rvid).save()
                        rv.lesson_status = 2
                elif firstLessonClaim.claim_text_id == 2:
                    # tutee was absent
                    if firstLessonClaim.resolve_type == 6:
                        LessonHistory(history_text_id=35, lesson_reservation_id=rvid).save()
                        rv.lesson_status = 7
                        WalletService.setConfirm(refund_tc, rv.tutor_id, rv.tutee_id, rvid)
                    elif firstLessonClaim.resolve_type == 5:
                        LessonHistory(history_text_id = 17, lesson_reservation_id = rvid).save()
                        rv.lesson_status = 2
                elif firstLessonClaim.claim_text_id == 3:
                    # other
                    if firstLessonClaim.resolve_type == 9:#tutor confirm
                        LessonHistory(history_text_id=35, lesson_reservation_id=rvid).save()
                        rv.lesson_status = 7
                        WalletService.setConfirm(refund_tc, rv.tutor_id, rv.tutee_id, rvid)
                    elif firstLessonClaim.resolve_type == 7:#부분환불
                        LessonHistory(history_text_id=19, lesson_reservation_id=rvid).save()
                        # need to know how much is split
                        rv.lesson_status = 9
                        tmpprice = rv.lesson.sg_price
                        if tmpprice is 0:
                            # package lesson
                            tmpprice = rv.lesson.pkg_price
                        if rv.tutor_id == user_id:
                            # tutor is want splitting the refund
                            WalletService.setRefund(firstLessonClaim.resolve_tc, rv.tutor_id, rv.tutee_id, rvid)
                        else:
                            WalletService.setRefund(tmpprice-firstLessonClaim.resolve_tc, rv.tutor_id, rv.tutee_id, rvid)
                    elif firstLessonClaim.resolve_type == 8:#tutee refund
                        LessonHistory(history_text_id = 18, lesson_reservation_id = rvid).save()
                        WalletService.setRefund(refund_tc, rv.tutor_id, rv.tutee_id, rvid)
                        rv.lesson_status = 9
                    elif firstLessonClaim.resolve_type == 5:
                        LessonHistory(history_text_id=17, lesson_reservation_id=rvid).save()
                        rv.lesson_status = 2
                firstLessonClaim.save()
                rv.save()
                return True
            elif action == "problem_statement":#SOO
                if current_time < datetime.strptime(str(firstLessonClaim.created_time)[:19], '%Y-%m-%d %H:%M:%S') + timedelta(hours=72):
                    if user_id != firstLessonClaim.reportor_id:
                        rv.lesson_status = 4#lesson status in disput
                        rv.save()
                        LessonHistory(history_text_id=30, lesson_reservation_id=rvid).save()
                    LessonClaim(status=1, claim_detail=data['claim_detail'],
                                lesson_reservation_id=rvid, course_id=rv.course_id,
                                reportee_id=data['claim_reportee_id'],
                                reportor_id=data['claim_reportor_id'],
                                claim_text_id=3
                                ).save()

#                     if rv.tutor_id == user_id:
#                         # if the user is the tutor
#                         LessonHistory(history_text_id=28, lesson_reservation_id=rvid).save()
#                     else:
#                         # if the user is the tutee of the lesson
#                         LessonHistory(history_text_id=29, lesson_reservation_id=rvid).save()
#             elif action == "Edit statement":
#                 pass
#             elif action == "Make statement":
#                 if current_time < datetime.strptime(str(firstLessonClaim.created_time)[:19], '%Y-%m-%d %H:%M:%S') + timedelta(hours=72):
#                     if rv.tutor_id == user_id:
#                         # if the user is the tutor
#                         LessonHistory(history_text_id=28, lesson_reservation_id=rvid).save()
#                     else:
#                         # if the user is the tutee of the lesson
#                         LessonHistory(history_text_id=29, lesson_reservation_id=rvid).save()
            elif action == "Undo report":
                if rv.tutor_id == user_id:
                    # if the user is the tutor
                    LessonHistory(history_text_id=26, lesson_reservation_id=rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 26)
                else:
                    # if the user is the tutee of the lesson
                    LessonHistory(history_text_id=27, lesson_reservation_id=rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutee_id, rv.tutor_id, rvid, 27)
                rv.lesson_status = 6
            # if the time is still valid
            # check the type of the claim and change the history text accordingly
#             lh.save()
            rv.save()
            return True
        #################### REVISED LINE #########################
        elif status == "Need confirmation":
            # tutor or tutee reports a problem -> set status to Problem reported
            # make new entry in LessonClaim
            if action == "Report a problem":
                if rv.lesson.sg_min is not None:#single lesson
                    refund_tc = rv.lesson.sg_price
                else:#pkg_lesson
                    refund_tc = rv.lesson.pkg_price
                print refund_tc
                if rv.tutor_id == user_id:#tutor
                    # I am the tutor of this course
                    LessonHistory(history_text_id=20, lesson_reservation_id=rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 20)
                    if data['claim_text'] == '1':
                        if data['resolve_type'] == '4':
                            rv.lesson_status = 9 # 바로 해결
                            LessonClaim(status=2, claim_detail=data['claim_detail'],
                                        claim_text_id=data['claim_text'], resolve_type=data['resolve_type'],
                                        reportee_id=rv.tutee_id, reportor_id=user_id,
                                        lesson_reservation_id=rvid, resolve_tc=refund_tc,
                                        course_id=rv.course_id).save()
                            WalletService.setRefund(refund_tc, rv.tutor_id, rv.tutee_id, rvid)
                        elif data['resolve_type'] == '5':
                            rv.lesson_status = 3
                            LessonClaim(status=1, claim_detail=data['claim_detail'],
                                        claim_text_id=data['claim_text'], resolve_type=data['resolve_type'],
                                        reportee_id=rv.tutee_id, reportor_id=user_id, lesson_reservation_id=rvid,
                                        course_id=rv.course_id).save()
                    elif data['claim_text'] == '2':
                        if data['resolve_type'] == '6':
                            rv.lesson_status = 3
                            LessonClaim(status=1, claim_detail=data['claim_detail'],
                                        claim_text_id=data['claim_text'], resolve_type=data['resolve_type'],
                                        reportee_id=rv.tutee_id, reportor_id=user_id,
                                        lesson_reservation_id=rvid, resolve_tc=refund_tc,
                                        course_id=rv.course_id).save()
                        elif data['resolve_type'] == '5':
                            rv.lesson_status = 3
                            LessonClaim(status=1, claim_detail=data['claim_detail'],
                                        claim_text_id=data['claim_text'], resolve_type=data['resolve_type'],
                                        reportee_id=rv.tutee_id, reportor_id=user_id, lesson_reservation_id=rvid,
                                        course_id=rv.course_id).save()
                    elif data['claim_text'] == '3':#other
                        if data['resolve_type'] == '7':
                            rv.lesson_status = 3
                            LessonClaim(status=1, claim_detail=data['claim_detail'],
                                        claim_text_id=data['claim_text'], resolve_type=data['resolve_type'],
                                        resolve_tc=data['resolve_tc'], reportee_id=rv.tutee_id,
                                        reportor_id=user_id, lesson_reservation_id=rvid,
                                        course_id=rv.course_id).save()
                        elif data['resolve_type'] == '8':
                            rv.lesson_status = 7
                            LessonClaim(status=1, claim_detail=data['claim_detail'],
                                        claim_text_id=data['claim_text'], resolve_type=data['resolve_type'],
                                        reportee_id=rv.tutee_id, reportor_id=user_id,
                                        lesson_reservation_id=rvid, resolve_tc=refund_tc,
                                        course_id=rv.course_id).save()
                            WalletService.setConfirm(refund_tc, rv.tutor_id, rv.tutee_id, rvid)
                        elif data['resolve_type'] == '9':
                            rv.lesson_status = 3
                            LessonClaim(status=1, claim_detail=data['claim_detail'],
                                        claim_text_id=data['claim_text'], resolve_type=data['resolve_type'],
                                        reportee_id=rv.tutee_id, reportor_id=user_id,
                                        lesson_reservation_id=rvid, resolve_tc=refund_tc,
                                        course_id=rv.course_id).save()
                        else:
                            rv.lesson_status = 3
                            LessonClaim(status=1, claim_detail=data['claim_detail'],
                                        claim_text_id=data['claim_text'], resolve_type=data['resolve_type'],
                                        reportee_id=rv.tutee_id, reportor_id=user_id, lesson_reservation_id=rvid,
                                        course_id=rv.course_id).save()
                else:#tutee
                    LessonHistory(history_text_id=21, lesson_reservation_id=rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutee_id, rv.tutor_id, rvid, 21)
                    TutorService.set_lesson_notification(1, rv.tutee_id, rv.tutor_id, rvid, 21)
                    if data['claim_text'] == '1':
                        rv.lesson_status = 3
                        LessonClaim(status=1, claim_detail=data['claim_detail'],
                                        claim_text_id=data['claim_text'], resolve_type=data['resolve_type'],
                                        reportee_id=rv.tutor_id, reportor_id=user_id,
                                        lesson_reservation_id=rvid, resolve_tc=refund_tc,
                                        course_id=rv.course_id).save()
                    elif data['claim_text'] == '2':
                        if data['resolve_type'] == '6':
                            rv.lesson_status = 7
                            LessonClaim(status=2, claim_detail=data['claim_detail'],
                                        claim_text_id=data['claim_text'], resolve_type=data['resolve_type'],
                                        reportee_id=rv.tutor_id, reportor_id=user_id,
                                        lesson_reservation_id=rvid, resolve_tc=refund_tc,
                                        course_id=rv.course_id).save()
                            WalletService.setConfirm(refund_tc, rv.tutor_id, rv.tutee_id, rvid)
                        elif data['resolve_type'] == '5':
                            rv.lesson_status = 3
                            LessonClaim(status=1, claim_detail=data['claim_detail'],
                                        claim_text_id=data['claim_text'], resolve_type=data['resolve_type'],
                                        reportee_id=rv.tutor_id, reportor_id=user_id, lesson_reservation_id=rvid,
                                        course_id=rv.course_id).save()
                    elif data['claim_text'] == '3':
                        if data['resolve_type'] == '7':
                            rv.lesson_status = 3
                            LessonClaim(status=1, claim_detail=data['claim_detail'],
                                        claim_text_id=data['claim_text'], resolve_type=data['resolve_type'],
                                        reportee_id=rv.tutor_id, reportor_id=user_id,
                                        lesson_reservation_id=rvid, resolve_tc=data['resolve_tc'],
                                        course_id=rv.course_id).save()
                        elif data['resolve_type'] == '8':
                            rv.lesson_status = 3
                            LessonClaim(status=1, claim_detail=data['claim_detail'],
                                        claim_text_id=data['claim_text'], resolve_type=data['resolve_type'],
                                        reportee_id=rv.tutor_id, reportor_id=user_id,
                                        lesson_reservation_id=rvid, resolve_tc=refund_tc,
                                        course_id=rv.course_id).save()
                        elif data['resolve_type'] == '9':
                            rv.lesson_status = 7
                            LessonClaim(status=2, claim_detail=data['claim_detail'],
                                        claim_text_id=data['claim_text'], resolve_type=data['resolve_type'],
                                        reportee_id=rv.tutor_id, reportor_id=user_id,
                                        lesson_reservation_id=rvid, resolve_tc=refund_tc,
                                        course_id=rv.course_id).save()
                            WalletService.setConfirm(refund_tc, rv.tutor_id, rv.tutee_id, rvid)
                        else:
                            rv.lesson_status = 3
                            LessonClaim(status=1, claim_detail=data['claim_detail'],
                                        claim_text_id=data['claim_text'], resolve_type=data['resolve_type'],
                                        reportee_id=rv.tutor_id, reportor_id=user_id, lesson_reservation_id=rvid,
                                        course_id=rv.course_id).save()
            elif action == "Confirm":#tutee
                if rv.lesson.pkg_times is not 1 and rv.pkg_order is rv.lesson.pkg_times:
                    # this is a package lesson and it's the last
                    rv.lesson.pkg_status = 2
                    rv.lesson.save()
                # tutee confirmed the status
                LessonHistory(history_text_id=31, lesson_reservation_id=rvid).save()
                TutorService.set_lesson_notification(1, rv.tutee_id, rv.tutor_id, rvid, 31)
                rv.lesson_status = 7
                LessonFeedback(
                    tutor_id=rv.tutor_id, tutee_id=user_id,
                    lesson_reservation_id=rv.id, course_id=rv.course_id,
                    tutee_feedback=data['comment'], ratings=data['ratings']
                ).save()
                # need to give the lesson fee to the tutor here
                tmpprice = rv.lesson.sg_price
                if tmpprice is 0:
                    # package lesson
                    tmpprice = rv.lesson.pkg_price
                WalletService.setConfirm(tmpprice, rv.tutor_id, rv.tutee_id, rvid)
            elif action == "Leave Feedback":#tutor
                # if the user is the tutor
                LessonFeedback(
                    tutor_id=user_id, tutee_id=rv.tutee_id,
                    lesson_reservation_id=rv.id, course_id=rv.course_id,
                    tutor_feedback=data['comment'], ratings=0
                ).save()
                LessonHistory(history_text_id=33, lesson_reservation_id=rvid).save()
                TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 33)
#             lh.save()
            rv.save()
            return True
        elif status == "Confirmed":
            if action == "Leave Feedback":
                if rv.tutor_id == user_id:#tutor
                    LessonFeedback(
                        tutor_id=rv.tutor_id, tutee_id=user_id,
                        lesson_reservation_id=rv.id, course_id=rv.course_id,
                        tutor_feedback=data['comment'], ratings=data['ratings']
                    ).save()
                    LessonHistory(history_text_id=33, lesson_reservation_id=rvid).save()
                    TutorService.set_lesson_notification(1, rv.tutor_id, rv.tutee_id, rvid, 33)
        elif status == "Canceled":
            return True
        elif status == "Refunded":
            return True
        elif status == "Expired":
            return True
        else:
            print 'else, ' + str(comment_id)
        ################# REVISION LINE ########################
        # set notification 
        #TutorService.set_lesson_notification(status, from_user, to_user, rv, comment_id)

        return True


    @staticmethod
    def get_problems(rvid):
        """
        @summary: 강의 문제제기 카운트
        @author: msjang
        @param rvid: 강의 예약 id
        @return: pr( int )
        """
        lessonClaimCount = LessonClaim.objects.filter(lesson_reservation_id=rvid).count()
        print "get_problems : "
        print lessonClaimCount

        return lessonClaimCount

    @staticmethod
    def get_problems_details(rvid, _user_id, is_reporter=True):
        """
        @summary: 강의 문제제기 상세내용
        @author: msjang
        @param rvid: 강의 예약 id
        @param _user_id: 유저 id
        @param is_reporter: 문제제기여부
        @return: result_list( list )
        """
        result_list = []
        obj = {}
#         pkgLesson = Lesson.objects.select_related('course', 'tutor', 'tutee').get(id=reservation_id)
        rv = LessonReservation.objects.select_related('lesson').get(id=rvid)
        obj["rvid"] = rv.id
        obj['status_time'] = (rv.expired_time + timedelta(hours=72)).strftime('%Y-%m-%d, %I:%M %p')
        obj["reservation_number"] = ReservationService.set_lesson_id(rv.id)
        obj["status_name"] = ReservationService.RESERVATION_STATUS[6]#Need comfirmation
#         obj["status_time"] = (LessonClaim.objects.get(id= rv.id).created_time + timedelta(hours=72)).strftime('%Y-%m-%d, %I:%M %p')
#         online_type = rv.online and 1 or 2
#         price = rv.lesson.pkg_total_price
#         per_price = price["total"] / float(rv.bundle_times)

        obj["max_coin"] = rv.lesson.pkg_price
        if rv.tutor_id == _user_id:
            obj["is_tutor"] = 1
            obj["reportee"] = rv.tutee_id
        elif rv.tutee_id == _user_id:
            obj["is_tutor"] = 0
            obj["reportee"] = rv.tutor_id
        obj["is_reporter"] = is_reporter

        _date = datetime.strptime(str(rv.rv_time)[:8] + str(rv.rv_time)[9:], "%Y%m%d%H%M")
        if _date - datetime.utcnow() > timedelta(hours=24):
            obj['before_24_hours'] = True
        else:
            obj['before_24_hours'] = False
        if _date - datetime.utcnow() <= timedelta(hours=24) and _date - datetime.utcnow() > timedelta():
            obj['between_24_hours'] = True
            print 123
        else:
            obj['between_24_hours'] = False
            print 456

        # 현재시간 경과
        if datetime.utcnow() > _date and datetime.utcnow() - _date < timedelta(hours=72):
            obj['between_72_hours'] = True
        else:
            obj['between_72_hours'] = False

        if datetime.utcnow() - _date > timedelta(hours=72):
            obj['after_72_hours'] = True
        else:
            obj['after_72_hours'] = False

        result_list.append(obj)

        return result_list

    @staticmethod
    def get_problems_details_list(rvid, _user_id):
        """
        @summary: 강의 문제제기 리스트
        @author: msjang
        @param rvid: 강의 예약 id
        @param _user_id: 유저 id
        @return: result_list( list )
        """

#         LC_LIST = ['Reschedule the lesson', 'Transfer the lesson coins to the tutor',
#                    'Return lesson coins to the tutee']
#         RS_TUTEE_LIST = [
#             'You confirm that both agree to reschedule the lesson. <br> ' \
#             'The Tutee will be able to select a new date and time for rescheduling as soon as you accept.',
#             'You confirm that both agree to transfer the lesson coins to the tutor.<br>' \
#             'You will be able to check out whether lesson coins are correctly transferred to the tutor on ‘My Coins’ page.',
#             'You confirm that both agree to return lesson coins to the tutee.<br>' \
#             'The rest of coins after this refund will be transferred to the tutor.<br>' \
#             'You will be able to check out whether lesson coins are correctly distributed on ‘My Coins’ page.']
#         RS_TUTOR_LIST = ['You confirm that both agree to reschedule the lesson. <br> ' \
#                          'You will be able to select a new date and time for rescheduling as soon as you accept.',
#                          'You confirm that both agree to transfer the lesson coins to the tutor.<br>' \
#                          'You will be able to check out whether lesson coins are correctly transferred to you on ‘My Coins’ page.',
#                          'You confirm that both agree to return lesson coins to the tutee.<br>' \
#                          'The rest of coins after this refund will be transferred to the tutor.<br>' \
#                          'You will be able to check out whether lesson coins are correctly distributed on ‘My Coins’ page.']

        result_list = []
        obj = {}

        rv = LessonReservation.objects.get(id=rvid)
        print 'rvid: '
        print rvid
        lessonClaim = LessonClaim.objects.select_related('lesson_reservation').filter(lesson_reservation_id=rvid).order_by('-created_time')
        obj["rvid"] = rv.id
        obj["reservation_number"] = ReservationService.set_lesson_id(rv.id)
        obj["status_name"] = ReservationService.RESERVATION_STATUS[3]#Problem reported
        obj["tutor_id"] = rv.tutor_id
        obj["tutee_id"] = rv.tutee_id
        obj["user"] = _user_id

        if rv.tutor_id == _user_id:
            obj["is_tutor"] = 1
        elif rv.tutee_id == _user_id:
            obj["is_tutor"] = 0

        lcCount = LessonClaim.objects.filter(lesson_reservation_id=rvid).count()
        obj["list"] = []
        for lc in lessonClaim:
            print "get_problems_details_list"
            print lc.id
            obj2 = {}
            obj2["created_time"] = str(lc.created_time)
            obj2["claim_text"] = lc.claim_text_id
            obj2["claim_detail"] = lc.claim_detail
            obj2["problem_image"] = lc.claim_fid
            obj2["resolve_type"] = lc.resolve_type
            obj2["problem_amount"] = lc.resolve_tc
            obj2["reportor"] = lc.reportor_id
            obj2["reportee"] = lc.reportee_id
            obj2["claim_id"] = lc.id

            if lc.reportor_id == _user_id:
                if TutorService.is_tutor(_user_id):
                    obj_reportor_tutor = Tutor.objects.get(user_id=lc.reportor_id)
                    obj2["reportor_name"] = obj_reportor_tutor.name
                    obj2["reportor_image"] = obj_reportor_tutor.photo_filename
                else:
                    obj_reportor_tutee = Tutee.objects.get(user_id=lc.reportor_id)
                    obj2["reportor_name"] = obj_reportor_tutee.name
                    obj2["reportor_image"] = obj_reportor_tutee.photo_filename
                if TutorService.is_tutor(lc.reportee_id):
                    obj_reportee_tutor = Tutor.objects.get(user_id=lc.reportee_id)
                    obj2["reportee_name"] = obj_reportee_tutor.name
                    obj2["reportee_image"] = obj_reportee_tutor.photo_filename
                else:
                    obj_reportee_tutee = Tutee.objects.get(user_id=lc.reportee_id)
                    obj2["reportee_name"] = obj_reportee_tutee.name
                    obj2["reportee_image"] = obj_reportee_tutee.photo_filename
            else:
                if TutorService.is_tutor(_user_id):
                    obj_reportee_tutor = Tutor.objects.get(user_id=lc.reportee_id)
                    obj2["reportee_name"] = obj_reportee_tutor.name
                    obj2["reportee_image"] = obj_reportee_tutor.photo_filename
                else:
                    obj_reportee_tutee = Tutee.objects.get(user_id=lc.reportee_id)
                    obj2["reportee_name"] = obj_reportee_tutee.name
                    obj2["reportee_image"] = obj_reportee_tutee.photo_filename
                if TutorService.is_tutor(lc.reportor_id):
                    obj_reportee_tutor = Tutor.objects.get(user_id=lc.reportor_id)
                    obj2["reportor_name"] = obj_reportee_tutor.name
                    obj2["reportor_image"] = obj_reportee_tutor.photo_filename
                else:
                    obj_reportee_tutee = Tutee.objects.get(user_id=lc.reportor_id)
                    obj2["reportor_name"] = obj_reportee_tutee.name
                    obj2["reportor_image"] = obj_reportee_tutee.photo_filename

            if lcCount == 1:
                obj2["status_time"] = (lc.created_time + timedelta(hours=72)).strftime('%Y-%m-%d, %I:%M %p')
            else:
                obj2["status_time"] = None
                lcCount = lcCount - 1

            obj["list"].append(obj2)

        result_list.append(obj)
        return result_list

    @staticmethod
    def set_problem_accept(self, rvid, type):
        """
        @summary: 문제제기에 따른 강의 처리
        @author: msjang
        @param rvid: 강의 예약 id
        @param type: 처리 방법
        @return: none
        """
        print rvid, type
        pr = LessonClaim.objects.get(lesson_reservation=rvid)
        pr.is_accepted = 1
        pr.save()
        rv = LessonReservation.objects.get(lesson_reservation=rvid)
        if type == 0:
            # reschdule -> 상태 변경(unscheduled) , history
            ReservationService.lesson_status_commit(rvid, 'Unschedule', _data=None)

        elif type == 1:
            # tutor에게 코인 전부 이전 -> 상태변경(confirm), coin, history
            ReservationService.lesson_status_commit(rvid, 'Confirm2', _data=None)
            # coin
        elif type == 2:
            # tutee에게 일부 환급 -> 상태변경(refunded), coin, history
            ReservationService.lesson_status_commit(rvid, 'Refund', _data=None)
            # coin

#     @staticmethod
#     def get_lesson_price(l_id, online_type, _bundle, duration):
#         """
#         @summary: 강의 가격 정보
#         @author: msjang
#         @param l_id: lesson id
#         @param online_type: 강의 타입
#         @param _bundle: 번들 여부
#         @param duration: 한 강의 시간
#         @return: object
#         """
#         try:
#             _type = Lesson.objects.get(lid=l_id).type
#         except:
#             _type = 0
#             print "get_lesson_price get lid error"
# 
#         per_price = 0
#         try:
#             if int(_type) == 1:
#                 # 정식 강의
#                 if int(online_type) == 1:  # online
#                     if int(_bundle) == 1:
#                         if int(duration) == 30:
#                             per_price = Lesson.objects.get(lid=l_id).online_30m_1times_cost
#                         elif int(duration) == 60:
#                             per_price = Lesson.objects.get(lid=l_id).online_60m_1times_cost
#                     elif int(_bundle) == 5:
#                         if int(duration) == 30:
#                             per_price = Lesson.objects.get(lid=l_id).online_30m_5times_cost
#                         elif int(duration) == 60:
#                             per_price = Lesson.objects.get(lid=l_id).online_60m_5times_cost
#                     elif int(_bundle) == 10:
#                         if int(duration) == 30:
#                             per_price = Lesson.objects.get(lid=l_id).online_30m_10times_cost
#                         elif int(duration) == 60:
#                             per_price = Lesson.objects.get(lid=l_id).online_60m_10times_cost
# 
#                 elif int(online_type) == 2:  # offline
#                     if int(_bundle) == 1:
#                         if int(duration) == 60:
#                             per_price = Lesson.objects.get(lid=l_id).offline_60m_1times_cost
#                         elif int(duration) == 90:
#                             per_price = Lesson.objects.get(lid=l_id).offline_90m_1times_cost
#                         elif int(duration) == 120:
#                             per_price = Lesson.objects.get(lid=l_id).offline_120m_1times_cost
#                     elif int(_bundle) == 5:
#                         if int(duration) == 60:
#                             per_price = Lesson.objects.get(lid=l_id).offline_60m_5times_cost
#                         elif int(duration) == 90:
#                             per_price = Lesson.objects.get(lid=l_id).offline_90m_5times_cost
#                         elif int(duration) == 120:
#                             per_price = Lesson.objects.get(lid=l_id).offline_120m_5times_cost
#                     elif int(_bundle) == 10:
#                         if int(duration) == 60:
#                             per_price = Lesson.objects.get(lid=l_id).offline_60m_10times_cost
#                         elif int(duration) == 90:
#                             per_price = Lesson.objects.get(lid=l_id).offline_90m_10times_cost
#                         elif int(duration) == 120:
#                             per_price = Lesson.objects.get(lid=l_id).offline_120m_10times_cost
# 
#             elif int(_type) == 0:
#                 # 시범 강의
#                 total_price = Lesson.objects.get(lid=l_id).online_30m_1times_cost
#                 return {"per": total_price, "total": total_price}
# 
#         except Exception as err:
#             print str(err)
# 
#         # total_price = per_price * _bundle
#         total_price = per_price
# 
#         return {"per": per_price, "total": total_price}
