# -*- coding: utf-8 -*-
###############################################################################
# filename    : tutoring > models.py
# description : User 모델 정의
# author      : hsrjmk@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160919 hsrjmk 최조 작성
#
###############################################################################
from django.db import models
from common.models import LanguageInfo, SkillLevelInfo
from common.utils import get_expiration_datetime
from tutor.models import Tutor, Tutee
from user_auth.models import File, TellpinUser


DAY_OF_WEEK = (
    (1, 'Mon'),
    (2, 'Tue'),
    (3, 'Wed'),
    (4, 'Thu'),
    (5, 'Fri'),
    (6, 'Sat'),
    (7, 'Sun'),
)
SCHEDULE_TYPE = (
    (1, 'Added'),
    (2, 'Deleted'),
)
PACKAGE_TIMES = (
    (5, '5 times package'),
    (10, '10 times package'),
    (20, '20 times package'),
    (30, '30 times package'),
)
BG_IMG_TYPE = (
    (1, 'Business'),
    (2, 'National Flags'),
    (3, 'People'),
    (4, 'Scenery'),
    (5, 'Themes'),
)
LESSON_MESSAGE_TYPE = (
    (0, 'Normal message'),
    (1, 'Request message'),
)
LESSON_TYPE = (
    (1, 'trial'),
    (2, 'single'),
    (3, 'package'),
)
LESSON_MINUTES = (
    (30, '30 minutes lesson'),
    (60, '60 minutes lesson'),
    (90, '90 minutes lesson'),
)
LESSON_STATUS = (
    (1, 'Requested'),
    (2, 'Approved/Upcoming'),
    (3, 'Problem reported'),
    (4, 'In dispute'),
    (5, 'In progress'),
    (6, 'Need confirmation'),
    (7, 'Confirmed'),
    (8, 'Canceled'),
    (9, 'Refunded'),
    (10, 'Expired'),
    (21, 'Approved/Upcoming - Request to change'),
    (22, 'Approved/Upcoming - Change date/time'),
    (23, 'Approved/Upcoming - Cancel'),
    (9999, 'None'),
)
TICKET_STATUS = (
    (0, 'unused'),
    (1, 'used'),
)
MESSENGER_TYPE = (
    (None, 'Select one'),
    (1, 'Skype'),
    (2, 'HangOut'),
    (3, 'FaceTime'),
    (4, 'QQ'),
)
RATINGS = (
    (1, 'one star'),
    (2, 'two stars'),
    (3, 'three stars'),
    (4, 'four stars'),
    (5, 'five stars'),
)
CLAIM_STATUS = (
    (1, 'Reported'),
    (2, 'Resolved'),
    (3, 'Canceled'),
    (4, 'Rejected'),
    (5, 'In dispute'),
)
# 추후 i18n 병행작업 필요
CLAIM_TEXT = (
    (None, 'Select one'),
    (1, 'The tutor was absent'),
    (2, 'The tutee was absent'),
    (3, 'Other'),
)
RESOLVE_TYPE = (
    (None, 'Select one'),
    (1, 'Transfer all Tellpin Credits of the lesson fee to the tutor'),
    (2, 'Return all Tellpin Credits of the lesson fee to the tutee'),
    (3, 'Return partial Tellpin Credits of the lesson fee to the tutee'),
    (4, 'Reschedule the lesson'),
)
PACKAGE_STATUS = (
    (None, 'None'),
    (0, 'Expired'),
    (1, 'Refunded'),
    (2, 'Completed'),
    (3, 'Cancelled'),
    (4, 'In progress'),
)


def get_default_expired_datetime():
    return get_expiration_datetime(days=30)


class TutorScheduleWeek(models.Model):
    tutor = models.ForeignKey(Tutor)

    day_of_week = models.PositiveSmallIntegerField("Day of week", choices=DAY_OF_WEEK, default=1, null=True)
    time0000 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0030 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0100 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0130 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0200 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0230 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0300 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0330 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0400 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0430 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0500 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0530 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0600 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0630 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0700 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0730 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0800 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0830 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0900 = models.BooleanField("Disable: False, Enable: True", default=False)
    time0930 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1000 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1030 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1100 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1130 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1200 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1230 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1300 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1330 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1400 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1430 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1500 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1530 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1600 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1630 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1700 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1730 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1800 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1830 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1900 = models.BooleanField("Disable: False, Enable: True", default=False)
    time1930 = models.BooleanField("Disable: False, Enable: True", default=False)
    time2000 = models.BooleanField("Disable: False, Enable: True", default=False)
    time2030 = models.BooleanField("Disable: False, Enable: True", default=False)
    time2100 = models.BooleanField("Disable: False, Enable: True", default=False)
    time2130 = models.BooleanField("Disable: False, Enable: True", default=False)
    time2200 = models.BooleanField("Disable: False, Enable: True", default=False)
    time2230 = models.BooleanField("Disable: False, Enable: True", default=False)
    time2300 = models.BooleanField("Disable: False, Enable: True", default=False)
    time2330 = models.BooleanField("Disable: False, Enable: True", default=False)

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True, null=True)

    class Meta:
        db_table = u'tutor_schedule_week'


class TutorScheduleCalendar(models.Model):
    tutor = models.ForeignKey(Tutor)

    schedule_time = models.CharField("Schedule time: YYYYMMDDWHHMM", max_length=13, null=True)
    type = models.PositiveSmallIntegerField("Schedule type", choices=SCHEDULE_TYPE, default=1, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True, null=True)

    class Meta:
        db_table = u'tutor_schedule_calendar'


class TutorCourse(models.Model):
    tutor = models.ForeignKey(Tutor)

    course_lang = models.ForeignKey(LanguageInfo)
    course_name = models.CharField("Course name", max_length=200, null=True)
    course_desc = models.CharField("Description about course", max_length=300, null=True)
    course_level_from = models.ForeignKey(SkillLevelInfo, related_name='+')
    course_level_to = models.ForeignKey(SkillLevelInfo, related_name='+')
    course_bgimg = models.ForeignKey('TutorCourseBgImg')
    course_has_30min = models.BooleanField("Course has 30 minutes", default=False)
    course_has_60min = models.BooleanField("Course has 60 minutes", default=False)
    course_has_90min = models.BooleanField("Course has 90 minutes", default=False)
    sg_30min_price = models.IntegerField("Price for 30 minutes single course", default=0, null=True)
    sg_60min_price = models.IntegerField("Price for 60 minutes single course", default=0, null=True)
    sg_90min_price = models.IntegerField("Price for 90 minutes single course", default=0, null=True)
    pkg_30min_times = models.PositiveSmallIntegerField("Times for 30 minutes package course", choices=PACKAGE_TIMES, default=5, null=True)
    pkg_60min_times = models.PositiveSmallIntegerField("Times for 60 minutes package course", choices=PACKAGE_TIMES, default=5, null=True)
    pkg_90min_times = models.PositiveSmallIntegerField("Times for 90 minutes package course", choices=PACKAGE_TIMES, default=5, null=True)
    pkg_30min_price = models.IntegerField("Price for 30 minutes package course", default=0, null=True)
    pkg_60min_price = models.IntegerField("Price for 60 minutes package course", default=0, null=True)
    pkg_90min_price = models.IntegerField("Price for 90 minutes package course", default=0, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'tutor_course'


class TutorCourseBgImg(models.Model):
    type = models.PositiveSmallIntegerField("Background image type", choices=BG_IMG_TYPE, default=1, null=True)
    filepath = models.CharField("Image file path", max_length=200, null=True)

    class Meta:
        db_table = u'tutor_course_bgimg'


class Lesson(models.Model):
    course = models.ForeignKey('TutorCourse', null=True)
    tutor = models.ForeignKey(Tutor)
    tutee = models.ForeignKey(Tutee)

    lesson_type = models.PositiveSmallIntegerField("Lesson type", choices=LESSON_TYPE, default=1, null=True)
    trial_price = models.IntegerField("Year to certificate", null=True)
    sg_min = models.PositiveSmallIntegerField("Minute of single lesson", choices=LESSON_MINUTES, default=30, null=True)
    sg_price = models.IntegerField("Price of single lesson", default=0, null=True)
    pkg_min = models.PositiveSmallIntegerField("Minute per package lesson", choices=LESSON_MINUTES, default=30, null=True)
    pkg_times = models.PositiveSmallIntegerField("Lesson times of package", choices=PACKAGE_TIMES, default=5, null=True)
    pkg_main_id = models.IntegerField("First lesson id in a package", default=0, null=True)
    pkg_price = models.IntegerField("Price per package lesson", default=0, null=True)
    pkg_total_price = models.IntegerField("Total price of package lesson", default=0, null=True)
    expired_time = models.DateTimeField("Expired time", default=get_default_expired_datetime, null=True)
    pkg_status = models.PositiveSmallIntegerField("Status of the package", choices=PACKAGE_STATUS, null=True)
    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'lesson'


class LessonReservation(models.Model):
    lesson = models.ForeignKey('Lesson')
    course = models.ForeignKey('TutorCourse')
    tutor = models.ForeignKey(Tutor)
    tutee = models.ForeignKey(Tutee)

    pkg_order = models.PositiveSmallIntegerField("Lesson order in package", default=1, null=True)
    rv_time = models.CharField("Reservation time: YYYYMMDDWHHMM", max_length=13, null=True)
    messenger_type = models.PositiveSmallIntegerField("Messenger type", choices=MESSENGER_TYPE, default=1, null=True)
    messenger_id = models.CharField("Messenger id", max_length=100, null=True)
    message = models.CharField("Message for lesson reservation", max_length=2000, null=True)
    lesson_status = models.PositiveSmallIntegerField("Lesson status", choices=LESSON_STATUS, default=9999, null=True)
    ticket_status = models.PositiveSmallIntegerField("Ticket status", choices=TICKET_STATUS, default=0, null=True)
    expired_time = models.DateTimeField("Expired time", default=get_default_expired_datetime, null=True)
    reservation_status_text = models.ForeignKey('LessonReservationStatusText', null=True)
    decline_time = models.PositiveSmallIntegerField("Times declined", default=0, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'lesson_reservation'

class LessonReservationTemp(models.Model):
    lesson = models.ForeignKey('Lesson')
    course = models.ForeignKey('TutorCourse')
    tutor = models.ForeignKey(Tutor)
    tutee = models.ForeignKey(Tutee)

    pkg_order = models.PositiveSmallIntegerField("Lesson order in package", default=1, null=True)
    rv_time = models.CharField("Reservation time: YYYYMMDDWHHMM", max_length=13, null=True)
    messenger_type = models.PositiveSmallIntegerField("Messenger type", choices=MESSENGER_TYPE, default=1, null=True)
    messenger_id = models.CharField("Messenger id", max_length=100, null=True)
    message = models.CharField("Message for lesson reservation", max_length=2000, null=True)
    lesson_status = models.PositiveSmallIntegerField("Lesson status", choices=LESSON_STATUS, default=9999, null=True)
    ticket_status = models.PositiveSmallIntegerField("Ticket status", choices=TICKET_STATUS, default=0, null=True)
    expired_time = models.DateTimeField("Expired time", default=get_default_expired_datetime, null=True)
    reservation_status_text = models.ForeignKey('LessonReservationStatusText', null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'lesson_reservation_temp'


class LessonMessage(models.Model):
    lesson_reservation = models.ForeignKey('LessonReservation')

    msg_type = models.SmallIntegerField("Message type: normal message, request message", choices=LESSON_MESSAGE_TYPE, default=0)
    sender = models.ForeignKey(TellpinUser, related_name='+')
    receiver = models.ForeignKey(TellpinUser, related_name='+')
    message = models.CharField("Message for lesson", max_length=1000, null=True)
    is_read = models.BooleanField("Message read", default=False)

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)

    class Meta:
        db_table = u'lesson_message'


class LessonHistory(models.Model):
    lesson_reservation = models.ForeignKey('LessonReservation')

    history_text = models.ForeignKey('LessonHistoryText')

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'lesson_history'


class LessonHistoryText(models.Model):
    history_english = models.CharField("History text in English", max_length=200, null=True)
    history_korean = models.CharField("History text in Korean", max_length=200, null=True)
    history_chinese = models.CharField("History text in Chinese", max_length=200, null=True)
    history_spanish = models.CharField("History text in Spanish", max_length=200, null=True)

    class Meta:
        db_table = u'lesson_history_text'


class LessonFeedback(models.Model):
    lesson_reservation = models.ForeignKey('LessonReservation')
    course = models.ForeignKey('TutorCourse')

    tutor = models.ForeignKey(Tutor)
    tutee = models.ForeignKey(Tutee)
    tutor_feedback = models.CharField("Tutor's feedback comment to tutee", max_length=2000, null=True)
    tutee_feedback = models.CharField("Tutee's feedback comment to tutor", max_length=2000, null=True)
    ratings = models.PositiveSmallIntegerField("Rating about lesson", choices=RATINGS, default=1, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True, null=True)

    class Meta:
        db_table = u'lesson_feedback'


class LessonClaim(models.Model):
    lesson_reservation = models.ForeignKey('LessonReservation')
    course = models.ForeignKey('TutorCourse')

    reportor = models.ForeignKey(TellpinUser, related_name='+')
    reportee = models.ForeignKey(TellpinUser, related_name='+')
    status = models.PositiveSmallIntegerField("Claim status", choices=CLAIM_STATUS, default=1, null=True)

    claim_text = models.ForeignKey('LessonClaimText')
    claim_detail = models.CharField("Detail comment for claim", max_length=1000)
    claim_fid = models.ForeignKey(File, null=True)
    resolve_type = models.PositiveSmallIntegerField("Resolve type", choices=RESOLVE_TYPE, default=3, null=True)
    resolve_tc = models.IntegerField("Tellpin credit amount to return", default=0, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    class Meta:
        db_table = u'lesson_claim'


class LessonClaimText(models.Model):
    claim_english = models.CharField("Claim text in English", max_length=300, null=True)
    claim_korean = models.CharField("Claim text in Korean", max_length=300, null=True)
    claim_chinese = models.CharField("Claim text in Chinese", max_length=300, null=True)
    claim_spanish = models.CharField("Claim text in Spanish", max_length=300, null=True)

    class Meta:
        db_table = u'lesson_claim_text'


class LessonReservationStatusText(models.Model):
    status_english = models.CharField("Reservation status text in English", max_length=1000, null=True)
    status_korean = models.CharField("Reservation status text in Korean", max_length=1000, null=True)
    status_chinese = models.CharField("Reservation status text in Chinese", max_length=1000, null=True)
    status_spanish = models.CharField("Reservation status text in Spanish", max_length=1000, null=True)

    class Meta:
        db_table = u'lesson_reservation_status_text'