# -*- coding: utf-8 -*-

###############################################################################
# filename    : tutoring > views.py
# description : 새로운 강의 등록 시 필요한 함수들을 구성하는 파일
# author      : kihyun@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 j746 최초 작성
#
#
###############################################################################

from calendar import month
from datetime import datetime, date
import json
import time
from urllib2 import HTTPRedirectHandler

from django.core.serializers.json import DjangoJSONEncoder
from django.http.response import HttpResponse, JsonResponse, \
    HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt

from common.common import tellpin_login_required
import mypage
from common.service import CommonService
from mypage.service import MyPageService
import tutor
# from common.models import Schedule, Profile, Resume, Files
from tutor.service import TutorService
from tutor.service4registration import TutorService4Registration
from tutoring import NewLessonService
from tutoring.NewLessonService import NewLesson
from tutoring.service import CourseService, ScheduleService, ReservationService
from tutor.views import date_handler
from datetime import datetime, timedelta


mypage_service = MyPageService(object)
ts4r = TutorService4Registration(object)


@csrf_exempt
@tellpin_login_required
def tutoring_info(request):
    """
    @summary: 예약 리스트 출력 페이지 호출
    @author: sj746
    @param request
    @return: url : tutoring/tutoring.html
    """
    # resoponse data
    resData = {
        'isSuccess': 1
    }
    user_id = request.user.id
    print 'tutoring_info()'
    print request.POST
    _status = None
    _filter = request.POST.get("search_name", "")
    _tutee_name = request.POST.get("tutee_name", "")

    if request.POST.get("search_name", ""):
        if request.POST.get("search_name", "") == "Action required":
            if request.path == '/tutorlessons/':
                _status = [1, 5, 6, 8]
            else:
                _status = [0, 2, 4, 6]
        elif request.POST.get("search_name", "") == "Waiting for action":
            if request.path == '/tutorlessons/':
                _status = [0, 2]
            else:
                _status = [1, 8]
        elif request.POST.get("search_name", "") == "Not scheduled":
            _status = 0
        elif request.POST.get("search_name", "") == "In Problem":
            _status = 6
        elif request.POST.get("search_name", "") == "New Message":
            _status = "New Message"
    else:
        print 456

    resData["is_tutor"] = request.user.tellpinuser.is_tutor
    utc_time = request.session.get('utc_delta', 0)
    print _status
    print _filter
    print _tutee_name

    if request.path == '/tutorlessons/':
        resData["display"] = 1
        if request.POST.get("search_name", ""):
            resData["filter"] = _filter
            resData["rv_data"] = ReservationService.get_tutor_reservations(
                user_id, utc_time, _status, 1, _filter
            )
        else:
            if _tutee_name is not None:
                print 11111111111111111111111111111111111111
                print _tutee_name
                resData["user_name"] = _tutee_name
                resData["rv_data"] = ReservationService.get_tutor_reservations(
                    user_id, utc_time, None, 1, None, _tutee_name
                )
            else:
                print 22222222222222222222222222222222222222
                resData["rv_data"] = ReservationService.get_tutor_reservations(user_id, utc_time)
    else:
        resData["display"] = 2
        if request.POST.get("search_name", ""):
            resData["filter"] = _filter
            resData["rv_data"] = ReservationService.get_tutee_reservations(user_id, utc_time, _status, 1, _filter)
        else:
            resData["rv_data"] = ReservationService.get_tutee_reservations(user_id, utc_time)

    jsonData = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("tutoring/tutoring.html",
                              {'data': jsonData, 'is_tutor': resData["is_tutor"], 'display': resData["display"]},
                              RequestContext(request))


@csrf_exempt
@tellpin_login_required
def tutoring_info_package(request):
    """
    @summary: 예약 리스트 출력 페이지 호출
    @author: sj746
    @param request
    @return: url : tutoring/tutoring.html
    """
    # resoponse data
    resData = {
        'isSuccess': 1
    }
    user_id = request.user.id
    print 'tutoring_info_package()'

    _status = None
    _filter = request.POST.get("search_name", "")
    _tutee_name = request.POST.get("tutee_name", "")

    if request.POST.get("search_name", ""):
        if request.POST.get("search_name", "") == "In progress":
            if request.path == '/tutorlessons/package/':
                _status = [1, 5, 6, 8]
            else:
                _status = [0, 2, 4, 6]
        elif request.POST.get("search_name", "") == "Completed":
            if request.path == '/tutorlessons/package/':
                _status = [0, 2]
            else:
                _status = [1, 8]
        elif request.POST.get("search_name", "") == "Refunded":
            _status = 0
        elif request.POST.get("search_name", "") == "Expired":
            _status = 6
    else:
        print 456

    resData["is_tutor"] = request.user.tellpinuser.is_tutor
    utc_time = request.session.get('utc_delta', 0)
    print _status
    print _filter
    print _tutee_name

    if request.path == '/tutorlessons/package/':
        resData["display"] = 15
        if request.POST.get("search_name", ""):
            print 55555555555555
            resData["filter"] = _filter
            resData["rv_data"] = ReservationService.get_tutor_package_lesson(
                user_id, utc_time, _status, 1, _filter
            )
        else:
            if _tutee_name is not None:
                resData["user_name"] = _tutee_name
                resData["rv_data"] = ReservationService.get_tutor_package_lesson(
                    user_id, utc_time, None, 1, None, _tutee_name
                )
            else:
                resData["rv_data"] = ReservationService.get_tutor_package_lesson(user_id, utc_time)
    else:
        resData["display"] = 16
        if request.POST.get("search_name", ""):
            resData["filter"] = _filter
            resData["rv_data"] = ReservationService.get_tutee_package_lesson(user_id, utc_time, _status, 1, _filter)
        else:
            resData["rv_data"] = ReservationService.get_tutee_package_lesson(user_id, utc_time)

    jsonData = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("tutoring/tutoring_pkg.html",
                              {'data': jsonData, 'is_tutor': resData["is_tutor"], 'display': resData["display"]},
                              RequestContext(request))


@csrf_exempt
@tellpin_login_required
def tutoring_tutor_reserved(request):
    """
    @summary: 예약 강의 리스트 tutor일때 data
    @author: sj746
    @param request
    @return: url : tutoring/tutoring.html
    """
    # resoponse data
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    resData["is_tutor"] = TutorService.is_tutor(user_id)
    resData["display"] = 1
    resData["rv_list"] = TutorService.get_tutor_reservations(resData["is_tutor"])

    return render_to_response("tutoring/tutoring.html", resData, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def tutoring_tutee_reserved(request):
    """
    @summary: 예약 강의 리스트 tutee일때 data
    @author: sj746
    @param request
    @return: url : tutoring/tutoring.htm
    """
    # resoponse data
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    resData["is_tutor"] = TutorService.is_tutor(user_id)
    resData["display"] = 2
    resData["rv_list"] = ReservationService.get_tutee_reservations(user_id)
    print resData

    return render_to_response("tutoring/tutoring.html", resData, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def tutoring_tutor_commit(request):
    """
    @summary: 튜터 강의에 대한 confirm, feedback, problem
    @author: msjang
    @param none
    @return: json
    """
    #resoponse data 
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    # 파라미터 읽기
    post_dict = json.loads(request.body)
    print post_dict
    print 'tutoring_tutor_commit'

    try:
        #추가 스케쥴 예약
        if post_dict["action"] == "Request to change":
            print "Request to change - must be from approved/upcoming"
            utc_delta = request.session.get('utc_delta', 0)
            ReservationService.lesson_status_commit(user_id, post_dict["rvid"], post_dict["command"], post_dict["action"], post_dict['data'], post_dict["new_date"])
            resData["rv_data"] = ReservationService.get_reservation_detail(user_id, post_dict["rvid"], utc_delta )
            return JsonResponse(resData)
        elif post_dict["action"] == "Undo request":
            resData["add_schedule"] = None
            utc_delta = request.session.get('utc_delta', 0)
            ReservationService.lesson_status_commit(user_id, post_dict["rvid"], post_dict["command"], post_dict["action"])
            resData["rv_data"] = ReservationService.get_reservation_detail(user_id, post_dict["rvid"], utc_delta )
        elif post_dict["action"] == "Change date/time":
            if post_dict['isTutor']:
                resData["add_schedule"] = None
                utc_delta = request.session.get('utc_delta', 0)
                ReservationService.lesson_status_commit(user_id, post_dict["rvid"], post_dict["command"], post_dict["action"])
                resData["rv_data"] = ReservationService.get_reservation_detail(user_id, post_dict["rvid"], utc_delta )
            else:
                ReservationService.lesson_status_commit(user_id, post_dict["rvid"], post_dict["command"], post_dict["action"])
                resData["rv_data"] = ReservationService.add_schedule_reserve(post_dict["rvid"], request)
            return JsonResponse(resData)
        elif post_dict["command"] == "Schedule" or post_dict["command"] == "Reschedule":
            if post_dict['isTutor']:
                resData["add_schedule"] = None
                utc_delta = request.session.get('utc_delta', 0)
                ReservationService.lesson_status_commit(user_id, post_dict["rvid"], post_dict["command"], post_dict["action"])
                resData["rv_data"] = ReservationService.get_reservation_detail(user_id, post_dict["rvid"], utc_delta )
            else:
                ReservationService.lesson_status_commit(user_id, post_dict["rvid"], post_dict["command"], post_dict["action"])
                resData["rv_data"] = ReservationService.add_schedule_reserve(post_dict["rvid"], request)
        elif post_dict["command"] == "Need confirmation" or post_dict["command"] == "Confirmed":
            utc_delta = request.session.get('utc_delta', 0)
            ReservationService.lesson_status_commit(user_id, post_dict["rvid"], post_dict["command"], post_dict["action"], post_dict["data"])
            resData["rv_data"] = ReservationService.get_reservation_detail(user_id, post_dict["rvid"], utc_delta )
            return JsonResponse(resData)
    except Exception as e:
            print '@@@@@tutoring_tutor_commit 1 : ', str(e)

    try:
        if post_dict['type'] == 'details':
            try:
                #confirm, feedback, problem
#                 post_dict['data']
                if post_dict['action'] == 'Report a problem' or post_dict['action'] == "problem_statement":
                    print "problem_statementproblem_statementproblem_statementproblem_statement"
                    if ReservationService.lesson_status_commit(user_id, post_dict["rvid"], post_dict["command"], post_dict["action"], post_dict['data']):
                        resData["add_schedule"] = None
                        utc_delta = request.session.get('utc_delta', 0)
                        resData["rv_data"] = ReservationService.get_reservation_detail(user_id, post_dict["rvid"], utc_delta )
                        return JsonResponse(resData)
                elif post_dict['action'] == "Canceled":
                    if ReservationService.lesson_status_commit(user_id, post_dict["rvid"], post_dict["command"], post_dict["action"], post_dict['data']):
                        resData["add_schedule"] = None
                        utc_delta = request.session.get('utc_delta', 0)
                        resData["rv_data"] = ReservationService.get_reservation_detail(user_id, post_dict["rvid"], utc_delta )
                        return JsonResponse(resData)
                else:
                    if ReservationService.lesson_status_commit(user_id, post_dict["rvid"], post_dict["command"], post_dict["action"]):
                        resData["add_schedule"] = None
                        utc_delta = request.session.get('utc_delta', 0)
                        resData["rv_data"] = ReservationService.get_reservation_detail(user_id, post_dict["rvid"], utc_delta )
                        return JsonResponse(resData)
            except Exception as e:
                print '@@@@@tutoring_tutor_commit 2 : ', str(e)
                print e

    except Exception as e:
        print '@@@@@tutoring_tutor_commit 3 : ', str(e)

#     if ts.set_resevation_commit(post_dict["rvid"], post_dict["command"]):
#         try:
#             if post_dict["type"] == 'details':
#                 resData["add_schedule"] = None
#                 print 'datils service'
#                 resData["rv_data"] = ts.get_reservation_details(user_id, post_dict["rvid"] )
#                 print resData
#                 return JsonResponse(resData)
#         except:
#                 
#             if int(post_dict["display"]) == 1:
#                 resData["rv_data"] = ts.get_tutor_reservations(ts.is_tutor(user_id), post_dict["status"], post_dict["page"])
#             else:
#                 resData["rv_data"] = ts.get_tutee_reservations(user_id, post_dict["status"], post_dict["page"])
     
                
    return JsonResponse(resData)


@tellpin_login_required
def tutoring_mylessons(request):
    """
    @summary: 사용자의 등록된 수업 리스트 진입 url
    @author: sj746
    @param request
    @return: url : tutoring/tutor_mylesson/tutor_mylesson.html
    """
    # resoponse data
    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id
    resData["is_tutor"] = request.user.tellpinuser.is_tutor
    resData["display"] = 13
    if resData['is_tutor']:
        resData["has_trial"] = request.user.tellpinuser.tutor.has_trial
        resData['trial_price'] = request.user.tellpinuser.tutor.trial_price
        resData["course_list"] = CourseService.course_list(user_id)

    return render_to_response("tutoring/tutor_mylesson/tutor_mylesson.html",
                              {"data": json.dumps(resData), "is_tutor": resData["is_tutor"],
                               "display": resData["display"]}, RequestContext(request))


@tellpin_login_required
def tutoring_new_lesson(request):
    """
    @summary: 새로운 수업 추가하는 진입 url
    @author: sj746
    @param request
    @return: url : tutoring/tutor_mylesson/tutor_lesson_add.html
    """
    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id
    resData["is_tutor"] = request.user.tellpinuser.is_tutor
    resData["display"] = 13
    resData["fromList"] = CommonService.skill_level_list_exclude_native()
    resData["toList"] = resData['fromList']
    resData["speak_list"] = CourseService.teaching_list(user_id)
    resData['courseImg'] = CourseService.course_bgimg(1)
    resData['user_currency'] = request.session.get('currency_exchange')
    resData['tabType'] = 1

    return render_to_response("tutoring/tutor_mylesson/tutor_lesson_add.html",
                              {"data": json.dumps(resData), "is_tutor": resData["is_tutor"],
                               "display": resData["display"]}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def tutoring_make_lesson(request):
    """
    @summary: 새로운 수업 추가하는 함수
    @author: sj746
    @param request
    @return: JsonResponse( resData )
    """
    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id
    jsonObj = json.loads(request.body)
    lessonObj = jsonObj["course"]
    lesson_type = jsonObj["type"]
    if lesson_type == 'trial':
        resData = CourseService.create_trial(user_id, lessonObj)
    else:
        resData = CourseService.create_course(user_id, lessonObj)
    print 'lesson', resData['isSuccess']

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def tutoring_stat_filter(request):
    """
    @summary: 예약 리스트 filter
    @author: msjang
    @param request
    @return: json
    """
    # resoponse data
    resData = {
        'isSuccess': 1
    }
    user_id = request.user.id
    # 파라미터 읽기
    post_dict = json.loads(request.body)
    print post_dict

    try:
        utc_time = request.session.get('utc_delta', 0)
        if int(post_dict["display"]) == 1:
            if post_dict["search_name"] is not None:
                resData["rv_data"] = ReservationService.get_tutor_reservations(
                    user_id, utc_time, None, post_dict["page"], None, post_dict["search_name"]
                )
            else:
                resData["rv_data"] = ReservationService.get_tutor_reservations(
                    user_id, utc_time, post_dict["status"], post_dict["page"], post_dict["filterType"],
                    None
                )
        else:#display = 2
            if post_dict["search_name"] is not None:
                resData["rv_data"] = ReservationService.get_tutee_reservations(
                    user_id, utc_time, None, post_dict["page"], None, post_dict["search_name"]
                )
            else:
                resData["rv_data"] = ReservationService.get_tutee_reservations(
                    user_id, utc_time, post_dict["status"], post_dict["page"], None
                )
    except Exception as e:
        print e

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def tutoring_package_stat_filter(request):
    """
    @summary: 예약 리스트 filter
    @author: msjang
    @param request
    @return: json
    """
    # resoponse data
    resData = {
        'isSuccess': 1
    }
    user_id = request.user.id
    # 파라미터 읽기
    post_dict = json.loads(request.body)
    print "tutoring_package_stat_filter"
    print post_dict

    try:
        utc_time = request.session.get('utc_delta', 0)
        if int(post_dict["display"]) == 15:
            if post_dict["search_name"] is not None:
                resData["rv_data"] = ReservationService.get_tutor_package_lesson(
                    user_id, utc_time, None, post_dict["page"], None, post_dict["search_name"]
                )
            else:
                print "filter!!!!!!!!!!!"
                resData["rv_data"] = ReservationService.get_tutor_package_lesson(
                    user_id, utc_time, None, post_dict["page"], post_dict["filterType"], None
                )
        else:#display = 16
            if post_dict["search_name"] is not None:
                print "16  tutoring_stat_filter"
                resData["rv_data"] = ReservationService.get_tutee_package_lesson(
                    user_id, utc_time, None, post_dict["page"], None, post_dict["search_name"]
                )
            else:
                resData["rv_data"] = ReservationService.get_tutee_package_lesson(
                    user_id, utc_time, None, post_dict["page"], post_dict["filterType"], None
                )
    except Exception as e:
        print '@@@@@tutoring_package_stat_filter : ', str(e)

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def ajax_problem_accept(request):
    """
    @summary: 문제제기 접수
    @author: msjang
    @param request
    @return: json
    """
    # resoponse data
    resData = {}
    resData["isSuccess"] = 1
    # get request parameter
    post_dict = json.loads(request.body)
    print post_dict
    ReservationService.set_problem_accept(post_dict["rvid"], post_dict["type"])

    return JsonResponse(resData)


@tellpin_login_required
def problem_decline(request, rvid):
    """
    @summary: course problem decline
    @author: msjang
    @param request
    @param rvid
    @return: url - tutoring/tutoring_details_problem.html
    """
    print 'test'
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    resData["rv_data"] = ReservationService.get_problems_details(rvid, user_id, False)
    resData["reporter_id"] = user_id
    resData = json.dumps(resData)

    return render_to_response("tutoring/tutoring_details_problem.html", {'data': resData}, RequestContext(request))


# add trial lesson
@csrf_exempt
@tellpin_login_required
def ajax_newTrial(request):
    """
    @summary: trial 강의를 추가 함수(강의타입(0:trial, 1:정식강의)
    @author: sj746
    @param request
    @return: JsonResponse( resData )
    """
    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)
    trial_price = post_dict["trial_price"]
    has_trial = post_dict['has_trial']
    resData["isSuccess"] = CourseService.create_trial(user_id, has_trial, trial_price)
    resData["isTrial"] = CourseService.get_trial(user_id)

    return JsonResponse(resData)


def tutoring_modify_lesson(request):
    """
    @summary: 등록된 수업을 편집하는 진입 url
    @author: sj746
    @param request
    @return: url : tutoring/tutor_mylesson/tutor_lesson_add.html
    """
    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id

    if request.POST["course_id"]:
        course_id = request.POST["course_id"]

    if request.user.tellpinuser.is_tutor:
        resData["is_tutor"] = user_id
    else:
        resData['is_tutor'] = None
    resData["display"] = 13
    newLesson = NewLessonService.NewLesson()
    resData["fromList"] = CommonService.skill_level_list()
    resData["toList"] = resData['fromList']
    resData["speak_list"] = CourseService.teaching_list(user_id)
    resData['courseImg'] = CourseService.course_bgimg()
    resData['tabType'] = 1
    resData["course"] = CourseService.get_course(course_id)
    resData['user_currency'] = request.session.get('currency_exchange')

    return render_to_response("tutoring/tutor_mylesson/tutor_lesson_add.html",
                              {"data": json.dumps(resData), "is_tutor": resData["is_tutor"],
                               "display": resData["display"]}, RequestContext(request))


@tellpin_login_required
def tutoring_tutor_schedule(request):
    """
    @summary: 튜터의 수업 가능한 일정 달력을 보여주는  url
    @author: sj746
    @param request
    @return: url : tutoring/tutor_schedule/tutor_schedule.html
    """

    resData = {
        'isSuccess': 1
    }
    user_id = request.user.id
    user_utc = request.session["utc_delta"]
    resData["is_tutor"] = request.user.tellpinuser.is_tutor
    resData["display"] = 13

    # tutor schedule
    if resData["is_tutor"]:
        resData["week_schedule"] = ScheduleService.get_week_schedule(user_id, user_utc)

    jsonData = json.dumps(resData)

    return render_to_response("tutoring/tutor_schedule/tutor_schedule.html",
                              {"schedule": jsonData, "is_tutor": resData["is_tutor"], "display": 12},
                              RequestContext(request))


@csrf_exempt
@tellpin_login_required
def add_schedule(request):
    """
    @summary: 튜터의 주간 단위 스케쥴을 저장하는 함수
    @author: sj746
    @param request
    @return: JsonResponse( resData )
    """
    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id
    user_utc = request.session["utc_time"]
    if not request.user.tellpinuser.is_tutor:
        user_id = None
    post_dict = json.loads(request.body)
    resData["week_schedule"] = trs.save_week_schedule(user_id, post_dict["day_of_week"],
                                                      int(post_dict["fromTime"]), int(post_dict["toTime"]),
                                                      int(post_dict["fn"]), user_utc)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def set_schedule(request):
    """
    @summary: 튜터의 주간 단위 스케쥴을 저장/삭제 하는 함수
    @author: sj746
    @param request
    @return: JsonResponse( resData )
    """
    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id
    user_utc = request.session["utc_delta"]
    if not request.user.tellpinuser.is_tutor:
        user_id = None
    post_list = json.loads(request.body)

    # delete all the time
    for schedule in post_list["dellist"]:
        resData["week_schedule"] = ScheduleService.save_week_schedule(user_id, schedule['day_of_week'],
                                                                      int(schedule['fromTime']), int(schedule['toTime']),
                                                                      int(schedule['fn']), user_utc)

    # add selected time
    for schedule in post_list["addlist"]:
        resData["week_schedule"] = ScheduleService.save_week_schedule(user_id, schedule['day_of_week'],
                                                                      int(schedule['fromTime']), int(schedule['toTime']),
                                                                      int(schedule['fn']), user_utc)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def get_calendar(request):
    """
    @summary: 스케쥴 화면에서 달력을 보여주기 위해 사용자의 시간으로 월간 일정을 보여주는 함수
    @author: sj746
    @param request
    @return: JsonResponse( resData )
    """
    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id
    user_utc = request.session["utc_delta"]

    if request.user.tellpinuser.is_tutor:
        tid = user_id
    else:
        tid = False
    year_month, now_year, now_month, now_day = ScheduleService.get_date()
    resData["month"] = now_month
    resData["year"] = now_year
    resData["month_info"] = ScheduleService.get_month_info(now_year, now_month)
    resData["month_schedule"] = ScheduleService.get_month_schedule(tid, year_month, user_utc)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def get_prev_calendar(request):
    """
    @summary: 스케쥴 화면에서 달력을 보여주기 위해 사용자의 시간으로 월간 일정을 보여주는 함수 - 현재 날짜의 전 달의 달력을 계산
    @author: sj746
    @param request
    @return: JsonResponse( resData )
    """
    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id
    user_utc = request.session["utc_delta"]
    if not request.user.tellpinuser.is_tutor:
        user_id = None
    post_dict = json.loads(request.body)
    now_year = post_dict["year"]
    now_month = post_dict["month"]
    now_month -= 1

    if now_month == 0:
        now_month = 12
        now_year -= 1

    calendar_date = date(now_year, now_month, 1)
    year_month = calendar_date.strftime("%Y%m")

    resData["month"] = now_month
    resData["year"] = now_year
    resData["month_info"] = ScheduleService.get_month_info(now_year, now_month)
    resData["month_schedule"] = ScheduleService.get_month_schedule(user_id, year_month, user_utc)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def get_next_calendar(request):
    """
    @summary: 스케쥴 화면에서 달력을 보여주기 위해 사용자의 시간으로 월간 일정을 보여주는 함수 - 현재 날짜의 다음 달의 달력을 계산
    @author: sj746
    @param request
    @return: JsonResponse( resData )
    """

    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id
    user_utc = request.session["utc_delta"]
    if not request.user.tellpinuser.is_tutor:
        user_id = None
    post_dict = json.loads(request.body)
    now_year = post_dict["year"]
    now_month = post_dict["month"]
    now_month += 1

    if now_month == 13:
        now_month = 1
        now_year += 1

    calendar_date = date(now_year, now_month, 1)
    year_month = calendar_date.strftime("%Y%m")
    resData["month"] = now_month
    resData["year"] = now_year
    resData["month_info"] = ScheduleService.get_month_info(now_year, now_month)
    resData["month_schedule"] = ScheduleService.get_month_schedule(user_id, year_month, user_utc)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def add_delta_schedule(request):  # 이름 변경 add_delta_schedule => add_monthly_schedule
    """
    @summary: 사용자의 월간 일정 중 특정한 날의 수업 가능한 레슽 시간을 저장하는 함수
    @author: sj746
    @param request
    @return: JsonResponse( resData )
    """
    resData = {
        'isSuccess': 1
    }
    user_id = request.user.id
    user_utc = request.session["utc_delta"]
    if request.user.tellpinuser.is_tutor:
        tid = user_id
    else:
        tid = None
    post_dict = json.loads(request.body)
    resData["month_schedule"] = ScheduleService.add_month_schedule(tid, post_dict["date"], post_dict["fromTime"],
                                                                   post_dict["toTime"], user_utc)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def delete_delta_schedule(request):
    """
    @summary: 튜터가 등록한 월간 레슨 가능한 날짜중 특정일정을  삭제하는 함수
    @author: sj746
    @param request
    @return: JsonResponse( resData )
    """
    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id
    user_utc = request.session["utc_delta"]
    if not request.user.tellpinuser.is_tutor:
        user_id = None
    post_dict = json.loads(request.body)
    resData["month_schedule"] = ScheduleService.delete_month_schedule(user_id,
                                                                      post_dict["date"], post_dict["fromTime"],
                                                                      post_dict["toTime"], user_utc)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def delete_lesson(request):
    """
    @summary: 튜터의 등록된 수업을 삭제하는 함수
    @author: sj746
    @param request
    @return: JsonResponse( resData )
    """
    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id
    if request.user.tellpinuser.is_tutor:
        resData["is_tutor"] = user_id
    else:
        resData["is_tutor"] = None

    post_dict = json.loads(request.body)
    course_id = post_dict["course_id"]
    resData["isSuccess"] = CourseService.delete_course(course_id, user_id)
    resData["course_list"] = CourseService.course_list(user_id)

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def tutor_schedule_pop(request):
    """
    @summary: get tutor schedule calendar
    @author: khyun
    @param request
    @return: json
    """
    # resoponse json
    print 'tutor_schedule()'
    resData = dict()
    resData["isSuccess"] = 1
    user_utc = request.session["utc_delta"]
    now = datetime.utcnow() + timedelta(days=1)
    utc_delta = request.session.get('utc_delta', 0)
#     utc_time = request.session.get('utc_time', 0)
    resData["isSuccess"] = 1

    # get request parameter
    post_dict = json.loads(request.body)

    resData["rv_data"] = ReservationService.get_reservation_detail(post_dict["tid"], post_dict["rvid"], utc_delta)
    resData["today"] = ScheduleService.get_year_month_day(user_utc)
    resData["schedule"] = ScheduleService.get_tutor_schedule(post_dict["tid"], user_utc, now)
    resData['reserved'] = ScheduleService.get_reserved_schedule(post_dict["tid"], user_utc, now)

    return JsonResponse(resData)

# @csrf_exempt
# @tellpin_login_required
# def change_lesson_date(request):
#     """
#     @summary: 이미 예약된 수업의 날짜를 바꾼다.
#     @author: sjkim
#     @param request
#     @return: json
#     """
#     resData = dict()
#     resData["isSuccess"] = 1
#  
#     return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def delete_range_schedule(request):
    """
    @summary: 사용자의 특정 시간의 일정을 지우는 함수
    @author: sj746
    @param request
    @return: JsonResponse( resData )
    """
    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id
    user_utc = request.session["utc_delta"]
    if not request.user.tellpinuser.is_tutor:
        user_id = None
    post_dict = json.loads(request.body)
    resData["month_schedule"] = ScheduleService.delete_range_schedule(user_id,
                                                                      post_dict["year"], post_dict["month"],
                                                                      post_dict["fromDate"], post_dict["toDate"],
                                                                      user_utc)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def delete_range_schedule_for_mobile(request):
    """
    @summary: 사용자의 특정 시간의 일정을 지우는 함수
    @author: sj746
    @param request
    @return: JsonResponse( resData )
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    tid = TutorService.is_tutor(user_id)
    post_dict = json.loads(request.body)
    print post_dict
    trs.delete_range_schedule_for_mobile(tid, post_dict["fromDate"], post_dict["toDate"])

    return JsonResponse(resData)


# trial change
@csrf_exempt
@tellpin_login_required
def ajax_changeTrial(request):
    """
    @summary: trial 강의를 편집하는 함수
    @author: sj746
    @param request
    @return: JsonResponse( resData )
    """
    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)
    trial_price = post_dict["trial_price"]
    has_trial = post_dict['has_trial']
    resData["isSuccess"] = CourseService.create_trial(user_id, has_trial, trial_price)
    resData["isTrial"] = CourseService.get_trial(user_id)
    print resData["isTrial"]

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def tutoring_package_details(request, rvid):
    """
    @summary: tutee일때 예약 강의 리스트 정보를 보여주는 상세 페이지 진입 url
    @author: sj746
    @param request
    @param rvid : 예약 강의 id
    @return: url : tutoring/tutoring_details.html
    """
        # response data
    resData = {}
    user_id = request.user.id
    utc_delta = request.session.get('utc_delta', 0)
    utc_time = request.session.get('utc_time', 0)
    resData["isSuccess"] = 1

    resData["rv_data"] = ReservationService.get_reservation_pkg_detail(user_id, rvid, utc_delta)
    resData["rv_data"]['utc_time'] = utc_time
    resData["rv_data"]["available_balance"] = request.user.tellpinuser.tutee.available_balance
    resData["rv_data"]["total_balance"] = request.user.tellpinuser.tutee.total_balance
    resData["rv_data"]["pending_balance"] = request.user.tellpinuser.tutee.pending_balance
    print 'tutoring_package_details'
    resData1 = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("tutoring/tutoring_pkg_details.html",
                          {'data': resData1, 'title': resData['rv_data']['is_tutor']}, RequestContext(request))

@csrf_exempt
@tellpin_login_required
def tutoring_details(request, rvid):
    """
    @summary: tutee일때 예약 강의 리스트 정보를 보여주는 상세 페이지 진입 url
    @author: sj746
    @param request
    @param rvid : 예약 강의 id
    @return: url : tutoring/tutoring_details.html
    """
    # resoponse data
    resData = {}
    user_id = request.user.id
    utc_delta = request.session.get('utc_delta', 0)
    utc_time = request.session.get('utc_time', 0)
    resData["isSuccess"] = 1

    resData["rv_data"] = ReservationService.get_reservation_detail(user_id, rvid, utc_delta)
    resData["rv_data"]['utc_time'] = utc_time
    resData1 = json.dumps(resData, cls=DjangoJSONEncoder)
    print 'tutoring_details'

    return render_to_response("tutoring/tutoring_details.html",
                              {'data': resData1, 'title': resData['rv_data']['is_tutor']}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def tutoring_details_problem(request, rvid):
    """
    @summary: 튜터의 강의에 신고된 문제점 리스트 진입 url
    @author: sj746
    @param request
    @param rvid : 예약 강의 id
    @return: url : tutoring/tutoring_details_problemlist.html (문제점이 여러개 일 때), tutoring/tutoring_details_problem.html(문제가 하나일 때)
    """

    # resoponse data
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    # TODO data
    lessonClaimCount = ReservationService.get_problems(rvid)

    if lessonClaimCount > 0:
        resData["rv_data"] = ReservationService.get_problems_details_list(rvid, user_id)
        resData = json.dumps(resData)
        return render_to_response("tutoring/tutoring_details_problemlist.html", {'data': resData},
                                  RequestContext(request))
    else:
        resData["rv_data"] = ReservationService.get_problems_details(rvid, user_id)
        resData["reporter_id"] = user_id
        resData = json.dumps(resData)
        return render_to_response("tutoring/tutoring_details_problem.html", {'data': resData}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def tutorPendingLanguage(request):
    """
    @summary: 튜터의 언어 변경에 대한 저장 이전에 관리자가 확인 후 저장할 수 있도록 우선 pending db에 저장하는 함수
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)
    profile = post_dict["profile"]
    ts4r.pending_tutor_language(profile)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def getCourseImg(request):
    """
    @summary: 강의 등록 시 필요한 배경 이미지를 가져오는 함수
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = dict()
    resData["isSuccess"] = 1
    post = json.loads(request.body)
    img_type = post['type']
    resData['courseImg'] = CourseService.course_bgimg(img_type)
    resData['tabType'] = img_type

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def tutor_available(request):
    """
    @summary: 튜터 강의 가능 여부
    @param request
    @return: json
    """
    resData = dict()
    user_id = request.user.id
    resData = ScheduleService.get_course_available(user_id)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def set_tutor_available(request):
    """
    @summary: 튜터 강의 가능 여부 설정
    @param request
    @return: json
    """
    resData = dict()
    user_id = request.user.id
    post_dict = json.loads(request.body)
    is_available = post_dict['is_available']
    if ScheduleService.set_course_available(user_id, is_available):
        resData = ScheduleService.get_course_available(user_id)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def set_schedule_comment(request):
    """
    @summary: 튜터 스케줄 코멘트 설정
    @param request
    @return: json
    """
    resData = dict()
    user_id = request.user.id
    post_dict = json.loads(request.body)
    schedule_comment = post_dict['schedule_comment']
    if ScheduleService.set_schedule_comment(user_id, schedule_comment):
        resData = ScheduleService.get_course_available(user_id)

    return JsonResponse(resData)
