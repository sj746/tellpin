from django.conf.urls import url
import views

urlpatterns = [
    url( r'^questions/$', views.listQna ), # qna list
    url( r'^questions/ajaxGetLqList/$', views.ajaxGetLqList ), # qna list
    url( r'^questions/(?P<q_id>\d+)/$', views.detailQna ), # qna detail page
    url( r'^questions/write/$', views.writeQna ), # write qna
#    url( r'^qna/edit/(?P<qid>\d+)/$', views.editQna ),
#    url( r'^qna/edit/answer/(?P<aid>\d+)/$', views.editQna_answer ),
#    url( r'^qna/edit/comment/(?P<cid>\d+)/$', views.editQna_comment ),
    url( r'^questions/vote/$', views.ajax_vote ), # do vote
    url( r'^questions/pin/$', views.ajax_pin ), # do like
#     url( r'^questions/myqna/$', views.ajax_myqna ), # get my question
    url( r'^questions/myquestion/$', views.ajax_myQustion ), # get my question
    url( r'^questions/myanswer/$', views.ajax_myAnswer ), # get myanswer
    url( r'^questions/search_question/$', views.ajax_search_question ), # get myanswer
    url( r'^questions/post/$', views.postQna ), # write qna
    url( r'^questions/removeqna/$', views.remove_qna ), # remove_tp
    url( r'^questions/editqna/$', views.modify_qna ), # remove_tp
    url( r'^questions/modify/$', views.updateQna ), # write qna
    url( r'^questions/addcm/$', views.ajax_addComment ), # add comment
    url( r'^questions/removeanswer/$', views.removeAnswer ), # remove comment
    url( r'^questions/removecm/$', views.ajax_removeComment ), # remove comment
    url( r'^questions/allquestion/$', views.ajax_allQna ), # get all question
    url( r'^questions/image/$', views.upload_file ), # image pre-upload qna
    url( r'^questions/violation/$', views.ajax_addReport ), # add report
    url( r'^questions/answerViolation/$', views.ajax_addanswerReport ), # add report
    url( r'^questions/answer/$', views.ajax_addAnswer ), # add answer
    url( r'^questions/bestanswer/$', views.ajax_selectBestAnswer ), # select best answer
    url( r'^questions/editanswer/$', views.qna_editAnswer ), # Translation&Proofreading answer
    url( r'^questions/moreqnaList/$', views.moreListQna ), # QNA more list
    
#    Translation&Proofreading
    url( r'^trans/$', views.listTP ), # Translation&Proofreading list
    url( r'^trans/ajaxGetTpList/$', views.ajaxGetTpList ), # Translation&Proofreading list
    url( r'^trans/(?P<q_id>\d+)/$', views.detailTP ), # Translation&Proofreading detail page
    url( r'^trans/write/$', views.write_tp), # write qna

    url( r'^trans/both/$', views.list_all_TP ), # Translation&Proofreading ajax list
#     url( r'^trans/mytp/$', views.ajax_mytp ), # get my tp list
    url( r'^trans/translation/$', views.list_translation ), # Translation ajax list
    url( r'^trans/proofreading/$', views.list_proofreading ), # Proofreading ajax list
    url( r'^trans/vote/$', views.tp_vote ), # Translation&Proofreading vote
    url( r'^trans/pin/$', views.tp_pin ), # Translation&Proofreading pin
    url( r'^trans/violation/$', views.tp_addReport ), # Translation&Proofreading report
    url( r'^trans/answerViolation/$', views.tp_addanswerReport ), # Translation&Proofreading report
    url( r'^trans/removecm/$', views.tp_removeComment ), # Translation&Proofreading comment
    url( r'^trans/removeanswer/$', views.tp_removeAnswer ), # Translation&Proofreading comment
    url( r'^trans/answer/$', views.tp_addAnswer ), # Translation&Proofreading answer
    url( r'^trans/editanswer/$', views.tp_editAnswer ), # Translation&Proofreading answer
    url( r'^trans/addcm/$', views.tp_addComment ), # add comment
    url( r'^trans/bestanswer/$', views.tp_selectBestAnswer ), # select best answer
    url( r'^trans/post/$', views.post_tp ), # write qna
    url( r'^trans/removetp/$', views.remove_tp ), # remove_tp
    url( r'^trans/editTP/$', views.modify_tp ), # remove_tp
    url( r'^trans/modify/$', views.update_tp ), # write qna
    url( r'^trans/moreTPList/$', views.moreTPList ), # more TP list

#     Language board
    url( r'^board/$', views.initLb ), # language_board
    url(r'^board/ajaxGetLbList/$', views.ajaxGetLbList),
    url( r'^board/change_lb/$', views.change_lb ),
    url( r'^board/search_boards/$', views.search_lb ),
    url( r'^board/(?P<pid>\d+)/$', views.deatail_lb ),
#     url( r'^board/mylb/$', views.ajax_get_mylb ), # get my lb list
    url( r'^board/add_comment/$', views.post_lb_comment),
    url( r'^board/delete_comment/$', views.delete_lb_comment),
    url( r'^board/pin/$', views.pin_lb),
    url( r'^board/vote/$', views.vote_lb),
    url( r'^board/write/$', views.write_lb),
    url( r'^board/write/upload_media/$', views.upload_media),
    url( r'^board/write/upload_video_preview/$', views.upload_video_preview),
    url( r'^board/post/$', views.post_lb),
    url( r'^board/updatepost/$', views.update_lb),
    url( r'^board/violation/$', views.report_lb),
    url( r'^board/get_more_lb/$', views.get_more_lb),
    url( r'^board/removelb/$', views.remove_lb),
    url( r'^board/editpost/$', views.modify_lb ), # remove_tp
]
