# -*- coding: utf-8 -*-

###############################################################################
# filename    : community > service.py
# description : community 화면을 구성하는데 필요한 정보를 얻기 위한 service file
# author      : sj746@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 j746 최초 작성
#
#
###############################################################################

import datetime
import json
import logging
import os
import time

from django.forms.models import model_to_dict
from django.http.response import HttpResponse, HttpResponseRedirect, \
    JsonResponse
from django.shortcuts import render, render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.core.serializers.json import DjangoJSONEncoder

from common import common
from common.common import tellpin_login_required
# from common.models import LqAnswer, LqComment, LqPin, LqQuestion, LqReport, LqVote, \
#     Files
from user_auth.models import File
from community.service import Community
from dashboard.service import DashboardService
from mypage.service import MyPageService
from mypage.views import DATA_ROOT
from tellpin.settings import BASE_DIR
from tutor.service4registration import TutorService4Registration
from common.models import LanguageInfo
from tutor.models import Tutee
from common.service import CommonService

logger = logging.getLogger("mylogger")
ms = MyPageService(object)
ts4r = TutorService4Registration(object)


# get Question list
def listQna(request):
    """
    @summary: Language Question 리스트로 진입하는 url
    @author: sj46
    @param request
    @return: url - community/qna_list.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    resData["userInfo"] = model_to_dict(Tutee.objects.get(user=user_id),
                                                         exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])

    resData["languageInfo"] = CommonService.language_list()
    jsonData = json.dumps(resData)

    return render_to_response("community/qna_list.html", {"data": jsonData}, RequestContext(request))


@csrf_exempt
def ajaxGetLqList(request):
    """
    @summary: Language Question 리스트로 진입하는 url
    @author: sj46
    @param request
    @return: url - community/qna_list.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    try:
        resData["LQs"] = Community.get_language_question_list(user_id, 0)
        resData["nationsInfo"] = CommonService.country_list()
        return JsonResponse(resData)
    except Exception as e:
        print 'ajaxGetQnaList error'
        print e
        return JsonResponse(e)


@csrf_exempt
def moreListQna(request):
    """
    @summary: Language Question 리스트 화면에서 스크롤 동작에 의한 추가 리스트를 보여주기 위한 url
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    post_dict = json.loads(request.body)
    resData["LQs"] = Community.get_language_question_list(user_id, post_dict["start"])

    return JsonResponse(resData)


def listTP(request):
    """
    @summary: Trans & proofreading 초기 진입 url
    @author: sj46
    @param request
    @return: url - community/tp_list.html
    """

    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    resData["userInfo"] = model_to_dict(Tutee.objects.get(user=user_id),
                                                         exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])

    resData["languageInfo"] = CommonService.language_list()
    jsonData = json.dumps(resData)

    return render_to_response("community/tp_list.html", {"data": jsonData}, RequestContext(request))


@csrf_exempt
def ajaxGetTpList(request):
    """
    @summary: Trans & proofreading ajax 로 리스트 구성
    @author: sj46
    @param request
    @return: 처음 진입 시 보여주어야 할 tp list
    """

    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    try:
        resData["TPs"] = Community.get_translation_proofreading_list(user_id, 0, 0, 0, 0)
        resData["nationsInfo"] = CommonService.country_list()
        return JsonResponse(resData)
    except Exception as e:
        print 'ajaxGetTpList error'
        print e
        return JsonResponse(e)


@csrf_exempt
def moreTPList(request):
    """
    @summary: trans&proof 리스트 화면에서 스크롤 동작에 의한 추가 리스트를 보여주기 위한 url
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    post_dict = json.loads(request.body)
    resData["TPs"] = Community.get_translation_proofreading_list(user_id, post_dict["start"], 0, 0, 0)

    return JsonResponse(resData)


@csrf_exempt
def list_translation(request):
    """
    @summary: trans&proof 리스트 구성 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    post_dict = json.loads(request.body)
    resData["TPs"] = Community.get_translation_list(user_id, post_dict["fromLang"], post_dict["toLang"],
                                                    post_dict["start"])

    return JsonResponse(resData)


@csrf_exempt
def list_all_TP(request):
    """
    @summary: trans&proof 모든 리스트 구성 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    post_dict = json.loads(request.body)
    resData["TPs"] = Community.get_translation_proofreading_list(user_id, post_dict["start"], post_dict["fromLang"],
                                                                 post_dict["toLang"], post_dict["inLang"])

    return JsonResponse(resData)


@csrf_exempt
def list_proofreading(request):
    """
    @summary: proofreading 의 모든 리스트 구성 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    post_dict = json.loads(request.body)
    resData["TPs"] = Community.get_proofreading_list(user_id, post_dict["inLang"], post_dict["start"])

    return JsonResponse(resData)


@csrf_exempt
def ajax_allQna(request):
    """
    @summary: language question 의 모든 리스트 구성 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    resData["LQs"] = Community.get_language_question_list(user_id, 0)

    return JsonResponse(resData)


# get My Qna
@csrf_exempt
@tellpin_login_required
def ajax_myQustion(request):
    """
    @summary: 내가 작성한 language question 의 모든 리스트 구성 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    resData["LQs"] = Community.get_my_language_question_list(user_id , 0)

    return JsonResponse(resData)


# get my Answer
@csrf_exempt
@tellpin_login_required
def ajax_myAnswer(request):
    """
    @summary: 내가 작성한 language question 의 모든  답변 리스트 구성 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    resData["LQs"] = Community.get_language_question_answer_list(user_id, 0)

    return JsonResponse(resData)


@csrf_exempt
def ajax_search_question(request):
    """
    @summary: language question 의  리스트를  가져오는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    post_dict = json.loads(request.body)

    resData["LQs"] = Community.get_language_question_search_list(user_id, 0, post_dict["written"],
                                                                 post_dict["translated"])

    return JsonResponse(resData)


# get detail Question
def detailQna(request, q_id):
    """
    @summary: language question 의  상세보기 화면 진입 url
    @author: sj46
    @param q_id : 질문 id
    @return: url : community/qna_detail.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    Community.add_language_question_views(q_id)
    resData["questionInfo"] = Community.get_language_question_item(q_id, user_id)
    resData["languageInfo"] = CommonService.language_list()
    resData["nationsInfo"] = CommonService.country_list()
    resData["userInfo"] = model_to_dict(Tutee.objects.get(user=user_id),
                                                             exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
    jsonData = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("community/qna_detail.html",
                              {
                                  "data": jsonData,
                                  'title': resData['questionInfo']['question']['title']
                              },
                              RequestContext(request))


def detailTP(request, q_id):
    """
    @summary: trans&proof 의  상세보기 화면 진입 url
    @author: sj46
    @param q_id : 질문 id
    @return: url : community/tp_detail.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    Community.add_translation_proofreading_views(q_id)
    resData["questionInfo"] = Community.get_translation_proofreading_item(q_id, user_id)
    resData["languageInfo"] = CommonService.language_list()
    resData["nationsInfo"] = CommonService.country_list()
    resData["userInfo"] = model_to_dict(Tutee.objects.get(user=user_id),
                                                             exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
    print "detailTPdetailTPdetailTP"
    print resData
    jsonData = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("community/tp_detail.html",
                              {
                                  "data": jsonData,
                                  'title': resData['questionInfo']['question']['title']
                              },
                              RequestContext(request))


# write page( add languagelist )
@tellpin_login_required
def writeQna(request):
    """
    @summary: language question 질문 작성 화면 진입 url
    @author: sj46
    @param request
    @return: url : community/qna_write.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    resData["languageInfo"] = CommonService.language_list()
    resData["bestanswercreditInfo"] = [0, 2, 4, 6, 8, 10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 70, 80, 90, 100]
    resData["availablecoin"] = common.getTuteeTotalAvailable(user_id)
    resData["userInfo"] = model_to_dict(Tutee.objects.get(user=user_id),
                                                         exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
    jsonData = json.dumps(resData)

    return render_to_response("community/qna_write.html", {"data": jsonData}, RequestContext(request))


# write page( add languagelist )
@tellpin_login_required
def write_tp(request):
    """
    @summary: trans&proof 질문 작성 화면 진입 url
    @author: sj46
    @param request
    @return: url : community/tp_write.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    resData["languageInfo"] = CommonService.language_list()
    resData["bestanswercreditInfo"] = [0, 2, 4, 6, 8, 10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 70, 80, 90, 100]
#     resData["availablecoin"] = common.getTuteeTotalAvailable(user_id)
    resData["userInfo"] = model_to_dict(Tutee.objects.get(user=user_id),
                                                         exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
    jsonData = json.dumps(resData)

    return render_to_response("community/tp_write.html", {"data": jsonData}, RequestContext(request))


@csrf_exempt
def post_tp(request):
    """
    @summary: trans&proof 질문 작성 후 저장하기 위한 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    if "img" in request.FILES.keys():
        imgfile = request.FILES["img"]
    else:
        imgfile = None

    resData["postURL"], resData["newPid"] = Community.create_translation_proofreading(user_id, request.POST["title"], request.POST["content"],
                                                       request.POST["written_id"], request.POST["written_name"], request.POST["translated_id"], request.POST["translated_name"],
                                                       request.POST["best_answer_credit"], request.POST["type"], imgfile)

#     if int(request.POST["best_answer_credit"]):
#         common.insertTuteeAccount(user_id, int(request.POST["best_answer_credit"]), 11, 7, resData["newPid"])

    Community.set_notification_trans_post(user_id, resData["newPid"], request.POST["type"])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def update_tp(request):
    """
    @summary: trans&proof 작성된 질문을 편집하기  위한 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    if "img" in request.FILES.keys():
        imgfile = request.FILES["img"]
    else:
        imgfile = None

    resData["postURL"], resData["newPid"] = Community.update_translation_proofreading(user_id, request.POST["qid"], request.POST["title"], request.POST["content"],
                                                       request.POST["written_id"], request.POST["written_name"], request.POST["translated_id"], request.POST["translated_name"],
                                                       request.POST["best_answer_credit"], request.POST["type"], imgfile)

#     if int(request.POST["best_answer_credit"]) != int(request.POST["initPoint"]):
#         common.insertTuteeAccount(user_id, int(request.POST["best_answer_credit"]), 11, 7, resData["newPid"])

#     Community.set_notification_trans_post(user_id, resData["newPid"], request.POST["type"])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def modify_tp(request):
    """
    @summary: trans&proof 작성된 질문을 편집하기 위한 화면으로 진입하는 url
    @author: sj46
    @param request
    @return: url : community/tp_write.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    resData["id"] = request.POST["qid"]
    resData["languageInfo"] = CommonService.language_list()
    resData["bestanswercreditInfo"] = [0, 2, 4, 6, 8, 10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 70, 80, 90, 100]
    resData["availablecoin"] = common.getTuteeTotalAvailable(user_id)
    resData["userInfo"] = model_to_dict(Tutee.objects.get(user=user_id),
                                                         exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
    resData["data"] = Community.get_translation_proofreading_item(resData["id"], user_id)
    jsonData = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("community/tp_write.html", {"data": jsonData}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def remove_lb(request):
    """
    @summary: language board 작성된 post을 삭제하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)
    Community.delete_language_board(post_dict["pid"])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def remove_tp(request):
    """
    @summary: trans&proof 작성된 질문을 삭제하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    post_dict = json.loads(request.body)
#     point = -(post_dict["best_answer_credit"])
    Community.delete_translation_proofreading(post_dict["qid"])

#     if post_dict["best_answer_credit"]:
#         common.insertTuteeAccount(user_id, point, 11, 7, post_dict["id"])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def remove_qna(request):
    """
    @summary: language question 작성된 질문을 삭제하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    post_dict = json.loads(request.body)
    point = -(post_dict["best_answer_credit"])
    Community.delete_language_question(post_dict["lq_id"])

#     if post_dict["best_answer_credit"]:
#         common.insertTuteeAccount(user_id, point, 11, 6, post_dict["id"])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def postQna(request):
    """
    @summary: language Question 포스트 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    if "img" in request.FILES.keys():
        imgfile = request.FILES["img"]
    else:
        imgfile = None

    resData["postURL"], id = Community.create_language_question(user_id, request.POST["title"], request.POST["content"],
                                         request.POST["written_lang"], request.POST["written_lang_name"], request.POST["question_lang"], request.POST["question_lang_name"],
                                         request.POST["best_answer_credit"], imgfile)

#     if int(request.POST["best_answer_credit"]):
#         common.insertTuteeAccount(user_id, int(request.POST["best_answer_credit"]), 11, 6, id)

    Community.set_notification_qna_post(user_id, id)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def modify_lb(request):
    """
    @summary: language board 작성된 post를 편집하기 위한 화면으로 진입하는 url
    @author: sj46
    @param request
    @return: url : community/languageboard_write.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    resData["userInfo"] = model_to_dict(Tutee.objects.get(user=user_id),
                                                         exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
    resData["languageInfo"] = CommonService.language_list()
    resData["nationsInfo"] = CommonService.country_list()
    resData["post"] = Community.get_language_board_item(user_id, request.POST["pid"])
    jsonData = json.dumps(resData)

    return render_to_response("community/languageboard_write.html", {"data": jsonData}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def modify_qna(request):
    """
    @summary: language question 작성된 post를 편집하기 위한 화면으로 진입하는 url
    @author: sj46
    @param request
    @return: url : community/qna_write.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    resData["id"] = request.POST["qid"]
    resData["languageInfo"] = CommonService.language_list()
    resData["bestanswercreditInfo"] = [0, 2, 4, 6, 8, 10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 70, 80, 90, 100]
    resData["availablecoin"] = common.getTuteeTotalAvailable(user_id)
    resData["userInfo"] = model_to_dict(Tutee.objects.get(user=user_id),
                                                         exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
    resData["data"] = Community.get_language_question_item(resData["id"], user_id)
    jsonData = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("community/qna_write.html", {"data": jsonData}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def updateQna(request):
    """
    @summary: language question 작성된 post를 편집 후 저장하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    if "img" in request.FILES.keys():
        imgfile = request.FILES["img"]
    else:
        imgfile = None

    resData["postURL"], id = Community.update_language_question(user_id, request.POST["qid"], request.POST["title"], request.POST["content"]
                                        ,request.POST["written_lang"], request.POST["written_lang_name"], request.POST["question_lang"], request.POST["question_lang_name"],
                                        request.POST["best_answer_credit"], imgfile)

#     if int(request.POST["best_answer_credit"]):
#         common.insertTuteeAccount(user_id, int(request.POST["best_answer_credit"]), 11, 6, id)

#     Community.set_notification_qna_post(user_id, id)

    return JsonResponse(resData)


def upload_image(image, user_email):
    """
    @summary: community에서 첨부된 파일을 파일 서버에 업로드 하는 함수
    @author: sj46
    @param image
    @param user_email : 사용자 이메일 정보로 파일의 이름을 저장함
    @return: image_path : 첨부된 파일 path
    """
    image_name = "%d&&%s" % (int(round(time.time() * 1000)), image.name)
    repo_path, image_path = make_user_directory(user_email)
    full_path = '%s/%s' % (repo_path, image_name)
    image_path = '%s/%s' % (image_path, image_name)

    try:
        with open(full_path, 'wb') as fp:
            for chunk in image.chunks():
                fp.write(chunk)
    except Exception as err:
        print full_path

    return image_path


def make_user_directory(user):
    """
    @summary: community에서 첨부된 파일을 업로드  시 파일의 경로를 설정하는 함수
    @author: sj46
    @param user : 사용자 이메일 정보로 파일의 이름을 저장함
    @return: directory : 파일 업로드 , image_path : 첨부된 파일 path
    """
    directory = '%s%s' % (BASE_DIR, DATA_ROOT)
    directory = '%s/%s' % (directory, user)
    image_path = '%s/%s' % (DATA_ROOT, user)

    if not os.path.exists(directory):
        os.makedirs(directory)
    return directory, image_path


# ajax pin click
@csrf_exempt
@tellpin_login_required
def ajax_pin(request):
    """
    @summary: language question pin 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    post_dict = json.loads(request.body)
    resData["pin"], resData["pin_count"] = Community.pin_language_question(user_id, post_dict["lq_id"])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def tp_pin(request):
    """
    @summary: trans&proof pin 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)
    resData["pin"], resData["pin_count"] = Community.pin_translation_proofreading(user_id, post_dict["id"])

    return JsonResponse(resData)


# ajax vote click
@csrf_exempt
@tellpin_login_required
def ajax_vote(request):
    """
    @summary: language question 추천 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    resData["like"], resData["like_count"] = Community.vote_language_question(user_id, post_dict["type"], post_dict["lq_id"], post_dict["lq_ref_id"])
    Community.set_notification_qna_vote(user_id, post_dict["type"], post_dict["lq_ref_id"], resData["like"])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def tp_vote(request):
    """
    @summary: trans&proof 추천 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    resData["like"], resData["like_count"] = Community.vote_translation_proofreading(user_id, post_dict["type"], post_dict["ref_id"], post_dict["ref_aid"])
    Community.set_notification_trans_vote(user_id, post_dict["type"], post_dict["ref_id"], resData["like"], post_dict["ref_aid"])

    return JsonResponse(resData)


# ajax add Comment
@csrf_exempt
@tellpin_login_required
def ajax_addComment(request):
    """
    @summary: language question 의  코멘트를 저장하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    cid = Community.add_language_question_comment(user_id, post_dict["type"], post_dict["lq_id"], post_dict["lq_ref_id"],  post_dict["content"])
    resData["comments"] = Community.get_language_question_comments(post_dict["type"], post_dict["lq_ref_id"])
    Community.set_notification_qna_comment(user_id, post_dict["type"], post_dict["lq_ref_id"], cid)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def tp_addComment(request):
    """
    @summary: tran&proof 의  코멘트를 저장하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    cid = Community.add_translation_proofreading_comment(user_id, post_dict["type"], post_dict["tp_id"], post_dict["tp_ref_id"], post_dict["content"])
    resData["comments"] = Community.get_translation_proofreading_comments(post_dict["type"], post_dict["tp_ref_id"])
    print 'tp_addComment'
    try:
        Community.set_notification_trans_comment(user_id, post_dict["type"], post_dict["tp_ref_id"], cid)
    except Exception as e:
        print e
    
    print 'tp_addComment'
    print resData["comments"]

    return JsonResponse(resData)


# ajax remove Comment
@csrf_exempt
@tellpin_login_required
def removeAnswer(request):
    """
    @summary: language question 의 답변을 삭제하는  함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)
    Community.delete_language_question_answer(user_id, post_dict["aid"])
    resData["answers"] = Community.get_language_question_answers_list(post_dict["id"], user_id)

    return JsonResponse(resData)

# ajax remove Comment
@csrf_exempt
@tellpin_login_required
def ajax_removeComment(request):
    """
    @summary: language board 의 comment를 삭제하는  함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    comment_type, ref_id = Community.delete_language_question_comment(user_id, post_dict["cid"])
    resData["comments"] = Community.get_language_question_comments(comment_type, ref_id)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def tp_removeComment(request):
    """
    @summary: trans&proof 의 comment를 삭제하는  함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    comment_type, ref_id = Community.delete_translation_proofreading_comment(user_id, post_dict["cid"])
    resData["comments"] = Community.get_translation_proofreading_comments(comment_type, ref_id)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def tp_removeAnswer(request):
    """
    @summary: trans&proof 의 답변을 삭제하는  함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    Community.delete_translation_proofreading_answer(user_id, post_dict["aid"])
    resData["answers"] = Community.get_translation_proofreading_answers(post_dict["id"], user_id)

    return JsonResponse(resData)


@csrf_exempt
def upload_file(request):
    """
    @summary: community에서 첨부된 파일을 파일 서버에 업로드 하는 함수
    @author: sj46
    @return: image_path : 첨부한 파일 경로 
    """
    # session에서 email을 확인 후 user_id 값 얻기
    if request.session.has_key('email'):
        print "user access check"
        user_email = request.session['email']
    else:
        err_msg = "no session 'email'"
        return render_to_response("community/qna_write.html", {"err_msg": err_msg}, RequestContext(request))

    # image 파일 데이터 받기
    if 'attach' in request.FILES:
        try:
            image = request.FILES[request.FILES.keys()[0]]
            image_path = upload_image(image, user_email)
            print image_path
        except Exception as err:
            print("upload Exception : " + str(err))
            err_msg = "file upload failed. " + str(err)
            return HttpResponse(err_msg, RequestContext(request))
    else:
        err_msg = "file upload failed. "
        return HttpResponse(err_msg, RequestContext(request))

    return HttpResponse(image_path, RequestContext(request))


# add report
@csrf_exempt
@tellpin_login_required
def ajax_addReport(request):
    """
    @summary: language question 포스트 신고 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    Community.violate_language_question(user_id, post_dict["type"], post_dict["qna_id"], post_dict["ref_id"], post_dict["content"])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def ajax_addanswerReport(request):
    """
    @summary: language question 답변을 신고 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    Community.violate_language_question(user_id, post_dict["type"], post_dict["qna_id"], post_dict["ref_id"], post_dict["content"])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def tp_addReport(request):
    """
    @summary: trans&proof 포스트 신고 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    Community.violate_translation_proofreading(user_id, post_dict["type"], post_dict["tp_id"], post_dict["ref_id"], post_dict["content"])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def tp_addanswerReport(request):
    """
    @summary: trans&proof 답글을 신고 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    Community.violate_translation_proofreading(user_id, post_dict["type"], post_dict["tp_id"], post_dict["ref_id"], post_dict["content"])

    return JsonResponse(resData)


# add Answer
@csrf_exempt
@tellpin_login_required
def ajax_addAnswer(request):
    """
    @summary: language question 의  답변을 저장하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    aid = Community.add_language_question_answer(user_id, post_dict["lq_id"], post_dict["content"])
    resData["answers"] = Community.get_language_question_answers_list(post_dict["lq_id"], user_id)
    Community.set_notification_qna_answer(user_id, aid)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def tp_addAnswer(request):
    """
    @summary: tran&proof 의  답변을 저장하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    try:
        aid = Community.add_translation_proofreading_answer(user_id, post_dict["id"], post_dict["content"])
        resData["answers"] = Community.get_translation_proofreading_answers(post_dict["id"], user_id)
        Community.set_notification_trans_answer(user_id, post_dict["id"], aid)
    except Exception as e:
        print e

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def tp_editAnswer(request):
    """
    @summary: tran&proof 의  작성된 답변을 업데이트 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    try:
        aid = Community.modify_translation_proofreading_answer(user_id, post_dict["id"], post_dict["aid"], post_dict["content"])
        resData["answers"] = Community.get_translation_proofreading_answers(post_dict["id"], user_id)
#         Community.set_notification_trans_answer(user_id, post_dict["id"], aid)
    except Exception as e:
        print e

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def qna_editAnswer(request):
    """
    @summary: language question 의  작성된 답변을 편집 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    try:
        aid = Community.modify_language_question_answer(user_id, post_dict["id"], post_dict["aid"], post_dict["content"])
        resData["answers"] = Community.get_language_question_answers_list(post_dict["id"], user_id)
#         Community.set_notification_qna_answer(user_id, post_dict["id"], aid)
    except Exception as e:
        print e

    return JsonResponse(resData)


# select best answer
@csrf_exempt
@tellpin_login_required
def ajax_selectBestAnswer(request):
    """
    @summary: language question 베스트 답변 채택하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    Community.set_language_question_best_answer(user_id, post_dict["lq_id"], post_dict["aid"])
    answer_user_id = Community.get_language_question_answer_user_id(post_dict["aid"])
#     common.insertTuteeAccount(user_id, post_dict["best_answer_credit"], 12, 6, post_dict["id"])
#     common.insertTuteeAccount(answer_user_id, post_dict["best_answer_credit"], 13, 6, post_dict["id"])
    resData["answers"] = Community.get_language_question_answers_list(post_dict["lq_id"], user_id)
    Community.set_notification_qna_best(user_id, post_dict["lq_id"], post_dict["aid"])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def tp_selectBestAnswer(request):
    """
    @summary: trans&proof 베스트 답변 채택하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    Community.set_translation_proofreading_best_answer(user_id, post_dict["id"], post_dict["aid"])
    answer_user_id = Community.get_translation_proofreading_answer_user_id(post_dict["aid"])
#     common.insertTuteeAccount(user_id, post_dict["best_answer_credit"], 12, 7, post_dict["id"])
#     common.insertTuteeAccount(answer_user_id, post_dict["best_answer_credit"], 13, 7, post_dict["id"])
    resData["answers"] = Community.get_translation_proofreading_answers(post_dict["id"], user_id)
    Community.set_notification_trans_best(user_id, post_dict["id"], post_dict["aid"])

    return JsonResponse(resData)


# Language Board
def initLb(request):
    """
    @summary: language board 리스트 화면 진입 url
    @author: sj46
    @param request
    @return: url : community/languageboard_list.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    resData["userInfo"] = model_to_dict(Tutee.objects.get(user=user_id),
                                                     exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])

    resData["languageInfo"] = CommonService.language_list()
    #     resData["nationsInfo"] = CommonService.country_list()
    #     resData["cards"] = Community.get_all_cards()
    jsonData = json.dumps(resData)
    return render_to_response("community/languageboard_list.html", {"data": jsonData}, RequestContext(request))


@csrf_exempt
def ajaxGetLbList(request):
    """
    @summary: language board 리스트 화면 진입 url
    @author: sj46
    @param request
    @return: url : community/languageboard_list.html
    """
    resData = {}
    resData["isSuccess"] = 1

#     resData["nationsInfo"] = CommonService.country_list()
#     resData["boards"] = Community.get_language_board_all_list()
#     return JsonResponse(resData)
    try:
        resData["nationsInfo"] = CommonService.country_list()
        resData["boards"] = Community.get_language_board_all_list()
        return JsonResponse(resData)
    except Exception as e:
        print 'ajaxGetLbList error'
        print e
        return JsonResponse(e)

@csrf_exempt
def change_lb(request):
    """
    @summary: language board 리스트를 구성한 후 편집하기 위한 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)

    resData["cards"] = Community.change_language_board_list(post_dict["learning"], post_dict["explained"], post_dict["start"])

    return JsonResponse(resData)

@csrf_exempt
def search_lb(request):
    """
    @summary: language board 리스트에서 특정 문구가 포함된 리스트 구성 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)

    resData["cards"] = Community.search_language_board_list(post_dict["learning_lang"], post_dict["explained_lang"],
                                       post_dict["start"], post_dict["search"])

    return JsonResponse(resData)


def deatail_lb(request, pid):
    """
    @summary: language board 특정 포스트의 상세화면 진입 url
    @author: sj46
    @param request
    @param pid
    @return: url : community/languageboard_detail.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    Community.add_language_board_views(pid)
    resData["languageInfo"] = CommonService.language_list()
    resData["nationsInfo"] = CommonService.country_list()
    resData["post"] = Community.get_language_board_item(user_id, pid)
    resData["userInfo"] = model_to_dict(Tutee.objects.get(user=user_id),
                                                     exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
    jsonData = json.dumps(resData)

    return render_to_response("community/languageboard_detail.html",
                              {"data": jsonData, 'title': resData['post']['title']},
                              RequestContext(request))


@csrf_exempt
@tellpin_login_required
def post_lb_comment(request):
    """
    @summary: language board 댓글 작성하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    resData["comments"], comment_id = Community.add_language_board_comment(user_id, post_dict["type"], post_dict["ref_id"], post_dict["content"])
    try:
        Community.set_notification_club_comment(user_id, post_dict["type"], post_dict["ref_id"], comment_id)
    except Exception as e:
        print e

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def delete_lb_comment(request):
    """
    @summary: language board 댓글 삭제하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    resData["comments"] = Community.delete_language_board_comment(user_id, post_dict["cid"])

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def pin_lb(request):
    """
    @summary: language board pin 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    resData["pin"], resData["pin_count"] = Community.pin_language_board(user_id, post_dict["pid"])

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def vote_lb(request):
    """
    @summary: language board 투표 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    print 'vote_lb()'
    try:
        resData = {}
        resData["isSuccess"] = 1
        user_id = request.user.id
        post_dict = json.loads(request.body)
    
        resData["like"], resData["like_count"] = Community.vote_language_board(user_id, post_dict["ref_id"])
        Community.set_notification_club_vote(user_id, post_dict["ref_id"], resData["like"])
    except Exception as e:
        print 'vote_lb() Error'
        print e
    return JsonResponse(resData)


@tellpin_login_required
def write_lb(request):
    """
    @summary: language board post를 작성하기위한 write 화면 진입 url
    @author: sj46
    @param request
    @return: url : community/languageboard_write.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    resData["userInfo"] = model_to_dict(Tutee.objects.get(user=user_id),
                                                         exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
    resData["languageInfo"] = CommonService.language_list()
    jsonData = json.dumps(resData)

    return render_to_response("community/languageboard_write.html", {"data": jsonData}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def upload_media(request):
    """
    @summary: language board 이미지 파일 업로드 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    media = request.FILES["media"]
    resData["fid"], resData["path"] = Community.upload_media(media)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def upload_video_preview(request):
    """
    @summary: language board 동영상 파일 업로드 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)
    resData["fid"] = Community.upload_video_preview(post_dict["url"])

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def post_lb(request):
    """
    @summary: language board post 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)
    print "post_club"
    resData["pid"] = Community.create_language_board(user_id, post_dict["learning_lang_id"], post_dict["learning_lang_name"], post_dict["explained_lang_id"], post_dict["explained_lang_name"],
                                                     post_dict["title"], post_dict["content"], post_dict["img_fid"], post_dict["fid"])
    Community.set_notification_club_post(user_id, resData["pid"])

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def update_lb(request):
    """
    @summary: language board 작성된 post를 편집하는  함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)
    print "update_club"
    resData["pid"] = Community.update_language_board(user_id, post_dict["id"], post_dict["learning_lang_id"], post_dict["learning_lang_name"], post_dict["explained_lang_id"], post_dict["explained_lang_name"],
                                    post_dict["title"], post_dict["content"], post_dict["img_fid"], post_dict["fid"])
    Community.set_notification_club_post(user_id, resData["pid"])

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def report_lb(request):
    """
    @summary: language board 포스트 신고 하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    Community.violate_language_board(user_id, post_dict["type"], post_dict["ref_id"], post_dict["content"])

    return JsonResponse(resData)


@csrf_exempt
def get_more_lb(request):
    """
    @summary: language board 리스트 구성 후에 스크롤을 이용해서 추가적으로 리스트 구성시 호출되는 함수
    @author: sj46
    @return: JsonResponse(resData)
    """

    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)

    if "search" in post_dict.keys():
        resData["cards"] = Community.get_more_language_board_for_search(post_dict["learning"], post_dict["explained"],
                                                       post_dict["start"], post_dict["search"])
    else:
        resData["cards"] = Community.get_more_language_board(post_dict["learning"], post_dict["explained"], post_dict["start"])

    return JsonResponse(resData)
