# -*- coding: utf-8 -*-

###############################################################################
# filename    : community > service.py
# description : community 화면을 구성하는데 필요한 정보를 얻기 위한 service file
# author      : sj746@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 sj746 최초 작성
# 20160905 msjang Community 클래스 추가
#
###############################################################################


from datetime import datetime
from operator import itemgetter
import sys
import time

from _mysql import NULL
from django.contrib.sessions.models import Session
from django.db import connection
from django.db.models.aggregates import Count
from django.db.models.query_utils import Q
from django.forms.models import model_to_dict
from django.utils import timezone

from common import common
from common.common import send_noti_email
from community.models import LB, LBComment, LBPin, LBVote, LBViolation, \
                            TP, TPAnswer, TPComment, TPPin, TPVote, TPViolation, \
                            LQ, LQAnswer, LQComment, LQPin, LQVote, LQViolation 
from dashboard.models import Notification, Message
from settings.models import SettingsNotification
from tellpin.settings import BASE_DIR, TELLPIN_HOME
from tutor.models import Friend
from tutor.models import Tutee, Tutor
from tutor.service import TutorService
from tutoring.models import LessonReservation as Reservation
from user_auth.models import File


# from common.models import Ratings, Lesson, Reservation, Follow, Message, Friend, Notification
# from common.models import Language, TellpinUser, DjangoSession
# from user_auth.models import TellpinAuthUser, TellpinUser
# from common.models import SettingsNotification
reload(sys)
sys.setdefaultencoding('utf-8')



class Community(object):

    def __init__(self):
        pass

    @staticmethod
    def create_language_board(_user_id, _learning_id, _learning_name, _explained_id, _explained_name, _title, _content, _img_fid, _fid):
        """
        @summary: language learning club post 하는 함수
        @author: sj46
        @param param1: _user_id : 작성자 id
        @param param2: _learning : 작성된 언어
        @param param3: _explained : 설명될 언어
        @param param4: _title : 제목
        @param param5: _content : 내용
        @param param6: _img_fid : 첨부된 파일 정보
        @param param7: _fid : 첨부할 파일 id
        @return: post id
        """

        print _user_id, _learning_id, _learning_name, _explained_id, _explained_name, _title, _content, _img_fid, _fid
        print _img_fid
        print _fid
        try:
            lb = LB()
            lb.user_id = _user_id
            lb.learning_lang_id = _learning_id
            lb.learning_lang_name = _learning_name
            lb.explained_lang_id = _explained_id
            lb.explained_lang_name = _explained_name
            lb.title = _title
            lb.contents = _content
            if _img_fid != 0:
                lb.main_img_fid_id = _img_fid
            lb.fid = _fid
            lb.created_time = timezone.now()
            lb.updated_time = timezone.now()
            lb.view_count = 0
            lb.save()
            return lb.id
        except Exception as err:
            print err

    @staticmethod
    def update_language_board(_user_id, _id, _learning_id, _learning_name,  _explained_id, _explained_name, _title, _content, _img_fid, _fid):
        """
        @summary: language learning club 편집 함수
        @author: sj46
        @param param1: _user_id : 작성자 id
        @param param2: _id : 편집할 post id
        @param param3: _learning : 작성된 언어
        @param param4: _explained : 설명될 언어
        @param param5: _title : 제목
        @param param6: _content : 내용
        @param param7: _img_fid : 첨부된 파일 정보
        @param param8: _fid : 첨부할 파일 id
        @return: 편집된 post id
        """

        try:
            modifybl = LB.objects.get(id=_id)
            modifybl.user_id = _user_id
            modifybl.learning_lang_id = _learning_id
            modifybl.learning_lang_name = _learning_name
            modifybl.explained_lang_id = _explained_id
            modifybl.explained_lang_name = _explained_name
            modifybl.title = _title
            modifybl.contents = _content
            if _img_fid != 0:
                modifybl.main_img_fid_id = _img_fid
            modifybl.fid = _fid
            modifybl.updated_time = timezone.now()
            modifybl.view_count += 1
            modifybl.save()
            return modifybl.id
        except Exception as err:
            print err

    @staticmethod
    def get_language_board_all_list():
        """
        @summary: language_board 리스트 구성 함수
        @author: sj46
        @param none
        @return: language_board list
        """

        boards = []
        posts = LB.objects.all().order_by("-created_time")[0:18]
        for post in posts:
            post_dict = model_to_dict(post, exclude=["updated_time", "created_time", 'del_by_admin', 'del_by_user'])
            post_dict["created_time"] = str(post.created_time)
            post_dict["updated_time"] = str(post.updated_time)
            if post_dict["main_img_fid"] is not None:
                post_dict["main_img_fid"] = model_to_dict(File.objects.get(type='LC', fid=post_dict["main_img_fid"]))
            post_dict["comment_count"] = LBComment.objects.filter(type="P", lb_id=post.id).count()
            post_dict["vote_count"] = LBVote.objects.filter(lb_id=post.id).count()
            post_dict["pin_count"] = LBPin.objects.filter(lb_id=post.id).count()
            if TutorService.is_tutor(post.user_id):
                if Tutor.objects.filter(user=post.user_id):
                    post_dict["writer_info"] = model_to_dict(Tutor.objects.get(user=post.user_id),
                        exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
            else:
                if Tutee.objects.filter(user=post.user_id):
                    post_dict["writer_info"] = model_to_dict(Tutee.objects.get(user=post.user_id),
                        exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])

            boards.append(post_dict)

        return boards

    @staticmethod
    def get_more_language_board_for_search(_learning, _explained, _start, _search):
        """
        @summary: language learning club 리스트에서 특정 문구가 포함된 리스트 구성 후 스크롤을 동작에 따른 다음 리스트를 가져오는 함수
        @author: sj46
        @param param1: _learning : 배우는 언어
        @param param2: _explained : 설명되어질 언어
        @param param3: _start : 가져올 리스트의 시작 인덱스
        @param param4: _search : 찾으려는 문구
        @return: club list
        """

        boards = []
        try:
            cursor = connection.cursor()
            #             필터와 상관없이 모두 검색되도록 수정
            #             if _learning != 0 and _explained != 0 :
            #                 sql = "SELECT a.*, b.name FROM lb a INNER JOIN user_auth_tellpinuser b ON a.user_id = b.id WHERE a.explained_lang_id = %s AND a.learning_lang_id = %s AND (a.title LIKE %s OR a.content like %s OR b.name LIKE %s) limit %s,12;"
            #                 cursor.execute( sql, [_explained, _learning,"%"+_search+"%", "%"+_search+"%","%"+_search+"%",  _start] )
            #             elif _learning == 0 and _explained != 0 :
            #                 sql = "SELECT a.*, b.name FROM lb a INNER JOIN user_auth_tellpinuser b ON a.user_id = b.id WHERE a.explained_lang_id = %s AND (a.title LIKE %s OR a.content like %s OR b.name LIKE %s) limit %s,12;"
            #                 cursor.execute( sql, [_explained,"%"+_search+"%", "%"+_search+"%", "%"+_search+"%", _start] )
            #             elif _learning != 0 and _explained == 0 :
            #                 sql = "SELECT a.*, b.name FROM lb a INNER JOIN user_auth_tellpinuser b ON a.user_id = b.id WHERE a.learning_lang_id = %s AND (a.title LIKE %s OR a.content like %s OR b.name LIKE %s) limit %s,12;"
            #                 cursor.execute( sql, [_learning, "%"+_search+"%", "%"+_search+"%", "%"+_search+"%", _start] )
            #             else :
            sql = "SELECT a.*, b.name FROM lb a INNER JOIN user_auth_tellpinuser b ON a.user_id = b.id WHERE a.title like %s OR a.contents like %s or b.name like %s limit %s,12;"
            cursor.execute(sql, ["%" + _search + "%", "%" + _search + "%", "%" + _search + "%", _start])

            for card in common.dictfetchall(cursor):
                card["created_time"] = str(card["created_time"])
                del card["updated_time"]
                del card["name"]
                if card["main_img_fid"] is not None:
                    card["main_img_fid"] = model_to_dict(File.objects.get(type='LC', fid=card["main_img_fid"]))
                card["comment_count"] = LBComment.objects.filter(type="P", lq_ref_id=card["id"]).count()
                card["vote_count"] = LBVote.objects.filter(type="P", lq_ref_id=card["id"]).count()
                card["pin_count"] = LBPin.objects.filter(lq_ref_id=card["id"]).count()
                if TutorService.is_tutor(card.user_id):
                    if Tutor.objects.filter(user=card.user_id):
                        card["writer_info"] = model_to_dict(Tutor.objects.get(user=card.user_id),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                else:
                    if Tutee.objects.filter(user=card.user_id):
                        card["writer_info"] = model_to_dict(Tutee.objects.get(user=card.user_id),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])

                boards.append(card)

        except Exception as err:
            print err

        return boards

    @staticmethod
    def get_more_language_board(_learning, _explained, _start):
        """
        @summary: language learning club 리스트 구성 후에 스크롤을 이용해서 추가적으로 리스트 구성시 호출되는 함수
        @author: sj46
        @param param1: _learning : 배우는 언어
        @param param2: _explained : 설명되어질 언어
        @param param3: _start : 가져올 리스트의 시작 인덱스
        @return: club list
        """

        try:
            print _learning, _explained, _start
            boards = []
            if _learning != 0 and _explained != 0:
                posts = LB.objects.filter(learning_lang_id=_learning, explained_lang_id=_explained).order_by(
                    "-created_time")[_start:_start + 12]
            elif _learning == 0 and _explained != 0:
                posts = LB.objects.filter(explained_lang_id=_explained).order_by("-created_time")[
                        _start:_start + 12]
            elif _learning != 0 and _explained == 0:
                posts = LB.objects.filter(learning_lang_id=_learning).order_by("-created_time")[_start:_start + 12]
            else:
                posts = LB.objects.all().order_by("-created_time")[_start:_start + 12]

            for post in posts:
                post_dict = model_to_dict(post, exclude=["updated_time", "created_time", 'del_by_admin', 'del_by_user'])
                post_dict["updated_time"] = str(post.updated_time)
                if post_dict["main_img_fid"] is not None:
                    post_dict["main_img_fid"] = model_to_dict(File.objects.get(type='LC', fid=post_dict["main_img_fid"]))
                post_dict["comment_count"] = LBComment.objects.filter(type="P", lb_id=post_dict["id"]).count()
                post_dict["vote_count"] = LBVote.objects.filter(lb_id=post_dict["id"]).count()
                post_dict["pin_count"] = LBPin.objects.filter(lb_id=post_dict["id"]).count()
                if TutorService.is_tutor(post.user_id):
                    if Tutor.objects.filter(user=post.user_id):
                        post_dict["writer_info"] = model_to_dict(Tutor.objects.get(user=post.user_id),
                                                         exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                else:
                    if Tutee.objects.filter(user=post.user_id):
                        post_dict["writer_info"] = model_to_dict(Tutee.objects.get(user=post.user_id),
                                                         exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])

                boards.append(post_dict)

            return boards
        except Exception as err:
            print err

    @staticmethod
    def change_language_board_list(_learning, _explained, _start):
        """
        @summary: language board 선택된 언어로  리스트 구성
        @author: sj46
        @param param1: _learning : 배우는 언어
        @param param2: _explained : 설명되어질 언어
        @param param3: _start : 가져올 리스트의 시작 인덱스
        @return: board list
        """

        cards = []
        if _learning != 0 and _explained != 0:
            posts = LB.objects.filter(learning_lang_id=_learning, explained_lang_id=_explained).order_by(
                "-created_time")[_start:_start + 18]
        elif _learning == 0 and _explained != 0:
            posts = LB.objects.filter(explained_lang_id=_explained).order_by("-created_time")[_start:_start + 18]
        elif _learning != 0 and _explained == 0:
            posts = LB.objects.filter(learning_lang_id=_learning).order_by("-created_time")[_start:_start + 18]
        else:
            posts = LB.objects.all().order_by("-created_time")[_start:_start + 18]

        for post in posts:
            post_dict = model_to_dict(post, exclude=['created_time', 'updated_time'])
            post_dict["created_time"] = str(post.created_time)
            post_dict["updated_time"] = str(post.updated_time)
            if 0 != post_dict["main_img_fid"] and post_dict["main_img_fid"] is not None:
                post_dict["main_img_fid"] = model_to_dict(File.objects.get(type='LC', fid=post_dict["main_img_fid"]))
            post_dict["comment_count"] = LBComment.objects.filter(type="P", lb_id=post_dict["id"]).count()
            post_dict["vote_count"] = LBVote.objects.filter(lb_id=post_dict["id"]).count()
            post_dict["pin_count"] = LBPin.objects.filter(lb_id=post_dict["id"]).count()
            if TutorService.is_tutor(post.user_id):
                if Tutor.objects.filter(user=post.user_id):
                    post_dict["writer_info"] = model_to_dict(Tutor.objects.get(user_id=post.user_id),
                            exclude=['password', 'reg_date', 'updated_time', 'last_login'])
            else:
                if Tutee.objects.filter(user=post.user_id):
                    post_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=post.user_id),
                            exclude=['password', 'reg_date', 'updated_time', 'last_login'])

            cards.append(post_dict)

        return cards

    @staticmethod
    def search_language_board_list(_learning, _explained, _start, _search):
        """
        @summary: language board 리스트에서 특정 문구가 포함된 리스트 구성 함수
        @author: sj46
        @param param1: _learning : 배우는 언어
        @param param2: _explained : 설명되어질 언어
        @param param3: _start : 가져올 리스트의 시작 인덱스
        @param param4: _search : 찾으려는 문구
        @return: board list
        """

        cards = []
        try:
            cursor = connection.cursor()
            #             필터와 상관없이 모두 검색되도록 수정
            #             if _learning != 0 and _explained != 0 :
            #                 sql = "SELECT a.*, b.name FROM lb a INNER JOIN user_auth_tellpinuser b ON a.user_id = b.id WHERE a.explained_lang_id = %s AND a.learning_lang_id = %s AND (a.title LIKE %s  OR a.content like %s OR b.name LIKE %s) limit %s,18;"
            #                 cursor.execute( sql, [_explained, _learning,"%"+_search+"%", "%"+_search+"%", "%"+_search+"%", _start] )
            #             elif _learning == 0 and _explained != 0 :
            #                 sql = "SELECT a.*, b.name FROM lb a INNER JOIN user_auth_tellpinuser b ON a.user_id = b.id WHERE a.explained_lang_id = %s AND (a.title LIKE %s OR a.content like %s OR b.name LIKE %s) limit %s,18;"
            #                 cursor.execute( sql, [_explained,"%"+_search+"%", "%"+_search+"%", "%"+_search+"%", _start] )
            #             elif _learning != 0 and _explained == 0 :
            #                 sql = "SELECT a.*, b.name FROM lb a INNER JOIN user_auth_tellpinuser b ON a.user_id = b.id WHERE a.learning_lang_id = %s AND (a.title LIKE %s OR a.content like %s OR b.name LIKE %s) limit %s,18;"
            #                 cursor.execute( sql, [_learning, "%"+_search+"%", "%"+_search+"%", "%"+_search+"%", _start] )
            #             else :
            sql = "SELECT a.*, b.name FROM lb a INNER JOIN user_auth_tellpinuser b ON a.user_id = b.id WHERE (a.title LIKE %s OR a.contents like %s or b.name like %s) limit %s,18;"
            cursor.execute(sql, ["%" + _search + "%", "%" + _search + "%", "%" + _search + "%", _start])

            for card in common.dictfetchall(cursor):
                card["created_time"] = str(card["created_time"])
                del card["updated_time"]
                del card["name"]
                if 0 != card["main_img_fid"] and card["main_img_fid"] is not None:
                    card["main_img_fid"] = model_to_dict(File.objects.get(type='LC', fid=card["main_img_fid"]))
                card["comment_count"] = LBComment.objects.filter(type="P", lb_id=card["id"]).count()
                card["vote_count"] = LBVote.objects.filter(lb_id=card["id"]).count()
                card["pin_count"] = LBPin.objects.filter(lb_id=card["id"]).count()
                if TutorService.is_tutor(card["user_id"]):
                    if Tutor.objects.filter(user=card["user_id"]):
                        card["writer_info"] = model_to_dict(Tutor.objects.get(user=card["user_id"]),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                else:
                    if Tutee.objects.filter(user=card["user_id"]):
                        card["writer_info"] = model_to_dict(Tutee.objects.get(user=card["user_id"]),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                cards.append(card)

        except Exception as err:
            print err

        return cards

    @staticmethod
    def get_language_board_item(_user_id, _pid):
        """
        @summary: language learning club 특정 post 를  가져오는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _pid : post id
        @return: club post
        """
        post_model = LB.objects.get(id=_pid)

        post = model_to_dict(post_model, exclude=['created_time', 'updated_time'])
        post["created_time"] = str(post_model.created_time)
        post["updated_time"] = str(post_model.updated_time)
        if TutorService.is_tutor(post["user"]):
            if Tutor.objects.filter(user=post["user"]):
                post["writer_info"] = model_to_dict(Tutor.objects.get(user=post["user"]),
                                 exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
        else:
            if Tutee.objects.filter(user=post["user"]):
                post["writer_info"] = model_to_dict(Tutee.objects.get(user=post["user"]),
                                 exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
        post["comments"] = Community.get_language_board_comments('P', post_model.id)
        if _user_id is not None:
            post["like"] = LBVote.objects.filter(user_id=_user_id, lb_id=post_model.id).count()
        else:
            post["like"] = 0
        post["vote_count"] = LBVote.objects.filter(lb_id=post_model.id).count()
        if _user_id is not None:
            post["pin"] = LBPin.objects.filter(user_id=_user_id, lb_id=post_model.id).count()
        else:
            post["pin"] = 0
        post["pin_count"] = LBPin.objects.filter(lb_id=post_model.id).count()

        return post

    @staticmethod
    def language_board_list(user_id=None, page=1):
        """
            @summary: Language Board 리스트
            @author: msjang
            @param user_id: 로그인 한 사용자
            @param page: 가져올 항목의 처음 시작 인덱스
        """
        pass

    @staticmethod
    def language_board_detail(lb_id):
        """
            @summary: Language Board 게시물 디테일뷰
            @author: msjang
            @param lb_id: 해당 게시물 id
        """
        pass

    @staticmethod
    def language_board_update(lb_id, user_id, learning_lang, explained_lang,
                              title, contents, main_img_fid, fid):
        """
            @summary: Language Board 업데이트
            @author: msjang
            @param lb_id: language_board id
            @param user_id: 로그인 한 사용자
            @param learning_lang: 배우려는 언어
            @param explained_lang: 설명할 언어
            @param title: 제목
            @param contents: 내용
            @param main_img_fid: 메인 이미지 파일 id
            @param fid: 첨부 파일 id
        """
        pass

    @staticmethod
    def delete_language_board(_p_id):
        """
        @summary: language board 의 board를 삭제하는  함수
        @author: sj46
        @param _p_id : post id
        @return: language board 삭제된 post 정보
        """
        board = LB.objects.get(id=_p_id)
        board.delete()

        return board

    @staticmethod
    def language_board_comment_delete(lb_id, user_id):
        """
            @summary: Language Board 게시물 댓글 삭제
            @author: msjang
            @param lb_id: language_board id
            @param user_id: 로그인 한 사용자
        """
        pass

    @staticmethod
    def add_language_board_views( _pid):
        """
        @summary: language learning club 조회수 추가 함수
        @author: sj46
        @param param1: _pid : post id
        @return: none
        """

        post = LB.objects.get(id=_pid)
        post.view_count += 1
        post.save()

    @staticmethod
    def pin_language_board(_user_id, _pid):
        """
        @summary: language learning club pin 하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param1: _pid : 게시글 id
        @return: pin 상태, pin count
        """

        try:
            user_pin = LBPin.objects.filter(lb=_pid, user_id=_user_id).count()
            if user_pin:
                LBPin.objects.get(lb=_pid, user_id=_user_id).delete()
                pin_state = 0
            else:
                pin = LBPin()
                pin.user_id = _user_id
                pin.lb_id = _pid
                pin.save()
                pin_state = 1
            pin_count = LBPin.objects.filter(lb=_pid).count()

            return pin_state, pin_count
        except Exception as err:
            print err

    @staticmethod
    def language_board_vote(lb_id, user_id, like, dislike):
        """
            @summary: Language Board 게시물 투표
            @author: msjang
            @param lb_id: language_board id
            @param user_id: 로그인 한 사용자
            @param like: 게시물 좋아요
            @param dislike: 게시물 싫어요
        """
        pass

    @staticmethod
    def add_language_board_comment(_user_id, _type, _ref_id, _content):
        """
        @summary: language learning club 댓글 작성하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _type : 게시글 타입 P: Post C: Comment
        @param param3: _ref_id : 게시글 ID
        @param param4: _content : 댓글 내용
        @return: none
        """

        try:
            comment = LBComment()
            comment.user_id = _user_id
            comment.type = _type
            comment.lb_id = _ref_id
            comment.lb_ref_id = _ref_id
            comment.contents = _content
            comment.save()
            comment_list = Community.get_language_board_comments(_type, _ref_id)
            return comment_list, comment.id

        except Exception as err:
            print err

    @staticmethod
    def get_language_board_comments(_type, _ref_id):
        """
        @summary: language learning club 댓글 리스트 구성 함수
        @author: sj46
        @param param1: _type : 게시글 타입 P: Post C: Comment
        @param param2: _ref_id : 게시글 ID
        @return: comment_list
        """

        comment_list = []
        comments = LBComment.objects.filter(type=_type, lb_id=_ref_id).order_by('created_time')
        for comment in comments:
            comment_dict = model_to_dict(comment, exclude=["created_time"])
            comment_dict["created_time"] = str(comment.created_time)
            if TutorService.is_tutor(comment.user_id):
                if Tutor.objects.filter(user=comment.user_id):
                    comment_dict["writer_info"] = model_to_dict(Tutor.objects.get(user=comment.user_id),
                        exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
            else:
                if Tutee.objects.filter(user=comment.user_id):
                    comment_dict["writer_info"] = model_to_dict(Tutee.objects.get(user=comment.user_id),
                        exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
# sub comment delete
#             sub_comment_list = []
#             sub_comments = LBComment.objects.filter(type='C', lb_id=comment.lb_id).order_by('created_time')
#             for sub_comment in sub_comments:
#                 sub_comment_dict = model_to_dict(sub_comment, exclude=["created_time"])
#                 sub_comment_dict["created_time"] = str(sub_comment.created_time)
#                 sub_comment_dict["writer_info"] = model_to_dict(Tutee.objects.get(user=sub_comment.user_id),
#                                  exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
#                 sub_comment_list.append(sub_comment_dict)
#             comment_dict["sub_comments"] = sub_comment_list
            comment_list.append(comment_dict)
        print "AAAAAAAAAAAA"
        print comment_list
        return comment_list

    @staticmethod
    def delete_language_board_comment(_user_id, _cid):
        """
        @summary: language learning club 댓글 삭제하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param1: _cid : 댓글 id
        @return: 댓글 리스트
        """

        comment = LBComment.objects.get(id=_cid)
        if comment.user_id == _user_id:
            sub_comments = LBComment.objects.filter(type='C', id=_cid)
            if sub_comments:
                comment.contents = '!delete!'
                comment.save()
            else:
                comment.delete()
        comment_list = Community.get_language_board_comments(comment.type, comment.lb_id)
        return comment_list

    @staticmethod
    def vote_language_board(_user_id, _ref_id):
        """
        @summary: language board 투표 하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param1: _type : 참조할 테이블 유형 P: Post C: Comment
        @param param1: _ref_id : 참조 ID
        @return: like_state, like_count
        """

        try:
            user_like = LBVote.objects.filter(user_id=_user_id, lb_id =_ref_id)
            if user_like:
                LBVote.objects.get(user_id=_user_id, lb_id =_ref_id).delete()
                like_state = 0
            else:
                like = LBVote()
                like.user_id = _user_id
                like.lb_id = _ref_id
                like.like = 1
                like.dislike = 0
                like.save()
                like_state = 1
            like_count = LBVote.objects.filter(lb_id =_ref_id).count()
            return like_state, like_count
        except Exception as err:
            print err

    @staticmethod
    def violate_language_board(_user_id, _type, _ref_id, _content):
        """
        @summary: language learning club 포스트 신고 하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _type : 게시글 타입 (P:club post)
        @param param3: _ref_id : 게시글 ID
        @param param4: _content : 신고 내용
        @return: none
        """

        try:
            report = LBViolation()
            report.user_id = _user_id
#             report.type = _type
            report.lb_id = _ref_id
            report.contents = _content
            report.save()
        except Exception as err:
            print "@@@violate_language_board error : " + err



    def get_myCard(self, _user_id, _learning, _explained, _start):
        """
        @summary: user 가 작성한 클럽 리스트 가져오기
        @author: sj46
        @param param1: _user_id : 로그인한 유저
        @param param2: _learning : learning lang
        @param param3: _explained : explain lang
        @param param4: _start : 가져올 항목 개수
        @return: club list
        """

        cards = []
        posts = LB.objects.filter(user_id=_user_id).order_by("-created_time")[_start: _start + 15]
        print posts
        for post in posts:
            post_dict = model_to_dict(post, exclude=["updated_time", "created_time"])
            post_dict["created_time"] = str(post.created_time)
            post_dict["updated_time"] = str(post.updated_time)
            if 0 != post_dict["main_img_fid"]:
                post_dict["main_img_fid"] = model_to_dict(File.objects.get(type='LC', fid=post_dict["main_img_fid"]))
            post_dict["comment_count"] = LBComment.objects.filter(type="P", lb_id=post_dict["id"]).count()
            post_dict["vote_count"] = LBVote.objects.filter(lb_id=post_dict["id"]).count()
            post_dict["pin_count"] = LBPin.objects.filter(lb_id=post_dict["id"]).count()
            if TutorService.is_tutor(post.user_id):
                if Tutor.objects.filter(user=post.user_id):
                    post_dict["writer_info"] = model_to_dict(Tutor.objects.get(user=post.user_id),
                                     exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
            else:
                if Tutee.objects.filter(user=post.user_id).exists():
                    post_dict["writer_info"] = model_to_dict(Tutee.objects.get(user=post.user_id),
                                     exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])


                cards.append(post_dict)

        return cards

    def get_userLangCards(self, _user_id):
        """
        @summary: language learning club 선택된 언어로 클럽 리스트 구성
        @author: sj46
        @param param1: _lang_list : user의 native, learning, other 언어 리스트
        @param param2: _start : 가져올 리스트의 시작 인덱스
        @return: club list
        """

        cards = []
        posts = LB.objects.all().order_by("-created_time")[0:30]

        speak_list = Community.get_speaks_list(_user_id)
        #         posts = LB.objects.filter(Q(learning_lang_id = speak_list[0]) | Q(learning_lang_id = ))
        print "speaks_listspeaks_listspeaks_listspeaks_list"
        print speak_list

        size = len(speak_list)
        print size
        for post in posts:
            print post
            index = 0
            while (index < len(speak_list)):
                print index
                if post.learning_lang_id == speak_list[index]:
                    print index
                    post_dict = model_to_dict(post, exclude=["updated_time", "created_time"])
                    post_dict["created_time"] = str(post.created_time)
                    post_dict["updated_time"] = str(post.updated_time)
                    if 0 != post_dict["main_img_fid"]:
                        post_dict["main_img_fid"] = model_to_dict(File.objects.get(type='LC', fid=post_dict["main_img_fid"]))
                    post_dict["comment_count"] = LBComment.objects.filter(type="P", lb_id=post_dict["id"]).count()
                    post_dict["vote_count"] = LBVote.objects.filter(type="P", lb_id=post_dict["id"]).count()
                    post_dict["pin_count"] = LBPin.objects.filter(lb_id=post_dict["id"]).count()
                    if TutorService.is_tutor(post.user_id):
                        if Tutor.objects.filter(user=post.user_id):
                            post_dict["writer_info"] = model_to_dict(Tutor.objects.get(user=post.user_id),
                                exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                    else:
                        if Tutee.objects.filter(user=post.user_id):
                            post_dict["writer_info"] = model_to_dict(Tutee.objects.get(user=post.user_id),
                                exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])

                    cards.append(post_dict)
                index += 1

        print len(cards)
        return cards

    def get_speaks_list(self, _user_id):
        """
        @summary: user의 native lang, learning lang, other lang list 구성
        @author: sj46
        @param param1: _user_id : 배우는 언어
        @return: speaks_list
        """
        if Tutee.objects.filter(user=_user_id).exists():
            user = Tutee.objects.get(user_id=_user_id)
            speaks_list = []
            if user.native1 is not None:  # native
                speaks_list.append(user.native1)
            if user.native2 is not None:
                speaks_list.append(user.native2)
            if user.native3 is not None:
                speaks_list.append(user.native3)
            if user.learning1 is not None:  # learning
                speaks_list.append(user.learning1)
            if user.learning2 is not None:
                speaks_list.append(user.learning2)
            if user.learning3 is not None:
                speaks_list.append(user.learning3)
            if user.other1 is not None:  # other
                speaks_list.append(user.other1)
            if user.other2 is not None:
                speaks_list.append(user.other2)
            if user.other3 is not None:
                speaks_list.append(user.other3)

            return speaks_list


    @staticmethod
    def create_translation_proofreading(_user_id, _title, _content, _written_id, _written_name, _translated_id, _translated_name, _point, _type, _img_file):
        """
        @summary: trans&proof post 하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _title : 게시글 제목
        @param param3: _content : 게시글 내용
        @param param4: _written : 쓰여진 언어
        @param param5: _translated : 번역될 언어
        @param param6: _point : 답변 채택 시 걸 포인트
        @param param7: _type : T:trans, P:proof
        @param param8: _img_file : 첨부할 파일의 이미지 정보
        @return: url : /trans/+ 포스트 id
        """

        print _user_id, _title, _content, _written_id, _written_name, _translated_id, _translated_name, _point, _type, _img_file
        tp = TP()
        tp.user_id = _user_id
        tp.title = _title
        tp.contents = _content
        if _written_id == 0:
            tp.written_lang_id = None
            tp.written_lang_name = ""
        else:
            tp.written_lang_id = _written_id
            tp.written_lang_name = _written_name

        if _translated_id == 0:
            tp.translated_lang_id = None
            tp.translated_lang_name = ""
        else:
            tp.translated_lang_id = _translated_id
            tp.translated_lang_name = _translated_name
        tp.best_answer_credit = _point
        if int(_type) == 0:
            tp.type = 'T'
        else:
            tp.type = 'P'
            tp.translated_lang_id = None
            tp.translated_lang_name = ""

        tp.view_count = 0
#         tp.expired_time = timezone.now()
        tp.created_time = timezone.now()
        tp.updated_time = timezone.now()
        tp.save()

        if _img_file:
            fid = Community.upload_file(_img_file, "TP", tp.id)
            tp.fid = fid
            tp.save()

        return "/trans/" + str(tp.id) + "/", tp.id

    @staticmethod
    def update_translation_proofreading(_user_id, _id, _title, _content, _written_id, _written_name, _translated_id, _translated_name, _point, _type, _img_file):
        """
        @summary: trans&proof 편집 하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _id : 편집 할 post id
        @param param3: _title : 게시글 제목
        @param param4: _content : 게시글 내용
        @param param5: _written : 쓰여진 언어
        @param param6: _translated : 번역될 언어
        @param param7: _point : 답변 채택 시 걸 포인트
        @param param8: _type : T:trans, P:proof
        @param param9: _img_file : 첨부할 파일의 이미지 정보
        @return: url : /trans/+ 편집 된 id
        """

        print _id, _written_id, _written_name, _translated_id, _translated_name
        modifytp = TP.objects.get(id=_id)
        modifytp.user_id = _user_id
        modifytp.title = _title
        modifytp.contents = _content
        if _written_id == 0:
            modifytp.written_lang_id = None
            modifytp.written_lang_name = ""
        else:
            modifytp.written_lang_id = _written_id
            modifytp.written_lang_name = _written_name

        if _translated_id == 0:
            modifytp.translated_lang_id = None
            modifytp.translated_lang_name = ""
        else:
            modifytp.translated_lang_id = _translated_id
            modifytp.translated_lang_name = _translated_name
        modifytp.best_answer_credit = _point
        if int(_type) == 0:
            modifytp.type = 'T'
        else:
            modifytp.type = 'P'
            modifytp.translated_lang_id = None
            modifytp.translated_lang_name = ""

        modifytp.view_count += 1
        modifytp.updated_time = timezone.now()
        modifytp.save()

        if _img_file:
            fid = Community.upload_file(_img_file, "TP", modifytp.id)
            modifytp.fid = fid
            modifytp.save()

        return "/trans/" + str(modifytp.id) + "/", modifytp.id

    @staticmethod
    def delete_translation_proofreading(_q_id):
        """
        @summary: trans&proof 의 post를 삭제하는  함수
        @author: sj46
        @param param1: _p_id : post id
        @return: trans&proof 삭제된 post 정보
        """

        tp = TP.objects.get(id=_q_id)
        tp.delete()

        return tp

    @staticmethod
    def get_translation_proofreading_list(_user_id, _start, _from_lang, _to_lang, _in_lang):
        """
        @summary: tras&proof 의 전체 리스트를  가져오는 함수
        @author: sj46
        @param param1: _user_id - 로그인 한 사용자
        @param param2: _start : 가져올 항목의 처음 시작 인덱스
        @param param3: _from_lang - trans의 변환 할 언어
        @param param4: _to_lang : trans의 변환 하고자 하는 언어
        @param param5: _in_lang : proofreading 작성 언어
        @return: tras&proof 항목
        """

        TP_list = []
        print _from_lang, _to_lang, _in_lang
        if _from_lang == 0 and _to_lang == 0 and _in_lang == 0:
            print "11"
            TPs = TP.objects.all().order_by("-created_time")[_start: _start + 15]
        elif _from_lang != 0 and _to_lang == 0 and _in_lang == 0:
            print "22"
            TPs = TP.objects.filter((Q(type='T', written_lang_id=_from_lang) | Q(type='P'))).order_by(
                "-created_time")[_start: _start + 15]
        elif _from_lang == 0 and _to_lang != 0 and _in_lang == 0:
            print "33"
            TPs = TP.objects.filter((Q(type='T', translated_lang_id=_to_lang) | Q(type='P'))).order_by(
                "-created_time")[_start: _start + 15]
        elif _from_lang == 0 and _to_lang == 0 and _in_lang != 0:
            print "44"
            TPs = TP.objects.filter((Q(type='T') | Q(type='P', written_lang_id=_in_lang))).order_by(
                "-created_time")[_start: _start + 15]
        elif _from_lang != 0 and _to_lang != 0 and _in_lang == 0:
            print "55"
            TPs = TP.objects.filter(
                (Q(type='T', written_lang_id=_from_lang, translated_lang_id=_to_lang) | Q(type='P'))).order_by(
                "-created_time")[_start: _start + 15]
        elif _from_lang == 0 and _to_lang != 0 and _in_lang != 0:
            print "66"
            TPs = TP.objects.filter(
                (Q(type='T', translated_lang_id=_to_lang) | Q(type='P', written_lang_id=_in_lang))).order_by(
                "-created_time")[_start: _start + 15]
        elif _from_lang != 0 and _to_lang == 0 and _in_lang != 0:
            print "77"
            TPs = TP.objects.filter(
                (Q(type='T', written_lang_id=_from_lang) | Q(type='P', written_lang_id=_in_lang))).order_by(
                "-created_time")[_start: _start + 15]
        else:
            print "88"
            TPs = TP.objects.filter((Q(type='T', written_lang_id=_from_lang, translated_lang_id=_to_lang) | Q(
                type='P', written_lang_id=_in_lang))).order_by("-created_time")[_start: _start + 15]

        for tp in TPs:
            if Tutee.objects.filter(user_id=tp.user_id).exists() :
                tp_dict = model_to_dict(tp, exclude=["updated_time", "created_time"])
                tp_dict["created_time"] = str(tp.created_time)
                tp_dict["answer_count"] = TPAnswer.objects.filter(tp_id=tp.id).count()
                if _user_id is not None:
                    tp_dict["pin"] = TPPin.objects.filter(tp_id=tp.id, user_id=_user_id).count()
                else:
                    tp_dict["pin"] = 0
                tp_dict["pin_count"] = TPPin.objects.filter(id=tp.id).count()
                tp_dict["comment_count"] = TPComment.objects.filter(type="Q", tp_id=tp.id).count()
                if _user_id is not None:
                    vote = TPVote.objects.filter(type="Q", tp_ref_id=tp.id, user_id=_user_id)
                else:
                    vote = None
                if vote:
                    vote = TPVote.objects.get(type="Q", tp_ref_id=tp.id, user_id=_user_id)
                    tp_dict["like"] = vote.like
                    tp_dict["dislike"] = vote.dislike
                else:
                    tp_dict["like"] = 0
                    tp_dict["dislike"] = 0
                like_count = TPVote.objects.filter(type="Q", tp_ref_id=tp.id, like=1).count()
                dislike_count = TPVote.objects.filter(type="Q", tp_ref_id=tp.id, dislike=1).count()
                tp_dict["like_count"] = like_count - dislike_count
                if TutorService.is_tutor(tp.user_id):
                    if Tutor.objects.filter(user=tp.user_id):
                        tp_dict["writer_info"] = model_to_dict(Tutor.objects.get(user_id=tp.user_id),
                                         exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                else:
                    if Tutee.objects.filter(user=_user_id):
                        tp_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=tp.user_id),
                                         exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                TP_list.append(tp_dict)

        return TP_list

    @staticmethod
    def get_translation_list(_user_id, _from_lang, _to_lang, _start):
        """
        @summary: tras 의 전체 리스트를  가져오는 함수
        @author: sj46
        @param param1: _user_id - 로그인 한 사용자
        @param param2: _start : 가져올 항목의 처음 시작 인덱스
        @param param3: _from_lang - trans의 변환 할 언어
        @param param4: _to_lang : trans의 변환 하고자 하는 언어
        @return: tras 항목
        """

        TP_list = []
        print _from_lang, _to_lang
        if _from_lang == 0 and _to_lang == 0:
            TPs = TP.objects.filter(type='T').order_by("-created_time")[_start: _start + 15]
        elif _from_lang != 0 and _to_lang == 0:
            TPs = TP.objects.filter(type='T', written_lang_id=_from_lang).order_by("-created_time")[
                  _start: _start + 15]
        elif _from_lang == 0 and _to_lang != 0:
            TPs = TP.objects.filter(type='T', translated_lang_id=_to_lang).order_by("-created_time")[
                  _start: _start + 15]
        else:
            TPs = TP.objects.filter(type='T', written_lang_id=_from_lang, translated_lang_id=_to_lang).order_by(
                "-created_time")[_start: _start + 15]

        for tp in TPs:
            if Tutee.objects.filter(user_id=tp.user_id).exists() :
                tp_dict = model_to_dict(tp, exclude=["updated_time", "created_time"])
                tp_dict["created_time"] = str(tp.created_time)
                if TutorService.is_tutor(tp.user_id):
                    if Tutor.objects.filter(user=tp.user_id):
                        tp_dict["writer_info"] = model_to_dict(Tutor.objects.get(user=tp.user_id),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                else:
                    if Tutee.objects.filter(user=tp.user_id):
                        tp_dict["writer_info"] = model_to_dict(Tutee.objects.get(user=tp.user_id),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                tp_dict["answer_count"] = TPAnswer.objects.filter(id=tp.id).count()
                if _user_id is not None:
                    tp_dict["pin"] = TPPin.objects.filter(id=tp.id, user_id=_user_id).count()
                else:
                    tp_dict["pin"] = 0
                tp_dict["pin_count"] = TPPin.objects.filter(id=tp.id).count()
                tp_dict["comment_count"] = TPComment.objects.filter(type="Q", tp_ref_id=tp.id).count()
                if _user_id is not None:
                    vote = TPVote.objects.filter(type="Q", tp_ref_id=tp.id, user_id=_user_id)
                else:
                    vote = None
                if vote:
                    vote = TPVote.objects.get(type="Q", tp_ref_id=tp.id, user_id=_user_id)
                    tp_dict["like"] = vote.like
                    tp_dict["dislike"] = vote.dislike
                else:
                    tp_dict["like"] = 0
                    tp_dict["dislike"] = 0

                like_count = TPVote.objects.filter(type="Q", tp_ref_id=tp.id, like=1).count()
                dislike_count = TPVote.objects.filter(type="Q", tp_ref_id=tp.id, dislike=1).count()
                tp_dict["like_count"] = like_count - dislike_count
                TP_list.append(tp_dict)

        return TP_list

    @staticmethod
    def get_proofreading_list(_user_id, _in_lang, _start):
        """
        @summary: proof 의 전체 리스트를  가져오는 함수
        @author: sj46
        @param param1: _user_id - 로그인 한 사용자
        @param param2: _in_lang : proofreading 작성 언어
        @param param3: _start : 가져올 항목의 처음 시작 인덱스
        @return: proof 항목
        """

        TP_list = []
        if _in_lang == 0:
            TPs = TP.objects.filter(type='P').order_by("-created_time")[_start: _start + 15]
        else:
            TPs = TP.objects.filter(type='P', written_lang_id=_in_lang).order_by("-created_time")[
                  _start: _start + 15]

        for tp in TPs:
            if Tutee.objects.filter(user_id=tp.user_id).exists() :
                tp_dict = model_to_dict(tp, exclude=["updated_time", "created_time"])
                tp_dict["created_time"] = str(tp.created_time)
                if TutorService.is_tutor(tp.user_id):
                    if Tutor.objects.filter(user=tp.user_id):
                        tp_dict["writer_info"] = model_to_dict(Tutor.objects.get(user=tp.user_id),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                else:
                    if Tutee.objects.filter(user=tp.user_id):
                        tp_dict["writer_info"] = model_to_dict(Tutee.objects.get(user=tp.user_id),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                tp_dict["answer_count"] = TPAnswer.objects.filter(id=tp.id).count()
                if _user_id is not None:
                    tp_dict["pin"] = TPPin.objects.filter(id=tp.id, user_id=_user_id).count()
                else:
                    tp_dict["pin"] = 0
                tp_dict["pin_count"] = TPPin.objects.filter(id=tp.id).count()
                tp_dict["comment_count"] = TPComment.objects.filter(type="Q", tp_ref_id=tp.id).count()
                if _user_id is not None:
                    vote = TPVote.objects.filter(type="Q", tp_ref_id=tp.id, user_id=_user_id)
                else:
                    vote = None
                if vote:
                    vote = TPVote.objects.get(type="Q", tp_ref_id=tp.id, user_id=_user_id)
                    tp_dict["like"] = vote.like
                    tp_dict["dislike"] = vote.dislike
                else:
                    tp_dict["like"] = 0
                    tp_dict["dislike"] = 0

                if tp.fid:
                    tp_dict["filename"] = File.objects.get(fid=tp.fid).filename

                like_count = TPVote.objects.filter(type="Q", tp_ref_id=tp.id, like=1).count()
                dislike_count = TPVote.objects.filter(type="Q", tp_ref_id=tp.id, dislike=1).count()
                tp_dict["like_count"] = like_count - dislike_count
                TP_list.append(tp_dict)

        return TP_list

    @staticmethod
    def get_translation_proofreading_item(_q_id, _user_id):
        """
        @summary: tran&proof 의  한 항목의 정보를 가져오는 함수
        @author: sj46
        @param param1: _q_id - 질문 id
        @param param2: _user_id : 작성자
        @return: tran&proof 한 항목에 대한 정보
        """

        question_info = {}
        question = TP.objects.get(id=_q_id)
        question_info["question"] = model_to_dict(question, exclude=['updated_time', 'created_time'])
        question_info["question"]["created_time"] = str(question.created_time)
        if TutorService.is_tutor(question.user_id):
            if Tutor.objects.filter(user=question.user_id):
                question_info["writer_info"] = model_to_dict(Tutor.objects.get(user_id=question.user_id),
                                 exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
        else:
            if Tutee.objects.filter(user=question.user_id):
                question_info["writer_info"] = model_to_dict(Tutee.objects.get(user_id=question.user_id),
                                 exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
        question_info["answers"] = Community.get_translation_proofreading_answers(_q_id, _user_id)
        if _user_id is not None:
            question_info["question"]["pin"] = TPPin.objects.filter(id=_q_id, user_id=_user_id).count()
        else:
            question_info["question"]["pin"] = 0
        question_info["question"]["pin_count"] = TPPin.objects.filter(id=_q_id).count()
        question_info["question"]["comments"] = Community.get_translation_proofreading_comments('Q', _q_id)
        if _user_id is not None:
            vote = TPVote.objects.filter(type="Q", tp_ref_id=_q_id, user_id=_user_id)
        else:
            vote = None
        if vote:
            vote = TPVote.objects.get(type="Q", tp_ref_id=_q_id, user_id=_user_id)
            question_info["question"]["like"] = vote.like
            question_info["question"]["dislike"] = vote.dislike
        else:
            question_info["question"]["like"] = 0
            question_info["question"]["dislike"] = 0

        if question.fid:
            question_info["question"]["filename"] = File.objects.get(fid=question.fid).filename

        like_count = TPVote.objects.filter(type="Q", tp_ref_id=_q_id, like=1).count()
        dislike_count = TPVote.objects.filter(type="Q", tp_ref_id=_q_id, dislike=1).count()
        question_info["question"]["like_count"] = like_count - dislike_count

        return question_info

    @staticmethod
    def translation_proofreading_list(user_id=None, page=1):
        """
            @summary: Language Board 리스트
            @author: msjang
            @param user_id: 로그인 한 사용자
            @param page: 가져올 항목의 처음 시작 인덱스
        """
        pass

    @staticmethod
    def translation_proofreading_detail(tp_id):
        """
            @summary: Language Board 게시물 디테일뷰
            @author: msjang
            @param tp_id: 해당 게시물 id
        """
        pass

    @staticmethod
    def translation_proofreading_update(tp_id, user_id, learning_lang, explained_lang,
                                        title, contents, main_img_fid, fid):
        """
            @summary: Language Board 업데이트
            @author: msjang
            @param tp_id: translation_proofreading id
            @param user_id: 로그인 한 사용자
            @param learning_lang: 배우려는 언어
            @param explained_lang: 설명할 언어
            @param title: 제목
            @param contents: 내용
            @param main_img_fid: 메인 이미지 파일 id
            @param fid: 첨부 파일 id
        """
        pass

    @staticmethod
    def translation_proofreading_delete(tp_id, user_id):
        """
            @summary: translation & proofreading 게시물 삭제
            @author: msjang
            @param tp_id: translation_proofreading id
            @param user_id: 로그인 한 사용자
        """
        pass

    @staticmethod
    def translation_proofreading_answer_create(tp_id, user_id, contents):
        """
            @summary: translation & proofreading 게시물 답변 작성
            @author: msjang
            @param tp_id: translation_proofreading id
            @param user_id: 로그인 한 사용자
            @param contents: 답변 내용
        """
        pass

    @staticmethod
    def translation_proofreading_answer_detail(tp_id):
        """
            @summary: Language Board 게시물 답변 상세
            @author: msjang
            @param tp_id: translation_proofreading id
        """
        pass

    @staticmethod
    def add_translation_proofreading_views(_q_id):
        """
        @summary: trans&proof 조회수 증가 함수
        @author: sj46
        @param param1: _q_id : 질문 id
        @return: none
        """

        question = TP.objects.get(id=_q_id)
        question.view_count += 1
        question.save()

    @staticmethod
    def pin_translation_proofreading(_user_id, _q_id):
        """
        @summary: trans&proof pin 하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _q_id : 질문 id
        @return: pin한 상태, pin count
        """

        print _q_id
        try:
            user_pin = TPPin.objects.filter(tp_id=_q_id, user_id=_user_id).count()
            if user_pin:
                TPPin.objects.get(tp_id=_q_id, user_id=_user_id).delete()
                pin_state = 0
            else:
                pin = TPPin()
                pin.user_id = _user_id
                pin.tp_id = _q_id
                pin.save()
                pin_state = 1

            pin_count = TPPin.objects.filter(tp_id=_q_id).count()

            return pin_state, pin_count
        except Exception as err:
            print err

    @staticmethod
    def vote_translation_proofreading(_user_id, _type, _ref_id, _ref_aid):
        """
        @summary: trans&proof 추천 하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _type : 참조할 테이블 유형 'Q' : TP_QUESTION 'A' : TP_ANSWER 'C' : TP_COMMENT
        @param param3: _ref_id : 참조 ID key
        @return: vote 상태, like_count
        """

        print _ref_id
        user_like = TPVote.objects.filter(user_id=_user_id, type=_type, tp_id=_ref_id, tp_ref_id = _ref_aid)
        if user_like:
            TPVote.objects.get(user_id=_user_id, type=_type, tp_id=_ref_id, tp_ref_id = _ref_aid).delete()
            like_state = 0
        else:
            like = TPVote()
            like.user_id = _user_id
            like.type = _type
            like.tp_id = _ref_id
            like.tp_ref_id = _ref_aid
            like.like = 1
            like.dislike = 0
            like.save()
            like_state = 1
        like_count = TPVote.objects.filter(type=_type, tp_id=_ref_id).count()

        return like_state, like_count

    @staticmethod
    def get_translation_proofreading_answers(_q_id, _user_id):
        """
        @summary: tran&proof 의  한 항목의 답변 리스트를 가져오는 함수
        @author: sj46
        @param param1: _q_id - 질문 id
        @param param2: _user_id : 작성자
        @return: tran&proof 한 항목에 달린 전체 답변에 대한 리스트
        """

        question = TP.objects.get(id=_q_id)
        answers = TPAnswer.objects.filter(tp_id=_q_id).order_by('-created_time')
        answer_list = []
        if answers:
            if question.best_answer_id:
                best_answer = TPAnswer.objects.get(id=question.best_answer_id)
                best_answer_dict = model_to_dict(best_answer, exclude=['created_time', 'updated_time'])
                best_answer_dict["created_time"] = str(best_answer.created_time)
                if TutorService.is_tutor(best_answer.user_id):
                    if Tutor.objects.filter(user=best_answer.user_id):
                        best_answer_dict["writer_info"] = model_to_dict(Tutor.objects.get(user_id=best_answer.user_id),
                                     exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                else:
                    if Tutee.objects.filter(user=question.user_id):
                        best_answer_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=best_answer.user_id),
                                     exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                best_answer_dict["comments"] = Community.get_translation_proofreading_comments('A', best_answer.id)
                if _user_id is not None:
                    vote = TPVote.objects.filter(type="A", tp_ref_id=best_answer.id, user_id=_user_id)
                else:
                    vote = None
                if vote:
                    vote = TPVote.objects.get(type="A", tp_ref_id=best_answer.id, user_id=_user_id)
                    best_answer_dict["like"] = vote.like
                    best_answer_dict["dislike"] = vote.dislike
                else:
                    best_answer_dict["like"] = 0
                    best_answer_dict["dislike"] = 0
                like_count = TPVote.objects.filter(type="A", tp_ref_id=best_answer.id, like=1).count()
                dislike_count = TPVote.objects.filter(type="A", tp_ref_id=best_answer.id, dislike=1).count()
                best_answer_dict["like_count"] = like_count - dislike_count
                answer_list.append(best_answer_dict)
    
            for answer in answers:
                if not answer.id == question.best_answer_id:
                    answer_dict = model_to_dict(answer, exclude=['created_time', 'updated_time'])
                    answer_dict["created_time"] = str(answer.created_time)
                    if TutorService.is_tutor(answer.user_id):
                        if Tutor.objects.filter(user=answer.user_id):
                            answer_dict["writer_info"] = model_to_dict(Tutor.objects.get(user_id=answer.user_id),
                                         exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                    else:
                        if Tutee.objects.filter(user=answer.user_id):
                            answer_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=answer.user_id),
                                         exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                    answer_dict["comments"] = Community.get_translation_proofreading_comments('A', answer.id)
                    if _user_id is not None:
                        vote = TPVote.objects.filter(type="A", tp_ref_id=answer.id, user_id=_user_id)
                    else:
                        vote = None
                    if vote:
                        vote = TPVote.objects.get(type="A", tp_ref_id=answer.id, user_id=_user_id)
                        answer_dict["like"] = vote.like
                        answer_dict["dislike"] = vote.dislike
                    else:
                        answer_dict["like"] = 0
                        answer_dict["dislike"] = 0
                    like_count = TPVote.objects.filter(type="A", tp_ref_id=answer.id, like=1).count()
                    dislike_count = TPVote.objects.filter(type="A", tp_ref_id=answer.id, dislike=1).count()
                    answer_dict["like_count"] = like_count - dislike_count
                    answer_list.append(answer_dict)

        return answer_list

    @staticmethod
    def add_translation_proofreading_answer(_user_id, _q_id, _content):
        """
        @summary: tran&proof 의  답변을 저장하는 함수
        @author: sj46
        @param param1: _user_id - 게시글 작성자
        @param param2: _q_id : 질문 id
        @param param3: _content : 답변 내용
        @return: tran&proof 작성한 답변의 id
        """

        answerModel = TPAnswer()
        answerModel.tp_id = _q_id
        answerModel.user_id = _user_id
        answerModel.contents = _content
        answerModel.save()

        return answerModel.id

    @staticmethod
    def modify_translation_proofreading_answer(_user_id, _q_id, _a_id, _content):
        """
        @summary: tran&proof 의  작성된 답변을 업데이트 하는 함수
        @author: sj46
        @param param1: _user_id - 게시글 작성자
        @param param2: _q_id : 질문 id
        @param param3: _a_id : 작성된 답변 id
        @param param4: _content : 답변 내용
        @return: tran&proof 수정한 답변의 id
        """

        modifyanswer = TPAnswer.objects.get(id=_a_id)
        modifyanswer.id = _q_id
        modifyanswer.id = _a_id
        modifyanswer.user_id = _user_id
        modifyanswer.contents = _content
        modifyanswer.save()

        return modifyanswer.id

    @staticmethod
    def delete_translation_proofreading_answer(_user_id, _a_id):
        """
        @summary: trans&proof 의 답변을 삭제하는  함수
        @author: sj46
        @param param1: _user_id - 게시글 작성자
        @param param2: _a_id : 답변 id
        @return: trans&proof 삭제된 답변 정보
        """

        answer = TPAnswer.objects.get(id=_a_id)
        if answer.user_id == _user_id:
            answer.delete()

        return answer

    @staticmethod
    def add_translation_proofreading_comment(_user_id, _type, _tp_id, _tp_ref_id, _content):
        """
        @summary: tran&proof 의  코멘트를 저장하는 함수
        @author: sj46
        @param param1: _user_id - 게시글 작성자
        @param param2: _type : 게시글 타입 Q: 질문 A: 답변  C: Comment
        @param param3: _ref_id : 게시글 ID
        @param param4: _content : 코멘트 내용
        @return: tran&proof 저장한 코멘트 id
        """

        answerModel = TPComment()
        answerModel.type = _type
        answerModel.tp_id = _tp_id
        answerModel.tp_ref_id = _tp_ref_id
        answerModel.user_id = _user_id
        answerModel.contents = _content
        answerModel.save()

        return answerModel.id

    @staticmethod
    def get_translation_proofreading_comments(_type, _ref_id):
        """
        @summary: tran&proof 의  한 항목의 코멘트 리스트를 가져오는 함수
        @author: sj46
        @param param1: _type - 게시글 타입 (Q:질문, A:답변, C:코멘트)
        @param param2: _ref_id : 질문 id
        @return: tran&proof 한 항목에 달린 전체 코멘트 대한 리스트
        """

        comments = TPComment.objects.filter(type=_type, tp_ref_id=_ref_id).order_by('created_time')
        comment_list = []
        for comment in comments:
            comment_dict = model_to_dict(comment, exclude=['created_time'])
            print comment.created_time
            comment_dict["created_time"] = str(comment.created_time)
            if TutorService.is_tutor(comment.user_id):
                if Tutor.objects.filter(user=comment.user_id):
                    comment_dict["writer_info"] = model_to_dict(Tutor.objects.get(user_id=comment.user_id),
                        exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
            else:
                if Tutee.objects.filter(user=comment.user_id):
                    comment_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=comment.user_id),
                        exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
# sub comment delete
#             sub_comments = TPComment.objects.filter(type="C", tp_ref_id=comment.id).order_by('reg_date')
#             comment_dict["sub_comments"] = []
#             for sub_comment in sub_comments:
#                 sub_comment_dict = model_to_dict(sub_comment, exclude=['reg_date'])
#                 sub_comment_dict["reg_date"] = str(sub_comment.reg_date)
#                 sub_comment_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=sub_comment.user_id),
#                                  exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
#                 comment_dict["sub_comments"].append(sub_comment_dict)
            comment_list.append(comment_dict)

        return comment_list

    @staticmethod
    def delete_translation_proofreading_comment(_user_id, _c_id):
        """
        @summary: trans&proof 의 comment를 삭제하는  함수
        @author: sj46
        @param param1: _user_id : 사용자 ID
        @param param2: _c_id : 댓글 ID
        @return: 게시글 타입 P: Post C: Comment, 게시글 ID
        """

        comment = TPComment.objects.get(id=_c_id)
        comment_type = comment.type
        ref_id = comment.tp_ref_id
        if comment.user_id == _user_id:
            sub_comments = TPComment.objects.filter(type='C', tp_ref_id=comment.id)
            if sub_comments:
                comment.contents = '!delete!'
                comment.save()
            else:
                comment.delete()

        return comment_type, ref_id

    @staticmethod
    def violate_translation_proofreading(_user_id, _type, _tp_id, _ref_id, _content):
        """
        @summary: trans&proof 포스트 신고 하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _type : 게시글 타입 (T:trans&proof)
        @param param3: _ref_id : 게시글 ID
        @param param4: _content : 신고 내용
        @return: none
        """

        report = TPViolation()
        report.user_id = _user_id
        report.tp_id = _tp_id
        report.type = _type
        report.tp_ref_id = _ref_id
        report.contents = _content
        report.save()

    @staticmethod
    def set_translation_proofreading_best_answer(_user_id, _q_id, _a_id):
        """
        @summary: trans&proof 베스트 답변 채택하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 ID
        @param param2: _q_id : 질문 id
        @param param3: _a_id : 답변 id
        @return: none
        """

        question = TP.objects.get(id=_q_id)
        if question.user_id == _user_id:
            question.best_answer_id = _a_id
            question.save()

    @staticmethod
    def get_translation_proofreading_answer_user_id(_a_id):
        """
        @summary: trans&proof 답글로 사용자를 알 수 있는 함수
        @author: sj46
        @param param1: _a_id : 답변 id
        @return: 사용자 id
        """

        answer = TPAnswer.objects.get(id=_a_id)

        return answer.user_id


    @staticmethod
    def get_language_question_list(_user_id, _start):
        """
        @summary: Language Qudstion 항목을 구성하는 함수
        @author: sj46
        @param _user_id: 로그인 한 사용자
        @param _start: 가져올 항목의 처음 시작 인덱스
        @return: Language Qudstion 항목
        """
        LQ_list = []
        LQs = LQ.objects.all().order_by("-created_time")[_start: _start + 15]
        for lq in LQs:
            lq_dict = model_to_dict(lq, exclude=["updated_time", "created_time"])
            lq_dict["created_time"] = str(lq.created_time)
            lq_dict["answer_count"] = LQAnswer.objects.filter(id=lq.id).count()
            if _user_id is not None:
                lq_dict["pin"] = LQPin.objects.filter(id=lq.id, user_id=_user_id).count()
            else:
                lq_dict["pin"] = 0
            lq_dict["pin_count"] = LQPin.objects.filter(id=lq.id).count()
            lq_dict["comment_count"] = LQComment.objects.filter(type="Q", lq_ref_id=lq.id).count()
            if _user_id is not None:
                vote = LQVote.objects.filter(type="Q", lq_ref_id=lq.id, user_id=_user_id)
            else:
                vote = None
            if vote:
                vote = LQVote.objects.get(type="Q", lq_ref_id=lq.id, user_id=_user_id)
                lq_dict["like"] = vote.like
                lq_dict["dislike"] = vote.dislike
            else:
                lq_dict["like"] = 0
                lq_dict["dislike"] = 0
            like_count = LQVote.objects.filter(type="Q", lq_ref_id=lq.id, like=1).count()
            dislike_count = LQVote.objects.filter(type="Q", lq_ref_id=lq.id, dislike=1).count()
            lq_dict["like_count"] = like_count - dislike_count
            if TutorService.is_tutor(lq.user_id):
                if Tutor.objects.filter(user_id=lq.user_id):
                    lq_dict["writer_info"] = model_to_dict(Tutor.objects.get(user_id=lq.user_id),
                                     exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
            else:
                if Tutee.objects.filter(user_id=lq.user_id):
                    lq_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=lq.user_id),
                                     exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])

            LQ_list.append(lq_dict)

        return LQ_list

    @staticmethod
    def get_my_language_question_list(_user_id, _start):
        """
        @summary: 로그인한 사용자가 작성한  전체 language question 리스트를  가져오는 함수
        @author: sj46
        @param param1: _user_id - 로그인 한 사용자
        @param param2: _start : 가져올 항목의 처음 시작 인덱스
        @return: language question 항목
        """

        LQ_list = []
        LQs = LQ.objects.filter(user_id=_user_id).order_by("-created_time")[_start: _start + 15]
        for lq in LQs:
            lq_dict = model_to_dict(lq, exclude=["updated_time", "created_time"])
            lq_dict["created_time"] = str(lq.created_time)
            lq_dict["answer_count"] = LQAnswer.objects.filter(id=lq.id).count()
            lq_dict["pin"] = LQPin.objects.filter(id=lq.id, user_id=_user_id).count()
            lq_dict["pin_count"] = LQPin.objects.filter(id=lq.id).count()
            lq_dict["comment_count"] = LQComment.objects.filter(type="Q", lq_ref_id=lq.id).count()
            vote = LQVote.objects.filter(type="Q", lq_ref_id=lq.id, user_id=_user_id)
            if vote:
                vote = LQVote.objects.get(type="Q", lq_ref_id=lq.id, user_id=_user_id)
                lq_dict["like"] = vote.like
                lq_dict["dislike"] = vote.dislike
            else:
                lq_dict["like"] = 0
                lq_dict["dislike"] = 0

            like_count = LQVote.objects.filter(type="Q", lq_ref_id=lq.id, like=1).count()
            dislike_count = LQVote.objects.filter(type="Q", lq_ref_id=lq.id, dislike=1).count()
            lq_dict["like_count"] = like_count - dislike_count
            if TutorService.is_tutor(lq.user_id):
                if Tutor.objects.filter(user_id=lq.user_id):
                    lq_dict["writer_info"] = model_to_dict(Tutor.objects.get(user_id=lq.user_id),
                                     exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
            else:
                if Tutee.objects.filter(user=lq.user_id):
                    lq_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=lq.user_id),
                                     exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])

            LQ_list.append(lq_dict)

        return LQ_list

    @staticmethod
    def get_language_question_answer_list(_user_id, _start):
        """
        @summary: language question 의 답변 리스트를  가져오는 함수
        @author: sj46
        @param param1: _user_id - 로그인 한 사용자
        @param param2: _start : 가져올 항목의 처음 시작 인덱스
        @return: language question 답변 항목
        """

        LQ_list = []
        Answers = LQAnswer.objects.filter(user_id=_user_id).order_by("-created_time")[_start: _start + 15]
        for answer in Answers:
            lq = LQ.objects.get(id=answer.id)
            lq_dict = model_to_dict(lq, exclude=["updated_time", "created_time"])
            lq_dict["created_time"] = str(lq.created_time)
            lq_dict["answer_count"] = LQAnswer.objects.filter(id=lq.id).count()
            lq_dict["pin"] = LQPin.objects.filter(id=lq.id, user_id=_user_id).count()
            lq_dict["pin_count"] = LQPin.objects.filter(id=lq.id).count()
            lq_dict["comment_count"] = LQComment.objects.filter(type="Q", lq_ref_id=lq.id).count()
            vote = LQVote.objects.filter(type="Q", lq_ref_id=lq.id, user_id=_user_id)
            if vote:
                vote = LQVote.objects.get(type="Q", lq_ref_id=lq.id, user_id=_user_id)
                lq_dict["like"] = vote.like
                lq_dict["dislike"] = vote.dislike
            else:
                lq_dict["like"] = 0
                lq_dict["dislike"] = 0

            like_count = LQVote.objects.filter(type="Q", lq_ref_id=lq.id, like=1).count()
            dislike_count = LQVote.objects.filter(type="Q", lq_ref_id=lq.id, dislike=1).count()
            lq_dict["like_count"] = like_count - dislike_count
            if TutorService.is_tutor(lq.user_id):
                if Tutor.objects.filter(user_id=answer.user_id):
                    lq_dict["writer_info"] = model_to_dict(Tutor.objects.get(user_id=answer.user_id),
                                     exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
            else:
                if Tutee.objects.filter(user=lq.user_id):
                    lq_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=answer.user_id),
                                     exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])


                LQ_list.append(lq_dict)

        return LQ_list

    @staticmethod
    def get_language_question_search_list(_user_id, _start, _written, _translated):
        """
        @summary: language question 의  리스트를  가져오는 함수
        @author: sj46
        @param param1: _user_id - 로그인 한 사용자
        @param param2: _start : 가져올 항목의 처음 시작 인덱스
        @param param3: _written : 쓰여진 언어
        @param param4: _translated : 질문으로 알고자 하는 언어
        @return: language question 항목
        """

        LQ_list = []

        if _written == 0 and _translated == 0:
            LQs = LQ.objects.all().order_by("-created_time")[_start: _start + 15]
        #             LQs = LQ.objects.filter(user_id__in = Tutee.objects.all()).select_related().order_by( "-created_time" )[_start: _start+10]
        elif _written != 0 and _translated == 0:
            LQs = LQ.objects.filter(written_lang=_written).order_by("-created_time")[_start: _start + 15]
        elif _written == 0 and _translated != 0:
            LQs = LQ.objects.filter(question_lang=_translated).order_by("-created_time")[
                  _start: _start + 15]
        else:
            LQs = LQ.objects.filter(written_lang=_written, question_lang=_translated).order_by(
                "-created_time")[_start: _start + 15]

        for lq in LQs:
            if Tutee.objects.filter(user_id=lq.user_id):
                lq_dict = model_to_dict(lq, exclude=['created_time', 'updated_time'])
                lq_dict["created_time"] = str(lq.created_time)
                if TutorService.is_tutor(lq.user_id):
                    if Tutor.objects.filter(user=lq.user_id):
                        lq_dict["writer_info"] = model_to_dict(Tutor.objects.get(user_id=lq.user_id),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                else:
                    if Tutee.objects.filter(user=lq.user_id):
                        lq_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=lq.user_id),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                lq_dict["answer_count"] = LQAnswer.objects.filter(id=lq.id).count()
                if _user_id is not None:
                    lq_dict["pin"] = LQPin.objects.filter(id=lq.id, user_id=_user_id).count()
                else:
                    lq_dict["pin"] = 0
                lq_dict["pin_count"] = LQPin.objects.filter(id=lq.id).count()
                lq_dict["comment_count"] = LQComment.objects.filter(type="Q", lq_ref_id=lq.id).count()
                if _user_id is not None:
                    vote = LQVote.objects.filter(type="Q", lq_ref_id=lq.id, user_id=_user_id)
                else:
                    vote = None
                if vote:
                    lq_dict["like"] = vote.like
                    lq_dict["dislike"] = vote.dislike
                else:
                    lq_dict["like"] = 0
                    lq_dict["dislike"] = 0

                like_count = LQVote.objects.filter(type="Q", lq_ref_id=lq.id, like=1).count()
                dislike_count = LQVote.objects.filter(type="Q", lq_ref_id=lq.id, dislike=1).count()
                lq_dict["like_count"] = like_count - dislike_count

                LQ_list.append(lq_dict)

        return LQ_list

    @staticmethod
    def add_language_question_views(_q_id):
        """
        @summary: language question 조회수 증가 함수
        @author: sj46
        @param param1: _q_id : 질문 id
        @return: none
        """

        question = LQ.objects.get(id=_q_id)
        question.view_count += 1
        question.save()

    @staticmethod
    def get_language_question_item(_q_id, _user_id):
        """
        @summary: language question 의  한 항목의 정보를 가져오는 함수
        @author: sj46
        @param param1: _q_id - 질문 id
        @param param2: _user_id : 작성자
        @return: language question 한 항목에 대한 정보
        """

        question_info = {}
        question = LQ.objects.get(id=_q_id)
        question_info["question"] = model_to_dict(question, exclude=['updated_time', 'created_time'])
        question_info["question"]["created_time"] = str(question.created_time)
        if TutorService.is_tutor(question.user_id):
            if Tutor.objects.filter(user=question.user_id):
                question_info["writer_info"] = model_to_dict(Tutor.objects.get(user_id=question.user_id),
                    exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
        else:
            if Tutee.objects.filter(user=question.user_id):
                question_info["writer_info"] = model_to_dict(Tutee.objects.get(user_id=question.user_id),
                    exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
        question_info["answers"] = Community.get_language_question_answers_list(_q_id, _user_id)
        if _user_id is not None:
            question_info["question"]["pin"] = LQPin.objects.filter(id=_q_id, user_id=_user_id).count()
        else:
            question_info["question"]["pin"] = 0
        question_info["question"]["pin_count"] = LQPin.objects.filter(id=_q_id).count()
        question_info["question"]["comments"] = Community.get_language_question_comments('Q', _q_id)
        if _user_id is not None:
            vote = LQVote.objects.filter(type="Q", lq_id=_q_id, user_id=_user_id)
        else:
            vote = None
        if vote:
            vote = LQVote.objects.get(type="Q", lq_id=_q_id, user_id=_user_id)
            question_info["question"]["like"] = vote.like
            question_info["question"]["dislike"] = vote.dislike
        else:
            question_info["question"]["like"] = 0
            question_info["question"]["dislike"] = 0

        if question.fid:
            question_info["question"]["filename"] = File.objects.get(fid=question.fid).filename

        like_count = LQVote.objects.filter(type="Q", lq_id=_q_id, like=1).count()
        dislike_count = LQVote.objects.filter(type="Q", lq_id=_q_id, dislike=1).count()
        question_info["question"]["like_count"] = like_count - dislike_count

        return question_info

    @staticmethod
    def create_language_question(_user_id, _title, _content, _written_id, _written_name, _question_id, _question_name, _point, _img_file):
        """
        @summary: language Question 포스트 하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _title : 게시글 제목
        @param param3: _content : 게시글 내용
        @param param4: _written : 쓰여진 언어
        @param param5: _translated : 번역될 언어
        @param param6: _point : 답변 채택 시 걸 포인트
        @param param7: _img_file : 첨부할 파일의 이미지 정보
        @return: url : /questions/+작성된 포스트 id
        """

        lq = LQ()
        lq.user_id = _user_id
        lq.title = _title
        lq.contents = _content
        if _written_id == 0:
            lq.written_lang_id = None
            lq.written_lang_name = ""
        else:
            lq.written_lang_id = _written_id
            lq.written_lang_name = _written_name

        if _question_id == 0:
            lq.question_lang_id = None
            lq.question_lang_name = ""
        else:
            lq.question_lang_id = _question_id
            lq.question_lang_name = _question_name

        lq.best_answer_credit = _point
        lq.view_count = 0
#         lq.expired_time = timezone.now()
        lq.created_time = timezone.now()
        lq.updated_time = timezone.now()
        lq.save()

        if _img_file:
            fid = Community.upload_file(_img_file, "LQ", lq.id)
            lq.fid = fid
            lq.save()

        return "/questions/" + str(lq.id) + "/", lq.id

    @staticmethod
    def update_language_question(_user_id, _id, _title, _content, _written_id, _written_name, _question_id, _question_name, _point, _img_file):
        """
        @summary: language Question 편집하는 하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _id : 편집 할 post id
        @param param3: _title : 게시글 제목
        @param param4: _content : 게시글 내용
        @param param5: _written : 쓰여진 언어
        @param param6: _translated : 번역될 언어
        @param param7: _point : 답변 채택 시 걸 포인트
        @param param8: _img_file : 첨부할 파일의 이미지 정보
        @return: url : /questions/+편집된 포스트 id
        """

        modifylq = LQ.objects.get(id=_id)
        modifylq.user_id = _user_id
        modifylq.title = _title
        modifylq.contents = _content
        if _written_id == 0:
            modifylq.written_lang_id = None
            modifylq.written_lang_name = ""
        else:
            modifylq.written_lang_id = _written_id
            modifylq.written_lang_name = _written_name

        if _question_id == 0:
            modifylq.question_lang_id = None
            modifylq.question_lang_name = ""
        else:
            modifylq.question_lang_id = _question_id
            modifylq.question_lang_name = _question_name
        modifylq.best_answer_credit = _point
        modifylq.view_count += 1
        modifylq.updated_time = timezone.now()
        modifylq.save()

        if _img_file:
            fid = Community.upload_file(_img_file, "LQ", modifylq.id)
            modifylq.fid = fid
            modifylq.save()

        return "/questions/" + str(modifylq.id) + "/", modifylq.id

    @staticmethod
    def add_language_question_answer(_user_id, _q_id, _content):
        """
        @summary: language question 의  답변을 저장하는 함수
        @author: sj46
        @param param1: _user_id - 게시글 작성자
        @param param2: _q_id : 질문 id
        @param param3: _content : 답변 내용
        @return: language question 작성한 답변의 id
        """

        answerModel = LQAnswer()
        answerModel.lq_id = _q_id
        answerModel.user_id = _user_id
        answerModel.contents = _content
        answerModel.save()

        return answerModel.id

    @staticmethod
    def modify_language_question_answer(_user_id, _q_id, _a_id, _content):
        """
        @summary: language question 의  작성된 답변을 편집 하는 함수
        @author: sj46
        @param param1: _user_id - 게시글 작성자
        @param param2: _q_id : 질문 id
        @param param3: _a_id : 작성된 답변 id
        @param param4: _content : 답변 내용
        @return: language question 수정한 답변의 id
        """

        modifyanswer = LQAnswer.objects.get(id=_a_id)
        modifyanswer.id = _q_id
        modifyanswer.id = _a_id
        modifyanswer.user_id = _user_id
        modifyanswer.contents = _content
        modifyanswer.save()

        return modifyanswer.id

    @staticmethod
    def delete_language_question(_q_id):
        """
        @summary: language question 의 post를 삭제하는  함수
        @author: sj46
        @param param1: _p_id : post id
        @return: language question 삭제된 post 정보
        """

        qna = LQ.objects.get(id=_q_id)
        qna.delete()

        return qna

    @staticmethod
    def delete_language_question_answer(_user_id, _a_id):
        """
        @summary: language question 의 답변을 삭제하는  함수
        @author: sj46
        @param param1: _user_id - 게시글 작성자
        @param param2: _a_id : 답변 id
        @return: language question 삭제된 답변 정보
        """

        answer = LQAnswer.objects.get(id=_a_id)
        if answer.user_id == _user_id:
            answer.delete()

        return answer

    @staticmethod
    def get_language_question_answers_list(_q_id, _user_id):
        """
        @summary: language question 의  한 항목의 답변 리스트를 가져오는 함수
        @author: sj46
        @param param1: _q_id - 질문 id
        @param param2: _user_id : 작성자
        @return: language question 한 항목에 달린 전체 답변에 대한 리스트
        """

        question = LQ.objects.get(id=_q_id)
        answers = LQAnswer.objects.filter(lq_id=_q_id).order_by('-created_time')
        answer_list = []
        if answers:
            if question.best_answer_id:
                best_answer = LQAnswer.objects.get(id=question.best_answer_id)
                best_answer_dict = model_to_dict(best_answer, exclude=['created_time', 'updated_time'])
                best_answer_dict["created_time"] = str(best_answer.created_time)
                if TutorService.is_tutor(best_answer.user_id):
                    if Tutor.objects.filter(user=best_answer.user_id):
                        best_answer_dict["writer_info"] = model_to_dict(Tutor.objects.get(user_id=best_answer.user_id),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                else:
                    if Tutee.objects.filter(user=best_answer.user_id):
                        best_answer_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=best_answer.user_id),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                best_answer_dict["comments"] = Community.get_language_question_comments('A', best_answer.id)
                if _user_id is not None:
                    vote = LQVote.objects.filter(type="A", lq_ref_id=best_answer.id, user_id=_user_id)
                else:
                    vote = None
                if vote:
                    vote = LQVote.objects.get(type="A", lq_ref_id=best_answer.id, user_id=_user_id)
                    best_answer_dict["like"] = vote.like
                    best_answer_dict["dislike"] = vote.dislike
                else:
                    best_answer_dict["like"] = 0
                    best_answer_dict["dislike"] = 0
                like_count = LQVote.objects.filter(type="A", lq_ref_id=best_answer.id, like=1).count()
                dislike_count = LQVote.objects.filter(type="A", lq_ref_id=best_answer.id, dislike=1).count()
                best_answer_dict["like_count"] = like_count - dislike_count
                answer_list.append(best_answer_dict)
            
            for answer in answers:
                if not answer.id == question.best_answer_id:
                    answer_dict = model_to_dict(answer, exclude=['created_time', 'updated_time'])
                    answer_dict["created_time"] = str(answer.created_time)
                    if TutorService.is_tutor(answer.user_id):
                        if Tutor.objects.filter(user_id=answer.user_id):
                            answer_dict["writer_info"] = model_to_dict(Tutor.objects.get(user_id=answer.user_id),
                                             exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                    else:
                        if Tutee.objects.filter(user_id=answer.user_id):
                            answer_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=answer.user_id),
                                             exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                    answer_dict["comments"] = Community.get_language_question_comments('A', answer.id)
                    if _user_id is not None:
                        vote = LQVote.objects.filter(type="A", lq_ref_id=answer.id, user_id=_user_id)
                    else:
                        vote = None
                    if vote:
                        vote = LQVote.objects.get(type="A", lq_ref_id=answer.id, user_id=_user_id)
                        answer_dict["like"] = vote.like
                        answer_dict["dislike"] = vote.dislike
                    else:
                        answer_dict["like"] = 0
                        answer_dict["dislike"] = 0
                    like_count = LQVote.objects.filter(type="A", lq_ref_id=answer.id, like=1).count()
                    dislike_count = LQVote.objects.filter(type="A", lq_ref_id=answer.id, dislike=1).count()
                    answer_dict["like_count"] = like_count - dislike_count
                    answer_list.append(answer_dict)

        return answer_list

    @staticmethod
    def add_language_question_comment(_user_id, _type, _lq_id, _lq_ref_id, _content):
        """
        @summary: language question 의  코멘트를 저장하는 함수
        @author: sj46
        @param param1: _user_id - 게시글 작성자
        @param param2: _type : 게시글 타입 P: Post C: Comment
        @param param3: _ref_id : 게시글 ID
        @param param4: _content : 코멘트 내용
        @return: language question 저장한 코멘트 id
        """

        answerModel = LQComment()
        answerModel.type = _type
        answerModel.lq_id = _lq_id
        answerModel.lq_ref_id = _lq_ref_id
        answerModel.user_id = _user_id
        answerModel.contents = _content
        answerModel.save()

        return answerModel.id

    @staticmethod
    def get_language_question_comments(_type, _lq_ref_id):
        """
        @summary: language question 의  한 항목의 코멘트 리스트를 가져오는 함수
        @author: sj46
        @param param1: _type - 게시글 타입 (Q:질문, A:답변, C:코멘트)
        @param param2: _ref_id : 질문 id
        @return: language question 한 항목에 달린 전체 답변에 대한 리스트
        """

        comments = LQComment.objects.filter(type=_type, lq_ref_id=_lq_ref_id).order_by('created_time')
        comment_list = []
        for comment in comments:
            if Tutee.objects.filter(user_id=comment.user_id):
                comment_dict = model_to_dict(comment, exclude=['created_time'])
                comment_dict["created_time"] = str(comment.created_time)
                if TutorService.is_tutor(comment.user_id):
                    if Tutor.objects.filter(user=comment.user_id):
                        comment_dict["writer_info"] = model_to_dict(Tutor.objects.get(user_id=comment.user_id),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                else:
                    if Tutee.objects.filter(user=comment.user_id):
                        comment_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=comment.user_id),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
# sub comment delete
#                 sub_comments = LQComment.objects.filter(type="C", lq_ref_id=comment.id).order_by('reg_date')
#                 comment_dict["sub_comments"] = []
#                 for sub_comment in sub_comments:
#                     sub_comment_dict = model_to_dict(sub_comment, exclude=['reg_date'])
#                     sub_comment_dict["reg_date"] = str(sub_comment.reg_date)
#                     sub_comment_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=sub_comment.user_id),
#                                      exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
#                     comment_dict["sub_comments"].append(sub_comment_dict)
                comment_list.append(comment_dict)

        return comment_list

    @staticmethod
    def delete_language_question_comment(_user_id, _c_id):
        """
        @summary: language learning club 의 comment를 삭제하는  함수
        @author: sj46
        @param param1: _user_id : 사용자 ID
        @param param2: _c_id : 댓글 ID
        @return: 게시글 타입 P: Post C: Comment, 게시글 ID
        """

        comment = LQComment.objects.get(id=_c_id)
        comment_type = comment.type
        ref_id = comment.lq_ref_id
        if comment.user_id == _user_id:
            sub_comments = LQComment.objects.filter(type='C', lq_ref_id=comment.id)
            if sub_comments:
                comment.contents = '!delete!'
                comment.save()
            else:
                comment.delete()

        return comment_type, ref_id

    @staticmethod
    def pin_language_question(_user_id, _q_id):
        """
        @summary: language question pin 하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _q_id : 질문 id
        @return: pin한 상태, pin count
        """

        try:
            user_pin = LQPin.objects.filter(lq_id=_q_id, user_id=_user_id).count()
            if user_pin:
                LQPin.objects.get(lq_id=_q_id, user_id=_user_id).delete()
                pin_state = 0
            else:
                pin = LQPin()
                pin.user_id_id = _user_id
                pin.lq_id = _q_id
                pin.save()
                pin_state = 1

            pin_count = LQPin.objects.filter(lq_id=_q_id).count()

            return pin_state, pin_count
        except Exception as err:
            print err

    @staticmethod
    def vote_language_question(_user_id, _type, _lq_id, _lq_ref_id):
        """
        @summary: language question 추천 하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _type : 참조할 테이블 유형 'Q' : LQ_QUESTION 'A' : LQ_ANSWER 'C' : LQ_COMMENT
        @param param3: _ref_id : 참조 ID key
        @return: vote 상태, like_count
        """

        user_like = LQVote.objects.filter(user_id=_user_id, type=_type, lq_ref_id=_lq_ref_id)
        if user_like:
            LQVote.objects.get(user_id=_user_id, type=_type, lq_ref_id=_lq_ref_id).delete()
            like_state = 0
        else:
            like = LQVote()
            like.user_id = _user_id
            like.type = _type
            like.lq_id = _lq_id
            like.lq_ref_id = _lq_ref_id
            like.like = 1
            like.dislike = 0
            like.save()
            like_state = 1
        like_count = LQVote.objects.filter(type=_type, lq_ref_id=_lq_ref_id).count()

        return like_state, like_count

    @staticmethod
    def violate_language_question(_user_id, _type, _qna_id, _ref_id, _content):
        """
        @summary: language question 포스트 신고 하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _type : 게시글 타입 (Q:Language question)
        @param param3: _ref_id : 게시글 ID
        @param param4: _content : 신고 내용
        @return: none
        """

        report = LQViolation()
        report.user_id_id = _user_id
        report.lq_id_id = _qna_id
        report.type = _type
        report.lq_ref_id = _ref_id
        report.contents = _content
        report.save()

    @staticmethod
    def set_language_question_best_answer(_user_id, _q_id, _a_id):
        """
        @summary: language question 베스트 답변 채택하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 ID
        @param param2: _q_id : 질문 id
        @param param3: _a_id : 답변 id
        @return: none
        """

        question = LQ.objects.get(id=_q_id)
        if question.user_id == _user_id:
            question.best_answer_id = _a_id
            question.save()

    @staticmethod
    def get_language_question_answer_user_id(_a_id):
        """
        @summary: language question 답글로 사용자를 알 수 있는 함수
        @author: sj46
        @param param1: _a_id : 답변 id
        @return: 사용자 id
        """

        answer = LQAnswer.objects.get(id=_a_id)

        return answer.user_id

    @staticmethod
    def upload_media(_img_file):
        """
        @summary: language learning club 이미지 파일 업로드 하는 함수
        @author: sj46
        @param param1: _img_file : 첨부할 파일 정보
        @return: 첨부한 파일 id, 파일 경로
        """

        # int(round(time.time() * 1000)) time milliseconds
        try:
            image_name = "%d&&%s" % (int(round(time.time() * 1000)), _img_file.name)
            image_path = '%s/%s' % ("/static/img/upload", image_name)
            full_path = '%s%s' % (BASE_DIR, image_path)
            with open(full_path, 'wb') as fp:
                for chunk in _img_file.chunks():
                    fp.write(chunk)
            imgfile = File()
            imgfile.type = 'LC'
            imgfile.filename = image_name
            imgfile.filepath = image_path
            imgfile.save()
            return imgfile.fid, image_path

        except Exception as err:
            print err

    @staticmethod
    def upload_video_preview(_url):
        """
        @summary: language learning club 동영상 파일 업로드 하는 함수
        @author: sj46
        @param param1: _url : 첨부할 파일 정보
        @return: 첨부한 파일 id
        """

        try:
            imgfile = File()
            imgfile.type = 'LC'
            imgfile.filename = _url
            imgfile.filepath = _url
            imgfile.save()
            return imgfile.fid
        except Exception as err:
            print err

    @staticmethod
    def upload_file(_img_file, _type, _ref_id):
        """
        @summary: 첨부된 파일을 업로드 하는 함수
        @author: sj46
        @param param1: _img_file : 파일 정보
        @param param2: _type : 참조할 테이블 유형 'LQ' : LQ_QUESTION 'LA' : LQ_ANSWER 'TQ' : TP_QUESTION 'TA' : TP_ANSWER 'LC' : LC_POST 'HF' : HELP_FAQ_ML 'TT' : Tutor Profile Resume 'AP' : Tutor Applications 'HQ' : HELP_QNA
        @param param3: _ref_id : 참조 ID
        @return: 첨부된 파일 id
        """

        # int(round(time.time() * 1000)) time milliseconds
        try:
            image_name = "%d&&%s" % (int(round(time.time() * 1000)), _img_file.name)
            image_path = '%s/%s' % ("/static/img/upload", image_name)
            full_path = '%s%s' % (BASE_DIR, image_path)
            with open(full_path, 'wb') as fp:
                for chunk in _img_file.chunks():
                    fp.write(chunk)
        except Exception as err:
            print err

        imgfile = File()
        imgfile.type = _type
        imgfile.filename = image_name
        imgfile.filepath = image_path
        imgfile.ref_id = _ref_id
        imgfile.save()

        return imgfile.fid


    # SettingsNotification, Reservation
    @staticmethod
    def set_notification_club_post(_user_id, pid):
        """
        @summary: 내가 친구 등록한 사용자가 클럽에 글 작성 시 알림을 주는 함수
        @author: sj46
        @param param1: _user_id : 친구 요청한 id
        @param param2: _pid : 친구 요청한 사용자가 작성한 post id
        @return: none
        """
        
        print 'set_notification_club_post'
        try:
            # TODO all freinds, if tutor-> all tutee
            # to_user =
            friend_list = []
            tutee_list = []
            to_user_list = []
            friends = Friend.objects.filter((Q(req_user=_user_id) | Q(res_user=_user_id)) & Q(status=2))
            tutees = None
            
            for f in friends:
                if f.req_user_id == _user_id:
                    friend_list.append(f.res_user_id)
                else:
                    friend_list.append(f.req_user_id)

            if Tutor.objects.get(user_id=_user_id) and Tutor.objects.get(user_id=_user_id).is_available == 1:
                tutee_list = Reservation.objects.filter(tutor_id=_user_id).values("tutee_id").distinct()

            print friend_list
            print tutee_list
            for tu in tutee_list:
                friend_list.append(tu.get('tutee_id'))

            to_user_list = list(set(friend_list))
            print 'club_post() to_user_list: '
            print to_user_list
            for to_user in to_user_list:
                if SettingsNotification.objects.get(user_id=to_user).t_lb_new == 1:
                    noti = Notification()
                    noti.sender_id = _user_id
                    noti.receiver_id = to_user
                    # 1:lesson, 2:club, 3:trans, 4:qa
                    noti.type = 2
                    # post, comment 등에 따른 history id
                    noti.noti_text = 1
                    # 1:post, 2:comment, 3:reply to comment
                    noti.community_type = 1
                    # post or comment id
                    noti.ref_id = pid
                    noti.redirect_url = "/board/" + str(pid)
                    noti.is_read = 0
                    noti.save()
                if SettingsNotification.objects.get(user_id=to_user).m_lb_new == 1:
                    print 'send_mail post'
                    _title = LB.objects.get(id=pid).title
                    _user_name = Tutee.objects.get(user_id=_user_id).name
                    to_user_email = Tutee.objects.select_related('user').get(user_id=to_user).user.email
                    send_noti_email(to_user_email, "New post on language learning club by \'" + _user_name + "\'",
                                    "\'" + _user_name + "\' added a post : \"" + str(_title) + "\"")
        except Exception as e:
            print 'set_notification_club_post() error'
            print e

    @staticmethod
    def set_notification_club_comment(_user_id, _type, _refid, comment_id):
        """
        @summary: 내가 작성한 클럽에 코멘트가 달렸을 경우 알림을 주는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id (from_user)
        @param param2: _type : P:post, C:comment
        @param param3: _refid : 게시글 id
        @param param4: _comment : 작성된 코멘트 내용
        @return: none
        """

        print 'set_notification_club_comment'
        # print _user_id, _type, _refid, _comment
        # print _comment[0]['id']
        
        #kihyun
        #reply_id = _comment[0]['id']
        comment_id = comment_id

        try:
            # post
            if _type == "P":
                lc = LB.objects.get(id=_refid)

                from_user = _user_id
                to_user = lc.user_id
                _type = 2
                # comment
                noti_text = 2
                community_type = 2
                ref_id = comment_id
                redirect_url = "/board/" + str(_refid)
                state = 0

            # comment
            else:
                lc = LBComment.objects.get(id=_refid)

                from_user = _user_id
                to_user = lc.user_id
                _type = 2
                # comment
                noti_text = 3
                community_type = 3
                ref_id = comment_id
                redirect_url = "/club/" + str(lc.ref_id)
                state = 0

            if SettingsNotification.objects.get(user_id=to_user).t_lb_comment == 1:
                Notification(
                    sender_id=from_user,
                    receiver_id=to_user,
                    type=_type,
                    #type_msg=type_msg, #noti text?
                    noti_text=noti_text,
                    #ntid=ntid,
                    ref_id=ref_id,
                    community_type=community_type,
                    #msg_id=msg_id, 해당 메시지 아이디 (코멘트, 답변등의 아이디, 삭제된듯)
                    redirect_url=redirect_url,
                    is_read=state
                ).save()

            if SettingsNotification.objects.get(user_id=to_user).m_lb_comment == 1:
                print 'send_mail comment'
                _comment = LBComment.objects.get(id=comment_id).contents
                _user_name = Tutee.objects.get(user_id=_user_id).name
                to_user_email = Tutee.objects.select_related('user').get(user_id=to_user).user.email
                send_noti_email(to_user_email, "New comment on your writing ",
                                "\'" + _user_name + "\' says … \"" + _comment + "\"")

        except Exception as e:
            print 'errrror, '
            print e

    @staticmethod
    def set_notification_club_vote(user_id, refid, state):
        """
        @summary: 내가 작성한 클럽 포스트를 추천할 경우 알림을 주는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _type : 1:lesson, 2:club, 3:trans, 4:qa
        @param param3: _refid : 게시글 id
        @param param4: state : 읽음상태표시
        @return: none
        """

        print 'set_notification_club_vote'
        print user_id, refid, state

        if state == 1:
            to_user_id = LB.objects.get(id=refid).user_id
            if SettingsNotification.objects.get(user_id=to_user_id).t_lb_upvote == 1:
                Notification(
                    sender_id=user_id,
                    receiver_id=to_user_id,
                    type=2,
                    noti_text=4,
                    community_type=4,
                    ref_id=refid,
                    redirect_url='/board/' + str(refid),
                    is_read=0
                ).save()

    @staticmethod
    def set_notification_trans_post(_user_id, pid, _type):
        """
        @summary: 친구로 등록된 사용자가  trans&proof포스트 작성 시  알림을 주는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: pid : 게시글 id
        @param param3: _type : 0: trans, 1: proof
        @return: none
        """

        try:
            # TODO all freinds, if tutor-> all tutee
            # to_user =
            friend_list = []
            tutor_list = []
            to_user_list = []
            friends = Friend.objects.filter((Q(req_user=_user_id) | Q(res_user=_user_id)) & Q(status=2))

            for f in friends:
                if f.req_user_id == _user_id:
                    friend_list.append(f.res_user_id)
                else:
                    friend_list.append(f.req_user_id)

            tutor_list = Reservation.objects.filter(tutee_id=_user_id).values("tutor_id").distinct()

            for tu in tutor_list:
                friend_list.append(tu.get('tutor_id'))

            to_user_list = list(set(friend_list))

            for to_user in to_user_list:
                if SettingsNotification.objects.get(user_id=to_user).t_tp_new == 1:
                    noti = Notification()
                    noti.sender_id = _user_id
                    noti.receiver_id = to_user
                    # 1:lesson, 2:club, 3:trans, 4:qa
                    noti.type = 3
                    # post, comment 등에 따른 history id
                    if int(_type) == 0:
                        # trnas
                        noti.noti_text = 5
                    else:
                        # proof
                        noti.noti_text = 6
                    print 'type:' + str(_type) + ', ' + str(type(_type))
                    # 1:post, 2:comment, 3:reply to comment
                    noti.community_type = 1
                    # post or comment id
                    noti.ref_id = pid
                    noti.redirect_url = "/trans/" + str(pid) + "/"
                    noti.is_read = 0
                    noti.save()

                if SettingsNotification.objects.get(user_id=to_user).m_tp_new == 1:
                    print 'send_mail trans post'
                    _title = TP.objects.get(id=pid).title
                    from_user_name = Tutee.objects.get(user_id=_user_id).name
                    to_user_email = Tutee.objects.select_related('user').get(user_id=to_user).user.email
                    send_noti_email(to_user_email, "\'" + from_user_name + "\' asked a Question : " + _title,
                                    "\'" + from_user_name + "\' asked, \"" + _title + "\"")

        except Exception as e:
            print 'set_notification_trans() error'
            print e

    @staticmethod
    def set_notification_trans_answer(_user_id, id, aid):
        """
        @summary: 내가 작성한 trans&proof포스트에 답변이 달렸을 때  알림을 주는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: id : 게시글 id
        @param param3: aid : 답변 id
        @return: none
        """

        print 'set_notification_trans_answer'
        print _user_id, aid

        ta = TPAnswer.objects.get(id=aid)
        tp = TP.objects.get(id=ta.tp_id)

        if SettingsNotification.objects.get(user_id=tp.user_id).t_tp_answer == 1:
            # question 작성자에게 알림
            noti = Notification()
            noti.sender_id  = _user_id
            noti.receiver_id = tp.user_id
            # 1:lesson, 2:club, 3:trans, 4:qa
            noti.type = 3
            # post, comment 등에 따른 history id
            noti.noti_text = 9
            # 1:post, 2:comment, 3:reply to comment
            noti.community_type = 5
            # post or comment id
            noti.ref_id = aid
            noti.redirect_url = "/trans/" + str(tp.id) + "/"
            noti.is_read = 0
            noti.save()

        if SettingsNotification.objects.get(user_id=tp.user_id).m_tp_answer == 1:
            print 'send_mail trans answer'
            _title = tp.title
            _content = TPAnswer.objects.get(id=aid).contents
            from_user_name = Tutee.objects.get(user_id=_user_id).name
            to_user_email = Tutee.objects.select_related('user').get(user_id=tp.user_id).user.email
            send_noti_email(to_user_email, "New answer for your question : " + _title,
                            "\'" + from_user_name + "\' answers, \"" + _content + "\"")

        # pin 한사람들에게 알림
        print 'pin user send'
        to_user_list = TPPin.objects.filter(tp_id=tp.id)

        for to_user in to_user_list:
            if SettingsNotification.objects.get(user_id=to_user.user_id).t_tp_pin == 1:
                noti = Notification()
                noti.sender_id = _user_id
                noti.receiver_id = to_user.user_id
                # 1:lesson, 2:club, 3:trans, 4:qa
                noti.type = 3
                # post, comment 등에 따른 history id
                noti.noti_text = 10
                # 1:post, 2:comment, 3:reply to comment
                noti.community_type = 5
                # post or comment id
                noti.ref_id = aid
                noti.redirect_url = "/trans/" + str(tp.id) + "/"
                noti.is_read = 0
                noti.save()

            if SettingsNotification.objects.get(user_id=to_user.user_id).m_tp_pin == 1:
                print 'send_mail trans answer pinned'
                _title = tp.title
                _content = TPAnswer.objects.get(id=aid).contents
                from_user_name = Tutee.objects.get(user_id=_user_id).name
                to_user_email = Tutee.objects.select_related('user').get(user_id=to_user.user_id).user.email
                send_noti_email(to_user_email, "New answer for the question you pinned : " + _title,
                                "\'" + from_user_name + "\' answers, \"" + _content + "\"")

    @staticmethod
    def set_notification_trans_comment(_user_id, _type, _refid, cid):
        """
        @summary: 내가 작성한 trans&proof포스트에 댓글이 달렸을 때  알림을 주는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _type : 1:lesson, 2:club, 3:trans, 4:qa
        @param param3: _refid : 게시글 id
        @param param3: cid : 댓글 id
        @return: none
        """

        print 'set_notification_trans_comment'
        # print _user_id, _type, _refid, _comment
        # print _comment[0]['id']
        reply_id = cid
        print _type
        # post comment
        if _type == "Q":
            print 'POST comment'
            lc = TP.objects.get(id=_refid)

            from_user = _user_id
            to_user = lc.user_id
            type = 3
            # comment
            type_msg = 7
            ntid = reply_id
            msg_type = 2
            msg_id = reply_id
            redirect_url = "/trans/" + str(_refid)
            state = 0

        # answer comment
        elif _type == "A":
            print 'answer comment'
            ta = TPAnswer.objects.get(id=_refid)

            from_user = _user_id
            to_user = ta.user_id
            type = 3
            type_msg = 7
            ntid = reply_id
            msg_id = reply_id
            msg_type = 2
            redirect_url = "/trans/" + str(ta.id)
            state = 0

        # comment comment
        else:
            # post comment comment
            print _refid, cid, TPComment.objects.get(id=_refid).tp_ref_id
            tp = TPComment.objects.get(id=_refid)
            from_user = _user_id
            type = 3
            type_msg = 8
            ntid = reply_id
            msg_id = reply_id
            msg_type = 3
            state = 0
            to_user = tp.user_id
            if tp.type == 'Q':
                print 'post comment comment'

                redirect_url = "/trans/" + str(tp.tp_ref_id)
            else:
                print 'answer comment comment'
                ta = TPAnswer.objects.get(id=TPComment.objects.get(id=_refid).tp_ref_id)

                redirect_url = "/trans/" + str(ta.id)

        if SettingsNotification.objects.get(user_id=to_user).t_tp_comment == 1:
            Notification(
                sender_id=from_user,
                receiver_id=to_user,
                type=type,
                noti_text=type_msg,
                community_type=msg_type,
                ref_id=msg_id,
                redirect_url=redirect_url,
                is_read=state
            ).save()

        if SettingsNotification.objects.get(user_id=to_user).m_tp_comment == 1:
            print 'send_mail trans comment'
            _content = TPComment.objects.get(id=ntid).contents
            from_user_name = Tutee.objects.get(user_id=from_user).name
            to_user_email = Tutee.objects.select_related('user').get(user_id=to_user).user.email
            send_noti_email(to_user_email, "New comment on your writing",
                            "\'" + from_user_name + "\' says, \"" + _content + "\"")

    @staticmethod
    def set_notification_trans_vote(user_id, _type, refid, state, refaid):
        """
        @summary: 내가 작성한 trans&proof포스트에 투표(좋아요 or 싫어요)가 되었을 때  알림을 주는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _type : 1:lesson, 2:club, 3:trans, 4:qa
        @param param3: _refid : 게시글 id
        @param param3: state : 좋아요 or 싫어요 상태
        @return: none
        """

        print 'set_notification_trans_vote'
        print user_id, _type, refid, state, refaid

        try:
            if state == 1:
                if _type == 'Q':
                    redirect_url = '/trans/' + str(refid)
                    to_user_id = TP.objects.get(id=refid).user_id
                elif _type == 'A':
                    ta = TPAnswer.objects.get(id=refaid)
                    redirect_url = '/trans/' + str(ta.tp_id)
                    to_user_id = ta.user_id

                if SettingsNotification.objects.get(user_id=to_user_id).t_tp_upvote == 1:
                    Notification(
                        sender_id=user_id,
                        receiver_id=to_user_id,
                        type=3,
                        noti_text=11,
                        community_type=4,
                        ref_id=refid,
                        redirect_url=redirect_url,
                        is_read=0
                    ).save()
        except Exception as e:
            print e

    @staticmethod
    def set_notification_trans_best(user_id, id, aid):
        """
        @summary: 내가 작성한 trans&proof 답변이 베스트 답변으로 채택 되었을 때  알림을 주는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: id : 게시글 id
        @param param3: aid : 답변  id
        @return: none
        """

        print 'set_notification_trans_best_answer'
        print user_id, id, aid

        try:
            ta = TPAnswer.objects.get(id=aid)
            tp = TP.objects.get(id=id)

            # if SettingsNotification.objects.get(user_id=ta.user_id).t_t_request_answer== 1:
            # question 작성자에게 알림
            noti = Notification()
            noti.sender_id = user_id
            noti.receiver_id = ta.user_id
            # 1:lesson, 2:club, 3:trans, 4:qa
            noti.type = 3
            # post, comment 등에 따른 history id
            noti.noti_text = 13
            # 1:post, 2:comment, 3:reply to comment
            noti.community_type = 5
            # post or comment id
            noti.ref_id = aid
            noti.redirect_url = "/trans/" + str(id) + "/"
            noti.is_read = 0
            noti.save()

            # if SettingsNotification.objects.get(user_id=ta.user_id).e_t_request_answer == 1:
            print 'send_mail trans answer'
            _content = TPAnswer.objects.get(id=aid).contents
            from_user_name = Tutee.objects.get(user_id=user_id).name
            to_user_email = Tutee.objects.select_related('user').get(user_id=ta.user_id).user.email
            send_noti_email(to_user_email, "Your answer is chosen as the best answer.",
                            "As your answer is chosen as the best answer, you received " + str(
                                tp.best_answer_credit) + " TC from " + from_user_name + ".<br><a href=\'" + TELLPIN_HOME + 'trans/' + str(
                                id) + '/' + "\'>View Detail</a>")
        except Exception as e:
            print e

    @staticmethod
    def set_notification_qna_post(_user_id, id):
        """
        @summary: 내가 친구로 등록한 유저가  language question에 post 햇을 때 알림을 주는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: id : 게시글 id
        @return: none
        """

        try:
            print 'set_notification_qa_post()'
            # TODO all freinds, if tutor-> all tutee
            # to_user =
            friend_list = []
            tutor_list = []
            to_user_list = []
            friends = Friend.objects.filter((Q(req_user=_user_id) | Q(res_user=_user_id)) & Q(status=2))

            print friends
            print len(friends)
            for f in friends:
                print 'f: '
                print f.id
                if f.req_user_id == _user_id:
                    friend_list.append(f.res_user_id)
                else:
                    friend_list.append(f.req_user_id)

            tutor_list = Reservation.objects.filter(tutee_id=_user_id).values("tutor_id").distinct()

            for tu in tutor_list:
                friend_list.append(tu.get('tutor_id'))

            to_user_list = list(set(friend_list))

            print 'friend_list: '
            print friend_list
            print to_user_list
            for to_user in to_user_list:
                if SettingsNotification.objects.get(user_id=to_user).t_lq_new == 1:
                    noti = Notification()
                    noti.sender_id = _user_id
                    noti.receiver_id = to_user
                    # 1:lesson, 2:club, 3:trans, 4:qa
                    noti.type = 4
                    # post, comment 등에 따른 history id

                    noti.noti_text = 14
                    # 1:post, 2:comment, 3:reply to comment
                    noti.community_type = 1
                    # post or comment id
                    noti.ref_id = id
                    noti.redirect_url = "/questions/" + str(id) + "/"
                    noti.is_read = 0
                    noti.save()

                if SettingsNotification.objects.get(user_id=to_user).m_lq_new == 1:
                    print 'send_mail qa post'
                    _title = LQ.objects.get(id=id).title
                    from_user_name = Tutee.objects.get(user_id=_user_id).name
                    to_user_email = Tutee.objects.select_related('user').get(user_id=to_user).user.email
                    send_noti_email(to_user_email, "\'" + from_user_name + "\' asked a Question : " + _title,
                                    "\'" + from_user_name + "\' asked, \"" + _title + "\"")
        except Exception as e:
            print 'set_notification_qna() error'
            print e

    @staticmethod
    def set_notification_qna_answer(_user_id, aid):
        """
        @summary: 내가 등록한 language question post에 답글이 달렸을 때 알림을 주는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: aid : 답글 id
        @return: none
        """

        print 'set_notification_qna_answer'
        print _user_id, aid

        ta = LQAnswer.objects.get(id=aid)
        tp = LQ.objects.get(id=ta.lq_id)

        if SettingsNotification.objects.get(user_id=tp.user_id).t_lq_answer == 1:
            # question 작성자에게 알림
            noti = Notification()
            noti.sender_id = _user_id
            noti.receiver_id = tp.user_id
            # 1:lesson, 2:club, 3:trans, 4:qa
            noti.type = 4
            # post, comment 등에 따른 history id
            noti.noti_text = 17
            # 1:post, 2:comment, 3:reply to comment, 4:upvote, 5:answer
            noti.community_type = 5
            # post or comment id
            noti.ref_id = aid
            noti.redirect_url = "/questions/" + str(tp.id) + "/"
            noti.is_read = 0
            noti.save()

        if SettingsNotification.objects.get(user_id=tp.user_id).m_lq_answer == 1:
            print 'send_mail qns answer'
            _title = tp.title
            _content = LQAnswer.objects.get(id=aid).contents
            from_user_name = Tutee.objects.get(user_id=_user_id).name
            to_user_email = Tutee.objects.select_related('user').get(user_id=tp.user_id).user.email
            send_noti_email(to_user_email, "New answer for your question : " + _title,
                            "\'" + from_user_name + "\' answers, \"" + _content + "\"")

        try:
            # pin 한사람들에게 알림
            to_user_list = LQPin.objects.filter(lq_id=tp.id)
        except Exception as e:
            print e
            return
        
        for to_user in to_user_list:
            print 'to_user:'
            print to_user
            print to_user.user_id
            if SettingsNotification.objects.get(user_id=to_user.user_id).t_lq_pin == 1:
                noti = Notification()
                noti.sender_id = _user_id
                noti.receiver_id = to_user.user_id_id
                # 1:lesson, 2:club, 3:trans, 4:qa
                noti.type = 4
                # post, comment 등에 따른 history id
                noti.noti_text = 18
                # 1:post, 2:comment, 3:reply to comment
                noti.community_type = 5
                # post or comment id
                noti.ref_id = aid
                noti.redirect_url = "/questions/" + str(tp.id) + "/"
                noti.is_read = 0
                noti.save()

            if SettingsNotification.objects.get(user_id=to_user.user_id_id).m_lq_pin == 1:
                print 'send_mail qna answer pinned'
                _title = tp.title
                _content = LQAnswer.objects.get(id=aid).contents
                from_user_name = Tutee.objects.get(user_id=_user_id).name
                to_user_email = Tutee.objects.select_related('user').get(user_id=to_user.user_id_id).user.email
                send_noti_email(to_user_email, "New answer for the question you pinned : " + _title,
                                "\'" + from_user_name + "\' answers, \"" + _content + "\"")

    @staticmethod
    def set_notification_qna_comment(_user_id, _type, _refid, cid):
        """
        @summary: 내가 등록한 language question post(질문 or 답변)에 댓글이 달렸을 때 알림을 주는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _type : Q: 질문, A: 답변
        @param param3: _refid : 게시글 id
        @param param4: cid : 댓글 id
        @return: none
        """

        print 'set_notification_qna_comment'
        # print _user_id, _type, _refid, _comment
        # print _comment[0]['lq_id']
        reply_id = cid

        # post comment
        if _type == "Q":
            print 'post comment'
            lc = LQ.objects.get(id=_refid)

            from_user = _user_id
            to_user = lc.user_id
            type = 4
            # comment
            type_msg = 15
            ntid = reply_id
            msg_type = 2
            msg_id = reply_id
            redirect_url = "/questions/" + str(_refid)
            state = 0

        elif _type == 'A':
            print 'answer comment'
            la = LQAnswer.objects.get(id=_refid)

            from_user = _user_id
            to_user = la.user_id
            type = 4
            type_msg = 20
            ntid = reply_id
            msg_id = reply_id
            msg_type = 2
            redirect_url = "/questions/" + str(la.lq_id)
            state = 0

        else:
            # post comment comment
            # print _refid, cid, TPComment.objects.get(id = _refid).ref_id
            lc = LQComment.objects.get(id=_refid)
            from_user = _user_id
            type = 4
            type_msg = 16
            ntid = reply_id
            msg_id = reply_id
            msg_type = 3
            state = 0
            to_user = lc.user_id
            if lc.type == 'Q':
                print 'post comment comment'

                redirect_url = "/questions/" + str(lc.lq_id)
            else:
                print 'answer comment comment'
                la = LQAnswer.objects.get(id=LQComment.objects.get(id=_refid).ref_id)

                redirect_url = "/questions/" + str(la.lq_id)

        if SettingsNotification.objects.get(user_id=to_user).t_lq_comment == 1:
            Notification(
                sender_id=from_user,
                receiver_id=to_user,
                type=type,
                noti_text=type_msg,
                community_type=msg_type,
                ref_id=msg_id,
                redirect_url=redirect_url,
                is_read=state
            ).save()

        if SettingsNotification.objects.get(user_id=to_user).m_lq_comment == 1:
            print 'send_mail qna comment'
            _content = LQComment.objects.get(id=ntid).contents
            from_user_name = Tutee.objects.get(user_id=from_user).name
            to_user_email = Tutee.objects.select_related('user').get(user_id=to_user).user.email
            send_noti_email(to_user_email, "New comment on your writing",
                            "\'" + from_user_name + "\' says, \"" + _content + "\"")

    @staticmethod
    def set_notification_qna_vote(user_id, _type, refid, state):
        """
        @summary: 내가 등록한 language question post에 투표가 되었을 때 알림을 주는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: _type : Q: 질문, A: 답변
        @param param3: _refid : 게시글 id
        @param param4: state : 좋아요  or 싫어요
        @return: none
        """

        print 'set_notification_qna_vote'
        print user_id, _type, refid, state

        try:
            if state == 1:
                if _type == 'Q':
                    redirect_url = '/questions/' + str(refid)
                    to_user_id = LQ.objects.get(id=refid).user_id
                elif _type == 'A':
                    ta = LQAnswer.objects.get(id=refid)
                    redirect_url = '/questions/' + str(ta.lq_id)
                    to_user_id = ta.user_id
                if SettingsNotification.objects.get(user_id=to_user_id).t_lq_comment == 1:
                    Notification(
                        sender_id=user_id,
                        receiver_id=to_user_id,
                        type=4,
                        noti_text=19,
                        community_type=4,
                        ref_id=refid,
                        redirect_url=redirect_url,
                        is_read=0
                    ).save()
        except Exception as e:
            print e

    @staticmethod
    def set_notification_qna_best(user_id, id, aid):
        """
        @summary: 내가 등록한 language question 답변이 베스트 답변으로 채택 되었을 때 알림을 주는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param2: id : 게시글 id
        @param param3: aid : 답변 id
        @return: none
        """

        print 'set_notification_qns_best_answer'
        print user_id, id, aid

        try:
            ta = LQAnswer.objects.get(id=aid)
            tp = LQ.objects.get(id=id)

            # if SettingsNotification.objects.get(user_id=ta.user_id).t_t_request_answer== 1:
            # question 작성자에게 알림
            noti = Notification()
            noti.sender_id = user_id
            noti.receiver_id = ta.user_id
            # 1:lesson, 2:club, 3:trans, 4:qa
            noti.type = 4
            # post, comment 등에 따른 history id
            noti.noti_text = 21
            # 1:post, 2:comment, 3:reply to comment
            noti.community_type = 5
            # post or comment id
            noti.ref_id = aid
            noti.redirect_url = "/questions/" + str(id) + "/"
            noti.is_read = 0
            noti.save()

            # if SettingsNotification.objects.get(user_id=ta.user_id).e_t_request_answer == 1:
            print 'send_mail trans answer'
            _content = LQAnswer.objects.get(id=aid).contents
            from_user_name = Tutee.objects.get(user_id=user_id).name
            to_user_email = Tutee.objects.select_related('user').get(user_id=ta.user_id).user.email
            send_noti_email(to_user_email, "Your answer is chosen as the best answer.",
                            "As your answer is chosen as the best answer, you received " + str(
                                tp.best_answer_credit) + " TC from " + from_user_name + ".<br><a href=\'" + TELLPIN_HOME + 'questions/' 
                                + str(id) + '/' + "\'>View Detail</a>")
        except Exception as e:
            print e

    @staticmethod
    def set_notification_exchange_add_friend(from_user_id, to_user_id, friend):
        """
        @summary: 나를 친구로 등록한 사용자가 있을 경우 알림을 주는 함수
        @author: sj46
        @param param1: from_user_id : 나를 친구로 등록한 사용자
        @param param2: to_user_id : 친구로 등록된 사용자
        @param param3: friend : 친구 요청 id
        @return: none
        """

        print 'set_notification_exchange_add_friend'

        try:
            print 'try'
            from_user = Tutee.objects.get(user_id=from_user_id)
            # set notification
            noti = Notification()
            noti.sender_id = from_user_id
            noti.receiver_id = to_user_id
            # 1:lesson, 2:club, 3:trans, 4:qa, 5:exchange
            noti.type = 5
            # post, comment 등에 따른 history id
            noti.noti_text = 22
            # 1:post, 2:comment, 3:reply to comment
            noti.community_type = 1
            # post or comment id
            noti.ref_id = friend.id
            noti.redirect_url = "/people/friend/"
            noti.is_read = 0
            noti.save()

            # send email
            _content = Friend.objects.get(id=friend.id).message
            from_user_name = Tutee.objects.get(user_id=from_user_id).name
            to_user_email = Tutee.objects.select_related('user').get(user_id=to_user_id).user.email
            send_noti_email(to_user_email, "" + from_user_name + " wants to be your language exchange partner.",
                            from_user_name + " wants to be your language exchange partner. <a href=\'http://www.tellpin.com/people/friend/\' style=\'display:block;width:100px;height:30px;\'>Accept or Ignore</a>",
                            from_user)
        except Exception as e:
            print e

    @staticmethod
    def set_notification_exchange_approve_friend(res_id, req_id, type):
        """
        @summary: 친구 등록을 한 경우 상대방에서 친구 등록 수락을 한 경우 알림을 주는 함수
        @author: sj46
        @param param1: id : 친구 요청 id
        @param param2: type : all:전체 수락
        @return: none
        """

        print 'set_notification_exchange_approve_friend'

        try:
            print 'try'
            if type != 'all':
                print '3'
                fr = Friend.objects.get(res_user=res_id, req_user=req_id)
                from_user_id = fr.res_user_id
                from_user = Tutee.objects.get(user_id=from_user_id)
                to_user_id = fr.req_user_id
                noti = Notification()
                noti.sender_id = from_user_id
                noti.receiver_id = to_user_id
                # 1:lesson, 2:club, 3:trans, 4:qa, 5:exchange
                noti.type = 5
                # post, comment 등에 따른 history id
                noti.noti_text = 23
                # 1:post, 2:comment, 3:reply to comment
                noti.community_type = 2
                # post or comment id
                noti.ref_id = fr.id
                noti.redirect_url = "/people/friend/"
                noti.is_read = 0
                noti.save()

                print '11'
                # send email
                _content = Friend.objects.get(id=fr.id).message
                from_user_name = Tutee.objects.get(user_id=from_user_id).name
                to_user_email = Tutee.objects.select_related('user').get(user_id=to_user_id).user.email
                print '22'
                print to_user_email, from_user_name, from_user_name
                send_noti_email(to_user_email, "" + from_user_name + " added you as a friend.",
                                from_user_name + " added you as a friend. <a href=\'http://www.tellpin.com/people/friend/\' style=\'display:block;width:100px;height:30px;\'>View more</span> ",
                                from_user)
                print '44'
            else:
                print 'else'
                fr = Friend.objects.filter(res_user=res_id)

                for f in fr:
                    print f
                    from_user_id = f.res_user_id
                    from_user = Tutee.objects.get(user_id=from_user_id)
                    to_user_id = f.req_user_id
                    noti = Notification()
                    noti.sender_id = from_user_id
                    noti.receiver_id = to_user_id
                    # 1:lesson, 2:club, 3:trans, 4:qa, 5:exchange
                    noti.type = 5
                    # post, comment 등에 따른 history id
                    noti.noti_text = 23
                    # 1:post, 2:comment, 3:reply to comment
                    noti.community_type = 2
                    # post or comment id
                    noti.ref_id = f.fr_id
                    noti.redirect_url = "/people/friend/"
                    noti.is_read = 0
                    noti.save()

                    # send email
                    _content = Friend.objects.get(fr_id=f.fr_id).message
                    from_user_name = Tutee.objects.get(user_id=from_user_id).name
                    to_user_email = Tutee.objects.select_related('user').get(user_id=to_user_id).user.email
                    send_noti_email(to_user_email, "" + from_user_name + " added you as a friend.",
                                    from_user_name + " added you as a friend. ", from_user)

        except Exception as e:
            print e

    @staticmethod
    def set_notification_message(from_user_id, to_user_id, msg, _type=1):
        """
        @summary: 친구 등록을 한 경우 상대방에서 친구 등록 수락을 한 경우 알림을 주는 함수
        @author: sj46
        @param param1: from_user_id : 메세지를 보낸 사용자
        @param param2: to_user_id : 메세지 받는 사용자
        @param param3: msg : 메세지
        @param param4: _type : 1:
        @return: none
        """

        print 'set_notification_message'

        try:
            if _type == 1:

                noti = Notification()
                noti.sender_id = from_user_id
                noti.receiver_id = to_user_id
                # 1:lesson, 2:club, 3:trans, 4:qa, 5:exchange
                noti.type = 6
                # post, comment 등에 따른 history id
                noti.noti_text = 1
                # 1:post, 2:comment, 3:reply to comment
                noti.community_type = 1
                # post or comment id
                noti.ref_id = msg.mid
                noti.redirect_url = ""
                noti.is_read = 0
                noti.save()

                # send email
                _content = Message.objects.get(mid=msg.mid).message
                from_user_name = Tutee.objects.get(user_id=from_user_id).name
                to_user_email = Tutee.objects.select_related('user').get(user_id=to_user_id).email
                send_noti_email(to_user_email, "New Message from " + from_user_name,
                                "<a href='" + TELLPIN_HOME + "/user/" + str(
                                    from_user_id) + "'> " + from_user_name + "</a> sent you a message :<br><p style='font-size:12px !important;'>" + msg.message + "</p><br><br><p><a href='" + TELLPIN_HOME + "'>Open chat window</a></p>",
                                None, 1)
            else:
                noti = Notification()
                noti.sender_id = from_user_id
                noti.receiver_id = to_user_id
                # 1:lesson, 2:club, 3:trans, 4:qa, 5:exchange
                noti.type = 6
                # post, comment 등에 따른 history id
                noti.noti_text = 2
                # 1:post, 2:comment, 3:reply to comment
                noti.community_type = 2
                # post or comment id
                noti.ref_id = msg.mid
                noti.redirect_url = ""
                noti.is_read = 0
                noti.save()

                # send email
                _content = Message.objects.get(mid=msg.mid).message
                from_user_name = Tutee.objects.get(user_id=from_user_id).name
                to_user_email = Tutee.objects.select_related('user').get(user_id=to_user_id).user.email
                send_noti_email(to_user_email, "New Lesson Message from Tutor " + from_user_name,
                                "<a href='" + TELLPIN_HOME + "/user/" + str(
                                    from_user_id) + "'> " + from_user_name + "</a> sent you a message :<br><p style='font-size:12px !important;'>" + msg.message + "</p>")

        except Exception as e:
            print e















