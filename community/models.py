# -*- coding: utf-8 -*-
###############################################################################
# filename    : community > models.py
# description : Community 모델 정의
# author      : hsrjmk@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160905 hsrjmk 최조 작성
#
###############################################################################
from django.db import models
from common.models import LanguageInfo
from common.utils import get_expiration_datetime
from user_auth.models import File, TellpinUser


TP_TYPE = (
    ('T', 'Translation'),
    ('P', 'Proofreading'),
)
COMMENT_TYPE = (
    ('P', 'Post'),
    ('Q', 'Question'),
    ('A', 'Answer'),
    # ('C', 'Comment'),
)
VOTE_TYPE = (
    ('Q', 'Question'),
    ('A', 'Answer'),
)
VIOLATION_TYPE = (
    ('Q', 'Question'),
    ('A', 'Answer'),
)

def get_default_expired_datetime():
    return get_expiration_datetime(days=7)


class LB(models.Model):
    user = models.ForeignKey(TellpinUser)

    learning_lang = models.ForeignKey(LanguageInfo, related_name='+')
    learning_lang_name = models.CharField("For learning language", max_length=100, null=True)
    explained_lang = models.ForeignKey(LanguageInfo, related_name='+')
    explained_lang_name = models.CharField("Explained in language", max_length=100, null=True)
    title = models.CharField("Title", max_length=150)
    contents = models.CharField("Contents", max_length=5000)
    main_img_fid = models.ForeignKey(File, db_column='main_img_fid', null=True)
    fid = models.CharField("File ids", max_length=500, null=True)
    view_count = models.IntegerField("View counts")

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'lb'


class LBComment(models.Model):
    lb = models.ForeignKey('LB')

    user = models.ForeignKey(TellpinUser)
    type = models.CharField("Comment type", max_length=1, choices=COMMENT_TYPE)
    contents = models.CharField("Contents", max_length=2000)
    lb_ref_id = models.IntegerField("Reference id: post id", null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'lb_comment'


class LBPin(models.Model):
    lb = models.ForeignKey('LB')

    user = models.ForeignKey(TellpinUser)

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'lb_pin'


class LBVote(models.Model):
    lb = models.ForeignKey('LB')

    user = models.ForeignKey(TellpinUser)
    like = models.BooleanField("User like this language board contents", default=False)
    dislike = models.BooleanField("User dislike this language board contents", default=False)

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'lb_vote'


class LBViolation(models.Model):
    lb = models.ForeignKey('LB')

    user = models.ForeignKey(TellpinUser)
    contents = models.TextField("Contents")

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'lb_violation'


class TP(models.Model):
    user = models.ForeignKey(TellpinUser)

    type = models.CharField("Contents type", max_length=1, choices=TP_TYPE)
    written_lang = models.ForeignKey(LanguageInfo, related_name='+')
    written_lang_name = models.CharField("Written language name", max_length=100, null=True)
    translated_lang = models.ForeignKey(LanguageInfo, related_name='+', null=True)
    translated_lang_name = models.CharField("Translated language name", max_length=100, null=True)
    title = models.CharField("Title", max_length=150)
    contents = models.TextField("TP contents")
    fid = models.CharField("File ids", max_length=500, null=True)
    best_answer_id = models.IntegerField("Best answer id", null=True)
    best_answer_credit = models.IntegerField("Credit for best answer", default=0, null=True)
    view_count = models.IntegerField("View count", default=0)
    expired_time = models.DateTimeField("Expired time", default=get_default_expired_datetime, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'tp'


class TPAnswer(models.Model):
    tp = models.ForeignKey('TP')

    user = models.ForeignKey(TellpinUser)
    contents = models.CharField("TP answer contents", max_length=2000)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'tp_answer'


class TPComment(models.Model):
    tp = models.ForeignKey('TP')

    user = models.ForeignKey(TellpinUser)
    type = models.CharField(max_length=1, choices=COMMENT_TYPE)
    contents = models.CharField("TP comment contents", max_length=2000)
    tp_ref_id = models.IntegerField("Reference id: question id or answer id", null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'tp_comment'


class TPPin(models.Model):
    tp = models.ForeignKey('TP')

    user = models.ForeignKey(TellpinUser)

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'tp_pin'


class TPVote(models.Model):
    tp = models.ForeignKey('TP')

    user = models.ForeignKey(TellpinUser)
    type = models.CharField("Vote type", max_length=1, choices=VOTE_TYPE)
    tp_ref_id = models.IntegerField("Reference id: question id or answer id")
    like = models.BooleanField("User like this translate and proofreading contents", default=False)
    dislike = models.BooleanField("User dislike this translate and proofreading contents", default=False)

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'tp_vote'


class TPViolation(models.Model):
    tp = models.ForeignKey('TP')

    user = models.ForeignKey(TellpinUser)
    type = models.CharField("Violation type", max_length=1, choices=VIOLATION_TYPE)
    tp_ref_id = models.IntegerField("Reference id: question id or answer id")
    contents = models.TextField("TP violation contents")

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'tp_violation'


class LQ(models.Model):
    user = models.ForeignKey(TellpinUser)

    written_lang = models.ForeignKey(LanguageInfo, related_name='+')
    written_lang_name = models.CharField("Written language name", max_length=100, null=True)
    question_lang = models.ForeignKey(LanguageInfo, related_name='+')
    question_lang_name = models.CharField("Question language name", max_length=100, null=True)
    title = models.CharField("Title", max_length=150)
    contents = models.TextField("LQ contents")
    fid = models.CharField("File ids", max_length=500, null=True)
    best_answer_id = models.IntegerField("Best answer id", null=True)
    best_answer_credit = models.IntegerField("Credit for best answer", default=0, null=True)
    view_count = models.IntegerField("View count", default=0)
    expired_time = models.DateTimeField("Expired time", default=get_default_expired_datetime, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'lq'


class LQAnswer(models.Model):
    lq = models.ForeignKey('LQ')

    user = models.ForeignKey(TellpinUser)
    contents = models.CharField("LQ answer contents", max_length=2000)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'lq_answer'


class LQComment(models.Model):
    lq = models.ForeignKey('LQ')

    user = models.ForeignKey(TellpinUser)
    type = models.CharField(max_length=1, choices=COMMENT_TYPE)
    contents = models.CharField("LQ comment contents", max_length=2000)
    lq_ref_id = models.IntegerField("Reference id: question id or answer id", null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'lq_comment'


class LQPin(models.Model):
    lq = models.ForeignKey('LQ')

    user_id = models.ForeignKey(TellpinUser)

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'lq_pin'


class LQVote(models.Model):
    lq = models.ForeignKey('LQ')

    user = models.ForeignKey(TellpinUser)
    type = models.CharField("Vote type", max_length=1, choices=VOTE_TYPE)
    lq_ref_id = models.IntegerField("Reference id: question id or answer id")
    like = models.BooleanField("User dislike this language question contents", default=False)
    dislike = models.BooleanField("User dislike this language question contents", default=False)

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'lq_vote'


class LQViolation(models.Model):
    lq_id = models.ForeignKey('LQ')

    user_id = models.ForeignKey(TellpinUser)
    type = models.CharField("Violation type", max_length=1, choices=VIOLATION_TYPE)
    lq_ref_id = models.IntegerField("Reference id: question id or answer id")
    contents = models.TextField("LQ violation contents")

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'lq_violation'
