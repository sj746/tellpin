# -*- encoding: utf-8 -*-
'''
Created on 2015. 5. 11.

@author: kihyun, yun
'''

#페이스북

SOCIAL_AUTH_FACEBOOK_KEY = '500864693449223'#'899501793454403'
SOCIAL_AUTH_FACEBOOK_SECRET = 'f40c6e48eaa1411a3b48715799baee09'#'c13878eb0d06e8b6e6deebe10443728f'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email','user_friends']
#FACEBOOK_AUTH_SCOPE = ['email']
#SOCIAL_AUTH_FACEBOOK_EXTRA_DATA = [ ('email', 'email'), ('user_friends', 'friends'), ('friends_location', 'location') ]
SOCIAL_AUTH_FACEBOOK_EXTRA_DATA = [ ('email', 'email') ]
SOCIAL_AUTH_FACEBOOK_EXTENDED_PERMISSIONS = [ 'email']
#FACEBOOK_EXTENDED_PERMISSIONS = ['email']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {'fields': 'id,name,email,picture' }#{'locale': 'ru_RU'}



#로컬 페이스북
"""
SOCIAL_AUTH_FACEBOOK_KEY = '1632914706971248'
SOCIAL_AUTH_FACEBOOK_SECRET = 'f9309e8fa4497686e637a27eca19575b'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email','user_friends']
#FACEBOOK_AUTH_SCOPE = ['email']
#SOCIAL_AUTH_FACEBOOK_EXTRA_DATA = [ ('email', 'email'), ('user_friends', 'friends') ]
SOCIAL_AUTH_FACEBOOK_EXTRA_DATA = [ ('email', 'email'), ('user_friends', 'friends')]
SOCIAL_AUTH_FACEBOOK_EXTENDED_PERMISSIONS = [ 'email']
#FACEBOOK_EXTENDED_PERMISSIONS = ['email']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {'fields': 'id,name,email,picture' }#{'locale': 'ru_RU'}
"""

"""
#로컬 페이스북2
#개발자 아이디만 인증 및 사용 가능하므로 새로 발급 받아 사용할 것
#리다이렉트 URI 는 http://127.0.0.1:8000/complete/facebook
SOCIAL_AUTH_FACEBOOK_KEY = '1189157927836782'
SOCIAL_AUTH_FACEBOOK_SECRET = '1360b95e199f587cbad1fea302bae90b'
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email','user_friends']
#FACEBOOK_AUTH_SCOPE = ['email']
#SOCIAL_AUTH_FACEBOOK_EXTRA_DATA = [ ('email', 'email'), ('user_friends', 'friends') ]
SOCIAL_AUTH_FACEBOOK_EXTRA_DATA = [ ('email', 'email'), ('user_friends', 'friends')]
SOCIAL_AUTH_FACEBOOK_EXTENDED_PERMISSIONS = [ 'email']
#FACEBOOK_EXTENDED_PERMISSIONS = ['email']
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {'fields': 'id,name,email,picture' }#{'locale': 'ru_RU'}
"""

#[ ('email', 'email')]
#{'locale': 'ru_RU', }

#트위터
SOCIAL_AUTH_TWITTER_KEY = 'u4bLDhq6WCEZogJ36kCZJ69hu'#'xCHEzYsMxM19Y0sSm7gAlEmfc'
SOCIAL_AUTH_TWITTER_SECRET = 'Z1JQN3nzskANcTSuPXBIwd88I3nk2dvs2CYKzWuVDttu4U7td6'#'CAWy6YE1pKCfke79wbGCPOOOgef8wlkbIJ3T5h5P27nm77WgQc'

#구글

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '328722880630-28c423a6rbm7ktblrc16mh6q93vvegnk.apps.googleusercontent.com'#'27537771813-fu35d8ttddfp1r7hhvsva5f4fknnq5v0.apps.googleusercontent.com'

SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'sh0Wsp7ey0FzuBMzhF4A8kYY'#'vaMSjyOi2GVZi0Fa1vkEPjHm'


"""
#로컬 구글
#api 를 막아 놓았다면 새로 발급받아 사용할 것
#http://127.0.0.1:8000, (callback)http://127.0.0.1:8000/complete/google-oauth2
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '967835061393-gp9a21nseev41hicv4t0vrm8d5so37eo.apps.googleusercontent.com'

SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = '4GVriiOXVactlbCDpAZI8ASl'
"""

# Google OAuth2 (google-oauth2)
SOCIAL_AUTH_GOOGLE_OAUTH2_IGNORE_DEFAULT_SCOPE = True
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/userinfo.profile'
]

#구글플러스
SOCIAL_AUTH_GOOGLE_PLUS_KEY = '...'
SOCIAL_AUTH_GOOGLE_PLUS_SECRET = '...'
# Google+ SignIn (google-plus)
SOCIAL_AUTH_GOOGLE_PLUS_IGNORE_DEFAULT_SCOPE = True
SOCIAL_AUTH_GOOGLE_PLUS_SCOPE = [
    'https://www.googleapis.com/auth/plus.login',
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/userinfo.profile'
]

SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/user_auth/signup_form/' #'/home/'
SOCIAL_AUTH_LOGIN_URL = '/'
SOCIAL_AUTH_LOGIN_ERROR_URL = '/'

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
#    'social.pipeline.mail.mail_validation',
    'social.pipeline.social_auth.associate_by_email',
    'social.pipeline.user.get_username',
    'social.pipeline.user.create_user',
    'user_auth.tellpin_social.social_pipeline',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details',
)