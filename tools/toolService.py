#-*- coding: utf-8 -*-
# from common.models import Language, TellpinSearchSiteInfo,\
#     TellpinSearchLangCateInfo, TellpinSearchCategoryInfo, TellpinSearchMySite,\
#     TellpinSearchMyCategory
from django.forms.models import model_to_dict
from django.db.models.aggregates import Max
from operator import itemgetter

###############################################################################
# filename    : tools > toolService.py
# description : TellpinSearch 관련 toolService.py
# author      : msjang@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160615 msjang 최초 작성
# 
#
###############################################################################

def getCategoryInfo( user_id, lang_id = 2 ):
    """
    @summary: 해당 언어에 해당하는 카테고리 리스트
    @author: msjang
    @param user_id : 유저 id
    @param lang_id : language id
    @return: obj( object )
    """
    obj = {}
    try:
        # 사용자 삭제 카테고리 정보
        userDeleteCategoryList = TellpinSearchMyCategory.objects.filter( user_id = user_id,
                                                                         category_id__lt = 10001
                                                                ).values_list( 'category_id', flat = True )
        # 카테고리 정보
        categoryIDList = TellpinSearchLangCateInfo.objects.filter( lang_id__in = [ 0, lang_id ]
                                                        ).exclude( category_id__in = userDeleteCategoryList 
                                                        ).values_list( 'category_id', flat = True
                                                        ).distinct().order_by( 'category_id' )
        listCategoryID = list( categoryIDList )
        print userDeleteCategoryList
        if lang_id == 1:
            defaultCategoryList = TellpinSearchCategoryInfo.objects.filter( category_id__in = listCategoryID,
                                                                            is_dic = 0
                                                                    ).extra( select = { 'category_name' : 'category_korean' }
                                                                    ).values( 'category_id', 'category_name'
                                                                    ).order_by( 'category_name' )
            dictCategoryList = TellpinSearchCategoryInfo.objects.filter( category_id__in = listCategoryID,
                                                                         is_dic = 1
                                                                ).extra( select = { 'category_name' : 'category_korean' }
                                                                ).values( 'category_id', 'category_name'
                                                                ).order_by( 'category_name' )
        elif lang_id == 3:
            defaultCategoryList = TellpinSearchCategoryInfo.objects.filter( category_id__in = listCategoryID,
                                                                            is_dic = 0 ).extra( select = { 'category_name' : 'category_chinese' } ).values( 'category_id', 'category_name' ).order_by( 'category_name' )
            dictCategoryList = TellpinSearchCategoryInfo.objects.filter( category_id__in = listCategoryID,
                                                                         is_dic = 1
                                                                ).extra( select = { 'category_name' : 'category_chinese' }
                                                                ).values( 'category_id', 'category_name'
                                                                ).order_by( 'category_name' )
        elif lang_id == 4:
            defaultCategoryList = TellpinSearchCategoryInfo.objects.filter( category_id__in = listCategoryID,
                                                                            is_dic = 0 ).extra( select = { 'category_name' : 'category_spanish' } ).values( 'category_id', 'category_name' ).order_by( 'category_name' )
            dictCategoryList = TellpinSearchCategoryInfo.objects.filter( category_id__in = listCategoryID,
                                                                         is_dic = 1
                                                                ).extra( select = { 'category_name' : 'category_spanish' }
                                                                ).values( 'category_id', 'category_name'
                                                                ).order_by( 'category_name' )
        else:
            defaultCategoryList = TellpinSearchCategoryInfo.objects.filter( category_id__in = listCategoryID,
                                                                            is_dic = 0 ).extra( select = { 'category_name' : 'category_english' } ).values( 'category_id', 'category_name' ).order_by( 'category_name' )
            dictCategoryList = TellpinSearchCategoryInfo.objects.filter( category_id__in = listCategoryID,
                                                                         is_dic = 1
                                                                ).extra( select = { 'category_name' : 'category_english' }
                                                                ).values( 'category_id', 'category_name'
                                                                ).order_by( 'category_name' )
        # 유저가 생성한 카테고리 리스트
        userAddCategoryList = TellpinSearchMyCategory.objects.filter( user_id = user_id,
                                                                      lang_id__in = [ 0, lang_id ],
                                                                      type__in = [ 1, 3 ]
                                                            ).values( 'category_id', 'category_name' )
        arrDefault = list( dictCategoryList ) + list( defaultCategoryList ) + list( userAddCategoryList )
        sortList = sorted( arrDefault, key = itemgetter('category_name') )          
        obj['category'] = sortList
        #print 'arrdefault', arrDefault
        #print obj['category']
        if lang_id <= 10:
            categoryID = dictCategoryList[0]['category_id']
        else:
            categoryID = defaultCategoryList[0]['category_id']
        obj['site'] = getSiteInfo( user_id, categoryID, lang_id )
        #print obj['category']
    except Exception as e:
        obj['err'] = str( e )
        print str( e )
    finally:
        return obj
    
def getSiteInfo( user_id, category_id, lang_id ):
    """
    @summary: 해당 카테고리에 해당하는 사이트 리스트
    @author: msjang
    @param user_id : 유저 id
    @return: obj( object )
    """
    result = []
    try:
        # 사용자가 삭제한 default 사이트 리스트
        userDelSiteList = TellpinSearchMySite.objects.filter( type = 2, site_id__lt = 10001,
                                                              lang_id__in = [ 0, lang_id ],
                                                              user_id = user_id,
                                                              category_id = category_id
                                                            ).values_list( 'site_id' )
        # default 사이트 리스트
        defaultSiteList = TellpinSearchSiteInfo.objects.filter( category_id = category_id,
                                                                in_lang_id__in = [ 0, lang_id ]
                                                    ).exclude( site_id__in = userDelSiteList
                                                    ).extra( select = { 'lang_id' : 'in_lang_id' }
                                                    ).values( 'lang_id', 'category_id', 'site_id', 'site_name',
                                                              'search_url', 'site_url', 'is_dic', 'is_default',
                                                              'is_mobile' ).order_by( 'site_name' )
        userAddSiteList = TellpinSearchMySite.objects.filter( type__in = [ 1, 3 ],
                                                              site_id__gte = 10001,
                                                              lang_id__in = [ 0, lang_id ],
                                                              user_id = user_id,
                                                              category_id = category_id
                                                    ).values().order_by( 'site_name' )
        result = list( defaultSiteList ) + list( userAddSiteList )
    except Exception as e:
        print str( e )
    finally:
        return result
    
def addCategory( user_id, category, lang_id ):
    """
    @summary: 해당 카테고리에 해당하는 사이트 리스트
    @author: msjang
    @param user_id: 유저 id
    @param category: category 정보 담겨있는 객체 
    @return: 1( success ), 0( fail )
    """
    result = 1
    try:
        customCategory = TellpinSearchMyCategory()
        for i in category:
            setattr( customCategory, i, category[i] )
        maxCategory = TellpinSearchMyCategory.objects.all().aggregate( max = Max( 'category_id' ) )
        if maxCategory['max']:
            maxCategoryID = int( maxCategory['max'] )
            if maxCategoryID <= 10000:
                maxCategoryID = 10000 
        else:
            maxCategoryID = 10000 
        maxCategoryID += 1
        customCategory.user_id = user_id
        customCategory.category_id = maxCategoryID
        customCategory.lang_id = lang_id
        #print model_to_dict( customCategory )
        customCategory.save()
    except Exception as e:
        print 'addCategory ', str( e )
        result = 0
    finally:
        return result
    
def addSite( user_id, site, lang_id ):
    """
    @summary: 해당 카테고리에 해당하는 사이트 리스트
    @author: msjang
    @param user_id: 유저 id
    @param site: site 정보 담겨있는 객체 
    @return: 1( success ), 0( fail )
    """
    result = 1
    try:
        customSite = TellpinSearchMySite()
        for i in site:
            setattr( customSite, i, site[i] )
        maxSite = TellpinSearchMySite.objects.all().aggregate( max = Max( 'site_id' ) )
        if maxSite['max']:
            maxSiteID = int( maxSite['max'] )
        else:
            maxSiteID = 10000
        maxSiteID += 1
        customSite.user_id = user_id
        customSite.site_id = maxSiteID
        customSite.lang_id = lang_id
        customSite.save()
    except Exception as e:
        print 'addSite ', str( e )
        result = 0
    finally:
        return result
    
def deleteCategory( user_id, category, lang_id ):    
    """
    @summary: 해당 카테고리에 삭제 표시
    @author: msjang
    @param user_id: 유저 id
    @param category_id: tellpin_search_category_info pk 
    @return: 1( success ), 0( fail )
    """
    result = 1
    try:
        updated_values = {}
        updated_values['type'] = 2
        category, created = TellpinSearchMyCategory.objects.update_or_create( user_id = user_id,
                                                                     category_id = category['category_id'],
                                                                     lang_id = lang_id,
                                                                     category_name = category['category_name'],
                                                                     defaults = updated_values ) 
    except Exception as e:
        print 'deleteCategory ', str( e )
        result = 0
    finally:
        return result
    
def deleteSite( user_id, site, lang_id ):    
    """
    @summary: 해당하는 사이트에 삭제 표시
    @author: msjang
    @param user_id: 유저 id
    @param site: site 정보 담겨있는 객체 
    @return: 1( success ), 0( fail )
    """
    result = 1
    try:
        updated_values = {}
        updated_values['type'] = 2
        site, created = TellpinSearchMySite.objects.update_or_create( user_id = user_id,
                                                                      lang_id = lang_id,
                                                                      category_id = site['category_id'],
                                                                      site_id = site['site_id'],
                                                                      site_name = site['site_name'],
                                                                      search_url = site['search_url'],
                                                                      site_url = site['site_url'],
                                                                      defaults = updated_values )
    except Exception as e:
        print 'deleteSite ', str( e )
        result = 0
    finally:
        return result
    
def restoreCategoryList( user_id, lang_id ):
    """
    @summary: 삭제한 카테고리 리스트
    @author: msjang
    @param user_id: 유저 id
    @param lang_id: language table pk 
    @return: result( list )
    """
    result = []
    try:
        arrCategory = TellpinSearchMyCategory.objects.filter( user_id = user_id,
                                                              lang_id = lang_id,
                                                              type = 2
                                                    ).values().order_by( 'category_name' )
        result = list( arrCategory )
        #print 'restoreCategoryList', result
    except Exception as e:
        print 'restoreCategoryList', str( e )
        result = []
        result.append( str( e ) )
    finally:
        return result
    
def restoreCategory( user_id, category, lang_id ):
    """
    @summary: 삭제한 카테고리 복원
    @author: msjang
    @param user_id: 유저 id
    @param category: category_id 들어있는 list
    @param lang_id: language table 참조키
    @return: 1( success ), 0( fail )
    """
    result = 1
    try:
        TellpinSearchMyCategory.objects.filter( user_id = user_id,
                                                lang_id = lang_id,
                                                category_id__in = category
                                        ).update( type = 3 )
    except Exception as e:
        print 'restoreCategory', str( e )
        result = 0
    finally:
        return result
    
def restoreSiteList( user_id, lang_id, category_id ):
    """
    @summary: 삭제한 사이트 리스트
    @author: msjang
    @param user_id: 유저 id
    @param category: category_id 들어있는 list 
    @return: result( list )
    """
    result = []
    try:
        arrSite = TellpinSearchMySite.objects.filter( user_id = user_id,
                                                      lang_id = lang_id,
                                                      category_id = category_id,
                                                      type = 2
                                            ).values().order_by( 'site_name' )
        result = list( arrSite )
    except Exception as e:
        print 'restoreSiteList ', str( e )
        result = []
        result.append( str( e ) )
    finally:
        return result
    
def restoreSite( user_id, lang_id, category_id, site ):
    """
    @summary: 삭제한 사이트 복원
    @author: msjang
    @param user_id: 유저 id
    @param lang_id: language table 참조키
    @param category_id: category id
    @param site: 복원할 사이트 id list
    @return: 1( success ), 0( fail )
    """
    result = 1
    try:
        TellpinSearchMySite.objects.filter( user_id = user_id,
                                            lang_id = lang_id,
                                            category_id = category_id,
                                            site_id__in = site
                                    ).update( type = 3 )
    except Exception as e:
        print 'restoreSite ', str( e )
        result = 0
    finally:
        return result
    
def resetLanguage( user_id, lang_id ):    
    """
    @summary: 해당언어 카테고리정보, 사이트 정보 초기화
    @author: msjang
    @param user_id: 유저 id
    @param lang_id: language table 참조키
    @return: 1( success ), 0( fail )
    """
    result = 1
    try:
        TellpinSearchMySite.objects.filter( user_id = user_id,
                                            lang_id = lang_id
                                    ).delete()
        TellpinSearchMyCategory.objects.filter( user_id = user_id,
                                                lang_id = lang_id
                                        ).delete()
    except Exception as e:
        print 'resetLanguage ', str( e )
        result = 0
    finally:
        return result