# -*- coding: utf-8 -*-
from django.conf.urls import url, include

import views

urlpatterns = [
    url(r'^settings_old/$', views.set_page),
    url(r'^settings/$', views.set_page1),
    url(r'^settings/change/language/$', views.changeLanguage ),
    url(r'^settings/change/category/$', views.changeCategory ),
    url(r'^settings/new/category/$', views.addCategory ),
    url(r'^settings/new/site/$', views.addSite ),
    url(r'^settings/remove/category/$', views.deleteCategory ),
    url(r'^settings/remove/site/$', views.deleteSite ),
    url(r'^settings/restorelist/category/$', views.restoreCategoryList ),
    url(r'^settings/restore/category/$', views.restoreCategory ),
    url(r'^settings/restorelist/site/$', views.restoreSiteList ),
    url(r'^settings/restore/site/$', views.restoreSite ),
    url(r'^settings/reset/$', views.resetLanguage ),
#     url(r'^make/$', views.ajax_makeTxt),
    url(r'^remove/$', views.ajax_removeTxt),
    url(r'^getdata/$', views.get_data),
    url(r'^getcategory/$', views.get_category),
    url(r'^getsiteinfo/$', views.get_siteInfo),
    url(r'^getlanguageinfo/$', views.get_mylanguage),
    url(r'^setlanguageinfo/$', views.set_mylanguage),
    url(r'^getTsearchInfo/$', views.send_to_tellpinsearch),
    url(r'^setdata/$', views.set_data),
    url(r'^gethistory/$', views.get_history),
    url(r'^setmyhistory/$', views.set_myhistory),
    url(r'^setmypin/$', views.set_mypin),
    #url(r'^tellpinsearch_settings/$', views.tellpin_search_settings),
#     url(r'^tellpinplayer/$', views.tellpinplayer),

    url(r'^getAutoSearchPlayerData/$', views.get_auto_search_player_data),
    url(r'^getAutoSearchPlayerDic/$', views.get_auto_search_player_dic),
    url(r'^getAutoSearchPlayerSite/$', views.get_auto_search_player_site),
    url(r'^history/$', views.history_page),
    url(r'^get_pin_name/$', views.get_pin_name),
    url(r'^set_pin_name/$', views.set_pin_name),
    url(r'^test/$', views.test),
    url(r'^more/history/$', views.ajax_moreHistory),
    url(r'^add_word/$', views.add_word),


    url(r'^tellpinsearch_settings/$', views.tellpin_search_settings),
    url(r'^searchwordlog/$', views.tellpinsearchWord),
    url(r'^searchpinlog/$', views.tellpinsearchPinWord),
    url(r'^$', views.tellpinsearch),

    url(r'^login/$', views.login),
    url(r'^get_site_by_category/$', views.get_site_by_category),
    url(r'^get_default_site_by_category/$', views.get_default_site_by_category),

    url(r'^get_word_list/$', views.get_word_list),
    url(r'^get_category_site/$', views.get_category_site),
    url(r'^set_category_site/$', views.set_category_site),
    url(r'^get_last_language/$', views.get_last_language),
    url(r'^set_last_language/$', views.set_last_language),
    url(r'^get_last_category_site/$', views.get_last_category_site),
]