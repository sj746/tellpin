# -*- coding: utf-8 -*-

###############################################################################
# filename    : tools > views.py
# description : TellpinSearch 관련 views.py
# author      : msjang@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 msjang 최초 작성
# 
#
###############################################################################

from datetime import date
import datetime
import json
import logging
import os
import time

from _mysql import NULL
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models.query_utils import Q
from django.forms.models import model_to_dict
from django.http.response import HttpResponse, HttpResponseRedirect, \
    JsonResponse
from django.shortcuts import render, render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import login as auth_login

from common.common import tellpin_login_required
from tellpin.settings import BASE_DIR
from user_auth.forms import UserLoginForm
from common.models import LanguageInfo, SkillLevelInfo
import tutor
from tools.service import ToolsService
from tutor.service import TutorService
from user_auth.service import UserAuthService
from tools.models import TSearchMyWord, TSearchMyPin, TSearchMyLastCategorySite,\
    TSearchMyCategory, TSearchMySite
from user_auth.models import TellpinUser, TellpinAuthUser
from tutor.models import Tutee, Tutor

logger = logging.getLogger('mylogger')


# Create your views here.
@tellpin_login_required
def set_page(request):
    """
    @summary: set_page page
    @author: msjang
    @param request
    @return: url - tellpin_search/set_page.html
    """
    resData = {}
    resData["isSuccess"] = 1

    return render_to_response("tellpin_search/set_page_back.html", resData, RequestContext(request))


@tellpin_login_required
def set_page1(request):
    """
    @summary: set_page page
    @author: hsrjmk
    @param request
    @return: url - tellpin_search/set_page.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    isTutor = TutorService.is_tutor(user_id)
    resData['languageList'] = TutorService.get_language_info()
    resData['toolData'] = ToolsService.get_category_info(user_id)
    resData['userInfo'] = TutorService.get_user_info(user_id, isTutor)

    return render_to_response("tellpin_search/set_page.html", {'data': json.dumps(resData)}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def changeLanguage(request):
    """
    @summary: 언어 변경에 따른 카테고리, 사이트리스트
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    language_id = post['language_id']
    resData['toolData'] = ToolsService.get_category_info(user_id, language_id)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def changeCategory(request):
    """
    @summary: 카테고리 변경에 따른 사이트 목록
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    category_id = post['category_id']
    lang_id = post['lang_id']
    resData['site'] = ToolsService.get_site_info(user_id, lang_id, category_id)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def addCategory(request):
    """
    @summary: 사용자 커스텀 카테고리 추가
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    category = post['category']
    lang_id = post['lang_id']
    result = ToolsService.add_category(user_id, lang_id, category)

    if result == 1:
        resData['toolData'] = ToolsService.get_category_info(user_id, lang_id)
    else:
        resData['toolData'] = None

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def addSite(request):
    """
    @summary: 사용자 커스텀 사이트 추가
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    site = post['site']
    lang_id = post['lang_id']
    result = ToolsService.add_site(user_id, lang_id, site)

    if result == 1:
        resData['site'] = ToolsService.get_site_info(user_id, lang_id, site['category_id'])
    else:
        resData['site'] = None

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def deleteCategory(request):
    """
    @summary: 카테고리 삭제
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    category = post['category']
    lang_id = post['lang_id']
    result = ToolsService.delete_category(user_id, lang_id, category)

    if result == 1:
        resData['toolData'] = ToolsService.get_category_info(user_id, lang_id)
    else:
        resData['toolData'] = None

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def deleteSite(request):
    """
    @summary: 사이트 삭제
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    site = post['site']
    lang_id = post['lang_id']
    result = ToolsService.delete_site(user_id, lang_id, site)

    if result == 1:
        resData['site'] = ToolsService.get_site_info(user_id, lang_id, site['category_id'])
    else:
        resData['site'] = None

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def restoreCategoryList(request):
    """
    @summary: 삭제된 카테고리 목록 조회
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    lang_id = post['lang_id']
    resData['restoreCategory'] = ToolsService.get_restore_category_list(user_id, lang_id)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def restoreCategory(request):
    """
    @summary: 카테고리 복구
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    lang_id = post['lang_id']
    category = post['category']
    result = ToolsService.restore_category(user_id, lang_id, category)

    if result == 1:
        resData['toolData'] = ToolsService.get_category_info(user_id, lang_id)
    else:
        resData['toolData'] = None

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def restoreSiteList(request):
    """
    @summary: 삭제된 사이트 목록
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    lang_id = post['lang_id']
    category_id = post['category_id']
    resData['restoreSite'] = ToolsService.get_restore_site_list(user_id, lang_id, category_id)

    return JsonResponse(resData)


@csrf_exempt
def restoreSite(request):
    """
    @summary: 사이트 복구
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    lang_id = post['lang_id']
    category_id = post['category_id']
    site = post['site']
    result = ToolsService.restore_site(user_id, lang_id, category_id, site)

    if result == 1:
        resData['site'] = ToolsService.get_site_info(user_id, lang_id, category_id)
    else:
        resData['site'] = None

    return JsonResponse(resData)


@csrf_exempt
def resetLanguage(request):
    """
    @summary: 카테고리, 사이트 초기화
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    lang_id = post['lang_id']
    result = ToolsService.reset_category_site(user_id, lang_id)

    if result == 1:
        resData['toolData'] = ToolsService.get_category_info(user_id, lang_id)
    else:
        resData['toolData'] = None

    return JsonResponse(resData)


@tellpin_login_required
def history_page(request):
    """
    @summary: tellpinsearch history page
    @author: hsrjmk
    @param request
    @return: url - tellpin_search/pin_list.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    try:
        # log 정보 얻기
        resData['historylist'] = ToolsService.get_my_word_list(user_id, page_count=3)
        # my_pin1 ~ my_pin3 정보 얻기
        resData['mypin1'] = ToolsService.get_my_pin_list(user_id, 1, page_count=3)
        resData['mypin2'] = ToolsService.get_my_pin_list(user_id, 2, page_count=3)
        resData['mypin3'] = ToolsService.get_my_pin_list(user_id, 3, page_count=3)
        # my_pin 정보 얻기.
        # TODO: UI에서 사용하지 않는 듯함.
#         resData['mypin'] = ToolsService.get_mypin(user_id)
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print "@@@@@history_page : ", str(err)

    return render_to_response("tellpin_search/pin_list.html", {'data': json.dumps(resData, cls=DjangoJSONEncoder)}, RequestContext(request))


###############################################################################
# function for tools 
# respose : json 
###############################################################################
@csrf_exempt
def login(request):
    """
    @summary: 로그인 요청
    @author: hsrjmk
    @param request
    @return: json
    """
    if request.method == 'GET':
        post_dict = request.GET
    elif request.method == 'POST':
        post_dict = json.loads(request.body)

    resData = {}
    resData["isSuccess"] = True
    try:
        user = UserAuthService.check_user(post_dict['email'], post_dict['pw'])
        if user:
            resData['data'] = [{'email': user.email}, {'id': user.id}]
        else:
            resData["isSuccess"] = False
            resData['data'] = [{'email': post_dict['email']}, {'id': 0}]
    except Exception as e:
        resData["isSuccess"] = False
        resData['data'] = [{'email': post_dict['email']}, {'id': -1}]
        print "@@@@@login : ", str(e)

    resp = json.dumps(resData)
    return HttpResponse(resp, content_type='application/json')


@csrf_exempt
def get_word_list(request):
    """
    @summary: 검색한 단어 리스트 요청
    @author: hsrjmk
    @param request
    @return: json
    """
    if request.method == 'GET':
        post_dict = request.GET
    elif request.method == 'POST':
        post_dict = json.loads(request.body)

    try:
        resData = ToolsService.get_word_list(post_dict['user_id'], 5)
    except Exception as e:
        resData = {}
        resData["isSuccess"] = False
        print "@@@@@get_word_list : ", str(e)

#     resp = json.dumps(resData)
#     return HttpResponse(resp, content_type='application/json')

    resp = json.dumps(resData, cls=DjangoJSONEncoder, ensure_ascii=False)
    return HttpResponse(resp, content_type="application/json;")


@csrf_exempt
def add_word(request):
    """
    @summary: 검색한 단어 저장 요청
    @author: hsrjmk
    @param request
    @return: json
    """
    if request.method == 'GET':
        post_dict = request.GET
    elif request.method == 'POST':
        post_dict = json.loads(request.body)

    try:
        resData = ToolsService.add_word(post_dict['user_id'], post_dict['word_obj'])
    except Exception as e:
        resData = {}
        resData["isSuccess"] = False
        print "@@@@@add_word : ", str(e)

    resp = json.dumps(resData)
    return HttpResponse(resp, content_type='application/json')


@csrf_exempt
def get_last_language(request):
    """
    @summary: 마지막 사용 언어 조회 요청
    @author: hsrjmk
    @param request
    @return: json
    """
    if request.method == 'GET':
        post_dict = request.GET
    elif request.method == 'POST':
        post_dict = json.loads(request.body)

    try:
        resData = ToolsService.get_last_language(post_dict['user_id'])
    except Exception as e:
        resData = {}
        resData["isSuccess"] = False
        print "@@@@@get_last_language : ", str(e)

    resp = json.dumps(resData, cls=DjangoJSONEncoder)
    return HttpResponse(resp, content_type='application/json')


@csrf_exempt
def set_last_language(request):
    """
    @summary: 마지막 사용 언어 저장 요청
    @author: hsrjmk
    @param request
    @return: json
    """
    if request.method == 'GET':
        post_dict = request.GET
    elif request.method == 'POST':
        post_dict = json.loads(request.body)

    try:
        resData = ToolsService.set_last_language(post_dict['user_id'], post_dict['idx1'])
    except Exception as e:
        resData = {}
        resData["isSuccess"] = False
        print "@@@@@set_last_language : ", str(e)

    resp = json.dumps(resData, cls=DjangoJSONEncoder)
    return HttpResponse(resp, content_type='application/json')


@csrf_exempt
def get_category_site(request):
    """
    @summary: 언어 별 카테고리/사이트 조회 요청
    @author: hsrjmk
    @param request
    @return: json
    """
    if request.method == 'GET':
        post_dict = request.GET
    elif request.method == 'POST':
        post_dict = json.loads(request.body)

    try:
        resData = ToolsService.get_category_site(post_dict['user_id'], post_dict['idx1'])
    except Exception as e:
        resData = {}
        resData["isSuccess"] = False
        print "@@@@@get_category_site : ", str(e)

    resp = json.dumps(resData, cls=DjangoJSONEncoder)
    return HttpResponse(resp, content_type='application/json')


@csrf_exempt
def set_category_site(request):
    """
    @summary: 언어 별 카테고리/사이트 저장 요청
    @author: hsrjmk
    @param request
    @return: json
    """
    if request.method == 'GET':
        post_dict = request.GET
    elif request.method == 'POST':
        post_dict = json.loads(request.body)

    try:
        obj = {}
        obj['category_set1'] = post_dict['idx2']
        obj['category_set2'] = post_dict['idx3']
        obj['category_set3'] = post_dict['idx4']
        obj['site_set1'] = post_dict['idx5']
        obj['site_set2'] = post_dict['idx6']
        obj['site_set3'] = post_dict['idx7']
        if post_dict['wnd1'] == str(1):
            obj['use_wnd1'] = True
        else:
            obj['use_wnd1'] = False
        if post_dict['wnd2'] == str(1):
            obj['use_wnd2'] = True
        else:
            obj['use_wnd2'] = False
        if post_dict['wnd3'] == str(1):
            obj['use_wnd3'] = True
        else:
            obj['use_wnd3'] = False
        obj['pin_number'] = post_dict['pin']
        resData = ToolsService.set_category_site(post_dict['user_id'], post_dict['idx1'], obj)
    except Exception as e:
        resData = {}
        resData["isSuccess"] = False
        print "@@@@@set_category_site : ", str(e)

    resp = json.dumps(resData, cls=DjangoJSONEncoder)
    return HttpResponse(resp, content_type='application/json')


@csrf_exempt
def get_last_category_site(request):
    """
    @summary: 마지막으로 사용한 카테고리/사이트 조회 요청
    @author: hsrjmk
    @param request
    @return: json
    """
    if request.method == 'GET':
        post_dict = request.GET
    elif request.method == 'POST':
        post_dict = json.loads(request.body)

    try:
        resData = ToolsService.get_last_category_site(post_dict['user_id'], post_dict['idx1'])
    except Exception as e:
        resData = {}
        resData["isSuccess"] = False
        print "@@@@@get_last_category_site : ", str(e)

    resp = json.dumps(resData, cls=DjangoJSONEncoder)
    return HttpResponse(resp, content_type='application/json')


@csrf_exempt
def get_site_by_category(request):
    """
    @summary: 언어 별 카테고리/사이트 조회 요청
    @author: hsrjmk
    @param request
    @return: json
    """
    if request.method == 'GET':
        post_dict = request.GET
    elif request.method == 'POST':
        post_dict = json.loads(request.body)

    try:
        resData = ToolsService.get_site_by_category(post_dict['user_id'])
    except Exception as e:
        resData = {}
        resData["isSuccess"] = False
        print "@@@@@get_site_by_category : ", str(e)

#     return JsonResponse(resData)
#     resp = json.dumps(resData, cls=DjangoJSONEncoder)
#     return HttpResponse(resp, content_type='application/json')

    resp = json.dumps(resData, cls=DjangoJSONEncoder, ensure_ascii=False)
    return HttpResponse(resp, content_type="application/json;charset=utf-8")


@csrf_exempt
def get_default_site_by_category(request):
    """
    @summary: 언어 별 카테고리/사이트 조회 요청
    @author: hsrjmk
    @param request
    @return: json
    """
    if request.method == 'GET':
        post_dict = request.GET
    elif request.method == 'POST':
        post_dict = json.loads(request.body)

    try:
        resData = ToolsService.get_default_site_by_category(post_dict['user_id'])
    except Exception as e:
        resData = {}
        resData["isSuccess"] = False
        print "@@@@@get_default_site_by_category : ", str(e)

    resp = json.dumps(resData, cls=DjangoJSONEncoder)
    return HttpResponse(resp, content_type='application/json')


###############################################################################
# tset function 
#  
# url : tools/test/
# respose : json 
###############################################################################
def test(request):
    """
    @summary: test page
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 0
    obj = {"target_url": "test"}
    item = dict(obj)
    print item
    try:
        TSearchMyWord.objects.get(user_id=29, word="test34")
        TSearchMyWord.objects.filter(user_id=29, word="test34").update(target_url="chekc")
    except:
        TSearchMyWord.objects.create(user_id=29, word="test34")
    resData["isSuccess"] = 1
    resp = json.dumps(resData)

    return HttpResponse(resp, content_type='application/json')


###############################################################################
# tools 세팅에서 display를 선택했을 때 전체 언어리스트, level 정보,
# 자신의 언어 설정 정보를 읽어온다. 
# url : tools/getlanguageinfo/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def get_mylanguage(request):
    """
    @summary: user 언어정보
    @author: msjang
    @param none
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 0
    userinfo = TellpinUser.objects.get(email=request.session["email"])
    user = userinfo.id
    resData["user_id"] = user
    obj = {}
    res = model_to_dict(userinfo)
    obj["coin"] = res["coin"]
    obj["exp"] = res["exp"]
    obj["country"] = res["livein"]
    obj["state"] = res["livein_city"]
    obj["display_language_id"] = res["display_language"]
    obj["display_language_name"] = LanguageInfo.objects.get(id=res["display_language"]).lang_english
    resData['userinfo'] = obj

    # all language list
    try:
        allLanguage = LanguageInfo.objects.all()
        result = []

        for item in allLanguage:
            obj = {}
            res = model_to_dict(item)
            for field_name in item._meta.get_all_field_names():
                obj[field_name] = res[field_name]
            result.append(obj)

        resData["languagelist"] = result
        resData["isSuccess"] = 1
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    # level info
    try:
        levelInfo = SkillLevelInfo.objects.all()
        result = []

        for item in levelInfo:
            obj = {}
            res = model_to_dict(item)
            for field_name in item._meta.get_all_field_names():
                obj[field_name] = res[field_name]
            result.append(obj)

        resData["levellist"] = result
        resData["isSuccess"] = 1
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    # my learn language
    try:
#         learnlangList = UserLanguage.objects.filter(user_id=user)
        if TutorService.is_tutor(user):
            user_obj = Tutor.objects.get(user_id=user)
        else:
            user_obj = Tutee.objects.get(user_id=user)
        learnlangList = TutorService.learn_list(user_obj)
        result = []

        for item in learnlangList:
            obj = {}
            res = model_to_dict(item)
            for field_name in item._meta.get_all_field_names():
                obj[field_name] = res[field_name]
            result.append(obj)

        resData["mylearnlanglist"] = result
        resData["isSuccess"] = 1
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)
    resp = json.dumps(resData)
    print(resp);

    return HttpResponse(resp, content_type='application/json')


###############################################################################
# tools > tellpinsearch 
# url : tools/tellpinsearch/
# respose : json 
###############################################################################
@csrf_exempt
def tellpinsearch(request):
    """
    @summary: tellpinsearch 소개 페이지
    @author: msjang
    @param request
    @return: url - tellpin_search/search_dec.html
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    resData["display"] = 1

    return render_to_response("tellpin_search/search_dec.html", resData, RequestContext(request))

###############################################################################
# tools 세팅에서 display탭에서 변경된 전체 언어리스트, level 정보,
# 자신의 언어 설정 정보를 받아 저장한다. 
# url : tools/setlanguageinfo/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def set_mylanguage(request):
    """
    @summary: 내 언어정보 설정
    @author: msjang
    @param request
    @return: json
    """
    start = time.time()
    resData = {}
    resData["isSuccess"] = 0
    # parameter
    post_dict = json.loads(request.body)
    print post_dict["learnList"]

    for learninfo in post_dict["learnList"]:
        print learninfo

    user = TellpinUser.objects.get(email=request.session["email"])
    userID = user.id
    user.country = post_dict["userInfo"]["country"]
    user.state = post_dict["userInfo"]["state"]
    user.display_language = post_dict["userInfo"]["display_language_id"]
    user.save()
    resData["isSuccess"] = 1
    newlangList = []

    for learninfo in post_dict["learnList"]:
        newlangList.append(learninfo["language_id"])

    # my_status 삭제
    try:
        my_status = TSearchMyLastCategorySite.objects.filter(user_id=userID)
        if my_status.exists():
            my_status.delete()
    except Exception as err:
        print "@@@set_mylanguage_1", str(err)

    try:
        # 리스트에 없는 Language는 삭제해야 한다.
        # removeList = UserLanguage.objects.filter(user_id=userID).exclude(language_id__in=newlangList)
        # language와 연관된 사이트, 카테고리 제거
        # for lang in removeList:
        #    mycategorylist = MyCategory.objects.filter(user_id=userID, language_id=lang.language_id)
        #    for mycategory in mycategorylist:
        #        MySite.objects.filter(user_id=userID, category_id=mycategory.category_id).delete()
        #    mycategorylist.delete()
        # removeList.delete()

        removeList = UserLanguage.objects.filter(user_id=userID).exclude(language_id__in=newlangList)
        lang_id_list = []

        for lang in removeList:
            lang_id_list.append(lang.language_id)

        mycategorylist = TSearchMyCategory.objects.filter(user_id=userID, lang_id__in=lang_id_list)
        mycategory_id_list = []

        for mycategory in mycategorylist:
            mycategory_id_list.append(mycategory.category_id)

        TSearchMySite.objects.filter(user_id=userID, category_id__in=mycategory_id_list).delete()
        mycategorylist.delete()
        removeList.delete()

        # language 리스트의 내용을 업데이트 혹은 새로 생성한다.
        for item in post_dict["learnList"]:
            try:
                # 디폴트 (사전류) 카테고리, 사이트 세팅
                # print "defualt category"
                d = []
                f = []

                for ct in DefaultCategory.objects.filter(language1=item["language_id"], language2__in=newlangList):
                    # MyCategory.objects.get_or_create(user_id=userID, category_id=ct.category_id, language_id=item["language_id"])
                    if not TSearchMyCategory.objects.filter(user_id=userID, category_id=ct.category_id,
                                                     language_id=item["language_id"]).exists():
                        d.append(
                            TSearchMyCategory(user_id=userID, category_id=ct.category_id, language_id=item["language_id"]))

                    for st in RecommendedSite.objects.filter(category_id=ct.category_id):
                        try:
                            # MySite.objects.get_or_create(user_id=userID, category_id=ct.category_id, site_id=st.site_id)
                            if not TSearchMySite.objects.filter(user_id=userID, category_id=ct.category_id,
                                                         site_id=st.site_id).exists():
                                f.append(TSearchMySite(user_id=userID, category_id=ct.category_id, site_id=st.site_id))
                        except:
                            print "사이트 데이터 없음"

                try:
                    TSearchMyCategory.objects.bulk_create(d)
                except Exception as err:
                    # print "MyCategory bulk exception : " + str(err)
                    pass

                try:
                    TSearchMySite.objects.bulk_create(f)
                except Exception as err:
                    # print "MySite bulk exception : " + str(err)
                    pass

                obj = UserLanguage.objects.filter(user_id=userID, language_id=item["language_id"])

                if obj.exists():
                    # print "exists"
                    obj.update(level=item["level"], learn_speak=item["learn_speak"], primary=item["primary"],
                               learn_primary=item["learn_primary"])
                else:
                    # print "new"
                    obj = UserLanguage.objects.create(user_id=userID,
                                                      language_id=item["language_id"],
                                                      level=item["level"],
                                                      learn_speak=item["learn_speak"],
                                                      primary=item["primary"],
                                                      learn_primary=item["learn_primary"]
                                                      )

                    # 디폴트 (기타) 카테고리, 사이트 세팅
                    _q1 = Q(language_id=item["language_id"])
                    cateList = SearchCategory.objects.filter(_q1, dictionary=0)
                    new_category_list = []
                    new_site_list = []

                    for ct in cateList:
                        # MyCategory.objects.get_or_create(user_id=userID, category_id=ct.category_id, language_id=item["language_id"])
                        new_category_list.append(
                            TSearchMyCategory(user_id=userID, category_id=ct.category_id, language_id=item["language_id"]))

                        for st in RecommendedSite.objects.filter(category_id=ct.category_id):
                            try:
                                # MySite.objects.get_or_create(user_id=userID, category_id=ct.category_id, site_id=st.site_id)
                                new_site_list.append(
                                    TSearchMySite(user_id=userID, category_id=ct.category_id, site_id=st.site_id))
                            except:
                                print "사이트 데이터 없음"

                    TSearchMyCategory.objects.bulk_create(new_category_list)
                    TSearchMySite.objects.bulk_create(new_site_list)
            except UserLanguage.DoesNotExist:
                print "UserLanguage.DoesNotExist"
        resData["isSuccess"] = 1
        resData["message"] = "update success"
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)
    end = time.time()
    print "total time : ",
    print end - start
    resp = json.dumps(resData)

    return HttpResponse(resp, content_type='application/json')


###############################################################################
# 언어 리스트, 사용자 카테고리 리스트, 사용자-카테고리별 사이트 리스트
# 
# url : 
# respose :  
###############################################################################
@csrf_exempt
@tellpin_login_required
def get_data(request):
    """
    @summary: 내 언어정보, 카테고리정보, 사이트정보
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 0
    user_id = request.user.id

    # {언어정보, [{카테고리 정보, [{사이트정보}] ]}
    try:
        learnlangList = UserLanguage.objects.filter(user_id=user_id)
        result = []

        for item in learnlangList:
            obj = {}
            langinfo = LanguageInfo.objects.get(id=item.language_id)
            res = model_to_dict(langinfo)
            for field_name in langinfo._meta.get_all_field_names():
                if field_name == 'created_time':
                    obj[field_name] = str(res[field_name])
                else:
                    obj[field_name] = res[field_name]

            # category 정보 담기 ######################################################################
            mycategoryList = MyCategory.objects.filter(user_id=user_id, language_id=item.language_id)
            categorylist = []

            for category in mycategoryList:
                ctg_obj = {}
                try:
                    if category.category_id == 0:
                        continue
                    if category.category_id < 10001:
                        categoryInfo = SearchCategory.objects.get(category_id=category.category_id)
                    else:
                        categoryInfo = CustomSearchCategory.objects.get(category_id=category.category_id)

                    res = model_to_dict(categoryInfo)

                    for field_name in categoryInfo._meta.get_all_field_names():
                        if field_name == 'created_time':
                            ctg_obj[field_name] = str(res[field_name])
                        else:
                            ctg_obj[field_name] = res[field_name]

                    # site정보 담기 ######################################################################
                    mySiteList = MySite.objects.filter(user_id=user_id, category_id=category.category_id)
                    sitelist = []

                    for site in mySiteList:
                        site_obj = {}
                        try:
                            if site.site_id < 10001:
                                siteInfo = SearchSite.objects.get(site_id=site.site_id)
                            else:
                                siteInfo = CustomSearchSite.objects.get(site_id=site.site_id)
                        except:
                            print site.site_id
                            continue
                        res = model_to_dict(siteInfo)
                        for field_name in siteInfo._meta.get_all_field_names():
                            if field_name == 'created_time':
                                site_obj[field_name] = str(res[field_name])
                            else:
                                site_obj[field_name] = res[field_name]
                        sitelist.append(site_obj)

                    # site end ######################################################################
                    ctg_obj["sitelist"] = sitelist
                    categorylist.append(ctg_obj)
                except Exception as err:
                    print category.category_id
                    print str(err)
            # category end ######################################################################
            obj["categorylist"] = categorylist
            result.append(obj)
        resData["data"] = result
        resData["isSuccess"] = 1
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    # 첫번째 language에 해당 하는 카테고리(언어와 관련된) 리스트 얻기
    try:
        _q1 = Q(language_id=learnlangList[0].language_id)
        categoryList = SearchCategory.objects.filter(_q1, dictionary=0)
        result = []

        for item in categoryList:
            obj = {}
            res = model_to_dict(item)
            for field_name in item._meta.get_all_field_names():
                obj[field_name] = res[field_name]
            result.append(obj)

        resData["categorylist"] = result
        resData["isSuccess"] = 1
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    # 첫번째 language에 해당 하는 카테고리(사전) 리스트 얻기
    # 보여질 카테고리 리스트 추려내기
    newlangList = []
    try:
        learnlangList = UserLanguage.objects.filter(user_id=user_id)

        for learninfo in learnlangList:
            newlangList.append(learninfo.language_id)
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    try:
        dictionaryList = SearchCategory.objects.filter(language_id=learnlangList[0].language_id, dictionary=1)
        ctCage = []

        for ct in DefaultCategory.objects.filter(language1=learnlangList[0].language_id, language2__in=newlangList):
            ctCage.append(ct.category_id)

        result = []

        for item in dictionaryList:
            if item.category_id not in ctCage:
                continue
            obj = {}
            res = model_to_dict(item)
            for field_name in item._meta.get_all_field_names():
                obj[field_name] = res[field_name]
            result.append(obj)

        resData["dictionarylist"] = result
        resData["isSuccess"] = 1
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    resp = json.dumps(resData)
    return HttpResponse(resp, content_type='application/json')


###############################################################################
# 
# language에 해당하는 category 리스트를 전송한다.
# url : 
# respose :  
###############################################################################
@csrf_exempt
@tellpin_login_required
def get_category(request):
    """
    @summary: language에 해당하는 category list
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 0
    user_id = request.user.id
    # language parameter
    post_dict = json.loads(request.body)
    # language에 해당 하는 카테고리 리스트 얻기
    try:
        _q1 = Q(language_id=post_dict["language"])
        categoryList = SearchCategory.objects.filter(_q1, dictionary=0)
        result = []

        for item in categoryList:
            obj = {}
            res = model_to_dict(item)
            for field_name in item._meta.get_all_field_names():
                obj[field_name] = res[field_name]
            result.append(obj)

        resData["categorylist"] = result
        resData["isSuccess"] = 1
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    # 보여질 카테고리 리스트 추려내기
    newlangList = []
    try:
        learnlangList = UserLanguage.objects.filter(user_id=user)

        for learninfo in learnlangList:
            newlangList.append(learninfo.language_id)
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    try:
        dictionaryList = SearchCategory.objects.filter(language_id=post_dict["language"], dictionary=1)
        ctCage = []

        for ct in DefaultCategory.objects.filter(language1=post_dict["language"], language2__in=newlangList):
            ctCage.append(ct.category_id)

        result = []

        for item in dictionaryList:
            if item.category_id not in ctCage:
                continue
            obj = {}
            res = model_to_dict(item)
            for field_name in item._meta.get_all_field_names():
                obj[field_name] = res[field_name]
            result.append(obj)

        resData["dictionarylist"] = result
        resData["isSuccess"] = 1
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    resp = json.dumps(resData)
    return HttpResponse(resp, content_type='application/json')


###############################################################################
# 리스트 정렬
# parameter : 정렬방식(sort), 리스트 종류(type), 선택된 아이디(selected)
# url : 
# respose :  
###############################################################################
@csrf_exempt
def ordering_list(request):
    """
    @summary: category 정렬
    @author: msjang
    @param request
    @return: json
    """
    # TO-DO
    resData = {}
    resData["isSuccess"] = 0
    # language parameter
    post_dict = json.loads(request.body)
    # All Categories, My Categories, Site Recommendations, My Sites
    print post_dict["type"]
    print post_dict["sort"]
    # language에 해당 하는 카테고리 리스트 얻기
    try:
        categoryList = SearchCategory.objects.filter(language_id=post_dict["selected"]).order_by("category_name")
        result = []

        for item in categoryList:
            obj = {}
            res = model_to_dict(item)
            for field_name in item._meta.get_all_field_names():
                obj[field_name] = res[field_name]
            result.append(obj)

        resData["categorylist"] = result
        resData["isSuccess"] = 1
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    resp = json.dumps(resData)
    return HttpResponse(resp, content_type='application/json')


###############################################################################
# 
#
# url : 
# respose :  
###############################################################################
@csrf_exempt
@tellpin_login_required
def get_siteInfo(request):
    """
    @summary: 해당 카테고리에 속하는 사이트 리스트
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 0
    # 카테고리 id
    post_dict = json.loads(request.body)
    # 추천 사이트 리스트 얻기
    try:
        recommendList = RecommendedSite.objects.filter(category_id=post_dict["category"])
        result = []

        for item in recommendList:
            obj = {}
            siteInfo = SearchSite.objects.get(site_id=item.site_id)
            res = model_to_dict(siteInfo)
            for field_name in siteInfo._meta.get_all_field_names():
                obj[field_name] = res[field_name]
            result.append(obj)

        resData["recommendlist"] = result
        resData["isSuccess"] = 1
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    resp = json.dumps(resData)

    return HttpResponse(resp, content_type='application/json')


###############################################################################
# 
# parameter : category list
# url : 
# respose :  
###############################################################################
@csrf_exempt
@tellpin_login_required
def set_data(request):
    """
    @summary: 카테고리 리스트, 사이트 리스트 사용자가 편집한 내용 저장
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 0
    user_id = request.user.id
    # parameter
    post_dict = json.loads(request.body)

    # my_status 삭제
    try:
        TSearchMyLastCategorySite.objects.get(user_id=user_id).delete()
    except Exception as err:
        print str(err)

    # my category
    try:
        for langinfo in post_dict["data"]:
            print langinfo["language1"]
            newcategoryList = []

            if langinfo.has_key("categorylist"):
                for category in langinfo["categorylist"]:
                    if category['category_id'] > 0:
                        newcategoryList.append(category["category_id"])
                    else:
                        ctCategory = CustomSearchCategory.objects.create(
                            category_name=category['category_name'],
                            language_id=category['language_id'],
                            user_id=user_id
                        )
                        print 'categoryID', ctCategory.category_id
                        category['category_id'] = ctCategory.category_id
                        ctMyCategory = TSearchMyCategory.objects.create(
                            user_id=user_id,
                            category_id=category['category_id'],
                            language_id=ctCategory.language_id
                        )
                        newcategoryList.append(category['category_id'])

            print newcategoryList
            # 리스트에 없는 카테고리는 삭제해야 한다.
            mycategoryList = TSearchMyCategory.objects.filter(user_id=user_id, language_id=langinfo["id"]).exclude(
                category_id__in=newcategoryList)
            for check in mycategoryList:
                print check.category_id
            mycategoryList.delete()

            if langinfo.has_key("categorylist"):
                # 카테고리 리스트의 내용을 업데이트 혹은 새로 생성한다.
                for categoryinfo in langinfo["categorylist"]:
                    up_category, created = TSearchMyCategory.objects.update_or_create(user_id=user_id, language_id=langinfo["id"],
                                                                               category_id=categoryinfo["category_id"])
                    if created:
                        print "add category"

                    newsiteList = []
                    if categoryinfo.has_key("sitelist"):
                        for item in categoryinfo["sitelist"]:
                            if item['site_id'] > 0:
                                newsiteList.append(item["site_id"])
                            else:
                                ctSite = CustomSearchSite.objects.create(
                                    site_name=item['site_name'],
                                    search_url=item['search_url'],
                                    site_url=item['site_url'],
                                    user_id=user_id
                                )
                                ctMySite = TSearchMySite.objects.create(
                                    user_id=user_id,
                                    category_id=categoryinfo['category_id'],
                                    site_id=ctSite.site_id
                                )
                                item['site_id'] = ctSite.site_id
                                newsiteList.append(item['site_id'])
                    print newsiteList
                    if categoryinfo.has_key("sitelist"):
                        # 리스트에 없는 사이트는 삭제해야 한다.
                        mysitelist = TSearchMySite.objects.filter(user_id=user_id,
                                                           category_id=categoryinfo["category_id"]).exclude(
                            site_id__in=newsiteList)
                        for check in mysitelist:
                            print check.site_id
                        mysitelist.delete()
                        print categoryinfo
                        # 사이트 리스트의 내용을 업데이트 혹은 새로 생성한다.
                        for siteinfo in categoryinfo["sitelist"]:
                            up_site, created = TSearchMySite.objects.update_or_create(user_id=user_id,
                                                                               category_id=categoryinfo["category_id"],
                                                                               site_id=siteinfo["site_id"])
                            if created:
                                print "add site"
        resData["isSuccess"] = 1
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print "error : " + str(err)

    # My Categories, Site Recommendations, My Sites

    resp = json.dumps(resData)
    return HttpResponse(resp, content_type='application/json')


###############################################################################
# 
# tellpin search 클라이언트 프로그램 데이터 전송
# url : tools/getTsearchInfo
# parameter : user_id
# respose :  json
###############################################################################
@csrf_exempt
def send_to_tellpinsearch(request):
    """
    @summary: tellpin search 클라이언트 프로그램 데이터 전송
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 0

    # 사용자 정보
    if request.method == 'GET':
        qd = request.GET
    elif request.method == 'POST':
        qd = request.POST

    user = qd['user_id']

    # {언어정보, [{카테고리 정보, [{사이트정보}] ]}
    try:
        learnlangList = UserLanguage.objects.filter(user_id=user)
        result = []
        for item in learnlangList:
            obj = {}
            langinfo = LanguageInfo.objects.get(id=item.language_id)
            res = model_to_dict(langinfo)
            for field_name in langinfo._meta.get_all_field_names():
                obj[field_name] = res[field_name]

            # category 정보 담기 ######################################################################
            mycategoryList = TSearchMyCategory.objects.filter(user_id=user, language_id=item.language_id)
            categorylist = []
            for category in mycategoryList:
                ctg_obj = {}
                categoryInfo = SearchCategory.objects.get(category_id=category.category_id)
                res = model_to_dict(categoryInfo)
                for field_name in categoryInfo._meta.get_all_field_names():
                    ctg_obj[field_name] = res[field_name]

                # site정보 담기 ######################################################################
                mySiteList = TSearchMySite.objects.filter(user_id=user, category_id=category.category_id)
                sitelist = []
                for site in mySiteList:
                    site_obj = {}
                    siteInfo = SearchSite.objects.get(site_id=site.site_id)
                    res = model_to_dict(siteInfo)
                    for field_name in siteInfo._meta.get_all_field_names():
                        site_obj[field_name] = res[field_name]
                    sitelist.append(site_obj)
                # site end ######################################################################
                ctg_obj["sitelist"] = sitelist
                categorylist.append(ctg_obj)
            # category end ######################################################################
            obj["categorylist"] = categorylist
            result.append(obj)

        resData["data"] = result
        resData["isSuccess"] = 1
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    resp = json.dumps(resData)
    return HttpResponse(resp, content_type='application/json')


###############################################################################
# 
# tellpin.com tools history 데이터 전송
# url : tools/gethistory
# parameter : 
# respose :  json
##############################################################################
@csrf_exempt
@tellpin_login_required
def get_history(request):
    """
    @summary: tellpin search word log list
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 0
    user_id = request.user.id
    start = 80
    # log 정보 얻기
    try:
        historyList = TSearchMyWord.objects.filter(user=user_id).order_by("-created_time")[:start]
        result = []

        for item in historyList:
            obj = {}
            res = model_to_dict(item)
            for field_name in item._meta.get_all_field_names():
                obj[field_name] = res[field_name]
            obj["create_date"] = str(obj["created_time"])
            result.append(obj)

        resData["historylist"] = result
        resData["isSuccess"] = 1
        resData['allStart'] = start
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print "@@@@@@@@@@@get_history : TSearchMyWord" + str(err)

    # my_pin1 정보 얻기
    try:
        MyPin1List = TSearchMyPin.objects.filter(user=user_id, pin=1).order_by('-created_time')[:start]
        result = []

        for item in MyPin1List:
            obj = {}
            res = model_to_dict(item)
            print res
            for field_name in item._meta.get_all_field_names():
                obj[field_name] = res[field_name]
                obj['created_time'] = str(res['created_time'])
            result.append(obj)

        resData["mypin1"] = result
        resData["isSuccess"] = 1
        resData['pin1Start'] = start
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print "@@@@@@@@@@@get_history : TSearchMyPin1" + str(err)

    # my_pin2 정보 얻기
    try:
        MyPin2List = TSearchMyPin.objects.filter(user=user_id, pin=2).order_by('-created_time')[:start]
        result = []

        for item in MyPin2List:
            obj = {}
            res = model_to_dict(item)
            for field_name in item._meta.get_all_field_names():
                obj[field_name] = res[field_name]
                obj['created_time'] = str(res['created_time'])
            result.append(obj)

        resData["mypin2"] = result
        resData["isSuccess"] = 1
        resData['pin2Start'] = start
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print "@@@@@@@@@@@get_history : TSearchMyPin2" + str(err)

    # my_pin3 정보 얻기
    try:
        MyPin3List = TSearchMyPin.objects.filter(user=user_id, pin=3).order_by('-created_time')[:start]
        result = []

        for item in MyPin3List:
            obj = {}
            res = model_to_dict(item)
            for field_name in item._meta.get_all_field_names():
                obj[field_name] = res[field_name]
                obj['created_time'] = str(res['created_time'])
            result.append(obj)

        resData["mypin3"] = result
        resData["isSuccess"] = 1
        resData['pin3Start'] = start
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print "@@@@@@@@@@@get_history : TSearchMyPin3" + str(err)

    # my_pin 정보 얻기
    try:
        MyPinList = TSearchMyPin.objects.filter(user=user_id)
        result = []

        for item in MyPinList:
            obj = {}
            res = model_to_dict(item)
            for field_name in item._meta.get_all_field_names():
                obj[field_name] = res[field_name]
            obj["create_date"] = str(obj["created_time"])
            result.append(obj)

        resData["mypin"] = result
        resData["isSuccess"] = 1
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print "@@@@@@@@@@@get_history : MyPinList" + str(err)

    resp = json.dumps(resData)
    return HttpResponse(resp, content_type='application/json')


###############################################################################
# 
# tellpin.com tools history 데이터 전송
# url : tools/gethistory
# parameter : 
# respose :  json
##############################################################################
@csrf_exempt
@tellpin_login_required
def set_mypin(request):
    """
    @summary: tellpin search my pin word 설정
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    # parameter
    post_dict = json.loads(request.body)
    print post_dict

    try:
        # mypin1
        pin1_word_list = post_dict["mypin1"]
        # 리스트에 없는 word는 삭제해야 한다.
        # 사이트 리스트의 내용을 업데이트 혹은 새로 생성한다.
        result = ToolsService.set_my_pin_word_list(user_id, 1, pin1_word_list)
        if result:
            resData["message"] = "update success"
        else:
            resData["isSuccess"] = 0
            resData["message"] = "update fail"

        # mypin2
        pin2_word_list = post_dict["mypin2"]
        result = ToolsService.set_my_pin_word_list(user_id, 2, pin2_word_list)
        if result:
            resData["message"] = "update success"
        else:
            resData["isSuccess"] = 0
            resData["message"] = "update fail"

        # mypin3
        pin3_word_list = post_dict["mypin3"]
        result = ToolsService.set_my_pin_word_list(user_id, 3, pin3_word_list)
        if result:
            resData["message"] = "update success"
        else:
            resData["isSuccess"] = 0
            resData["message"] = "update fail"
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = "update fail"
        print str(err)

    resp = json.dumps(resData)
    return HttpResponse(resp, content_type='application/json')


@csrf_exempt
@tellpin_login_required
def set_myhistory(request):
    """
    @summary: tellpinsearch wordlog 설정
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    # parameter
    post_dict = json.loads(request.body)
    print post_dict

    # 삭제하지 않을 목록
    word_list = []
    for item in post_dict["myhistory"]:
        word_list.append(item["word_id"])
    print word_list

    try:
        # 리스트에 없는 word는 삭제해야 한다.
        result = ToolsService.set_my_word_list(user_id, word_list)
        if result:
            resData["message"] = "update success"
        else:
            resData["isSuccess"] = 0
            resData["message"] = "update fail"
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = "update fail"
        print str(err)

    resp = json.dumps(resData)
    return HttpResponse(resp, content_type='application/json')


###############################################################################
# tellpin search 클라이언트에서 세팅 화면으로 연결
#  
# url : tools/settings/
# respose : 
###############################################################################
@csrf_exempt
@tellpin_login_required
def tellpin_search_settings(request):
    """
    @summary: tellpin search 사용자 세팅 설정
    @author: msjang
    @param request
    @return: json
    """
    # 사용자 정보
    if request.method == 'GET':
        print "GET"
        qd = request.GET
    elif request.method == 'POST':
        print "POST"
        qd = request.POST

    if 'user_id' in qd:
        rec_email = qd['user_id']
    else:
        return HttpResponse("request no id parameter error")

    if 'user_pw' in qd:
        pwd = qd['user_pw']
    else:
        return HttpResponse("request no pw parameter error")

    if 'tab' in qd:
        tab = qd['tab']
    else:
        return HttpResponse("request no tab parameter error")

    # email check
    userinfo = TellpinUser.objects.get(email=rec_email)
    auth_user = TellpinAuthUser.objects.get(email=rec_email)
    if userinfo:
        # 아이디 일치
#추후 password 9999바꿀 때 해당 소스 적용.
        if UserAuthService.check_password(auth_user, pwd):
            # 패스워드 일치
            print "password valid"
            login_user = UserAuthService.check_user(rec_email, pwd)    #향후 비밀번호 처리 필요9999
            if login_user is not None:
                UserAuthService.setLoginSession(request, login_user)
                auth_login(request, login_user)
        else:
            # 패스워드 틀림
            print "password invalid"
            message = "이메일이나 비밀번호가 정확하지 않습니다."
            form = UserLoginForm()
            language = LanguageInfo.objects.filter()
            # return render_to_response('user_auth/landingpage.html', {'message':message, 'form':form}, RequestContext(request))
            # return render_to_response('비밀번호 다름 (페이지구현 or 안내말). 로그인 실패')
            return render_to_response('user_auth/landingpage.html',
                                      {'message': message, 'form': form, 'languages': language},
                                      RequestContext(request))
    else:
        # 이메일 존재안함.
        print "email invalid"
        message = "이메일이나 비밀번호가 정확하지 않습니다."
        form = UserLoginForm()
        language = LanguageInfo.objects.filter()
        # return render_to_response('user_auth/landingpage.html', {'message':message, 'form':form}, RequestContext(request))
        # return render_to_response('이메일 없음 (페이지구현 or 안내말 ). 로그인 실패')
        return render_to_response('user_auth/landingpage.html',
                                  {'message': message, 'form': form, 'languages': language}, RequestContext(request))

    if tab == '1' or tab == 1:
        # return render_to_response("tellpin_search/set_page.html", {"tab":tab, "menu":menu}, RequestContext( request ) )
        return HttpResponseRedirect('/tellpinsearch/settings/')
    elif tab == '2' or tab == 2:
        return HttpResponseRedirect('/help/tellpinsearch/')
    else:
        # return render_to_response("tellpin_search/set_page.html", {"tab":tab}, RequestContext( request ) )
        return HttpResponseRedirect('/tellpinsearch/history/')


###############################################################################
# tellpin search 클라이언트 프로그램에서 검색 기록을 DB에 저장한다.
# parameter : 
# url : tools/tellpin_search_log
# respose :  
###############################################################################
@csrf_exempt
def tellpinsearchWord(request):
    """
    @summary: tellpin search 클라이언트 프로그램 wordlog 저장
    @author: msjang
    @param request
    @return: json
    """
    print "tellpinsearchWord"

    if request.method == 'GET':
        qd = request.GET
    elif request.method == 'POST':
        qd = request.POST
    try:
        print qd
#         user_id = qd["user_id"]
        user_id = TellpinUser.objects.get(user_id=qd["user_id"])
        Language = LanguageInfo.objects.get(id=qd["language"])
#         Language = qd["language"]
        Category1 = qd["category1"]
        Category2 = qd["category2"]
        Category3 = qd["category3"]
        Word = qd["word"]
        Url = qd["target_url"]

        Url_info = urlParser(Url)

        result_url1 = qd["result_url1"]
        result_url2 = qd["result_url2"]
        result_url3 = qd["result_url3"]
        print user_id, Url_info
    except Exception as err:
        logger.error(str(err))
        print '@@@@@tellpinsearchWord_1 : ', str(err)
        return HttpResponse("request Error : " + str(err))

    try:
        searchWord_obj = TSearchMyWord()
        searchWord_obj.user = user_id

        searchWord_obj.word = Word
        searchWord_obj.image = Url_info['img']
        searchWord_obj.url = Url
        searchWord_obj.title = Url_info['title']

        searchWord_obj.lang = Language
        searchWord_obj.category1_id = Category1
        searchWord_obj.category2_id = Category2
        searchWord_obj.category3_id = Category3

        searchWord_obj.site1_id = result_url1
        searchWord_obj.site2_id = result_url2
        searchWord_obj.site3_id = result_url3

        searchWord_obj.save()

#         newwordlog.language = Language
#         newwordlog.category1 = Category1
#         newwordlog.category2 = Category2
#         newwordlog.category3 = Category3
#         newwordlog.word = Word
#         newwordlog.target_url = Url
#         newwordlog.result_url1 = result_url1
#         newwordlog.result_url2 = result_url2
#         newwordlog.result_url3 = result_url3
#         newwordlog.title = Url_info['title']
#         newwordlog.image = Url_info['img']
#         newwordlog.user_id = user_id
#         newwordlog.save()
    except Exception as err:
        logger.error(str(err))
        print '@@@@@tellpinsearchWord_2 : ', str(err)
        return HttpResponse("DB Insert Error : " + str(err))

    return HttpResponse("success")


@csrf_exempt
def tellpinsearchPinWord(request):
    """
    @summary: tellpin search word 해당 pin list 저장
    @author: msjang
    @param request
    @return: json
    """
    print "tellpinsearchPinWord"
    if request.method == 'GET':
        qd = request.GET
    elif request.method == 'POST':
        qd = request.POST

    try:
        user_id = TellpinUser.objects.get(user_id=qd["user_id"])
        word = qd["word"]
        pin_n = qd["table"]
        print user_id, word, pin_n
    except Exception as err:
        logger.error(str(err))
        print '@@@@@tellpinsearchPinWord_1 : ', str(err)
        return HttpResponse("request Error : " + str(err))

    try:
        pin_obj = TSearchMyPin()
        word_list = TSearchMyWord.objects.filter(word=str(word)).order_by("-created_time")
        pin_obj.word_id = word_list[0]
        pin_obj.user = user_id
        pin_obj.word = word
        pin_obj.pin = pin_n
        pin_obj.save()

#         if pin_n == "1":
#             MyPin1.objects.update_or_create(user_id=user_id, word=word)
#         elif pin_n == "2":
#             MyPin2.objects.update_or_create(user_id=user_id, word=word)
#         elif pin_n == "3":
#             MyPin3.objects.update_or_create(user_id=user_id, word=word)
    except Exception as err:
        logger.error(str(err))
        print '@@@@@tellpinsearchPinWord_2 : ', str(err)
        return HttpResponse("DB Insert Error : " + str(err))

    return HttpResponse("success")


###############################################################################
# url 파싱
#
# url : 
# respose :  
###############################################################################
def urlParser(url):
    """
    @summary: tellpin search search_url 파싱
    @author: msjang
    @param url
    @return: info( object )
    """
    from urllib import urlopen
    if os.name != 'nt':
        from BeautifulSoup import BeautifulSoup
    else:
        from bs4 import BeautifulSoup
    # from bs4 import BeautifulSoup
    import re

#     url = urllib.quote(url.encode('utf8'), ':/')

    info = {'title': 'No Title', 'img': 'No Image', 'url': url}

    try:
        if not (re.search(r'http://', url) or re.search(r'https://', url)):
            url = "http://" + url
        optionsPage = urlopen(url)
        soup = BeautifulSoup(optionsPage)
        main_title = soup.findAll('title')
        info['title'] = main_title[0].text
        # img_re=r'https?://'
        # img_urls= soup.findAll('img', src=re.compile(img_re))
        img_urls = soup.findAll('img')
        if len(img_urls) == 0:
            info['img'] = 'No Image'
        else:
            try:
                info['img'] = img_urls[0]['src']
            except:
                info['img'] = img_urls[0]['data-src']

    except Exception, err:
        print('[htmlParser]BeautifulSoup except ' + str(err))

    return info


###############################################################################
# 
# 
# url : 
# respose :  
###############################################################################
@csrf_exempt
def ajax_removeTxt(request):
    """
    @summary: 서버에 만들어진 text 파일 삭제
    @author: msjang
    @param request
    @return: HttpResponse
    """
    full_path = "%s%s" % (BASE_DIR, "/static/img/upload/tellpin_search/")

    list_dir = os.listdir(full_path)

    for file in list_dir:
        if (os.path.isfile("%s%s" % (full_path, file))):
            os.remove("%s%s" % (full_path, file))

    return HttpResponse("")


###############################################################################
# 
# url : tools/getAutoSearchPlayerData/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def get_auto_search_player_data(request):
    """
    @summary: tellpin.com AutoSearchPlayer 해당 정보 가져오기
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    try:
        # 사용자 사용 언어 리스트 얻기
        resData["myLanguageList"] = ToolsService.get_my_language(user_id)
        # 첫번째 language에 해당 하는 카테고리(사전) 리스트 얻기
        resData["myDictionaryList"] = ToolsService.get_my_category(user_id, resData["myLanguageList"][0]['id'])
        # 첫번째 Category에 해당 하는 사이트 리스트 얻기
        resData["mySiteList"] = ToolsService.get_my_site(user_id, resData["myDictionaryList"][0]['category_id'])
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    resp = json.dumps(resData)

    return HttpResponse(resp, content_type='application/json')


###############################################################################
#  
# url : tools/getAutoSearchPlayerDic/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def get_auto_search_player_dic(request):
    """
    @summary: tellpin.com AutoSearchPlayer 사전 정보 가져오기
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    try:
        resData["myDictionaryList"] = ToolsService.get_my_category(user_id, post_dict["id"])
        # 첫번째 Category에 해당 하는 사이트 리스트 얻기
        resData["mySiteList"] = ToolsService.get_my_site(user_id, resData["myDictionaryList"][0]['category_id'])
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    resp = json.dumps(resData)

    return HttpResponse(resp, content_type='application/json')


###############################################################################
#  
# url : tools/getAutoSearchPlayerSite/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def get_auto_search_player_site(request):
    """
    @summary: tellpin.com AutoSearchPlayer 사이트 정보 가져오기
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    try:
        resData["mySiteList"] = ToolsService.get_my_site(user_id, post_dict['id'])
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    resp = json.dumps(resData)

    return HttpResponse(resp, content_type='application/json')


###############################################################################
#  
# url : tools/get_pin_name/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def get_pin_name(request):
    """
    @summary: TellpinSearch 클라이언트 프로그램 pin list 이름 가져오기
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    try:
        result = ToolsService.get_my_pin_name(user_id)
        resData["pinName1"] = result['pin1']
        resData["pinName2"] = result['pin2']
        resData["pinName3"] = result['pin3']
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    resp = json.dumps(resData)

    return HttpResponse(resp, content_type='application/json')


###############################################################################
#  
# url : tools/set_pin_name/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def set_pin_name(request):
    """
    @summary: TellpinSearch 클라이언트 프로그램 pin list 이름 설정하기
    @author: hsrjmk
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1

    post_dict = json.loads(request.body)
    key = post_dict.keys()
    print key[0], post_dict.get(key[0])
    user_id = request.user.id

    try:
        if key[0] == 'pin1':
            ToolsService.set_my_pin_name(user_id, 1, post_dict.get(key[0]))
        elif key[0] == 'pin2':
            ToolsService.set_my_pin_name(user_id, 2, post_dict.get(key[0]))
        elif key[0] == 'pin3':
            ToolsService.set_my_pin_name(user_id, 3, post_dict.get(key[0]))
        else:
            resData["isSuccess"] = 0
            resData["message"] = '[set_pin_name] Unknown pin number'

        resData["pinType"] = key[0]
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    resp = json.dumps(resData)

    return HttpResponse(resp, content_type='application/json')


@csrf_exempt
@tellpin_login_required
def ajax_moreHistory(request):
    """
    @summary: TellpinSearch wordlog 무한스크롤
    @author: hsrjmk
    @param none
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    # type 1 : all, 2 : pin1, 3 : pin2, 4 : pin3
    post = json.loads(request.body)
    count = 10  # 10개씩 가져오기
    type = post['type']
    page = (post['start'] + count) / 10

    try:
        if (post['start'] + count) % 10 == 0:
            if type == 1:
                resData["historylist"] = ToolsService.get_my_word_list(user_id, page)
            elif type == 2:
                resData["mypin1"] = ToolsService.get_my_pin_list(user_id, 1, page)
            elif type == 3:
                resData["mypin2"] = ToolsService.get_my_pin_list(user_id, 2, page)
            elif type == 4:
                resData["mypin3"] = ToolsService.get_my_pin_list(user_id, 3, page)
            else:
                resData["isSuccess"] = 0
                resData["message"] = '[ajax_moreHistory] Unknown type'
        else:
            resData["isSuccess"] = 0
            resData["message"] = 'end of list'
    except Exception as err:
        resData["isSuccess"] = 0
        resData["message"] = str(err)
        print str(err)

    resp = json.dumps(resData, cls=DjangoJSONEncoder)
    return HttpResponse(resp, content_type='application/json')
