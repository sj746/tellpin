# -*- coding: utf-8 -*-

###############################################################################
# filename    : tools > service.py
# description : Tellpin tools
# author      : hsrjmk@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20161124 hsrjmk 최초 작성
#
#
###############################################################################
from datetime import timedelta

from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator
from django.db.models.aggregates import Max

from common.models import LanguageInfo
from tutor.models import Tutee, Tutor
from tools.models import (
    TSearchCategoryByLangInfo, TSearchCategoryInfo, TSearchSiteByCategoryInfo,
    TSearchMyCategory, TSearchMySite, TSearchMyWord, TSearchMyPin, TSearchMyPinName, 
    TSearchMyLastLang, TSearchMyLastCategorySite
)
from user_auth.models import TellpinUser
from operator import itemgetter
from tutor.service import TutorService


class ToolsService(object):
    def __init__(self):
        pass

    PER_PAGE = 10

    ####################################################################
    #    Tools 관련 QuerySet
    ####################################################################
    @staticmethod
    def get_my_word_list(user_id, page=1, page_count=1):
        """
        @summary: 검색 단어 리스트 조회
        @author: hsrjmk
        @param user_id: User id
        @param page: 시작 페이지 번호
        @param page_count: 페이지 개수
        @return: result( list )
        """
        search_my_word_objs = TSearchMyWord.objects.filter(user_id=user_id).select_related('lang').order_by("-created_time")

        paginator = Paginator(search_my_word_objs, ToolsService.PER_PAGE)
        current_page = paginator.page(page)

        result = []
        for i in xrange(page_count):
            current_page = paginator.page(page + i)
            result += ToolsService.make_search_my_word_list(current_page)
            if current_page.has_next() is False:
                break

        return result

    @staticmethod
    def get_my_pin_list(user_id, pin=1, page=1, page_count=1):
        """
        @summary: 핀에 저장된 검색 단어 리스트 조회
        @author: hsrjmk
        @param user_id: User id
        @param pin: 핀 번호
        @param page: 시작 페이지 번호
        @param page_count: 페이지 개수
        @return: result( list )
        """
        my_pin_word_objs = TSearchMyPin.objects.filter(user_id=user_id, pin=pin).select_related('word_id').order_by("-created_time")

        paginator = Paginator(my_pin_word_objs, ToolsService.PER_PAGE)
        current_page = paginator.page(page)

        result = []
        for i in xrange(page_count):
            current_page = paginator.page(page + i)
            result += ToolsService.make_my_pin_list(current_page)
            if current_page.has_next() is False:
                break

        return result

    @staticmethod
    def get_mypin(user_id, utc_delta=0):
        """
        @summary: 전체 핀 리스트 조회. View에서 호출하지만 자바스크립트(js)에서 쓰이는지 확인 필요함...사용하지 않는 듯...
        @author: hsrjmk
        @param user_id: User id
        @return: result( list )
        """
        result = []
#         try:
#             my_pin_objs = TSearchMyPin.objects.filter(user_id=user_id).select_related('word_id').order_by("-created_time")
#             for my_pin in my_pin_objs:
#                 obj = model_to_dict(my_pin.word_id)
#                 obj['created_date'] = (my_pin.created_time + timedelta(hours=utc_delta)).date()
#
#                 result.append(obj)
#         except ObjectDoesNotExist as oe:
#             print str(oe)

        return result

    @staticmethod
    def get_my_pin_name(user_id):
        """
        @summary: 핀 이름 조회
        @author: hsrjmk
        @param user_id: User id
        @return result( object )
        """
        result = {}
        print "get_my_pin_name"
        try:
            if TSearchMyPinName.objects.filter(user_id=user_id).exists():
                my_pin_name_obj = TSearchMyPinName.objects.get(user_id=user_id)
                result['pin1'] = my_pin_name_obj.pin1
                result['pin2'] = my_pin_name_obj.pin2
                result['pin3'] = my_pin_name_obj.pin3
        except ObjectDoesNotExist as e:
            print '@@@@@get_my_pin_name : ', str(e)

        return result

    @staticmethod
    def set_my_pin_name(user_id, pin_num, name):
        """
        @summary: 핀 이름 저장
        @author: hsrjmk
        @param user_id: User id
        @param pin_num: pin number
        @param name: pin name
        @return result: My word history list
        """
        result = {}
        print "set_my_pin_name"
        try:
            my_pin_name_obj, created = TSearchMyPinName.objects.get_or_create(user_id=user_id)
            if pin_num == 1:
                my_pin_name_obj.pin1 = name
            elif pin_num == 2:
                my_pin_name_obj.pin2 = name
            elif pin_num == 3:
                my_pin_name_obj.pin3 = name

            my_pin_name_obj.save()
        except ObjectDoesNotExist as e:
            print '@@@@@set_my_pin_name : ', str(e)

        return result

    @staticmethod
    def get_my_language(user_id):
        """
        @summary: 입력 언어 리스트 조회
        @author: hsrjmk
        @param user_id: User id
        @return result: My language list
        """
        result = []
        try:
            user_obj = TellpinUser.objects.get(user_id=user_id)
            if user_obj.is_tutor:
                tutee_or_tutor_obj = Tutor.objects.get(user_id=user_id)
            else:
                tutee_or_tutor_obj = Tutee.objects.get(user_id=user_id)

            lang_id_list = []
            for i in xrange(1, 3):
                lang_id_list.append(getattr(tutee_or_tutor_obj, 'native{0}_id_id'.format(i), None))
            for i in xrange(1, 6):
                lang_id_list.append(getattr(tutee_or_tutor_obj, 'lang{0}_id_id'.format(i), None))

            lang_objs = LanguageInfo.objects.filter(id__in=lang_id_list)
            for lang in lang_objs:
                language = {}
                language['id'] = lang.id
                language['language1'] = lang.lang_english
                result.append(language)
        except ObjectDoesNotExist as e:
            print '@@@@@get_my_language : ', str(e)

        return result

    @staticmethod
    def get_my_category(user_id, lang_id):
        """
        @summary: 검색 카테고리 리스트 조회
        @author: hsrjmk
        @param user_id: TellpinUser id
        @param lang_id: LanguageInfo id
        @return result: My word history list
        """
        result = []
        try:
            exclude_category_id_list = TSearchMyCategory.objects.filter(user_id=user_id, lang_id=lang_id, status=2).values_list('category_id')
            category_by_lang_objs = TSearchCategoryByLangInfo.objects.filter(lang_id=lang_id, category__is_dic=1)\
                                                                .select_related('category')\
                                                                .exclude(category_id__in=exclude_category_id_list)

            for category_by_lang in category_by_lang_objs:
                category = {}
                category['category_id'] = category_by_lang.category_id
                category['language_id'] = category_by_lang.lang_id
                category['category_name'] = category_by_lang.category.category_english
                category['dictionary'] = category_by_lang.category.is_dic
                result.append(category)

#             for include_category in include_category_list:
#                 category = {}
#                 category['category_id'] = None
#                 category['language_id'] = include_category.lang_id
#                 category['category_name'] = include_category.category
#                 category['dictionary'] = getattr(include_category, 'is_dic', 0)
#                 result.append(category)

        except ObjectDoesNotExist as e:
            print '@@@@@get_my_category : ', str(e)

        return result

    @staticmethod
    def get_my_site(user_id, category_id):
        """
        @summary: 검색 카테고리 사이트 리스트 조회
        @author: hsrjmk
        @param user_id: TSearchMyWord model object
        @param category_id: TSearchMyWord model object
        @return result: My word history list
        """
        result = []
        try:
            exclude_site_id_list = TSearchMySite.objects.filter(user_id=user_id, category_id=category_id, status=2).values_list('site_id')
            site_by_category_objs = TSearchSiteByCategoryInfo.objects.filter(category_id=category_id, is_dic=1)\
                                                                .exclude(id__in=exclude_site_id_list)

            for site_by_category in site_by_category_objs:
                site = {}
                site['site_id'] = site_by_category.id
                site['search_url'] = site_by_category.search_url
                site['site_name'] = site_by_category.site
                site['site_url'] = site_by_category.site_url
                result.append(site)
        except ObjectDoesNotExist as e:
            print '@@@@@get_my_site : ', str(e)

        return result

    @staticmethod
    def set_my_word_list(user_id, word_list=[]):
        """
        @summary: 검색한 단어 목록 재설정 (현재 UI에서는 삭제만 제공)
        @author: hsrjmk
        @param user_id: User id
        @param word_list: 삭제에서 제외할 단어 목록
        @return result: Boolean 삭제 성공 여부
        """
        result = True
        try:
            delete_word_objs = TSearchMyWord.objects.filter(user_id=user_id).exclude(id__in=word_list)
            delete_word_objs.delete()
        except Exception as e:
            print '@@@@@set_my_word_list : ', str(e)
            result = False

        return result

    @staticmethod
    def set_my_pin_word_list(user_id, pin_num, word_list=[]):
        """
        @summary: pin 단어 목록 재설정 (추가, 삭제)
        @author: hsrjmk
        @param user_id: User id
        @param pin_num: pin number
        @param word_list: 삭제에서 제외 또는 추가할 단어 목록
        @return result: Boolean 삭제 성공 여부
        """
        result = True
        try:
            # 성능 trade-off가 있음.
            # update_or_create를 하면, 속도 이슈가 생길  듯함.
            # delete all 이후 bulk create를 하면, 속도는 빠르나 row 생성 id 최대치가 금방 될 듯함.
            if True:
#                 pin_word_list = []
#                 for word in word_list:
#                     pin_word_list.append(word['word'])
# 
#                 delete_word_objs = TSearchMyPin.objects.filter(user_id=user_id, pin=pin_num).exclude(word__in=pin_word_list)
#                 delete_word_objs.delete()
#                 for word in word_list:
#                     obj, created = TSearchMyPin.objects.update_or_create(
#                                     user_id=word['user_id'],
#                                     word_id_id=word['word_id'],
#                                     pin=pin_num,
#                                     word=word['word'],
#                                     defaults={'user_id': word['user_id'],
#                                               'word_id_id': word['word_id'],
#                                               'pin': pin_num,
#                                               'word': word['word']},
#                                 )
                pin_word_id_list = []
                for word in word_list:
                    pin_word_id_list.append(word['word_id'])
                delete_word_objs = TSearchMyPin.objects.filter(user_id=user_id, pin=pin_num).exclude(word_id__in=pin_word_id_list)
                delete_word_objs.delete()

                add_word_objs = []
                keep_word_id_list = TSearchMyPin.objects.filter(user_id=user_id, pin=pin_num).values_list('word_id', flat=True)
                for word in word_list:
                    if word['word_id'] not in keep_word_id_list:
                        my_pin = TSearchMyPin(
                            user_id=word['user_id'],
                            word_id_id=word['word_id'],
                            pin=pin_num,
                            word=word['word'],
                        )
                        add_word_objs.append(my_pin)
 
                TSearchMyPin.objects.bulk_create(add_word_objs)
            else:
                # Delete all
                delete_word_objs = TSearchMyPin.objects.filter(user_id=user_id, pin=pin_num)
                delete_word_objs.delete()
                # Bulk create            
                add_word_objs = []
                for word in word_list:
                    my_pin = TSearchMyPin(
                            user_id=word['user_id'],
                            word_id_id=word['word_id'],
                            pin=pin_num,
                            word=word['word'])
                    add_word_objs.append(my_pin)
 
                TSearchMyPin.objects.bulk_create(add_word_objs)
        except Exception as e:
            print '@@@@@set_my_pin_word_list : ', str(e)
            result = False

        return result

    #########################################################################################
    #    TellpinSearch Setting API
    #########################################################################################
    @staticmethod
    def get_category_info(user_id, lang_id=2):
        """
        @summary: 해당 언어에 해당하는 카테고리 리스트
        @author: hsrjmk
        @param user_id: User id
        @param lang_id : language id
        @return result: obj( object )
        """
        obj = {}
        try:
            # 사용자 삭제 카테고리 정보
            userDeleteCategoryList = TSearchMyCategory.objects.filter(user_id=user_id, status=2, category_id__lt=10001)\
                                                                .values_list('category_id', flat=True)
            # 카테고리 정보
            categoryIDList = TSearchCategoryByLangInfo.objects.filter(lang_id__in=[0, lang_id])\
                                                                .exclude(category_id__in=userDeleteCategoryList)\
                                                                .values_list('category_id', flat=True)\
                                                                .distinct()\
                                                                .order_by('category_id')
            listCategoryID = list(categoryIDList)
            print userDeleteCategoryList
            if lang_id == 1:
                defaultCategoryList = TSearchCategoryInfo.objects.filter(id__in=listCategoryID, is_dic=0)\
                                                                .extra(select={'category_id' : 'id', 'category_name' : 'category_korean'})\
                                                                .values('category_id', 'category_name')\
                                                                .order_by('category_name')
                dictCategoryList = TSearchCategoryInfo.objects.filter(id__in=listCategoryID, is_dic=1)\
                                                                .extra(select={'category_id' : 'id', 'category_name' : 'category_korean'})\
                                                                .values('category_id', 'category_name')\
                                                                .order_by('category_name')
            elif lang_id == 3:
                defaultCategoryList = TSearchCategoryInfo.objects.filter(id__in=listCategoryID, is_dic=0)\
                                                                .extra(select={'category_id' : 'id', 'category_name' : 'category_chinese'})\
                                                                .values('category_id', 'category_name')\
                                                                .order_by('category_name')
                dictCategoryList = TSearchCategoryInfo.objects.filter(id__in=listCategoryID, is_dic=1)\
                                                                .extra(select={'category_id' : 'id', 'category_name' : 'category_chinese'})\
                                                                .values('category_id', 'category_name')\
                                                                .order_by('category_name')
            elif lang_id == 4:
                defaultCategoryList = TSearchCategoryInfo.objects.filter(id__in=listCategoryID, is_dic=0)\
                                                                .extra(select={'category_id' : 'id', 'category_name' : 'category_spanish'})\
                                                                .values('category_id', 'category_name').order_by('category_name')
                dictCategoryList = TSearchCategoryInfo.objects.filter(id__in = listCategoryID, is_dic=1)\
                                                                .extra(select={'category_id' : 'id', 'category_name' : 'category_spanish'})\
                                                                .values('category_id', 'category_name')\
                                                                .order_by('category_name')
            else:
                defaultCategoryList = TSearchCategoryInfo.objects.filter(id__in=listCategoryID, is_dic=0)\
                                                                .extra(select={'category_id' : 'id', 'category_name' : 'category_english'})\
                                                                .values('category_id', 'category_name')\
                                                                .order_by('category_name')
                dictCategoryList = TSearchCategoryInfo.objects.filter(id__in=listCategoryID, is_dic=1)\
                                                                .extra(select={'category_id' : 'id', 'category_name' : 'category_english'})\
                                                                .values('category_id', 'category_name')\
                                                                .order_by('category_name')
            # 유저가 생성한 카테고리 리스트
            userAddCategoryList = TSearchMyCategory.objects.filter(user_id=user_id,
                                                                   lang_id__in=[0, lang_id],
                                                                   status__in=[ 1, 3 ])\
                                                                   .extra(select={'category_name' : 'category'})\
                                                                   .values('category_id', 'category_name')
            arrDefault = list( dictCategoryList ) + list( defaultCategoryList ) + list( userAddCategoryList )
            sortList = sorted( arrDefault, key = itemgetter('category_name') )          
            obj['category'] = sortList
            #print 'arrdefault', arrDefault
            #print obj['category']
            if lang_id <= 10:
                categoryID = dictCategoryList[0]['category_id']
            else:
                categoryID = defaultCategoryList[0]['category_id']
            obj['site'] = ToolsService.get_site_info( user_id, lang_id, categoryID )
            #print obj['category']
        except Exception as e:
            obj['err'] = str(e)
            print '@@@@@get_category_info : ', str(e)
        finally:
            return obj

    @staticmethod
    def get_site_info(user_id, lang_id, category_id):
        """
        @summary: 해당 카테고리에 해당하는 사이트 리스트
        @author: hsrjmk
        @param user_id : 유저 id
        @return: obj( object )
        """
        result = []
        try:
            # 사용자가 삭제한 default 사이트 리스트
            userDelSiteList = TSearchMySite.objects.filter(status=2,
                                                           site_id__lt=10001,
                                                           lang_id__in=[0, lang_id],
                                                           user_id=user_id,
                                                           category_id=category_id).values_list('site_id')
            # default 사이트 리스트
            defaultSiteList = TSearchSiteByCategoryInfo.objects.filter(category_id=category_id,
                                                                       in_lang_id__in=[ 0, lang_id ])\
                                                        .exclude(id__in=userDelSiteList)\
                                                        .extra(select={'site_id' : 'id', 'site_name' : 'site', 'lang_id' : 'in_lang_id'})\
                                                        .values('lang_id', 'category_id',
                                                                'site_id', 'site_name', 'search_url', 'site_url',
                                                                'is_dic', 'is_default', 'support_mobile')\
                                                        .order_by('site_name')
            userAddSiteList = TSearchMySite.objects.filter(status__in=[1, 3],
                                                           site_id__gte=10001,
                                                           lang_id__in=[ 0, lang_id ],
                                                           user_id=user_id,
                                                           category_id=category_id)\
                                                        .extra(select={'site_name' : 'site'})\
                                                        .values().order_by('site')
            result = list( defaultSiteList ) + list( userAddSiteList )
        except Exception as e:
            print '@@@@@get_site_info : ', str(e)
        finally:
            return result

    @staticmethod
    def add_category(user_id, lang_id, category_obj):
        """
        @summary: 해당 카테고리에 해당하는 사이트 리스트
        @author: hsrjmk
        @param user_id: 유저 id
        @param category: category 정보 담겨있는 객체 
        @return: 1( success ), 0( fail )
        """
        result = 1
        try:
            customCategory = TSearchMyCategory()
            for i in category_obj:
                setattr( customCategory, i, category_obj[i] )
            maxCategory = TSearchMyCategory.objects.all().aggregate( max = Max( 'category_id' ) )
            if maxCategory['max'] and maxCategory['max'] > 10000:
                maxCategoryID = int( maxCategory['max'] )
            else:
                maxCategoryID = 10000 
            maxCategoryID += 1
            customCategory.user_id = user_id
            customCategory.lang_id = lang_id
            customCategory.category_id = maxCategoryID
            customCategory.category = category_obj['category_name']
            customCategory.status = 1
            customCategory.save()
        except Exception as e:
            print '@@@@@add_category : ', str(e)
            result = 0
        finally:
            return result

    @staticmethod
    def delete_category(user_id, lang_id, category_obj):
        """
        @summary: 해당 카테고리에 삭제 표시
        @author: hsrjmk
        @param user_id: 유저 id
        @param category_id: tellpin_search_category_info pk 
        @return: 1( success ), 0( fail )
        """
        result = 1
        try:
            updated_values = {}
            updated_values['status'] = 2
            my_category_obj, created = TSearchMyCategory.objects.update_or_create(
                                                                    user_id=user_id,
                                                                    lang_id=lang_id,
                                                                    category_id=category_obj['category_id'],
                                                                    category=category_obj['category_name'],
                                                                    defaults=updated_values)
        except Exception as e:
            print '@@@@@delete_category : ', str(e)
            result = 0
        finally:
            return result

    @staticmethod
    def get_restore_category_list(user_id, lang_id):
        """
        @summary: 삭제한 카테고리 리스트
        @author: hsrjmk
        @param user_id: 유저 id
        @param lang_id: language table pk 
        @return: result( list )
        """
        result = []
        try:
            arrCategory = TSearchMyCategory.objects.filter(user_id=user_id,
                                                           lang_id=lang_id,
                                                           status=2)\
                                                    .extra(select={'category_name' : 'category'})\
                                                    .values().order_by('category_name')
            result = list(arrCategory)
        except Exception as e:
            print '@@@@@get_restore_category_list : ', str(e)
            result = []
            result.append(str(e))
        finally:
            return result

    @staticmethod
    def restore_category(user_id, lang_id, category_id_list):
        """
        @summary: 삭제한 카테고리 복원
        @author: hsrjmk
        @param user_id: 유저 id
        @param lang_id: language table 참조키
        @param category_id_list: category_id 들어있는 list
        @return: 1( success ), 0( fail )
        """
        result = 1
        try:
            TSearchMyCategory.objects.filter(user_id=user_id,
                                             lang_id=lang_id,
                                             category_id__in=category_id_list).update(status=3)
        except Exception as e:
            print '@@@@@restore_category : ', str(e)
            result = 0
        finally:
            return result

    @staticmethod
    def add_site(user_id, lang_id, site_obj):
        """
        @summary: 해당 카테고리에 해당하는 사이트 리스트
        @author: hsrjmk
        @param user_id: 유저 id
        @param site_obj: site 정보 담겨있는 객체 
        @return: 1( success ), 0( fail )
        """
        result = 1
        try:
            customSite = TSearchMySite()
            for i in site_obj:
                setattr( customSite, i, site_obj[i] )
            maxSite = TSearchMySite.objects.all().aggregate( max = Max( 'site_id' ) )
            if maxSite['max'] and maxSite['max'] > 10000:
                maxSiteID = int( maxSite['max'] )
            else:
                maxSiteID = 10000
            maxSiteID += 1
            customSite.user_id = user_id
            customSite.lang_id = lang_id
            customSite.site_id = maxSiteID
            customSite.site = site_obj['site_name']
            customSite.status = 1
            customSite.save()
        except Exception as e:
            print '@@@@@add_site : ', str(e)
            result = 0
        finally:
            return result

    @staticmethod
    def delete_site(user_id, lang_id, site_obj):
        """
        @summary: 해당하는 사이트에 삭제 표시
        @author: hsrjmk
        @param user_id: 유저 id
        @param site: site 정보 담겨있는 객체 
        @return: 1( success ), 0( fail )
        """
        result = 1
        try:
            updated_values = {}
            updated_values['status'] = 2
            my_site_obj, created = TSearchMySite.objects.update_or_create(
                                                            user_id=user_id,
                                                            lang_id=lang_id,
                                                            category_id=site_obj['category_id'],
                                                            site_id=site_obj['site_id'],
                                                            site=site_obj['site_name'],
                                                            search_url=site_obj['search_url'],
                                                            site_url=site_obj['site_url'],
                                                            defaults=updated_values)
        except Exception as e:
            print '@@@@@delete_site : ', str(e)
            result = 0
        finally:
            return result

    @staticmethod
    def get_restore_site_list(user_id, lang_id, category_id):
        """
        @summary: 삭제한 사이트 리스트
        @author: hsrjmk
        @param user_id: 유저 id
        @param category: category_id 들어있는 list 
        @return: result( list )
        """
        result = []
        try:
            arrSite = TSearchMySite.objects.filter(user_id=user_id,
                                                   lang_id=lang_id,
                                                   category_id=category_id,
                                                   status=2)\
                                            .extra(select={'site_name' : 'site'})\
                                            .values().order_by('site_name')
            result = list( arrSite )
        except Exception as e:
            print '@@@@@get_restore_site_list : ', str(e)
            result = []
            result.append(str(e))
        finally:
            return result

    @staticmethod
    def restore_site(user_id, lang_id, category_id, site_id_list):
        """
        @summary: 삭제한 사이트 복구
        @author: hsrjmk
        @param user_id: 유저 id
        @param lang_id: language table 참조키
        @param category_id: category id
        @param site_id_list: 복원할 사이트 id list
        @return: 1( success ), 0( fail )
        """
        result = 1
        try:
            TSearchMySite.objects.filter(user_id=user_id,
                                         lang_id=lang_id,
                                         category_id=category_id,
                                         site_id__in=site_id_list).update(status=3)
        except Exception as e:
            print '@@@@@restore_site : ', str(e)
            result = 0
        finally:
            return result

    @staticmethod
    def reset_category_site(user_id, lang_id):
        """
        @summary: 해당언어 카테고리정보, 사이트 정보 초기화
        @author: hsrjmk
        @param user_id: User id
        @param lang_id: Language id
        @param search_my_word_objs: TSearchMyWord model object
        @return: 1( success ), 0( fail )
        """
        result = 1
        try:
            TSearchMySite.objects.filter(user_id=user_id,
                                         lang_id=lang_id).delete()
            TSearchMyCategory.objects.filter(user_id=user_id,
                                             lang_id=lang_id).delete()
        except Exception as e:
            print '@@@@@reset_category_site : ', str(e)
            result = 0
        finally:
            return result

    #########################################################################################
    #    TellpinSearch App API
    #########################################################################################
    @staticmethod
    def set_last_language(user_id, lang_id=2):
        """
        @summary: Tellpin Search App 마지막 언어 설정
        @author: hsrjmk
        @param user_id: User id
        @param lang_id: LanguageInfo id
        @return result: 
        """
        result = {}
        result['isSuccess'] = True
        try:
            my_last_lang_obj, created = TSearchMyLastLang.objects.update_or_create(user_id=user_id,
                                                                                   defaults={'lang_id': lang_id})
            obj = {}
            obj['user_id'] = my_last_lang_obj.user_id
            obj['language_set'] = my_last_lang_obj.lang_id
            obj['created'] = created
            result['data'] = obj
        except Exception as e:
            result['isSuccess'] = False
            print '@@@@@set_last_language : ', str(e)

        return result

    @staticmethod
    def get_last_language(user_id):
        """
        @summary: Tellpin Search App 마지막 언어 정보
        @author: hsrjmk
        @param user_id: User id
        @return result: obj ( Object )
        """
        result = {}
        result['isSuccess'] = True
        try:
            my_last_lang_obj, created = TSearchMyLastLang.objects.get_or_create(user_id=user_id,
                                                                                defaults={'lang_id': 2})
            obj = {}
            obj['user_id'] = my_last_lang_obj.user_id
            obj['lang_id'] = my_last_lang_obj.lang_id
            result['data'] = obj
        except Exception as e:
            result['isSuccess'] = False
            print '@@@@@get_last_language : ', str(e)

        return result

    @staticmethod
    def set_category_site(user_id, lang_id, obj):
        """
        @summary: Tellpin Search App 언어 별 마지막 설정 저장(카테고리, 사이트)
        @author: hsrjmk
        @param user_id: User id
        @return result: 
        """
        result = {}
        result['isSuccess'] = True

        try:
            last_category_site_objs, created = TSearchMyLastCategorySite.objects.update_or_create(
                                                    user_id=user_id,
                                                    lang_id=lang_id,
                                                    defaults={
                                                        'category1_id': obj['category_set1'],
                                                        'category2_id': obj['category_set2'],
                                                        'category3_id': obj['category_set3'],
                                                        'site1_id': obj['site_set1'],
                                                        'site2_id': obj['site_set2'],
                                                        'site3_id': obj['site_set3'],
                                                        'use_window1': obj['use_wnd1'],
                                                        'use_window2': obj['use_wnd2'],
                                                        'use_window3': obj['use_wnd3'],
                                                        'pin': obj['pin_number']
                                                    })
            obj = {}
            obj['user_id'] = last_category_site_objs.user_id
            obj['language_set'] = last_category_site_objs.lang_id
            obj['category_set1'] = last_category_site_objs.category1_id
            obj['category_set2'] = last_category_site_objs.category2_id
            obj['category_set3'] = last_category_site_objs.category3_id
            obj['site_set1'] = last_category_site_objs.site1_id
            obj['site_set2'] = last_category_site_objs.site2_id
            obj['site_set3'] = last_category_site_objs.site3_id
            obj['use_wnd1'] = last_category_site_objs.use_window1
            obj['use_wnd2'] = last_category_site_objs.use_window2
            obj['use_wnd3'] = last_category_site_objs.use_window3
            obj['pin_number'] = last_category_site_objs.pin
            obj['created'] = created
            result['data'] = obj
        except Exception as e:
            result['isSuccess'] = False
            print '@@@@@set_category_site : ', str(e)

        return result

    @staticmethod
    def get_category_site(user_id, lang_id):
        """
        @summary: Tellpin Search App 언어 별 마지막 설정 조회(카테고리, 사이트)
        @author: hsrjmk
        @param user_id: User id
        @return result: obj ( Object )
        """
        result = {}
        result['isSuccess'] = True
        try:
            last_category_site_objs = TSearchMyLastCategorySite.objects.get(user_id=user_id, lang_id=lang_id)
            obj = {}
            obj['user_id'] = last_category_site_objs.user_id
            obj['language_set'] = last_category_site_objs.lang_id
            obj['category_set1'] = last_category_site_objs.category1_id
            obj['category_set2'] = last_category_site_objs.category2_id
            obj['category_set3'] = last_category_site_objs.category3_id
            obj['site_set1'] = last_category_site_objs.site1_id
            obj['site_set2'] = last_category_site_objs.site2_id
            obj['site_set3'] = last_category_site_objs.site3_id
            obj['use_wnd1'] = last_category_site_objs.use_window1
            obj['use_wnd2'] = last_category_site_objs.use_window2
            obj['use_wnd3'] = last_category_site_objs.use_window3
            obj['pin_number'] = last_category_site_objs.pin
            result['data'] = obj
        except Exception as e:
            result['isSuccess'] = False
            print '@@@@@get_category_site : ', str(e)

        return result

    @staticmethod
    def get_last_category_site(user_id, lang_id):
        """
        @summary: Tellpin Search App 마지막 언어의 설정 조회(카테고리, 사이트)
        @author: hsrjmk
        @param user_id: User id
        @return result: obj ( Object )
        """
        result = {}
        result['isSuccess'] = True
        try:
            my_last_lang_obj = TSearchMyLastLang.objects.get(user_id=user_id, lang_id=lang_id)
            if my_last_lang_obj:
                result = ToolsService.get_category_site(user_id, lang_id)
        except Exception as e:
            result['isSuccess'] = False
            print '@@@@@get_last_category_site : ', str(e)

        return result

    @staticmethod
    def get_site_by_category(user_id):
        """
        @summary: Tellpin Search App 카테고리에 따른 사이트 정보
        @author: hsrjmk
        @param user_id: User id
        @return result: 
        """
        result = {}
        result['isSuccess'] = True

        try:
            exclude_category_id_list = TSearchMyCategory.objects.filter(user_id=user_id, status=2).values_list('category_id')
            exclude_site_id_list = TSearchMySite.objects.filter(user_id=user_id, status=2).values_list('site_id')

            if TutorService.is_tutor(user_id):
                user_info = Tutor.objects.get(user_id=user_id)
            else:
                user_info = Tutee.objects.get(user_id=user_id)
            lang_list = list()
            try:
                for i in range(1, 3):
                    if getattr(user_info, 'native' + str(i) + '_id'):
                        speak_dict = dict()
                        speak_dict = getattr(user_info, 'native' + str(i) + '_id_id')
                        lang_list.append(speak_dict)
                for i in range(1, 6):
                    if getattr(user_info, 'lang' + str(i) + '_id'):
                        speak_dict = dict()
                        speak_dict = getattr(user_info, 'lang' + str(i) + '_id_id')
                        lang_list.append(speak_dict)
                if 2 in lang_list:
                    pass
                else:
                    lang_list.append(2)

            except Exception as e:
                result['isSuccess'] = False
                print '@@@@@get_site_by_category_lang_list : ', str(e)

#             lang_list.insert(0, 0)
            print lang_list
            site_by_category_objs = TSearchSiteByCategoryInfo.objects.filter(in_lang_id__in=lang_list)\
                                                                    .exclude(category_id__in=exclude_category_id_list)\
                                                                    .exclude(id__in=exclude_site_id_list)\
                                                                    .select_related('category')\
                                                                    .order_by('in_lang_id', '-category__is_dic', 'category__category_english', 'site')

            default_site_list = list()

            for site_by_category_obj in site_by_category_objs:
                obj = {}
                obj['language_id'] = site_by_category_obj.in_lang_id_id
                obj['category_id'] = site_by_category_obj.category_id
                obj['category_name'] = site_by_category_obj.category.category_english
                obj['dictionary'] = site_by_category_obj.category.is_dic
                obj['site_id'] = site_by_category_obj.id
                obj['site_name'] = site_by_category_obj.site
#                 print type(obj['site_name'])
#                 print obj['site_name']
                obj['search_url'] = site_by_category_obj.search_url
                obj['site_url'] = site_by_category_obj.site_url
                default_site_list.append(obj)

            site_by_category_objs_none_lang = TSearchSiteByCategoryInfo.objects.filter(in_lang_id=0)\
                                                                    .exclude(category_id__in=exclude_category_id_list)\
                                                                    .exclude(id__in=exclude_site_id_list)\
                                                                    .select_related('category')\
                                                                    .order_by('in_lang_id', '-category__is_dic', 'category__category_english', 'site')
            for index, lan in enumerate(lang_list):
                for site_lang_none in site_by_category_objs_none_lang:
                    obj = {}
                    obj['language_id'] = lan
                    obj['category_id'] = site_lang_none.category_id
                    obj['category_name'] = site_lang_none.category.category_english
                    obj['dictionary'] = site_lang_none.category.is_dic
                    obj['site_id'] = site_lang_none.id
                    obj['site_name'] = site_lang_none.site
                    obj['search_url'] = site_lang_none.search_url
                    obj['site_url'] = site_lang_none.site_url
                    default_site_list.append(obj)

            my_site_objs1 = TSearchMySite.objects.filter(user_id=user_id, category_id__lte=10000, site_id__gt=10000, status__in=[1, 3])\
                                                .extra(tables=["tsearch_my_site", "tsearch_category_info"],
                                                       select={"category_name": "tsearch_category_info.category_english", "is_dic": "tsearch_category_info.is_dic"},
                                                       where=["tsearch_my_site.category_id=tsearch_category_info.id"])
            my_site_objs2 = TSearchMySite.objects.filter(user_id=user_id, category_id__gt=10000, site_id__gt=10000, status__in=[1, 3])\
                                                .extra(tables=["tsearch_my_site", "tsearch_my_category"],
                                                       select={"category_name": "tsearch_my_category.category", "is_dic": False},
                                                       where=["tsearch_my_site.category_id=tsearch_my_category.category_id"])
            my_site_list = list()
            for my_site_obj in my_site_objs1:
                obj = {}
                obj['language_id'] = my_site_obj.lang_id
                obj['category_id'] = my_site_obj.category_id
                obj['category_name'] = my_site_obj.category_name
                obj['dictionary'] = my_site_obj.is_dic
                obj['site_id'] = my_site_obj.site_id
                obj['site_name'] = my_site_obj.site
                obj['search_url'] = my_site_obj.search_url
                obj['site_url'] = my_site_obj.site_url
                my_site_list.append(obj)
            for my_site_obj in my_site_objs2:
                obj = {}
                obj['language_id'] = my_site_obj.lang_id
                obj['category_id'] = my_site_obj.category_id
                obj['category_name'] = my_site_obj.category_name
                obj['dictionary'] = my_site_obj.is_dic
                obj['site_id'] = my_site_obj.site_id
                obj['site_name'] = my_site_obj.site
                obj['search_url'] = my_site_obj.search_url
                obj['site_url'] = my_site_obj.site_url
                my_site_list.append(obj)

            site_list = default_site_list + my_site_list
            site_list = sorted(site_list, key=itemgetter('language_id', 'category_id', 'site_id'))
            result['data'] = site_list
        except Exception as e:
            result['isSuccess'] = False
            print '@@@@@get_site_by_category : ', str(e)

        return result

    @staticmethod
    def get_default_site_by_category(user_id):
        """
        @summary: Tellpin Search App 카테고리에 따른 사이트 정보
        @author: hsrjmk
        @param user_id: User id
        @return result:
        """
        result = {}
        result['isSuccess'] = True
        try:
            if TutorService.is_tutor(user_id):
                user_info = Tutor.objects.get(user_id=user_id)
            else:
                user_info = Tutee.objects.get(user_id=user_id)
            lang_list = list()

            try:
                for i in range(1, 3):
                    if getattr(user_info, 'native' + str(i) + '_id'):
                        speak_dict = dict()
                        speak_dict = getattr(user_info, 'native' + str(i) + '_id_id')
                        lang_list.append(speak_dict)
                for i in range(1, 6):
                    if getattr(user_info, 'lang' + str(i) + '_id'):
                        speak_dict = dict()
                        speak_dict = getattr(user_info, 'lang' + str(i) + '_id_id')
                        lang_list.append(speak_dict)
                if 2 in lang_list:
                    pass
                else:
                    lang_list.append(2)
                lang_list.insert(0, 0)
                print lang_list
            except Exception as e:
                result['isSuccess'] = False
                print '@@@@@get_default_site_by_category_lang_list : ', str(e)

            category_site_objs = TSearchSiteByCategoryInfo.objects.filter(is_default=True, in_lang_id__in=lang_list)
            default_category_site_list = []
            for category_site_obj in category_site_objs:
                obj = {}
                obj['category_id'] = category_site_obj.category_id
                obj['site_id'] = category_site_obj.id
                default_category_site_list.append(obj)
            result['data'] = default_category_site_list
        except Exception as e:
            result['isSuccess'] = False
            print '@@@@@get_default_site_by_category : ', str(e)

        return result

    @staticmethod
    def get_word_list(user_id, limit=5):
        """
        @summary: Tellpin Search App 검색 단어 목록 조회
        @author: hsrjmk
        @param user_id: User id
        @return result: list
        """
        result = {}
        word_list = []
        result['isSuccess'] = True
        try:
            word_objs = TSearchMyWord.objects.filter(user_id=user_id).order_by('-id').values('word')[:limit]

            for word_obj in word_objs:
                word_list.append(word_obj)
            result['data'] = word_list
        except Exception as e:
            result['isSuccess'] = False
            print '@@@@@get_word_list : ', str(e)

        return result

    @staticmethod
    def add_word(user_id, word):
        """
        @summary: Tellpin Search App 카테고리에 따른 사이트 정보
        @author: hsrjmk
        @param user_id: User id
        @return result: 
        """
        result = {}
        result['isSuccess'] = True
        word_obj = TSearchMyWord()
        word_obj.word
        word_obj.save()
        return result


    #########################################################################################
    #    Method
    #########################################################################################
    @staticmethod
    def make_search_my_word_list(search_my_word_objs, utc_delta=0):
        """
        @summary: 검색한 단어 리스트 만들기
        @author: hsrjmk
        @param search_my_word_objs: TSearchMyWord model object
        @return result: My word history list
        """
        result = []
        for search_my_word in search_my_word_objs:
            li = {} # List item
            li['word'] = search_my_word.word
            li['word_id'] = search_my_word.id
#             li['image'] = search_my_word.image
            li['user_id'] = search_my_word.user_id
#             li['title'] = search_my_word.title
#             li['target_url'] = search_my_word.url
            li['created_time'] = (search_my_word.created_time + timedelta(hours=utc_delta)).strftime('%Y-%m-%d %H:%M:%S')

            result.append(li)
        return result

    @staticmethod
    def make_my_pin_list(my_pin_objs, utc_delta=0):
        """
        @summary: 검색한 단어 리스트 만들기
        @author: hsrjmk
        @param my_pin_objs: TSearchMyPin model object
        @return result: My pin list
        """
        result = []
        for my_pin in my_pin_objs:
            li = {} # List item
#             li['id'] = my_pin.id
            li['word'] = my_pin.word
            li['word_id'] = my_pin.word_id.id
            li['user_id'] = my_pin.user_id
            li['created_time'] = (my_pin.created_time + timedelta(hours=utc_delta)).strftime('%Y-%m-%d %H:%M:%S')

            result.append(li)
        return result