# -*- coding: utf-8 -*-
###############################################################################
# filename    : tools > models.py
# description : Tools 모델 정의 (Tellpin Search Windows App 관련 모델)
# author      : hsrjmk@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160923 hsrjmk 최조 작성
#
###############################################################################
from django.db import models
from common.models import LanguageInfo
from user_auth.models import TellpinUser


STATUS_TYPE = (
    (1, 'Added'),
    (2, 'Deleted'),
    (3, 'Restored'),
)
PIN = (
    (0, 'None'),
    (1, 'Pin 1'),
    (2, 'Pin 2'),
    (3, 'Pin 3'),
)


class TSearchCategoryByLangInfo(models.Model):
    lang = models.ForeignKey(LanguageInfo)
    category = models.ForeignKey('TSearchCategoryInfo')

    class Meta:
        db_table = u'tsearch_category_by_lang_info'


class TSearchCategoryInfo(models.Model):
    category_english = models.CharField("Category in English", max_length=100, null=True)
    category_korean = models.CharField("Category in Korean", max_length=100, null=True)
    category_chinese = models.CharField("Category in Chinese", max_length=100, null=True)
    category_spanish = models.CharField("Category in Spanish", max_length=100, null=True)

    is_dic = models.BooleanField("Whether dictionary is", default=False)

    class Meta:
        db_table = u'tsearch_category_info'


class TSearchSiteByCategoryInfo(models.Model):
    in_lang_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='in_lang_id', null=True)
    in_lang = models.CharField("Input language", max_length=100, null=True)
    out_lang_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='out_lang_id', null=True)
    out_lang = models.CharField("", max_length=100, null=True)
    category = models.ForeignKey('TSearchCategoryInfo')
    site = models.CharField("Site name", max_length=100)
    site_url = models.CharField("Site URL", max_length=300)
    search_url = models.CharField("Site URL for searching", max_length=300, null=True)
    is_dic = models.BooleanField("Whether dictionary is", default=False)
    is_default = models.BooleanField("Whether default is", default=False)
    support_mobile = models.BooleanField("Whether support mobile", default=False)

    class Meta:
        db_table = u'tsearch_site_by_category_info'


class TSearchMyCategory(models.Model):
    user = models.ForeignKey(TellpinUser)
    lang = models.ForeignKey(LanguageInfo)
    category_id = models.IntegerField("Category id: TSearchCategoryInfo or new generated id for TSearchMyCategory - start 10001")
    category = models.CharField("Category name", max_length=100)
    status = models.PositiveSmallIntegerField("Category status in user account", choices=STATUS_TYPE, default=1)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    class Meta:
        db_table = u'tsearch_my_category'


class TSearchMySite(models.Model):
    user = models.ForeignKey(TellpinUser)
    lang = models.ForeignKey(LanguageInfo)
    category_id = models.IntegerField("Category id: TSearchCategoryInfo or category_id in TSearchMyCategory")
    site_id = models.IntegerField('Site id: TSearchSiteByCategoryInfo or new generated id for TSearchMySite - start 10001')
    site = models.CharField("Site name", max_length=100)
    site_url = models.CharField("Site URL", max_length=300)
    search_url = models.CharField("Site URL for searching", max_length=300, null=True)
    status = models.PositiveSmallIntegerField("Site status in user account", choices=STATUS_TYPE, default=1)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    class Meta:
        db_table = u'tsearch_my_site'


class TSearchMyWord(models.Model):
    user = models.ForeignKey(TellpinUser)

    word = models.CharField("Search word", max_length=2048)
    image = models.CharField("Representative image url", max_length=2048, null=True)
    url = models.CharField("Searched URL", max_length=2048, null=True)
    title = models.CharField("Searched site title", max_length=2048, null=True)

    lang = models.ForeignKey(LanguageInfo)
    category1_id = models.IntegerField('Category id: TSearchCategoryInfo or new generated id for TSearchMySite - start 10001')
    site1_id = models.IntegerField('Site id: TSearchSiteByCategoryInfo or new generated id for TSearchMySite - start 10001')
    category2_id = models.IntegerField('Category id: TSearchCategoryInfo or new generated id for TSearchMySite - start 10001')
    site2_id = models.IntegerField('Site id: TSearchSiteByCategoryInfo or new generated id for TSearchMySite - start 10001')
    category3_id = models.IntegerField('Category id: TSearchCategoryInfo or new generated id for TSearchMySite - start 10001')
    site3_id = models.IntegerField('Site id: TSearchSiteByCategoryInfo or new generated id for TSearchMySite - start 10001')

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'tsearch_my_word'


class TSearchMyPin(models.Model):
    user = models.ForeignKey(TellpinUser)

    word_id = models.ForeignKey(TSearchMyWord, db_column='word_id')
    pin = models.PositiveSmallIntegerField("Pin index", choices=PIN, default=1)
    word = models.CharField("Search word", max_length=2048)

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'tsearch_my_pin'


class TSearchMyPinName(models.Model):
    user = models.ForeignKey(TellpinUser)

    pin1 = models.CharField("Alias pin 1", max_length=20, null=True)
    pin2 = models.CharField("Alias pin 2", max_length=20, null=True)
    pin3 = models.CharField("Alias pin 3", max_length=20, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    class Meta:
        db_table = u'tsearch_my_pin_name'


class TSearchMyLastLang(models.Model):
    user = models.ForeignKey(TellpinUser)

    lang = models.ForeignKey(LanguageInfo)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    class Meta:
        db_table = u'tsearch_my_last_lang'


class TSearchMyLastCategorySite(models.Model):
    user = models.ForeignKey(TellpinUser)

    lang = models.ForeignKey(LanguageInfo)
    category1_id = models.IntegerField("Category id for 1st browser", null=True)
    category2_id = models.IntegerField("Category id for 2nd browser", null=True)
    category3_id = models.IntegerField("Category id for 3rd browser", null=True)
    site1_id = models.IntegerField("Site id for 1st browser", null=True)
    site2_id = models.IntegerField("Site id for 2nd browser", null=True)
    site3_id = models.IntegerField("Site id for 3rd browser", null=True)
    use_window1 = models.BooleanField("Whether use 1st browser", default=True)
    use_window2 = models.BooleanField("Whether use 2nd browser", default=True)
    use_window3 = models.BooleanField("Whether use 3rd browser", default=True)
    pin = models.PositiveSmallIntegerField("Pin index where save search word", default=1)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    class Meta:
        db_table = u'tsearch_my_last_category_site'
