# -*- coding: utf-8 -*-
###############################################################################
# filename    : tutor > views.py
# description : tutor views ( find tutor )
# author      : khyun@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 khyun 최초 작성
#
#
###############################################################################
import json
import logging
import time
from datetime import datetime, timedelta

from django.core.serializers.json import DjangoJSONEncoder
from django.http.response import HttpResponse, JsonResponse, \
    HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt

from common import common
from common.common import tellpin_login_required
from common.service import CommonService, MessageService
from community.service import Community
from dashboard.service import DashboardService
from mypage.service import MyPageService
# from common.models import Problem, Profile, Resume, Files
from tutor.service import TutorApplyService, TutorService
from tutor.service4registration import TutorService4Registration
from tutoring import NewLessonService
from tutor.models import TutorDoc, Tutor, Tutee

# service class
from tutoring.service import ScheduleService, CourseService, ReservationService
from user_auth.service import UserAuthService
from wallet.service import WalletService

ts4reg = TutorService4Registration(object)
mypage_service = MyPageService(object)
ts4r = TutorService4Registration(object)
logger = logging.getLogger('tplogger')


###############################################################################
# tutor 상세 페이지 호출
#  
# url : tutor/tutorinfo/(?P<t_id>\d+)/$
# respose : json 
###############################################################################
def tutor_info(request, tutor_id):
    """
    @summary: get tutor information
    @author: khyun
    @param request
    @param tutor_id : tutor id
    @return: url - tutor/tutor_info/tutor_info.html
    """
    # resoponse json
    resData = dict()
    resData["isSuccess"] = 1

    # if request.session.get('email', False):
    #     # ('email' 라는 key 가 있다면 value 를 return 하고, 아니면 False 를 return 합니다.)
    #     user_id = request.user.id
    # else:
    #     user_id = None

    user_id = request.user.id
    user_utc = request.session.get('utc_delta', 0)

    tutor_detail = TutorService.get_tutor_detail(tutor_id, user_id, user_utc)
    cur_page = 1
    # 기본 정보
    resData["profile"] = tutor_detail['profile']
    # 이력서
    resData["resume_list"] = tutor_detail['resume_list']
    # statistics
    resData["statistics"] = tutor_detail['statistics']
    # feedback
    resData['feedback'] = tutor_detail['feedback']
    # lesson list
    resData["lesson_list"] = CourseService.course_list(tutor_id)
    # # community activities
    # resData['page'] = 0
    # 자기 자신 확인
    # trial lesson
    # community activities
    resData["LBs"], resData["LBShowMore"] = DashboardService.get_language_board(cur_page, tutor_id, 5)
    resData["TPs"], resData["TPShowMore"] = DashboardService.get_translation_proofreading(cur_page, tutor_id, 5)
    resData["LQs"], resData["LQShowMore"] = DashboardService.get_language_question(cur_page, tutor_id, 5)
    resData["all"], resData["AllShowMore"] = DashboardService.get_all_community(cur_page, tutor_id, 5)
    resData['page'] = cur_page
    resData["mypage"] = int(tutor_id) == int(user_id or 0)
    jsonData = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("tutor/tutor_info/tutor_info.html",
                              {'data': jsonData, 'title': resData['profile']['name']}, RequestContext(request))


###############################################################################
# tutor 검색, 리스트 페이지 호출
#  
# url : tutor/findtutor/
# respose data: json 
###############################################################################
def tutor_finder(request):
    """
    @summary: tutor finder
    @author: khyun
    @param request
    @return: url - tutor/tutor_find.html
    """
    # resoponse json
    resData = dict()
    resData["isSuccess"] = 1
    resData["search_data"] = TutorService.get_search_data()
    # tutor list
    # resData["tutor_list"], resData["page_info"] =  ts.listup_tutors(user_id)
    jsonData = json.dumps(resData)
    logger.info('test-----------------')

    return render(request, "tutor/tutor_find.html", {'data': jsonData})


###############################################################################
# tutor 검색 필터 처리
#  
# url : tutor/search_tutor/
# respose : json 
###############################################################################
@csrf_exempt
def tutor_finder_search(request):
    """
    @summary: tutor list ( filtering )
    @author: khyun
    @param request
    @return: json
    """
    # resoponse json
    resData = dict()
    resData["isSuccess"] = 1

    if request.user.id:
        user_id = request.user.id
        utc_time = request.session['utc_delta']
    else:
        user_id = None
        utc_time = 0

    # get request parameter
    post_dict = json.loads(request.body)
    tutor_id_list = TutorService.filter_listup_tutor(utc_time, post_dict)
    resData["tutor_list"], resData["page_info"] = TutorService.listup_tutor(user_id,
                                                                            tutor_id_list,
                                                                            post_dict['page'])

    return JsonResponse(resData)


###############################################################################
# 강의 예약 페이지 호출
#  강의 선택
# url : tutor/reservation/which/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def reservation_which(request):
    """
    @summary: call reservation which page function
    @author: khyun
    @param request
    @return: url - tutor/tutor_reservation/tutor_reservation_which.html
    """
    # resoponse json
    resData = {
        'isSuccess': 1
    }
    user_id = request.user.id

    # get parameter
    if request.method == 'GET':
        qd = request.GET
    elif request.method == 'POST':
        qd = request.POST

    if request.session.get('reservation', False):
        if 'lid' in request.session['reservation']:
            resData["selected_lid"] = request.session['reservation']['lid']

    b_checked, data = CommonService.check_post_data(qd, "tid")

    if not b_checked:
        tutor_id = request.session['reservation']['tid']
    else:
        tutor_id = data

    if int(tutor_id) == int(user_id):
        return HttpResponseRedirect("/tutor/" + tutor_id + "/")

    b_checked, data = CommonService.check_post_data(qd, "accessType")

    if not b_checked:
        access_type = request.session['reservation']['accessType']
    else:
        access_type = data

    # 예약정보 세션에 저장
    request.session['reservation'] = {
        "tid": tutor_id,
        "accessType": access_type
    }

    # lessons
    resData["tutor"] = TutorService.get_tutor_info(tutor_id)
    resData["course"] = CourseService.course_list(tutor_id)
    # resData["lesson"] = ts.get_reserv_all_lesson(tutor_id, access_type)
    jsonData = json.dumps(resData, default=date_handler)
    return render_to_response("tutor/tutor_reservation/tutor_reservation_which.html",
                              {'data': jsonData, 'title': resData['tutor']['name']}, RequestContext(request))


###############################################################################
# 강의 예약 페이지 호출
#  기간(번들 예약) 선택
# url : tutor/reservation/dt/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def reservation_dt(request):
    """
    @summary: call reservation dt page function
    @author: khyun
    @param request
    @return: url - tutor/tutor_reservation/tutor_reservation_dt.html
    """
    # resoponse json
    resData = {
        'isSuccess': 1
    }
    user_id = request.user.id

    # get parameter
    if request.method == 'GET':
        qd = request.GET
    elif request.method == 'POST':
        qd = request.POST

    if 'reservation' in request.session:
        if 'bundle' in request.session['reservation']:
            resData["bundle"] = request.session['reservation']['bundle']
        if 'duration' in request.session['reservation']:
            resData["duration"] = request.session['reservation']['duration']

    # check accessType
    if "accessType" in qd:
        if int(qd["accessType"]) == 1:  # lesson choice root entered
            # 예약정보 세션에 저장
            if "reservation" in request.session:
                request.session['reservation']["tid"] = qd["tid"]
                request.session['reservation']["accessType"] = qd["accessType"]
            else:
                request.session['reservation'] = {
                    "tid": qd["tid"],
                    "accessType": qd["accessType"]
                }

    if "reservation" in request.session:
        t_id = request.session['reservation']["tid"]
    else:
        resData["isSuccess"] = 0
        resData['message'] = "no reservation session key"
        jsonData = json.dumps(resData)

        return HttpResponse(jsonData, content_type='application/json')

    if int(t_id) == int(user_id):
        return HttpResponseRedirect("/tutor/" + t_id + "/")

    b_checked, data = CommonService.check_post_data(qd, "selected_lid")
    if not b_checked:
        if "reservation" in request.session:
            if "lid" in request.session['reservation']:
                selected_lid = request.session['reservation']['lid']
    else:
        selected_lid = data
        # 예약정보 세션에 저장
    request.session['reservation'] = {
        "tid": request.session['reservation']["tid"],
        "accessType": request.session['reservation']["accessType"],
        "lid": selected_lid
    }

    # lessons
    resData["tutor"] = TutorService.get_tutor_info(t_id)
    resData["course"] = CourseService.get_course(selected_lid)
    resData['user_currency'] = request.session.get('currency_exchange')
    jsonData = json.dumps(resData, default=date_handler)

    return render_to_response("tutor/tutor_reservation/tutor_reservation_dt.html",
                              {'data': jsonData, 'title': resData['tutor']['name']}, RequestContext(request))


###############################################################################
# 강의 예약 페이지 호출
#  방식 선택
# url : tutor/reservation/how/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def reservation_how(request):
    """
    @summary: call reservation how page function
    @author: khyun
    @param request
    @return: url - tutor/tutor_reservation/tutor_reservation_how.html
    """
    # resoponse json
    resData = {
        "isSuccess": 1
    }
    user_id = request.user.id
    print 'reservation_how()'

    # check accessType 추가 스케쥴 예약일 경우
    if "reservation" in request.session:
        if "accessType" in request.session['reservation']:
            if int(request.session['reservation']['accessType']) == 3: # reschedule
                t_id = request.session['reservation']["tid"]
                print type(t_id)
                if int(t_id) == int(user_id):
                    return HttpResponseRedirect("/tutor/" + str(t_id) + "/")
                request.session['reservation']['is_reschedule'] = 1
                resData["purchase"] = True
                resData["tutor"] = TutorService.get_tutor_info(t_id)
                resData["tools"] = TutorService.get_tutor_tools(t_id, resData['tutor'])
                jsonData = json.dumps(resData, default=date_handler)

                return render_to_response("tutor/tutor_reservation/tutor_reservation_how.html", {"data": jsonData},
                                          RequestContext(request))

    if request.method == 'GET':
        qd = request.GET
    elif request.method == 'POST':
        qd = request.POST

    print 'qd'
    print qd
    # check accessType
    if "accessType" in qd:
        if int(qd["accessType"]) == 2:  # Trial root entered
            # 예약정보 세션에 저장
            if "reservation" in request.session:
                request.session['reservation']["tid"] = qd["tid"]
                request.session['reservation']["accessType"] = qd["accessType"]
            else:
                request.session['reservation'] = {
                    "tid": qd["tid"],
                    "accessType": qd["accessType"]
                }

            return reservation_how_trial(request, user_id, qd["tid"])

    if "reservation" in request.session:
        t_id = request.session['reservation']["tid"]
    else:
        resData["isSuccess"] = 0
        resData['message'] = "no reservation session key"
        jsonData = json.dumps(resData)

        return HttpResponse(jsonData, content_type='application/json')

    b_checked, data = CommonService.check_post_data(qd, "bundle")

    if not b_checked:
        if "reservation" in request.session:
            if "bundle" in request.session['reservation']:
                bundle = request.session['reservation']['bundle']
    else:
        bundle = data

    b_checked, data = CommonService.check_post_data(qd, "duration")

    if not b_checked:
        if "reservation" in request.session:
            if "duration" in request.session['reservation']:
                duration = request.session['reservation']['duration']
    else:
        duration = data

    if "reservation" in request.session:
        if "message" in request.session['reservation']:
            resData["message"] = request.session['reservation']['message']
            print request.session['reservation']['message']
        if "tool" in request.session['reservation']:
            resData["tool"] = request.session['reservation']['tool']
            print request.session['reservation']['tool']
        if "r_tool_info" in request.session['reservation']:
            resData["r_tool_info"] = request.session['reservation']['r_tool_info']
            print request.session['reservation']['r_tool_info']

    # 예약정보 세션에 저장
    request.session['reservation'] = {
        "tid": request.session['reservation']["tid"],
        "accessType": request.session['reservation']["accessType"],
        "lid": request.session['reservation']["lid"],
        "bundle": bundle,
        "duration": duration
    }
    resData["purchase"] = True
    resData["tutor"] = TutorService.get_tutor_info(t_id)
    resData["tools"] = TutorService.get_tutor_tools(t_id, resData['tutor'])
    jsonData = json.dumps(resData, cls=DjangoJSONEncoder, default=date_handler)

    return render_to_response("tutor/tutor_reservation/tutor_reservation_how.html",
                              {"data": jsonData, 'title': resData['tutor']['name']}, RequestContext(request))


###############################################################################
# 강의 예약 페이지 호출
#  방식 선택
# url : reservation_how()
# respose : json 
###############################################################################
@csrf_exempt
def reservation_how_trial(request, user_id, t_id):
    """
    @summary: reservation how trial
    @author: khyun
    @param request
    @param user_id : user id
    @param t_id : tutor id
    @return: url - tutor/tutor_reservation/tutor_reservation_how.html
    """
    # resoponse json
    resData = {
        "isSuccess": 1
    }

    if int(t_id) == int(user_id):
        return HttpResponseRedirect("/tutor/" + t_id + "/")

    trial_lesson = CourseService.get_trial(t_id)

    if not trial_lesson['has_trial']:
        url = "/tutor/" + str(t_id) + "/"
        return HttpResponseRedirect(url)

    # 예약정보 세션에 저장
    request.session['reservation'] = {
        "tid": t_id,
        "accessType": "2",
        "lid": None,
        "bundle": 1,
        "duration": 30
    }

    # resData["purchase"] = ts.check_coin(user_id, t_id)
    resData["purchase"] = True
    resData["tutor"] = TutorService.get_tutor_info(t_id)
    resData['tools'] = TutorService.get_tutor_tools(t_id, resData['tutor'])
    jsonData = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("tutor/tutor_reservation/tutor_reservation_how.html", {"data": jsonData},
                              RequestContext(request))


###############################################################################
# 강의 예약 페이지 호출
#  시간 선택
# url : tutor/reservation/when/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def reservation_when(request):
    """
    @summary: call reservation when page
    @author: khyun
    @param request
    @return: url - tutor/tutor_reservation/tutor_reservation_when.html
    """
    from datetime import datetime, timedelta
    # resoponse json
    resData = {
        'isSuccess': 1
    }
    user_utc = request.session["utc_time"]
    user_utc_delta = request.session['utc_delta']
    print 'request when()!!'
    print request.session['reservation']

    # get session data
    if "reservation" in request.session:
        t_id = request.session['reservation']["tid"]
    else:
        resData["isSuccess"] = 0
        resData['message'] = "no reservation session key"
        jsonData = json.dumps(resData)
        return HttpResponse(jsonData, content_type='application/json')

    # get request parameter
    if request.method == 'GET':
        qd = request.GET
    elif request.method == 'POST':
        qd = request.POST

    b_checked, data = CommonService.check_post_data(qd, "message", False)
    if not b_checked:
        if "reservation" in request.session:
            if "message" in request.session['reservation']:
                r_message = request.session['reservation']['message']
    else:
        r_message = data

    if "reservation" in request.session:
        if "rv_time" in request.session['reservation']:
            resData["rv_time"] = request.session['reservation']['rv_time']
            print request.session['reservation']['rv_time']

    b_checked, data = CommonService.check_post_data(qd, "tool")
    if not b_checked:
        if "reservation" in request.session:
            if "tool" in request.session['reservation']:
                r_tool = request.session['reservation']['tool']
    else:
        r_tool = data

    b_checked, data = CommonService.check_post_data(qd, "tool_info")
    if not b_checked:
        if "reservation" in request.session:
            if "r_tool_info" in request.session['reservation']:
                print "55555555555"
                r_tool_info = request.session['reservation']['r_tool_info']
    else:
        print "7777777777"
        r_tool_info = data

    if "reservation" in request.session:
        print "11111111"
        if "rv_time" in request.session['reservation']:
            resData["rv_time"] = request.session['reservation']['rv_time']
            print "AAAAAAAAAAAAAAAAAAA"
            print request.session['reservation']['rv_time']

    # 예약정보 세션에 저장
    # check accessType 추가 스케쥴 예약일 경우
    if int(request.session['reservation']['accessType']) == 3:
        request.session['reservation'] = {
            "tid": request.session['reservation']["tid"],
            "accessType": request.session['reservation']["accessType"],
            "lid": request.session['reservation']["lid"],
            "bundle": request.session['reservation']["bundle"],
            "duration": request.session['reservation']["duration"],
            "message": r_message,
            "tool": r_tool,
            "r_tool_info": r_tool_info,
            "rvid": request.session['reservation']["rvid"],
            "command": request.session['reservation']["command"],
            "orders": request.session['reservation']["orders"]
        }
    else:
        request.session['reservation'] = {
            "tid": request.session['reservation']["tid"],
            "accessType": request.session['reservation']["accessType"],
            "lid": request.session['reservation']["lid"],
            "bundle": request.session['reservation']["bundle"],
            "duration": request.session['reservation']["duration"],
            "message": r_message,
            "tool": r_tool,
            "r_tool_info": r_tool_info
        }

    resData["tutor"] = TutorService.get_tutor_info(t_id)
    resData["course"] = CourseService.get_course(request.session['reservation']["lid"])
    now = datetime.utcnow() + timedelta(days=1)
    resData["today"] = now.strftime('%Y%m%d')
    resData["schedule"] = ScheduleService.get_tutor_schedule(t_id, user_utc_delta, now)
    resData["reserved"] = ScheduleService.get_reserved_schedule(t_id, user_utc_delta, now)
    resData["accessType"] = request.session['reservation']["accessType"]
    jsonData = json.dumps(resData, default=date_handler)

    return render_to_response("tutor/tutor_reservation/tutor_reservation_when.html",
                              {"data": jsonData, 'title': resData['tutor']['name']}, RequestContext(request))


###############################################################################
# 강의 시간 데이터 처리
#  다음 주 날짜 리스트
# url : 
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def reservation_when_next(request):
    """
    @summary: get reservation when next week
    @author: khyun
    @param request
    @return: json
    """
    from datetime import datetime, timedelta
    # resoponse json
    resData = {
        'isSuccess': 1
    }
    user_utc = request.session["utc_delta"]
    print 'reservation_when_next()!!'
    print request.session['reservation']

    # get session data
    if "reservation" in request.session:
        t_id = request.session['reservation']["tid"]
    else:
        resData["isSuccess"] = 0
        resData['message'] = "no reservation session key"
        jsonData = json.dumps(resData)

        return HttpResponse(jsonData, content_type='application/json')

    try:
        # get request parameter
        post_dict = json.loads(request.body)
        mark_date = datetime.strptime(post_dict["date"], "%Y%m%d").date()
        timegap = timedelta(days=7)  # 날짜 차이
        next_week = mark_date + timegap
        resData["schedule"] = ScheduleService.get_tutor_schedule(t_id, user_utc, next_week)
        resData["reserved"] = ScheduleService.get_reserved_schedule(t_id, user_utc, next_week)
    except Exception as err:
        print str(err)

    return JsonResponse(resData)


###############################################################################
# 강의 시간 데이터 처리
#  지난 주 날짜 리스트
# url : 
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def reservation_when_prev(request):
    """
    @summary: get reservation when prev week
    @author: khyun
    @param request
    @return: json
    """
    from datetime import datetime, timedelta
    # resoponse json
    resData = {
        'isSuccess': 1
    }
    user_utc = request.session["utc_delta"]

    # get session data
    if 'reservation' in request.session:
        t_id = request.session['reservation']["tid"]
    else:
        resData["isSuccess"] = 0
        resData['message'] = "no reservation session key"
        jsonData = json.dumps(resData)

        return HttpResponse(jsonData, content_type='application/json')

    # get request parameter
    post_dict = json.loads(request.body)
    mark_date = datetime.strptime(post_dict["date"], "%Y%m%d").date()
    timegap = timedelta(days=7)  # 날짜 차이
    next_week = mark_date - timegap
    resData["schedule"] = ScheduleService.get_tutor_schedule(t_id, user_utc, next_week)
    resData["reserved"] = ScheduleService.get_reserved_schedule(t_id, user_utc, next_week)

    return JsonResponse(resData)


###############################################################################
# 강의 시간 데이터 처리
#  다음 주 날짜 리스트
# url : 
# respose : json 
###############################################################################
@csrf_exempt
def get_schdule_next(request):
    """
    @summary: get schedule next week
    @author: khyun
    @param request
    @return: json
    """
    from datetime import datetime, timedelta
    # resoponse json
    resData = dict()
    resData["isSuccess"] = 1
    user_utc = request.session["utc_delta"]
    # get request parameter
    post_dict = json.loads(request.body)
    mark_date = datetime.strptime(post_dict["date"], "%Y%m%d").date()
    timegap = timedelta(days=7)  # 날짜 차이
    next_week = mark_date + timegap
    resData["schedule"] = ScheduleService.get_tutor_schedule(post_dict["tid"], user_utc, next_week)
    resData["reserved"] = ScheduleService.get_reserved_schedule(post_dict['tid'], user_utc, next_week)

    return JsonResponse(resData)


###############################################################################
# 강의 시간 데이터 처리
#  다음 주 날짜 리스트
# url : 
# respose : json 
###############################################################################
@csrf_exempt
def get_schdule_prev(request):
    """
    @summary: get schedule prev week
    @author: khyun
    @param request
    @return: json
    """
    from datetime import datetime, timedelta
    # resoponse json
    resData = dict()
    resData["isSuccess"] = 1
    user_utc = request.session["utc_delta"]
    # get request parameter
    post_dict = json.loads(request.body)
    mark_date = datetime.strptime(post_dict["date"], "%Y%m%d").date()
    timegap = timedelta(days=7)  # 날짜 차이
    next_week = mark_date - timegap
    resData["schedule"] = ScheduleService.get_tutor_schedule(post_dict["tid"], user_utc, next_week)
    resData["reserved"] = ScheduleService.get_reserved_schedule(post_dict["tid"], user_utc, next_week)

    return JsonResponse(resData)


###############################################################################
# 강의 시간 데이터 처리
#  다음 주 날짜 리스트
# url : 
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def tutor_schedule(request):
    """
    @summary: get tutor schedule
    @author: khyun
    @param request
    @return: json
    """
    # resoponse json
    print 'tutor_schedule()'
    resData = dict()
    resData["isSuccess"] = 1
    user_utc = request.session["utc_delta"]
    now = datetime.utcnow() + timedelta(days=1)
    # get request parameter
    post_dict = json.loads(request.body)

    resData["today"] = ScheduleService.get_year_month_day(user_utc)
    resData["schedule"] = ScheduleService.get_tutor_schedule(post_dict["tid"], user_utc, now)
    resData['reserved'] = ScheduleService.get_reserved_schedule(post_dict["tid"], user_utc, now)

    return JsonResponse(resData)


###############################################################################
# 강의 예약 페이지 호출
#  시간 선택
# url : tutor/reservation/confirm/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def reservation_confirm(request):
    """
    @summary: call reservation confirm page
    @author: khyun
    @param request
    @return: url - tutor/tutor_reservation/tutor_reservation_confirm.html
    """
    # resoponse json
    resData = {
        'isSuccess': 1
    }
    user_id = request.user.id
    print 'request!! confirm()!!'
    print request.session['reservation']

    # get session data
    if 'reservation' in request.session:
        t_id = request.session['reservation']["tid"]
    else:
        resData["isSuccess"] = 0
        resData['message'] = "no reservation session key"
        jsonData = json.dumps(resData)

        return HttpResponse(jsonData, content_type='application/json')

    # get request parameter
    if request.method == 'GET':
        qd = request.GET
    elif request.method == 'POST':
        qd = request.POST

    b_checked, data = CommonService.check_post_data(qd, "rv_time")
    if not b_checked:
        return data
    else:
        rv_time = data

    # 예약정보 세션에 저장
    # check accessType 추가 스케쥴 예약일 경우
    if int(request.session['reservation']['accessType']) == 3:
        request.session['reservation'] = {
            "tid": request.session['reservation']["tid"],
            "accessType": request.session['reservation']["accessType"],
            "lid": request.session['reservation']["lid"],
            "bundle": request.session['reservation']["bundle"],
            "duration": request.session['reservation']["duration"],
            "message": request.session['reservation']["message"],
            "tool": request.session['reservation']["tool"],
            "r_tool_info": request.session['reservation']["r_tool_info"],
            "rv_time": rv_time,
            "rvid": request.session['reservation']["rvid"],
            "command": request.session['reservation']["command"],
            "orders": request.session['reservation']["orders"]
        }
    else:
        request.session['reservation'] = {
            "tid": request.session['reservation']["tid"],
            "accessType": request.session['reservation']["accessType"],
            "lid": request.session['reservation']["lid"],
            "bundle": request.session['reservation']["bundle"],
            "duration": request.session['reservation']["duration"],
            "message": request.session['reservation']["message"],
            "tool": request.session['reservation']["tool"],
            "r_tool_info": request.session['reservation']["r_tool_info"],
            "rv_time": rv_time
        }

    # check accessType 추가 스케쥴 예약일 경우
    if "reservation" in request.session:
        if "accessType" in request.session['reservation']:
            if int(request.session['reservation']['accessType']) == 3:
                resData["tutor"] = TutorService.get_tutor_info(t_id)
                resData["course"] = CourseService.get_course(request.session['reservation']["lid"])
                resData["price"] = CourseService.get_course_price(
                    request.session['reservation']["lid"],
                    request.session['reservation']["bundle"],
                    request.session['reservation']["duration"]
                )
                # 2016-03-15 khyun
                resData["coin"] = request.user.tellpinuser.tutee.available_balance
                jsonData = json.dumps(resData, default=date_handler)

                return render_to_response("tutor/tutor_reservation/tutor_reservation_confirm.html", {"data": jsonData},
                                          RequestContext(request))

    resData["tutor"] = TutorService.get_tutor_info(t_id)
    resData["course"] = CourseService.get_course(request.session['reservation']["lid"])
    resData["price"] = CourseService.get_course_price(
        request.session['reservation']["lid"],
        request.session['reservation']["bundle"],
        request.session['reservation']["duration"]
    )
    # 2016-03-15 khyun
    resData["coin"] = request.user.tellpinuser.tutee.available_balance
    resData["rv_time"] = rv_time

    jsonData = json.dumps(resData, cls=DjangoJSONEncoder)
#     jsonData = json.dumps(resData, default=date_handler)

    return render_to_response("tutor/tutor_reservation/tutor_reservation_confirm.html",
                              {"data": jsonData, 'title': resData['tutor']['name']}, RequestContext(request))

def date_handler(obj):
        if hasattr(obj, 'isoformat'):
            return obj.isoformat()
        else:
            raise TypeError
@csrf_exempt
@tellpin_login_required
def reservation_complete(request):
    """
    @summary: call reservation complete page
    @author: khyun
    @param request
    @return: url - tutor/tutor_reservation/tutor_reservation_complete.html
    """
    # resoponse json
    resData = {
        'isSuccess': 1
    }
    user_id = request.user.id
    user_utc = request.session["utc_delta"]
    qd = None

    # get session data
    if "reservation" in request.session:
        t_id = request.session['reservation']["tid"]
    else:
        resData["isSuccess"] = 0
        resData['message'] = "no reservation session key"
        return HttpResponseRedirect("/tutor/")

    # get request parameter
    if request.method == 'GET':
        qd = request.GET
    elif request.method == 'POST':
        qd = request.POST

    b_checked, data = CommonService.check_post_data(qd, "password")
    if not b_checked:
        return data
    else:
        password = data

    # check password
    if not UserAuthService.check_password(request.user, password):
        resData["isSuccess"] = 0
        resData['message'] = "wrong password"
        resData["tutor"] = TutorService.get_tutor_info(t_id)
        resData["course"] = CourseService.get_course(request.session['reservation']["lid"])
        resData["price"] = CourseService.get_course_price(
            request.session['reservation']["lid"],
            request.session['reservation']["bundle"],
            request.session['reservation']["duration"]
        )
        # 2016-03-15 khyun
        # TODO
        # 코인 관련 처리
        # coin_data = ts.get_tutee_coin(user_id)
        # resData["coin"] = coin_data["balance"]
        resData['coin'] = request.user.tellpinuser.tutee.available_balance
        jsonData = json.dumps(resData, cls=DjangoJSONEncoder)

        return render_to_response("tutor/tutor_reservation/tutor_reservation_confirm.html", {"data": jsonData},
                                  RequestContext(request))

    resData["tutor"] = TutorService.get_tutor_info(t_id)

    # check accessType 추가 스케쥴 예약일 경우
    if "reservation" in request.session:
        print 'accessType ==' + str(request.session['reservation']['accessType'])
        if "accessType" in request.session['reservation']:
            if int(request.session['reservation']['accessType']) == 3:
                # 스케쥴 추가 예약!!
                update_lesson = ReservationService.update_reservation(user_utc, request.session['reservation'])
                ReservationService.set_status_history(update_lesson,
                                                      request.session['reservation']['command'])
            else:
                # 강의 예약!!
                first_rv = ReservationService.set_reservation(user_id, user_utc, request.session['reservation'])
                # 코인 계산!!!
                WalletService.buy_lesson(user_id, first_rv.lesson_id)
                ReservationService.set_status_history(first_rv, "Schedule")
                TutorService.set_lesson_notification(1, first_rv.tutee_id, first_rv.tutor_id, first_rv.id, 1)
    # session data expire
    access_type = request.session['reservation']["accessType"]
    del request.session['reservation']
    resData["access_type"] = access_type
    jsonData = json.dumps(resData, cls=DjangoJSONEncoder)
#     jsonData = json.dumps(resData, default=date_handler)

    return render_to_response("tutor/tutor_reservation/tutor_reservation_complete.html",
                              {"data": jsonData, 'title': resData['tutor']['name']}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def get_apply_status(request):
    """
    @summary: user apply status
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    resData["status"] = TutorApplyService.get_tutor_doc_status(user_id)
    print resData["status"]

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def registration_personal(request):
    """
    @summary: 튜터 지원서
    @author: msjang
    @param request
    @return: url - tutor/apply/personal.html
    """
    # resoponse json
    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id
    # Tutor 등록 세션에 저장
    request.session['registration'] = {"uid": user_id}
    # 작성 완료 후 처리 대기중 지원서 여부 확인
    if TutorApplyService.is_doc_complete(user_id):
        return HttpResponseRedirect("/")

    # 작성한 지원서가 있는지 확인!
    resData['personal'] = TutorApplyService.get_doc_personal(user_id)
    # if ts4reg.is_personal_info_state(user_id):
    #     # 있다면 지원서에 내용을 가져온다.!
    #     resData["personal"] = ts4reg.get_application_personal_info(user_id)
    # else:
    #     # 지원서를 작성한 적이 없다면 회원정보에서 내용을 가져온다.
    #     resData["personal"] = ts4reg.get_tutor_personal_info(user_id)

    resData["nations"] = CommonService.country_list()
    resData["languages"] = CommonService.language_list()
    resData["tools"] = CommonService.tool_list()
    resData["levels"] = CommonService.skill_level_list()
    resData["timezoneInfo"] = CommonService.timezone_list()
    jsonData = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("tutor/apply/personal.html", {'data': jsonData}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def registration_personal_save(request):
    """
    @summary: 튜터 지원서 저장
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    personal = json.loads(request.body)
    result = TutorApplyService.set_doc_personal(user_id, personal)
    resData["isSuccess"] = result
    resData["message"] = result

    return JsonResponse(resData)


@csrf_exempt
def get_tutor_personal_info(request):
    """
    @summary: get tutor personal info
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    user_id = ts4reg.get_user_id(request.session["email"])
    resData["personal"] = ts4reg.get_tutor_personal_info(user_id)
    resData["nations"] = ts4reg.get_nations_info()
    resData["languages"] = ts4reg.get_language_info()
    resData["tools"] = ts4reg.get_tools_info()
    resData["levels"] = ts4reg.get_skill_level_info()
    resData["timezoneInfo"] = mypage_service.get_timezone_info()

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def registration_profile(request):
    """
    @summary: tutor profile registration
    @author: msjang
    @param request
    @return: url - tutor/apply/personal.html
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    if "registration" in request.session:
        pass
    else:
        resData["isSuccess"] = 0
        resData['message'] = "no registration session key"
        jsonData = json.dumps(resData)
        return render_to_response("tutor/apply/personal.html", {'data': jsonData}, RequestContext(request))

    # get parameter
    """
    if request.method == 'GET':
        qd = request.GET
    elif request.method == 'POST':
        qd = request.POST
        
    b_checked, data = check_post_data(qd, "personal")
    if b_checked:
        personal = json.loads(data)
    else: 
        return data
        
    name = personal["name"]
    filename = personal["profile_filename"]
    from_country = personal["from"]
    from_city = personal["from_city"]
    livein_country = personal["livein"]
    livein_city = personal["livein_city"]
    timezone = personal["timezone"]
    
    tools = personal["tools"]
    online = personal["online"]
    offline = personal["offline"]
    voice = personal["audio"]
    
    speak_lang_list = []
    
    native1 = personal["native1"]
    speak_lang_list.append({"id":native1, "level": 7})
    native2 = personal["native2"]
    speak_lang_list.append({"id":native2, "level": 7})
    native3 = personal["native3"] 
    speak_lang_list.append({"id":native3, "level": 7})
    
    
    print personal
    learning1 = personal["learning1"]
    learning1_skill_level = personal["learning1_skill_level"]
    speak_lang_list.append({"id":learning1, "level": learning1_skill_level})
    learning2 = personal["learning2"]
    learning2_skill_level = personal["learning2_skill_level"]
    speak_lang_list.append({"id":learning2, "level": learning2_skill_level})
    learning3 = personal["learning3"]
    learning3_skill_level = personal["learning3_skill_level"]
    speak_lang_list.append({"id":learning3, "level": learning3_skill_level})
    
    other1 = personal["other1"]
    other1_skill_level = personal["other1_skill_level"]
    speak_lang_list.append({"id":other1, "level": other1_skill_level})
    other2 = personal["other2"]
    other2_skill_level = personal["other2_skill_level"]
    speak_lang_list.append({"id":other2, "level": other2_skill_level})
    other3 = personal["other3"]
    other3_skill_level = personal["other3_skill_level"]
    speak_lang_list.append({"id":other3, "level": other3_skill_level})
    
    
    teach_lang_list = []
    
    teaching1 = personal["teaching1"]
    if personal.has_key("teaching1_skill_level"):
        teach_lang_list.append({"id":teaching1, "level":personal["teaching1_skill_level"]})
    else:
        teach_lang_list.append({"id":teaching1, "level":None})
        
    teaching2 = personal["teaching2"]
    if personal.has_key("teaching2_skill_level"):
        teach_lang_list.append({"id":teaching2, "level":personal["teaching2_skill_level"]})
    else:
        teach_lang_list.append({"id":teaching2, "level":None})
    teaching3 = personal["teaching3"]
    
    if personal.has_key("teaching3_skill_level"):
        teach_lang_list.append({"id":teaching3, "level":personal["teaching3_skill_level"]})
    else:
        teach_lang_list.append({"id":teaching3, "level":None})
    
    

    
#     b_checked, data = check_post_data_list(qd, "teach_lang[]")
#     if not b_checked:
#         return data
#     else: 
#         teach_lang = data
       
 
    #Tutor 등록 세션에 저장
    tutor = {
             "uid": request.session['registration']["uid"],
             "name": name,
             "filename": filename,
             "from_country": from_country,
             "from_city": from_city,
             "livein_country": livein_country,
             "livein_city": livein_city,
             "timezone": timezone,
             "speak_lang": speak_lang_list,
             "teach_lang": teach_lang_list,
             "online": online,
             "voice": voice,
             "offline": offline,
             "tools": tools, 
             
             "native1": native1,
             "native2": native2,
             "native3": native3,
             "learning1": learning1,
             "learning1_skill_level": learning1_skill_level,
             "learning2": learning2,
             "learning2_skill_level": learning2_skill_level,
             "learning3": learning3,
             "learning3_skill_level": learning3_skill_level,
             "other1": other1,
             "other1_skill_level": other1_skill_level,
             "other2": other2,
             "other2_skill_level": other2_skill_level,
             "other3": other3,
             "other3_skill_level": other3_skill_level,
             "teaching1": teaching1,
             "teaching2": teaching2,
             "teaching3": teaching3,
             }
    request.session['registration'] = {
                                      "uid": request.session['registration']["uid"],
                                      "tutor": tutor,
                                      }
                                      
    """

    # 작성 완료 후 처리 대기중 지원서 여부 확인
    if TutorApplyService.is_doc_complete(user_id):
        return HttpResponseRedirect("/")

    # 전 단계 확인
    # if not TutorApplyService.is_doc_personal(user_id):
    #     return HttpResponseRedirect("/tutor/apply/")

    # 작성한 지원서가 있는지 확인!
    if TutorApplyService.is_doc_profile(user_id):
        # 있다면 지원서에 내용을 가져온다.!
        resData["profile_info"] = TutorApplyService.get_doc_profile(user_id)
    resData['nationsInfo'] = CommonService.country_list()

    jsonData = json.dumps(resData)
    return render_to_response("tutor/apply/profile.html", {"data": jsonData}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def registration_profile_save(request):
    """
    @summary: tutor profile save
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    try:
        profile = json.loads(request.body)
    except Exception as err:
        print str(err)

    result = TutorApplyService.set_doc_profile(user_id, profile)
    resData["isSuccess"] = result
    resData["message"] = result

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def proflie_upload_file(request):
    """
    @summary: user profile upload file
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    resData['message'] = "success"
    upload_file_data = []

    for key in request.FILES.keys():
        try:
            obj = {}
            file_path, file_name = CommonService.upload_image(request.FILES[key],
                                                              request.session["email"],
                                                              'resume_temp')
            obj["type"] = key
            obj["filepath"] = file_path + "/"
            obj["filename"] = file_name
            upload_file_data.append(obj)
        except Exception as err:
            resData["isSuccess"] = 0
            resData['message'] = "file upload failed. " + str(err)
            jsonData = json.dumps(resData)

            return HttpResponse(jsonData, content_type='application/json')

    resData['upload_file_list'] = upload_file_data

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def registration_payment(request):
    """
    @summary: tutor payment 등록
    @author: msjang
    @param request
    @return: url - tutor/apply/personal.html
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    if 'registration' in request.session:
        pass
    else:
        resData["isSuccess"] = 0
        resData['message'] = "no registration session key"
        jsonData = json.dumps(resData)

        return render_to_response("tutor/apply/personal.html", {'data': jsonData}, RequestContext(request))

    # get parameter
    '''
    if request.method == 'GET':
        qd = request.GET
    elif request.method == 'POST':
        qd = request.POST
    
    b_checked, data = check_post_data(qd, "quick_intro")
    if not b_checked:
        return data
    else: 
        quick_intro = data
    
    b_checked, data = check_post_data(qd, "education", novalue=False)
    if not b_checked:
        return data
    else: 
        education = data
        
    b_checked, data = check_post_data(qd, "work_exp", novalue=False)
    if not b_checked:
        return data
    else: 
        work_exp = data
        
    b_checked, data = check_post_data(qd, "certification", novalue=False)
    if not b_checked:
        return data
    else: 
        certification = data
        
    b_checked, data = check_post_data(qd, "long_intro")
    if not b_checked:
        return data
    else: 
        long_intro = data
        
    b_checked, data = check_post_data(qd, "video_intro")
    if not b_checked:
        return data
    else: 
        video_intro = data

        
#     b_checked, data = check_post_data_list(qd, "tags[]")
#     if not b_checked:
#         return data
#     else: 
#         tags = data
    
    #Tutor 등록 세션에 저장
    profile = {
               "quick_intro": quick_intro,
               "education": education,
               "work_exp": work_exp,
               "certification": certification,
               "long_intro": long_intro,
               "video_intro": video_intro,
#                "tags": tags
               }
    
    request.session['registration'] = {
                                       "uid": request.session['registration']["uid"],
                                       "tutor": request.session['registration']["tutor"],
                                       "profile": profile
                                       }
    '''

    # 작성 완료 후 처리 대기중 지원서 여부 확인
    if TutorApplyService.is_doc_complete(user_id):
        return HttpResponseRedirect("/")

    # 전 단계 확인
    if not TutorApplyService.is_doc_profile(user_id):
        return HttpResponseRedirect("/tutor/apply/profile/")

    # 작성한 지원서가 있는지 확인!
    if TutorApplyService.is_doc_payment(user_id):
        # 있다면 지원서에 내용을 가져온다.!
        resData["payment_info"] = TutorApplyService.get_doc_payment(user_id)

    resData["country"] = CommonService.country_list()
    jsonData = json.dumps(resData)

    return render_to_response("tutor/apply/payment.html", {"data": jsonData}, RequestContext(request))


@csrf_exempt
def registration_payment_save(request):
    """
    @summary: tutor payment info save
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    try:
        payment_info = json.loads(request.body)
        print payment_info
    except Exception as err:
        print str(err)

    result = TutorApplyService.set_doc_payment(user_id, payment_info)
    resData["isSuccess"] = result
    resData["message"] = result

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def registration_terms(request):
    """
    @summary: Tutor 지원 중간저장
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    if 'registration' in request.session:
        pass
    else:
        resData["isSuccess"] = 0
        resData['message'] = "no registration session key"
        jsonData = json.dumps(resData)

        return render_to_response("tutor/apply/personal.html", {'data': jsonData}, RequestContext(request))

    # get parameter
    '''
    if request.method == 'GET':
        qd = request.GET
    elif request.method == 'POST':
        qd = request.POST
    
    b_checked, data = check_post_data(qd, "first_name")
    if not b_checked:
        return data
    else: 
        first_name = data

    b_checked, data = check_post_data(qd, "last_name")
    if not b_checked:
        return data
    else: 
        last_name = data

    b_checked, data = check_post_data(qd, "street_addr")
    if not b_checked:
        return data
    else: 
        street_addr = data
    
    b_checked, data = check_post_data(qd, "city")
    if not b_checked:
        return data
    else: 
        city = data
        
    b_checked, data = check_post_data(qd, "state")
    if not b_checked:
        return data
    else: 
        state = data
        
    b_checked, data = check_post_data(qd, "zip")
    if not b_checked:
        return data
    else: 
        zip_code = data

    b_checked, data = check_post_data(qd, "country")
    if not b_checked:
        return data
    else: 
        country = data
 
    b_checked, data = check_post_data(qd, "phone")
    if not b_checked:
        return data
    else: 
        phone = data

    b_checked, data = check_post_data(qd, "email")
    if not b_checked:
        return data
    else: 
        email = data
        
    #Tutor 등록 세션에 저장
    payment = {
               "first_name": first_name,
               "last_name": last_name,
               "street_addr": street_addr,
               "city": city,
               "state": state,
               "zip": zip_code,
               "country": country,
               "phone": phone,
               "email": email,               
               }
    
    request.session['registration'] = {
                                       "uid": request.session['registration']["uid"],
                                       "tutor": request.session['registration']["tutor"],
                                       "profile": request.session['registration']["profile"],
                                       "payment": payment
                                       }
    '''

    # 작성 완료 후 처리 대기중 지원서 여부 확인
    if TutorApplyService.is_doc_complete(user_id):
        return HttpResponseRedirect("/")

    # 전 단계 확인
    if not TutorApplyService.is_doc_payment(user_id):
        return HttpResponseRedirect("/tutor/apply/payment/")

    return render_to_response("tutor/apply/terms.html", RequestContext(request))


@csrf_exempt
@tellpin_login_required
def submit_application(request):
    """
    @summary: Tutor 지원서 submit
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    result = TutorApplyService.submit_doc(user_id)
    resData["isSuccess"] = result
    resData["message"] = result

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def registration_complete(request):
    """
    @summary: tutor 등록 완료
    @author: msjang
    @param request
    @return: 지원서 등록 단계에 따라 url return
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    if 'registration' in request.session:
        pass
    else:
        resData["isSuccess"] = 0
        resData['message'] = "no registration session key"
        jsonData = json.dumps(resData)

        return render_to_response("tutor/apply/personal.html", {'data': jsonData}, RequestContext(request))

    # 단계 완료 여부 확인
    if not TutorApplyService.is_doc_complete(user_id):
        return HttpResponseRedirect("/")

    #TutorApplyService.confirm_tutor_doc(user_id=user_id)

    return render_to_response("tutor/apply/complete.html", RequestContext(request))


@csrf_exempt
@tellpin_login_required
def get_tutor_profile_info(request):
    """
    @summary: tutor profile info 가져오기
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    user_id = ts4reg.get_user_id(request.session["email"])
    resData["profile"] = ts4reg.get_tutor_profile_info(user_id)
    resData["tags"] = ts4reg.get_tags_info()

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def get_tutor_payment_info(request):
    """
    @summary: get tutor payment info
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    user_id = ts4reg.get_user_id(request.session["email"])

    if request.session.has_key("registration"):
        resData["tutor"] = request.session['registration']["tutor"]
        city_name = ts4reg.get_city_name(resData["tutor"]["livein_city"])
        if city_name != "기타":
            resData["tutor"]["city_name"] = city_name

    resData["payment"] = ts4reg.get_tutor_payments_info(user_id)
    resData["country"] = ts4reg.get_nations_info()

    return JsonResponse(resData)


###############################################################################
# 강의 후기 데이터 처리
#
# url : tutor/ratingpage/
# respose : json 
###############################################################################
@csrf_exempt
def tutor_info_ratings(request):
    """
    @summary: get tutor rating info
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    resData = {
        'isSuccess': True
    }
    user_utc = request.session.get('utc_delta', 0)
    # get request parameter
    post_dict = json.loads(request.body)
    page = post_dict['page']
    tutor_id = post_dict['tid']
    resData["Ratings"] = TutorService.set_feedback_info(tutor_id, page, user_utc)

    return JsonResponse(resData)


###############################################################################
# report 데이터 처리
#  
# url : tutor/sendmessage/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def tutor_info_message(request):
    """
    @summary: get tutor message info
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    user_id = request.user.id
    # get request parameter
    post_dict = json.loads(request.body)
    sender = user_id
    receiver = post_dict['tid']
    msg = post_dict['content']
    resData = MessageService.send_message(sender, receiver, msg)

    # 알림
    # Community.set_notification_message(user_id, post_dict["tid"], obj)

    return JsonResponse(resData)


###############################################################################
# 강사 follow 처리
#  
# url : tutor/follow/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def tutor_follow(request):
    """
    @summary: tutor follow
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    resData = dict()
    resData["isSuccess"] = 1
    user_id = request.user.id
    # get request parameter
    post_dict = json.loads(request.body)
    resData["follow"] = TutorService.set_bookmark(post_dict['tid'], user_id)

    CommonService.bitbucket_get_tellpin()
    CommonService.bitbucket_get_tellpin_branches()
    return JsonResponse(resData)


###############################################################################
# 강사 신고
#  
# url : tutor/report/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def tutor_report(request):
    """
    @summary: tutor report
    @author: msjang
    @param request
    @return: json
    """
    user_id = request.user.id
    # get request parameter
    post_dict = json.loads(request.body)
    reporter_id = user_id
    reportee_id = post_dict['tid']
    report_type = post_dict['type']
    report_content = post_dict['content']

    resData = TutorService.tutor_violation(reporter_id, reportee_id, report_type, report_content)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def user_report(request):
    """
    @summary: tutor report
    @author: msjang
    @param request
    @return: json
    """
    user_id = request.user.id
    # get request parameter
    post_dict = json.loads(request.body)
    reporter_id = user_id
    reportee_id = post_dict['tid']
    report_type = post_dict['type']
    report_content = post_dict['content']

    resData = TutorService.user_violation(reporter_id, reportee_id, report_type, report_content)

    return JsonResponse(resData)

###############################################################################
# 강사 숨기기
#  
# url : tutor/videopage/
# respose : json 
###############################################################################
@csrf_exempt
@tellpin_login_required
def tutor_hide(request):
    """
    @summary: tutor hide
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    user_id = ts.get_user_id(request.session["email"])
    # get request parameter
    post_dict = json.loads(request.body)
    b_checked, msg = ts.tutor_hide(post_dict["tid"], user_id)

    if not b_checked:
        resData["isSuccess"] = 0
        resData['message'] = msg

    return JsonResponse(resData)


###############################################################################
# check parameter key value
# 
# reservation_which(), reservation_dt(), reservation_how(), reservation_when(), ...
# respose : tuple(bool, dict) or tuple(bool, HttpResponse)
###############################################################################
def check_post_data(qd, key, novalue=True):
    """
    @summary: check post parameter
    @author: msjang
    @param qd: parameter list
    @param key: parameter name
    @param novalue: 값 존재 유무
    @return: boolean, string
    """
    resData = {}

    if key in qd:
        value = qd[key]
        if novalue:
            if not value or value == "undefined":
                resData["isSuccess"] = 0
                resData['message'] = "no value, check the parameter by post"
                jsonData = json.dumps(resData)
                return False, HttpResponse(jsonData, content_type='application/json')
    else:
        resData["isSuccess"] = 0
        resData['message'] = "no key('" + key + "'), check the parameter by post"
        jsonData = json.dumps(resData)
        return False, HttpResponse(jsonData, content_type='application/json')

    return True, value


@csrf_exempt
@tellpin_login_required
def upload_file(request):
    """
    @summary: file upload
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    params = dict()

    # if request.body:
    #     params = json.loads(request.body)

    for key in request.FILES.keys():
        print "check key : " + key
        print request.FILES[key].name

    # image 파일 데이터 받기
    if 'profile' in request.FILES:
        try:
            image = request.FILES[request.FILES.keys()[0]]
            image_path, image_name = CommonService.upload_image(image, request.user.email, 'profile')
            if 'fid' in params:
                file_id = CommonService.file_insert(image_path, image_name, 'PROFILE', params['fid'])
            else:
                file_id = CommonService.file_insert(image_path, image_name, 'PROFILE')
            resData["photo_filepath"] = image_path
            resData["photo_filename"] = image_name
            resData['photo_fileid'] = file_id

        except Exception as err:
            resData["isSuccess"] = 0
            resData['message'] = "file upload failed. " + str(err)
            jsonData = json.dumps(resData)
            return HttpResponse(jsonData, content_type='application/json')

    # image 파일 데이터 받기
    if 'problem' in request.FILES:
        try:
            image = request.FILES[request.FILES.keys()[0]]
            image_path, image_name = CommonService.upload_image(image, request.user.email, 'problem')
            resData["photo_filepath"] = image_path + "/"
            resData["photo_filename"] = image_name

        except Exception as err:
            resData["isSuccess"] = 0
            resData['message'] = "file upload failed. " + str(err)
            jsonData = json.dumps(resData)
            return HttpResponse(jsonData, content_type='application/json')

    # 파일 데이터 받기
    if 'resume' in request.FILES:
        try:
            image = request.FILES[request.FILES.keys()[0]]
            image_path, image_name = CommonService.upload_image(image, request.user.email, 'resume')
            resData["photo_filepath"] = image_path + "/"
            resData["photo_filename"] = image_name

        except Exception as err:
            resData["isSuccess"] = 0
            resData['message'] = "file upload failed. " + str(err)
            jsonData = json.dumps(resData)
            return HttpResponse(jsonData, content_type='application/json')

    jsonData = json.dumps(resData)
    return HttpResponse(jsonData, content_type='application/json')


# 메세지 보내기
@csrf_exempt
@tellpin_login_required
def ajax_newMessage(request):
    """
    @summary: send message
    @author: msjang, chsin
    @param request
    @return: json
    """
    resData = {}
    resData['isSuccess'] = 1
    # user_id = ts.get_user_id(request.session["email"])
    user_id = request.session['id']
    utc_time = request.session.get('utc_time', '+00:00')
    dict = json.loads(request.body)
    to_user_id = dict['to_user_id']
    from_user_id = user_id
    message = dict['message']
    rvid = dict['rvid']
    resData['message'], obj = TutorService.sendMessage(to_user_id, from_user_id, message, rvid, utc_time)
#    Community.set_notification_message(from_user_id, to_user_id, obj, 2)

    return JsonResponse(resData)


# 메세지 목록
@csrf_exempt
@tellpin_login_required
def ajax_getMessage(request):
    """
    @summary: get message list
    @author: msjang, chsin
    @param request
    @return: json
    """
    print 'ajax_getMessage()'
    resData = {}
    resData['isSuccess'] = 1
#    user_id = ts.get_user_id(request.session["email"])
    user_id = request.session['id']
    utc_time = request.session.get('utc_time', '+00:00')
    dict = json.loads(request.body)
    to_user_id = dict['to_user_id']
    rvid = dict['rvid']
    resData['message'] = TutorService.getMessage(user_id, to_user_id, rvid, utc_time)

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def ajax_checkNewMessage(request):
    """
    @summary: get new message count
    @author: chsin
    @param request
    @return: json
    """
    print 'ajax_checkNewMessage'
    resData = {}
    resData['isSuccess'] = 1
    
    user_id = request.session['id']
    dict = json.loads(request.body)
    to_user_id = dict['to_user_id']
    rvid = dict['rvid']
    
    resData['newMsg'] = TutorService.checkNewMessage(user_id, to_user_id, rvid)
    
    return JsonResponse(resData)

# @tellpin_login_required
# def tutorEditProfile(request):
#     """
#     @summary: tutor profile edit
#     @author: msjang
#     @param request
#     @return: url - tutor/tutor_edit_profile.html or url - tutor/tutee_edit_profile.html
#     """
#     resData = {}
#     resData['isSuccess'] = 1
#     user_id = request.user.id
#     isTutor = TutorService.is_tutor(user_id)
# 
#     if isTutor:
#         return render_to_response('tutor/tutor_edit_profile.html',
#                                   {'is_tutor': isTutor},
#                                   RequestContext(request))
#     else:
#         return render_to_response('tutor/tutee_edit_profile.html',
#                                   {'is_tutor': isTutor},
#                                   RequestContext(request))


@csrf_exempt
def more_rating(request):
    """
    @summary: get tutor rating info 
    @author: msjang
    @param request
    @return: json
    """
    resData = {
        'isSuccess': True
    }
    user_utc = request.session.get('utc_delta', 0)
    post = json.loads(request.body)
    feedback_id = post['rtid']
    tutee_id = post['tutee_id']
    tutor_id = post['tutor_id']
    resData['moreRating'] = TutorService.more_feedback(feedback_id, tutor_id, tutee_id, user_utc)

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def saveProfile(request):
    """
    @summary: 사용자 프로필을 저장하는 함수
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    isTutor = TutorService.is_tutor(user_id)
    post_dict = json.loads(request.body)
    profile = post_dict["profile"]
    profile["user"] = user_id
    print profile
    request.session["name"], request.session["photo"], request.session["utc_time"], request.session[
        "utc_location"] = TutorService.save_user_profile(profile, isTutor, request)
    resData["tools"] = TutorService.get_tutor_tools(user_id, None)
    resData["name"] = request.session["name"]
    resData["photo"] = request.session["photo"]

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def userSaveLanguage(request):
    """
    @summary: 사용자는 언어 변경 시 관리자의 허가 없이 바로 저장이 되므로 사용자의 언어 변경에 대한 함수
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    isTutor = TutorService.is_tutor(user_id)
    post_dict = json.loads(request.body)
    profile = post_dict["profile"]
    profile["user"] = user_id
    TutorService.save_user_language(profile, isTutor)

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def userSaveIntro(request):
    """
    @summary: 설정->내프로필에서 자기소개를 변경 후 저장할 때 불리는 함수
    @author: shkim
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)
    TutorService.save_user_intro(post_dict["profile"])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def tutorPendingIntro(request):
    """
    @summary: 튜터가 resume 변경 시 관리자 승인이 필요하므로 우선 pending db에 저장하는 함수
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)
    profile = post_dict["profile"]
    print "tutorSaveTutorIntro  bbbbb"
    TutorService.pending_tutor_intro(profile)

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def tutorPendingResume(request):
    """
    @summary: 튜터가 resume 변경 시 관리자 승인이 필요하므로 우선 pending db에 저장하는 함수
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)
    profile = post_dict["profile"]
    #TutorService.pending_resume(profile)
    TutorService.pending_resume(request.user.id, profile)

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def tutorSaveTools(request):
    """
    @summary: 튜터의 메신저 정보를  저장하는 함수
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)
    profile = post_dict["profile"]
    user_id = request.user.id
    isTutor = TutorService.is_tutor(user_id)
    TutorService.save_tools(profile, isTutor)

    return JsonResponse(resData)

@csrf_exempt
@tellpin_login_required
def ajax_tutorEditProfile(request):
    """
    @summary: 사용자 정보를 편집하는데 필요한 데이터를 구성하는 함수(에디트 화면 진입 시 많은 시간이 소요되므로 html 파일만 로딩 후 추후에 데이터를 구성한다)
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    isTutor = TutorService.is_tutor(user_id)
    try:
        resData['profile'] = TutorService.get_user_info(user_id, isTutor)
        if isTutor:
            resData['profile']['short_intro'] = TutorDoc.objects.get(user_id=user_id).short_intro
            resData['profile']['long_intro'] = TutorDoc.objects.get(user_id=user_id).long_intro
            resData['profile']['video_intro'] = TutorDoc.objects.get(user_id=user_id).video_intro
        resData['profile']['user_shortIntro'] = Tutee.objects.get(user_id=user_id).short_intro
        resData['profile']['user_longIntro'] = Tutee.objects.get(user_id=user_id).long_intro
        '''
        resData['video_intro'] = tutorIntro.video_intro
        profilePending['pending'] = tutorIntro.pending
        profilePending['p_quick_intro'] = tutorIntro.p_quick_intro
        profilePending['p_long_intro'] = tutorIntro.p_long_intro
        profilePending['p_video_intro'] = tutorIntro.p_video_intro
        resume_info = Resume.objects.get(user_id=user_id)
        resumePending["pending"] = resume_info.pending
        resumePending["p_education"] = resume_info.p_education
        resumePending["p_work_exp"] = resume_info.p_work_exp
        resumePending["p_certification"] = resume_info.p_certification
        resData['profilePending'] = profilePending
        resData['resumePending'] = resumePending

        if isTutor:
            if resume_info.fid:
                resume_info.fid = resume_info.fid.split(',')
                resume_info.fid = map(int, resume_info.fid)
                resumePending["fid"] = resume_info.fid
                while index < 9:
                    if resume_info.fid[index] != 0:
                        resumeFile["filename" + str(index)] = Files.objects.get(fid=resume_info.fid[index]).filename
                        index += 1
                    else:
                        resumeFile["filename" + str(index)] = "no file selected"
                        index += 1
            else:
                while index < 9:
                    resumeFile["filename" + str(index)] = "no file selected"
                    index += 1
            resData['resumePending']['filename'] = resumeFile
            '''

        resData['nationsInfo'] = CommonService.country_list()
        resData['languageInfo'] = CommonService.language_list()
        resData['skillLevelInfo'] = CommonService.skill_level_list()
        resData['timezoneInfo'] = CommonService.timezone_list()
        resData['tools'] = CommonService.tool_list()
        resData['currencyInfo'] = CommonService.currency_list()

        return JsonResponse(resData)
    except Exception as e:
        print 'ajax_tutorEditProfile error'
        print e

        return JsonResponse(e)

@tellpin_login_required
def tutorViewProfile(request):
    """
    @summary: 사용자 상세 정보를 보여준 진입 함수
    @author: sj746
    @param request
    @return: tutor 상세 페이지 호출
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id

    return tutor_info(request, user_id)
