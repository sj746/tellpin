from django.db import models



class TestView(models.Model):
    vid = models.AutoField(primary_key=True)
    tid = models.IntegerField()
    display_name = models.CharField(max_length=100)
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    tutor_image = models.CharField(max_length=100, blank=True, null=True)
    nation_from = models.CharField(max_length=45)
    city_from = models.CharField(max_length=45)
    nation_live = models.CharField(max_length=45)
    city_live = models.CharField(max_length=45)
    nation_flag = models.CharField(max_length=45)
    
    class Meta:
        managed = False
        db_table = 'test_view'
        