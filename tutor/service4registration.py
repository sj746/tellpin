# -*- coding: utf-8 -*-
'''
Created on 2016. 1. 13.

@author: unichal
'''

from datetime import date
import os
import time

from django.core.exceptions import ObjectDoesNotExist
from django.db.models.aggregates import Min, Max, Count, Sum
from django.forms.models import model_to_dict

# from common.models import NationInfo, CityInfo, Lesson, Ratings, \
#     Reservation, Specialtag, SpecialtagInfo, Profile, Tools, ToolsInfo, Follow, \
#     Resume, Videolesson, Payments, CurrencyInfo, Files, Applications, WorkExp, \
#     Certification
# from common.models import SkillLevel, Education
# from common.models import TellpinUser, Language, Timezone
# from common.models import WeekSchedule
from tellpin.settings import BASE_DIR
from common.models import LanguageInfo, SkillLevelInfo, CurrencyInfo
from user_auth.models import TellpinUser

# from django.db.models.query_utils import Q
class TutorService4Registration(object):
    '''
    classdocs
    '''

    NATION_FLAG_FILE_DIR = "/static/img/nations"
    PRFILE_IMAGE_FILE_DIR = "/static/img/upload/profile"

    VL_PERPAGE = 3
    RT_PERPAGE = 5

    def __init__(self, params):
        '''
        Constructor
        '''

    # get user ID
#     def get_user_id(self, _email):
#         """
#         @summary: 유저 id 가져오기
#         @param _email: 유저 email
#         @return: id( int )
#         """
#         # 사용자 id 얻기
#         try:
#             return TellpinUser.objects.get(email=_email).id
#         except:
#             return None

    

    # 메신저 정보
    def get_tools_info(self):
        """
        @summary: 메신저 정보
        @author: msjang
        @param none
        @return: tool_list( list )
        """
        tool_list = []
        tools = ToolsInfo.objects.all()

        for tool in tools:
            tool_obj = {}
            tool_obj["id"] = tool.tool_id
            tool_obj["name"] = tool.tool_name
            tool_list.append(tool_obj)
        return tool_list

    # 태그 정보
    def get_tags_info(self):
        """
        @summary: 태그 정보
        @author: msjang
        @param none
        @return: tag_list( list )
        """
        tag_list = []
        tags = SpecialtagInfo.objects.all()

        for tag in tags:
            tag_obj = {}
            tag_obj["id"] = tag.stid
            tag_obj["name"] = tag.stname
            tag_list.append(tag_obj)
        return tag_list


   
    # get tutor information
    def get_tutor_personal_info(self, user_id):
        """
        @summary: 튜터 프로필
        @author: msjang
        @param user_id: 유저 id
        @return: obj( object )
        """
        # tutor profile
        try:
            obj = {}
            # Personal info
            tutor_info = TellpinUser.objects.get(id=user_id)
            #             print "get_tutor_personal_info"

            obj["tid"] = tutor_info.id
            obj["name"] = tutor_info.name
            obj["profile_path"] = self.PRFILE_IMAGE_FILE_DIR + "/"
            obj["profile_filename"] = tutor_info.profile_photo
            obj["from"] = tutor_info.from_field
            obj["from_city"] = tutor_info.from_city
            obj["livein"] = tutor_info.livein
            obj["livein_city"] = tutor_info.livein_city
            obj["timezone"] = tutor_info.timezone

            # add Table column
            obj["native1"] = tutor_info.native1
            obj["native2"] = tutor_info.native2
            obj["native3"] = tutor_info.native3
            obj["learning1"] = tutor_info.learning1
            obj["learning1_skill_level"] = tutor_info.learning1_skill_level
            obj["learning2"] = tutor_info.learning2
            obj["learning2_skill_level"] = tutor_info.learning2_skill_level
            obj["learning3"] = tutor_info.learning3
            obj["learning3_skill_level"] = tutor_info.learning3_skill_level
            obj["other1"] = tutor_info.other1
            obj["other1_skill_level"] = tutor_info.other1_skill_level
            obj["other2"] = tutor_info.other2
            obj["other2_skill_level"] = tutor_info.other2_skill_level
            obj["other3"] = tutor_info.other3
            obj["other3_skill_level"] = tutor_info.other3_skill_level
            obj["teaching1"] = tutor_info.teaching1
            obj["teaching2"] = tutor_info.teaching2
            obj["teaching3"] = tutor_info.teaching3

            tool_info = Tools.objects.filter(user_id=tutor_info.id)
            if tool_info:
                tool_info = Tools.objects.get(user_id=tutor_info.id)
                obj["offline"] = tool_info.offline
                obj["online"] = tool_info.online
                obj["audio"] = tool_info.audio
                obj["tools"] = [];
                if tool_info.skype == 1:
                    obj["tools"].append({"name": "Skype", "account": tool_info.skype_id})
                if tool_info.hangout == 1:
                    obj["tools"].append({"name": "HangOut", "account": tool_info.hangout_id})
                if tool_info.facetime == 1:
                    obj["tools"].append({"name": "FaceTime", "account": tool_info.facetime_id})
                if tool_info.qq == 1:
                    obj["tools"].append({"name": "QQ", "account": tool_info.qq_id})
            else:
                obj["offline"] = None
                obj["online"] = None
                obj["audio"] = None

        except Exception as err:
            print err
            obj["tid"] = None
            obj["name"] = None
            obj["profile_path"] = None
            obj["profile_filename"] = None
            obj["from"] = None
            obj["from_city"] = None
            obj["livein"] = None
            obj["livein_city"] = None
            obj["timezone"] = None

            # add Table column
            obj["native1"] = None
            obj["native2"] = None
            obj["native3"] = None
            obj["learning1"] = None
            obj["learning1_skill_level"] = None
            obj["learning2"] = None
            obj["learning2_skill_level"] = None
            obj["learning3"] = None
            obj["learning3_skill_level"] = None
            obj["other1"] = None
            obj["other1_skill_level"] = None
            obj["other2"] = None
            obj["other2_skill_level"] = None
            obj["other3"] = None
            obj["other3_skill_level"] = None
            obj["teaching1"] = None
            obj["teaching2"] = None
            obj["teaching3"] = None

            obj["offline"] = None
            obj["online"] = None
            obj["audio"] = None
        return obj

    # get tutor profile information
    def get_tutor_profile_info(self, user_id):
        """
        @summary: 튜터 resume 정보
        @author: msjang
        @param user_id: 유저 id
        @return: obj( object )
        """
        try:
            tutor_id = self.get_tutor_id(user_id)
            obj = {}
            resumeFile = {}
            index = 0

            resume_info = Resume.objects.get(user_id=tutor_id)
            obj["rsid"] = resume_info.rsid
            obj["education"] = resume_info.education
            obj["work_exp"] = resume_info.work_exp
            obj["certification"] = resume_info.certification
            obj["pending"] = resume_info.pending
            obj["fid"] = resume_info.fid

            if (resume_info.fid):
                resume_info.fid = resume_info.fid.split(',')
                resume_info.fid = map(int, resume_info.fid);

                while (index < 9):
                    if (resume_info.fid[index] != 0):
                        resumeFile["filename" + str(index)] = Files.objects.get(fid=resume_info.fid[index]).filename
                        index += 1
                    else:
                        resumeFile["filename" + str(index)] = "no file selected"
                        index += 1
            else:
                while (index < 9):
                    resumeFile["filename" + str(index)] = "no file selected"
                    index += 1

            obj['filename'] = resumeFile

            profile_info = Profile.objects.get(user_id=tutor_id)
            obj["quick_intro"] = profile_info.quick_intro
            obj["long_intro"] = profile_info.long_intro
            obj["video_intro"] = profile_info.video_intro

            tag_list = []
            tags_info = Specialtag.objects.filter(user_id=tutor_id)
            for tag in tags_info:
                tag_list.append(tag.stid)

            obj["tags"] = tag_list

        except Exception as err:
            print str(err)

        return obj

    # get tutor payments information
    def get_tutor_payments_info(self, user_id):
        """
        @summary: tutor payment info
        @author: msjang
        @param user_id: 유저 id
        @return: obj( object )
        """
        try:
            tutor_id = self.get_tutor_id(user_id)
            obj = {}

            payments_info = Payments.objects.get(user_id=tutor_id)
            obj["first_name"] = payments_info.first_name
            obj["last_name"] = payments_info.last_name
            obj["street_addr"] = payments_info.street_addr
            obj["city"] = payments_info.city
            obj["state"] = payments_info.state
            obj["zip"] = payments_info.zip
            obj["country"] = payments_info.country
            obj["phone"] = payments_info.phone
            obj["email"] = payments_info.email

        except Exception as err:
            print str(err)

        return obj

    def get_city_name(self, _city_id):
        """
        @summary: 도시 이름
        @author: msjang
        @param _city_id : city id
        @return: city.name( string )
        """
        city = CityInfo.objects.get(city_id=_city_id)
        return city.name

    def pending_tutor_intro(self, _profile):
        """
        @summary: 튜터 정보pending
        @author: msjang
        @param _profile: 프로필 정보
        @return: True, False( boolean )
        """
        try:
            pf = Profile.objects.get(user_id=_profile["id"])
        except:
            pf = Profile()
            pf.user_id = _profile["id"]

        print "save_tutor_intro >>>>"
        print _profile
        pf.p_quick_intro = _profile["quick_intro"]
        pf.p_long_intro = _profile["long_intro"]
        pf.p_video_intro = _profile["video_intro"]
        pf.pending = 1;
        pf.save()

        return True

    

    def upload_file(self, _file, _type):
        """
        @summary: 파일 업로드 DB insert
        @author: msjang
        @param _file: 파일정보
        @param _type: resume 타입
        @return: resumefile.fid( int )
        """
        resumefile = Files()
        resumefile.type = _type
        resumefile.filename = _file["profile_filename"]
        resumefile.filepath = "/static/img/upload/resume"
        resumefile.save()

        return resumefile.fid



    
    def pending_tutor_language(self, profile):
        """
        @summary: 튜터 언어 변경 pending
        @author: msjang
        @param profile: 해당 튜터 정보
        @return: True, False( boolean )
        """
        try:  # 이미 tutor 인가요?
            tutor = TellpinUser.objects.get(id=profile["id"])
        except:  # 아니면 새로 생성
            is_new = True
            tutor = TellpinUser()

        tutor.p_native1 = profile["native1"]
        tutor.p_native2 = profile["native2"]
        tutor.p_native3 = profile["native3"]

        tutor.p_learning1 = profile["learning1"]
        tutor.p_learning1_skill_level = profile["learning1_skill_level"]
        tutor.p_learning2 = profile["learning2"]
        tutor.p_learning2_skill_level = profile["learning2_skill_level"]
        tutor.p_learning3 = profile["learning3"]
        tutor.p_learning3_skill_level = profile["learning3_skill_level"]

        tutor.p_other1 = profile["other1"]
        tutor.p_other1_skill_level = profile["other1_skill_level"]
        tutor.p_other2 = profile["other2"]
        tutor.p_other2_skill_level = profile["other2_skill_level"]
        tutor.p_other3 = profile["other3"]
        tutor.p_other3_skill_level = profile["other3_skill_level"]
        #
        tutor.p_teaching1 = profile["teaching1"]
        tutor.p_teaching2 = profile["teaching2"]
        tutor.p_teaching3 = profile["teaching3"]
        tutor.pending_lang = 1
        tutor.is_tutor = 1

        tutor.save()

        return True

    def save_user_language(self, profile):
        """
        @summary: 튜터 언어변경
        @author: msjang
        @param profile: 프로필 정보
        @return: True, False( boolean )
        """
        try:  # 이미 tutor 인가요?
            tutor = TellpinUser.objects.get(id=profile["id"])
        except:  # 아니면 새로 생성
            is_new = True
            tutor = TellpinUser()

        tutor.native1 = profile["native1"]
        tutor.native2 = profile["native2"]
        tutor.native3 = profile["native3"]

        tutor.learning1 = profile["learning1"]
        tutor.learning1_skill_level = profile["learning1_skill_level"]
        tutor.learning2 = profile["learning2"]
        tutor.learning2_skill_level = profile["learning2_skill_level"]
        tutor.learning3 = profile["learning3"]
        tutor.learning3_skill_level = profile["learning3_skill_level"]

        tutor.other1 = profile["other1"]
        tutor.other1_skill_level = profile["other1_skill_level"]
        tutor.other2 = profile["other2"]
        tutor.other2_skill_level = profile["other2_skill_level"]
        tutor.other3 = profile["other3"]
        tutor.other3_skill_level = profile["other3_skill_level"]

        tutor.is_tutor = 0

        tutor.save()

        return True

    def get_application(self, _user_id):
        """
        @summary: 지원서 정보
        @author: msjang
        @param _user_id: 유저 id
        @return: True, False( boolean )
        """
        if Applications.objects.filter(user_id=_user_id, submit_state=0).exists():
            return Applications.objects.get(user_id=_user_id, submit_state=0)
        else:
            return False

    def is_complete_apply(self, _user_id):
        """
        @summary: 튜터 지원 단계 완료 여부
        @author: msjang
        @param _user_id: 유저 id
        @return: True, False( boolean )
        """
        return Applications.objects.filter(user_id=_user_id, submit_state=1).exists()

    def is_personal_info_state(self, _user_id):
        """
        @summary: 튜터 지원 단계중 개인정보 입력 여부
        @author: msjang
        @param _user_id: 유저 id
        @return: True, False( boolean )
        """
        return Applications.objects.filter(user_id=_user_id, submit_state=0, personal_state=1).exists()

    def is_profile_info_state(self, _user_id):
        """
        @summary: 튜터 지원 단계중 프로필 입력 여부
        @author: msjang
        @param _user_id: 유저 id
        @return: True, False( boolean )
        """
        return Applications.objects.filter(user_id=_user_id, submit_state=0, profile_state=1).exists()

    def is_payment_info_state(self, _user_id):
        """
        @summary: 튜터 지원 단계중 payment 입력 여부
        @author: msjang
        @param _user_id: 유저 id
        @return: True, False( boolean )
        """
        return Applications.objects.filter(user_id=_user_id, submit_state=0, payinfo_state=1).exists()

    def get_application_personal_info(self, _user_id):
        """
        @summary: 튜터 지원 단계중 개인정보 
        @author: msjang
        @param _user_id: 유저 id
        @return: obj( object )
        """
        my_application = self.get_application(_user_id)
        if my_application is False:
            raise ObjectDoesNotExist

        obj = {}
        # Personal info
        print "get_application_personal_info"

        obj["tid"] = my_application.user_id
        obj["name"] = my_application.name
        obj["profile_path"] = self.PRFILE_IMAGE_FILE_DIR + "/"
        obj["profile_filename"] = my_application.profile_photo
        obj["from"] = my_application.from_field
        obj["from_city"] = my_application.from_city
        obj["livein"] = my_application.livein
        obj["livein_city"] = my_application.livein_city

        # add Table column
        obj["native1"] = my_application.native1
        obj["native2"] = my_application.native2
        obj["native3"] = my_application.native3
        obj["learning1"] = my_application.learning1
        obj["learning1_skill_level"] = my_application.learning1_skill_level
        obj["learning2"] = my_application.learning2
        obj["learning2_skill_level"] = my_application.learning2_skill_level
        obj["learning3"] = my_application.learning3
        obj["learning3_skill_level"] = my_application.learning3_skill_level
        obj["other1"] = my_application.other1
        obj["other1_skill_level"] = my_application.other1_skill_level
        obj["other2"] = my_application.other2
        obj["other2_skill_level"] = my_application.other2_skill_level
        obj["other3"] = my_application.other3
        obj["other3_skill_level"] = my_application.other3_skill_level
        obj["teaching1"] = my_application.teaching1
        obj["teaching2"] = my_application.teaching2
        obj["teaching3"] = my_application.teaching3

        if my_application:
            obj["offline"] = my_application.offline
            obj["online"] = my_application.online
            obj["audio"] = my_application.audio
            obj["tools"] = [];
            if my_application.skype == 1:
                obj["tools"].append({"name": "Skype", "account": my_application.skype_id})
            if my_application.hangout == 1:
                obj["tools"].append({"name": "HangOut", "account": my_application.hangout_id})
            if my_application.facetime == 1:
                obj["tools"].append({"name": "FaceTime", "account": my_application.facetime_id})
            if my_application.qq == 1:
                obj["tools"].append({"name": "QQ", "account": my_application.qq_id})
        else:
            obj["offline"] = None
            obj["online"] = None
            obj["audio"] = None

        return obj

    def set_application_personal_info(self, _user_id, personal):
        """
        @summary: 튜터 지원 단계중 개인정보 저장
        @author: msjang
        @param _user_id: 유저 id
        @param personal: 개인정보
        @return: object
        """
        try:
            my_application, created = Applications.objects.get_or_create(user_id=_user_id, submit_state=0)
            print created

            for key in personal.keys():
                if key == "from":
                    setattr(my_application, "from_field", personal[key])
                elif key == "profile_filename":
                    setattr(my_application, "profile_photo", personal[key])
                else:
                    setattr(my_application, key, personal[key])

            my_application.personal_state = 1

            my_application.save()

            return {"isSuccess": 1, "message": "personal info set"}
        except Exception as err:
            print "set_application_personal_info() : " + str(err)
            return {"isSuccess": 0, "message": "set Fail"}

    def get_application_profile_info(self, _user_id):
        """
        @summary: 튜터 지원 단계중 프로필 정보
        @author: msjang
        @param _user_id: 유저 id
        @return: obj( object )
        """
        my_application = self.get_application(_user_id)
        if my_application is False:
            raise ObjectDoesNotExist

        obj = {}
        # Profile info
        print "get_application_profile_info"

        profile_info_keys = ["education", "work_exp", "certification", "quick_intro", "long_intro", "video_intro"]
        for key in profile_info_keys:
            obj[key] = getattr(my_application, key)

        profile_work_exp_keys = ["work_exp_1", "work_exp_2", "work_exp_3"]
        profile_education_keys = ["education_1", "education_2", "education_3"]
        profile_certification_keys = ["certificate_1", "certificate_2", "certificate_3"]

        for key in profile_work_exp_keys:
            if WorkExp.objects.filter(wid=getattr(my_application, key)).exists():
                obj[key] = model_to_dict(WorkExp.objects.get(wid=getattr(my_application, key)))

        for key in profile_education_keys:
            if Education.objects.filter(eid=getattr(my_application, key)).exists():
                obj[key] = model_to_dict(Education.objects.get(eid=getattr(my_application, key)))

        for key in profile_certification_keys:
            if Certification.objects.filter(cid=getattr(my_application, key)).exists():
                obj[key] = model_to_dict(Certification.objects.get(cid=getattr(my_application, key)))

        file_keys = ["education_file0", "education_file1", "education_file2",
                     "work_exp_file0", "work_exp_file1", "work_exp_file2",
                     "certification_file0", "certification_file1", "certification_file2"]

        for key in file_keys:
            if getattr(my_application, key) != 0 and not None:
                obj[key] = self.get_upload_resume_file_data(getattr(my_application, key))
            else:
                obj[key] = None

        return obj

    def set_work_exp_info(self, _user_id, data, my_application):
        ref_id_list = []
        fid_matching_obj = {
            "work_exp_1": "work_exp_file0", "work_exp_2": "work_exp_file1", "work_exp_3": "work_exp_file2"
        }
        print data
        try:
            for we in data:
                field, value = we.items()[0]
                if value["info"]:
                    if getattr(my_application, field):
                        my_we = WorkExp.objects.get(wid=getattr(my_application, field))

                        if value['file_del_yn']:
                            if my_we.fid != 0 and not None:
                                target = self.get_upload_file_path(my_we.fid)
                                if target is not False:
                                    self.delete_file(target)
                                    self.delete_upload_file_data(my_we.fid)
                            my_we.fid = None
                            setattr(my_application, fid_matching_obj[field], None)
                    else:
                        my_we = WorkExp()
                    my_we.user_id = _user_id
                    my_we.start_year = value["startYear"]["val"]
                    my_we.end_year = value["endYear"]["val"]
                    my_we.company = value["company"]["val"]
                    my_we.country = value["country_name"]
                    my_we.city = value["city"]["val"]
                    my_we.position = value["position"]["val"]
                    my_we.description = value["desc"]["val"]
                    my_we.save()
                    ref_id_list.append({field: my_we.wid})
                else:
                    if getattr(my_application, field):
                        del_tar = WorkExp.objects.get(wid=getattr(my_application, field))
                        if del_tar.fid != 0 and not None:
                            target = self.get_upload_file_path(del_tar.fid)
                            if target is not False:
                                self.delete_file(target)
                                self.delete_upload_file_data(del_tar.fid)
                        del_tar.delete()
                        setattr(my_application, field, None)
                        setattr(my_application, fid_matching_obj[field], None)

        except Exception as err:
            print str(err)

        return ref_id_list

    def set_work_exp_info_fid(self, fidlist):
        for item in fidlist:
            b_setting_fid = False
            for key in item:
                if key != "fid":
                    _wid = item[key]
                else:
                    _fid = item[key]
                    b_setting_fid = True

            if b_setting_fid and WorkExp.objects.filter(wid=_wid).exists():
                data = WorkExp.objects.get(wid=_wid)
                data.fid = _fid
                data.save()

    def set_education_info(self, _user_id, data, my_application):
        ref_id_list = [];
        fid_matching_obj = {
            "education_1": "education_file0", "education_2": "education_file1", "education_3": "education_file2"
        }

        try:
            for education in data:
                field, value = education.items()[0]
                if value["info"]:
                    if getattr(my_application, field):
                        my_edu = Education.objects.get(eid=getattr(my_application, field))

                        if value['file_del_yn']:
                            if my_edu.fid != 0 and not None:
                                target = self.get_upload_file_path(my_edu.fid)
                                if target is not False:
                                    self.delete_file(target)
                                    self.delete_upload_file_data(my_edu.fid)
                            my_edu.fid = None
                            setattr(my_application, fid_matching_obj[field], None)
                    else:
                        my_edu = Education()

                    my_edu.user_id = _user_id
                    my_edu.start_year = value["startYear"]["val"]
                    my_edu.end_year = value["endYear"]["val"]
                    my_edu.school = value["school"]["val"]
                    my_edu.location = value["location_name"]
                    my_edu.degree = value["degree"]
                    my_edu.major = value["major"]["val"]
                    my_edu.description = value["desc"]["val"]
                    my_edu.save()
                    ref_id_list.append({field: my_edu.eid})
                else:
                    if getattr(my_application, field):
                        del_tar = Education.objects.get(eid=getattr(my_application, field))
                        if del_tar.fid != 0 and not None:
                            target = self.get_upload_file_path(del_tar.fid)
                            if target is not False:
                                self.delete_file(target)
                                self.delete_upload_file_data(del_tar.fid)
                        del_tar.delete()
                        setattr(my_application, field, None)
                        setattr(my_application, fid_matching_obj[field], None)

        except Exception as err:
            print str(err)

        return ref_id_list

    def set_education_info_fid(self, fidlist):
        for item in fidlist:
            b_setting_fid = False
            for key in item:
                if key != "fid":
                    _eid = item[key]
                else:
                    _fid = item[key]
                    b_setting_fid = True

            if b_setting_fid and Education.objects.filter(eid=_eid).exists():
                data = Education.objects.get(eid=_eid)
                data.fid = _fid
                data.save()

    def set_certification_info(self, _user_id, data, my_application):
        ref_id_list = []
        fid_matching_obj = {
            "certificate_1": "certification_file0", "certificate_2": "certification_file1",
            "certificate_3": "certification_file2"
        }
        try:
            for certi in data:
                field, value = certi.items()[0]
                if value["info"]:
                    if getattr(my_application, field):
                        my_we = Certification.objects.get(cid=getattr(my_application, field))

                        if value['file_del_yn']:
                            if my_we.fid != 0 and not None:
                                target = self.get_upload_file_path(my_we.fid)
                                if target is not False:
                                    self.delete_file(target)
                                    self.delete_upload_file_data(my_we.fid)
                            my_we.fid = None
                            setattr(my_application, fid_matching_obj[field], None)
                    else:
                        my_we = Certification()

                    my_we.user_id = _user_id
                    my_we.year = value["startYear"]["val"]
                    my_we.name = value["name"]["val"]
                    my_we.organization = value["organization"]["val"]
                    my_we.description = value["desc"]["val"]
                    my_we.save()
                    ref_id_list.append({field: my_we.cid})
                else:
                    if getattr(my_application, field):
                        del_tar = Certification.objects.get(cid=getattr(my_application, field))
                        if del_tar.fid != 0 and not None:
                            target = self.get_upload_file_path(del_tar.fid)
                            if target is not False:
                                self.delete_file(target)
                                self.delete_upload_file_data(del_tar.fid)
                        del_tar.delete()

                        setattr(my_application, field, None)
                        setattr(my_application, fid_matching_obj[field], None)
        except Exception as err:
            print str(err)

        return ref_id_list

    def set_certification_info_fid(self, fidlist):
        for item in fidlist:
            b_setting_fid = False
            for key in item:
                if key != "fid":
                    _cid = item[key]
                else:
                    _fid = item[key]
                    b_setting_fid = True

            if b_setting_fid and Certification.objects.filter(cid=_cid).exists():
                data = Certification.objects.get(cid=_cid)
                data.fid = _fid
                data.save()

    def set_application_profile_info(self, _user_id, profile):
        """
        @summary: 튜터 지원 단계중 프로필 입력 저장
        @author: msjang
        @param _user_id: 유저 id
        @param profile: 프로필 정보
        @return: object
        """
        my_application = self.get_application(_user_id)
        if my_application is False:
            raise ObjectDoesNotExist

        try:
            print "set_application_profile_info"

            # 인사말
            my_application.quick_intro = profile["quick_intro"]
            my_application.long_intro = profile["long_intro"]
            my_application.video_intro = profile["video_intro"]

            # resume
            work_id_list = self.set_work_exp_info(_user_id, profile["work_exp"], my_application)
            edu_id_list = self.set_education_info(_user_id, profile["education"], my_application)
            certi_id_list = self.set_certification_info(_user_id, profile["certification"], my_application)
            # my_application.education = profile["education"]
            for edu in edu_id_list:
                field, value = edu.items()[0]
                setattr(my_application, field, value)
                # my_application.work_exp = profile["work_exp"]
            for we in work_id_list:
                field, value = we.items()[0]
                setattr(my_application, field, value)
                # my_application.certification = profile["certification"]
            for certi in certi_id_list:
                field, value = certi.items()[0]
                setattr(my_application, field, value)

                # upload files
            fid_list = self.set_upload_resume_file_data(my_application.aid, profile["upload_file_list"])
            print fid_list
            fid_matching_obj = {
                "work_exp_file0": "work_exp_1", "work_exp_file1": "work_exp_2", "work_exp_file2": "work_exp_3",
                "education_file0": "education_1", "education_file1": "education_2", "education_file2": "education_3",
                "certification_file0": "certificate_1", "certification_file1": "certificate_2",
                "certification_file2": "certificate_3"
            }

            for wf in work_id_list:
                for item in fid_list:
                    if wf.has_key(fid_matching_obj[item["type"]]):
                        wf["fid"] = item["fid"]
                        break

            for ef in edu_id_list:
                for item in fid_list:
                    if ef.has_key(fid_matching_obj[item["type"]]):
                        ef["fid"] = item["fid"]
                        break

            for cf in certi_id_list:
                for item in fid_list:
                    if cf.has_key(fid_matching_obj[item["type"]]):
                        cf["fid"] = item["fid"]
                        break

            print work_id_list

            self.set_work_exp_info_fid(work_id_list)
            self.set_education_info_fid(edu_id_list)
            self.set_certification_info_fid(certi_id_list)

            for item in fid_list:
                _fid = getattr(my_application, item["type"])
                if _fid != 0 and not None:
                    target = self.get_upload_file_path(_fid)
                    if target is not False:
                        self.delete_file(target)
                        self.delete_upload_file_data(_fid)

                setattr(my_application, item["type"], item["fid"])

            my_application.profile_state = 1

            my_application.save()

            return {"isSuccess": 1, "message": "profile info set"}
        except Exception as err:
            print "set_application_profile_info() : " + str(err)
            return {"isSuccess": 0, "message": "set Fail"}

    def set_upload_resume_file_data(self, _ref_id, _files):
        """
        @summary: 튜터 지원 단계중 resume 파일 업로드
        @author: msjang
        @param _ref_id: 파일 테이블 id
        @param _files: 파일 정보
        @return: True, False( boolean )
        """
        fid_list = []
        for item in _files:
            newFile = Files()
            newFile.type = "AP"
            newFile.ref_id = _ref_id
            newFile.filepath = item["filepath"]
            newFile.filename = item["filename"]
            newFile.save()

            fid_list.append({"fid": newFile.fid, "type": item["type"]})

        return fid_list

    def get_upload_file_path(self, _fid):
        """
        @summary: 해당유저 
        @author: msjang
        @param _fid: 파일 id
        @return: file_path( string )
        """
        item = Files.objects.filter(fid=_fid)
        if item.exists():
            target = Files.objects.get(fid=_fid)

            return BASE_DIR + target.filepath + target.filename
        else:
            return False

    def get_upload_resume_file_data(self, _fid):
        """
        @summary: resume 파일 정보
        @author: msjang
        @param _fid: 파일 id
        @return: filename( string )
        """
        if Files.objects.filter(fid=_fid).exists():
            return Files.objects.get(fid=_fid).filename.split("&&")[1]

    def delete_upload_file_data(self, _fid):
        """
        @summary: 업로드한 파일 테이블에서 데이터 삭제
        @author: msjang
        @param _fid: 파일 id
        @return: True, False( boolean )
        """
        if Files.objects.filter(fid=_fid).exists():
            Files.objects.filter(fid=_fid).delete()
            return True
        else:
            return False

    def delete_file(self, filepath):
        """
        @summary: 파일 삭제
        @author: msjang
        @param filepath: 파일 경로
        @return: none
        """
        try:
            if os.path.isfile(filepath):
                os.remove(filepath)
        except Exception as err:
            print "delete file error : " + str(err)

    def get_application_payment_info(self, _user_id):
        """
        @summary: 튜터 지원 단계중 payment 정보
        @author: msjang
        @param _user_id: 유저 id
        @return: obj( object )
        """
        my_application = self.get_application(_user_id)
        if my_application is False:
            raise ObjectDoesNotExist

        obj = {}
        # Profile info
        print "get_application_payment_info"

        payment_info_keys = ["first_name", "last_name", "street_addr", "city", "state", "zip", "country", "phone",
                             "email"]
        for key in payment_info_keys:
            obj[key] = getattr(my_application, key)

        return obj

    def set_application_payment_info(self, _user_id, payment_info):
        """
        @summary: 튜터 지원 단계중 payment 정보 저장
        @author: msjang
        @param _user_id: 유저 id
        @return: object
        """
        my_application = self.get_application(_user_id)
        if my_application is False:
            raise ObjectDoesNotExist

        try:
            print "set_application_payment_info"

            for key in payment_info.keys():
                setattr(my_application, key, payment_info[key])

            my_application.payinfo_state = 1

            my_application.save()

            return {"isSuccess": 1, "message": "payment info set"}
        except Exception as err:
            print "set_application_payment_info() : " + str(err)
            return {"isSuccess": 0, "message": "set Fail"}

    def submit_application(self, _user_id):
        """
        @summary: 지원서 최종 제출
        @author: msjang
        @param _user_id: 유저 id
        @return: obj( object )
        """
        my_application = self.get_application(_user_id)
        if my_application is False:
            raise ObjectDoesNotExist
        try:
            print "set_application_payment_info"

            my_application.submit_state = 1

            my_application.save()

            return {"isSuccess": 1, "message": "submit application"}
        except Exception as err:
            print "submit_application() : " + str(err)
            return {"isSuccess": 0, "message": "submit Fail"}
