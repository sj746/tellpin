# -*- coding: utf-8 -*-
###############################################################################
# filename    : tutor > models.py
# description : User 모델 정의
# author      : hsrjmk@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160831 hsrjmk 최조 작성
#
###############################################################################
from django.db import models
from common.models import CityInfo, CountryInfo, CurrencyInfo, TimezoneInfo, LanguageInfo, SkillLevelInfo
from user_auth.models import File, TellpinUser


TUTOR_DOC_STATUS = (
    (0, 'None'),
    (1, 'Applied'),
    (2, 'Modified'),
    (3, 'Confirmed'),
    (4, 'Rejected'),
)
APPLY_STEP = (
    (0, 'None'),
    (1, 'Personal'),
    (2, 'Profile'),
    (3, 'Payment Info'),
    (4, 'Terms of service'),
)

USER_VIOLATION_STATUS = (
    (0, 'Reported'),
    (1, 'Answered'),
)


class Tutee(models.Model):
    user = models.OneToOneField(TellpinUser, primary_key=True)

    is_visible = models.BooleanField("Visible on language exchange", default=True)

    name = models.CharField("User full name", max_length=50, null=True)
    photo_fid = models.ForeignKey(File, null=True)
    photo_filename = models.CharField("User photo filename", max_length=255, null=True)
    gender = models.IntegerField("Gender: 1: man, 2: woman", null=True)
    display_language = models.IntegerField("Display_language: 0: en-us, 1: ko, 2: ja", null=True)
    birthdate = models.DateField("Date of birth", null=True)
    from_country_id = models.ForeignKey(CountryInfo, related_name='+', db_column='from_country_id', null=True)
    from_country = models.CharField("Country name where is user from", max_length=50, null=True)
    from_city_id = models.ForeignKey(CityInfo, related_name='+', db_column='from_city_id', null=True)
    from_city = models.CharField("City name where is user from", max_length=50, null=True)
    livingin_country_id = models.ForeignKey(CountryInfo, related_name='+', db_column='livingin_country_id', null=True)
    livingin_country = models.CharField("Country name where is user living in", max_length=50, null=True)
    livingin_city_id = models.ForeignKey(CityInfo, related_name='+', db_column='livingin_city_id', null=True)
    livingin_city = models.CharField("City name where is user living in", max_length=50, null=True)
    timezone_id = models.ForeignKey(TimezoneInfo, db_column='timezone_id', null=True)
    timezone = models.CharField("Timezone location", max_length=100, null=True)
    currency_id = models.ForeignKey(CurrencyInfo, db_column='currency_id', null=True)
    currency = models.CharField("Currency code", max_length=3, null=True)

    native1_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='native1_id', null=True)
    native1 = models.CharField("Native language name", max_length=100, null=True)
    native2_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='native2_id', null=True)
    native2 = models.CharField("Native language name", max_length=100, null=True)
    native3_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='native3_id', null=True)
    native3 = models.CharField("Native language name", max_length=100, null=True)

    lang1_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang1_id', null=True)
    lang1 = models.CharField("Speak language name", max_length=100, null=True)
    lang1_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang1_learning = models.NullBooleanField("is learning language", null=True)

    lang2_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang2_id', null=True)
    lang2 = models.CharField("Speak language name", max_length=100, null=True)
    lang2_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang2_learning = models.NullBooleanField("is learning language", null=True)

    lang3_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang3_id', null=True)
    lang3 = models.CharField("Speak language name", max_length=100, null=True)
    lang3_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang3_learning = models.NullBooleanField("is learning language", null=True)

    lang4_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang4_id', null=True)
    lang4 = models.CharField("Speak language name", max_length=100, null=True)
    lang4_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang4_learning = models.NullBooleanField("is learning language", null=True)

    lang5_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang5_id', null=True)
    lang5 = models.CharField("Speak language name", max_length=100, null=True)
    lang5_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang5_learning = models.NullBooleanField("is learning language", null=True)

    lang6_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang6_id', null=True)
    lang6 = models.CharField("Speak language name", max_length=100, null=True)
    lang6_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang6_learning = models.NullBooleanField("is learning language", null=True)

    short_intro = models.CharField("Tutee's short introduction", max_length=250, null=True)
    long_intro = models.CharField("Tutee's long introduction", max_length=2000, null=True)

    skype_id = models.CharField("User id for Skype", max_length=100, null=True)
    facetime_id = models.CharField("User id for Facetime", max_length=100, null=True)
    hangout_id = models.CharField("User id for Hangout", max_length=100, null=True)
    qq_id = models.CharField("User id for QQ", max_length=100, null=True)

    total_balance = models.IntegerField("Total balance of Tutee's wallet", default=0)
    available_balance = models.IntegerField("Available balance of Tutee's wallet", default=0)
    pending_balance = models.IntegerField("Pending balance of Tutee's wallet", default=0)

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True, null=True)

    del_by_user = models.NullBooleanField("Deleted by user", default=False, null=True)
    del_by_admin = models.NullBooleanField("Deleted by administrator", default=False, null=True)

    class Meta:
        db_table = u'tutee'


class Tutor(models.Model):
    user = models.OneToOneField(TellpinUser, primary_key=True)

    is_available = models.NullBooleanField("Available to teach", default=True, null=True)
    has_schedule = models.NullBooleanField("Have schedules to teach", default=False, null=True)
    has_course = models.NullBooleanField("Have courses to teach", default=False, null=True)

    schedule_comment = models.CharField("Tutor's comment for schedule", max_length=2000, null=True)

    name = models.CharField("User full name", max_length=50, null=True)
    photo_fid = models.ForeignKey(File, null=True)
    photo_filename = models.CharField("User photo filename", max_length=255, null=True)
    gender = models.IntegerField("Gender: 1: man, 2: woman", null=True)
    display_language = models.IntegerField("Display_language: 0: en-us, 1: ko, 2: ja", null=True)
    birthdate = models.DateField("Date of birth", null=True)
    from_country_id = models.ForeignKey(CountryInfo, related_name='+', db_column='from_country_id', null=True)
    from_country = models.CharField("Country name where is user from", max_length=50, null=True)
    from_city_id = models.ForeignKey(CityInfo, related_name='+', db_column='from_city_id', null=True)
    from_city = models.CharField("City name where is user from", max_length=50, null=True)
    livingin_country_id = models.ForeignKey(CountryInfo, related_name='+', db_column='livingin_country_id', null=True)
    livingin_country = models.CharField("Country name where is user living in", max_length=50, null=True)
    livingin_city_id = models.ForeignKey(CityInfo, related_name='+', db_column='livingin_city_id', null=True)
    livingin_city = models.CharField("City name where is user living in", max_length=50, null=True)
    timezone_id = models.ForeignKey(TimezoneInfo, db_column='timezone_id', null=True)
    timezone = models.CharField("Timezone location", max_length=100, null=True)
    currency_id = models.ForeignKey(CurrencyInfo, db_column='currency_id', null=True)
    currency = models.CharField("Currency code", max_length=3, null=True)

    native1_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='native1_id', null=True)
    native1 = models.CharField("Native language name", max_length=100, null=True)
    native2_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='native2_id', null=True)
    native2 = models.CharField("Native language name", max_length=100, null=True)
    native3_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='native3_id', null=True)
    native3 = models.CharField("Native language name", max_length=100, null=True)

    lang1_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang1_id', null=True)
    lang1 = models.CharField("Speak language name", max_length=100, null=True)
    lang1_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang1_learning = models.NullBooleanField("is learning language", null=True)

    lang2_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang2_id', null=True)
    lang2 = models.CharField("Speak language name", max_length=100, null=True)
    lang2_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang2_learning = models.NullBooleanField("is learning language", null=True)

    lang3_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang3_id', null=True)
    lang3 = models.CharField("Speak language name", max_length=100, null=True)
    lang3_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang3_learning = models.NullBooleanField("is learning language", null=True)

    lang4_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang4_id', null=True)
    lang4 = models.CharField("Speak language name", max_length=100, null=True)
    lang4_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang4_learning = models.NullBooleanField("is learning language", null=True)

    lang5_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang5_id', null=True)
    lang5 = models.CharField("Speak language name", max_length=100, null=True)
    lang5_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang5_learning = models.NullBooleanField("is learning language", null=True)

    lang6_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang6_id', null=True)
    lang6 = models.CharField("Speak language name", max_length=100, null=True)
    lang6_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang6_learning = models.NullBooleanField("is learning language", null=True)

    teaching1_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='teaching1_id', null=True)
    teaching1 = models.CharField("Language name to teach", max_length=100, null=True)
    teaching2_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='teaching2_id', null=True)
    teaching2 = models.CharField("Language name to teach", max_length=100, null=True)
    teaching3_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='teaching3_id', null=True)
    teaching3 = models.CharField("Language name to teach", max_length=100, null=True)

    # TODO: 필요한 필드인가????     
    how_to_meet = models.CharField("How to meet???", max_length=10, null=True)

    audio_only = models.BooleanField("Using audio only for lesson", default=False)
    skype_id = models.CharField("User id for Skype", max_length=100, null=True)
    facetime_id = models.CharField("User id for Facetime", max_length=100, null=True)
    hangout_id = models.CharField("User id for Hangout", max_length=100, null=True)
    qq_id = models.CharField("User id for QQ", max_length=100, null=True)

    video_intro = models.CharField("Tutor's video introduction", max_length=1000, null=True)
    short_intro = models.CharField("Tutor's short introduction", max_length=250, null=True)
    long_intro = models.CharField("Tutor's long introduction", max_length=2000, null=True)

    work1 = models.ForeignKey('TutorWork', related_name='+', null=True)
    work2 = models.ForeignKey('TutorWork', related_name='+', null=True)
    work3 = models.ForeignKey('TutorWork', related_name='+', null=True)
    education1 = models.ForeignKey('TutorEducation', related_name='+', null=True)
    education2 = models.ForeignKey('TutorEducation', related_name='+', null=True)
    education3 = models.ForeignKey('TutorEducation', related_name='+', null=True)
    certificate1 = models.ForeignKey('TutorCertificate', related_name='+', null=True)
    certificate2 = models.ForeignKey('TutorCertificate', related_name='+', null=True)
    certificate3 = models.ForeignKey('TutorCertificate', related_name='+', null=True)
    paymentinfo = models.ForeignKey('TutorPaymentInfo', related_name='+', null=True)

    has_trial = models.NullBooleanField("Tutor has a trial course", default=False, null=True)
    trial_price = models.IntegerField("Price for a trial course", null=True)

    total_balance = models.IntegerField("Total balance of Tutor's wallet", default=0)
    available_balance = models.IntegerField("Available balance of Tutor's wallet", default=0)
    pending_balance = models.IntegerField("Pending of Tutor's wallet", default=0)
    withdrawal_pending = models.IntegerField("Withdrawal pending", default=0)

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True, null=True)

    del_by_user = models.NullBooleanField("Deleted by user", default=False, null=True)
    del_by_admin = models.NullBooleanField("Deleted by administrator", default=False, null=True)

    class Meta:
        db_table = u'tutor'


class TutorWork(models.Model):
    start_year = models.IntegerField("Year to start work", null=True)
    end_year = models.IntegerField("Year to end work", null=True)
    company = models.CharField("Company where work", max_length=200, null=True)
    country = models.CharField("Country where work", max_length=100, null=True)
    city = models.CharField("City where work", max_length=100, null=True)
    position = models.CharField("Position", max_length=100, null=True)
    description = models.CharField("Description", max_length=500, null=True)
    fid = models.ForeignKey(File, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'tutor_work'


class TutorEducation(models.Model):
    start_year = models.IntegerField("Year to start education", null=True)
    end_year = models.IntegerField("Year to end education", null=True)
    school = models.CharField("School where education", max_length=200, null=True)
    location = models.CharField("Location where education", max_length=100, null=True)
    degree = models.CharField("Degree", max_length=100, null=True)
    major = models.CharField("Major", max_length=100, null=True)
    description = models.CharField("Description", max_length=500, null=True)
    fid = models.ForeignKey(File, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)
    
    class Meta:
        db_table = u'tutor_education'


class TutorCertificate(models.Model):
    year = models.IntegerField("Year to certificate", null=True)
    name = models.CharField("Certificate name", max_length=100, null=True)
    organization = models.CharField("Certificate organization", max_length=100, null=True)
    description = models.CharField("Description", max_length=500, null=True)
    fid = models.ForeignKey(File, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)

    class Meta:
        db_table = u'tutor_certificate'


class TutorPaymentInfo(models.Model):
    first_name = models.CharField("Tutor's first name", max_length=50, null=True)
    last_name = models.CharField("Tutor's last name", max_length=50, null=True)
    street_address = models.CharField("Street address", max_length=50, null=True)
    city = models.CharField("City", max_length=50, null=True)
    state_province_region = models.CharField("State, province, region", max_length=100, null=True)
    zip_code = models.CharField("Zip code", max_length=30, null=True)
    country_region = models.ForeignKey(CountryInfo, null=True)
    phone = models.CharField("Phone number", max_length=30, null=True)
    email = models.CharField("Email", max_length=100, null=True)

    paypal = models.CharField("Paypal id", max_length=80, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)

    class Meta:
        db_table = u'tutor_paymentinfo'


class TutorDoc(models.Model):
    user = models.ForeignKey(TellpinUser, null=True)

    status = models.PositiveSmallIntegerField("Document processing status:", choices=TUTOR_DOC_STATUS, default=0)

    apply_step = models.PositiveSmallIntegerField("Apply step:", choices=APPLY_STEP, default=0)

    lang_confirmed = models.BooleanField("Language change confirmed", default=False)
    intro_confirmed = models.BooleanField("Introduce change confirmed", default=False)
    resume_confirmed = models.BooleanField("Resume change confirmed", default=False)

    modify_type = models.IntegerField("Modify type: language(1), introduce(2), resume(3)", default=0)
    lang_modify = models.CharField("", max_length=3, null=True)
    intro_modify = models.CharField("---: 000 (video, short, long)", max_length=3, null=True)
    resume_modify = models.CharField(": 000000000", max_length=9, null=True)

    name = models.CharField("User full name", max_length=50, null=True)
    photo_fid = models.ForeignKey(File, null=True)
    photo_filename = models.CharField("User photo filename", max_length=255, null=True)
    gender = models.IntegerField("Gender: 1: man, 2: woman", null=True)
    birthdate = models.DateField("Date of birth", null=True)
    from_country_id = models.ForeignKey(CountryInfo, related_name='+', db_column='from_country_id', null=True)
    from_country = models.CharField("Country name where is user from", max_length=50, null=True)
    from_city_id = models.ForeignKey(CityInfo, related_name='+', db_column='from_city_id', null=True)
    from_city = models.CharField("City name where is user from", max_length=50, null=True)
    livingin_country_id = models.ForeignKey(CountryInfo, related_name='+', db_column='livingin_country_id', null=True)
    livingin_country = models.CharField("Country name where is user living in", max_length=50, null=True)
    livingin_city_id = models.ForeignKey(CityInfo, related_name='+', db_column='livingin_city_id', null=True)
    livingin_city = models.CharField("City name where is user living in", max_length=50, null=True)
    timezone_id = models.ForeignKey(TimezoneInfo, db_column='timezone_id', null=True)
    timezone = models.CharField("Timezone location", max_length=100, null=True)
    currency_id = models.ForeignKey(CurrencyInfo, db_column='currency_id', null=True)
    currency = models.CharField("Currency code", max_length=3, null=True)

    native1_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='native1_id', null=True)
    native1 = models.CharField("Native language name", max_length=100, null=True)
    native2_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='native2_id', null=True)
    native2 = models.CharField("Native language name", max_length=100, null=True)
    native3_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='native3_id', null=True)
    native3 = models.CharField("Native language name", max_length=100, null=True)

    lang1_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang1_id', null=True)
    lang1 = models.CharField("Speak language name", max_length=100, null=True)
    lang1_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang1_learning = models.NullBooleanField("is learning language", null=True)

    lang2_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang2_id', null=True)
    lang2 = models.CharField("Speak language name", max_length=100, null=True)
    lang2_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang2_learning = models.NullBooleanField("is learning language", null=True)

    lang3_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang3_id', null=True)
    lang3 = models.CharField("Speak language name", max_length=100, null=True)
    lang3_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang3_learning = models.NullBooleanField("is learning language", null=True)

    lang4_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang4_id', null=True)
    lang4 = models.CharField("Speak language name", max_length=100, null=True)
    lang4_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang4_learning = models.NullBooleanField("is learning language", null=True)

    lang5_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang5_id', null=True)
    lang5 = models.CharField("Speak language name", max_length=100, null=True)
    lang5_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang5_learning = models.NullBooleanField("is learning language", null=True)
 
    lang6_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='lang6_id', null=True)
    lang6 = models.CharField("Speak language name", max_length=100, null=True)
    lang6_level = models.ForeignKey(SkillLevelInfo, related_name='+', null=True)
    lang6_learning = models.NullBooleanField("is learning language", null=True)

    teaching1_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='teaching1_id', null=True)
    teaching1 = models.CharField("Language name to teach", max_length=100, null=True)
    teaching2_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='teaching2_id', null=True)
    teaching2 = models.CharField("Language name to teach", max_length=100, null=True)
    teaching3_id = models.ForeignKey(LanguageInfo, related_name='+', db_column='teaching3_id', null=True)
    teaching3 = models.CharField("Language name to teach", max_length=100, null=True)

    # TODO: 필요한 필드인가????     
    how_to_meet = models.CharField("How to meet???: video, voice, offline", max_length=10, null=True)

    audio_only = models.BooleanField("Using audio only for lesson", default=False)
    skype_id = models.CharField("User id for Skype", max_length=100, null=True)
    facetime_id = models.CharField("User id for Facetime", max_length=100, null=True)
    hangout_id = models.CharField("User id for Hangout", max_length=100, null=True)
    qq_id = models.CharField("User id for QQ", max_length=100, null=True)

    video_intro = models.CharField("Tutor's video introduction", max_length=1000, null=True)
    short_intro = models.CharField("Tutor's short introduction", max_length=250, null=True)
    long_intro = models.CharField("Tutor's long introduction", max_length=2000, null=True)

    work1 = models.ForeignKey('TutorWork', related_name='+', null=True)
    work2 = models.ForeignKey('TutorWork', related_name='+', null=True)
    work3 = models.ForeignKey('TutorWork', related_name='+', null=True)
    education1 = models.ForeignKey('TutorEducation', related_name='+', null=True)
    education2 = models.ForeignKey('TutorEducation', related_name='+', null=True)
    education3 = models.ForeignKey('TutorEducation', related_name='+', null=True)
    certificate1 = models.ForeignKey('TutorCertificate', related_name='+', null=True)
    certificate2 = models.ForeignKey('TutorCertificate', related_name='+', null=True)
    certificate3 = models.ForeignKey('TutorCertificate', related_name='+', null=True)
    paymentinfo = models.ForeignKey('TutorPaymentInfo', related_name='+', null=True)

    check_agree = models.BooleanField("Check agreement", default=False)

    applied_time = models.DateTimeField("Applied time", auto_now_add=True, null=True)
    modified_time = models.DateTimeField("Modified time", auto_now_add=True, null=True)
    confirmed_time = models.DateTimeField("Confirmed time", auto_now_add=True, null=True)
    rejected_time = models.DateTimeField("Rejected time", auto_now_add=True, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True, null=True)

    class Meta:
        db_table = u'tutor_doc'


class Bookmark(models.Model):
    user = models.ForeignKey(TellpinUser, null=True)
    tutor = models.ForeignKey('Tutor', null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'bookmark'


class Friend(models.Model):
    req_user = models.ForeignKey(TellpinUser, related_name='+')
    res_user = models.ForeignKey(TellpinUser, related_name='+')
    status = models.IntegerField("Friend status: 1: request, 2: response, 3: canceled, 4: rejected", null=True)
    message = models.CharField("Message to send in order to request", max_length=500, null=True)
    req_time = models.DateTimeField("Time for request", null=True)
    res_time = models.DateTimeField("Time for response", null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)

    class Meta:
        db_table = u'friend'


class FriendHide(models.Model):
    user = models.ForeignKey(TellpinUser, related_name='+')
    hidden_user = models.ForeignKey(TellpinUser, related_name='+')

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)

    class Meta:
        db_table = u'friend_hide'


class FriendBlock(models.Model):
    user = models.ForeignKey(TellpinUser, related_name='+')
    blocked_user = models.ForeignKey(TellpinUser, related_name='+')

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)

    class Meta:
        db_table = u'friend_block'

class UserViolation(models.Model):
    reporter = models.ForeignKey(TellpinUser, related_name='+')
    reportee = models.ForeignKey(TellpinUser, related_name='+')
    type = models.IntegerField("Violation type: 1:허위,과장, 2: 무관한 내용, 3: 선정성, 폭력성, 9: 기타", null=True)
    content = models.CharField("Violation content", max_length=1000, null=True)

    status = models.SmallIntegerField("Violation status", choices=USER_VIOLATION_STATUS, default=0)
    checked_time = models.DateTimeField("Checked time", auto_now=True, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)
 
    class Meta:
        db_table = u'user_violation'


class TutorViolation(models.Model):
    reporter = models.ForeignKey(TellpinUser, related_name='+')
    reportee = models.ForeignKey(TellpinUser, related_name='+')
    type = models.IntegerField("Violation type: 1:허위,과장, 2: 무관한 내용, 3: 선정성, 폭력성, 9: 기타", null=True)
    content = models.CharField("Violation content", max_length=1000, null=True)

    is_checked = models.BooleanField("Violation check", default=False)
    checked_time = models.DateTimeField("Checked time", auto_now=True, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True, null=True)
 
    class Meta:
        db_table = u'tutor_violation'
