# -*- coding: utf-8 -*-

###############################################################################
# filename    : tutor > service.py
# description : tutor 관련 service.py
# author      : msjang@unichal.com
#
# history
# -----------------------------------------------------------------------------
# 20160603 msjang 최초 작성
#
#
###############################################################################

from collections import namedtuple
from datetime import datetime, timedelta, date
import json
import math
import operator
import os
import time
import urllib2

from MySQLdb.constants.FIELD_TYPE import NULL
from dateutil.relativedelta import relativedelta
from django.contrib.sessions.models import Session
from django.core.paginator import EmptyPage, Page, Paginator
from django.core.serializers.json import DjangoJSONEncoder
from django.db import connection, transaction
from django.db.models.aggregates import Min, Max, Count, Sum, Avg
from django.db.models.query_utils import Q
from django.forms.models import model_to_dict
from django.utils import timezone

from common import common
from common.common import getTuteeTotalAvailable, getTuteeTotalBalance, getTuteeTotalPending, getTutorTotalAvailable, \
    getTutorTotalBalance, getTutorTotalPending, \
    getTutorTotalWithPending, get_login_users
from common.common import send_noti_email
from common.models import LanguageInfo, CountryInfo, SkillLevelInfo, CurrencyInfo, \
    TimezoneInfo, CityInfo
from common.service import CommonService
from people.service import PeopleService
from tellpin.settings import BASE_DIR
from tutor.models import TutorDoc, Tutee, TutorWork, TutorEducation, TutorCertificate, TutorPaymentInfo, Tutor, Bookmark, \
    TutorViolation, UserViolation
from tutoring.models import TutorScheduleWeek, TutorCourse, LessonClaim, LessonFeedback, \
    LessonReservation, LessonHistory, LessonHistoryText, LessonClaimText, \
    LessonMessage
from user_auth.models import TellpinUser, TellpinAuthUser
from user_auth.utils import encrypt_password
from dashboard.models import Notification

# from common.models import TutorAccount, TuteeAccount
# from tellpin.settings import TELLPIN_HOME
# from common.models import SkillLevel
# from common.models import NationInfo, CityInfo, Lesson, Ratings, \
#     Reservation, Specialtag, SpecialtagInfo, Profile, Tools, ToolsInfo, Follow, \
#     Resume, Videolesson, Schedule, Message, WeekSchedule, RV_Message, \
#     TutorReport, TutorHide, Payments, Feedback, StatusHistory, HistoryInfo, Problem, Notification
# from common.models import TellpinUser, Language, DjangoSession, Timezone
# from wallet.models import Coin
class TutorService1(object):
    '''
    classdocs
    '''
    LESSON_TITLE = [
        "",
        "A new lesson request for you. Please approve or decline it by %s",
        "Your lesson request has been approved.",
        "Your lesson request has been declined.",
        "",
        "A new lesson request for you. Please approve or decline it by %s",
        "lesson time passed",
        "The tutee confirmed that the lesson went well and left a feedback for you.",
        "Problem reported from %s",
        "Problem reported from %s",
        "",
        "Tellpin Help Center made a decision on the lesson in dispute.",
        "The tutor left a feedback for you."
    ]
    LESSON_CONTEXT = [
        "",
        "<p> There is a new lesson request for you. <a href=\'%s\'\>Please approve or decline it by %s </a></p>",
        "<p> As the tutor approved your lesson request, there is an upcoming lesson for you!<br><a href=\'%s\'>View details</a></p>",
        "<p>The tutor declined your lesson request. <a href=\'%s\'>View details</a></p>",
        "",
        "<p> There is a new lesson request for you. <a href=\'%s\'>Please approve or decline it by %s </a></p>",
        "<p>lesson time passed</p>",
        "<p>The tutee confirmed that the lesson went well and left a feedback for you. <a href=\'%s\'>Check this out </a></p>",
        "<p>\'%s\' has stated that there was a problem. Please <a href=\'%s\'>respond to this</a> as soon as possible. </p>",
        "<p>\'%s\' has stated that there was a problem. Please <a href=\'%s\'>respond to this</a> as soon as possible. </p>",
        "",
        "<p>Tellpin Help Center made a decision on the lesson in dispute. " \
        "We concluded that the lesson status should be changed as 'confirmed or Unscheduled or Refunded ( 관리자 처리 개발후 처리)'." \
        "'Confirmed' means that the tutor gets all the lesson credits for the lesson." \
        "If you want to raise an objection to this, please send us your detail by support@tellpin.com immediately.</p>",
        "<p>The tutor left a feedback for you. <a href=\'%s\'>Check this out</a></p>"
    ]
    LESSON_CONTEXT2 = [
        "Tutor : <a href=\'%s\'>%s</a> (User ID : %s) <br>" \
        "Tutee : <a href=\'%s\'>%s</a> (User ID : %s) <br>" \
        "Course name : %s <br>" \
        "Lesson time : %s <br>" \
        "Duration : %s mins <br>" \
        "Reservation ID : %s <br>",
        "<a href=\'%s\'> Rearrange it with a new date/time</a> or <a href=\'%s\'> make a new appointment with another tutor. </a>"
    ]

    NATION_FLAG_FILE_DIR = "/static/img/nations"
    PROFILE_IMAGE_FILE_DIR = "/static/img/upload/profile"
    PROBLEM_IMAGE_FILE_DIR = "/static/img/upload/problem"
    RESUME_FILE_DIR = "/static/img/upload/resume"
    RESUME_TEMP_FILE_DIR = "/static/img/upload/resume/temp"

    VL_PERPAGE = 3
    RT_PERPAGE = 5

    def __init__(self, params):
        '''
        Constructor
        '''

   
    # get user ID
    

    # get user ID
    def get_user_timezone_location(self, _email):
        """
        @summary: 유저 타임존 정보
        @author: msjang
        @param _email: 유저 email
        @return: user.timezone( int )
        """
        # 사용자 id 얻기
        try:
            return TellpinUser.objects.get(email=_email).timezone
        except:
            return None



    def dictfetchall(self, cursor):
        """
        @summary: Return all rows from a cursor as a dict
        @author: msjang
        @param cursor: DB cursor
        @return: dictionary
        """
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
            ]

        # 검색 데이터

    def get_search_data(self):
        """
        @summary: 튜터목록 검색 기본 data
        @author: msjang
        @param cursor: DB cursor
        @return: result( list )
        """
        try:
            result = []
            obj = {}

            obj["language_list"] = []
            for item in LanguageInfo.objects.all():
                item_obj = {}
                item_obj["id"] = item.id
                item_obj["language"] = item.lang_english
                obj["language_list"].append(item_obj)

            obj["nation_info"] = []
            for nation in CountryInfo.objects.all():
                nat_obj = {}
                nat_obj["nation"] = nation.country
                nat_obj["nation_id"] = nation.id
                nat_obj["citylist"] = []
                obj["nation_info"].append(nat_obj)

            obj["tool_list"] = []
            for item in ["skype", "facetime", "hangout", "qq"]:
                obj["tool_list"].append(item)

            # obj["tag_list"] = []
            # for item in SpecialtagInfo.objects.all():
            #     item_obj = {}
            #     item_obj["stname"] = item.stname
            #     item_obj["stid"] = item.stid
            #     obj["tag_list"].append(item_obj)

            result.append(obj)
        except Exception as err:
            print str(err)

        return result

    def get_city_name(self, _nid):
        """
        @summary: 도시 이름 정보
        @author: msjang
        @param _nid: Nation table pk
        @return: nat_obj( object )
        """
        nat_obj = {}
        nat_obj["citylist"] = []
        for city in CityInfo.objects.filter(nid=_nid):
            city_obj = {}
            city_obj["city_id"] = city.city_id
            city_obj["city"] = city.name
            nat_obj["citylist"].append(city_obj)

        return nat_obj

    def get_skill_level_name(self, skill_level):
        """
        @summary: language skill level name 정보
        @author: msjang
        @param skill_level: Skill_level table pk
        @return: skill_level_name( string )
        """
        skill_level_name = ""
        if skill_level >= 1 and skill_level <= 7:
            return SkillLevel.objects.get(level_id=skill_level).level_name[3:]

        return skill_level_name

    """
    ajax_listup_tutors 함수로 통합 예정

         조건 검색하는 경우 ajax_listup_tutors (view 검색) 에서  tutor list를 filtering 후
    listup_tutors 함수에서 상세 검색을 하고 있다.

    -> 하나의 함수로 통합해야 함

    """

    # view tutor list
    def view_get_tutors(self, search_type, search_list, hide_list, user_id=None):
        result = []
        try:
            count_t = time.time()
            cursor = connection.cursor()
            if search_list:
                search_last = len(search_list) - 1
                search_tutor_list = " AND id IN ("

                for i, search in enumerate(search_list):
                    search_tutor_list += str(search)
                    if i != search_last:
                        search_tutor_list += ", "

                search_tutor_list += ")"

            if hide_list:
                hide_last = len(hide_list) - 1
                hide_tutor_list = " AND id NOT IN ("

                for i, hide in enumerate(hide_list):
                    hide_tutor_list += str(hide)
                    if i != hide_last:
                        hide_tutor_list += ", "

                hide_tutor_list += ")"
            else:
                hide_tutor_list = ""

            if search_type == 1:
                sql = """
                    SELECT *
                    FROM view_user_info
                    WHERE is_tutor = 1
                """
                sql += search_tutor_list
                sql += hide_tutor_list
                cursor.execute(sql)
            else:
                sql = """
                    SELECT *
                    FROM view_user_info
                    WHERE is_tutor = 1
                """
                sql += hide_tutor_list
                cursor.execute(sql)

            result = self.dictfetchall(cursor)
            sql = """
                SELECT language1 AS name
                     , t.id AS user_id
                     , 1 AS is_native
                     , ( CASE WHEN native1 IN ( teaching1, teaching2, teaching3 ) THEN 1 ELSE 0 END ) AS is_teaching
                     , 0 AS is_learning
                     , 1 AS is_speaks
                     , 7 AS level
                FROM user_auth_tellpinuser t, user_auth_language l
                WHERE t.native1 = l.id
                  AND t.id = %s
                UNION
                SELECT language1 AS name
                     , t.id AS user_id
                     , 1 AS is_native
                     , ( CASE WHEN native2 IN ( teaching1, teaching2, teaching3 ) THEN 1 ELSE 0 END ) AS is_teaching
                     , 0 AS is_learning
                     , 1 AS is_speaks
                     , 7 AS level
                FROM user_auth_tellpinuser t, user_auth_language l
                WHERE t.native2 = l.id
                  AND t.id = %s
                UNION
                SELECT language1 AS name
                     , t.id AS user_id
                     , 1 AS is_native
                     , ( CASE WHEN native3 IN ( teaching1, teaching2, teaching3 ) THEN 1 ELSE 0 END ) AS is_teaching
                     , 0 AS is_learning
                     , 1 AS is_speaks
                     , 7 AS level
                FROM user_auth_tellpinuser t, user_auth_language l
                WHERE t.native3 = l.id
                  AND t.id = %s
                UNION
                SELECT language1 AS name
                     , t.id AS user_id
                     , 0 AS is_native
                     , ( CASE WHEN other1 IN ( teaching1, teaching2, teaching3 ) THEN 1 ELSE 0 END ) AS is_teaching
                     , 0 AS is_learning
                     , 1 AS is_speaks
                     , other1_skill_level AS level
                FROM user_auth_tellpinuser t, user_auth_language l
                WHERE t.other1 = l.id
                  AND t.id = %s
                UNION
                SELECT language1 AS name
                     , t.id AS user_id
                     , 0 AS is_native
                     , ( CASE WHEN other2 IN ( teaching1, teaching2, teaching3 ) THEN 1 ELSE 0 END ) AS is_teaching
                     , 0 AS is_learning
                     , 1 AS is_speaks
                     , other2_skill_level AS level
                FROM user_auth_tellpinuser t, user_auth_language l
                WHERE t.other2 = l.id
                  AND t.id = %s
                UNION
                SELECT language1 AS name
                     , t.id AS user_id
                     , 0 AS is_native
                     , ( CASE WHEN other3 IN ( teaching1, teaching2, teaching3 ) THEN 1 ELSE 0 END ) AS is_teaching
                     , 0 AS is_learning
                     , 1 AS is_speaks
                     , other3_skill_level AS level
                FROM user_auth_tellpinuser t, user_auth_language l
                WHERE t.other3 = l.id
                  AND t.id = %s
                UNION
                SELECT language1 AS name
                     , t.id AS user_id
                     , 0 AS is_native
                     , ( CASE WHEN learning1 IN ( teaching1, teaching2, teaching3 ) THEN 1 ELSE 0 END ) AS is_teaching
                     , 1 AS is_learning
                     , 0 AS is_speaks
                     , learning1_skill_level AS level
                FROM user_auth_tellpinuser t, user_auth_language l
                WHERE t.learning1 = l.id
                  AND t.id = %s
                UNION
                SELECT language1 AS name
                     , t.id AS user_id
                     , 0 AS is_native
                     , ( CASE WHEN learning2 IN ( teaching1, teaching2, teaching3 ) THEN 1 ELSE 0 END ) AS is_teaching
                     , 1 AS is_learning
                     , 0 AS is_speaks
                     , learning2_skill_level AS level
                FROM user_auth_tellpinuser t, user_auth_language l
                WHERE t.learning2 = l.id
                  AND t.id = %s
                UNION
                SELECT language1 AS name
                     , t.id AS user_id
                     , 0 AS is_native
                     , ( CASE WHEN learning3 IN ( teaching1, teaching2, teaching3 ) THEN 1 ELSE 0 END ) AS is_teaching
                     , 1 AS is_learning
                     , 0 AS is_speaks
                     , learning3_skill_level AS level
                FROM user_auth_tellpinuser t, user_auth_language l
                WHERE t.learning3 = l.id
                  AND t.id = %s
                ORDER BY level DESC
            """
            online_list = get_login_users()
            if user_id:
                follow_list = Follow.objects.filter(follower_id=user_id)
            for result_item in result:
                cursor.execute(sql, [result_item['id'], result_item['id'], result_item['id'],
                                     result_item['id'], result_item['id'], result_item['id'],
                                     result_item['id'], result_item['id'], result_item['id']])
                result_item['languages'] = self.dictfetchall(cursor)
                result_item['price_unit'] = 'KRW'  # 임시 단위저장
                if result_item['email'] in online_list:
                    result_item['is_login'] = 1
                else:
                    result_item['is_login'] = 0
                for follow in follow_list:
                    if result_item['id'] == follow.followee_id:
                        result_item['is_follower'] = 1
                        break
                    else:
                        result_item['is_follower'] = 0

        except Exception as e:
            print str(e)
        finally:
            cursor.close()
            print 'Performance Time : %.02f' % (time.time() - count_t)

            return result

    # 튜터 리스트업
    def listup_tutors(self, userID, search_keys=[], _type=0, _data=[], cur_page=0):
        """
        @summary: tutor listup
        @author: msjang
        @param userID: 로그인 유저 id 번호
        @param search_keys: 검색조건
        @param _type: 로그인, 비로그인
        @param cur_page: page 번호
        @return: page_info( object )
        """
        from django.core.paginator import Paginator
        count_t = time.time()

        # online_list = []
        if cur_page == 0:
            itemCount = 20
        else:
            itemCount = 20  # Show more 개수
        # online_list = get_login_users()
        '''
        for item in DjangoSession.objects.all():
            session = Session.objects.get(session_key=item.session_key)
            uid = session.get_decoded().get('email')
            if uid:
                online_list.append(uid)
        print online_list
        print 'test _data'
        print _data
        if len(_data) > 0 :
            print _data[0]['is_login']
            print _data[0]['is_native']
        return
        '''
        # print 'listsup_tutors'
        # print cur_page
        hide_list = []
        if userID is not None:
            for h in TutorHide.objects.filter(tutee_id=userID):
                hide_list.append(h.tutor_id)

        print search_keys
        try:
            # print 'try..????'
            if cur_page == 0:
                if _type == 1:
                    # tutorlist = TellpinUser.objects.filter(id__in=search_keys).exclude(id__in=hide_list).order_by(
                    #     "updated_time")
                    tutorlist = self.view_get_tutors(_type, search_keys, hide_list, userID)
                else:
                    # tutorlist = TellpinUser.objects.all().exclude(id__in=hide_list).order_by("updated_time")
                    tutorlist = self.view_get_tutors(_type, search_keys, hide_list, userID)
                p = Paginator(tutorlist, itemCount)
                # print p.num_pages
                # print len(tutorlist)
                last_page = p.num_pages
                try:
                    tutorlist = p.page(cur_page + 1)
                except EmptyPage:
                    tutorlist = p.page(p.num_pages)
            else:
                if _type == 1:
                    # print hide_list, search_keys
                    # tutorlist = TellpinUser.objects.filter(id__in=search_keys).exclude(id__in=hide_list).order_by(
                    #     "updated_time")
                    tutorlist = self.view_get_tutors(_type, search_keys, hide_list, userID)
                    # print len(tutorlist)
                    # print tutorlist[0].id
                    # print "total" , Tutor.objects.filter(tid__in = search_keys).exclude(tid__in=hide_list).count()
                    # print _type, 'list', tutorlist

                    p = Paginator(tutorlist, itemCount)
                    last_page = p.num_pages
                    try:
                        tutorlist = p.page(cur_page + 1)
                    except EmptyPage:
                        tutorlist = p.page(p.num_pages)

                else:
                    # tutorlist = TellpinUser.objects.all().exclude(id__in=hide_list).order_by("updated_time")
                    tutorlist = self.view_get_tutors(_type, search_keys, hide_list, userID)
                    # print "total" , Tutor.objects.all().exclude(tid__in=hide_list).count()
                    # print _type, 'list', tutorlist
                    p = Paginator(tutorlist, itemCount)
                    last_page = p.num_pages

                    try:
                        tutorlist = p.page(cur_page + 1)
                    except EmptyPage:
                        tutorlist = p.page(p.num_pages)
            # result = []
            result = tutorlist.object_list
            # print 'tutorlist:'
            # print tutorlist

            # for item in tutorlist:
            #     # print item.id, item.email, item.name
            #     # if len(_data) > 0 :
            #     # if _data[0]['is_login'] and TellpinUser.objects.get(id=item.user_id).email in online_list:
            #     obj = {}
            #     # print 'test2'
            #     # print TellpinUser.objects.get(id=item.user_id).email
            #     # print online_list
            #     if len(_data) > 0:
            #         if _data[0]['is_login']:
            #             if TellpinUser.objects.get(id=item.id).email not in online_list:
            #                 continue
            #     obj["tid"] = item.id
            #     if TellpinUser.objects.get(id=item.id).email in online_list:
            #         obj["is_login"] = 1
            #     else:
            #         obj["is_login"] = 0
            #     obj["profile_photo"] = self.PROFILE_IMAGE_FILE_DIR + "/" + item.profile_photo
            #     obj["name"] = item.name
            #     # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #     try:
            #         obj["nation_from"] = NationInfo.objects.get(nid=item.from_field).name_en
            #     except:
            #         obj["nation_from"] = ""
            #     try:
            #         # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #         obj["city_from"] = CityInfo.objects.get(city_id=item.from_city).name
            #     except:
            #         obj["city_from"] = ""
            #     # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #     try:
            #         obj["nation_live"] = NationInfo.objects.get(nid=item.livein).name_en
            #     except:
            #         obj["nation_live"] = ""
            #     try:
            #         # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #         obj["city_live"] = CityInfo.objects.get(city_id=item.livein_city).name
            #     except:
            #         obj["city_live"] = ""
            #     # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #     try:
            #         obj["nation_flag"] = self.NATION_FLAG_FILE_DIR + "/" + NationInfo.objects.get(
            #             nid=item.from_field).image
            #     except:
            #         obj["nation_flag"] = ""
            #     print '기본정보 Time : %.02f' % (time.time() - count_t)
            #     # speak, teach
            #     my_speak_list = []
            #     # print 'speaks'
            #     if item.native1 is not None:
            #         spk_item = {}
            #         # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #         spk_item["name"] = Language.objects.get(id=item.native1).language1
            #         spk_item["is_native"] = 1
            #         spk_item["level"] = 7
            #         my_speak_list.append(spk_item)
            #     if item.native2 is not None:
            #         spk_item = {}
            #         # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #         spk_item["name"] = Language.objects.get(id=item.native2).language1
            #         spk_item["is_native"] = 1
            #         spk_item["level"] = 7
            #         my_speak_list.append(spk_item)
            #     if item.native3 is not None:
            #         spk_item = {}
            #         # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #         spk_item["name"] = Language.objects.get(id=item.native3).language1
            #         spk_item["is_native"] = 1
            #         spk_item["level"] = 7
            #         my_speak_list.append(spk_item)
            #
            #     # print 'others'
            #     if item.other1 is not None:
            #         spk_item = {}
            #         # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #         spk_item["name"] = Language.objects.get(id=item.other1).language1
            #         spk_item["is_native"] = 0
            #         spk_item["level"] = item.other1_skill_level
            #         spk_item["level_name"] = self.get_skill_level_name(item.other1_skill_level)
            #         my_speak_list.append(spk_item)
            #     if item.other2 is not None:
            #         spk_item = {}
            #         # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #         spk_item["name"] = Language.objects.get(id=item.other2).language1
            #         spk_item["is_native"] = 0
            #         spk_item["level"] = item.other2_skill_level
            #         spk_item["level_name"] = self.get_skill_level_name(item.other2_skill_level)
            #         my_speak_list.append(spk_item)
            #     if item.other3 is not None:
            #         spk_item = {}
            #         # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #         spk_item["name"] = Language.objects.get(id=item.other3).language1
            #         spk_item["is_native"] = 0
            #         spk_item["level"] = item.other3_skill_level
            #         spk_item["level_name"] = self.get_skill_level_name(item.other3_skill_level)
            #         my_speak_list.append(spk_item)
            #
            #     obj["speaks"] = my_speak_list
            #
            #     '''
            #     for lang in Speaks.objects.filter(tid=tutor_info.tid):
            #         if lang.language_id > 11 or lang.language_id < 1: continue
            #         spk_item = {}
            #         spk_item["name"] = Language.objects.get(id=lang.language_id).language1
            #         spk_item["level"] = lang.skill_level
            #         my_speak_list.append(spk_item)
            #     obj["speaks"] = my_speak_list
            #     '''
            #
            #     # print 'teaches'
            #     my_teach_list = []
            #     if item.teaching1 is not None:
            #         tch_item = {}
            #         # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #         tch_item["name"] = Language.objects.get(id=item.teaching1).language1
            #         tch_item["is_native"] = 0
            #         if item.teaching1 == item.native1 or item.teaching1 == item.native2 or item.teaching1 == item.native3:
            #             tch_item["is_native"] = 1
            #         tch_item["level"] = 7
            #         tch_item["level_name"] = "native"
            #         if item.teaching1 == item.other1:
            #             tch_item["level"] = item.other1_skill_level
            #             tch_item["level_name"] = self.get_skill_level_name(item.other1_skill_level)
            #         elif item.teaching1 == item.other2:
            #             tch_item["level"] = item.other2_skill_level
            #             tch_item["level_name"] = self.get_skill_level_name(item.other2_skill_level)
            #         elif item.teaching1 == item.other3:
            #             tch_item["level"] = item.other3_skill_level
            #             tch_item["level_name"] = self.get_skill_level_name(item.other3_skill_level)
            #         my_teach_list.append(tch_item)
            #     if item.teaching2 is not None:
            #         tch_item = {}
            #         # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #         tch_item["name"] = Language.objects.get(id=item.teaching2).language1
            #         tch_item["is_native"] = 0
            #         if item.teaching2 == item.native1 or item.teaching2 == item.native2 or item.teaching2 == item.native3:
            #             tch_item["is_native"] = 1
            #         tch_item["level"] = 7
            #         tch_item["level_name"] = "native"
            #         if item.teaching2 == item.other1:
            #             tch_item["level"] = item.other1_skill_level
            #             tch_item["level_name"] = self.get_skill_level_name(item.other1_skill_level)
            #         elif item.teaching2 == item.other2:
            #             tch_item["level"] = item.other2_skill_level
            #             tch_item["level_name"] = self.get_skill_level_name(item.other2_skill_level)
            #         elif item.teaching2 == item.other3:
            #             tch_item["level"] = item.other3_skill_level
            #             tch_item["level_name"] = self.get_skill_level_name(item.other3_skill_level)
            #         my_teach_list.append(tch_item)
            #     if item.teaching3 is not None:
            #         tch_item = {}
            #         # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #         tch_item["name"] = Language.objects.get(id=item.teaching3).language1
            #         tch_item["is_native"] = 0
            #         if item.teaching3 == item.native1 or item.teaching3 == item.native2 or item.teaching3 == item.native3:
            #             tch_item["is_native"] = 1
            #         tch_item["level"] = 7
            #         if item.teaching3 == item.other1:
            #             tch_item["level"] = item.other1_skill_level
            #             tch_item["level_name"] = self.get_skill_level_name(item.other1_skill_level)
            #         elif item.teaching3 == item.other2:
            #             tch_item["level"] = item.other2_skill_level
            #             tch_item["level_name"] = self.get_skill_level_name(item.other2_skill_level)
            #         elif item.teaching3 == item.other3:
            #             tch_item["level"] = item.other3_skill_level
            #             tch_item["level_name"] = self.get_skill_level_name(item.other3_skill_level)
            #         my_teach_list.append(tch_item)
            #
            #     obj["teaches"] = my_teach_list
            #
            #     # learning
            #     my_learning_list = []
            #     if item.learning1 is not None:
            #         learning_item = {}
            #         # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #         learning_item["name"] = Language.objects.get(id=item.learning1).language1
            #         learning_item["is_native"] = 0
            #         learning_item["level"] = item.learning1_skill_level
            #         learning_item["level_name"] = self.get_skill_level_name(item.learning1_skill_level)
            #         my_learning_list.append(learning_item)
            #     if item.learning2 is not None:
            #         # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #         learning_item["name"] = Language.objects.get(id=item.learning2).language1
            #         learning_item["is_native"] = 0
            #         learning_item["level"] = item.learning2_skill_level
            #         learning_item["level_name"] = self.get_skill_level_name(item.learning2_skill_level)
            #
            #         my_learning_list.append(learning_item)
            #     if item.learning3 is not None:
            #         learning_item = {}
            #         learning_item["name"] = Language.objects.get(id=item.learning3).language1
            #         learning_item["is_native"] = 0
            #         learning_item["level"] = item.learning3_skill_level
            #         learning_item["level_name"] = self.get_skill_level_name(item.learning3_skill_level)
            #
            #         my_learning_list.append(learning_item)
            #
            #     obj["learnings"] = my_learning_list
            #     print '언어정보 Time : %.02f' % (time.time() - count_t)
            #
            #     '''
            #     for lang in Teaches.objects.filter(tid=tutor_info.tid):
            #         if lang.language_id > 11 or lang.language_id < 1: continue
            #         tch_item = {}
            #         tch_item["name"] = Language.objects.get(id=lang.language_id).language1
            #         tch_item["level"] = lang.skill_level
            #         my_teach_list.append(tch_item)
            #     obj["teaches"] = my_teach_list
            #     '''
            #     # rating 정보 가져오기
            #     total_rating = 0
            #     total_num = 0
            #     my_lesson_list = Lesson.objects.filter(user_id=item.id).values('lid')
            #     for lesson in my_lesson_list:
            #         num = Ratings.objects.filter(lid=lesson['lid']).count()
            #         if num < 1:
            #             continue
            #         for rating in Ratings.objects.filter(lid=lesson['lid']).values('rating'):
            #             total_rating += rating['rating']
            #         total_num += num
            #     if total_num < 1:
            #         ave_rating = 0
            #     else:
            #         ave_rating = total_rating / float(total_num)
            #
            #     obj["ave_rating"] = ave_rating
            #     print '평점 Time : %.02f' % (time.time() - count_t)
            #     '''
            #     #lessons, tutee 정보 가져오기
            #     Reservation.objects.filter(tid=item.tid)
            #
            #     obj["tutees"] = Reservation.objects.filter(tid=item.tid).values('user_id').annotate(Count('user_id'))
            #     print obj["tutees"]
            #     obj["ave_rating"] = ave_rating
            #
            #     '''
            #     # tag 가져오기
            #     my_tag_list = []
            #     for tag in Specialtag.objects.filter(user_id=item.id):
            #         item_list = {}
            #         try:
            #             # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #             item_list["stname"] = SpecialtagInfo.objects.get(stid=tag.stid).stname
            #             item_list["stid"] = SpecialtagInfo.objects.get(stid=tag.stid).stid
            #             my_tag_list.append(item_list)
            #         except:
            #             continue
            #     obj["tag_list"] = my_tag_list
            #
            #     # description
            #     my_profile = Profile.objects.get(user_id=item.id)
            #     obj["description"] = my_profile.quick_intro
            #     obj["video_intro"] = my_profile.video_intro
            #
            #     # follow info
            #     obj["follow_cnt"] = Follow.objects.filter(followee_id=item.id).count()
            #     try:
            #         if userID is not None:
            #             Follow.objects.get(followee_id=item.id, follower_id=userID)
            #             obj["is_follower"] = 1
            #         else:
            #             obj["is_follower"] = 0
            #     except:
            #         obj["is_follower"] = 0
            #
            #     # tools list
            #     tool_list = []
            #     my_tools = Tools.objects.get(user_id=item.id)
            #     tool_list.append({"offline": my_tools.offline})
            #     tool_list.append({"audio": my_tools.audio})
            #     tool_list.append({"skype": my_tools.skype})
            #     tool_list.append({"hangout": my_tools.hangout})
            #     tool_list.append({"facetime": my_tools.facetime})
            #     tool_list.append({"qq": my_tools.qq})
            #     tool_list.append({"wechat": my_tools.wechat})
            #     obj["tool_list"] = tool_list
            #     obj["tool_comment"] = my_tools.comment
            #
            #     # lesson price
            #     # trial
            #     try:
            #         # 따로 조회 하여 데이터를 입력하지 않고, view에 미리 데이터를 담아 둘 예정
            #         obj["trial_price"] = Lesson.objects.get(user_id=item.id, type=0).online_30m_1times_cost
            #     except Exception as err:
            #         # print "not trial ", str(err)
            #         obj["trial_price"] = 0
            #     if obj["trial_price"] is not None:
            #         obj["trial_price_ex"] = int(obj["trial_price"]) * 100
            #
            #     # lesson_price = Lesson.objects.filter(tid=item.tid, type=1).aggregate(Min('online_60m_1times_cost'),Max('online_60m_1times_cost'))
            #
            #     # min price, max price 부분도 view 부분에 계산하여 column 으로 생성할 예정
            #     cost_obj = Lesson.objects.filter(user_id=item.id, type=1, del_yn=0).aggregate(
            #         Max('online_30m_1times_cost'), Max('online_60m_1times_cost'), Max('offline_60m_1times_cost'),
            #         Max('offline_90m_1times_cost'), Max('offline_120m_1times_cost'))
            #     if cost_obj["online_30m_1times_cost__max"] is None:
            #         cost_obj["online_30m_1times_cost__max"] = 0
            #
            #     if cost_obj["offline_90m_1times_cost__max"] is None:
            #         cost_obj["offline_90m_1times_cost__max"] = 0
            #     if cost_obj["offline_120m_1times_cost__max"] is None:
            #         cost_obj["offline_120m_1times_cost__max"] = 0
            #     cost_list = [cost_obj["online_30m_1times_cost__max"] * 2, cost_obj["online_60m_1times_cost__max"],
            #                  cost_obj["offline_60m_1times_cost__max"],
            #                  (cost_obj["offline_90m_1times_cost__max"] * 2 / 3),
            #                  cost_obj["offline_120m_1times_cost__max"] / 2]
            #     obj["lesson_max_price"] = max(cost_list)
            #
            #     cost_list = [0, 0, 0, 0, 0]
            #     cost_obj = Lesson.objects.filter(user_id=item.id, type=1, del_yn=0,
            #                                      online_30m_1times_cost__gt=0).aggregate(Min('online_30m_1times_cost'))
            #     if cost_obj["online_30m_1times_cost__min"] is not None:
            #         cost_list[0] = (cost_obj["online_30m_1times_cost__min"] * 2)
            #     else:
            #         cost_list[0] = 0
            #     cost_obj = Lesson.objects.filter(user_id=item.id, type=1, del_yn=0,
            #                                      online_60m_1times_cost__gt=0).aggregate(Min('online_60m_1times_cost'))
            #     if cost_obj["online_60m_1times_cost__min"] is not None:
            #         cost_list[1] = (cost_obj["online_60m_1times_cost__min"])
            #     else:
            #         cost_list[1] = 0
            #     cost_obj = Lesson.objects.filter(user_id=item.id, type=1, del_yn=0,
            #                                      offline_60m_1times_cost__gt=0).aggregate(
            #         Min('offline_60m_1times_cost'))
            #     if cost_obj["offline_60m_1times_cost__min"] is not None:
            #         cost_list[2] = (cost_obj["offline_60m_1times_cost__min"])
            #     else:
            #         cost_list[2] = 0
            #     cost_obj = Lesson.objects.filter(user_id=item.id, type=1, del_yn=0,
            #                                      offline_90m_1times_cost__gt=0).aggregate(
            #         Min('offline_90m_1times_cost'))
            #     if cost_obj["offline_90m_1times_cost__min"] is not None:
            #         val = cost_obj["offline_90m_1times_cost__min"] / float(3) * 2
            #         cost_list.append(int(val))
            #     else:
            #         val = 0
            #         cost_list.append(int(val))
            #     cost_obj = Lesson.objects.filter(user_id=item.id, type=1, del_yn=0,
            #                                      offline_120m_1times_cost__gt=0).aggregate(
            #         Min('offline_120m_1times_cost'))
            #     if cost_obj["offline_120m_1times_cost__min"] is not None:
            #         cost_list.append(cost_obj["offline_120m_1times_cost__min"] / 2)
            #     else:
            #         cost_list.append(0)
            #
            #     res = [num for num in cost_list if num > 0]
            #     if res:
            #         obj["lesson_min_price"] = min(res)
            #     else:
            #         obj["lesson_min_price"] = 0
            #
            #     # if lesson_price["online_60m_1times_cost__min"]:
            #     #    obj["lesson_min_price"] = lesson_price["online_60m_1times_cost__min"]
            #     # else:
            #     #    obj["lesson_min_price"] = 0
            #     obj["lesson_min_price_ex"] = int(obj["lesson_min_price"]) * 100
            #
            #     # if lesson_price["online_60m_1times_cost__max"]:
            #     #    obj["lesson_max_price"] = lesson_price["online_60m_1times_cost__max"]
            #     # else:
            #     #    obj["lesson_max_price"] = 0
            #     obj["lesson_max_price_ex"] = int(obj["lesson_max_price"]) * 100
            #
            #     obj["price_unit"] = "KRW"
            #     print '강의가격 Time : %.02f' % (time.time() - count_t)
            #     result.append(obj)

        except Exception as err:
            print str(err)

        last_page -= 1
        page_info = {"page": cur_page, "last_page": last_page, 'has_next': tutorlist.has_next()}

        return result, page_info

    # 필터 튜터
    def ajax_listup_tutors(self, user_id, data, utc_time):
        """
        @summary: tutor list filter
        @author: msjang
        @param user_id: 접속 유저 id 번호
        @param data: 검색 데이터
        @param utc_time: utc_time 정보
        @return: page_info( object )
        """
        result = {}
        _type = 1;
        _data = []

        # page info
        cur_page = data["page"]

        # print data
        sql_string = "select distinct id from v_searchfilter where 1=1 "

        if data['type'] == 'name':
            # TODO
            # name search
            sql_string += "and name like '%" + data['name'] + "%'  group by id"
        else:
            # TODO
            # filter search
            teaches = data['teaches']  #
            avail = data['avail']
            hour = data['hour']  #
            speaks = data['speaks']  #
            nation_from = data['nation_from']  #
            livein = data['livein']  #
            liveincity = data['liveincity']  #
            meet = data['meet']  #
            tags = data['tags']  #
            native = data['native']
            online = data['online']  #
            trial = data['trial']  #
            video = data['video']  #

            if (teaches != "all"):
                sql_string += " and ( teaching1=" + teaches + " or teaching2=" + teaches + " or teaching3=" + teaches + " ) "
            if (speaks != "all"):
                sql_string += " and ( native1=" + speaks + " or native2=" + speaks + " or native3=" + speaks + " or other1=" + speaks + " or other2=" + speaks + " or other3=" + speaks + " ) "
            if (nation_from != "all"):
                sql_string += " and nation_from_id=" + nation_from + " "
            if (livein != "all"):
                sql_string += " and nation_live_id=" + livein + " "
            if (liveincity != "all"):
                sql_string += " and city_live_id=" + liveincity + " "
            if (hour != 'all'):
                # TODO online, offline check
                # only offline
                if (meet[0]['check_offline'] == True and meet[0]['check_online'] == False):
                    if hour == "1":
                        sql_string += " and ( offline_60m_1times_cost between " + str(hour) + " and " + str(
                            float(hour) + 99) + " "
                        sql_string += " or offline_90m_1times_cost between " + str(float(hour) * 3 / 2) + " and " + str(
                            (float(hour) + 99) * 3 / 2) + " "
                        sql_string += " or offline_120m_1times_cost between " + str(float(hour) * 2) + " and " + str(
                            (float(hour) + 99) * 2) + " ) "
                    elif hour == "251":
                        sql_string += " and ( offline_60m_1times_cost >= " + str(hour) + " "
                        sql_string += " or offline_90m_1times_cost >= " + str(float(hour) * 3 / 2) + " "
                        sql_string += " or offline_120m_1times_cost >= " + str(float(hour) * 2) + " ) "
                    else:
                        sql_string += " and ( offline_60m_1times_cost between " + str(hour) + " and " + str(
                            float(hour) + 49) + " "
                        sql_string += " or offline_90m_1times_cost between " + str(float(hour) * 3 / 2) + " and " + str(
                            (float(hour) + 49) * 3 / 2) + " "
                        sql_string += " or offline_120m_1times_cost between " + str(float(hour) * 2) + " and " + str(
                            (float(hour) + 49) * 2) + " ) "

                elif (meet[0]['check_offline'] == False and meet[0]['check_online'] == True):
                    if hour == "1":
                        sql_string += " and ( online_30m_1times_cost between " + str((hour) / 2) + " and " + str(
                            (float(hour) + 99) / 2) + " "
                        sql_string += " or online_60m_1times_cost between " + str(hour) + " and " + str(
                            (float(hour) + 99)) + " ) "
                    elif hour == "251":
                        sql_string += " and ( online_30m_1times_cost >= " + str(float(hour) / 2) + " "
                        sql_string += " or online_60m_1times_cost >= " + str(hour) + " ) "
                    else:
                        sql_string += " and ( online_30m_1times_cost between " + str(float(hour) / 2) + " and " + str(
                            (float(hour) + 49) / 2) + " "
                        sql_string += " or online_60m_1times_cost between " + str(hour) + " and " + str(
                            float(hour) + 49) + " ) "
                else:
                    if hour == "1":
                        sql_string += " and ( offline_60m_1times_cost between " + str(hour) + " and " + str(
                            float(hour) + 99) + " "
                        sql_string += " or offline_90m_1times_cost between " + str(float(hour) * 3 / 2) + " and " + str(
                            (float(hour) + 99) * 3 / 2) + " "
                        sql_string += " or offline_120m_1times_cost between " + str(float(hour) * 2) + " and " + str(
                            (float(hour) + 99) * 2) + " "
                        sql_string += " or online_30m_1times_cost between " + str(float(hour) / 2) + " and " + str(
                            (float(hour) + 99) / 2) + " "
                        sql_string += " or online_60m_1times_cost between " + str(hour) + " and " + str(
                            (float(hour) + 99)) + " ) "
                    elif hour == "251":
                        sql_string += " and ( offline_60m_1times_cost >= " + str(hour) + " "
                        sql_string += " or offline_90m_1times_cost >= " + str(float(hour) * 3 / 2) + " "
                        sql_string += " or offline_120m_1times_cost >= " + str(float(hour) * 2) + " "
                        sql_string += " or online_30m_1times_cost >= " + str(float(hour) / 2) + " "
                        sql_string += " or online_60m_1times_cost >= " + str(hour) + " ) "
                    else:
                        sql_string += " and ( offline_60m_1times_cost between " + str(hour) + " and " + str(
                            float(hour) + 49) + " "
                        sql_string += " or offline_90m_1times_cost between " + str(float(hour) * 3 / 2) + " and " + str(
                            (float(hour) + 49) * 2 / 3) + " "
                        sql_string += " or offline_120m_1times_cost between " + str(float(hour) * 2) + " and " + str(
                            (float(hour) + 49) * 2) + " "
                        sql_string += " or online_30m_1times_cost between " + str(float(hour) / 2) + " and " + str(
                            (float(hour) + 49) / 2) + " "
                        sql_string += " or online_60m_1times_cost between " + str(hour) + " and " + str(
                            float(hour) + 49) + " ) "

            # offline only
            if (meet[0]['check_offline']):
                sql_string += " and offline=1 "
            if (meet[0]['check_online']):
                sql_string += " and online=1 "
            if (meet[0]['check_audio']):
                sql_string += " and audio=1 "

            if (trial):
                sql_string += " and type=0 "

            if (video):
                sql_string += " and audio=0 "

            # avail
            all_time = False
            dayArray = ['day_mon', 'day_tue', 'day_wed', 'day_thu', 'day_fri', 'day_sat', 'day_sun']
            if avail[0]['day_mon'] == False and avail[0]['day_tue'] == False and avail[0]['day_wed'] == False and \
                            avail[0]['day_thu'] == False and avail[0]['day_fri'] == False \
                    and avail[0]['day_sat'] == False and avail[0]['day_sun'] == False:
                for day in dayArray:
                    avail[0][day] = True;
            timeArray = ['time0', 'time6', 'time12', 'time18']
            if avail[0]['time0'] == False and avail[0]['time6'] == False and avail[0]['time12'] == False and avail[0][
                'time18'] == False:
                for time in timeArray:
                    all_time = True
                    avail[0][time] = True;

            sql_avail = ''
            timeIndex = 0
            for time in timeArray:
                timeA = timeIndex * 6
                timeB = timeIndex * 6 + 6
                if (avail[0][time]):
                    dayIndex = 0
                    for day in dayArray:
                        print avail[0][day]
                        if (avail[0][day]):
                            print day, utc_time

                            for i in range(timeA, timeB):
                                if (i - utc_time < 0):
                                    if all_time:
                                        test = " or (day_of_week = " + str(7 if dayIndex == 0 else dayIndex) + " ) "
                                    else:
                                        test = " or (day_of_week = " + str(
                                            7 if dayIndex == 0 else dayIndex) + " and (time" + "{0:0>2}".format(
                                            i - utc_time + 24) + "00=1 or time" + "{0:0>2}".format(
                                            i - utc_time + 24) + "30=1))"
                                    sql_avail += test
                                    # print test
                                elif (i - utc_time > 23):
                                    if all_time:
                                        test = " or (day_of_week = " + str(
                                            1 if dayIndex + 2 == 8 else dayIndex + 2) + " ) "
                                    else:
                                        test = " or (day_of_week = " + str(
                                            1 if dayIndex + 2 == 8 else dayIndex + 2) + " and (time" + "{0:0>2}".format(
                                            i - utc_time - 24) + "00=1 or time" + "{0:0>2}".format(
                                            i - utc_time - 24) + "30=1))"
                                    sql_avail += test
                                    # print test
                                else:
                                    if all_time:
                                        test = " or (day_of_week = " + str(dayIndex + 1) + " ) "
                                    else:
                                        test = " or (day_of_week = " + str(
                                            dayIndex + 1) + " and (time" + "{0:0>2}".format(
                                            i - utc_time) + "00=1 or time" + "{0:0>2}".format(i - utc_time) + "30=1))"
                                    sql_avail += test
                                    # print test
                        dayIndex = dayIndex + 1
                timeIndex = timeIndex + 1

            if (sql_avail != ''):
                sql_string += ' and (' + sql_avail[4:] + ' )'

            # native_only
            if native:
                sql_string += " and (native1=" + teaches + " or native2=" + teaches + " or native3=" + teaches + " )"

            if (len(tags) > 0):
                '''
                sql_string += " and stid in ( "
                for index, tag in enumerate(tags):
                    if(index != 0):
                        sql_string += ", "
                    sql_string += str(tag['id'])
                sql_string += " ) "
                '''
                for tag in tags:
                    sql_string += " and user_id IN (SELECT user_id FROM v_searchfilter WHERE stid="
                    sql_string += str(tag['id'])
                    sql_string += " ) "

            # sql_string += " group by id"

            list = {}
            list["is_login"] = online
            list["is_native"] = native
            _data.append(list)

        print 'sql_string:'
        print sql_string
        tutor_list = []
        cursor = connection.cursor()
        cursor.execute(sql_string)

        # listup tutors와 합쳐질 경우  상세내용을 조회하여 json 형태로 넘겨주는 코드 필요
        result = self.dictfetchall(cursor)

        print result

        for tutor in result:
            tutor_list.append(tutor['id'])
        print 'tutor_list:'
        print tutor_list

        return self.listup_tutors(user_id, tutor_list, _type, _data, cur_page)


    # 튜터 강의 목록
    def get_tutor_lessons(self, t_id):
        """
        @summary: get tutor lesson list
        @author: khyun
        @param t_id : tutor id
        @return: object data
        """
        '''
        lessonlist = Lesson.objects.filter(user_id=t_id, type=1, del_yn=0)

        result = []
        for item in lessonlist:
            item_obj = {}
            res=model_to_dict(item)
            for field_name in item._meta.get_all_field_names():
                item_obj[field_name] = res[field_name]
            try:
                item_obj["language"] =   Language.objects.get(id=item_obj["language"]).language1
                item_obj["from_level"] = SkillLevel.objects.get(level_id = item_obj["from_level"]).level_name
                item_obj["to_level"] = SkillLevel.objects.get(level_id = item_obj["to_level"]).level_name
            except Exception as err:
                print str(err)

            result.append(item_obj)

        return result
        '''
        result = []
        try:
            cursor = connection.cursor()
            sql = '''
                SELECT lid, user_id, title, ul.language1 AS language
                     , description
                     , ( SELECT level_name FROM skill_level WHERE level_id = l.from_level ) AS from_level
                     , ( SELECT level_name FROM skill_level WHERE level_id = l.to_level) AS to_level
                     , ( SELECT CONCAT( ( CASE WHEN c.type = 1 THEN '/static/img/course_bgimg/Business/'
                                          WHEN c.type = 2 THEN '/static/img/course_bgimg/National flags/'
                                          WHEN c.type = 3 THEN '/static/img/course_bgimg/People/'
                                          WHEN c.type = 4 THEN '/static/img/course_bgimg/Scenery/'
                                          ELSE '/static/img/course_bgimg/Themes/' END ), filepath )
                         FROM course_bg_info c
                         WHERE id = l.display_color ) AS display_color
                     , has_online, online_30m, online_60m, online_1times, online_5times, online_10times
                     , online_30m_1times_cost, online_30m_5times_cost, online_30m_10times_cost
                     , online_60m_1times_cost, online_60m_5times_cost, online_60m_10times_cost
                FROM lesson l, user_auth_language ul
                WHERE l.language = ul.id
                  AND user_id = %s
                  AND TYPE = 1
                  AND del_yn = 0;
            '''
            cursor.execute(sql, [t_id])
            result = common.dictfetchall(cursor)
        except Exception as e:
            print str(e)
        finally:
            cursor.close()
            return result

    # 튜터 통계
    def get_tutor_statistics(self, t_id):
        """
        @summary: get tutor statistics
        @author: khyun
        @param t_id : tutor id
        @return: object data
        """
        obj = {}

        # 전체 강의 목록
        lessons = Lesson.objects.filter(user_id=t_id)
        my_lessons = []
        for lesson in lessons:
            my_lessons.append(lesson.lid)

        # rating 한달 평균
        item_obj = {}
        end_date = datetime.now()  # 현재
        timegap = timedelta(days=30)  # 날짜 차이
        start_date = end_date - timegap

        user_count = Ratings.objects.filter(lid__in=my_lessons, created_date__range=(start_date, end_date)).count()
        total_rating = Ratings.objects.filter(lid__in=my_lessons, created_date__range=(start_date, end_date)).aggregate(
            Sum('rating'))

        if user_count > 0:
            item_obj["last_month"] = total_rating["rating__sum"] / float(user_count)
        else:
            item_obj["last_month"] = None
        # rating 전체 평균
        user_count = Ratings.objects.filter(lid__in=my_lessons).count()
        total_rating = Ratings.objects.filter(lid__in=my_lessons).aggregate(Sum('rating'))

        if user_count > 0:
            item_obj["all_time"] = total_rating["rating__sum"] / float(user_count)
        else:
            item_obj["all_time"] = None

        obj["AR"] = item_obj

        item_obj = {}
        # Attendance : (신청완료 - dipute) / 신청완료
        # last_month
        now = datetime.now()
        timegap = timedelta(days=30)  # 날짜 차이
        term = now - timegap

        str_startday = str(term.year) + term.strftime("%m%d")
        str_endday = str(now.year) + now.strftime("%m%d")
        stat_range = []
        for i in range(int(str_startday), int(str_endday)):
            stat_range.append(i + 1)

        rv_list = Reservation.objects.filter(tutor_id=t_id, user_confirm__gt=0, tutor_confirm=1)
        rv_count = 0
        for item in rv_list:
            str_sc_time = item.rv_time
            str_sc_time.strip()
            timelist = str_sc_time.split('$')
            b_checked = False
            for t in timelist:
                rv_date = t[:8]
                if rv_date in stat_range:
                    b_checked = True
                    break
            if b_checked:
                rv_count += 1

        rv_list = Reservation.objects.filter(tutor_id=t_id, user_confirm__gt=2, tutor_confirm=1)
        rv_dispute = 0
        for item in rv_list:
            str_sc_time = item.rv_time
            str_sc_time.strip()
            timelist = str_sc_time.split('$')
            b_checked = False
            for t in timelist:
                rv_date = t[:8]
                if rv_date in stat_range:
                    b_checked = True
                    break
            if b_checked:
                rv_dispute += 1

        # print rv_count, rv_dispute

        # rv_count = Reservation.objects.filter(tid=t_id, user_confirm__gt=0, tutor_confirm=1).count()
        # rv_dispute = Reservation.objects.filter(tid=t_id, user_confirm__gt=2, tutor_confirm=1).count()
        if rv_count > 0:
            item_obj["last_month"] = str(((rv_count - rv_dispute) / float(rv_count)) * 100) + " %"
        else:
            item_obj["last_month"] = None

        rv_count = Reservation.objects.filter(tutor_id=t_id, user_confirm__gt=0, tutor_confirm=1).count()
        rv_dispute = Reservation.objects.filter(tutor_id=t_id, user_confirm__gt=1, tutor_confirm=1).count()

        if rv_count > 0:
            item_obj["all_time"] = str(((rv_count - rv_dispute) / float(rv_count)) * 100) + " %"
        else:
            item_obj["all_time"] = None

        obj["attendance"] = item_obj

        item_obj = {}
        # Disputes : user_confirm > 1
        item_obj["last_month"] = rv_dispute
        item_obj["all_time"] = rv_dispute
        obj["disputes"] = item_obj

        # confirmed lessons
        item_obj = {}
        now = datetime.now()
        timegap = timedelta(days=30)  # 날짜 차이
        term = now - timegap

        str_startday = str(term.year) + term.strftime("%m%d")
        str_endday = str(now.year) + now.strftime("%m%d")

        rv_list = Reservation.objects.filter(tutor_id=t_id, rv_status=5)

        last_month_confirmed_count = 0

        for item in rv_list:
            str_sc_time = item.rv_time
            str_sc_time.strip()
            timelist = str_sc_time.split('$')
            for t in timelist:
                rv_date = t[:8]
                print 'rv_date = ' + rv_date
                if int(rv_date) >= int(str_startday):
                    last_month_confirmed_count += 1

        item_obj["last_month"] = last_month_confirmed_count

        all_time_confirmed_count = Reservation.objects.filter(tutor_id=t_id, rv_status=5).count()

        item_obj["all_time"] = all_time_confirmed_count
        obj["confirmed_lesson"] = item_obj

        return obj

    # 튜터 후기 목록
    def get_tutor_ratings(self, t_id, user_id, page=1):
        """
        @summary: get tutor ratings
        @author: khyun
        @param tutor_id : tutor id
        @param user_id : user id
        @param page : page number
        @return: object data
        """
        per_page = self.RT_PERPAGE
        start_pos = (page - 1) * per_page
        end_pos = start_pos + per_page

        lessons = Lesson.objects.filter(user_id=t_id)
        my_lessons = []
        for lesson in lessons:
            my_lessons.append(lesson.lid)

        # entry = Ratings.objects.filter(lid__in=my_lessons).annotate(ucount=Count("user_id"))[start_pos:end_pos]
        # entry = Ratings.objects.filter(lid__in=my_lessons).order_by("-created_date").values("user_id").distinct()[start_pos:end_pos]
        # totalCount = Ratings.objects.filter(lid__in=my_lessons).count()
        totalCount = Ratings.objects.filter(lid__in=my_lessons).values("user_id").distinct().count()
        pg = self.paging(page, totalCount, per_page)

        arr = []
        for i in range(int(pg["startPage"]), int(pg["endPage"]) + 1):
            arr.append(i)

        vl_list = []
        '''
        for item in entry:
            item_obj = {}

            #item_obj["user_id"] = item.user_id
            item_obj["user_id"] = item["user_id"]

            item_obj["comment"] = Ratings.objects.filter(lid__in=my_lessons, user_id=item["user_id"]).first().comment
            item_obj["rating"] = Ratings.objects.filter(lid__in=my_lessons, user_id=item["user_id"]).first().rating
            item_obj["created_date"] = str(Ratings.objects.filter(lid__in=my_lessons, user_id=item["user_id"]).first().created_date)
            item_obj["comment_list"] = []
            for each_rating in Ratings.objects.filter(lid__in=my_lessons, user_id=item["user_id"]):
                each_obj = {}
                res=model_to_dict(each_rating)
                for field_name in each_rating._meta.get_all_field_names():
                    each_obj[field_name] = res[field_name]
                each_obj["created_date"] = str(each_obj["created_date"])
                item_obj["comment_list"].append(each_obj)

            try:
                user_info = TellpinUser.objects.get(id=item_obj["user_id"])
                item_obj["name"] = user_info.name
                item_obj["profile_photo"] = self.PROFILE_IMAGE_FILE_DIR + "/" + user_info.profile_photo
            except:
                item_obj["name"] = None
                item_obj["profile_photo"] = None
            vl_list.append(item_obj)
         '''
        try:
            cursor = connection.cursor()
            if user_id is not None:
                sql = '''
                    SELECT T.*, COUNT(*) AS rating_count
                         , DATE_ADD( T.created_date, INTERVAL( SELECT y.utc_time FROM timezone Y WHERE id = ( SELECT timezone FROM user_auth_tellpinuser WHERE id = %s ) ) HOUR ) AS created_date
                         , ( SELECT NAME FROM user_auth_tellpinuser WHERE id = user_id ) AS name
                         , CONCAT( %s, ( SELECT profile_photo FROM user_auth_tellpinuser WHERE id = user_id ) ) AS profile_photo
                    FROM
                    (
                        SELECT *
                        FROM ratings
                        WHERE lid IN ( SELECT lid FROM lesson WHERE user_id = %s )
                        ORDER BY created_date DESC
                    ) T
                    GROUP BY user_id
                    LIMIT %s, %s;
                '''
                cursor.execute(sql, [user_id, self.PROFILE_IMAGE_FILE_DIR + '/', t_id, start_pos, per_page])
            else:
                sql = '''
                    SELECT T.*, COUNT(*) AS rating_count
                         , T.created_date AS created_date
                         , ( SELECT NAME FROM user_auth_tellpinuser WHERE id = user_id ) AS name
                         , CONCAT( %s, ( SELECT profile_photo FROM user_auth_tellpinuser WHERE id = user_id ) ) AS profile_photo
                    FROM
                    (
                        SELECT *
                        FROM ratings
                        WHERE lid IN ( SELECT lid FROM lesson WHERE user_id = %s )
                        ORDER BY created_date DESC
                    ) T
                    GROUP BY user_id
                    LIMIT %s, %s;
                '''
                cursor.execute(sql, [self.PROFILE_IMAGE_FILE_DIR + '/', t_id, start_pos, per_page])
            vl_list = common.dictfetchall(cursor)
        except Exception as e:
            print str(e)
        finally:
            cursor.close()

        variables = {
            'entries': vl_list,
            'page': page,
            'last_page': pg["last_page"],
            'startPage': pg["startPage"],
            'endPage': pg["endPage"],
            'has_prev': pg["has_prev"],
            'has_next': pg["has_next"],
            "pgGroup": arr,
        }

        return variables

    # *****************************************************//
    # paging 함수
    # *****************************************************//
    def paging(self, _page, _totalCount, _per_page=3):
        """
        @summary: paging 함수
        @author: msjang
        @param _page: 현재 페이지 번호
        @param _totalCount: 총 데이터 count
        @param _per_page: page 당 표시할 데이터 수
        @return: retval( object )
        """
        if _totalCount % _per_page == 0:
            last_page = _totalCount / _per_page
        else:
            last_page = (_totalCount / _per_page) + 1

        if _page == 1:
            has_prev = 1
        else:
            has_prev = _page - 1

        if _page > last_page:
            has_next = last_page
        else:
            has_next = _page + 1

        blockPage = int(_totalCount / _per_page)
        # blockPage = last_page

        ts = []

        for x in range(0, blockPage):
            ts.append((5 * x) + 5)

        startPage = 1
        endPage = 1

        for i in ts:
            if _page <= int(i):
                startPage = (int(i) - 5) + 1
                if last_page < int(i):
                    endPage = last_page
                else:
                    endPage = i
                break

        retval = {
            "has_prev": has_prev,
            "has_next": has_next,
            "startPage": startPage,
            "endPage": endPage,
            "last_page": last_page

        }

        return retval


    # 강사 시범 강의
    def get_trial_lesson(self, t_id):
        """
        @summary: 강사 시범 강의 정보
        @author: msjang
        @param t_id: tutor id
        @return: item_obj( object )
        """
        try:
            item_obj = {}
            trial_lesson = Lesson.objects.get(user_id=t_id, type=0)
            res = model_to_dict(trial_lesson)
            for field_name in trial_lesson._meta.get_all_field_names():
                item_obj[field_name] = res[field_name]

            ot = [[0, 2], [1, 9]]
            item_obj["online_type"] = ot[item_obj["has_online"]][item_obj["has_offline"]]
            bun = [[[0, 3], [2, 3]], [[1, 3], [2, 3]]]
            bun[0][0][0] = 1  # trial
            bun[1][0][0] = 1
            bun[0][1][0] = 2
            bun[1][1][0] = 2
            bun[0][0][1] = 3
            bun[1][0][1] = 3
            bun[1][0][1] = 3
            bun[1][1][1] = 9
            item_obj["bundle"] = bun[item_obj["online_1times"]][item_obj["online_5times"]][item_obj["online_10times"]]
            item_obj["per_duration"] = 30

        except Exception as err:
            print str(err)
            return None

        return item_obj

    # 강사 전체 강의 목록
    def get_reserv_all_lesson(self, t_id, acc_type, l_id=0):
        """
        @summary: 강사 전체 강의 목록
        @author: msjang
        @param t_id: tutor id
        @param acc_type: 강의 타입
        @param l_id: lesson table pk
        @return: lesson_list( list )
        """
        acc_type = int(acc_type)

        lesson_list = []
        # print acc_type

        course_type_path = {1: '/static/img/course_bgimg/Business/',
                            2: '/static/img/course_bgimg/National flags/',
                            3: '/static/img/course_bgimg/People/',
                            4: '/static/img/course_bgimg/Scenery/'}
        course_type_path_etc = '/static/img/course_bgimg/Themes/'

        try:
            if acc_type == 0:
                lessons = Lesson.objects.filter(user_id=t_id).exclude(type=0)
            elif acc_type == 1:
                lessons = Lesson.objects.filter(lid=l_id, type=1)
            elif acc_type == 2:
                lessons = Lesson.objects.filter(user_id=t_id, type=0)

            for lesson in lessons:
                item_obj = {}
                res = model_to_dict(lesson)
                for field_name in lesson._meta.get_all_field_names():
                    item_obj[field_name] = res[field_name]

                ot = [[0, 2], [1, 9]]

                item_obj["online_type"] = ot[item_obj["has_online"]][item_obj["has_offline"]]

                bun = [[[0, 3], [2, 3]], [[1, 3], [2, 3]]]
                bun[0][0][0] = 1  # trial
                bun[1][0][0] = 1
                bun[0][1][0] = 2
                bun[1][1][0] = 2
                bun[0][0][1] = 3
                bun[0][1][1] = 3
                bun[1][0][1] = 3
                bun[1][1][1] = 9
                item_obj["bundle"] = bun[item_obj["online_1times"]][item_obj["online_5times"]][
                    item_obj["online_10times"]]
                du = [[0, 2], [1, 9]]
                item_obj["per_duration"] = 30
                try:
                    item_obj["language"] = Language.objects.get(id=item_obj["language"]).language1
                except:
                    item_obj["language"] = "Trial"
                try:
                    item_obj["from_level"] = SkillLevel.objects.get(level_id=item_obj["from_level"]).level_name[3:]
                    item_obj["to_level"] = SkillLevel.objects.get(level_id=item_obj["to_level"]).level_name[3:]
                except:
                    item_obj["from_level"] = SkillLevel.objects.get(level_id=1).level_name[3:]
                    item_obj["to_level"] = SkillLevel.objects.get(level_id=7).level_name[3:]

                course_bg = self.get_course_bg_info(item_obj["display_color"])
                if course_bg["type"] in range(1, 5):
                    item_obj["display_color"] = course_type_path[course_bg["type"]] + course_bg["filepath"]
                else:
                    item_obj["display_color"] = course_type_path_etc + course_bg["filepath"]

                lesson_list.append(item_obj)

        except Exception as err:
            print str(err)
        return lesson_list

    def get_course_bg_info(self, img_id):
        """
        @summary: 강의 배경 이미지 정보
        @author: msjang
        @param img_id: 강의 이미지 id
        @return: result( list )
        """
        result = []
        try:
            cursor = connection.cursor()
            sql = '''
                SELECT * FROM tellpin.course_bg_info where id=%s;
            '''
            cursor.execute(sql, [img_id])
            result = common.dictfetchall(cursor)
        except Exception as e:
            print str(e)
        finally:
            cursor.close()
            return result[0]

    # 강의 정보
    def get_tutor_lesson(self, l_id):
        """
        @summary: 강의 정보
        @author: msjang
        @param l_id: 강의 id
        @return: item_obj( object )
        """
        try:
            lesson = Lesson.objects.get(lid=l_id)
            item_obj = {}
            res = model_to_dict(lesson)
            for field_name in lesson._meta.get_all_field_names():
                item_obj[field_name] = res[field_name]

            ot = [[0, 2], [1, 9]]
            item_obj["online_type"] = ot[item_obj["has_online"]][item_obj["has_offline"]]
            bun = [[[0, 3], [2, 3]], [[1, 3], [2, 3]]]
            bun[0][0][0] = 1  # trial
            bun[1][0][0] = 1
            bun[0][1][0] = 2
            bun[1][1][0] = 2
            bun[0][0][1] = 3
            bun[1][0][1] = 3
            bun[1][0][1] = 3
            bun[1][1][1] = 9
            item_obj["bundle"] = bun[item_obj["online_1times"]][item_obj["online_5times"]][item_obj["online_10times"]]
            du = [[0, 2], [1, 9]]
            item_obj["per_duration"] = du[item_obj["online_30m"]][item_obj["online_60m"]]

            try:
                item_obj["language"] = Language.objects.get(id=item_obj["language"]).language1
            except:
                item_obj["language"] = "trial"

            try:
                item_obj["from_level"] = SkillLevel.objects.get(level_id=item_obj["from_level"]).level_name
                item_obj["to_level"] = SkillLevel.objects.get(level_id=item_obj["to_level"]).level_name
            except:
                item_obj["from_level"] = 1
                item_obj["to_level"] = 7

        except Exception as err:
            print str(err)

        return item_obj

    def get_tutor_schedule(self, t_id, _user_utc, now=datetime.utcnow()):
        """
        @summary: 튜터 스케줄 정보
        @author: msjang
        @param t_id: tutor id
        @param _user_utc: 접속유저 utc 정보
        @param now: 현재 시각
        @return: res_obj( object )
        """
        print "#####################################"
        print now
        print "#####################################"
        res_obj = {}
        week_list = []
        dow_list = []

        obj = {}
        try:
            user_now = datetime.strptime(common.time_to_user_timezone(now, _user_utc),
                                         "%Y-%m-%d %H:%M:%S")  # 문자열로된 시간을 datetime 객체로 만들기
        except Exception as err:
            user_now = now
            # now = datetime.now()
        currunt_day_of_weak = int(user_now.weekday())

        dow_gap = -currunt_day_of_weak
        dow_Str = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]
        for i in range(0, 7):
            timegap = timedelta(days=i)
            d = currunt_day_of_weak + i
            if d > 6:
                d -= 7
            obj[dow_Str[d]] = str((user_now + timegap).year) + str((user_now + timegap).strftime("%m%d"))
            dow_list.append(dow_Str[d])
            week_list.append(obj[dow_Str[d]])
            '''
            timegap = timedelta(days=dow_gap+i)
            obj[dow_Str[i]] = str((now + timegap).year)+str((now + timegap).strftime("%m%d"))
            week_list.append(obj[dow_Str[i]])
            '''

        res_obj["mark_day"] = str(user_now.year) + user_now.strftime("%m%d")
        res_obj[
            "week_list"] = week_list  # ['20160623', '20160624', '20160625', '20160626', '20160627', '20160628', '20160629']
        res_obj["dow_list"] = dow_list  # [0: "thu", 1:"fri"]

        '''
        try:
            tutor_schd = Schedule.objects.filter(tid=t_id).order_by("-date").first()
            str_sc_time = tutor_schd.available_time
            str_sc_time.strip()
            timelist = str_sc_time.split('$')

            res_obj["available_time"] = timelist
        except Exception as err:
            print str(err)
        '''

        sc_list = []
        dow_sign = ["A", "B", "C", "D", "E", "F", "G"]

        schedule_list = WeekSchedule.objects.filter(user_id=t_id).order_by("day_of_week")
        list = []
        for schedule in schedule_list:
            scd = model_to_dict(schedule, exclude="date")
            list.append(scd)
        client_list = common.applyUserUTCTime(list, _user_utc)
        try:
            for schd in client_list:
                dow_sign[schd["day_of_week"] - 1]
                for field_name in schd:
                    if field_name != "day_of_week":
                        if schd[field_name] == 1:
                            sc_list.append(dow_sign[schd["day_of_week"] - 1] + str(field_name)[4:])
        except Exception as err:
            print str(err)

        res_obj["available_time"] = sc_list  # 0:"A2000" , 9:"B1930"

        # calendar 날짜별 입력된 내용
        start_day = str(user_now.year) + user_now.strftime("%m%d")  # 20160623
        timegap = timedelta(days=7)
        end_day = str((user_now + timegap).year) + str((user_now + timegap).strftime("%m%d"))  # 20160630
        cld_list = []
        for cld_schd in Schedule.objects.filter(user_id=t_id):
            delta_time = common.apply_utc_month_schedule(cld_schd.delta_time, _user_utc)
            cld_obj = {}
            if start_day <= delta_time[:8] and delta_time[:8] <= end_day:
                cld_obj["delta_time"] = delta_time[8:]
                cld_obj["type"] = cld_schd.type
                cld_list.append(cld_obj)

        res_obj["appointed"] = cld_list  # 월간일정

        return res_obj

    # 예약된 시간표 얻기
    def get_reserved_schedule(self, t_id, _user_utc, now=datetime.utcnow()):
        """
        @summary: 예약된 시간표 얻기
        @author: msjang
        @param t_id: tutor id
        @param _user_utc: user utc 정보
        @param now: 현재 시각
        @return: r_list( list )
        """
        try:
            user_now = datetime.strptime(common.time_to_user_timezone(now, _user_utc), "%Y-%m-%d %H:%M:%S")
        except Exception as err:
            user_now = now

        start_day = str(user_now.year) + user_now.strftime("%m%d")
        timegap = timedelta(days=7)
        end_day = str((user_now + timegap).year) + str((user_now + timegap).strftime("%m%d"))

        str_today = str(now.year) + now.strftime("%m%d")

        this_week = []

        for i in range(0, 7):
            timegap = timedelta(days=i)
            this_week.append(str((user_now + timegap).year) + str((user_now + timegap).strftime("%m%d")))

        '''
        for i in range(0, 7 - int(now.weekday())):
            this_week.append(str(int(str_today) + i))

        for i in range(1, int(now.weekday()) + 1):
            this_week.append(str(int(str_today) - i))
        this_week.sort()
        '''

        # 20160126 예약 테이블 변경에 따라 시작 시각과 간격 정보를 쌍으로 넘김
        rv_list = Reservation.objects.filter(tutor_id=t_id)
        r_list = []
        print "get reservation"
        for item in rv_list:
            item_obj = {}
            if item.rv_time != None:
                utc_rv_time = common.user_utc_rv_time(item.rv_time, _user_utc)
                if int(start_day) <= int(utc_rv_time[:8]) and int(utc_rv_time[:8]) <= int(end_day):
                    item_obj["rv_time"] = utc_rv_time[8:]
                    item_obj["duration"] = item.duration
                    r_list.append(item_obj)

        return r_list
        '''
        rv_list = Reservation.objects.filter(tid=t_id)
        r_list = []
        for item in rv_list:
            item_obj = {}
            item_obj["rv_time"] = item.rv_time
            str_sc_time = item_obj["rv_time"]
            str_sc_time.strip()
            timelist = str_sc_time.split('$')

            for t in timelist:
                r_obj = {}
                r_obj["date"] = t[:8]
                r_obj["time"]= t[8:]
                if r_obj["date"] in this_week:
                    print r_obj["date"]
                    r_list.append(r_obj["time"])
            item_obj["rv_time"] = r_list

        return r_list
        '''

    # 예약 처리
    def set_reservation(self, _user_id, _user_utc, data):
        """
        @summary: 예약 처리
        @author: msjang
        @param _user_id: 유저 id
        @param _user_utc: 유저 utc 정보
        @param data: 예약 정보
        @return: ln_list( list )
        """

        time_list = [None for _ in range(int(data["bundle"]))]
        # time_list = data["rv_time"].split("$")
        for i, t in enumerate(data["rv_time"].split("$")):
            if i >= len(time_list):  # 수업 회수 보다 스케쥴 시간이 더 많이 입력
                break
            time_list[i] = common.server_rv_time(t, _user_utc)

        try:
            next_rvid = Reservation.objects.all().latest("rvid").rvid
            next_rvid += 1
        except:
            next_rvid = 1

        try:
            ln_list = []
            # rv_list = []
            for i in range(int(data["bundle"])):
                new_rv = Reservation()
                new_rv.tutor_id = data["tid"]
                new_rv.tutee_id = _user_id
                new_rv.lid = data["lid"]
                # new_rv.message = data["message"]
                if int(data["online_type"]) == 1:
                    new_rv.tool_id = data["tool"]
                    new_rv.tool_info = data["r_tool_info"]
                    new_rv.online = 1
                    new_rv.offline = 0
                else:
                    new_rv.online = 0
                    new_rv.offline = 1
                # next_rvid = int( next_rvid )
                # next_rvid += 1
                new_rv.bundle_rvid = next_rvid
                new_rv.duration = data["duration"]
                new_rv.rv_time = time_list[i]
                if int(data["bundle"]) != 1:
                    new_rv.is_bundle = 1

                new_rv.bundle_times = data["bundle"]
                new_rv.rv_orders = i + 1

                if not new_rv.rv_time:
                    new_rv.rv_status = 0
                else:
                    new_rv.rv_status = 1
                    new_rv.message = data["message"]

                new_rv.save()

                if (data["message"]):
                    if (new_rv.rv_orders == 1):
                        self.sendMessage(data["tid"], _user_id, data["message"], new_rv.bundle_rvid)

                ln_list.append(new_rv.rvid)
                # rv_list.append(new_rv)

            # Reservation.objects.bulk_create(rv_list)



            Message(to_user_id=data["tid"], from_user_id=_user_id, message=data["message"])
        except Exception as err:
            print "check reservation save() ", str(err)

        return ln_list

    # 추가 스케쥴 예약
    def update_reservation(self, _user_id, _user_utc, data):
        """
        @summary: 추가 스케줄 예약
        @author: msjang
        @param _user_id: 유저 id
        @param _user_utc: 유저 utc 정보
        @param data: 예약 정보
        @return: boolean
        """
        time_list = [None for _ in range(int(data["bundle"]))]
        # time_list = data["rv_time"].split("$")
        for i, t in enumerate(data["rv_time"].split("$")):
            if i >= len(time_list):  # 수업 회수 보다 스케쥴 시간이 더 많이 입력
                break
            time_list[i] = common.server_rv_time(t, _user_utc)

        rv_list = data["rvid"]
        for i in range(len(rv_list)):
            try:
                rv = Reservation.objects.get(rvid=rv_list[i])
            except:
                return False

            rv.rv_time = time_list[i]
            if not rv.rv_time:
                rv.rv_status = 0
            else:
                if data["command"] == 'Schedule':
                    rv.rv_status = 1
                elif data["command"] == 'Reschedule':
                    rv.rv_status = 8
                rv.message = data["message"]

            rv.save()

        return True

    # follow , unfollow
    def tutor_follow(self, _t_id, _user_id):
        """
        @summary: 튜터 follow
        @author: msjang
        @param _t_id: tutor id
        @param _user_id: 유저 id
        @return: 1( success ), 0( fail )
        """
        try:
            row_data = Follow.objects.get(followee_id=_t_id, follower_id=_user_id, division=1)
            row_data.delete()
            return 0
        except:
            Follow(followee_id=_t_id, follower_id=_user_id, division=1).save()
            return 1

    # message
    def tutor_message(self, t_id, _user_id, _message):
        """
        @summary: 튜터에게 메세지 전송
        @author: msjang
        @param _t_id: tutor id
        @param _user_id: 유저 id
        @param _message: message 내용
        @return: true, false( boolean ) and msg( string )
        """
        try:
            msg = Message(to_user_id=t_id, from_user_id=_user_id, message=_message)
            msg.save()
            print 'tutor_message'
            print msg
            return True, "success", msg
        except Exception as err:
            return False, str(err)

    # report
    def tutor_report(self, t_id, _user_id, _type, _message):
        """
        @summary: 튜터 report
        @author: msjang
        @param _t_id: tutor id
        @param _user_id: 유저 id
        @param _type: 신고 타입
        @param _message: 신고 내용
        @return: True, False( boolean )
        """
        try:
            TutorReport(tutor_id=t_id, tutee_id=_user_id, type=_type, content=_message).save()
            return True, "success"
        except Exception as err:
            return False, str(err)

    # 강사 숨기기
    def tutor_hide(self, t_id, _user_id):
        """
        @summary: 강사 숨기기
        @author: msjang
        @param _t_id: tutor id
        @param _user_id: 유저 id
        @return: True, False( boolean )
        """
        try:
            TutorHide(tutor_id=t_id, tutee_id=_user_id).save()
            return True, "success"
        except Exception as err:
            return False, str(err)

    

    """
    totoring app으로 이동 예정
    """

    def set_problem_accept(self, rvid, type):
        """
        @summary: 문제제기에 따른 강의 처리
        @author: msjang
        @param rvid: 강의 예약 id
        @param type: 처리 방법
        @return: none
        """
        print rvid, type
        pr = Problem.objects.get(rvid=rvid)
        pr.is_accepted = 1
        pr.save()
        rv = Reservation.objects.get(rvid=rvid)
        if type == 0:
            # reschdule -> 상태 변경(unscheduled) , history
            self.set_resevation_commit(rvid, 'Unschedule', _data=None)

        elif type == 1:
            # tutor에게 코인 전부 이전 -> 상태변경(confirm), coin, history
            self.set_resevation_commit(rvid, 'Confirm2', _data=None)
            # coin
        elif type == 2:
            # tutee에게 일부 환급 -> 상태변경(refunded), coin, history
            self.set_resevation_commit(rvid, 'Refund', _data=None)
            # coin

    """
    totoring app으로 이동 예정
    """
    
        
    """
    totoring app으로 이동 예정
    """

    def send_mail(self, rv, to_user, from_user, comment_id):
        """
        @summary: 메일 보내기
        @author: msjang
        @param rv: 강의 예약 id
        @param to_user: 받는 유저 id
        @param from_user: 보내는 유저 id
        @param comment_id: 댓글 id
        @return: none
        """
        print 'send_mail()'
        to_user_email = TellpinUser.objects.get(id=to_user).email
        to_user_name = TellpinUser.objects.get(id=to_user).name
        from_user_name = TellpinUser.objects.get(id=from_user).name
        tutor_name = TellpinUser.objects.get(id=rv.tutor_id).name
        tutee_name = TellpinUser.objects.get(id=rv.tutee_id).name
        lesson = Lesson.objects.get(lid=rv.lid)
        _utc_time = TellpinUser.objects.get(id=to_user).timezone
        _utc_time = Timezone.objects.get(id=_utc_time).utc_time
        try:

            utc_min = 0;
            if (int(_utc_time[:3]) > 0):
                utc_min = int(_utc_time[:3]) * 60 + int(_utc_time[4:])
            else:
                utc_min = int(_utc_time[:3]) * 60 - int(_utc_time[4:])
            timegap = timedelta(minutes=utc_min)

            _date = datetime.strptime(str(rv.rv_time)[:8] + str(rv.rv_time)[9:], "%Y%m%d%H%M")
            _date += timegap
            _date = str(_date)

            rv_time_format = _date[:4] + '/' + _date[5:7] + '/' + _date[8:10] + ', ' + _date[11:13] + ':' + _date[14:16]
            print rv_time_format, comment_id, _date

            # title
            if comment_id == 1:
                # request
                _title = self.LESSON_TITLE[comment_id] % (rv_time_format)
            elif comment_id == 5:
                _title = self.LESSON_TITLE[comment_id] % (rv_time_format)
            elif comment_id == 8 or comment_id == 9:
                _title = self.LESSON_TITLE[comment_id] % (from_user_name)
            else:
                _title = self.LESSON_TITLE[comment_id]

            # context
            _context = self.LESSON_CONTEXT[comment_id]
            if comment_id == 1:
                _context = self.LESSON_CONTEXT[comment_id] % (
                    TELLPIN_HOME + 'tutorlessons/status/' + str(rv.rvid) + '/', rv_time_format)
            if comment_id == 2 or comment_id == 3 or comment_id == 6 or comment_id == 7 or comment_id == 12:
                _context = self.LESSON_CONTEXT[comment_id] % (
                    TELLPIN_HOME + 'tutorlessons/status/' + str(rv.rvid) + '/')
            elif comment_id == 5:
                _context = self.LESSON_CONTEXT[comment_id] % (
                    TELLPIN_HOME + 'tutorlessons/status/' + str(rv.rvid) + '/', rv_time_format)
            elif comment_id == 8 or comment_id == 9:
                _context = self.LESSON_CONTEXT[comment_id] % (
                    from_user_name, TELLPIN_HOME + 'tutorlessons/status/' + str(rv.rvid) + '/')

            _context2 = self.LESSON_CONTEXT2[0]
            if comment_id != 3:
                _context2 = self.LESSON_CONTEXT2[0] % (
                    TELLPIN_HOME + 'tutor/' + str(rv.tutor_id), tutor_name, rv.tutor_id,
                    TELLPIN_HOME + 'user/' + str(rv.tutee_id), tutee_name, rv.tutee_id, lesson.title, rv_time_format,
                    rv.duration, "R" + str('%011d' % int(rv.rvid)))
            elif comment_id == 3:
                _context2 = self.LESSON_CONTEXT2[1] % (
                    TELLPIN_HOME + 'tutorlessons/status/' + str(rv.rvid) + '/', TELLPIN_HOME + 'tutor/')

            if comment_id != 11:
                _context += _context2

            send_noti_email(to_user_email, _title, _context)
        except Exception as e:
            print e

    """
    totoring app으로 이동 예정
    """

    # 추가 스케쥴 예약
    def add_schedule_reserve(self, _rvid, req):
        """
        @summary: 추가 스케줄 예약
        @author: msjang
        @param _rvid: 강의 예약 id
        @param req: http request
        @return: result_list( list )
        """
        print 'add_schedule_reserve'
        post_dict = json.loads(req.body)
        # print post_dict['command']
        try:
            rv = Reservation.objects.get(rvid=_rvid)
            #             rv.rv_time=None
            #             rv.status=0
            #             rv.user_confirm=None
            #             rv.tutor_confirm=None
            #             rv.save()
            add_rv_list = []
            add_rv_list.append(rv.rvid)
        # for item in Reservation.objects.filter(bundle_rvid=rv.bundle_rvid, rv_status=0):
        #                 add_rv_list.append(item.rvid)
        except:
            return {"add_schedule": "/tutoring/"}
        # 예약정보 세션에 저장

        print 'add_schedule_reserve2'
        # req.session['reservation']
        post_dict = json.loads(req.body)
        obj = {}
        obj["rvid"] = add_rv_list
        obj["tid"] = rv.tutor_id
        obj["accessType"] = 3
        obj["lid"] = rv.lid
        obj["bundle"] = len(add_rv_list)
        obj["duration"] = rv.duration
        obj["orders"] = rv.bundle_times
        obj["command"] = post_dict["command"]
        if rv.online == 1:
            obj["online_type"] = 1

        if rv.offline == 1:
            obj["online_type"] = 2

        print 'add_schedule_reserve3'
        print obj
        req.session['reservation'] = obj

        print 'add_schedule_reserve4'
        return {"add_schedule": "/tutor/reservation/how/"}

    """
    totoring app으로 이동 예정
    """

    # Approve 진행 시 coin 처리
    def pay_approve(self, _rvid):
        """
        @summary: 강의 approve 시 coin 처리
        @author: msjang
        @param _rvid: 강의 예약 id
        @return: True, False( boolean )
        """
        print 'pay_approve'
        try:
            rv_info = Reservation.objects.get(rvid=_rvid)
            # tutor_uid = Tellpin.objects.get(tid=rv_info.tid).user_id
            online_type = rv_info.online and 1 or 2
            price = self.get_lesson_price(rv_info.lid, online_type, rv_info.bundle_times, rv_info.duration)
            # price["total"] / float(data["bundle"])
            per_price = price["total"] / float(rv_info.bundle_times)
            _user_id = rv_info.tutor_id
            try:
                total_avail = getTutorTotalAvailable(_user_id)
                total_pending = getTutorTotalPending(_user_id)
                total_balance = getTutorTotalBalance(_user_id)
                total_withdraw_pending = getTutorTotalWithPending(_user_id)
                print total_avail, total_pending, total_balance
            except Exception as e:
                print e
                # u_balance = 0
                total_avail = 0
                total_pending = 0
                total_balance = 0

            TutorAccount(user_id=_user_id, total_withdraw_pending=total_withdraw_pending, a_change=0,
                         p_change=per_price, t_change=per_price, total_available=total_avail,
                         total_pending=total_pending + per_price, total_balance=total_balance + per_price, type=1,
                         more_type=1, more_id=rv_info.rvid).save()
        # tutor_uid = rv_info.tutor_id
        #             print rv_info.tutor_id, _rvid
        #             tutee_coin = Coin.objects.get(user_id=rv_info.tutee_id, lesson_number=_rvid)
        #             try:
        #                 t_balance = Coin.objects.filter(user_id=tutor_uid).latest("available_balance").available_balance
        #             except:
        #                 t_balance = 0
        #
        #             print tutor_uid, tutee_coin, t_balance, _rvid
        #             Coin(user_id=tutor_uid, cointype=4, coin=tutee_coin.coin, available_balance=t_balance, lesson_number = _rvid, is_pending=1).save()



        #             try:
        #                 t_user = TellpinUser.objects.get(id=tutor_uid)
        #                 t_user.coin += tutee_coin.coin
        #                 t_user.save()
        #             except:
        #                 print "tutor user id error"

        except Exception as err:
            print err
            return False

        return True

    """
    totoring app으로 이동 예정
    """

    # Confirm 진행 시 coin 처리
    def pay_confirm(self, _rvid, _type=None):
        """
        @summary: 강의 Confirm 진행 시 coin 처리
        @author: msjang
        @param _rvid: 강의 예약 id
        @param _type: None
        @return: True, False( boolean )
        """
        try:
            rv_info = Reservation.objects.get(rvid=_rvid)
            online_type = rv_info.online and 1 or 2
            price = self.get_lesson_price(rv_info.lid, online_type, rv_info.bundle_times, rv_info.duration)
            # price["total"] / float(data["bundle"])
            per_price = price["total"] / float(rv_info.bundle_times)

            # 튜티
            _user_id = rv_info.tutee_id
            print 'pay confirm!!!'
            try:
                total_avail = getTuteeTotalAvailable(_user_id)
                total_pending = getTuteeTotalPending(_user_id)
                total_balance = getTuteeTotalBalance(_user_id)
                print 'tutee'
                print total_avail, total_pending, total_balance
            except Exception as e:
                print e
                # u_balance = 0
                total_avail = 0
                total_pending = 0
                total_balance = 0

            TuteeAccount(user_id=_user_id, a_change=0, p_change=-per_price, t_change=-per_price,
                         total_available=total_avail, total_pending=total_pending - per_price,
                         total_balance=total_balance - per_price, type=4, more_type=2, more_id=rv_info.rvid).save()
            # 튜터
            _user_id = rv_info.tutor_id
            try:
                total_avail = getTutorTotalAvailable(_user_id)
                total_pending = getTutorTotalPending(_user_id)
                total_balance = getTutorTotalBalance(_user_id)
                total_withdraw = getTutorTotalWithPending(_user_id)
                print 'tutor'
                print total_avail, total_pending, total_balance
            except Exception as e:
                print e
                # u_balance = 0
                total_avail = 0
                total_pending = 0
                total_balance = 0

            TutorAccount(user_id=_user_id, total_withdraw_pending=total_withdraw, a_change=per_price,
                         p_change=-per_price, t_change=0, total_available=total_avail + per_price,
                         total_pending=total_pending - per_price, total_balance=total_balance, type=2, more_type=2,
                         more_id=rv_info.rvid).save()

            per_price = math.ceil(per_price * 0.15)
            print per_price
            TutorAccount(user_id=_user_id, total_withdraw_pending=total_withdraw, a_change=-per_price, p_change=0,
                         t_change=-per_price, total_available=total_avail - per_price, total_pending=total_pending,
                         total_balance=total_balance - per_price, type=8, more_type=2, more_id=rv_info.rvid).save()
        # tutee_uid = rv_info.tutee_id
        #             tutee_coin = Coin.objects.get(user_id=tutee_uid, lesson_number=_rvid)
        #             tutee_coin.is_pending = 0
        #             tutee_coin.cointype = 3
        #             tutee_coin.save()
        #
        #             user = TellpinUser.objects.get(id=tutee_uid)
        #             user.coin -= tutee_coin.coin
        #             user.save()
        #
        #             tutor_uid = rv_info.tutor_id
        #             tutor_coin = Coin.objects.get(user_id=tutor_uid, lesson_number=_rvid)
        #             tutor_coin.is_pending = 0
        #             tutor_coin.cointype = 5
        #             tutor_coin.available_balance += tutor_coin.coin
        #             tutor_coin.save()

        except Exception as e:
            print "pay_confirm"
            print e

        return True

    """
    totoring app으로 이동 예정
    """

    # Approve 진행 시 coin 처리
    def pay_refund(self, _rvid):
        """
        @summary: 강의 환불 시 coin 처리
        @author: msjang
        @param _rvid: 강의 예약 id
        @return: True, False( boolean )
        """
        try:
            rv_info = Reservation.objects.get(rvid=_rvid)
            # tutor_uid = Tellpin.objects.get(tid=rv_info.tid).user_id
            online_type = rv_info.online and 1 or 2
            price = self.get_lesson_price(rv_info.lid, online_type, rv_info.bundle_times, rv_info.duration)
            # price["total"] / float(data["bundle"])
            per_price = price["total"] / float(rv_info.bundle_times)
            refund_price = Problem.objects.get(rvid=_rvid).problem_amount

            _user_id = rv_info.tutee_id
            print 'pay refund!!!'
            try:
                total_avail = getTuteeTotalAvailable(_user_id)
                total_pending = getTuteeTotalPending(_user_id)
                total_balance = getTuteeTotalBalance(_user_id)
                print 'tutee'
                print total_avail, total_pending, total_balance
            except Exception as e:
                print e
                # u_balance = 0
                total_avail = 0
                total_pending = 0
                total_balance = 0

            TuteeAccount(user_id=_user_id, a_change=refund_price, p_change=-per_price,
                         t_change=((-per_price) + refund_price), total_available=total_avail + refund_price,
                         total_pending=total_pending - per_price,
                         total_balance=total_balance - per_price + refund_price, type=9, more_type=2,
                         more_id=rv_info.rvid).save()
            # 튜터
            _user_id = rv_info.tutor_id
            try:
                total_avail = getTutorTotalAvailable(_user_id)
                total_pending = getTutorTotalPending(_user_id)
                total_balance = getTutorTotalBalance(_user_id)
                total_withdraw_pending = getTutorTotalWithPending(_user_id)
                print 'tutor'
                print total_avail, total_pending, total_balance
            except Exception as e:
                print e
                # u_balance = 0
                total_avail = 0
                total_pending = 0
                total_balance = 0

            TutorAccount(user_id=_user_id, total_withdraw_pending=total_withdraw_pending,
                         a_change=per_price - refund_price, p_change=-per_price, t_change=-refund_price,
                         total_available=total_avail + per_price - refund_price,
                         total_pending=total_pending - per_price, total_balance=total_balance - refund_price, type=9,
                         more_type=2, more_id=rv_info.rvid).save()



        except Exception as err:
            print err
            return False

        return True

        # check user password

#     def check_password(self, _user_id, _password):
#         """
#         @summary: 유저 password check
#         @author: msjang
#         @param _user_id: 유저 id
#         @param _password: 유저 password
#         @return: True, False( boolean )
#         """
#         try:
#             if encrypt_password(_password) == TellpinUser.objects.get(id=_user_id).password:
#                 return True
#             else:
#                 return False
#         except:
#             return False

    def upload_image(self, image, user_email):
        """
        @summary: 이미지 업로드
        @author: msjang
        @param image: image 파일
        @param user_email: user email address
        @return: image_path, image_name( string )
        """
        image_name = "%d&&%s" % (int(round(time.time() * 1000)), image.name)
        repo_path, image_path = self.make_user_directory(user_email)
        full_path = '%s/%s' % (repo_path, image_name)
        try:
            with open(full_path, 'wb') as fp:
                for chunk in image.chunks():
                    fp.write(chunk)
        except Exception as err:
            print full_path
        return image_path, image_name

    def make_user_directory(self, user):
        """
        @summary: 유저 폴더 생성
        @author: msjang
        @param user: 유저 정보
        @return: directory, image_file_path( string )
        """
        directory = '%s%s' % (BASE_DIR, self.PROFILE_IMAGE_FILE_DIR)
        # directory = '%s/%s' % (directory, user)
        ##image_path = '%s/%s' % (self.PROFILE_IMAGE_FILE_DIR, user)
        if not os.path.exists(directory):
            os.makedirs(directory)
        return directory, self.PROFILE_IMAGE_FILE_DIR

    def upload_resume(self, image, user_email):
        """
        @summary: resume 파일 업로드
        @author: msjang
        @param image: image 파일
        @Param user_email: 유저 email
        @return: image_path, image_name( string )
        """
        # int(round(time.time() * 1000)) time milliseconds
        image_name = "%d&&%s" % (int(round(time.time() * 1000)), image.name)
        repo_path, image_path = self.make_user_directory_temp(user_email)
        full_path = '%s/%s' % (repo_path, image_name)
        # image_path = '%s/%s' % (image_path, image_name)
        try:
            with open(full_path, 'wb') as fp:
                for chunk in image.chunks():
                    fp.write(chunk)
        except Exception as err:
            print full_path, str(err)
        return image_path, image_name

    def make_user_directory_temp(self, user):
        """
        @summary: 해당 유저 폴더 생성
        @author: msjang
        @param user: user 정보
        @return: individual, image_path( string )
        """
        directory = '%s%s' % (BASE_DIR, self.RESUME_TEMP_FILE_DIR)
        individual = '%s/%s' % (directory, user)
        image_path = '%s/%s' % (self.RESUME_TEMP_FILE_DIR, user)

        if not os.path.exists(directory):
            os.makedirs(directory)

        if not os.path.exists(individual):
            os.makedirs(individual)

        return individual, image_path

    def upload_image_problem(self, image, user_email):
        """
        @summary: 문제 제기시 파일 업로드
        @author: msjang
        @param image: image 파일
        @param user_email: user email
        @return: image_path, image_name( string )
        """
        # int(round(time.time() * 1000)) time milliseconds
        print image, user_email
        image_name = "%d&&%s" % (int(round(time.time() * 1000)), image.name)
        print 1
        repo_path, image_path = self.make_user_directory_problem(user_email)
        print 2
        full_path = '%s/%s' % (repo_path, image_name)
        print 3
        # image_path = '%s/%s' % (image_path, image_name)
        try:
            with open(full_path, 'wb') as fp:
                for chunk in image.chunks():
                    fp.write(chunk)
        except Exception as err:
            print full_path
        return image_path, image_name

    def make_user_directory_problem(self, user):
        """
        @summary: 유저 문제제기 저장 폴더 생성
        @author: msjang
        @param user: 유저 정보
        @return: directory, profile_image( string )
        """
        directory = '%s%s' % (BASE_DIR, self.PROBLEM_IMAGE_FILE_DIR)
        # directory = '%s/%s' % (directory, user)
        ##image_path = '%s/%s' % (self.PROFILE_IMAGE_FILE_DIR, user)
        if not os.path.exists(directory):
            os.makedirs(directory)

        return directory, self.PROFILE_IMAGE_FILE_DIR

    def upload_file_resume(self, image, user_email):
        """
        @summary: resume 파일 업로드
        @author: msjang
        @param image: image 파일
        @param user_email: user email
        @return directory, resume폴더 생성
        """
        print "upload_file_resume"
        print image, user_email
        image_name = "%d&&%s" % (int(round(time.time() * 1000)), image.name)
        repo_path, image_path = self.make_user_directory_resume(user_email)
        full_path = '%s/%s' % (repo_path, image_name)

        try:
            with open(full_path, 'wb') as fp:
                for chunk in image.chunks():
                    fp.write(chunk)
        except Exception as err:
            print full_path
        return image_path, image_name

    def make_user_directory_resume(self, user):
        """
        @summary: 유저 resume 파일 업로드 생성
        @author: msjang
        @param user: 유저 정보
        @return: directory, resume_file_dir( string )
        """
        directory = '%s%s' % (BASE_DIR, self.RESUME_FILE_DIR)
        if not os.path.exists(directory):
            os.makedirs(directory)

        return directory, self.RESUME_FILE_DIR

    # 메세지 보내기
    def sendMessage(self, to_user_id, from_user_id, _message, _rvid):
        result = []
        try:
            print 'sendMessage()'
            rv = Reservation.objects.get(rvid=_rvid)
            message = RV_Message()
            message.to_user_id = to_user_id
            message.from_user_id = from_user_id
            message.message = _message
            message.rvid = _rvid
            message.save()
            utc_time_id = TellpinUser.objects.get(id=from_user_id).timezone
            utc_time = Timezone.objects.get(id=utc_time_id).utc_time

            print utc_time
            result = self.getMessage(from_user_id, to_user_id, _rvid, utc_time)
        except Exception as e:
            result = []
            obj = {}
            obj['log'] = str(e)
            result.append(obj)
        finally:
            return result, message

    # 메세지 목록
    def getMessage(self, user_id, to_user_id, _rvid, _utc_time='+00:00'):
        """
        @summary: 메세지 목록
        @author: msjang
        @param user_id: 유저 id
        @param to_user_id: 받는 유저 id
        @param _rvid: 강의 예약 id
        @param _utc_time: utc time 정보
        @return: result( list )
        """
        result = []
        try:
            list = RV_Message.objects.filter(
                Q(to_user_id=user_id, from_user_id=to_user_id) | Q(from_user_id=user_id, to_user_id=to_user_id),
                rvid=_rvid).order_by('created_time')
            print list
            rv = Reservation.objects.get(rvid=_rvid)
            for i in list:
                if rv.tutor_id == user_id and i.tutor_status == 0:
                    i.tutor_status = 1
                    i.save()
                elif rv.tutee_id == user_id and i.tutee_status == 0:
                    i.tutee_status = 1
                    i.save()
                print 'for list i'

                utc_min = 0;
                if (int(_utc_time[:3]) > 0):
                    utc_min = int(_utc_time[:3]) * 60 + int(_utc_time[4:])
                else:
                    utc_min = int(_utc_time[:3]) * 60 - int(_utc_time[4:])
                timegap = timedelta(minutes=utc_min)
                _date = datetime.strptime(str(i.created_time)[:16], "%Y-%m-%d %H:%M")
                _date += timegap

                timeDay = str(_date).split()
                print timeDay
                flag = True
                for j in result:
                    if j['day'] == timeDay[0]:
                        flag = False
                        break
                    flag = True
                if flag:  # 배열에 존재하지 않을 경우
                    obj = {}
                    obj['day'] = timeDay[0]
                    obj['message'] = []
                    result.append(obj)
                for j in result:
                    if j['day'] == timeDay[0]:
                        messageObj = {}
                        messageObj['mid'] = i.mid
                        messageObj['to_user_id'] = i.to_user_id
                        messageObj['from_user_id'] = i.from_user_id
                        messageObj['message'] = i.message
                        messageObj['user_id'] = user_id
                        messageObj['time'] = _date.strftime('%H:%M %p')
                        if user_id == i.to_user_id:  # 받은 메세지
                            messageObj['type'] = 'receive'
                        else:
                            messageObj['type'] = 'send'
                        j['message'].append(messageObj)
        except Exception as e:
            print e
            obj = {}
            obj['message'] = str(e)
            result.append(obj)
        finally:
            return result

    # Rating 더가져오기
    def moreRating(self, rtid, tutee_id, tutor_id, user_id):
        """
        @summary: rating 정보 가져오기
        @author: msjang
        @param rtid: rating id
        @param tutee_id: 튜티 id
        @param tutor_id: 튜터 id
        @param: user_id: user id
        @return: result( list )
        """
        result = []
        try:
            cursor = connection.cursor()
            sql = '''
                SELECT *
                     , DATE_ADD( created_date, INTERVAL( SELECT y.utc_time FROM timezone Y WHERE id = ( SELECT timezone FROM user_auth_tellpinuser WHERE id = %s ) ) HOUR )  AS created_date
                FROM ratings
                WHERE lid IN ( SELECT lid FROM lesson WHERE user_id = %s )
                  AND user_id = %s
                  AND rtid != %s
                ORDER BY created_date DESC;
            '''
            cursor.execute(sql, [user_id, tutor_id, tutee_id, rtid])
            result = common.dictfetchall(cursor)
        except Exception as e:
            print 'moreRating', str(e)
        finally:
            cursor.close()
            return result


class TutorApplyService(object):
    def __init__(self):
        pass

    @staticmethod
    def get_tutor_doc(user_id):
        """
        @summary: 지원서 정보
        @author: msjang
        @param user_id: user
        @return True, False
        """
        pass

    @staticmethod
    def get_tutor_doc_status(user_id):
        """
        @summary: 튜터 지원서 상태 정보
        @author: msjang
        @param user_id: user
        @return none(0), applied(1), modified(2), confirmed(3), rejected(4)
        """
        if TutorService.is_tutor(user_id):#tutor
            return 1

        if TutorDoc.objects.filter(user_id=user_id, status=1).exists():#지원서 제출
            return 2

        return 0

    @staticmethod
    def is_doc_complete(user_id):
        """
        @summary: 작성 완료 된 튜터 지원서가 있는지 확인
        @author: msjang
        @param user_id: user
        @return True, False
        """
        return TutorDoc.objects.filter(user_id=user_id, status__in=[1, 3], apply_step__gte=4).exists()

    @staticmethod
    def is_doc_personal(user_id):
        """
        @summary: 튜터 지원 단계 중 개인정보 입력 여부
        @author: msjang
        @param user_id: user
        @return True, False
        """
        return TutorDoc.objects.filter(user_id=user_id, status=0, apply_step__gte=1).exists()

    @staticmethod
    def is_doc_profile(user_id):
        """
        @summary: 튜터 지원 단계 중 프로필 입력 여부
        @author: msjang
        @param user_id: user
        @return True, False
        """
#         status = Q(status=0) | Q(status=3)
        return TutorDoc.objects.filter(user_id=user_id, apply_step__gte=2).exists()

    @staticmethod
    def is_doc_payment(user_id):
        """
        @summary: 튜터 지원 단계 중 payment 입력 여부
        @author: msjang
        @param user_id: user
        @return True, False
        """
        return TutorDoc.objects.filter(user_id=user_id, status=0, apply_step__gte=3).exists()

    @staticmethod
    def get_doc_personal(user_id):
        """
        @summary: 튜터 지원 단계 중 개인 정보
        @author: msjang
        @param user_id: user
        """
        if TutorApplyService.is_doc_personal(user_id):
            queryset = TutorDoc.objects.get(user_id=user_id, status=0, apply_step__gte=1)
        else:
            queryset = Tutee.objects.get(user_id=user_id)

        result = model_to_dict(queryset)
        if TutorApplyService.is_doc_personal(user_id):
            result['photo_filepath'] = CommonService.PROFILE_IMAGE_FILE_DIR

        return result

    @staticmethod
    def set_doc_personal(user_id, personal):
        """
        @summary: 튜터 지원 단계 중 개인 정보 저장
        @author: msjang
        @param user_id: user
        @param personal
        """
        result = True
        foreign_key = [
            'native1_id', 'native2_id', 'native3_id',
            'lang1_id', 'lang2_id', 'lang3_id',
            'lang4_id', 'lang5_id', 'lang6_id',
            'user', 'timezone_id',
            'lang1_level', 'lang2_level', 'lang3_level',
            'lang4_level', 'lang5_level', 'lang6_level',
            'teaching1_id', 'teaching2_id', 'teaching3_id',
            'from_country_id', 'from_city_id', 'livingin_country_id', 'livingin_city_id', 'currency_id',
            'work1', 'work2', 'work3', 'education1', 'education2', 'education3',
            'certificate1', 'certificate2', 'certificate3',
            'paymentinfo'
        ]
        model_key = TutorDoc._meta.get_all_field_names()
        try:
            personal['facetime_id'] = None
            personal['skype_id'] = None
            personal['hangout_id'] = None
            personal['qq_id'] = None
            for tool in personal['tools']:
                if tool['name'] == 'Facetime':
                    personal['facetime_id'] = tool['account']
                elif tool['name'] == 'Skype':
                    personal['skype_id'] = tool['account']
                elif tool['name'] == 'Hangout':
                    personal['hangout_id'] = tool['account']
                else:
                    personal['qq_id'] = tool['account']
            tutor_doc, created = TutorDoc.objects.get_or_create(user_id=user_id, status=0)
            tutor_doc.apply_step = 1
            for key, value in personal.items():
                if key in model_key:
                    if key in foreign_key:
                        key = '%s_%s' % (key, 'id')
                    setattr(tutor_doc, key, value)
            copied = CommonService.copy_image(tutor_doc.photo_filename, 'profile')
            if copied:
                tutor_doc.save()
                tutor_doc = TutorDoc.objects.select_related(
                    'from_country_id', 'from_city_id',
                    'livingin_country_id', 'livingin_city_id'
                ).get(user_id=user_id, status=0)
                tutor_doc.from_country = tutor_doc.from_country_id.country
                tutor_doc.from_city = tutor_doc.from_city_id.city
                tutor_doc.livingin_country = tutor_doc.livingin_country_id.country
                tutor_doc.livingin_city = tutor_doc.livingin_city_id.city
                tutor_doc.save()
                CommonService.file_copy_update(tutor_doc.photo_filename, 'profile')
        except Exception as err:
            result = False
            print "@@@@@set_doc_personal" + str(err)
        finally:
            return result

    @staticmethod
    def get_doc_profile(user_id):
        """
        @summary: 튜터 지원 단계 중 프로필 정보
        @author: msjang
        @param user_id: user
        @return object
        """
        obj = dict()
        try:
            if TutorApplyService.is_doc_profile(user_id):
#                 status = Q(status=0) | Q(status=3)
                tutor_doc = TutorDoc.objects.select_related(
                    'work1', 'work2', 'work3',
                    'work1__fid', 'work2__fid', 'work3__fid',
                    'education1', 'education2', 'education3',
                    'education1__fid', 'education2__fid', 'education3__fid',
                    'certificate1', 'certificate2', 'certificate3',
                    'certificate1__fid', 'certificate2__fid', 'certificate3__fid'
                ).get(user_id=user_id, apply_step__gte=2)
                obj = model_to_dict(tutor_doc,
                                    fields=[
                                        'short_intro',
                                        'long_intro',
                                        'video_intro'
                                    ])
                if tutor_doc.work1:
                    obj['work1'] = model_to_dict(tutor_doc.work1)
                    if tutor_doc.work1.fid:
                        obj['work1_filepath'] = tutor_doc.work1.fid.filepath
                        obj['work1_filename'] = tutor_doc.work1.fid.filename.split("&&", 1)[1]
                if tutor_doc.work2:
                    obj['work2'] = model_to_dict(tutor_doc.work2)
                    if tutor_doc.work2.fid:
                        obj['work2_filepath'] = tutor_doc.work2.fid.filepath
                        obj['work2_filename'] = tutor_doc.work2.fid.filename.split("&&", 1)[1]
                if tutor_doc.work3:
                    obj['work3'] = model_to_dict(tutor_doc.work3)
                    if tutor_doc.work3.fid:
                        obj['work3_filepath'] = tutor_doc.work3.fid.filepath
                        obj['work3_filename'] = tutor_doc.work3.fid.filename.split("&&", 1)[1]
                if tutor_doc.education1:
                    obj['education1'] = model_to_dict(tutor_doc.education1)
                    if tutor_doc.education1.fid:
                        obj['education1_filepath'] = tutor_doc.education1.fid.filepath
                        obj['education1_filename'] = tutor_doc.education1.fid.filename.split("&&", 1)[1]
                if tutor_doc.education2:
                    obj['education2'] = model_to_dict(tutor_doc.education2)
                    if tutor_doc.education2.fid:
                        obj['education2_filepath'] = tutor_doc.education2.fid.filepath
                        obj['education2_filename'] = tutor_doc.education2.fid.filename.split("&&", 1)[1]
                if tutor_doc.education3:
                    obj['education3'] = model_to_dict(tutor_doc.education3)
                    if tutor_doc.education3.fid:
                        obj['education3_filepath'] = tutor_doc.education3.fid.filepath
                        obj['education3_filename'] = tutor_doc.education3.fid.filename.split("&&", 1)[1]
                if tutor_doc.certificate1:
                    obj['certificate1'] = model_to_dict(tutor_doc.certificate1)
                    if tutor_doc.certificate1.fid:
                        obj['certificate1_filepath'] = tutor_doc.certificate1.fid.filepath
                        obj['certificate1_filename'] = tutor_doc.certificate1.fid.filename.split("&&", 1)[1]
                if tutor_doc.certificate2:
                    obj['certificate2'] = model_to_dict(tutor_doc.certificate2)
                    if tutor_doc.certificate2.fid:
                        obj['certificate2_filepath'] = tutor_doc.certificate2.fid.filepath,
                        obj['certificate2_filename'] = tutor_doc.certificate2.fid.filename.split("&&", 1)[1]
                if tutor_doc.certificate3:
                    obj['certificate3'] = model_to_dict(tutor_doc.certificate3)
                    if tutor_doc.certificate3.fid:
                        obj['certificate3_filepath'] = tutor_doc.certificate3.fid.filepath
                        obj['certificate3_filename'] = tutor_doc.certificate3.fid.filename.split("&&", 1)[1]
            else:
                obj = None
        except Exception as err:
            print "@@@@@get_doc_profile" + str(err)
            obj['err'] = str(err)
        finally:
            return obj

    @staticmethod
    @transaction.atomic
    def set_doc_profile(user_id, profile):
        """
        @summary: 튜터 지원 단계 중 프로필 정보 저장
        @author: msjang
        @param user_id: 유저 id
        @param profile: profile 정보
        @return True, False
        """
        result = True
        try:
            status = Q(status=0) | Q(status=3)
            tutor_doc = TutorDoc.objects.select_related('work1', 'work2', 'work3',
                                                        'education1', 'education2', 'education3',
                                                        'certificate1', 'certificate2', 'certificate3',
                                                        'work1__fid', 'work2__fid', 'work3__fid',
                                                        'education1__fid', 'education2__fid', 'education3__fid',
                                                        'certificate1__fid', 'certificate2__fid', 'certificate3__fid'
                                                        ).get(status, user_id=user_id,
                                                              apply_step__gte=1)

            work_key_list = ['work1', 'work2', 'work3']
            edu_key_list = ['education1', 'education2', 'education3']
            cert_key_list = ['certificate1', 'certificate2', 'certificate3']
            resume_list = ['work_exp', 'education', 'certification']
            resume_dict = dict()
            file_dict = dict()
            for key in work_key_list + edu_key_list + cert_key_list:
                file_dict[key] = None
                resume_dict[key] = None
            work_list = profile['work_exp']
            education_list = profile['education']
            certification_list = profile['certification']
            for upload_file in profile['upload_file_list']:
                file_id = CommonService.file_insert(upload_file['filepath'],
                                                    upload_file['filename'],
                                                    'PROFILE')
                if upload_file['type'] == 'work1_file':
                    file_dict['work1'] = file_id
                elif upload_file['type'] == 'work2_file':
                    file_dict['work2'] = file_id
                elif upload_file['type'] == 'work3_file':
                    file_dict['work3'] = file_id
                elif upload_file['type'] == 'education1_file':
                    file_dict['education1'] = file_id
                elif upload_file['type'] == 'education2_file':
                    file_dict['education2'] = file_id
                elif upload_file['type'] == 'education3_file':
                    file_dict['education3'] = file_id
                elif upload_file['type'] == 'certification1_file':
                    file_dict['certificate1'] = file_id
                elif upload_file['type'] == 'certification2_file':
                    file_dict['certificate2'] = file_id
                else:
                    file_dict['certificate3'] = file_id
            for key, value in profile.items():
                if key == 'upload_file_list' or key in resume_list:
                    continue
                else:
                    setattr(tutor_doc, key, value)
            for work in work_list:
                work_key, work_value = work.items()[0]
                work_id = TutorApplyService.set_doc_work(getattr(tutor_doc, work_key),
                                                         work_value,
                                                         file_dict[work_key])
                resume_dict[work_key] = work_id
            for edu in education_list:
                edu_key, edu_value = edu.items()[0]
                edu_id = TutorApplyService.set_doc_edu(getattr(tutor_doc, edu_key),
                                                       edu_value,
                                                       file_dict[edu_key])
                resume_dict[edu_key] = edu_id
            for cert in certification_list:
                cert_key, cert_value = cert.items()[0]
                cert_id = TutorApplyService.set_doc_cert(getattr(tutor_doc, cert_key),
                                                         cert_value,
                                                         file_dict[cert_key])
                resume_dict[cert_key] = cert_id
            for key, value in resume_dict.items():
                if value:
                    key += '_id'
                    setattr(tutor_doc, key, value)
                else:
                    key += '_id'
                    setattr(tutor_doc, key, None)
            if tutor_doc.apply_step < 2:
                tutor_doc.apply_step = 2
            if tutor_doc.status == 3 or tutor_doc.status == 0:
                tutor_doc.modify_type = 3
                tutor_doc.resume_modify = "111111111"
            tutor_doc.save()
        except Exception as err:
            print "@@@@@set_doc_profile :" + str(err)
            result = False
        finally:
            return result

    @staticmethod
    def get_doc_work(work_id):
        """
        @summary: 튜터 지원 단계 중 work 경험
        @author: msjang
        @return True, False
        """
        tutor_work = dict()
        try:
            tutor_work_obj = TutorWork.objects.get(id=work_id)
            tutor_work = model_to_dict(tutor_work_obj)
        except Exception as err:
            print "@@@@@get_doc_work  :" + str(err)
            tutor_work = None
        finally:
            return tutor_work

    @staticmethod
    def set_doc_work(work_obj, work_dict, fid=0):
        """
        @summary: 튜터 지원 단계 중 work 경험 저장
        @author: msjang
        @return True, False
        """
        result = 0
        try:
            if work_dict['info']:
                if work_obj:
                    tutor_work = work_obj
                else:
                    tutor_work = TutorWork()
                for key, value in work_dict.items():

                    setattr(tutor_work, key, value)
                if work_dict['file_del_yn']:
                    tutor_work.fid = None
                else:
                    if fid:
                        tutor_work.fid_id = fid
                tutor_work.save()
                result = tutor_work.pk
            else:
                if work_obj:
                    result = None
        except Exception as err:
            result = 0
            print "@@@@@set_doc_work  :" + str(err)
        finally:
            return result

    @staticmethod
    def get_doc_edu(education_id):
        """
        @summary: 튜터 지원 단계 중 education 정보
        @author: msjang
        @return True, False
        """
        tutor_education = dict()
        try:
            tutor_education_obj = TutorEducation.objects.get(id=education_id)
            tutor_education = model_to_dict(tutor_education_obj)
        except Exception as err:
            print "@@@@@get_doc_edu  :" + str(err)
            tutor_education = None
        finally:
            return tutor_education

    @staticmethod
    def set_doc_edu(education_obj, education_dict, fid=0):
        """
        @summary: 튜터 지원 단계 중 education 정보 저장
        @author: msjang
        @return True, False
        """
        result = 0
        try:
            if education_dict['info']:
                if education_obj:
                    tutor_education = education_obj
                else:
                    tutor_education = TutorEducation()
                for key, value in education_dict.items():
                    setattr(tutor_education, key, value)
                if education_dict['file_del_yn']:
                    tutor_education.fid = None
                else:
                    if fid:
                        tutor_education.fid_id = fid
                tutor_education.save()
                result = tutor_education.pk
            else:
                if education_obj:
                    result = None
        except Exception as err:
            result = 0
            print "@@@@@set_doc_edu  :" + str(err)
        finally:
            return result

    @staticmethod
    def get_doc_cert(certificate_id):
        """
        @summary: 튜터 지원 단계 중 certification 정보
        @author: msjang
        @return True, False
        """
        tutor_certificate = dict()
        try:
            tutor_certificate_obj = TutorEducation.objects.get(id=certificate_id)
            tutor_certificate = model_to_dict(tutor_certificate_obj)
        except Exception as err:
            print "@@@@@get_doc_cert :" + str(err)
            tutor_certificate = None
        finally:
            return tutor_certificate

    @staticmethod
    def set_doc_cert(certification_obj, certification_dict, fid=0):
        """
        @summary: 튜터 지원 단계 중 certification 정보 저장
        @author: msjang
        @return True, False
        """
        result = 0
        try:
            if certification_dict['info']:
                if certification_obj:
                    tutor_certification = certification_obj
                else:
                    tutor_certification = TutorCertificate()
                for key, value in certification_dict.items():
                    setattr(tutor_certification, key, value)
                if certification_dict['file_del_yn']:
                    tutor_certification.fid = None
                else:
                    if fid:
                        tutor_certification.fid_id = fid
                tutor_certification.save()
                result = tutor_certification.pk
            else:
                if certification_obj:
                    result = None
        except Exception as err:
            result = 0
            print "@@@@@set_doc_cert :" + str(err)
        finally:
            return result

    @staticmethod
    def get_doc_payment(user_id):
        """
        @summary: 튜터 지원 단계 중 payment 정보
        @author: msjang
        @param user_id: 유저 id
        @return True, False
        """
        payment_dict = dict()
        try:
            if TutorApplyService.is_doc_payment(user_id):
                queryset = TutorDoc.objects.select_related(
                    'paymentinfo'
                ).get(user_id=user_id, status=0, apply_step__gte=3)
                payment_dict = model_to_dict(queryset.paymentinfo, exclude=['created_time'])
            else:
                payment_dict = None
        except Exception as err:
            print "@@@@@get_doc_payment :" + str(err)
            payment_dict = None
        finally:
            return payment_dict

    @staticmethod
    @transaction.atomic
    def set_doc_payment(user_id, payment):
        """
        @summary: 튜터 지원 단계 중 지원서에 payment 정보 저장
        @author: msjang
        @param user_id: 유저 id
        @param payment: payment 정보
        @return True, False
        """
        result = True
        try:
            paymentinfo_id = TutorApplyService.create_payment(payment)
            if TutorApplyService.is_doc_payment(user_id):
                tutor_doc = TutorDoc.objects.select_related(
                    'paymentinfo'
                ).get(user_id=user_id, status=0, apply_step__gte=3)
                tutor_doc.paymentinfo_id = paymentinfo_id
            else:
                tutor_doc = TutorDoc.objects.select_related(
                    'paymentinfo'
                ).get(user_id=user_id, status=0, apply_step__gte=2)
                tutor_doc.paymentinfo_id = paymentinfo_id
            if tutor_doc.apply_step < 3:
                tutor_doc.apply_step = 3
            tutor_doc.save()
        except Exception as err:
            print "@@@@@set_doc_payment :" + str(err)
            result = False
        finally:
            return result

    @staticmethod
    def create_payment(payment):
        """
        @summary: 튜터 지원 단계 중 payment 정보 저장
        @author: msjang
        @param payment: payment 정보
        @return True, False
        """
        result = 0
        try:
            foreign_key = [
                'country_region'
            ]
            tutor_payment = TutorPaymentInfo()
            for key, value in payment.items():
                if key in foreign_key:
                    key += '_id'
                setattr(tutor_payment, key, value)
            tutor_payment.save()
            result = tutor_payment.pk
        except Exception as err:
            print "@@@@@create_payment :" + str(err)
            result = 0
        finally:
            return result

    @staticmethod
    def submit_doc(user_id):
        """
        @summary: 튜터 지원서 제출
        @author: msjang
        @param user_id: 유저 id
        @return True, False
        """
        result = True
        try:
            tutor_doc = TutorDoc.objects.get(user_id=user_id, status=0)
            tutor_doc.status = 1
            tutor_doc.apply_step = 4
            tutor_doc.save()
        except Exception as err:
            print "@@@@@submit_doc :" + str(err)
            result = False
        finally:
            return result

    @staticmethod
    @transaction.atomic
    def confirm_tutor_doc(user_id):
        """
        @summary: 튜터 지원서 confirm
        @author: msjang
        @param user_id: 유저 id
        @return True, False
        """
        result = True
        foreign_key = [
            'native1_id', 'native2_id', 'native3_id',
            'lang1_id', 'lang2_id', 'lang3_id',
            'lang4_id', 'lang5_id', 'lang6_id',
            'user', 'timezone_id',
            'lang1_level', 'lang2_level', 'lang3_level',
            'lang4_level', 'lang5_level', 'lang6_level',
            'teaching1_id', 'teaching2_id', 'teaching3_id',
            'from_country_id', 'from_city_id', 'livingin_country_id', 'livingin_city_id',
            'work1', 'work2', 'work3', 'education1', 'education2', 'education3',
            'certificate1', 'certificate2', 'certificate3',
            'paymentinfo', 'currency_id'
        ]
        try:
            tutor_doc = TutorDoc.objects.get(user_id=user_id, status=1, apply_step=4)
            tutor_doc.status = 3
            tutor_doc.save()
            tutor_doc_dict = model_to_dict(tutor_doc,
                                           exclude=[
                                               'created_time',
                                               'updated_time'
                                           ])
            tutor = Tutor()
            tutee = Tutee.objects.get(user_id=user_id)
            tutor_keys = tutor._meta.get_all_field_names()
            tutee_keys = tutee._meta.get_all_field_names()
            for key, value in tutor_doc_dict.items():
                if key in tutor_keys:
                    if key in foreign_key:
                        key += '_id'
                    setattr(tutor, key, value)
                if key in tutee_keys:
                    if key in foreign_key:
                        key += '_id'
                    setattr(tutee, key, value)
            tutor.user.is_tutor = 1
            tutor.user.save()
            tutor.save()
            tutee.save()
        except Exception as err:
            result = False
            print "@@@@@confirm_tutor_doc :" + str(err)
        finally:
            return result

    @staticmethod
    def reject_tutor_doc(user_id):
        """
        @summary: 튜터 지원서 reject
        @author: msjang
        @param user_id: 유저 id
        @return True, False
        """
        result = True
        try:
            tutor_doc = TutorDoc.objects.get(user_id=user_id, status=1, apply_step=4)
            tutor_doc.status = 4
            tutor_doc.save()
        except Exception as err:
            print "@@@@@reject_tutor_doc :" + str(err)
            result = False
        finally:
            return result


class TutorService(object):
    def __init__(self):
        pass


    # 사용자 튜터 확인
    @staticmethod
    def is_tutor(_user_id):
        """
        @summary: 튜터 여부
        @author: msjang
        @param _user_id: 유저 id
        @return: True, False( boolean )
        """
        print _user_id
        try:
            if TellpinUser.objects.get(user=_user_id).is_tutor == 1:
                return True
            else:
                return False
        except Exception as err:
            print "@@@@@is_tutor :" + str(err)
            return False

    # 강사 본인 확인
    @staticmethod
    def check_mine(_tid, _user_id):
        """
        @summary: 튜터 == 본인 확인 함수
        @author: msjang
        @param _tid: 튜터 id
        @param _user_id: 유저 id
        @return: boolean
        """
        try:
            if int(_tid) == int(_user_id):
                return True
            else:
                return False
        except Exception as err:
            print "@@@@@check_mine :" + str(err)
            return False

    @staticmethod
    def get_user_id(_email):
        """
        @summary: get user id
        @author: khyun
        @param _email : user email
        @return: user id
        """
        # 사용자 id 얻기
        try:
            return TellpinUser.objects.get(email=_email).user_id
        except Exception as err:
            print "@@@@@get_user_id :" + str(err)
            return None


    @staticmethod
    def set_lesson_notification(_command, from_user, to_user, rv, comment_id):
        """
        @summary: 강의 알림 설정
        @author: msjang
        @param _command: 알림타입
        @param from_user: 알림 보낸 유저 id
        @param to_user: 알림 받은 유저 id
        @param rv: 강의 예약 id
        @param comment_id: 댓글 id
        @return: none
        """
        print 'set_lesson_notification()' + str(_command)
        print rv

        # type, 1:lesson, 2:club, 3:trans, 4:qa
        # type_msg: type messge
        # ntid : rvid
        # msg_type, msg_id
        redirect_url = "/tutorlessons/lesson/" + str(rv) + "/"
        # Notification
        Notification(sender_id=from_user, receiver_id=to_user, type=1, noti_text=comment_id, ref_id=rv,
                     redirect_url=redirect_url).save()
        # send mail
        #self.send_mail(rv, to_user, from_user, comment_id)

    @staticmethod
    def bookmark_list(user_id):
        """
        @summary: 해당 유저와 bookmark 관계인 튜터 리스트
        @param user_id: 유저 id
        @return: 튜터 id list
        """
        bookmark_list = []
        try:
            queryset = Bookmark.objects.filter(user_id=user_id).values_list('tutor_id', flat=True)
            bookmark_list = list(queryset)
        except Exception as err:
            print '@@@@@bookmark_list : ', str(err)
            bookmark_list = []
        finally:
            return bookmark_list

    @staticmethod
    def get_language_info():
        """
        @summary: 언어 정보
        @author: msjang
        @param none
        @return: language_list( list )
        """
        language_list = []
        languages = LanguageInfo.objects.all()

        for language in languages:
            language_obj = {}
            language_obj["id"] = language.id
            language_obj["en_name"] = language.lang_english
            if language.lang_local != '':
                language_obj["name"] = language.lang_english + ' / ' + language.lang_local
            else:
                language_obj['name'] = language.lang_english
            language_list.append(language_obj)
        return language_list

    @staticmethod
    def save_user_profile(profile, isTutor, request):
        """
        @summary: 튜터 정보 저장
        @author: msjang, shkim
        @param profile: profile 정보
        @return: user.name, user.profile_photo, timezone.utc_time, timezone.utc_location( string )
        """

        if isTutor:
            print "comes here for tutor"
            user_model = Tutor.objects.select_related('user', 'from_country_id', 'from_city_id', 'timezone_id', 'currency_id').get(user = profile["user"])
        else:
            print "comes here for tutee"
            user_model = Tutee.objects.select_related('user', 'from_country_id', 'from_city_id', 'timezone_id', 'currency_id').get(user = profile["user"])

        #user_model.user = profile["user"]
        #if isTutor:

        if user_model.display_language != profile["display_language"]:
            CommonService.change_local_language(request, profile["display_language"])
        if profile["birthdate"]["year"] == None:
            user_model.birthdate = None
        else:
            birth_date = date(profile["birthdate"]["year"], profile["birthdate"]["month"], profile["birthdate"]["day"])
            user_model.birthdate = birth_date.strftime("%Y-%m-%d")

        if profile["photo_filename"]:
            copied = CommonService.copy_image(profile["photo_filename"], 'profile')
            CommonService.file_copy_update(profile["photo_filename"], 'profile')
        if profile["from_country_id"] is not 0:
            user_model.from_country_id = CountryInfo.objects.get(id = profile["from_country_id"])
            user_model.from_country = user_model.from_country_id.country
        if profile["from_city_id"] is not 0:
            user_model.from_city_id = CityInfo.objects.get(id = profile["from_city_id"])
            user_model.from_city = user_model.from_city_id.city
        if profile["livingin_country_id"] is not 0:
            user_model.livingin_country_id = CountryInfo.objects.get(id = profile["livingin_country_id"])
            user_model.livingin_country = user_model.livingin_country_id.country
        if profile["livingin_city_id"] is not 0:
            user_model.livingin_city_id = CityInfo.objects.get(id = profile["livingin_city_id"])
            user_model.livingin_city = user_model.livingin_city_id.city
        user_model.gender = profile["gender"]
        user_model.display_language = profile["display_language"]
        if profile["currency_id"] is not 0:
            user_model.currency_id = CurrencyInfo.objects.get(id = profile["currency_id"])
            user_model.currency = user_model.currency_id.code
        user_model.photo_filename = profile["photo_filename"]
        if profile["timezone_id"] is not 0:
            user_model.timezone_id = TimezoneInfo.objects.get(id = profile["timezone_id"])
            user_model.timezone = user_model.timezone_id.utc_location
            request.session['utc_delta'] = TimezoneInfo.objects.get(id = profile["timezone_id"]).utc_delta
        timezone = user_model.timezone_id
        user_model.name = profile["name"]
        user_model.save()

        #else:
        """
            if profile["photo_filename"]:
                copied = CommonService.copy_image(profile["photo_filename"], 'profile')
                if copied:
                    user_model.photo_filename = profile["photo_filename"]
                    CommonService.file_copy_update(profile["photo_filename"], 'profile')

            for field in profile:
                if field == 'birthdate':
                    if profile[field]["year"] == None:
                        user_model.birthdate = ""
                    else:
                        birth_date = date(profile[field]["year"], profile[field]["month"], profile[field]["day"])
                        user_model.birthdate = birth_date.strftime("%Y-%m-%d")
                else:
                    pass
            user_model.name = profile["name"]
            user_model.gender = profile["gender"]
            user_model.display_language = profile["display_language"]
            user_model.save()
            timezone = user_model.timezone_id
        """

        request.session['currency_exchange'] = TutorService.get_currency_exchange(profile["user"])

        return user_model.name, user_model.photo_filename, timezone.utc_time, timezone.utc_location

    @staticmethod
    def get_currency_exchange(user_id):
        """
        @summary 사용자가 지정한 currency에 따른 현지 통화 환율 구하기
        @param user_id: login 한 사용자
        @return currency : 환율
        """
        print "get_currency_exchange"

#         resData['currency'] = urllib2.urlopen("http://api.fixer.io/latest?base=USD").read()
        currency_base = 'USD'
        if TutorService.is_tutor(user_id):
            if Tutor.objects.get(user=user_id).currency is not None:
                currency_user = Tutor.objects.get(user=user_id).currency
            else:
                currency_user = 'USD'
        else:
            if Tutee.objects.get(user=user_id).currency is not None:
                currency_user = Tutee.objects.get(user=user_id).currency
            else:
                currency_user = 'USD'

        try:
            currency = urllib2.urlopen('http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s='+currency_base+currency_user+'=X').read()
        except urllib2.HTTPError, e:
            print "currency HTTPError : %d" % e.code
        except urllib2.URLError, e:
            print "currency URLError : %s" % e.reason.args[1]

#         resData['currency'] = urllib2.urlopen("http://rate-exchange.appspot.com/currency?from="+currency_in + "&to="+currency_out).read()
        currency = currency.split(',')
        print currency
        print currency[1], currency_user

        return currency[1], currency_user

    @staticmethod
    def save_user_language(profile, isTutor):
        """
        @summary: 튜터 언어변경
        @author: msjang
        @param profile: 프로필 정보
        @return: True, False( boolean )
        """

        print profile
        if isTutor:
            print "comes here for tutor"
            user_model = TutorDoc.objects.select_related('user').get(user_id=profile["user"])
            user_model.status = 2
            user_model.modify_type = 1
            user_model.lang_modify = "111"
        else:
            print "comes here for tutee"
            user_model = Tutee.objects.select_related('user').get(user_id=profile["user"])
        '''
        index = 1
        for native_id in profile["native"]:
            setattr(user_model, 'native' + str(index) + '_id', LanguageInfo.objects.get(id=native_id))
            setattr(user_model,'native' + str(index), LanguageInfo.objects.get(id=native_id).lang_english)
            index+=1
        '''

        if "native1_id" in profile:
            if profile["native1_id"] is not 0 and profile["native1_id"] is not None:
                user_model.native1_id = LanguageInfo.objects.get(id=profile["native1_id"])
                user_model.native1 = user_model.native1_id.lang_english
                print "Saved native language as %s", user_model.native1
            else:
                user_model.native1_id = None
                user_model.native1 = ''
        if "native2_id" in profile:
            if profile["native2_id"] is not 0 and profile["native2_id"] is not None:
                user_model.native2_id = LanguageInfo.objects.get(id=profile["native2_id"])
                user_model.native2 = user_model.native2_id.lang_english
            else:
                user_model.native2_id = None
                user_model.native2 = ''
        if "native3_id" in profile:
            if profile["native3_id"] is not 0 and profile["native3_id"] is not None:
                user_model.native3_id = LanguageInfo.objects.get(id=profile["native3_id"])
                user_model.native3 = user_model.native3_id.lang_english
            else:
                user_model.native3_id = None
                user_model.native3 = ''

        if "lang1_id" in profile:
            if profile["lang1_id"] is not 0 and profile["lang1_id"] is not None:
                user_model.lang1_id = LanguageInfo.objects.get(id=profile["lang1_id"])
                user_model.lang1 = user_model.lang1_id.lang_english
            else:
                user_model.lang1_id = None
                user_model.lang1_level_id = None
        if "lang1_level" in profile:
            if profile["lang1_level"] is not 0 and profile["lang1_level"] is not None:
                user_model.lang1_level_id = SkillLevelInfo.objects.get(id=profile["lang1_level"])
                if "lang1_learning" in profile:
                    if profile["lang1_learning"] is True:
                        user_model.lang1_learning = True
                    else:
                        user_model.lang1_learning = False
            else:
                user_model.lang1_id = None
                user_model.lang1_level_id = None
        if "lang2_id" in profile:
            if profile["lang2_id"] is not 0 and profile["lang2_id"] is not None:
                user_model.lang2_id = LanguageInfo.objects.get(id=profile["lang2_id"])
                user_model.lang2 = user_model.lang2_id.lang_english
            else:
                user_model.lang2_id = None
                user_model.lang2_level_id = None
        if "lang2_level" in profile:
            if profile["lang2_level"] is not 0 and profile["lang2_level"] is not None:
                user_model.lang2_level_id = SkillLevelInfo.objects.get(id=profile["lang2_level"])
                if "lang2_learning" in profile:
                    if profile["lang2_learning"] is True:
                        user_model.lang2_learning = True
                    else:
                        user_model.lang2_learning = False
            else:
                user_model.lang2_id = None
                user_model.lang2_level_id = None

        if "lang3_id" in profile:
            if profile["lang3_id"] is not 0 and profile["lang3_id"] is not None:
                user_model.lang3_id = LanguageInfo.objects.get(id=profile["lang3_id"])
                user_model.lang3 = user_model.lang3_id.lang_english
            else:
                user_model.lang3_id = None
                user_model.lang3_level_id = None
        if "lang3_level" in profile:
            if profile["lang3_level"] is not 0 and profile["lang3_level"] is not None:
                user_model.lang3_level_id = SkillLevelInfo.objects.get(id=profile["lang3_level"])
                if "lang3_learning" in profile:
                    if profile["lang3_learning"] is True:
                        user_model.lang3_learning = True
                    else:
                        user_model.lang3_learning = False
            else:
                user_model.lang3_id = None
                user_model.lang3_level_id = None

        if "lang4_id" in profile:
            if profile["lang4_id"] is not 0 and profile["lang4_id"] is not None:
                user_model.lang4_id = LanguageInfo.objects.get(id=profile["lang4_id"])
                user_model.lang4 = user_model.lang4_id.lang_english
            else:
                try:
                    user_model.lang4_id = None
                    user_model.lang4_level_id = None
                except:
                    print 'language data already None'
        if "lang4_level" in profile:
            if profile["lang4_level"] is not 0 and profile["lang4_level"] is not None:
                user_model.lang4_level_id = SkillLevelInfo.objects.get(id=profile["lang4_level"])
                if "lang4_learning" in profile:
                    if profile["lang4_learning"] is True:
                        user_model.lang4_learning = True
                    else:
                        user_model.lang4_learning = False
            else:

                user_model.lang4_id = None
                user_model.lang4_level_id = None

        if "lang5_id" in profile:
            if profile["lang5_id"] is not 0 and profile["lang5_id"] is not None:
                user_model.lang5_id = LanguageInfo.objects.get(id=profile["lang5_id"])
                user_model.lang5 = user_model.lang5_id.lang_english
            else:
                try:
                    user_model.lang5_id = None
                    user_model.lang5_level_id = None
                except:
                    print 'language data already None'
        if "lang5_level" in profile:
            if profile["lang5_level"] is not 0 and profile["lang5_level"] is not None:
                user_model.lang5_level_id = SkillLevelInfo.objects.get(id=profile["lang5_level"])
                if "lang5_learning" in profile:
                    if profile["lang5_learning"] is True:
                        user_model.lang5_learning = True
                    else:
                        user_model.lang5_learning = False
            else:
                try:
                    user_model.lang5_id = None
                    user_model.lang5_level_id = None
                except:
                    print 'language data already None'
        if "lang6_id" in profile:
            if profile["lang6_id"] is not 0 and profile["lang6_id"] is not None:
                user_model.lang6_id = LanguageInfo.objects.get(id=profile["lang6_id"])
                user_model.lang6 = user_model.lang6_id.lang_english
            else:
                try:
                    user_model.lang6_id = None
                    user_model.lang6_level_id = None
                except:
                    print 'language data already None'
        if "lang6_level" in profile:
            if profile["lang6_level"] is not 0 and profile["lang6_level"] is not None:
                user_model.lang6_level_id = SkillLevelInfo.objects.get(id=profile["lang6_level"])
                if "lang6_learning" in profile:
                    if profile["lang6_learning"] is True:
                        user_model.lang6_learning = True
                    else:
                        user_model.lang6_learning = False
            else:
                try:
                    user_model.lang6_id = None
                    user_model.lang6_level_id = None
                except:
                    print 'language data already None'

        user_model.save()

        return True

    @staticmethod
    def pending_resume(user_id, _profile):
        """
        @summary: profile resume pending
        @author: msjang
        @param _profile: resume 정보
        @return: True, False( boolean )
        """
        TutorApplyService.set_doc_profile(user_id, _profile)
        tutordoc = TutorDoc.objects.get(user_id = user_id)
        tutordoc.status = 2
        tutordoc.save()

        return True

    @staticmethod
    def save_user_intro(profile):
        """
        @summary: 튜터 정보pending
        @author: shkim
        @param _profile: 프로필 정보
        @return: True, False( boolean )
        """
        user_model = Tutee.objects.get(user=profile["user"])

        print "save_user_intro >>>>"
        print user_model.short_intro
        user_model.short_intro = profile["user_shortIntro"]
        user_model.long_intro = profile["user_longIntro"]
        user_model.save()

        return True

    @staticmethod
    def pending_tutor_intro(_profile):
        """
        @summary: 튜터 정보pending
        @author: msjang
        @param _profile: 프로필 정보
        @return: True, False( boolean )
        """
        try:
            tutorDoc = TutorDoc.objects.get(user_id=_profile["user"])
            tutorDoc.modify_type = 2
            tutorDoc.intro_modify = "111"
            tutorDoc.status = 2
        except:
            tutorDoc = TutorDoc()
            tutorDoc.user_id = _profile["user"]

        print "save_tutor_intro >>>>"
        print _profile
        tutorDoc.short_intro = _profile["short_intro"]
        tutorDoc.long_intro = _profile["long_intro"]
        tutorDoc.video_intro = _profile["video_intro"]
        tutorDoc.save()
        return True

    @staticmethod
    def save_tools(profile, istutor):
        """
        @summary: 메신저 정보 저장
        @author: msjang
        @param _profile: 정보 저장
        @return: True, False( boolean )
        """
        try:
            if istutor:
                user_obj = Tutor.objects.get(user_id= profile['user'])
            else:
                user_obj = Tutee.objects.get(user_id = profile['user'])
        except:
            pass

        print "save_tools"
        #tool.offline = profile["offline"]
        #tool.online = profile["online"]

        #         if _profile["online"] == '1':
        #             tool.audio = _profile["voice"]
        #         else:
        #             tool.audio = 0

        # 초기화
 
        user_obj.skype_id = None
        user_obj.hangout_id = None
        user_obj.facetime_id = None
        user_obj.qq_id = None


        # tool 정보 갱신
        for obj in profile["tools"]:
            if obj["name"] == "Skype":
                user_obj.skype_id = obj["account"]

            if obj["name"] == "Hangout":
                user_obj.hangout_id = obj["account"]

            if obj["name"] == "Facetime":
                user_obj.facetime_id = obj["account"]

            if obj["name"] == "QQ":
                user_obj.qq_id = obj["account"]

        user_obj.save()

        return True


    @staticmethod
    def get_skill_level_info():
        """
        @summary: skill 정보
        @author: msjang
        @param none
        @return: skill_level_list( list )
        """
        skill_level_list = []
        levels = SkillLevelInfo.objects.all()

        for level in levels:
            skill_level_obj = {}
            skill_level_obj["id"] = level.id
            skill_level_obj["name"] = level.level
            skill_level_list.append(skill_level_obj)
        return skill_level_list

    @staticmethod
    def get_currency_info():
        """
        @summary: currency 정보
        @author: msjang
        @param none
        @return: currency_list( list )
        """
        currency_list = []
        currency = CurrencyInfo.objects.all().order_by("code")

        for cur in currency:
            currency_obj = {}
            currency_obj["id"] = cur.id
            currency_obj["name"] = cur.code
            currency_obj["c_sign"] = cur.symbol
            currency_list.append(currency_obj)
        return currency_list
    
    
    @staticmethod
    def is_bookmark(tutor_id, user_id):
        """
        @summary: 접속자와 튜터간의 북마크 관계
        @param tutor_id: 튜터 id
        @param user_id: 사용자 id
        @return: True, False
        """
        return Bookmark.objects.filter(tutor_id=tutor_id, user_id=user_id).exists()

    @staticmethod
    def set_bookmark(tutor_id, user_id):
        """
        @summary: 튜터와 북마크 설정
        @param tutor_id: 튜터 id
        @param user_id: 유저 id
        @return: True, False
        """
        result = 1
        try:
            if not tutor_id == user_id:
                if not TutorService.is_bookmark(tutor_id, user_id):
                    Bookmark(
                        user_id=user_id,
                        tutor_id=tutor_id
                    ).save()
                else:
                    bookmark_obj = Bookmark.objects.get(user_id=user_id, tutor_id=tutor_id)
                    bookmark_obj.delete()
                    result = 0
        except Exception as err:
            print '@@@@@set_bookmark : ', str(err)
            result = None
        finally:
            return result

    @staticmethod
    def get_tutor_tools(user_id, user_dict=None):
        """
        @summary: 해당 튜터 tools 정보
        @param tutor_id: 튜터 id
        @param tutor_dict: 튜터 정보 객체
        @return: tools info dict
        """
        tools_dict = []
        print "get_tutor_tools"
        print user_dict
        try:
            if not user_dict:
                if TutorService.is_tutor(user_id):
                    tutor_obj = Tutor.objects.get(user=user_id)
                    user_dict = model_to_dict(tutor_obj)
#                     tutor_dict = TutorService.get_tutor_info(tutor_id)
                else:
                    tutee_obj = Tutee.objects.get(user=user_id)
                    user_dict = model_to_dict(tutee_obj)

            if user_dict['skype_id']:
                tools_dict.append({"skype": True, "account": user_dict['skype_id']})
            else:
                tools_dict.append({"skype": False, "account": None})

            if user_dict['facetime_id']:
                tools_dict.append({"facetime": True, "account": user_dict['facetime_id']})
            else:
                tools_dict.append({"facetime": False, "account": None})

            if user_dict['hangout_id']:
                tools_dict.append({"hangout": True, "account": user_dict['hangout_id']})
            else:
                tools_dict.append({"hangout": False, "account": None})

            if user_dict['qq_id']:
                tools_dict.append({"qq": True, "account": user_dict['qq_id']})
            else:
                tools_dict.append({"qq": False, "account": None})

        except Exception as err:
            print '@@@@@get_tutor_tools : ', str(err)
            tools_dict['err'] = str(err)
        finally:
            print "tools_dicttools_dict"
            print tools_dict
            return tools_dict

    @staticmethod
    def get_tutor_info(tutor_id):
        """
        @summary: 튜터 정보
        @param tutor_id: 튜터 id
        @return: obj
        """
        tutor_dict = dict()
        try:
            tutor_obj = Tutor.objects.get(user_id=tutor_id)
            tutor_dict = model_to_dict(tutor_obj)
            tutor_dict['photo_filename'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                          tutor_obj.photo_filename)

        except Exception as err:
            print '@@@@@tutor_info : ', str(err)
            tutor_dict = None
        finally:
            return tutor_dict

    @staticmethod
    def get_user_info(user_id, isTutor):
        """
        @summary: 튜터 정보
        @param tutor_id: 튜터 id
        @return: dictionary
        """
        user_dict = dict()
        # need to put in objects for foreign keys
        try:
            if isTutor:
                user_obj = Tutor.objects.get(user_id=user_id)
                tutor_doc_obj = TutorDoc.objects.get(user_id = user_id)
                tutor_doc_dict = model_to_dict(tutor_doc_obj)
                user_dict = model_to_dict(user_obj)
                user_dict["native1"] = tutor_doc_dict["native1"]
                user_dict["native2"] = tutor_doc_dict["native2"]
                user_dict["native3"] = tutor_doc_dict["native3"]
                user_dict["native1_id"] = tutor_doc_dict["native1_id"]
                user_dict["native2_id"] = tutor_doc_dict["native2_id"]
                user_dict["native3_id"] = tutor_doc_dict["native3_id"]
                user_dict["lang1"] = tutor_doc_dict["lang1"]
                user_dict["lang2"] = tutor_doc_dict["lang2"]
                user_dict["lang3"] = tutor_doc_dict["lang3"]
                user_dict["lang4"] = tutor_doc_dict["lang4"]
                user_dict["lang5"] = tutor_doc_dict["lang5"]
                user_dict["lang6"] = tutor_doc_dict["lang6"]
                user_dict["lang1_learning"] = tutor_doc_dict["lang1_learning"]
                user_dict["lang2_learning"] = tutor_doc_dict["lang2_learning"]
                user_dict["lang3_learning"] = tutor_doc_dict["lang3_learning"]
                user_dict["lang4_learning"] = tutor_doc_dict["lang4_learning"]
                user_dict["lang5_learning"] = tutor_doc_dict["lang5_learning"]
                user_dict["lang6_learning"] = tutor_doc_dict["lang6_learning"]
                user_dict["lang1_id"]=  tutor_doc_dict["lang1_id"]
                user_dict["lang2_id"]=  tutor_doc_dict["lang2_id"]
                user_dict["lang3_id"]=  tutor_doc_dict["lang3_id"]
                user_dict["lang4_id"]=  tutor_doc_dict["lang4_id"]
                user_dict["lang5_id"]=  tutor_doc_dict["lang5_id"]
                user_dict["lang6_id"]=  tutor_doc_dict["lang6_id"]
                user_dict["lang1_level"] = tutor_doc_dict["lang1_level"]
                user_dict["lang2_level"] = tutor_doc_dict["lang2_level"]
                user_dict["lang3_level"] = tutor_doc_dict["lang3_level"]
                user_dict["lang4_level"] = tutor_doc_dict["lang4_level"]
                user_dict["lang5_level"] = tutor_doc_dict["lang5_level"]
                user_dict["lang6_level"] = tutor_doc_dict["lang6_level"]
                user_dict["lang_modify"] = tutor_doc_dict["lang_modify"]
                user_dict["intro_modify"] = tutor_doc_dict["intro_modify"]
                user_dict["resume_modify"] = tutor_doc_dict["resume_modify"]
                if user_obj.photo_filename:
                    user_dict['photo_filepath'] = '%s' % (CommonService.PROFILE_IMAGE_FILE_DIR)
                    user_dict['photo_filename'] = '%s' % (user_obj.photo_filename)
                user_dict['profile_info'] = TutorApplyService.get_doc_profile(user_id)
                birthDate = {}
                if user_dict["birthdate"] == "" or user_dict["birthdate"] is None:
                    birthDate["year"] = None
                    birthDate["month"] = None
                    birthDate["day"] = None
                else:
                    birthDate["year"] = int(str(user_dict["birthdate"])[0:4])
                    birthDate["month"] = int(str(user_dict["birthdate"])[5:7])
                    birthDate["day"] = int(str(user_dict["birthdate"])[8:10])
                del user_dict["birthdate"]
                user_dict["birthdate"] = birthDate
                user_dict["server_year"] = datetime.today().year
                user_dict["tools"] = TutorService.get_tutor_tools(user_id, None)
            else:
                user_obj = Tutee.objects.get(user_id=user_id)
                user_dict = model_to_dict(user_obj)
                if user_obj.photo_filename:
                    user_dict['photo_filepath'] = '%s' % (CommonService.PROFILE_IMAGE_FILE_DIR)
                    user_dict['photo_filename'] = '%s' % (user_obj.photo_filename)

                birthDate = {}
                if user_dict["birthdate"] == "" or user_dict["birthdate"] is None:
                    birthDate["year"] = None
                    birthDate["month"] = None
                    birthDate["day"] = None
                else:
                    birthDate["year"] = int(str(user_dict["birthdate"])[0:4])
                    birthDate["month"] = int(str(user_dict["birthdate"])[5:7])
                    birthDate["day"] = int(str(user_dict["birthdate"])[8:10])
                del user_dict["birthdate"]
                user_dict["birthdate"] = birthDate
                user_dict["server_year"] = datetime.today().year
                user_dict["tools"] = TutorService.get_tutor_tools(user_id, None)
        except Exception as err:
            print '@@@@@get_user_info : ', str(err)
            print '@@@@@get_user_info :', str(err)
            #tutor_dict = None
            #tutee_dict = None
        finally:
            return user_dict

    @staticmethod
    def set_tutor_info(tutor, user_id, login_list, bookmark_list):
        """
        @summary: Tutor객체 필요한 dict형태로 변환
        @param tutor: tutor 객체
        @param user_id: 로그인 유저 id
        @param login_list: 현재 접속중인 유저 리스트
        @param bookmark_list: 접속중인 유저의 bookmark 리스트
        @return: dict
        """
        tutor_dict = dict()
        try:
            tutor_dict = model_to_dict(tutor,
                                       exclude=['teaching1_id', 'teaching1',
                                                'teaching2_id', 'teaching2',
                                                'teaching3_id', 'teaching3',
                                                'lang1_id', 'lang1', 'lang1_level', 'lang1_learning',
                                                'lang2_id', 'lang2', 'lang2_level', 'lang2_learning',
                                                'lang3_id', 'lang3', 'lang3_level', 'lang3_learning',
                                                'lang4_id', 'lang4', 'lang4_level', 'lang4_learning',
                                                'lang5_id', 'lang5', 'lang5_level', 'lang5_learning',
                                                'lang6_id', 'lang6', 'lang6_level', 'lang6_learning'])
            tutor_dict['lesson_max_price'] = int(tutor.max_price or 0)
            tutor_dict['lesson_min_price'] = int(tutor.min_price or 0)
            tutor_dict['country_filename'] = '%s/%s' % (CommonService.NATION_FILE_DIR,
                                                        tutor.from_country_id.flag_filename)
            tutor_dict['photo_filename'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                      tutor.photo_filename)
            tutor_dict['is_login'] = tutor.user.email in login_list
            tutor_dict['teaches'] = TutorService.teach_list(tutor)
            tutor_dict['speaks'] = TutorService.speak_list(tutor)
            tutor_dict['learns'] = TutorService.learn_list(tutor)
            tutor_dict['ratings'] = 5
            if user_id:
                tutor_dict['is_bookmark'] = tutor.user_id in bookmark_list
        except Exception as err:
            print '@@@@@set_tutor_info : ', str(err)
        finally:
            return tutor_dict

    @staticmethod
    def set_user_info(user, user_id, login_list, friend_list):
        """
        @summary: User 객체 필요한 dict 형태로 변환
        @param user: 유저 정보
        @param user_id: 접속 유저 id
        @param login_list: 접속 유저 리스트
        @param friend_list: 친구 리스트
        @return: dict
        """
        user_dict = {}
        try:
            user_dict = model_to_dict(user,
                                      exclude=['lang1_id', 'lang1', 'lang1_level', 'lang1_learning',
                                               'lang2_id', 'lang2', 'lang2_level', 'lang2_learning',
                                               'lang3_id', 'lang3', 'lang3_level', 'lang3_learning',
                                               'lang4_id', 'lang4', 'lang4_level', 'lang4_learning',
                                               'lang5_id', 'lang5', 'lang5_level', 'lang5_learning',
                                               'lang6_id', 'lang6', 'lang6_level', 'lang6_learning'])
            if getattr(user, 'from_country_id'):
                user_dict['country_filename'] = '%s/%s' % (CommonService.NATION_FILE_DIR,
                                                           user.from_country_id.flag_filename)
            else:
                user_dict['country_filename'] = None

            if user.user.is_tutor:
                user_dict['teaches'] = TutorService.teach_list(getattr(user.user, 'tutor'))
            user_dict['is_tutor'] = user.user.is_tutor
            user_dict['photo_filename'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                     user.photo_filename)
            user_dict['is_login'] = user.user.email in login_list
            user_dict['speaks'] = TutorService.speak_list(user)
            user_dict['learns'] = TutorService.learn_list(user)

            if user_id:
                user_dict['is_friend'] = user.user_id in friend_list
        except Exception as err:
            print '@@@@@set_user_info : ', str(err)
            user_dict = {}
        finally:
            return user_dict

    @staticmethod
    def set_resume_info(tutor_obj):
        """
        @summary: 튜터 resume 리스트
        @param tutor_obj: 튜터 정보
        @return: resume list
        """
        resume_dict = {}
        try:
            resume_dict['work_exp'] = []
            resume_dict['education'] = []
            resume_dict['certification'] = []
            for i in range(1, 4):
                if getattr(tutor_obj, 'work' + str(i)):
                    work_obj = getattr(tutor_obj, 'work' + str(i))
                    work_dict = model_to_dict(work_obj)
                    work_dict['country'] = LanguageInfo.objects.get(id=work_obj.country).lang_english
                    resume_dict['work_exp'].append(work_dict)
                if getattr(tutor_obj, 'education' + str(i)):
                    edu_obj = getattr(tutor_obj, 'education' + str(i))
                    edu_dict = model_to_dict(edu_obj)
                    edu_dict['location'] = LanguageInfo.objects.get(id=edu_obj.location).lang_english
                    resume_dict['education'].append(edu_dict)
                if getattr(tutor_obj, 'certificate' + str(i)):
                    resume_dict['certification'].append(
                        model_to_dict(
                            getattr(tutor_obj, 'certificate' + str(i))
                        )
                    )
        except Exception as err:
            print '@@@@@set_resume_info : ', str(err)
            resume_dict = {}
        finally:
            return resume_dict

    @staticmethod
    def set_statistics_info(tutor_obj):
        """
        @summary: 튜터 statistics 정보
        @param tutor_obj: 튜터 객체
        @return: statistics 정보
        """
        statistics_dict = {
            'AR': {
                'all_time': None,
                'last_month': None
            },
            'attendance': {
                'all_time': None,
                'last_month': None
            },
            'confirmed_lesson': {
                'all_time': None,
                'last_month': None
            },
            'disputes': {
                'all_time': None,
                'last_month': None
            }
        }
        try:
            one_month_ago = timezone.now() - relativedelta(months=1)
            all_time_rating = tutor_obj.lessonfeedback_set.aggregate(Avg('ratings'))
            last_month_rating = tutor_obj.lessonfeedback_set.filter(
                created_time__gte=one_month_ago
            ).aggregate(Avg('ratings'))
            all_time_confirm = tutor_obj.lessonreservation_set.filter(lesson_status=8).count()
            all_time_absent = LessonClaim.objects.filter(reportee_id=tutor_obj.user_id, claim_text_id=1).count()
            last_month_confirm = tutor_obj.lessonreservation_set.filter(lesson_status=8,
                                                                        updated_time__gte=one_month_ago).count()
            last_month_absent = LessonClaim.objects.filter(reportee_id=tutor_obj.user_id,
                                                           claim_text_id=1,
                                                           updated_time__gte=one_month_ago).count()
            statistics_dict['AR']['all_time'] = all_time_rating['ratings__avg']
            statistics_dict['AR']['last_month'] = last_month_rating['ratings__avg']
            try:
                statistics_dict['attendance']['all_time'] = (all_time_absent/all_time_confirm) * 100
                statistics_dict['attendance']['last_month'] = (last_month_absent/last_month_confirm) * 100
            except ZeroDivisionError:
                statistics_dict['attendance']['all_time'] = 0
                statistics_dict['attendance']['last_month'] = 0
            statistics_dict['confirmed_lesson']['all_time'] = all_time_confirm
            statistics_dict['confirmed_lesson']['last_month'] = last_month_confirm
            statistics_dict['disputes']['all_time'] = TutorViolation.objects.filter(
                reportee_id=tutor_obj.user_id
            ).count()
            statistics_dict['disputes']['last_month'] = TutorViolation.objects.filter(
                reportee_id=tutor_obj.user_id,
                created_time__gte=one_month_ago
            ).count()
        except Exception as err:
            print '@@@@@set_statistics_info : ', str(err)
            statistics_dict['err'] = str(err)
        finally:
            return statistics_dict

    @staticmethod
    def set_feedback_info(tutor_id, cur_page=1, user_utc=0):
        """
        @summary: 튜터 feedback 리스트
        @param tutor_id: 튜터 id
        @param cur_page: 현재 페이지
        @param user_utc: 접속 유저 utc 차이
        @return: feedback 리스트
        """
        feedback_list = []
        result = {}
        try:
            queryset = LessonFeedback.objects.raw('''
                SELECT lf1.*, tt.name, CONCAT(%s, tt.photo_filename) AS photo
                FROM lesson_feedback lf1
                JOIN (
                  SELECT tutee_id, MAX(created_time) AS created
                  FROM lesson_feedback
                  WHERE tutor_id = %s
                  GROUP BY tutee_id
                ) lf2
                ON lf1.tutee_id = lf2.tutee_id
                AND lf1.created_time = lf2.created
                JOIN tutee tt
                ON lf1.tutee_id = tt.user_id
                ORDER BY lf1.created_time DESC
            ''', [CommonService.PROFILE_IMAGE_FILE_DIR, tutor_id])

            p = Paginator(queryset, 5)
            p._count = len(list(queryset))
            page_item = p.page(cur_page)

            for i in page_item.object_list:
                feedback_dict = model_to_dict(i)
                feedback_dict['created_time'] = i.created_time + timedelta(hourse=user_utc)
                feedback_dict['name'] = i.name
                feedback_dict['photo'] = i.photo
                feedback_list.append(feedback_dict)

            result['entries'] = feedback_list
            result['pgGroup'] = CommonService.paging_list(p.page_range, cur_page, 5)
            result['page'] = cur_page
            result['has_next'] = page_item.has_next()
            result['has_prev'] = page_item.has_previous()

        except Exception as err:
            print '@@@@@set_feedback_info : ', str(err)
            result = None
        finally:
            return result

    @staticmethod
    def more_feedback(feedback_id, tutor_id, tutee_id, user_utc=0):
        """
        @summary: 유저가 남긴 feedback 전부 가져오는 함수
        @param feedback_id: 제외할 feedback id
        @param tutor_id: 튜터 id
        @param tutee_id: 튜티 id
        @param user_utc: utc와 접속유저 현지시간 차이
        @return: feedback list
        """
        feedback_list = []
        try:
            queryset = LessonFeedback.objects.select_related('tutee')\
                .filter(tutor_id=tutor_id, tutee_id=tutee_id)\
                .exclude(id=feedback_id)\
                .order_by('-created_time')

            for feedback in queryset:
                feedback_dict = model_to_dict(feedback,
                                              exclude=[
                                                  'tutor_feedback',
                                                  'lesson', 'course'
                                              ])
                feedback_dict['name'] = feedback.tutee.name
                feedback_dict['photo'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                    feedback.tutee.photo_filename)
                feedback_dict['created_time'] = feedback.created_time + timedelta(hours=user_utc)
        except Exception as err:
            print '@@@@@more_feedback : ', str(err)
            feedback_list = []
        finally:
            return feedback_list

    @staticmethod
    def get_tutor_detail(tutor_id, user_id, user_utc):
        """
        @summary: 튜터 상세 정보
        @param tutor_id: 튜터 id
        @param user_id: 접속 유저 id
        @param user_utc: 접속 유저 utc
        @return: obj
        """
        tutor_dict = dict()
        try:
            login_list = CommonService.get_login_users()
            bookmark_list = TutorService.bookmark_list(user_id)
            tutor_obj = Tutor.objects.select_related(
                'from_country_id', 'from_city_id', 'livingin_country_id', 'livingin_city_id',
                'timezone_id', 'currency_id',
                'native1_id', 'native2_id', 'native3_id',
                'lang1_id', 'lang2_id', 'lang3_id', 'lang4_id', 'lang5_id', 'lang6_id',
                'teaching1_id', 'teaching2_id', 'teaching3_id',
                'work1', 'work2', 'work3',
                'education1', 'education2', 'education3',
                'certificate1', 'certificate2', 'certificate3',
                'paymentinfo'
            ).extra(
                select={
                    'max_price': """
                        SELECT
                            MAX(CASE
                                WHEN sg_30min_price != 0
                                     AND sg_30min_price * 2 >= sg_60min_price
                                     AND sg_30min_price * 2 >= ( sg_90min_price*2 ) / 3
                                     AND sg_30min_price * 2 >= pkg_30min_price * 2
                                     AND sg_30min_price * 2 >= pkg_60min_price
                                     AND sg_30min_price * 2 >= pkg_90min_price*2 / 3
                                THEN sg_30min_price * 2
                                WHEN sg_60min_price != 0
                                     AND sg_60min_price >= sg_30min_price * 2
                                     AND sg_60min_price >= ( sg_90min_price*2 ) / 3
                                     AND sg_60min_price >= pkg_30min_price*2
                                     AND sg_60min_price >= pkg_60min_price
                                     AND sg_60min_price >= pkg_90min_price*2/3
                                THEN sg_60min_price
                                WHEN sg_90min_price != 0
                                     AND sg_90min_price*2 / 3 >= sg_30min_price * 2
                                     AND sg_90min_price*2 / 3 >= sg_60min_price
                                     AND sg_90min_price*2 / 3 >= pkg_30min_price * 2
                                     AND sg_90min_price*2 / 3 >= pkg_60min_price
                                     AND sg_90min_price*2 / 3 >= pkg_90min_price*2 / 3
                                THEN ROUND(sg_90min_price*2/3)
                                WHEN pkg_30min_price != 0
                                     AND pkg_30min_price * 2 >= sg_30min_price * 2
                                     AND pkg_30min_price * 2 >= sg_60min_price
                                     AND pkg_30min_price * 2 >= sg_90min_price*2 / 3
                                     AND pkg_30min_price * 2 >= pkg_60min_price
                                     AND pkg_30min_price * 2 >= pkg_90min_price*2 / 3
                                THEN pkg_30min_price * 2
                                WHEN pkg_60min_price != 0
                                     AND pkg_60min_price >= sg_30min_price * 2
                                     AND pkg_60min_price >= sg_60min_price
                                     AND pkg_60min_price >= sg_90min_price*2 / 3
                                     AND pkg_60min_price >= pkg_30min_price * 2
                                     AND pkg_60min_price >= pkg_90min_price*2 / 3
                                THEN pkg_60min_price
                                WHEN pkg_90min_price != 0
                                     AND pkg_90min_price*2 / 3 >= sg_30min_price * 2
                                     AND pkg_90min_price*2 / 3 >= sg_60min_price
                                     AND pkg_90min_price*2 / 3 >= sg_90min_price*2 / 3
                                     AND pkg_90min_price*2 / 3 >= pkg_30min_price * 2
                                     AND pkg_90min_price*2 / 3 >= pkg_60min_price
                                THEN ROUND(pkg_90min_price*2 / 3)
                                ELSE 0
                                END)
                        FROM tutor_course
                        WHERE tutor_course.tutor_id=tutor.user_id
                    """,
                    'min_price': """
                        SELECT
                            MIN(CASE
                                WHEN sg_30min_price != 0
                                     AND IFNULL(sg_30min_price * 2 <= NULLIF(sg_60min_price, 0), TRUE)
                                     AND IFNULL(sg_30min_price * 2 <= NULLIF(sg_90min_price*2 / 3, 0), TRUE)
                                     AND IFNULL(sg_30min_price * 2 <= NULLIF(pkg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(sg_30min_price * 2 <= NULLIF(pkg_60min_price, 0), TRUE)
                                     AND IFNULL(sg_30min_price * 2 <= NULLIF(pkg_90min_price*2 / 3, 0), TRUE)
                                THEN sg_30min_price * 2
                                WHEN sg_60min_price != 0
                                     AND IFNULL(sg_60min_price <= NULLIF(sg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(sg_60min_price <= NULLIF(sg_90min_price*2 / 3, 0), TRUE)
                                     AND IFNULL(sg_60min_price <= NULLIF(pkg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(sg_60min_price <= NULLIF(pkg_60min_price, 0), TRUE)
                                     AND IFNULL(sg_60min_price <= NULLIF(pkg_90min_price*2/3, 0), TRUE)
                                THEN sg_60min_price
                                WHEN sg_90min_price != 0
                                     AND IFNULL(sg_90min_price*2 / 3 <= NULLIF(sg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(sg_90min_price*2 / 3 <= NULLIF(sg_60min_price, 0), TRUE)
                                     AND IFNULL(sg_90min_price*2 / 3 <= NULLIF(pkg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(sg_90min_price*2 / 3 <= NULLIF(pkg_60min_price, 0), TRUE)
                                     AND IFNULL(sg_90min_price*2 / 3 <= NULLIF(pkg_90min_price*2 / 3, 0), TRUE)
                                THEN ROUND(sg_90min_price*2/3)
                                WHEN pkg_30min_price != 0
                                     AND IFNULL(pkg_30min_price * 2 <= NULLIF(sg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(pkg_30min_price * 2 <= NULLIF(sg_60min_price, 0), TRUE)
                                     AND IFNULL(pkg_30min_price * 2 <= NULLIF(sg_90min_price*2 / 3, 0), TRUE)
                                     AND IFNULL(pkg_30min_price * 2 <= NULLIF(pkg_60min_price, 0), TRUE)
                                     AND IFNULL(pkg_30min_price * 2 <= NULLIF(pkg_90min_price*2 / 3, 0), TRUE)
                                THEN pkg_30min_price * 2
                                WHEN pkg_60min_price != 0
                                     AND IFNULL(pkg_60min_price <= NULLIF(sg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(pkg_60min_price <= NULLIF(sg_60min_price, 0), TRUE)
                                     AND IFNULL(pkg_60min_price <= NULLIF(sg_90min_price*2 / 3, 0), TRUE)
                                     AND IFNULL(pkg_60min_price <= NULLIF(pkg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(pkg_60min_price <= NULLIF(pkg_90min_price*2 / 3, 0), TRUE)
                                THEN pkg_60min_price
                                WHEN pkg_90min_price != 0
                                     AND IFNULL(pkg_90min_price*2 / 3 <= NULLIF(sg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(pkg_90min_price*2 / 3 <= NULLIF(sg_60min_price, 0), TRUE)
                                     AND IFNULL(pkg_90min_price*2 / 3 <= NULLIF(sg_90min_price*2 / 3, 0), TRUE)
                                     AND IFNULL(pkg_90min_price*2 / 3 <= NULLIF(pkg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(pkg_90min_price*2 / 3 <= NULLIF(pkg_60min_price, 0), TRUE)
                                THEN ROUND(pkg_90min_price*2 / 3)
                                ELSE 0
                                END)
                        FROM tutor_course
                        WHERE tutor_course.tutor_id=tutor.user_id
                    """
                }
            ).get(
                user_id=tutor_id
            )
            lesson_count = tutor_obj.lessonreservation_set.filter(lesson_status=8).count()
            tutee_count_list = tutor_obj.lessonreservation_set.values('tutee').annotate(Count('tutee'))
            if tutee_count_list:
                tutee_count = tutee_count_list[0]['tutee__count']
            else:
                tutee_count = 0
            tutor_dict['profile'] = TutorService.set_tutor_info(tutor_obj, user_id, login_list, bookmark_list)
            tutor_dict['profile']['lessons'] = lesson_count
            tutor_dict['profile']['tutees'] = tutee_count
            tutor_dict['resume_list'] = TutorService.set_resume_info(tutor_obj)
            tutor_dict['statistics'] = TutorService.set_statistics_info(tutor_obj)
            tutor_dict['feedback'] = TutorService.set_feedback_info(tutor_obj.user_id, 1, user_utc)
        except Exception as err:
            print '@@@@@get_tutor_detail : ', str(err)
            tutor_dict['err'] = str(err)
        finally:
            return tutor_dict

    @staticmethod
    def get_search_data():
        """
        @summary: 튜터 목록 검색 기본 data
        @return: list
        """
        obj = dict()
        try:
            obj['language_list'] = CommonService.language_list()
            obj['country_list'] = CommonService.country_list()
            obj['tool_list'] = CommonService.tool_list()
        except Exception as err:
            print str(err)
            obj['message'] = str(err)
        finally:
            return obj

    @staticmethod
    def get_user_detail(user_id, user_utc):
        """
        @summary: exchange 유저 상세 정보
        @param user_id: exchange list 에서 선택한 유저 id
        @param user_utc: 접속 유저 utc
        @return: obj
        """
        user_dict = dict()
        try:
            login_list = CommonService.get_login_users()
            bookmark_list = TutorService.bookmark_list(user_id)
            if TutorService.is_tutor(user_id):
                user_obj = Tutor.objects.select_related(
                    'from_country_id', 'from_city_id', 'livingin_country_id', 'livingin_city_id',
                    'timezone_id', 'currency_id',
                    'native1_id', 'native2_id', 'native3_id',
                    'lang1_id', 'lang2_id', 'lang3_id', 'lang4_id', 'lang5_id', 'lang6_id',
                    'teaching1_id', 'teaching2_id', 'teaching3_id'
                ).get(user_id=user_id)
                user_intro = Tutee.objects.get(user=user_id)
                user_obj.short_intro = user_intro.short_intro
                user_obj.long_intro = user_intro.long_intro
            else:
                user_obj = Tutee.objects.select_related(
                    'from_country_id', 'from_city_id', 'livingin_country_id', 'livingin_city_id',
                    'timezone_id', 'currency_id',
                    'native1_id', 'native2_id', 'native3_id',
                    'lang1_id', 'lang2_id', 'lang3_id', 'lang4_id', 'lang5_id', 'lang6_id'
                ).get(user_id=user_id)
            user_dict['profile'] = TutorService.set_user_info(user_obj, user_id, login_list, bookmark_list)
            user_dict['statistics'] = TutorService.set_statistics_info(user_obj)
            user_dict['feedback'] = TutorService.set_feedback_info(user_obj.user_id, 1, user_utc)
        except Exception as err:
            print '@@@@@get_tutor_detail : ', str(err)
            user_dict['err'] = str(err)
        finally:
            return user_dict

    @staticmethod
    def filter_schedule(utc_time, data):
        """
        @summary: 튜터 조건 검색 중 스케줄 관련 검색
        @param utc_time: 사용자 utc_time
        @param data: 스케줄 조건
        @return: tutor id
        """
        tutor_id_list = list()
        try:
            utc_time_step = int(utc_time * -2)
            day_list = list()  # 요일 정보 리스트
            filter_schedule = list()  # 최종 필터 리스트
            q_list = list()
            q_list.append(Q(time0000=True)), q_list.append(Q(time0030=True))
            q_list.append(Q(time0100=True)), q_list.append(Q(time0130=True))
            q_list.append(Q(time0200=True)), q_list.append(Q(time0230=True))
            q_list.append(Q(time0300=True)), q_list.append(Q(time0330=True))
            q_list.append(Q(time0400=True)), q_list.append(Q(time0430=True))
            q_list.append(Q(time0500=True)), q_list.append(Q(time0530=True))
            q_list.append(Q(time0600=True)), q_list.append(Q(time0630=True))
            q_list.append(Q(time0700=True)), q_list.append(Q(time0730=True))
            q_list.append(Q(time0800=True)), q_list.append(Q(time0830=True))
            q_list.append(Q(time0900=True)), q_list.append(Q(time0930=True))
            q_list.append(Q(time1000=True)), q_list.append(Q(time1030=True))
            q_list.append(Q(time1100=True)), q_list.append(Q(time1130=True))
            q_list.append(Q(time1200=True)), q_list.append(Q(time1230=True))
            q_list.append(Q(time1300=True)), q_list.append(Q(time1330=True))
            q_list.append(Q(time1400=True)), q_list.append(Q(time1430=True))
            q_list.append(Q(time1500=True)), q_list.append(Q(time1530=True))
            q_list.append(Q(time1600=True)), q_list.append(Q(time1630=True))
            q_list.append(Q(time1700=True)), q_list.append(Q(time1730=True))
            q_list.append(Q(time1800=True)), q_list.append(Q(time1830=True))
            q_list.append(Q(time1900=True)), q_list.append(Q(time1930=True))
            q_list.append(Q(time2000=True)), q_list.append(Q(time2030=True))
            q_list.append(Q(time2100=True)), q_list.append(Q(time2130=True))
            q_list.append(Q(time2200=True)), q_list.append(Q(time2230=True))
            q_list.append(Q(time2300=True)), q_list.append(Q(time2330=True))

            schedule_start = 0
            schedule_end = len(q_list)

            if not data['day_mon']\
                    and not data['day_tue']\
                    and not data['day_wed']\
                    and not data['day_thu']\
                    and not data['day_fri']\
                    and not data['day_sat']\
                    and not data['day_sun']:
                day_list = range(1, 8)
            else:
                if data['day_mon']:
                    day_list.append(1)
                if data['day_tue']:
                    day_list.append(2)
                if data['day_wed']:
                    day_list.append(3)
                if data['day_thu']:
                    day_list.append(4)
                if data['day_fri']:
                    day_list.append(5)
                if data['day_sat']:
                    day_list.append(6)
                if data['day_sun']:
                    day_list.append(7)

            if data['time0']:
                schedule_start = 0
                schedule_end = 12
            if data['time6']:
                schedule_start = 12
                schedule_end = 24
                if not data['time0']:
                    schedule_start = 0
            if data['time12']:
                schedule_start = 24
                schedule_end = 36
                if not data['time6']:
                    schedule_start = 12
                if not data['time0']:
                    schedule_start = 0
            if data['time18']:
                schedule_start = 36
                schedule_end = 48
                if not data['time12']:
                    schedule_start = 24
                if not data['time6']:
                    schedule_start = 12
                if not data['time0']:
                    schedule_start = 0

            schedule_range = range(schedule_start + utc_time_step, schedule_end + utc_time_step)
            schedule_min = min(schedule_range)
            schedule_max = max(schedule_range)

            # 날짜범위가 전날부터 시작 일 때
            if schedule_min < 0 and 0 in schedule_range:
                filter_time = list()
                for i in schedule_range[:schedule_range.index(0)]:
                    filter_time.append(q_list[i])
                for i in day_list:
                    day_index = i - 1
                    if day_index == 0:
                        day_index = 7
                    filter_schedule.append(Q(reduce(operator.or_, filter_time), day_of_week=day_index))
                filter_time = list()
                for i in schedule_range[schedule_range.index(0):]:
                    filter_time.append(q_list[i])
                for i in day_list:
                    day_index = i
                    filter_schedule.append(Q(reduce(operator.or_, filter_time), day_of_week=day_index))

            elif schedule_min < 0 and schedule_max < 0:
                filter_time = list()
                for i in schedule_range:
                    filter_time.append(q_list[i])
                for i in day_list:
                    day_index = i - 1
                    if day_index == 0:
                        day_index = 7
                    filter_schedule.append(Q(reduce(operator.or_, filter_time), day_of_week=day_index))
            # 날짜 범위가 당일을 넘어설 때
            elif schedule_max > 48 and 48 in schedule_range:
                filter_time = list()
                for i in schedule_range[:schedule_range.index(48)]:
                    filter_time.append(q_list[i])
                for i in day_list:
                    day_index = i
                    filter_schedule.append(Q(reduce(operator.or_, filter_time), day_of_week=day_index))
                filter_time = list()
                for i in schedule_range[schedule_range.index(48):]:
                    filter_time.append(q_list[i % 48])
                for i in day_list:
                    day_index = i + 1
                    if day_index == 8:
                        day_index = 1
                    filter_schedule.append(Q(reduce(operator.or_, filter_time), day_of_week=day_index))
            else:
                filter_time = list()
                for i in schedule_range:
                    filter_time.append(q_list[i])
                for i in day_list:
                    day_index = i
                    filter_schedule.append(Q(reduce(operator.or_, filter_time), day_of_week=day_index))
            queryset = TutorScheduleWeek.objects.filter(
                reduce(operator.or_, filter_schedule)
            )
            tutor_id_list = list(queryset.values_list('tutor_id', flat=True).distinct())
        except Exception as err:
            print "@@@@@filter_schulde : ", str(err)
            tutor_id_list = None
        finally:
            return tutor_id_list

    @staticmethod
    def filter_listup_tutor(utc_time, data=None, list_type='tutor'):
        """
        @summary: 검색 조건에 해당하는 튜터
        @param utc_time: 유저 utc타임
        @param data: 검색 조건
        @param list_type: 튜터 리스트, 유저 리스트 구분
        @return: 튜터 id 리스트
        """
        tutor_id_list = list()
        try:
            if data['type'] == 'name':
                if list_type == 'tutor':
                    queryset = Tutor.objects.filter(name__icontains=data['name'],
                                                    is_available=1,
                                                    has_course=1,
                                                    has_schedule=1,
                                                    del_by_user=False,
                                                    del_by_admin=False
                                                    ).values_list('user_id', flat=True)
                else:
                    queryset = Tutee.objects.filter(name__icontains=data['name'],
                                                    del_by_user=False,
                                                    del_by_admin=False,
                                                    is_visible=True
                                                    )\
                        .values_list('user_id', flat=True)
                tutor_id_list = list(queryset)
            else:
                if list_type == 'tutor':
                    queryset = Tutor.objects.filter(is_available=1,
                                                    has_course=1,
                                                    has_schedule=1)
                else:
                    queryset = Tutee.objects.filter(del_by_user=False,
                                                    del_by_admin=False,
                                                    is_visible=True)
                if data['online']:
                    online_list = CommonService.get_login_users()
                    queryset = queryset.filter(
                        user__email__in=online_list
                    )
                if data['nation_from'] and data['nation_from'] != 'all':
                    queryset = queryset.filter(from_country_id=data['nation_from'])
                # if not data['city_from'] and data['city_from'] != 'all':
                #     queryset = queryset.filter(from_city_id=data['city_from'])
                if data['livein'] and data['livein'] != 'all':
                    queryset = queryset.filter(livingin_country_id=data['livein'])
                if data['liveincity'] and data['liveincity'] != 'all':
                    queryset = queryset.filter(livingin_city_id=data['liveincity'])
                if 'trial' in data and data['trial']:
                    queryset = queryset.filter(has_trial=1)
                if data['speaks'] and data['speaks'] != 'all':
                    queryset = queryset.filter(
                        Q(lang1_id_id=data['speaks']) | Q(lang2_id_id=data['speaks']) |
                        Q(lang3_id_id=data['speaks']) | Q(lang4_id_id=data['speaks']) |
                        Q(lang5_id_id=data['speaks']) | Q(lang6_id_id=data['speaks'])
                    )
                tutor_id_list = list(queryset.values_list('user_id', flat=True).distinct())
                if 'teaches' in data and data['teaches'] and data['teaches'] != 'all':
                    queryset = TutorCourse.objects.filter(course_lang_id=data['teaches'])
                    tutor_id_list = list(set(tutor_id_list) &
                                         set(list(queryset.values_list('tutor_id', flat=True).distinct())))
                if 'avail' in data and data['avail']:
                    schedule_id_list = TutorService.filter_schedule(utc_time, data['avail'][0])
                    tutor_id_list = list(set(tutor_id_list) & set(schedule_id_list))
                if 'hour' in data and data['hour'] and data['hour'] != 'all':
                    if data['hour'] == '1':
                        queryset = TutorCourse.objects.filter(
                            Q(sg_30min_price__gt=0, sg_30min_price__lte=50) |
                            Q(sg_60min_price__gt=0, sg_60min_price__lte=100) |
                            Q(sg_90min_price__gt=0, sg_90min_price__lte=150) |
                            Q(pkg_30min_price__gt=0, pkg_30min_price__lte=50) |
                            Q(pkg_60min_price__gt=0, pkg_60min_price__lte=100) |
                            Q(pkg_90min_price__gt=0, pkg_90min_price__lte=150)
                        )
                    elif data['hour'] == '101':
                        queryset = TutorCourse.objects.filter(
                            Q(sg_30min_price__gte=51, sg_30min_price__lte=75) |
                            Q(sg_60min_price__gte=101, sg_60min_price__lte=150) |
                            Q(sg_90min_price__gte=151, sg_90min_price__lte=225) |
                            Q(pkg_30min_price__gte=51, pkg_30min_price__lte=75) |
                            Q(pkg_60min_price__gte=101, pkg_60min_price__lte=150) |
                            Q(pkg_90min_price__gte=151, pkg_90min_price__lte=225)
                        )
                    elif data['hour'] == '151':
                        queryset = TutorCourse.objects.filter(
                            Q(sg_30min_price__gte=76, sg_30min_price__lte=100) |
                            Q(sg_60min_price__gte=151, sg_60min_price__lte=200) |
                            Q(sg_90min_price__gte=226, sg_90min_price__lte=300) |
                            Q(pkg_30min_price__gte=76, pkg_30min_price__lte=100) |
                            Q(pkg_60min_price__gte=151, pkg_60min_price__lte=200) |
                            Q(pkg_90min_price__gte=226, pkg_90min_price__lte=300)
                        )
                    elif data['hour'] == '201':
                        queryset = TutorCourse.objects.filter(
                            Q(sg_30min_price__gte=101, sg_30min_price__lte=125) |
                            Q(sg_60min_price__gte=201, sg_60min_price__lte=250) |
                            Q(sg_90min_price__gte=301, sg_30min_price__lte=375) |
                            Q(pkg_30min_price__gte=101, pkg_30min_price__lte=125) |
                            Q(pkg_60min_price__gte=201, pkg_60min_price__lte=250) |
                            Q(pkg_90min_price__gte=301, pkg_30min_price__lte=375)
                        )
                    else:
                        queryset = TutorCourse.objects.filter(
                            Q(sg_30min_price__gte=126) |
                            Q(sg_60min_price__gte=251) |
                            Q(sg_90min_price__gte=376) |
                            Q(pkg_30min_price__gte=126) |
                            Q(pkg_60min_price__gte=251) |
                            Q(pkg_90min_price__gte=376)
                        )
                    tutor_id_list = list(set(tutor_id_list) &
                                         set(list(queryset.values_list('tutor_id', flat=True).distinct())))
        except Exception as err:
            print '@@@@@filter_listup_tutor : ', str(err)
        finally:
            return tutor_id_list

    @staticmethod
    def listup_user(user_id, user_id_list, page):
        """
        @summary: 유저 리스트업
        @param user_id: 유저 id
        @param user_id_list: 검색 조건에 맞는 유저 리스트
        @param page: page 번호
        @return: 검색 조건에 맞는 유저 리스트
        """
        user_list = []
        page_dict = {}
        try:
            if page is not None and page is not 0:
                cur_page = page
            else:
                cur_page = 1

            login_list = CommonService.get_login_users()
            friend_list = [i['user'] for i in PeopleService.getFriends(user_id)]
            queryset = Tutee.objects.select_related(
                'from_country_id', 'from_city_id', 'livingin_country_id', 'livingin_city_id',
                'timezone_id', 'currency_id', 'user',
                'native1_id', 'native2_id', 'native3_id',
                'lang1_id', 'lang2_id', 'lang3_id', 'lang4_id', 'lang5_id', 'lang6_id',
                'user__tutor', 'user__tutor__teaching1_id', 'user__tutor__teaching2_id',
                'user__tutor__teaching3_id'
            ).filter(
                user_id__in=user_id_list
            )
            paginator = Paginator(queryset, 20)
            page_obj = list(paginator.page(cur_page).object_list)

            for item in page_obj:
                user_dict = TutorService.set_user_info(item, user_id, login_list, friend_list)
                user_list.append(user_dict)

            page_dict['has_next'] = paginator.page(cur_page).has_next()
        except Exception as err:
            print '@@@@@listup_user : ', str(err)
            user_list = []
            page_dict = {}
        finally:
            return user_list, page_dict

    @staticmethod
    def listup_tutor(user_id, tutor_id_list, page):
        """
        @summary: 튜터 리스트업
        @param user_id: 유저 id
        @param tutor_id_list: 검색조건에 맞는 튜터 리스트
        @param page: page 번호
        @return: 검색 조건에 맞는 튜터 리스트
        """
        tutor_list = list()
        page_dict = dict()
        try:
            if page is not None and page is not 0:
                cur_page = page
            else:
                cur_page = 1

            login_list = CommonService.get_login_users()
            bookmark_list = TutorService.bookmark_list(user_id)
            queryset = Tutor.objects.select_related(
                'from_country_id', 'from_city_id', 'livingin_country_id', 'livingin_city_id',
                'timezone_id', 'currency_id',
                'native1_id', 'native2_id', 'native3_id',
                'lang1_id', 'lang2_id', 'lang3_id', 'lang4_id', 'lang5_id', 'lang6_id',
                'teaching1_id', 'teaching2_id', 'teaching3_id', 'user'
            ).extra(
                select={
                    'max_price': """
                        SELECT
                            MAX(CASE
                                WHEN sg_30min_price != 0
                                     AND sg_30min_price * 2 >= sg_60min_price
                                     AND sg_30min_price * 2 >= ( sg_90min_price*2 ) / 3
                                     AND sg_30min_price * 2 >= pkg_30min_price * 2
                                     AND sg_30min_price * 2 >= pkg_60min_price
                                     AND sg_30min_price * 2 >= pkg_90min_price*2 / 3
                                THEN sg_30min_price * 2
                                WHEN sg_60min_price != 0
                                     AND sg_60min_price >= sg_30min_price * 2
                                     AND sg_60min_price >= ( sg_90min_price*2 ) / 3
                                     AND sg_60min_price >= pkg_30min_price*2
                                     AND sg_60min_price >= pkg_60min_price
                                     AND sg_60min_price >= pkg_90min_price*2/3
                                THEN sg_60min_price
                                WHEN sg_90min_price != 0
                                     AND sg_90min_price*2 / 3 >= sg_30min_price * 2
                                     AND sg_90min_price*2 / 3 >= sg_60min_price
                                     AND sg_90min_price*2 / 3 >= pkg_30min_price * 2
                                     AND sg_90min_price*2 / 3 >= pkg_60min_price
                                     AND sg_90min_price*2 / 3 >= pkg_90min_price*2 / 3
                                THEN ROUND(sg_90min_price*2/3)
                                WHEN pkg_30min_price != 0
                                     AND pkg_30min_price * 2 >= sg_30min_price * 2
                                     AND pkg_30min_price * 2 >= sg_60min_price
                                     AND pkg_30min_price * 2 >= sg_90min_price*2 / 3
                                     AND pkg_30min_price * 2 >= pkg_60min_price
                                     AND pkg_30min_price * 2 >= pkg_90min_price*2 / 3
                                THEN pkg_30min_price * 2
                                WHEN pkg_60min_price != 0
                                     AND pkg_60min_price >= sg_30min_price * 2
                                     AND pkg_60min_price >= sg_60min_price
                                     AND pkg_60min_price >= sg_90min_price*2 / 3
                                     AND pkg_60min_price >= pkg_30min_price * 2
                                     AND pkg_60min_price >= pkg_90min_price*2 / 3
                                THEN pkg_60min_price
                                WHEN pkg_90min_price != 0
                                     AND pkg_90min_price*2 / 3 >= sg_30min_price * 2
                                     AND pkg_90min_price*2 / 3 >= sg_60min_price
                                     AND pkg_90min_price*2 / 3 >= sg_90min_price*2 / 3
                                     AND pkg_90min_price*2 / 3 >= pkg_30min_price * 2
                                     AND pkg_90min_price*2 / 3 >= pkg_60min_price
                                THEN ROUND(pkg_90min_price*2 / 3)
                                ELSE 0
                                END)
                        FROM tutor_course
                        WHERE tutor_course.tutor_id=tutor.user_id
                    """,
                    'min_price': """
                        SELECT
                            MIN(CASE
                                WHEN sg_30min_price != 0
                                     AND IFNULL(sg_30min_price * 2 <= NULLIF(sg_60min_price, 0), TRUE)
                                     AND IFNULL(sg_30min_price * 2 <= NULLIF(sg_90min_price*2 / 3, 0), TRUE)
                                     AND IFNULL(sg_30min_price * 2 <= NULLIF(pkg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(sg_30min_price * 2 <= NULLIF(pkg_60min_price, 0), TRUE)
                                     AND IFNULL(sg_30min_price * 2 <= NULLIF(pkg_90min_price*2 / 3, 0), TRUE)
                                THEN sg_30min_price * 2
                                WHEN sg_60min_price != 0
                                     AND IFNULL(sg_60min_price <= NULLIF(sg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(sg_60min_price <= NULLIF(sg_90min_price*2 / 3, 0), TRUE)
                                     AND IFNULL(sg_60min_price <= NULLIF(pkg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(sg_60min_price <= NULLIF(pkg_60min_price, 0), TRUE)
                                     AND IFNULL(sg_60min_price <= NULLIF(pkg_90min_price*2/3, 0), TRUE)
                                THEN sg_60min_price
                                WHEN sg_90min_price != 0
                                     AND IFNULL(sg_90min_price*2 / 3 <= NULLIF(sg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(sg_90min_price*2 / 3 <= NULLIF(sg_60min_price, 0), TRUE)
                                     AND IFNULL(sg_90min_price*2 / 3 <= NULLIF(pkg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(sg_90min_price*2 / 3 <= NULLIF(pkg_60min_price, 0), TRUE)
                                     AND IFNULL(sg_90min_price*2 / 3 <= NULLIF(pkg_90min_price*2 / 3, 0), TRUE)
                                THEN ROUND(sg_90min_price*2/3)
                                WHEN pkg_30min_price != 0
                                     AND IFNULL(pkg_30min_price * 2 <= NULLIF(sg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(pkg_30min_price * 2 <= NULLIF(sg_60min_price, 0), TRUE)
                                     AND IFNULL(pkg_30min_price * 2 <= NULLIF(sg_90min_price*2 / 3, 0), TRUE)
                                     AND IFNULL(pkg_30min_price * 2 <= NULLIF(pkg_60min_price, 0), TRUE)
                                     AND IFNULL(pkg_30min_price * 2 <= NULLIF(pkg_90min_price*2 / 3, 0), TRUE)
                                THEN pkg_30min_price * 2
                                WHEN pkg_60min_price != 0
                                     AND IFNULL(pkg_60min_price <= NULLIF(sg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(pkg_60min_price <= NULLIF(sg_60min_price, 0), TRUE)
                                     AND IFNULL(pkg_60min_price <= NULLIF(sg_90min_price*2 / 3, 0), TRUE)
                                     AND IFNULL(pkg_60min_price <= NULLIF(pkg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(pkg_60min_price <= NULLIF(pkg_90min_price*2 / 3, 0), TRUE)
                                THEN pkg_60min_price
                                WHEN pkg_90min_price != 0
                                     AND IFNULL(pkg_90min_price*2 / 3 <= NULLIF(sg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(pkg_90min_price*2 / 3 <= NULLIF(sg_60min_price, 0), TRUE)
                                     AND IFNULL(pkg_90min_price*2 / 3 <= NULLIF(sg_90min_price*2 / 3, 0), TRUE)
                                     AND IFNULL(pkg_90min_price*2 / 3 <= NULLIF(pkg_30min_price * 2, 0), TRUE)
                                     AND IFNULL(pkg_90min_price*2 / 3 <= NULLIF(pkg_60min_price, 0), TRUE)
                                THEN ROUND(pkg_90min_price*2 / 3)
                                ELSE 0
                                END)
                        FROM tutor_course
                        WHERE tutor_course.tutor_id=tutor.user_id
                    """
                }
            ).filter(
                user_id__in=tutor_id_list
            )
            paginator = Paginator(queryset, 20)
            page_obj = list(paginator.page(cur_page).object_list)
            for item in page_obj:
                tutor_dict = TutorService.set_tutor_info(item, user_id, login_list, bookmark_list)
                tutor_list.append(tutor_dict)
            page_dict['has_next'] = paginator.page(cur_page).has_next()
        except Exception as err:
            print str(err)
        finally:
            return tutor_list, page_dict

    @staticmethod
    def teach_list(tutor):
        """
        @summary: 해당 튜터의 teaching language list
        @param tutor: 튜터 객체
        @return: teaching list
        """
        teach_list = list()
        try:
            for i in range(1, 3):
                if getattr(tutor, 'teaching' + str(i) + '_id'):
                    teach_dict = dict()
                    teach_dict['id'] = getattr(tutor, 'teaching' + str(i) + '_id_id')
                    teach_dict['language'] = getattr(tutor, 'teaching' + str(i))
                    teach_list.append(teach_dict)
        except Exception as err:
            print 'teach_list', str(err)
            teach_list = None
        finally:
            return teach_list

    @staticmethod
    def speak_list(tutor):
        """
        @summary: 해당 튜터의 speak language list
        @param tutor: 튜터 객체
        @return: speak list
        """
        speak_list = list()
        try:
            for i in range(1, 3):
                if getattr(tutor, 'native' + str(i) + '_id'):
                    speak_dict = dict()
                    speak_dict['id'] = getattr(tutor, 'native' + str(i) + '_id_id')
                    speak_dict['language'] = getattr(tutor, 'native' + str(i))
                    speak_dict['level'] = "7"
                    speak_dict['is_learning'] = "true"
                    speak_list.append(speak_dict)
            for i in range(1, 6):
                if getattr(tutor, 'lang' + str(i) + '_id'):
                    speak_dict = dict()
                    speak_dict['id'] = getattr(tutor, 'lang' + str(i) + '_id_id')
                    speak_dict['language'] = getattr(tutor, 'lang' + str(i))
                    speak_dict['level'] = getattr(tutor, 'lang' + str(i) + '_level_id')
                    speak_dict['is_learning'] = getattr(tutor, 'lang' + str(i) + '_learning')
                    speak_list.append(speak_dict)

        except Exception as err:
            print 'speak_list', str(err)
            speak_list = None
        finally:
            return speak_list

    @staticmethod
    def learn_list(tutor):
        """
        @summary: 해당 튜터의 speak language list
        @param tutor: 튜터 객체
        @return: learn list
        """
        learn_list = list()
        try:
            for i in range(1, 6):
                if getattr(tutor, 'lang' + str(i) + '_id'):
                    learn_dict = dict()
                    if getattr(tutor, 'lang' + str(i) + '_learning') == True:
                        learn_dict['id'] = getattr(tutor, 'lang' + str(i) + '_id_id')
                        learn_dict['language'] = getattr(tutor, 'lang' + str(i))
                        learn_dict['level'] = getattr(tutor, 'lang' + str(i) + '_level_id')
                        learn_dict['is_learning'] = getattr(tutor, 'lang' + str(i) + '_learning')
                        learn_list.append(learn_dict)

        except Exception as err:
            print 'learn_list', str(err)
            learn_list = None
        finally:
            return learn_list

    @staticmethod
    def tutor_violation(reporter_id, reportee_id, type, content):
        """
        @summary: 튜터 신고
        @param reporter_id: 신고자 id
        @param reportee_id: 신고당한 튜터 id
        @param type: 신고 타입
        @param content: 신고 내용
        @return: dict()
        """
        result = {
            'isSuccess': True,
            'message': 'Success'
        }
        try:
            violation = TutorViolation(
                reporter_id=reporter_id,
                reportee_id=reportee_id,
                type=type,
                content=content
            )
            violation.save()
        except Exception as err:
            print '@@@@@tutor_violation : ', str(err)
            result['isSuccess'] = False
            result['message'] = str(err)
        finally:
            return result
        
        
    @staticmethod
    def user_violation(reporter_id, reportee_id, type, content):
        """
        @summary: 튜터 신고
        @param reporter_id: 신고자 id
        @param reportee_id: 신고당한 튜터 id
        @param type: 신고 타입
        @param content: 신고 내용
        @return: dict()
        """
        result = {
            'isSuccess': True,
            'message': 'Success'
        }
        try:
            violation = UserViolation(
                reporter_id=reporter_id,
                reportee_id=reportee_id,
                type=type,
                content=content
            )
            violation.save()
        except Exception as err:
            print '@@@@@user_violation : ', str(err)
            result['isSuccess'] = False
            result['message'] = str(err)
        finally:
            return result

    @staticmethod
    def sendMessage(to_user_id, from_user_id, _message, _rvid, _utc_time):
        """
            @summary: 메세지  전송
            @author: msjang, chsin
            @param to_user_id: 받는 유저 id
            @param from_user_id: 보낸 유저 id
            @param _message: 메세지 내용
            @param _rvid: 강의 예약 id
            @param _utc_time: 보낸  유저 utc time 정보
            @return: result( list )
            """
        result = []

        try:
            rv = LessonReservation.objects.get(id=_rvid)
            message = LessonMessage()
            message.sender_id = from_user_id
            message.receiver_id = to_user_id
            message.message = _message
            message.lesson_reservation_id = _rvid
            message.save()

            result = TutorService.getMessage(
                from_user_id, to_user_id, _rvid, _utc_time)

        except Exception as e:
            result = []
            obj = {}
            obj['log'] = str(e)
            result.append(obj)
        finally:
            return result, message

    @staticmethod
    def getMessage(user_id, to_user_id, _rvid, _utc_time):
        """
            @summary: 메세지 목록
            @author: msjang, chsin
            @param user_id: 유저 id
            @param to_user_id: 받는 유저 id
            @param _rvid: 강의 예약 id
            @param _utc_time: utc time 정보
            @return: result( list )
            """
        result = []

        try:

            # UTC 로부터 timegap 계산
            utc_min = 0
            if (int(_utc_time[:3]) > 0):
                utc_min = int(_utc_time[:3]) * 60 + int(_utc_time[4:])
            else:
                utc_min = int(_utc_time[:3]) * 60 - int(_utc_time[4:])

            timegap = timedelta(minutes=utc_min)

            # 메세지 전부 읽음 처리
            LessonMessage.objects.filter(
                receiver_id=user_id, sender_id=to_user_id, lesson_reservation_id=_rvid
            ).update(is_read=1)

            # 튜터와 튜티 간에 주고 받은 메세지를 모두 읽어오는 쿼리셋
            message_qs = LessonMessage.objects.filter(
                Q(receiver_id=user_id, sender_id=to_user_id) | Q(
                    sender_id=user_id, receiver_id=to_user_id),
                lesson_reservation_id=_rvid).order_by('created_time')

            for message in message_qs.iterator():

                # UTC 에 따른 시간 계산
                _date = datetime.strptime(
                    str(message.created_time)[:16], "%Y-%m-%d %H:%M")
                _date += timegap

                timeDay = str(_date).split()
                print timeDay
                flag = True
                for j in result:
                    if j['day'] == timeDay[0]:
                        flag = False
                        break
                    flag = True
                if flag:  # 배열에 존재하지 않을 경우
                    obj = {}
                    obj['day'] = timeDay[0]
                    obj['message'] = []
                    result.append(obj)

                for j in result:
                    if j['day'] == timeDay[0]:
                        messageObj = {}
                        messageObj['mid'] = message.id
                        messageObj['to_user_id'] = message.receiver_id
                        messageObj['from_user_id'] = message.sender_id
                        messageObj['message'] = message.message
                        messageObj['user_id'] = user_id
                        messageObj['time'] = _date.strftime('%H:%M %p')
                        if user_id == message.receiver_id:  # 받은 메세지
                            messageObj['type'] = 'receive'
                        else:
                            messageObj['type'] = 'send'
                        j['message'].append(messageObj)

        except Exception as e:
            print e
            obj = {}
            obj['message'] = str(e)
            result.append(obj)
        finally:
            return result

    @staticmethod
    def checkNewMessage(user_id, to_user_id, _rvid):
        result = False
        try:
            new_msg_qs = LessonMessage.objects.filter(
                receiver_id=user_id, sender_id=to_user_id, lesson_reservation_id=_rvid, is_read=0
            ).aggregate(new_msg_cnt=Count(id))

            if new_msg_qs['new_msg_cnt'] > 0:
                result = True
            else:
                result = False
        except Exception as e:
            result = False
        finally:
            return result