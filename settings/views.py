# -*- encoding: utf-8 -*-
import json

from django.http.response import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, render_to_response, RequestContext, HttpResponse
from django.views.decorators.csrf import csrf_exempt

from settings.service import SettingsService
from tutor.service import TutorService
from wallet import walletService
# from common.models import Profile, Resume, Files, TellpinUser
from tutor.service4registration import TutorService4Registration
from user_auth.utils import check_letter, check_number
from common.common import tellpin_login_required
from common.service import CommonService

from user_auth.models import TellpinUser, TellpinAuthUser

from settings.service import WithdrawService

# Create your views here.
@tellpin_login_required
def privacy(request):
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    print 'privacy'
    resData2 = {}
    resData2['page'] = 'privacy'
    resData = SettingsService.getPrivacy(user_id)
    resData = json.dumps(resData)

    return render_to_response('settings/privacy.html', {'data': resData, 'menu': resData2}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def privacy_save(request):
    print 'privacy_save'
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)
    SettingsService.setPrivacy(user_id, post_dict['data'])
    resData = json.dumps(resData)

    return JsonResponse(resData, safe=False)

@csrf_exempt
@tellpin_login_required
def privacy_checkSNS(request):
    print 'privacy_checkSNS'
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)
    SettingsService.checkSNS(user_id, post_dict['data'])
    resData = json.dumps(resData)

    return JsonResponse(resData, safe=False)


@csrf_exempt
@tellpin_login_required
def privacy_change_password(request):
    print 'call privacy_change_password()'
    body_unicode = request.body.decode('utf-8')
    post_dict = json.loads(body_unicode)
    print post_dict
    password1 = post_dict['change_new_password']
    password2 = post_dict['change_new_password_again']

    print 'password1: '
    print password1
    print 'password2: '
    print password2
    # 비밀번호가 동일한지 확인
    if password1 and password2 and password1 != password2:
        # 비밀번호가 다를경우 validationerror 발생 exception으로 ..
        message = "password mismatch"

    numcheck = check_number(password1)
    charcheck = check_letter(password1)
    email = request.session["email"]

    isSuccess = True

    if not numcheck or not charcheck:
        message = "8 to 20 characters, case-sensitive, Must contain at least one alpha character [a-z,A-Z] and one numeric character[0-9] case-sensitive, Must contain at least one alpha character [a-z,A-Z] and one numeric character[0-9]."
        isSuccess = False
    else:
        auth_user = TellpinAuthUser.objects.get(email=email)
        auth_user.set_password(password1)
        auth_user.save()
        user = TellpinUser.objects.get(email=email)
        user.pw = auth_user.password
        user.save()
        message = "success"
        isSuccess = True

    resData = {}
    resData["message"] = message
    resData["isSuccess"] = isSuccess
    return JsonResponse(resData, safe=False)


@tellpin_login_required
def notification(request):
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    resData2 = {}
    resData2['page'] = 'notification'
    resData = SettingsService.getNotify(user_id)
    resData = json.dumps(resData)

    return render_to_response('settings/notification.html', {'data': resData, 'menu': resData2},
                              RequestContext(request))


@csrf_exempt
@tellpin_login_required
def notification_save(request):
    print 'notification_save'
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)
    print post_dict
    SettingsService.setNotify(user_id, post_dict['data'])
    resData = json.dumps(resData)

    return JsonResponse(resData, safe=False)


@csrf_exempt
@tellpin_login_required
def savePayment(request):
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)
    payment = post_dict['payment']
    resData['isSuccess'] = WithdrawService.savePayment(user_id, payment)

    if resData['isSuccess'] == 1:
        resData['payment'] = WithdrawService.getPayment(user_id)

    return JsonResponse(resData)


@tellpin_login_required
def setWithdraw(request):
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    resData['payment'] = WithdrawService.getPayment(user_id)
    resData['nation'] = CommonService.country_list()
    resData2 = {}
    resData2['page'] = 'withdraw'

    return render_to_response('settings/withdraw.html', {'data': json.dumps(resData), 'menu': resData2},
                              RequestContext(request))


@csrf_exempt
@tellpin_login_required
def modifyPaypal(request):
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    pw = post['pw']
    paypal = post['paypal']

    if walletService.checkPassword(user_id, pw) == 0:
        resData['checkPw'] = 0
        return JsonResponse(resData)
    else:
        resData['checkPw'] = 1

    resData['isSuccess'] = WithdrawService.modifyPaypal(user_id, paypal)

    return JsonResponse(resData)


@tellpin_login_required
def myprofile(request):
    """
    @summary: tutor profile edit
    @author: msjang
    @param request
    @return: url - tutor/tutor_edit_profile.html or url - tutor/tutee_edit_profile.html
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    isTutor = TutorService.is_tutor(user_id)
    resData2 = {}
    resData2['page'] = 'myprofile'
    if isTutor:
        return render_to_response('tutor/tutor_edit_profile.html', {'is_tutor': isTutor,
                                                                       'menu': resData2}, RequestContext(request))
    else:
        return render_to_response('tutor/tutee_edit_profile.html', {'is_tutor': isTutor,
                                                                       'menu': resData2}, RequestContext(request))
