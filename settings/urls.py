'''
Created on 2016. 3. 16.

@author: khyun
'''

from django.conf.urls import url
import views

urlpatterns = [
               url(r'privacy/$', views.privacy),
               url(r'privacy/save/$', views.privacy_save),
               url(r'privacy/save/checkSNS/$', views.privacy_checkSNS),
               url(r'privacy/change_password/$', views.privacy_change_password),
               url(r'notification/$', views.notification),
               url(r'notification/save/$', views.notification_save),
               url(r'withdraw/$', views.setWithdraw ),
               url(r'withdraw/save/$', views.savePayment ),
               url(r'withdraw/modify/$', views.modifyPaypal ),
               url(r'^editprofile/$', views.myprofile ),
               ]