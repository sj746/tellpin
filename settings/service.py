# -*- coding: utf-8 -*-
'''
Created on 2016. 3. 16.

@author: khyun
'''

from django.db import connection
# from common.models import SettingsProfile, SettingsNotification
from settings.models import SettingsNotification, SettingsPrivacy
from user_auth.models import TellpinUser
from tutor.models import Tutee


# from django.forms.models import model_to_dict
from common import common
from datetime import date, datetime
from tutor.models import TutorPaymentInfo


class SettingsService(object):

    def __init__(self, params):
        '''
        Constructor
        '''

    #get user ID
    @staticmethod
    def get_user_id(_email):
        """
        @summary: Tutor id 조회
        @author: hsrjmk
        @param user email
        @return: None
        """
        print _email
        try:
            return TellpinUser.objects.get(email=_email).id
        except:
            return None

    @staticmethod
    def getPrivacy(_user_id):
        """
        @summary: 개인 정보 조회
        @author: hsrjmk
        @param _user_id : 사용자 id
        @return: 개인정보 obj
        """
        try:
            pr = SettingsPrivacy.objects.get(user_id=_user_id)
        except:
            pr = SettingsPrivacy(user_id=_user_id).save()
            pr = SettingsPrivacy.objects.get(user_id=_user_id)

        obj = {}
        obj['show_gender'] = True if pr.show_gender == 1 else False
        obj['show_age'] = True if pr.show_age == 1 else False
        obj['receive_message'] = True if pr.receive_message == 1 else False
        
        #obj['is_visible'] = SettingsService.getLEVisible(_user_id)

        return obj

    @staticmethod
    def getLEVisible(_user_id):
        """
        @summary: 개인 정보 조회
        @author: hsrjmk
        @param _user_id : 사용자 id
        @return: 개인정보 obj
        """
        try:
            tutee = Tutee.objects.get(user_id=_user_id)
        except Exception as e:
            print e

        return True if tutee.is_visible == 1 else False

    @staticmethod
    def setPrivacy(_user_id, _data):
        """
        @summary: 개인 정보 변경
        @author: hsrjmk
        @param _user_id : 사용자
        @param _data : 바꿀 정보
        @return: True/False
        """
        result = True

        try:
            pr = SettingsPrivacy.objects.get(user_id = _user_id)
        except Exception as e:
            print e
            result = False
            return result

#         obj = {}
#         obj['show_gender'] = True if pr.show_gender==1 else False
#         obj['show_age'] = True if pr.show_age==1 else False
#         obj['receive_messages'] = True if pr.receive_messages==1 else False
        try:
            pr.show_gender = _data['show_gender']
            pr.show_age = _data['show_age']
            pr.receive_message = _data['receive_message']
            pr.save()
            
        except Exception as e:
            print e

        #result = SettingsService.setLEVisible(_user_id, _data)

        return result

    @staticmethod
    def checkSNS(user_id, data):
        """
        @summary: 설정 창에서 sns 로그인 정보 표시
        @author: shkim
        @param user_id: 사용자 id
        @param data: sns data
        @return True or False
        """
        pass

    @staticmethod
    def setLEVisible(_user_id, _data):
        """
        @summary: 개인 정보 변경
        @author: hsrjmk
        @param _user_id : 사용자
        @param _data : 바꿀 정보
        @return: True/False
        """
        try:
            tutee = Tutee.objects.get(user_id=_user_id)
        except Exception as e:
            print e
            return False

        try:
            tutee.is_visible = _data['is_visible']
            tutee.save()
        except Exception as e:
            print e

        return True

    @staticmethod
    def setNotify(_user_id, _data):
        """
        @summary: 개인 알람 설정
        @author: hsrjmk
        @param _user_id : 사용자 id
        @param _data : 변경될 정보
        @return: None
        """

        try:
            noti = SettingsNotification.objects.get(user_id = _user_id)
        except Exception as e:
            print e
            return False

#         obj = {}
#         obj['show_gender'] = True if pr.show_gender==1 else False
#         obj['show_age'] = True if pr.show_age==1 else False
#         obj['receive_messages'] = True if pr.receive_messages==1 else False

        print _data
        try:
            noti.t_lb_new = _data['t_lb_new']
            noti.t_lb_comment = _data['t_lb_comment'] 
            noti.t_lb_upvote = _data['t_lb_upvote']
            noti.t_tp_new = _data['t_tp_new']
            noti.t_tp_answer = _data['t_tp_answer'] 
            noti.t_tp_comment = _data['t_tp_comment']
            noti.t_tp_upvote = _data['t_tp_upvote']
            noti.t_tp_pin = _data['t_tp_pin'] 
            noti.t_lq_new = _data['t_lq_new']
            noti.t_lq_answer = _data['t_lq_answer']
            noti.t_lq_comment = _data['t_lq_comment'] 
            noti.t_lq_upvote = _data['t_lq_upvote']
            noti.t_lq_pin = _data['t_lq_pin'] 
            noti.t_news_announce = _data['t_news_announce']
            noti.t_news_weekly = _data['t_news_weekly']

            noti.noti_by_email = _data['noti_by_email'] 
            noti.m_lb_new = _data['m_lb_new'] 
            noti.m_lb_comment = _data['m_lb_comment']
            noti.m_lb_upvote = _data['m_lb_upvote']
            noti.m_tp_new = _data['m_tp_new']
            noti.m_tp_answer = _data['m_tp_answer']
            noti.m_tp_comment = _data['m_tp_comment'] 
            noti.m_tp_upvote = _data['m_tp_upvote']
            noti.m_tp_pin = _data['m_tp_pin'] 
            noti.m_lq_new = _data['m_lq_new']
            noti.m_lq_answer = _data['m_lq_answer'] 
            noti.m_lq_comment = _data['m_lq_comment']
            noti.m_lq_upvote = _data['m_lq_upvote']
            noti.m_lq_pin = _data['m_lq_pin'] 
            noti.m_news_announce = _data['m_news_announce']
            noti.m_news_weekly = _data['m_news_weekly'] 

            
            noti.save()
        except Exception as e:
            print e

        return True

    @staticmethod
    def getNotify(_user_id):
        """
        @summary: 개인 알람 설정
        @author: hsrjmk
        @param _user_id : 사용자 id
        @return: None
        """

        try:
            noti = SettingsNotification.objects.get(user_id = _user_id)
        except:
            SettingsNotification(user_id = _user_id).save()
            noti = SettingsNotification.objects.get(user_id = _user_id)

        print noti
        obj = {}
        obj['noti_by_tellpin'] = True if noti.noti_by_tellpin==1 else False
        obj['t_lb_new'] = True if noti.t_lb_new==1 else False
        obj['t_lb_comment'] = True if noti.t_lb_comment==1 else False
        obj['t_lb_upvote'] = True if noti.t_lb_upvote==1 else False
        #obj['t_lb_pin'] = True if noti.t_lb_pin==1 else False
        obj['t_tp_new'] = True if noti.t_tp_new==1 else False
        obj['t_tp_answer'] = True if noti.t_tp_answer==1 else False
        obj['t_tp_comment'] = True if noti.t_tp_comment==1 else False
        obj['t_tp_upvote'] = True if noti.t_tp_upvote==1 else False
        obj['t_tp_pin'] = True if noti.t_tp_pin==1 else False
        obj['t_lq_new'] = True if noti.t_lq_new==1 else False
        obj['t_lq_answer'] = True if noti.t_lq_answer==1 else False
        obj['t_lq_comment'] = True if noti.t_lq_comment==1 else False
        obj['t_lq_upvote'] = True if noti.t_lq_upvote==1 else False
        obj['t_lq_pin'] = True if noti.t_lq_pin==1 else False
        obj['t_news_announce'] = True if noti.t_news_announce==1 else False
        obj['t_news_weekly'] = True if noti.t_news_weekly==1 else False
        
        obj['noti_by_email'] = True if noti.noti_by_email==1 else False
        obj['m_lb_new'] = True if noti.m_lb_new==1 else False
        obj['m_lb_comment'] = True if noti.m_lb_comment==1 else False
        obj['m_lb_upvote'] = True if noti.m_lb_upvote==1 else False
        #obj['m_lb_pin'] = True if noti.m_lb_pin==1 else False
        obj['m_tp_new'] = True if noti.m_tp_new==1 else False
        obj['m_tp_answer'] = True if noti.m_tp_answer==1 else False
        obj['m_tp_comment'] = True if noti.m_tp_comment==1 else False
        obj['m_tp_upvote'] = True if noti.m_tp_upvote==1 else False
        obj['m_tp_pin'] = True if noti.m_tp_pin==1 else False
        obj['m_lq_new'] = True if noti.m_lq_new==1 else False
        obj['m_lq_answer'] = True if noti.m_lq_answer==1 else False
        obj['m_lq_comment'] = True if noti.m_lq_comment==1 else False
        obj['m_lq_upvote'] = True if noti.m_lq_upvote==1 else False
        obj['m_lq_pin'] = True if noti.m_lq_pin==1 else False
        obj['m_news_announce'] = True if noti.m_news_announce==1 else False
        obj['m_news_weekly'] = True if noti.m_news_weekly==1 else False


        return obj


class WithdrawService(object):
    def __init__(self):
        pass

    @staticmethod
    def getPayment(user_id):
        obj = {}
        try:
            cursor = connection.cursor()
            sql = '''
                SELECT first_name, last_name, street_addr, city, state, zip, phone, paypal, email
                     , country
                FROM payments
                WHERE user_id = %s
            '''
            cursor.execute(sql, [ user_id ])
            obj = common.dictfetchall( cursor )[0]
        except Exception as e:
            obj['err'] = str( e )
        finally:
            cursor.close()
            return obj

    @staticmethod
    def getNation():
        result = []
        try:
            cursor = connection.cursor()
            sql = '''
                SELECT nid, name_en
                FROM nation_info
            '''
            cursor.execute( sql )
            result = common.dictfetchall( cursor )
        except Exception as e:
            print str( e )
        finally:
            cursor.close()
            return result

    @staticmethod
    def savePayment(user_id, obj):
        result = 1
        try:
            pay, created = TutorPaymentInfo.objects.get_or_create(user_id=user_id)
            for field in obj:
                setattr(pay, field, obj[field])
            setattr(pay, 'updated_time', datetime.now())
            pay.save()
        except Exception as e:
            print str( e )
            result = 0
        finally:
            return result

    @staticmethod
    def modifyPaypal(user_id, paypal):
        result = 1
        print "modifyPaypal"
        try:
            pay = TutorPaymentInfo.objects.get(user_id=user_id)
            pay.paypal = paypal
            pay.save()
        except Exception as e:
            print "@@@@@@@@modifyPaypal : ", + str(e)
        finally:
            return result