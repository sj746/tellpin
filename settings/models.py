# -*- coding: utf-8 -*-
###############################################################################
# filename    : settings > models.py
# description : Settings 모델 정의
# author      : hsrjmk@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160922 hsrjmk 최조 작성
#
###############################################################################
from django.db import models
from user_auth.models import TellpinUser

              
class SettingsNotification(models.Model):
    user = models.ForeignKey(TellpinUser)

    noti_by_tellpin = models.BooleanField("Check 'On site' on 'Tellpin notifications'", default=True)
    t_lb_new = models.BooleanField("Check 'On site' on 'New post by my friend or tutor'", default=True)
    t_lb_comment = models.BooleanField("Check 'On site' on 'Comments on my post or comment'", default=True)
    t_lb_upvote = models.BooleanField("Check 'On site' on 'Upvotes my post'", default=True)
#     t_lb_pin = models.BooleanField("Check 'On site' on ", default=False)
    t_tp_new = models.BooleanField("Check 'On site' on 'New request(s) by my friend/tutee'", default=True)
    t_tp_answer = models.BooleanField("Check 'On site' on 'Answer to my request'", default=True)
    t_tp_comment = models.BooleanField("Check 'On site' on 'Comments on my request/answer/comment'", default=True)
    t_tp_upvote = models.BooleanField("Check 'On site' on 'Upvotes my answer'", default=True)
    t_tp_pin = models.BooleanField("Check 'On site' on 'Answer to my pinned request'", default=True)
    t_lq_new = models.BooleanField("Check 'On site' on 'New request(s) by my friend/tutee'", default=True)
    t_lq_answer = models.BooleanField("Check 'On site' on 'Answer to my request'", default=True)
    t_lq_comment = models.BooleanField("Check 'On site' on 'Comments on my request/answer/comment'", default=True)
    t_lq_upvote = models.BooleanField("Check 'On site' on 'Upvotes my answer'", default=True)
    t_lq_pin = models.BooleanField("Check 'On site' on 'Answer to my pinned request'", default=True)
    t_news_announce = models.BooleanField("Check 'On site' on 'Announcements on updates, tips and how-tos'", default=True)
    t_news_weekly = models.BooleanField("Check 'On site' on 'Weekly Newsletter'", default=True)
    
    noti_by_email = models.BooleanField("Check 'By e-mail' on Tellpin notifications", default=True)
    m_lb_new = models.BooleanField("Check 'By e-mail' on 'New post by my friend or tutor'", default=True)
    m_lb_comment = models.BooleanField("Check 'By e-mail' on 'Comments on my post or comment'", default=True)
    m_lb_upvote = models.BooleanField("Check 'By e-mail' on 'Upvotes my post'", default=True)
#     m_lb_pin = models.BooleanField("Check 'By e-mail' on ", default=False)
    m_tp_new = models.BooleanField("Check 'By e-mail' on 'New request(s) by my friend/tutee'", default=True)
    m_tp_answer = models.BooleanField("Check 'By e-mail' on 'Answer to my request'", default=True)
    m_tp_comment = models.BooleanField("Check 'By e-mail' on 'Comments on my request/answer/comment'", default=True)
    m_tp_upvote = models.BooleanField("Check 'By e-mail' on 'Upvotes my answer'", default=True)
    m_tp_pin = models.BooleanField("Check 'By e-mail' on 'Answer to my pinned request'", default=True)
    m_lq_new = models.BooleanField("Check 'By e-mail' on 'New request(s) by my friend/tutee'", default=True)
    m_lq_answer = models.BooleanField("Check 'By e-mail' on 'Answer to my request'", default=True)
    m_lq_comment = models.BooleanField("Check 'By e-mail' on 'Comments on my request/answer/comment'", default=True)
    m_lq_upvote = models.BooleanField("Check 'By e-mail' on 'Upvotes my answer'", default=True)
    m_lq_pin = models.BooleanField("Check 'By e-mail' on 'Answer to my pinned request'", default=True)
    m_news_announce = models.BooleanField("Check 'By e-mail' on 'Announcements on updates, tips and how-tos'", default=True)
    m_news_weekly = models.BooleanField("Check 'By e-mail' on 'Weekly Newsletter'", default=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    class Meta:
        db_table = u'settings_notification'


class SettingsPrivacy(models.Model):
    user = models.ForeignKey(TellpinUser)
    
    show_gender = models.BooleanField("Check on 'Show my gender in my profile'", default=True)
    show_age = models.BooleanField("Check on 'Show my age in my profile'", default=True)
    receive_message = models.BooleanField("Check on 'Receive messages from someone who is not my tutor, tutee or friend'", default=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    class Meta:
        db_table = u'settings_privacy'
