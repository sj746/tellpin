#-*- coding: utf-8 -*-

###############################################################################
# filename    : support > service.py
# description : 헬프 센터 화면 구성을 위한 service file
# author      : sj746@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 sj746 최초 작성
#
#
###############################################################################

import os
import time

from datetime import timedelta

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.forms.models import model_to_dict

from support.models import HelpFAQMultiLang, HelpQuestion, HelpReply, HelpAnswer
from user_auth.models import File, TellpinUser
from tellpin.settings import BASE_DIR
from tutor.service import TutorService
from tutor.models import Tutor, Tutee
from common.service import CommonService


class SupportService(object):
    def __init__(self):
        pass

    PER_PAGE = 10

    TOPIC = {1: {'name': 'Guide & Announcements', 'url': 'gna'},
             2: {'name': 'Profile & Account', 'url': 'pna'},
             3: {'name': 'For (Wannabe)Tutors', 'url': 'tutor'},
             4: {'name': 'For Tutees', 'url': 'tutee'},
             5: {'name': 'Tellpin Player', 'url': 'tellpinplayer'},
             6: {'name': 'Tellpin Search', 'url': 'tellpinsearch'},
             7: {'name': 'Community', 'url': 'community'},
    }
    SUB_TOPIC = {
             1: {'name': 'My Profile', 'url': 'myprofile'},
             2: {'name': 'Password', 'url': 'password'},
             3: {'name': 'Privacy', 'url': 'privacy'},
    }
    
    
    ####################################################################
    #    FAQ 관련 QuerySet
    ####################################################################
    @staticmethod
    def get_top_questions(lang_id):
        """
        @summary: Top Questions 정렬순서:1~5
        @author: hsrjmk
        @param lang_id: 다국어(1: English), 한글(2: Korean)
        @return: top_faq_list
        """
#         sql = "SELECT a.top, b.* FROM help_faq a INNER JOIN help_faq_ml b ON a.faq_id = b.faq_id WHERE a.top IS NOT NULL AND b.ml = %s ORDER BY a.top ASC;"
        faq_ml_objs = HelpFAQMultiLang.objects.filter(multilang_id=lang_id)\
                                                    .exclude(faq__top_question__isnull=True)\
                                                    .select_related('faq', 'multilang')\
                                                    .order_by('faq__top_question')
        top_faq_list = []
        for faq_ml_obj in faq_ml_objs:
            question = model_to_dict(faq_ml_obj, exclude=['id', 'faq', 'multilang', 'del_by_admin'])
            question['ml_id'] = faq_ml_obj.id
            question['ml'] = faq_ml_obj.multilang_id

            question['faq_id'] = faq_ml_obj.faq.id
            if faq_ml_obj.faq.sub_topic_id:
                question['sub_topic_id'] = faq_ml_obj.faq.sub_topic_id
                question['sub_topic_name'] = faq_ml_obj.faq.sub_topic
            question['created_time'] = faq_ml_obj.faq.created_time

            top_faq_list.append(question)
        return top_faq_list

    @staticmethod
    def get_gna(lang_id):
        """
        @summary: Guide & Announcements 항목을 가져오는 함수
        @author: hsrjmk
        @param lang_id: 다국어(1: English), 한글(2: Korean)
        @return: gna_faq_list
        """
#         sql = "SELECT b.* FROM help_faq a INNER JOIN help_faq_ml b ON a.faq_id = b.faq_id WHERE b.ml = %s AND a.topic_id = 1 ORDER BY b.created_time DESC;"
        faq_ml_objs = HelpFAQMultiLang.objects.filter(multilang_id=lang_id, faq__topic_id=1)\
                                                    .select_related('faq', 'multilang')\
                                                    .order_by('-created_time')
        gna_faq_list = []
        for faq_ml_obj in faq_ml_objs:
            question = model_to_dict(faq_ml_obj, exclude=['id', 'faq', 'multilang', 'del_by_admin'])
            question['ml_id'] = faq_ml_obj.id
            question['ml'] = faq_ml_obj.multilang_id
            
            question['faq_id'] = faq_ml_obj.faq.id
            if faq_ml_obj.faq.sub_topic_id:
                question['sub_topic_id'] = faq_ml_obj.faq.sub_topic_id
                question['sub_topic_name'] = faq_ml_obj.faq.sub_topic
            question['created_time'] = faq_ml_obj.faq.created_time
            
            gna_faq_list.append(question)
        return gna_faq_list
    
    @staticmethod
    def get_pna(lang_id):
        """
        @summary: Profile & Account 항목을 가져오는 함수
        @author: hsrjmk
        @param lang_id: 다국어(1: English), 한글(2: Korean)
        @return: pna_faq_list
        """
#         sql = "SELECT b.*, a.sub_topic_id FROM help_faq a INNER JOIN help_faq_ml b ON a.faq_id = b.faq_id WHERE b.ml = %s AND a.topic_id = 2 ORDER BY b.created_time DESC;"
        faq_ml_objs = HelpFAQMultiLang.objects.filter(multilang_id=lang_id, faq__topic_id=2)\
                                                    .select_related('faq', 'multilang')\
                                                    .order_by('-created_time')
        pna_faq_list = []
        for faq_ml_obj in faq_ml_objs:
            question = model_to_dict(faq_ml_obj, exclude=['id', 'faq', 'multilang', 'del_by_admin'])
            question['ml_id'] = faq_ml_obj.id
            question['ml'] = faq_ml_obj.multilang_id
            
            question['faq_id'] = faq_ml_obj.faq.id
            if faq_ml_obj.faq.sub_topic_id:
                question['sub_topic_id'] = faq_ml_obj.faq.sub_topic_id
                question['sub_topic_name'] = faq_ml_obj.faq.sub_topic
            question['created_time'] = faq_ml_obj.faq.created_time

            pna_faq_list.append(question)
        return pna_faq_list
    
    @staticmethod
    def get_for_tutors(lang_id):
        """
        @summary: For (Wannabe) Tutors 으로 가져오는 함수
        @author: hsrjmk
        @param lang_id: 다국어(1: English), 한글(2: Korean)
        @return: tutor_faq_list
        """
#         sql = "SELECT b.* FROM help_faq a INNER JOIN help_faq_ml b ON a.faq_id = b.faq_id WHERE b.ml = %s AND a.topic_id = 3 ORDER BY b.created_time DESC;"
        faq_ml_objs = HelpFAQMultiLang.objects.filter(multilang_id=lang_id, faq__topic_id=3)\
                                                    .select_related('faq', 'multilang')\
                                                    .order_by('-created_time')
        tutor_faq_list = []
        for faq_ml_obj in faq_ml_objs:
            question = model_to_dict(faq_ml_obj, exclude=['id', 'faq', 'multilang', 'del_by_admin'])
            question['ml_id'] = faq_ml_obj.id
            question['ml'] = faq_ml_obj.multilang_id
            
            question['faq_id'] = faq_ml_obj.faq.id
            if faq_ml_obj.faq.sub_topic_id:
                question['sub_topic_id'] = faq_ml_obj.faq.sub_topic_id
                question['sub_topic_name'] = faq_ml_obj.faq.sub_topic
            question['created_time'] = faq_ml_obj.faq.created_time
            
            tutor_faq_list.append(question)
        return tutor_faq_list
    
    @staticmethod
    def get_for_tutees(lang_id):
        """
        @summary: For Tutees 항목 가져오는 함수
        @author: hsrjmk
        @param lang_id: 다국어(1: English), 한글(2: Korean)
        @return: tutee_faq_list
        """
#         sql = "SELECT b.* FROM help_faq a INNER JOIN help_faq_ml b ON a.faq_id = b.faq_id WHERE b.ml = %s AND a.topic_id = 4 ORDER BY b.created_time DESC;"
        faq_ml_objs = HelpFAQMultiLang.objects.filter(multilang_id=lang_id, faq__topic_id=4)\
                                                    .select_related('faq', 'multilang')\
                                                    .order_by('-created_time')
        tutee_faq_list = []
        for faq_ml_obj in faq_ml_objs:
            question = model_to_dict(faq_ml_obj, exclude=['id', 'faq', 'multilang', 'del_by_admin'])
            question['ml_id'] = faq_ml_obj.id
            question['ml'] = faq_ml_obj.multilang_id
            
            question['faq_id'] = faq_ml_obj.faq.id
            if faq_ml_obj.faq.sub_topic_id:
                question['sub_topic_id'] = faq_ml_obj.faq.sub_topic_id
                question['sub_topic_name'] = faq_ml_obj.faq.sub_topic
            question['created_time'] = faq_ml_obj.faq.created_time
            
            tutee_faq_list.append(question)
        return tutee_faq_list
    
    @staticmethod
    def get_tps(lang_id):
        """
        @summary: Tellpin Player항목 가져오는 함수
        @author: hsrjmk
        @param lang_id: 다국어(1: English), 한글(2: Korean)
        @return: player_faq_list
        """
#         sql = "SELECT b.* FROM help_faq a INNER JOIN help_faq_ml b ON a.faq_id = b.faq_id WHERE b.ml = %s AND a.topic_id = 5 ORDER BY b.created_time DESC;"
        faq_ml_objs = HelpFAQMultiLang.objects.filter(multilang_id=lang_id, faq__topic_id=5)\
                                                    .select_related('faq', 'multilang')\
                                                    .order_by('-created_time')
        player_faq_list = []
        for faq_ml_obj in faq_ml_objs:
            question = model_to_dict(faq_ml_obj, exclude=['id', 'faq', 'multilang', 'del_by_admin'])
            question['ml_id'] = faq_ml_obj.id
            question['ml'] = faq_ml_obj.multilang_id
            
            question['faq_id'] = faq_ml_obj.faq.id
            if faq_ml_obj.faq.sub_topic_id:
                question['sub_topic_id'] = faq_ml_obj.faq.sub_topic_id
                question['sub_topic_name'] = faq_ml_obj.faq.sub_topic
            question['created_time'] = faq_ml_obj.faq.created_time
            
            player_faq_list.append(question)
        return player_faq_list
    
    @staticmethod
    def get_tss(lang_id):
        """
        @summary: Tellpin Search 항목 가져오는 함수
        @author: hsrjmk
        @param lang_id: 다국어(1: English), 한글(2: Korean)
        @return: search_faq_list
        """
#         sql = "SELECT b.* FROM help_faq a INNER JOIN help_faq_ml b ON a.faq_id = b.faq_id WHERE b.ml = %s AND a.topic_id = 6 ORDER BY b.created_time DESC;"
        faq_ml_objs = HelpFAQMultiLang.objects.filter(multilang_id=lang_id, faq__topic_id=6)\
                                                    .select_related('faq', 'multilang')\
                                                    .order_by('-created_time')
        search_faq_list = []
        for faq_ml_obj in faq_ml_objs:
            question = model_to_dict(faq_ml_obj, exclude=['id', 'faq', 'multilang', 'del_by_admin'])
            question['ml_id'] = faq_ml_obj.id
            question['ml'] = faq_ml_obj.multilang_id
            
            question['faq_id'] = faq_ml_obj.faq.id
            if faq_ml_obj.faq.sub_topic_id:
                question['sub_topic_id'] = faq_ml_obj.faq.sub_topic_id
                question['sub_topic_name'] = faq_ml_obj.faq.sub_topic
            question['created_time'] = faq_ml_obj.faq.created_time
            
            search_faq_list.append(question)
        return search_faq_list
    
    @staticmethod
    def get_communities(lang_id):
        """
        @summary: Community 항목 가져오는 함수
        @author: hsrjmk
        @param lang_id: 다국어(1: English), 한글(2: Korean)
        @return: community_faq_list
        """
#         sql = "SELECT b.* FROM help_faq a INNER JOIN help_faq_ml b ON a.faq_id = b.faq_id WHERE b.ml = %s AND a.topic_id = 6 ORDER BY b.created_time DESC;"
        faq_ml_objs = HelpFAQMultiLang.objects.filter(multilang_id=lang_id, faq__topic_id=6)\
                                                    .select_related('faq', 'multilang')\
                                                    .order_by('-created_time')
        community_faq_list = []
        for faq_ml_obj in faq_ml_objs:
            question = model_to_dict(faq_ml_obj, exclude=['id', 'faq', 'multilang', 'del_by_admin'])
            question['ml_id'] = faq_ml_obj.id
            question['ml'] = faq_ml_obj.multilang_id
            
            question['faq_id'] = faq_ml_obj.faq.id
            if faq_ml_obj.faq.sub_topic_id:
                question['sub_topic_id'] = faq_ml_obj.faq.sub_topic_id
                question['sub_topic_name'] = faq_ml_obj.faq.sub_topic
            question['created_time'] = faq_ml_obj.faq.created_time
            
            community_faq_list.append(question)
        return community_faq_list
    
    @staticmethod
    def get_faq(faq_ml_id):
        """
        @summary: FAQ 가져오는 함수
        @author: hsrjmk
        @param lang_id: 다국어(1: English), 한글(2: Korean)
        @return: question
        """
#         sql = "SELECT b.*, a.topic_id, a.sub_topic_id FROM help_faq a INNER JOIN help_faq_ml b ON a.faq_id = b.faq_id WHERE b.ml_id = %s ORDER BY b.created_time DESC;"
        try:
            faq_ml_obj = HelpFAQMultiLang.objects.select_related('faq', 'multilang').get(id=faq_ml_id)
                                            
            question = model_to_dict(faq_ml_obj, exclude=['id', 'faq', 'multilang', 'del_by_admin'])
            question['ml_id'] = faq_ml_obj.id
            question['ml'] = faq_ml_obj.multilang_id
            
            question['faq_id'] = faq_ml_obj.faq.id
            question['topic_url'] = SupportService.TOPIC[faq_ml_obj.faq.topic_id]['url']
            question['topic_name'] = faq_ml_obj.faq.topic
            if faq_ml_obj.faq.sub_topic_id:
                question['sub_topic_url'] = SupportService.SUB_TOPIC[faq_ml_obj.faq.sub_topic_id]['url']
                question['sub_topic_name'] = faq_ml_obj.faq.sub_topic
            question['created_time'] = faq_ml_obj.faq.created_time
        except ObjectDoesNotExist as e:
            print str(e)
             
        return question
    
    @staticmethod
    def get_sub_topic(lang_id, sub_topic):
        """
        @summary: sub faq 항목 가져오는 함수
        @author: hsrjmk
        @param sub_topic: sub topic id
        @return: top_list
        """
#         sql = "SELECT b.*, a.topic_id, a.sub_topic_id FROM help_faq a INNER JOIN help_faq_ml b ON a.faq_id = b.faq_id WHERE a.sub_topic_id = %s ORDER BY b.created_time DESC;"
        faq_ml_objs = HelpFAQMultiLang.objects.filter(multilang_id=lang_id, sub_topic_id=sub_topic)\
                                            .select_related('faq', 'multilang')\
                                            .order_by('-created_time')
        faq_list = []
        for faq_ml in faq_ml_objs:
            question = model_to_dict(faq_ml)
            question['topic_url'] = SupportService.TOPIC[faq_ml.faq.topic_id]['url']
            question['topic_name'] = faq_ml.faq.topic
            question['sub_topic_url'] = SupportService.SUB_TOPIC[faq_ml.faq.sub_topic_id]['url']
            question['sub_topic_name'] = faq_ml.faq.sub_topic
            faq_list.append(question)
        return faq_list
    
    
    ####################################################################
    #    QNA 관련 QuerySet
    ####################################################################
    @staticmethod
    def get_my_questions(user_id, status, user_utc):
        """
        @summary: 내가 작성한 질문을 가져오는 함수
        @author: hsrjmk
        @param user_id : 사용자 id
        @param status : 상태 1: open 2: awaiting your reply 3: closed
        @param user_utc : 현지 시간
        @return: my_question_list: 내가 작성한 질문 리스트
        """
        if status == 0 :
            question_objs = HelpQuestion.objects.filter(user_id=user_id, del_by_user=False).order_by('-created_time')
        else :
            question_objs = HelpQuestion.objects.filter(user_id=user_id, status=status, del_by_user=False).order_by('-created_time')
        
        my_question_list = []
        for question_obj in question_objs :
            question = model_to_dict(question_obj, exclude=['id', 'user', 'question_category', 'del_by_user', 'del_by_admin'])
            question['user_id'] = question_obj.user_id
            question['cate_id'] = question_obj.question_category
            question['qna_id'] = question_obj.id
            question['qna_ref_id'] = question_obj.id
            question['qna_type'] = 1
            question['created_time'] = (question_obj.created_time + timedelta(hours=user_utc)).strftime('%Y-%m-%d %H:%M:%S')
            my_question_list.append(question)
        return my_question_list
    
    @staticmethod
    def get_my_question_detail(question_id, user_utc):
        """
        @summary: 내가 작성한 질문의 상세보기 정보를 가져오는 함수
        @author: hsrjmk
        @param question_id : 질문 id
        @param user_utc : 현지 시간
        @return: 내가 작성한 질문 상세
        """
        detail_list = []
        # Question information
        try:
            question_obj = HelpQuestion.objects.select_related('user').get(id=question_id)
            question = model_to_dict(question_obj)
            question['qna_id'] = question_obj.id
            question['user_id'] = question_obj.user_id
            question['created_time'] = (question_obj.created_time + timedelta(hours=user_utc)).strftime('%Y-%m-%d %H:%M:%S')
            if TutorService.is_tutor(question_obj.user_id):
                user_info = Tutor.objects.get(user=question_obj.user_id)
                question['writer_name'] = user_info.name
                question['writer_photo'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                      user_info.photo_filename)
            else:
                user_info = Tutee.objects.get(user=question_obj.user_id)
                question['writer_name'] = user_info.name
                question['writer_photo'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                      user_info.photo_filename)

            #attach file
            question["attach_file"] = SupportService.get_attach_file(question_id, 'QNA_Q')
            detail_list.append(question)

            # Answer information
            answer_obj = HelpAnswer.objects.get(question_id=question_id)
            answer = model_to_dict(answer_obj)
            answer['qna_id'] = answer_obj.id
            answer['user_id'] = answer_obj.user_id
            answer['created_time'] = (answer_obj.created_time + timedelta(hours=user_utc)).strftime('%Y-%m-%d %H:%M:%S')
            answer['writer_info'] = {'name': 'Customer Service'}
            #attach file
            answer["attach_file"] = SupportService.get_attach_file(question_id, 'QNA_A')
            detail_list.append(answer)

            # Reply information
            reply_objs = HelpReply.objects.filter(question_id=question_id, del_by_user=False)\
                                        .select_related('question', 'answer', 'user')\
                                        .order_by('created_time')
            for reply_obj in reply_objs:
                reply = model_to_dict(reply_obj)
                reply['qna_id'] = reply_objs.id
                reply['user_id'] = reply_obj.user_id
                reply['created_time'] = (reply_obj.created_time + timedelta(hours=user_utc)).strftime('%Y-%m-%d %H:%M:%S')
                reply['writer_info'] = model_to_dict(reply_objs.user, exclude = ['pw'])
                detail_list.append(reply)
        except ObjectDoesNotExist as e:
            print str(e)
            
        return detail_list

    @staticmethod
    def get_attach_file(ref_id, file_type):
        """
        @summary: 첨부 파일  조회
        @author: hsrjmk
        @param ref_id: 참조 ID key
        @param file_type:   ('LB-P', 'Language Board Post'),
                            ('TP-Q', 'Translation and Proofreading Question'),
                            ('TP-A', 'Translation and Proofreading Answer'),
                            ('LQ-Q', 'Language Questions Question'),
                            ('LQ-A', 'Language Questions Answer'),
                            ('FAQ', 'FAQ'),
                            ('QNA_Q', 'QNA Question'),
                            ('QNA_A', 'QNA Answer'),
                            ('QNA_R', 'QNA Reply'),
                            ('PROFILE', 'Large'),
        @return: none
        """
        try:
            file_obj = File.objects.get(ref_id=ref_id, type=file_type)
            return {"filename" : file_obj.filename, "filepath" : file_obj.filepath}
        except:
            return None

    @staticmethod
    def save_help_qna(category, title, content, attached_file, ref_id, user_id):
        """
        @summary: 질문을 저장하는 함수
        @author: hsrjmk
        @param category: 질문 카테고리(Category ID 1 : Profile & Account 2 : Tutoring 3 : Taking lessons 4 : Payment 5 : Withdrawal 6 : Tellpin Player 7 : Tellpin Search 8 : Community 9 : Reporting about someone 10 : Others)
        @param title: 질문 제목
        @param content: 질문 내용
        @param attached_file: 파일 첨부
        @param ref_id: 참조 id
        @param user_id: 사용자 id
        @return: url : 내가 작성한 질문의 상세보기 화면(/help/qna/'+str(my_qna.qna_ref_id)+'/')
        """
        try:
            user = TellpinUser.objects.get(user_id=user_id)
            if ref_id != 'undefined':
                question = HelpQuestion.objects.select_related('user').get(id=ref_id)
                reply = HelpReply(
                    contents=content,
                    user=user,
                    question=question,
                    answer=None,
                    fid=SupportService.upload_file(attached_file, 'QNA_R', ref_id),
                )
                reply.save()
            else:
                question = HelpQuestion(
                    question_category=category,
                    title=title,
                    contents=content,
                    user=user,
                    status=1,
                )
                question.save()

                question.fid = SupportService.upload_file(attached_file, 'QNA_Q', question.id)
                question.save()
                ref_id = question.id
        except Exception as err:
            print err
            return '/help/qna/'

        return '/help/qna/'+str(ref_id)+'/'

    @staticmethod
    def upload_file(attached_file, file_type, ref_id):
        """
        @summary: 파일 첨부
        @author: hsrjmk
        @param attached_file: 파일 첨부
        @param file_type: 참조할 테이블 유형 'LQ' : LQ_QUESTION 'LA' : LQ_ANSWER 'TQ' : TP_QUESTION 'TA' : TP_ANSWER 'LC' : LC_POST 'HF' : HELP_FAQ_ML 'TT' : Tutor Profile Resume 'AP' : Tutor Applications 'HQ' : HELP_QNA
        @param ref_id: 참조 ID key
        @return: 파일 id
        """
        if attached_file is None:
            return None
        
        try:
            file_name = "%d&&%s" % (int(round(time.time() * 1000)), attached_file.name)
            file_path = '%s/%s' % ("/static/img/upload", file_name)
            full_path = '%s%s' % (BASE_DIR, file_path)
            with open(full_path,'wb') as fp:
                for chunk in attached_file.chunks():
                    fp.write(chunk)
        except Exception as err:
            print str(err)
        
        try:
            imgfile = File.objects.get(ref_id=ref_id, type=file_type)
            removefile = '%s%s' % (BASE_DIR, imgfile.filepath)
            os.remove(removefile)
        except IOError as err:
            print str(err)
            return None
        except:
            imgfile = File()

        imgfile.type = file_type
        imgfile.filename = file_name
        imgfile.filepath = file_path
        imgfile.ref_id = ref_id
        imgfile.save()

        return imgfile.fid

    @staticmethod
    def get_qna(question_id):
        """
        @summary: 내가 작성한 질문을 가져오는 함수
        @author: hsrjmk
        @param question_id : 질문 id
        @return: 작성한 질문
        """
        try:
            question_obj = HelpQuestion.objects.get(id=question_id)
            question = model_to_dict(question_obj)
            if HelpReply.objects.filter(question=question_id).exists():
                question_reply_obj = HelpReply.objects.get(question=question_id)
                question['reply'] = model_to_dict(question_reply_obj)

            #attach file
            question["attach_file"] = SupportService.get_attach_file(question_id, 'QNA_Q')
        except ObjectDoesNotExist as e:
            print str(e)
            return None

        return question

    @staticmethod
    def update_help_question(category, title, content, attached_file, question_id, user_id, keep):
        """
        @summary: 질문을 업데이트하는 함수
        @author: hsrjmk
        @param category : 질문 카테고리(Category ID 1 : Profile & Account 2 : Tutoring 3 : Taking lessons 4 : Payment 5 : Withdrawal 6 : Tellpin Player 7 : Tellpin Search 8 : Community 9 : Reporting about someone 10 : Others)
        @param title : 질문 제목
        @param content : 질문 내용
        @param attached_file : 파일 첨부
        @param question_id : 참조 id
        @param keep : 파일 유지 여부
        @return: url : 내가 작성한 질문의 상세보기 화면(/help/qna/'+str(my_qna.qna_ref_id)+'/')
        """
        try:
            question_obj = HelpQuestion.objects.get(id=question_id, user_id=user_id)
            question_obj.question_category = category
            question_obj.title = title
            question_obj.contents = content

            if attached_file is not None:
                fid = SupportService.upload_file(attached_file, 'QNA_Q', question_id)
                question_obj.fid = fid
            else:
                if keep == 'false':
                    SupportService.delete_file(question_id, 'QNA_Q')

            question_obj.save()
        except ObjectDoesNotExist as e:
            print str(e)
            return '/help/qna/'

        return '/help/qna/'+str(question_obj.id)+'/'

    @staticmethod
    def update_help_reply(content, attached_file, reply_id, user_id, keep):
        """
        @summary: 추가 질문을 업데이트하는 함수
        @author: hsrjmk
        @param content : 질문 내용
        @param attached_file : 파일 첨부
        @param reply_id : 참조 id
        @param user_id : 사용자 id
        @param keep : 파일 유지 여부
        @return: url : 내가 작성한 질문의 상세보기 화면(/help/qna/'+str(my_qna.qna_ref_id)+'/')
        """
        try:
            reply_obj = HelpReply.objects.get(id=reply_id, user_id=user_id)
            reply_obj.contents = content

            if attached_file is not None:
                fid = SupportService.upload_file(attached_file, 'QNA_R', reply_id)
                reply_obj.fid = fid
            else:
                if keep == 'false':
                    SupportService.delete_file(reply_id, 'QNA_R')

            reply_obj.save()
        except ObjectDoesNotExist as e:
            print str(e)
            return '/help/qna/'

        return '/help/qna/'+str(reply_obj.question_id)+'/'

    @staticmethod
    def delete_file(ref_id, file_type):
        """
        @summary: 내가 작성한 질문의 파일을 삭제하는 함수
        @author: hsrjmk
        @param ref_id: 참조 ID key
        @param file_type: 참조할 테이블 유형 'LQ' : LQ_QUESTION 'LA' : LQ_ANSWER 'TQ' : TP_QUESTION 'TA' : TP_ANSWER 'LC' : LC_POST 'HF' : HELP_FAQ_ML 'TT' : Tutor Profile Resume 'AP' : Tutor Applications 'HQ' : HELP_QNA
        @return: true, false
        """
        try:
            imgfile = File.objects.get(ref_id=ref_id, type=file_type)
            removefile = '%s%s' % (BASE_DIR, imgfile.filepath)
            imgfile.delete()
            os.remove(removefile)
        except OSError as err:
            print str(err)
            return False

        return True

    @staticmethod
    def delete_help_question(question_id):
        """
        @summary: 작성한 질문을 삭제하는 함수
        @author: hsrjmk
        @param qna_id : 질문 id
        @return: url : /help/qna/'
        """
        try:
            question = HelpQuestion.objects.get(id=question_id)
            question.del_by_user = True
            question.save()
        except ObjectDoesNotExist as e:
            print str(e)
        
        return '/help/qna/'
    
    @staticmethod
    def delete_help_reply(reply_id):
        """
        @summary: 작성한 질문의 답글을 삭제하는 함수
        @author: hsrjmk
        @param reply_id : 질문 id
        @return: url : /help/qna/'
        """
        try:
            reply = HelpReply.objects.get(id=reply_id)
            reply.del_by_user = True
            reply.save()
        except ObjectDoesNotExist as e:
            print str(e)
            return '/help/qna/'
        
        return '/help/qna/'+str(reply.question_id)+'/'

    @staticmethod
    def search_faq(search_word, lang_id):
        """
        @summary: 특정 단어로 질문을 찾는 함수
        @author: sj746
        @param search_word: 찾을 단어
        @param lang_id: 다국어 1: English 2: Korean
        @return: search_list
        """
#         sql = "SELECT b.*, a.topic_id, a.sub_topic_id FROM help_faq a INNER JOIN help_faq_ml b ON a.faq_id = b.faq_id WHERE b.ml = %s AND (b.title LIKE %s OR b.contents LIKE %s) ORDER BY b.created_time DESC;"
        queryset = Q(multilang_id=lang_id) & Q(title__contains=search_word) | Q(contents__contains=search_word)
        faq_ml_objs = HelpFAQMultiLang.objects.filter(queryset)\
                                                    .select_related('faq', 'multilang')\
                                                    .order_by('-created_time')
        search_faq_list = []
        for faq_ml_obj in faq_ml_objs:
            question = model_to_dict(faq_ml_obj, exclude=['id', 'faq', 'multilang', 'del_by_admin'])
            question['ml_id'] = faq_ml_obj.id
            question['ml'] = faq_ml_obj.multilang_id
            
            question['faq_id'] = faq_ml_obj.faq.id
            if faq_ml_obj.faq.sub_topic_id:
                question['sub_topic_id'] = faq_ml_obj.faq.sub_topic_id
                question['sub_topic_name'] = faq_ml_obj.faq.sub_topic
            question['created_time'] = faq_ml_obj.faq.created_time
            
            search_faq_list.append(question)
        return search_faq_list
