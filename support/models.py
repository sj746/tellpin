# -*- coding: utf-8 -*-
###############################################################################
# filename    : support > models.py
# description : Support 모델 정의
# author      : hsrjmk@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160922 hsrjmk 최조 작성
#
###############################################################################
from django.db import models
from common.models import LanguageInfo
from user_auth.models import TellpinUser, File


TOPIC = (
    (1, 'Guide & Announcements'),
    (2, 'Profile & Account'),
    (3, 'For (Wannabe) Tutors'),
    (4, 'For Tutees'),
    (5, 'Tellpin Player'),
    (6, 'Tellpin Search'),
    (7, 'Community'),
)
SUB_TOPIC = (
    (0, 'None'),
    (1, 'My Profile'),
    (2, 'Password'),
    (3, 'Privacy'),
)
QUESTION_STATUS = (
    (1, 'Open'),
    (2, 'Waiting'),
    (3, 'Closed'),
)
QUESTION_CATEGORY = (
    (1, 'Profile & Account'),
    (2, 'Tutoring'),
    (3, 'Taking lessons'),
    (4, 'Payment'),
    (5, 'Withdrawal'),
    (6, 'Tellpin Player'),
    (7, 'Tellpin Search'),
    (8, 'Community'),
    (9, 'Reporting about someone'),
    (10, 'Others'),
)


class HelpFAQ(models.Model):
    admin = models.ForeignKey(TellpinUser)

    topic_id = models.IntegerField("Topic id", choices=TOPIC, null=True)
    topic = models.CharField("Topic", max_length=1000)
    sub_topic_id = models.IntegerField("Sub topic id", choices=SUB_TOPIC, null=True)
    sub_topic = models.CharField("Subtopic", max_length=1000)
    top_question = models.IntegerField("Top question order", null=True)
    view_count = models.IntegerField("View count", default=0)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_admin = models.NullBooleanField("Deleted by administrator", default=False, null=True)

    class Meta:
        db_table = u'help_faq'


class HelpFAQMultiLang(models.Model):
    faq = models.ForeignKey('HelpFAQ')
    
    multilang = models.ForeignKey(LanguageInfo)
    title = models.CharField("FAQ title", max_length=150)
    contents = models.CharField("FAQ contents", max_length=5000, null=True)
    fid = models.CharField("File ids", max_length=500, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_admin = models.NullBooleanField("Deleted by administrator", default=False, null=True)

    class Meta:
        db_table = u'help_faq_multilang'


class HelpQuestion(models.Model):
    user = models.ForeignKey(TellpinUser)
    status = models.PositiveSmallIntegerField("Question status", choices=QUESTION_STATUS, default=1)
    question_category = models.PositiveSmallIntegerField("Question category", choices=QUESTION_CATEGORY, default=1)
    title = models.CharField("Question title", max_length=150)
    contents = models.CharField("Question contents", max_length=5000)
    fid = models.CharField("File ids", max_length=500, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'help_question'


class HelpAnswer(models.Model):
    question = models.ForeignKey('HelpQuestion')
    
    admin = models.ForeignKey(TellpinUser)
    contents = models.CharField("Answer contents", max_length=5000)
    fid = models.CharField("File ids", max_length=500, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'help_answer'


class HelpReply(models.Model):
    question = models.ForeignKey('HelpQuestion', null=True)
    answer = models.ForeignKey('HelpAnswer', null=True)
    
    user = models.ForeignKey(TellpinUser)
    contents = models.CharField("Reply contents", max_length=5000)
    fid = models.CharField("File ids", max_length=500, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'help_reply'
