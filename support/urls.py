from django.conf.urls import url, include

from support import views


urlpatterns = [
               
    url( r'^qna/ff/$', views.help_qna_delete_attach ),           
    url(r'^$', views.help_center),
    url(r'^ml/$', views.help_change_ml),
    url(r'^search/$', views.help_search),
    url( r'^faq/(?P<ml_id>\d+)/$', views.help_center_detail), 
    url( r'^faq/(?P<sub_topic>\D+)/$', views.help_center_sub_list),
    url( r'^qna/$', views.help_my_questions ),
    url( r'^qna/status/$', views.help_my_questions_status ),
    url( r'^qna/write/$', views.help_qna_write),
    url( r'^qna/write/followup/$', views.help_qna_write_follow),
    url( r'^qna/edit/$', views.help_qna_edit),
    url( r'^qna/update/$', views.help_qna_update),
    url( r'^qna/save/$', views.help_qna_save),
    url( r'^qna/delete/$', views.help_qna_delete),
    url( r'^(?P<topic>\D+)/$', views.help_center_list ), 
    url( r'^qna/(?P<qna_id>\d+)/$', views.help_qna_detail ),
   
    
   
]