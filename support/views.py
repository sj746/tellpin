# -*- coding: utf-8 -*-

###############################################################################
# filename    : support > views.py
# description : 헬프 센터 화면 구성을 위해 필요한 함수를 위한 views file
# author      : sj746@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 sj746 최초 작성
#
#
###############################################################################


import json

from django.core.serializers.json import DjangoJSONEncoder
from django.db.models.signals import post_init
from django.http.response import HttpResponse, JsonResponse, \
    HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt

from common.common import tellpin_login_required
from support.service import SupportService
from tutor.service import TutorService


# Create your views here.
def help_center(request):
    """
    @summary: 헬프 센터 진입 url
    @author: sj746
    @param request
    @return: url : help_center/help_center.html
    """
    resData = {}
    resData['isSuccess'] = 1

    if "ml" in request.session.keys():
        ml = request.session.get("ml")
        # TODO: 다국어에 맞는 Language id로 변환
        if ml == 2:
            lang_id = 1
        else:
            lang_id = 2
    else:
        lang_id = 2

    resData['top_qeustions'] = SupportService.get_top_questions(lang_id)
    resData['gnas'] = SupportService.get_gna(lang_id)
    resData['pnas'] = SupportService.get_pna(lang_id)
    resData['forTutors'] = SupportService.get_for_tutors(lang_id)
    resData['forTutees'] = SupportService.get_for_tutees(lang_id)
    resData['tps'] = SupportService.get_tps(lang_id)
    resData['tss'] = SupportService.get_tss(lang_id)
    resData['communities'] = SupportService.get_communities(lang_id)
    json_data = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("help_center/help_center.html", {"data": json_data}, RequestContext(request))


def help_center_list(request, topic):
    """
    @summary: 헬프 센터 각 토픽에 대한 리스트 구성 url
    @author: sj746
    @param request
    @param topic :  Topic ID
                    1 : Guide & Announcements 2 : Profile & Account 3 : For (Wannabe) Tutors
                    4 : For Tutees 5 : Tellpin Player 6 : Tellpin Search 7 : Community
    @return: url : help_center/help_center_list.html
    """
    resData = {}
    resData['isSuccess'] = 1

    if "ml" in request.session.keys():
        ml = request.session.get("ml")
        # TODO: 다국어에 맞는 Language id로 변환
        if ml == 2:
            lang_id = 1
        else:
            lang_id = 2
    else:
        lang_id = 2

    if topic == "gna":
        resData["topic_name"] = "Guide & Announcements"
        resData['topic_list'] = SupportService.get_gna(lang_id)
    elif topic == "pna":
        resData["topic_name"] = "Profile & Account"
        resData['topic_list'] = SupportService.get_pna(lang_id)
    elif topic == "tutor":
        resData["topic_name"] = "For (Wannabe)Tutors"
        resData['topic_list'] = SupportService.get_for_tutors(lang_id)
    elif topic == "tutee":
        resData["topic_name"] = "For Tutees"
        resData['topic_list'] = SupportService.get_for_tutees(lang_id)
    elif topic == "tellpinplayer":
        resData["topic_name"] = "Tellpin Player"
        resData['topic_list'] = SupportService.get_tps(lang_id)
    elif topic == "tellpinsearch":
        resData["topic_name"] = "Tellpin Search"
        resData['topic_list'] = SupportService.get_communities(lang_id)
    elif topic == "community":
        resData["topic_name"] = "Community"
        resData['topic_list'] = SupportService.get_tss(lang_id)

    json_data = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("help_center/help_center_list.html", {"data": json_data}, RequestContext(request))


def help_center_detail(request, ml_id):
    """
    @summary: 헬프 센터 개별 항목의 상세보기 진입 url
    @author: sj746
    @param request
    @param ml_id : 다국어: 1  English 2: Korean
    @return: url : help_center/help_center_detail.html
    """
    resData = {}
    resData['isSuccess'] = 1
    
    resData['faq'] = SupportService.get_faq(ml_id)
    json_data = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("help_center/help_center_detail.html", {"data": json_data}, RequestContext(request))


def help_center_sub_list(request, sub_topic):
    """
    @summary: 헬프 센터 각 서브 항목에 대한 리스트 구성 url
    @author: sj746
    @param request
    @param sub_topic :  Sub-topic ID
                        0 : 없음 Topic 2의 경우
                        1 : My Profile  2 : Password  3 : Privacy
    @return: url : support/help_center_sub.html
    """
    resData = {}
    resData['isSuccess'] = 1
    
    if "ml" in request.session.keys():
        ml = request.session.get("ml")
        # TODO: 다국어에 맞는 Language id로 변환
        if ml == 2:
            lang_id = 1
        else:
            lang_id = 2
    else:
        lang_id = 2

    if sub_topic == 'myprofile':
        resData['sub_topic_name'] = "My Profile"
        resData['topic_list'] = SupportService.get_sub_topic(lang_id, 1)
    elif sub_topic == 'password':
        resData['sub_topic_name'] = "Password"
        resData['topic_list'] = SupportService.get_sub_topic(lang_id, 2)
    elif sub_topic == 'privacy':
        resData['sub_topic_name'] = "Privacy"
        resData['topic_list'] = SupportService.get_sub_topic(lang_id, 3)
    json_data = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("help_center/help_center_sub.html", {"data": json_data}, RequestContext(request))


@tellpin_login_required
def help_my_questions(request):
    """
    @summary: 내가 작성한 질문 리스트 화면 진입 url
    @author: sj746
    @param request
    @return: url : help_center/help_center_qna.html
    """

    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    user_utc = request.session["utc_delta"]
    resData["my_questions"] = SupportService.get_my_questions(user_id, 0, user_utc)
    json_data = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("help_center/help_center_qna.html", {"data": json_data}, RequestContext(request))


def help_qna_detail(request, qna_id):
    """
    @summary: 특정 질문의 상세 보기 화면 진입 url
    @author: sj746
    @param request
    @param qna_id : 질문 id
    @return: url : help_center/help_qna_detail.html
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    user_utc = request.session["utc_delta"]
    resData["my_question"] = SupportService.get_my_question_detail(qna_id, user_utc)
    resData["user_id"] = user_id
    json_data = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("help_center/help_qna_detail.html", {"data": json_data}, RequestContext(request))


@tellpin_login_required
def help_qna_write(request):
    """
    @summary: 질문 작성 화면 진입 url
    @author: sj746
    @param request
    @return: url : help_center/help_qna_write.html
    """
    resData = {}
    resData['isSuccess'] = 1

    return render_to_response("help_center/help_qna_write.html", RequestContext(request))


@csrf_exempt
@tellpin_login_required
def help_qna_save(request):
    """
    @summary: 질문  저장하는 함수
    @author: sj746
    @param request
    @return: url : help_center/help_qna_write.html
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id

    if "file" in request.FILES.keys():
        attached_file = request.FILES["file"]
    else:
        attached_file = None

    resData["qnaURL"] = SupportService.save_help_qna(request.POST["category"], request.POST["title"], request.POST["content"],
                                         attached_file, request.POST["ref_id"], user_id)
    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def help_qna_write_follow(request):
    """
    @summary: 질문  작성하는 화면 진입 url
    @author: sj746
    @param request
    @return: url : help_center/help_qna_write.html
    """
    resData = {}
    resData['isSuccess'] = 1
    resData["ref_id"] = request.POST["ref_id"]
    json_data = json.dumps(resData)

    return render_to_response("help_center/help_qna_write.html", {"data": json_data}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def help_qna_edit(request):
    """
    @summary: 작성한 질문 편집하는 화면 진입 url
    @author: sj746
    @param request
    @return: url : help_center/help_qna_write.html
    """
    resData = {}
    resData['isSuccess'] = 1

    if "qna_id" not in request.POST.keys():
        return HttpResponseRedirect("/help/qna/")

    resData["qna_id"] = request.POST["qna_id"]
    resData["qna"] = SupportService.get_qna(resData["qna_id"])
    json_data = json.dumps(resData)

    return render_to_response("help_center/help_qna_write.html", {"data": json_data}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def help_qna_update(request):
    """
    @summary: 저장된 질문을 업데이트 함수
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    if "file" in request.FILES.keys():
        attached_file = request.FILES["file"]
    else:
        attached_file = None

    if post_dict['ref_id']:
        resData["qnaURL"] = SupportService.update_help_reply(request.POST["content"], attached_file, request.POST["qnaID"], user_id, request.POST["keep"])
    else:
        resData["qnaURL"] = SupportService.update_help_question(request.POST["category"], request.POST["title"], request.POST["content"],
                                           attached_file, request.POST["qnaID"], user_id, request.POST["keep"])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def help_qna_delete(request):
    """
    @summary: 내가 작성한 질문을 삭제하는 함수
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData['isSuccess'] = 1
    post_dict = json.loads(request.body)
    if post_dict['ref_id']:
        resData["qnaURL"] = SupportService.delete_help_reply(post_dict["qnaID"])
    else:
        resData["qnaURL"] = SupportService.delete_help_question(post_dict["qnaID"])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def help_qna_delete_attach(request):
    """
    @summary: 내가 작성한 질문을 삭제하는 함수
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData['isSuccess'] = 1
    
    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def help_my_questions_status(request):
    """
    @summary: 내가 작성한 질문의 상태를 확인하는 함수
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    user_utc = request.session["utc_delta"]
    post_dict = json.loads(request.body)
    resData["my_questions"] = SupportService.get_my_questions(user_id, post_dict["status"], user_utc)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def help_change_ml(request):
    """
    @summary: 멀티 언어 변경하는 함수
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData['isSuccess'] = 1

    post_dict = json.loads(request.body)
    request.session["ml"] = post_dict["ml"]
    resData['url'] = '/help/'
    return JsonResponse(resData)


@csrf_exempt
def help_search(request):
    """
    @summary: 특정 단어로 검색되는 리스트 구성 url
    @author: sj746
    @param request
    @return: url : help_center/help_center_search.html
    """
    resData = {}
    resData['isSuccess'] = 1

    if "ml" in request.session.keys():
        ml = request.session.get("ml")
        # TODO: 다국어에 맞는 Language id로 변환
        if ml == 2:
            lang_id = 1
        else:
            lang_id = 2
    else:
        lang_id = 2

    if request.POST.get("searchWord", False):
        searchword = request.POST["searchWord"]
    else:
        searchword = ""

    resData["searchWord"] = searchword
    resData["faqs"] = SupportService.search_faq(searchword, lang_id)
    json_data = json.dumps(resData, cls=DjangoJSONEncoder)

    return render_to_response("help_center/help_center_search.html", {"data": json_data}, RequestContext(request))
