# -*- encoding: utf-8 -*-
import json
import logging
import time
from datetime import datetime, timedelta

from django.core.serializers.json import DjangoJSONEncoder
from django.http.response import HttpResponse, JsonResponse, \
    HttpResponseRedirect
from django.shortcuts import render, render_to_response, redirect
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict
from user_auth.forms import UserLoginForm
from common.models import LanguageInfo
from user_auth.service import UserAuthService
from django.template.context_processors import request
from service import TellpinAdminService
from tutor.models import TutorDoc, Tutee, UserViolation
from django.core.paginator import Paginator
from common.service import CommonService
from common.common import isTutor, tellpin_login_required
from tutor.service import TutorService
from user_auth.models import TellpinUser, File
from tutoring.models import LessonClaim, LessonMessage
from tutoring.service import ReservationService
from community.service import Community
from community.service import LB, LQ, TP
import tellpin
from support.models import HelpQuestion, HelpAnswer


def tellpin_admin(request):

    print 'admin_page()'

    obj = []
    try:
        if request.session['email']:
            level = request.session['level']

        form = UserLoginForm()
        language = LanguageInfo.objects.filter()
        return render_to_response("tellpin_admin/login_page.html", {'form': form, 'languages': language},
                                  RequestContext(request))
    except Exception as e:
        print e
        form = UserLoginForm()
        language = LanguageInfo.objects.filter()
        return render_to_response("tellpin_admin/login_page.html", {'form': form, 'languages': language},
                                  RequestContext(request))


@csrf_exempt
def ajax_login(request):
    """
    @summary: 로그인 validation check 함수
    @author: khyun
    @param none
    @return: json
    """
    # TODO
    # 패스워드 체크 (유효성없음, 클라이언트 동시 체크)
    print "ajax_login"
    # TODO
    # 일반회원 로그인 구현.
    try:
        email = request.POST['email']
        pwd = request.POST['pwd']

        form = UserLoginForm()
        # email check
        user = UserAuthService.check_user(email, pwd)
        # 이메일 회원 존재
        if user is not None:
            resData = dict()
            if user.is_admin:
                request.session['is_admin'] = 1
                request.session['email'] = email
                resData["success"] = True
            else:
                resData["success"] = False
                resData["message"] = "The account is not an administrator."
            jsonData = json.dumps(resData)

            return HttpResponse(jsonData, RequestContext(request))
                # 권한 여부
        else:
            # 이메일 존재안함.
            print "email invalid"
            message = "You entered an invalid email or password"

            resData = {}
            resData["success"] = False
            resData["message"] = message
            jsonData = json.dumps(resData)

            return HttpResponse(jsonData, RequestContext(request))
            # return render_to_response('user_auth/landingpage.html', {'message':message, 'form':form}, RequestContext(request))
            # return render_to_response('이메일 없음 (페이지구현 or 안내말 ). 로그인 실패')
            # return render_to_response('user_auth/login_form.html', {'message': message, 'form': form, 'languages':language}, RequestContext(request))

    except Exception as e:
        print 'exception'
        print e
        message = "login error, please ask to administrator"
        # return HttpResponse("Login error 관리자에게 문의하세요")
        resData = {}
        resData["success"] = False
        resData["message"] = message
        jsonData = json.dumps(resData)

        return HttpResponse(jsonData, RequestContext(request))


def admin_home(request):

    try:
        request.session['email']
        if request.session['email'] and request.session['is_admin']:
            print "is admin"
            return render_to_response("tellpin_admin/admin_home.html",RequestContext(request))

    except Exception as e:
        print 'exception'
        print e
        form = UserLoginForm()
        language = LanguageInfo.objects.filter()
        return render_to_response("tellpin_admin/login_page.html", {'form': form, 'languages': language}, RequestContext(request))


def login_admin(request):
    """
    @summary: 로그인 함수
    @author: khyun
    @param request
    @return: url - /admin_home/
    """
    print "admin___login"
    # TODO
    # 일반회원 로그인 구현.
    email = request.POST['email']
    pwd = request.POST['pwd']
    message = "아이디 또는 비밀번호를 확인해 주세요."
    form = UserLoginForm()

    user = UserAuthService.check_user(email, pwd)
    return redirect('/admin_home/')


@csrf_exempt
def ajax_tutor_apply(request):
    """
    @summary: 튜터 신청 관리
    @author: shkim
    @param: request
    """
    print "tutor_apply"
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    tutorlist = []
    post_dict = json.loads(request.body)
    resData = TellpinAdminService.getTutorApplications(post_dict['page'])
    return JsonResponse(resData)


@csrf_exempt
def ajax_tutor_modify(request):
    """
    @summary: 튜터 변경 관리
    @author: shkim
    @param: reqeust

    """
    print "tutor modify"
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)
    resData = TellpinAdminService.getTutorModifications(post_dict['page'])
    return JsonResponse(resData)


@csrf_exempt
def ajax_post_list(request):
    """
    @summary: 튜터 변경 관리
    @author: shkim
    @param: reqeust

    """
    print "ajax_post_list"
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)
    resData = TellpinAdminService.getPostClaims(post_dict['page'])
    return JsonResponse(resData)


@csrf_exempt
def ajax_transaction_list(request):
    """
    @summary: 튜터 변경 관리
    @author: shkim
    @param: reqeust

    """
    print "ajax_transaction_list"
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)
    resData = TellpinAdminService.getTransactionClaims(post_dict['page'])
    return JsonResponse(resData)


@csrf_exempt
def ajax_help_list(request):
    """
    @summary: 튜터 변경 관리
    @author: shkim
    @param: reqeust

    """
    print "ajax_help_list"
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)
    resData = TellpinAdminService.getHelpClaims(post_dict['page'])
    return JsonResponse(resData)


@csrf_exempt
def ajax_lesson_claim(request):
    """
    @summary: 튜터 변경 관리
    @author: shkim
    @param: reqeust

    """
    print "ajax_lesson_claim"
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)
    resData = TellpinAdminService.getLessonClaims(post_dict['page'])
    return JsonResponse(resData)


@csrf_exempt
def ajax_user_violation(request):
    """
    @summary: 튜터 변경 관리
    @author: shkim
    @param: reqeust

    """
    print "ajax_lesson_claim"
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)
    resData = TellpinAdminService.getUserViolations(post_dict['page'])
    return JsonResponse(resData)


@csrf_exempt
def ajax_post_claim(request):
    """
    @summary: 튜터 변경 관리
    @author: shkim
    @param: reqeust

    """
    print "ajax_post_claim"
    resData = {}
    post_dict = json.loads(request.body)
    resData = TellpinAdminService.getPostClaims(post_dict['page'])
    resData["isSuccess"] = 1
    return JsonResponse(resData)


@csrf_exempt
def ajax_approve_changes(request):
    """
    @summary: 튜터 변경된 부분 적용
    @author: shkim
    @param: request
    """
    print "approve changes"
    resData = {}
    post_dict = json.loads(request.body)
    resData = TellpinAdminService.approveChanges(post_dict["data"])
    resData["isSuccess"] = 1
    return JsonResponse(resData)


@csrf_exempt
def ajax_decline_changes(request):
    """
    @summary: 튜터 변경된 부분 거절
    @author: shkim
    @param: request
    """
    print "decline changes"
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)
    resData = TellpinAdminService.declineChanges(post_dict["data"])
    return JsonResponse(resData)

@csrf_exempt
def ajax_confirm_tutor(request):
    """
    @summary: 튜터 변경된 부분 적용
    @author: shkim
    @param: request
    """
    print "confirm tutor"
    resData = {}
    post_dict = json.loads(request.body)
    resData = TellpinAdminService.confirmTutor(post_dict["data"])
    resData["isSuccess"] = 1
    return JsonResponse(resData)

@csrf_exempt
def ajax_tutorProfile(request, tutor_id):
    """
    @summary: tutor profile edit
    @author: msjang
    @param request
    @return: url - tutor/tutor_edit_profile.html or url - tutor/tutee_edit_profile.html
    """
    print "tutor profile"
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    resData2 = {}
    print tutor_id
    resData2['page'] = 'myprofile'
    return render_to_response('tellpin_admin/tutor_detail.html', {"tutor_id": tutor_id}, RequestContext(request))

@csrf_exempt
def ajax_tutorAppProfile(request, tutor_id):
    """
    @summary: tutor profile edit
    @author: msjang
    @param request
    @return: url - tutor/tutor_edit_profile.html or url - tutor/tutee_edit_profile.html
    """
    print "tutor app profile"
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    resData2 = {}
    print tutor_id
    resData2['page'] = 'myprofile'
    return render_to_response('tellpin_admin/tutor_app_detail.html', {"tutor_id": tutor_id}, RequestContext(request))

@csrf_exempt
def ajax_claimProfile(request, reservation_id):
    """
    @summary: tutor profile edit
    @author: msjang
    @param request
    @return: url - tutor/tutor_edit_profile.html or url - tutor/tutee_edit_profile.html
    """
    resData = {}
    resData['isSuccess'] = 1
    print "getting claim profile for lesson " + str(reservation_id)
    return render_to_response('tellpin_admin/lesson_claim_detail.html', {"reservation_id": reservation_id}, RequestContext(request))


@csrf_exempt
def ajax_helpProfile(request, help_id):
    """
    @summary: tutor profile edit
    @author: msjang
    @param request
    @return: url - tutor/tutor_edit_profile.html or url - tutor/tutee_edit_profile.html
    """
    print "help profile"
    resData = {}
    resData['isSuccess'] = 1
    print help_id
    return render_to_response('tellpin_admin/help_detail.html', {"help_id": help_id}, RequestContext(request))


@csrf_exempt
def ajax_violationProfile(request, user_id):
    """
    @summary: tutor profile edit
    @author: msjang
    @param request
    @return: url - tutor/tutor_edit_profile.html or url - tutor/tutee_edit_profile.html
    """
    print "help profile"
    resData = {}
    resData['isSuccess'] = 1
    return render_to_response('tellpin_admin/user_violation.html', {"user_id": user_id}, RequestContext(request))


@csrf_exempt
def ajax_lessonClaimProfile(request):
    """
    @summary: 사용자 정보를 편집하는데 필요한 데이터를 구성하는 함수(에디트 화면 진입 시 많은 시간이 소요되므로 html 파일만 로딩 후 추후에 데이터를 구성한다)
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData['isSuccess'] = 1
    post_dict = json.loads(request.body)

    utc_time = request.session.get('utc_delta', 0)
    reservation_id = post_dict['reservation_id']
    try:
        # need to give a list of problems
        problems = TellpinAdminService.get_problems_details_list(reservation_id)
        
        problem_list = problems['list']
        
        for lc in problem_list:
            reqString = ''
            if lc['resolve_type'] in (4, 8):
                reqString = "Return all Tellpin Credits of the lesson fee to the tutee"
            elif lc['resolve_type'] in (6, 9):
                reqString = "Transfer all Tellpin Credits of the lesson fee to the tutor"
            elif lc['resolve_type'] is 5:
                reqString = "Reschedule the lesson"
            elif lc['resolve_type'] is 7:
                reqString = "Return partial Tellpin Credits of the lesson fee to the tutee"
            lc["request"] = reqString
            
        resData["problems"] = problems
        #resData["message"] = message_obj
        print resData
        return JsonResponse(resData)
    except Exception as e:
        print 'ajax_lessonClaimProfile error'
        print e

        return JsonResponse(e)


@csrf_exempt
def ajax_helpClaimProfile(request):

    resData = {}
    resData['isSuccess'] = 1
    post_dict = json.loads(request.body)

    utc_time = request.session.get('utc_delta', 0)
    help_id = post_dict['help_id']
    # need to get the lesson claim obj so that the page can be populated with the data
    help_obj = HelpQuestion.objects.select_related('user').get(id = help_id)
    file_obj = File.objects.filter(fid = help_obj.fid)
    print file_obj
    try:
        #resData["message"] = message_obj
        resData["qid"] = help_obj.id
        resData["from"] = help_obj.user.name
        resData["about"] = help_obj.question_category
        resData["title"] = help_obj.title
        resData["description"] = help_obj.contents
        if file_obj:
            resData["fid"] = help_obj.fid
            resData["filename"] = file_obj[0].filename
        return JsonResponse(resData)
    except Exception as e:
        print 'ajax_helpClaimProfile error'
        print e

        return JsonResponse(e)


@csrf_exempt
def ajax_helpAnswer(request):

    resData = {}
    resData['isSuccess'] = 1
    post_dict = json.loads(request.body)
    qid = post_dict['qid']
    answer = post_dict['answer']
    # need to put the content of the answer
    q_obj = HelpQuestion.objects.get(id = qid)
    q_obj.status = 3
    q_obj.save()
    HelpAnswer(contents = answer, question_id = qid, admin_id = 2).save()

    return JsonResponse(resData)


@csrf_exempt
def ajax_postClaimProfile(request):
    """
    @summary: 사용자 정보를 편집하는데 필요한 데이터를 구성하는 함수(에디트 화면 진입 시 많은 시간이 소요되므로 html 파일만 로딩 후 추후에 데이터를 구성한다)
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData['isSuccess'] = 1
    post_dict = json.loads(request.body)
    user_id = request.user.id
    utc_time = request.session.get('utc_delta', 0)
    type_id = post_dict['type_id'] # lb =1, lq =2, tp =3
    post_id = post_dict['post_id'] # id of the post
    if type_id is 1:
        user_id = LB.objects.get(id = post_id).user_id
        resData["post"] = Community.get_language_board_item(user_id, post_id)
        resData["post_violation"] = TellpinAdminService.get_lb_violation(post_id, user_id)
    elif type_id is 2:
        user_id = LQ.objects.get(id = post_id).user_id
        resData["post"] = Community.get_language_question_item(post_id, user_id)
        resData["post_violation"] = TellpinAdminService.get_lq_violation(post_id, user_id)
    elif type_id is 3:
        user_id = TP.objects.get(id = post_id).user_id
        resData["post"] = Community.get_translation_proofreading_item(post_id, user_id)
        resData["post_violation"] = TellpinAdminService.get_tp_violation(post_id, user_id)

    try:
        print resData["post"]
        return JsonResponse(resData)
    except Exception as e:
        print 'ajax_lessonClaimProfile error'
        print e

        return JsonResponse(e)


@csrf_exempt
def delete_lb_comment(request):
    """
    @summary: language board 댓글 삭제하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)

    resData["comments"] = TellpinAdminService.delete_language_board_comment(post_dict["uid"], post_dict["cid"])

    return JsonResponse(resData)


@csrf_exempt
def remove_lb(request):
    """
    @summary: language board 작성된 post을 삭제하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)
    TellpinAdminService.delete_language_board(post_dict["pid"])

    return JsonResponse(resData)


@csrf_exempt
def ajax_postProfile(request, type_id, post_id):
    """
    @summary: tutor profile edit
    @author: msjang 
    @param request
    @return: url - tutor/tutor_edit_profile.html or url - tutor/tutee_edit_profile.html
    """
    print "claim profile"
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    resData2 = {}
    return render_to_response('tellpin_admin/post_claim_detail.html', {"type_id": type_id, "post_id": post_id}, RequestContext(request))


@csrf_exempt
def ajax_getMessage(request):
    """
    @summary: get message list
    @author: msjang, chsin
    @param request
    @return: json
    """
    print 'ajax_getMessage()'
    resData = {}
    resData['isSuccess'] = 1
#    user_id = ts.get_user_id(request.session["email"])
    utc_time = request.session.get('utc_time', '+00:00')
    dict = json.loads(request.body)
    tutee_id = dict['tutee_id']
    tutor_id = dict['tutor_id']
    rvid = dict['rvid']
    resData['message'] = TutorService.getMessage(tutee_id, tutor_id, rvid, utc_time)

    return JsonResponse(resData)


@csrf_exempt
def ajax_resolveClaim(request):
    """
    @summary: get message list
    @author: msjang, chsin
    @param request
    @return: json
    """
    print 'ajax_getMessage()'
    resData = {}
    resData['isSuccess'] = 1
#    user_id = ts.get_user_id(request.session["email"])
    post_dict = json.loads(request.body)
    claim_id = post_dict['claim_id']
    confirm = post_dict['confirm']
    if "tc1" in post_dict:
        tutor_amount = post_dict['tc1']
        tutee_amount = post_dict['tc2']
        TellpinAdminService.resolveClaim(claim_id, confirm, tutor_amount, tutee_amount)
    else:
        TellpinAdminService.resolveClaim(claim_id, confirm)
    return JsonResponse(resData)


@csrf_exempt
def ajax_tutorEditProfile(request):
    """
    @summary: 사용자 정보를 편집하는데 필요한 데이터를 구성하는 함수(에디트 화면 진입 시 많은 시간이 소요되므로 html 파일만 로딩 후 추후에 데이터를 구성한다)
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)
    tutor_id = post_dict['tutor_id']
    is_Tutor = TellpinUser.objects.get(user_id = tutor_id).is_tutor
    print is_Tutor
    try:
        print "getting user info"
        resData['profile'] = TellpinAdminService.get_user_info(tutor_id, is_Tutor)
        resData['profile']['short_intro'] = TutorDoc.objects.get(user_id=tutor_id).short_intro
        resData['profile']['long_intro'] = TutorDoc.objects.get(user_id=tutor_id).long_intro
        resData['profile']['video_intro'] = TutorDoc.objects.get(user_id=tutor_id).video_intro
        resData['profile']['user_shortIntro'] = Tutee.objects.get(user_id=tutor_id).short_intro
        resData['profile']['user_longIntro'] = Tutee.objects.get(user_id=tutor_id).long_intro
        '''
        resData['video_intro'] = tutorIntro.video_intro
        profilePending['pending'] = tutorIntro.pending
        profilePending['p_quick_intro'] = tutorIntro.p_quick_intro
        profilePending['p_long_intro'] = tutorIntro.p_long_intro
        profilePending['p_video_intro'] = tutorIntro.p_video_intro
        resume_info = Resume.objects.get(user_id=user_id)
        resumePending["pending"] = resume_info.pending
        resumePending["p_education"] = resume_info.p_education
        resumePending["p_work_exp"] = resume_info.p_work_exp
        resumePending["p_certification"] = resume_info.p_certification
        resData['profilePending'] = profilePending
        resData['resumePending'] = resumePending

        if isTutor:
            if resume_info.fid:
                resume_info.fid = resume_info.fid.split(',')
                resume_info.fid = map(int, resume_info.fid)
                resumePending["fid"] = resume_info.fid
                while index < 9:
                    if resume_info.fid[index] != 0:
                        resumeFile["filename" + str(index)] = Files.objects.get(fid=resume_info.fid[index]).filename
                        index += 1
                    else:
                        resumeFile["filename" + str(index)] = "no file selected"
                        index += 1
            else:
                while index < 9:
                    resumeFile["filename" + str(index)] = "no file selected"
                    index += 1
            resData['resumePending']['filename'] = resumeFile
            '''

        resData['nationsInfo'] = CommonService.country_list()
        resData['languageInfo'] = CommonService.language_list()
        resData['skillLevelInfo'] = CommonService.skill_level_list()
        resData['timezoneInfo'] = CommonService.timezone_list()
        resData['tools'] = CommonService.tool_list()
        resData['currencyInfo'] = CommonService.currency_list()

        return JsonResponse(resData)
    except Exception as e:
        print 'ajax_tutorEditProfile error'
        print e

        return JsonResponse(e)

@csrf_exempt
def ajax_tutorApplyProfile(request):
    """
    @summary: 사용자 정보를 편집하는데 필요한 데이터를 구성하는 함수(에디트 화면 진입 시 많은 시간이 소요되므로 html 파일만 로딩 후 추후에 데이터를 구성한다)
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)
    tutor_id = post_dict['tutor_id']
    try:
        print "getting applicant info"
        resData['profile'] = TellpinAdminService.get_tutor_applicant_info(tutor_id)
        resData['profile']['short_intro'] = TutorDoc.objects.get(user_id=tutor_id).short_intro
        resData['profile']['long_intro'] = TutorDoc.objects.get(user_id=tutor_id).long_intro
        resData['profile']['video_intro'] = TutorDoc.objects.get(user_id=tutor_id).video_intro
        resData['profile']['user_shortIntro'] = Tutee.objects.get(user_id=tutor_id).short_intro
        resData['profile']['user_longIntro'] = Tutee.objects.get(user_id=tutor_id).long_intro
        '''
        resData['video_intro'] = tutorIntro.video_intro
        profilePending['pending'] = tutorIntro.pending
        profilePending['p_quick_intro'] = tutorIntro.p_quick_intro
        profilePending['p_long_intro'] = tutorIntro.p_long_intro
        profilePending['p_video_intro'] = tutorIntro.p_video_intro
        resume_info = Resume.objects.get(user_id=user_id)
        resumePending["pending"] = resume_info.pending
        resumePending["p_education"] = resume_info.p_education
        resumePending["p_work_exp"] = resume_info.p_work_exp
        resumePending["p_certification"] = resume_info.p_certification
        resData['profilePending'] = profilePending
        resData['resumePending'] = resumePending

        if isTutor:
            if resume_info.fid:
                resume_info.fid = resume_info.fid.split(',')
                resume_info.fid = map(int, resume_info.fid)
                resumePending["fid"] = resume_info.fid
                while index < 9:
                    if resume_info.fid[index] != 0:
                        resumeFile["filename" + str(index)] = Files.objects.get(fid=resume_info.fid[index]).filename
                        index += 1
                    else:
                        resumeFile["filename" + str(index)] = "no file selected"
                        index += 1
            else:
                while index < 9:
                    resumeFile["filename" + str(index)] = "no file selected"
                    index += 1
            resData['resumePending']['filename'] = resumeFile
            '''

        resData['nationsInfo'] = CommonService.country_list()
        resData['languageInfo'] = CommonService.language_list()
        resData['skillLevelInfo'] = CommonService.skill_level_list()
        resData['timezoneInfo'] = CommonService.timezone_list()
        resData['tools'] = CommonService.tool_list()
        resData['currencyInfo'] = CommonService.currency_list()

        return JsonResponse(resData)
    except Exception as e:
        print 'ajax_tutorEditProfile error'
        print e

        return JsonResponse(e)

@csrf_exempt
def ajax_sendEmail(request):
    resData = {}
    resData['isSuccess'] = 1
    post_dict = json.loads(request.body)
    user_id = post_dict['user_id']
    email_contents = post_dict['contents']
    try:
        print 'sending email'
        TellpinAdminService.sendEmail(user_id, email_contents)
        return JsonResponse(resData)
    except Exception as e:
        print 'ajax_sendEmail error'
        print e

        return JsonResponse(e)


@csrf_exempt
def ajax_violationEditProfile(request):
    """
    @summary: 사용자 정보를 편집하는데 필요한 데이터를 구성하는 함수(에디트 화면 진입 시 많은 시간이 소요되므로 html 파일만 로딩 후 추후에 데이터를 구성한다)
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData['isSuccess'] = 1
    post_dict = json.loads(request.body)
    user_id = post_dict['user_id']
    print user_id
    is_Tutor = TellpinUser.objects.get(user_id = user_id).is_tutor
    violation_obj = UserViolation.objects.get(reportee_id = user_id)
    try:
        print "getting user info"
        resData['profile'] = TellpinAdminService.get_user_info(user_id, is_Tutor)
        if is_Tutor:
            resData['profile']['short_intro'] = TutorDoc.objects.get(user_id=user_id).short_intro
            resData['profile']['long_intro'] = TutorDoc.objects.get(user_id=user_id).long_intro
            resData['profile']['video_intro'] = TutorDoc.objects.get(user_id=user_id).video_intro
        resData['profile']['user_shortIntro'] = Tutee.objects.get(user_id=user_id).short_intro
        resData['profile']['user_longIntro'] = Tutee.objects.get(user_id=user_id).long_intro
        violation_type = ""
        if violation_obj.type is 1:
            violation_type = '허위, 과장'
        elif violation_obj.type is 2:
            violation_type = '무관한 내용'
        elif violation_obj.type is 3:
            violation_type = '선정성, 폭력성'
        else:
            violation_type = '기타'
        resData['violation_type'] = violation_type
        resData['violation_content'] = violation_obj.content

        resData['nationsInfo'] = CommonService.country_list()
        resData['languageInfo'] = CommonService.language_list()
        resData['skillLevelInfo'] = CommonService.skill_level_list()
        resData['timezoneInfo'] = CommonService.timezone_list()
        resData['tools'] = CommonService.tool_list()
        resData['currencyInfo'] = CommonService.currency_list()

        return JsonResponse(resData)
    except Exception as e:
        print 'ajax_violationEditProfile error'
        print e

        return JsonResponse(e)


@csrf_exempt
def remove_qna(request):
    """
    @summary: language question 작성된 질문을 삭제하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    post_dict = json.loads(request.body)
    TellpinAdminService.delete_language_question(post_dict["lq_id"])

#     if post_dict["best_answer_credit"]:
#         common.insertTuteeAccount(user_id, point, 11, 6, post_dict["id"])

    return JsonResponse(resData)


@csrf_exempt
def removeAnswer(request):
    """
    @summary: language question 의 답변을 삭제하는  함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)
    TellpinAdminService.delete_language_question_answer(post_dict["auid"], post_dict["aid"])
    resData["answers"] = Community.get_language_question_answers_list(post_dict["id"], post_dict["quid"])

    return JsonResponse(resData)


@csrf_exempt
def ajax_removeComment(request):
    """
    @summary: language board 의 comment를 삭제하는  함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)

    comment_type, ref_id = TellpinAdminService.delete_language_question_comment(post_dict["uid"], post_dict["cid"])
    resData["comments"] = TellpinAdminService.get_language_question_comments(comment_type, ref_id)
    print resData["comments"]
    return JsonResponse(resData)


@csrf_exempt
def remove_tp(request):
    """
    @summary: trans&proof 작성된 질문을 삭제하는 함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    post_dict = json.loads(request.body)
#     point = -(post_dict["best_answer_credit"])
    TellpinAdminService.delete_translation_proofreading(post_dict["tpid"])

#     if post_dict["best_answer_credit"]:
#         common.insertTuteeAccount(user_id, point, 11, 7, post_dict["id"])

    return JsonResponse(resData)


@csrf_exempt
def tp_removeComment(request):
    """
    @summary: trans&proof 의 comment를 삭제하는  함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    post_dict = json.loads(request.body)

    comment_type, ref_id = TellpinAdminService.delete_translation_proofreading_comment(post_dict["uid"], post_dict["cid"])
    resData["comments"] = Community.get_translation_proofreading_comments(comment_type, ref_id)

    return JsonResponse(resData)


@csrf_exempt
def tp_removeAnswer(request):
    """
    @summary: trans&proof 의 답변을 삭제하는  함수
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)

    TellpinAdminService.delete_translation_proofreading_answer(post_dict["auid"], post_dict["aid"])
    resData["answers"] = Community.get_translation_proofreading_answers(post_dict["id"], post_dict["quid"])

    return JsonResponse(resData)

@csrf_exempt
def request_withraw(request):
    """
    @summary: tutor가 요청한 withraw 목록
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
#     user_id = request.user.id
    post_dict = json.loads(request.body)

    resData = TellpinAdminService.get_tutor_withrawal_list(post_dict['page'])
    return JsonResponse(resData)


@csrf_exempt
def set_withraw(request):
    """
    @summary: tutor가 요청한 withraw 목록
    @author: sj46
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
#     user_id = request.user.id
    post_dict = json.loads(request.body)

    resData = TellpinAdminService.set_tutor_withrawal(post_dict['user'], post_dict['withraw'])
    resData = TellpinAdminService.get_tutor_withrawal_list(post_dict['page'])
    return JsonResponse(resData)

