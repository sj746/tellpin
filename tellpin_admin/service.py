# -*- coding: utf-8 -*-
import calendar
import json
from datetime import datetime, timedelta, date
from time import strftime

from dateutil.relativedelta import relativedelta
from django.core.paginator import Paginator
from django.db import connection, transaction
from django.db.models.aggregates import Count
from django.db.models.query_utils import Q
from django.forms.models import model_to_dict
from django.shortcuts import get_object_or_404
from django.utils import timezone

from common import common
from common.service import CommonService
from tutor.models import Tutor, Tutee, TutorDoc, UserViolation
from tutor.service import TutorService, TutorApplyService
from tutoring.models import TutorCourse, TutorCourseBgImg, TutorScheduleWeek, TutorScheduleCalendar, LessonReservation, \
    LessonMessage, LessonFeedback, LessonHistory, LessonClaim, LessonHistoryText,\
    LessonReservationStatusText, LessonReservationTemp
from user_auth.models import TellpinUser
from common.models import TimezoneInfo
from wallet.service import WalletService
from community.models import *
from itertools import chain
from community.service import *
from wallet.models import TuteeWalletHistory, TutorWalletHistory
from support.models import HelpQuestion, HelpAnswer
from django.core.mail.message import EmailMessage


class TellpinAdminService(object):
    def __init__(self):
        pass

    @staticmethod
    def tutorApplyList():
        print "tutor apply list"
        # get all the tutors that applied either 
        tutor_apply_obj = TutorDoc.objects.select_related('user').filter(apply_step = 1)
        return tutor_apply_obj

    @staticmethod
    def getTutorApplications(page=1):
        """
        @summary: 튜티 강의 신청 목록
        @param tutee_id: 튜티 id
        @param utc_time: utc time
        @param status: 강의 예약 타입
        @param page: page 번호
        @param filter_type: 강의 신청 목록 필터
        @param search_name: 이름검색
        @return:
        """

        print "get tutor applications"
        resData = {}
        resData["isSuccess"] = 1
        tl_obj = TutorDoc.objects.select_related('user').filter(status = 1)
        tutorlist= []
        paginator = Paginator(tl_obj, 5)
        page_item = paginator.page(page)
        page_list = paginator.page_range

        for tl in page_item.object_list:
            tmp = {
                'user_id': tl.user_id,
                'name':tl.user.name,
                'email': tl.user.email
            }
            tutorlist.append(tmp)
        resData["TLs"] = tutorlist
        resData['last_page'] = paginator.num_pages
        resData['page'] = page
        resData['pgGroup'] = CommonService.paging_list(page_list, page, 5)
        return resData

    @staticmethod
    def confirmTutor(data):
        resData = {}
        TutorApplyService.confirm_tutor_doc(user_id=data["user"])

        return resData

    @staticmethod
    def getTutorModifications(page=1):
        """
        @summary: 튜티 강의 신청 목록
        @param tutee_id: 튜티 id
        @param utc_time: utc time
        @param status: 강의 예약 타입
        @param page: page 번호
        @param filter_type: 강의 신청 목록 필터
        @param search_name: 이름검색
        @return:
        """

        print "get tutor applications"
        resData = {}
        resData["isSuccess"] = 1
        tl_obj = TutorDoc.objects.select_related('user').filter(Q(lang_modify__isnull = False) | Q(intro_modify__isnull = False) | Q(resume_modify__isnull = False))
        tutorlist = []
        paginator = Paginator(tl_obj, 5)
        page_item = paginator.page(page)
        page_list = paginator.page_range

        for tl in page_item.object_list:
            tmp = {
                'user_id': tl.user_id,
                'name':tl.user.name,
                'email': tl.user.email
            }
            tutorlist.append(tmp)
        resData["TLs"] = tutorlist
        resData['last_page'] = paginator.num_pages
        resData['page'] = page
        resData['pgGroup'] = CommonService.paging_list(page_list, page, 5)
        return resData

    @staticmethod
    def approveChanges(data):
        foreign_key = [
            'native1_id', 'native2_id', 'native3_id',
            'lang1_id', 'lang2_id', 'lang3_id',
            'lang4_id', 'lang5_id', 'lang6_id',
            'user', 'timezone_id',
            'lang1_level', 'lang2_level', 'lang3_level',
            'lang4_level', 'lang5_level', 'lang6_level',
            'teaching1_id', 'teaching2_id', 'teaching3_id',
            'from_country_id', 'from_city_id', 'livingin_country_id', 'livingin_city_id',
            'work1', 'work2', 'work3', 'education1', 'education2', 'education3',
            'certificate1', 'certificate2', 'certificate3',
            'paymentinfo', 'currency_id'
        ]
        print "approved changes, applying data to the table"
        resData = {}
        tl_obj = Tutor.objects.get(user_id = data["user"])
        tutor_dict = model_to_dict(tl_obj,
                                           exclude=[
                                               'created_time',
                                               'updated_time', 'birthdate', 'gender', 'currency', 'currency_id', 'del_by_admin', 'del_by_user'
                                           ])

        for key, value in tutor_dict.items():
            # overwrite what is on the Tutor db with the 
            if key in data:
                val = data[key]
                if data[key] is not None:
                    val = str(data[key])
                if key in foreign_key:
                    key += '_id'
                setattr(tl_obj, key, val)
        # by here all the columns changed their values
        tl_obj.save()
        # need to change the status of the tutor doc to confirmed, 3
        td_obj = TutorDoc.objects.get(user_id=data["user"])
        td_obj.status = 3
        td_obj.lang_modify = None
        td_obj.intro_modify = None
        td_obj.resume_modify = None
        td_obj.save()
        # need to get all the values from TutorDoc into the Tutor db table
        print "saved all the values into the db"
        return resData

    @staticmethod
    def declineChanges(data):
        print "declined changes, applying data to the table"
        resData = {}
        # need to change the status of the tutor doc to rejected, 4
        td_obj = TutorDoc.objects.get(user_id=data["user"])
        td_obj.status = 4
        td_obj.save()
        print "saved all the values into the db"
        return resData

    @staticmethod
    def getLessonClaims(page=1):
        """
        @summary: 튜티 강의 신청 목록
        @param tutee_id: 튜티 id
        @param utc_time: utc time
        @param status: 강의 예약 타입
        @param page: page 번호
        @param filter_type: 강의 신청 목록 필터
        @param search_name: 이름검색
        @return:
        """

        print "get lesson claims"
        resData = {}
        resData["isSuccess"] = 1
        # get the claims that are in dispute
        lc_obj = LessonReservation.objects.filter(lesson_status=4)
        lc_list = []
        paginator = Paginator(lc_obj, 5)
        page_item = paginator.page(page)
        page_list = paginator.page_range

        for lesson in page_item.object_list:
            tmp = {
                'reservation_id': lesson.id,
                'tutor': lesson.tutor_id,
                'tutee': lesson.tutee_id,
                'course': lesson.course_id,
                'claim_id': lesson.id
                   }
            lc_list.append(tmp)

        resData["TLs"] = lc_list
        resData['last_page'] = paginator.num_pages
        resData['page'] = page
        resData['pgGroup'] = CommonService.paging_list(page_list, page, 5)
        return resData


    @staticmethod
    def get_problems_details_list(rvid):
        """
        @summary: 강의 문제제기 리스트
        @author: msjang
        @param rvid: 강의 예약 id
        @param _user_id: 유저 id
        @return: result_list( list )
        """

        result = {}

        rv = LessonReservation.objects.get(id=rvid)
        lessonClaim = LessonClaim.objects.select_related('lesson_reservation', 'claim_text', 'reportor', 'reportee').filter(lesson_reservation_id=rvid).order_by('-created_time')
        result["rvid"] = rv.id
        result["tutor_id"] = rv.tutor_id
        result["tutee_id"] = rv.tutee_id

        result["list"] = []
        for lc in lessonClaim:
            obj2 = {}
            obj2["created_time"] = str(lc.created_time)
            obj2["claim_text"] = lc.claim_text.claim_english
            obj2["claim_detail"] = lc.claim_detail
            obj2["resolve_type"] = lc.resolve_type
            obj2["problem_amount"] = lc.resolve_tc
            obj2["reportor"] = lc.reportor_id
            obj2["reportee"] = lc.reportee_id
            obj2["reportor_name"] = lc.reportor.name
            obj2["reportee_name"] = lc.reportee.name
            obj2["claim_id"] = lc.id


            result["list"].append(obj2)
        
        return result


    @staticmethod
    def getUserViolations(page=1):
        """
        @summary: 튜티 강의 신청 목록
        @param tutee_id: 튜티 id
        @param utc_time: utc time
        @param status: 강의 예약 타입
        @param page: page 번호
        @param filter_type: 강의 신청 목록 필터
        @param search_name: 이름검색
        @return:
        """

        print "get user violations"
        resData = {}
        resData["isSuccess"] = 1
        uv_obj = UserViolation.objects.select_related('reportee').filter()
        uv_list = []
        paginator = Paginator(uv_obj, 5)
        page_item = paginator.page(page)
        page_list = paginator.page_range

        for tl in page_item.object_list:
            tmp = {
                'email': tl.reportee.email,
                'user_id': tl.reportee.user_id,
                'name': tl.reportee.name,
            }
            uv_list.append(tmp)
        resData["TLs"] = uv_list
        resData['last_page'] = paginator.num_pages
        resData['page'] = page
        resData['pgGroup'] = CommonService.paging_list(page_list, page, 5)
        return resData

    @staticmethod
    def sendEmail(user_id, mail):
        """
        @summary: sendemail test
        @author: whcho
        @param none
        @return: 0(success), 1(fail)
        """

        from_email = 'admin@tellpin.com'
        to_email = TellpinUser.objects.get(user_id = user_id).email

        result = 1
        #serial = serial1 + ' ' + serial2 + ' ' + serial3 + ' ' + serial4 
        subject, from_email, to = 'User Violation Report', from_email, to_email
        try :
            msg = EmailMessage( subject, mail, from_email, [to] )
            msg.send()
        except Exception as e :
            print str( e )
            result = 0
        finally:
            return result

    @staticmethod
    def resolveClaim(claim_id, confirm, tutor_amnt=None, tutee_amnt=None):
        """
        @summary: resolve the claim
        @author: shkim
        """
        lc_obj = LessonClaim.objects.select_related('lesson_reservation').get(id = claim_id)
        rv_obj = LessonReservation.objects.select_related('lesson').get(id = lc_obj.lesson_reservation.id)
        tmpprice = rv_obj.lesson.sg_price
        if tmpprice is  0:
            # package lesson
            tmpprice = rv_obj.lesson.pkg_price
        if tutor_amnt is None:
            if confirm is 1:
                # tutor gets the credit
                WalletService.setConfirm(tmpprice, rv_obj.tutor_id, rv_obj.tutee_id, rv_obj.id)
                LessonHistory(history_text_id = 35, lesson_reservation_id = rv_obj.id).save()
            else:
                WalletService.setRefund(tmpprice, rv_obj.tutor_id, rv_obj.tutee_id, rv_obj.id)
                LessonHistory(history_text_id = 18, lesson_reservation_id = rv_obj.id).save()
        else:
            # there is a tutor amount - thus, tc1 and tc2 are not none
            WalletService.setRefund(tutee_amnt, rv_obj.tutor_id, rv_obj.tutee_id, rv_obj.id)
            LessonHistory(history_text_id = 19, lesson_reservation_id = rv_obj.id).save()
        lc_obj.status = 2
        lc_obj.save()
        rv_obj.status = 7
        rv_obj.save()

    @staticmethod
    def getPostClaims(page=1):
        """
        @summary: 튜티 강의 신청 목록
        @param tutee_id: 튜티 id
        @param utc_time: utc time
        @param status: 강의 예약 타입
        @param page: page 번호
        @param filter_type: 강의 신청 목록 필터
        @param search_name: 이름검색
        @return:
        """

        print "get post claims"
        resData = {}
        resData["isSuccess"] = 1
        # need to get all from lb, lq, tp
        # need to find out those existing in 
        lb_obj = LBViolation.objects.select_related('lb').filter()
        lq_obj = LQViolation.objects.select_related('lq_id').filter()
        tp_obj = TPViolation.objects.select_related('tp').filter()
        post_list = []
        result_list = list(chain(lb_obj, lq_obj, tp_obj))
        paginator = Paginator(result_list, 5)
        page_item = paginator.page(page)
        page_list = paginator.page_range
        for tl in page_item.object_list:
            tmp = {}
            if isinstance(tl, LBViolation):
                tmp['post_type'] = "Language Learning Board"
                tmp['post_id'] = tl.lb.id
                tmp['post_title'] = tl.lb.title
            elif isinstance(tl, LQViolation):
                tmp['post_type'] = "Language Question"
                tmp['post_id'] = tl.lq_id.id
                tmp['post_title'] = tl.lq_id.title
            elif isinstance(tl, TPViolation):
                tmp['post_type'] = "Translation & Proofreading"
                tmp['post_id'] = tl.tp.id
                tmp['post_title'] = tl.tp.title
            post_list.append(tmp)

        resData["TLs"] = post_list
        resData['last_page'] = paginator.num_pages
        resData['page'] = page
        resData['pgGroup'] = CommonService.paging_list(page_list, page, 5)

        return resData

    @staticmethod
    def getTransactionClaims(page=1):
        """
        @summary: 튜티 강의 신청 목록
        @param tutee_id: 튜티 id
        @param utc_time: utc time
        @param status: 강의 예약 타입
        @param page: page 번호
        @param filter_type: 강의 신청 목록 필터
        @param search_name: 이름검색
        @return:
        """

        print "get transaction claims"
        resData = {}
        resData["isSuccess"] = 1
        # need to get all the transactions
        # from tuteewallethistory get buying credits
        tuteewallet_obj = TuteeWalletHistory.objects.select_related('tutee').filter(transaction_type = "B")
        tutorwallet_obj = TutorWalletHistory.objects.select_related('tutor').filter(transaction_type = "W")
        post_list = []
        result_list = list(chain(tuteewallet_obj, tutorwallet_obj))
        paginator = Paginator(result_list, 5)
        page_item = paginator.page(page)
        page_list = paginator.page_range
        for tl in page_item.object_list:
            tmp = {
                'payment_id': tl.id,
                'amount': tl.pending_delta,
            }
            if isinstance(tl, TuteeWalletHistory):
                # buying credit
                tmp['description'] = "Buy Credits"
                tmp['from'] = tl.tutee.user.name
                tmp['to'] = "Tellpin Admin"
            elif isinstance(tl, TutorWalletHistory):
                # withdrawing credit
                tmp['description'] = "Withdraw Credits"
                tmp['from'] = "Tellpin Admin"
                tmp['to'] = tl.tutor.user.name
            post_list.append(tmp)

        resData["TLs"] = post_list
        resData['last_page'] = paginator.num_pages
        resData['page'] = page
        resData['pgGroup'] = CommonService.paging_list(page_list, page, 5)
        return resData

    @staticmethod
    def getHelpClaims(page=1):
        """
        @summary: 튜티 강의 신청 목록
        @param tutee_id: 튜티 id
        @param utc_time: utc time
        @param status: 강의 예약 타입
        @param page: page 번호
        @param filter_type: 강의 신청 목록 필터
        @param search_name: 이름검색
        @return:
        """

        print "get help claims"
        resData = {}
        resData["isSuccess"] = 1
        hq_obj = HelpQuestion.objects.select_related('user').filter(status__in = [1, 2])
        lc_list = []
        paginator = Paginator(hq_obj, 5)
        page_item = paginator.page(page)
        page_list = paginator.page_range

        for tl in page_item.object_list:
            tmp = {
                'qid': tl.id,
                'username': tl.user.name,
                'title': tl.title,
            }
            lc_list.append(tmp)
        resData["TLs"] = lc_list
        resData['last_page'] = paginator.num_pages
        resData['page'] = page
        resData['pgGroup'] = CommonService.paging_list(page_list, page, 5)
        return resData

    @staticmethod
    def get_user_info(user_id, isTutor):
        """
        @summary: 튜터 정보
        @param tutor_id: 튜터 id
        @return: dictionary
        """
        user_dict = dict()
        # need to put in objects for foreign keys
        try:
            if isTutor:
                print "getting tutor info"
                user_obj = Tutor.objects.get(user_id=user_id)
                tutor_doc_obj = TutorDoc.objects.get(user_id = user_id)
                tutor_doc_dict = model_to_dict(tutor_doc_obj)
                user_dict = model_to_dict(user_obj)
                user_dict["native1"] = tutor_doc_dict["native1"]
                user_dict["native2"] = tutor_doc_dict["native2"]
                user_dict["native3"] = tutor_doc_dict["native3"]
                user_dict["native1_id"] = tutor_doc_dict["native1_id"]
                user_dict["native2_id"] = tutor_doc_dict["native2_id"]
                user_dict["native3_id"] = tutor_doc_dict["native3_id"]
                user_dict["lang1"] = tutor_doc_dict["lang1"]
                user_dict["lang2"] = tutor_doc_dict["lang2"]
                user_dict["lang3"] = tutor_doc_dict["lang3"]
                user_dict["lang4"] = tutor_doc_dict["lang4"]
                user_dict["lang5"] = tutor_doc_dict["lang5"]
                user_dict["lang6"] = tutor_doc_dict["lang6"]
                user_dict["lang1_learning"] = tutor_doc_dict["lang1_learning"]
                user_dict["lang2_learning"] = tutor_doc_dict["lang2_learning"]
                user_dict["lang3_learning"] = tutor_doc_dict["lang3_learning"]
                user_dict["lang4_learning"] = tutor_doc_dict["lang4_learning"]
                user_dict["lang5_learning"] = tutor_doc_dict["lang5_learning"]
                user_dict["lang6_learning"] = tutor_doc_dict["lang6_learning"]
                user_dict["lang1_id"]=  tutor_doc_dict["lang1_id"]
                user_dict["lang2_id"]=  tutor_doc_dict["lang2_id"]
                user_dict["lang3_id"]=  tutor_doc_dict["lang3_id"]
                user_dict["lang4_id"]=  tutor_doc_dict["lang4_id"]
                user_dict["lang5_id"]=  tutor_doc_dict["lang5_id"]
                user_dict["lang6_id"]=  tutor_doc_dict["lang6_id"]
                user_dict["lang1_level"] = tutor_doc_dict["lang1_level"]
                user_dict["lang2_level"] = tutor_doc_dict["lang2_level"]
                user_dict["lang3_level"] = tutor_doc_dict["lang3_level"]
                user_dict["lang4_level"] = tutor_doc_dict["lang4_level"]
                user_dict["lang5_level"] = tutor_doc_dict["lang5_level"]
                user_dict["lang6_level"] = tutor_doc_dict["lang6_level"]
                user_dict["lang_modify"] = tutor_doc_dict["lang_modify"]
                user_dict["intro_modify"] = tutor_doc_dict["intro_modify"]
                user_dict["resume_modify"] = tutor_doc_dict["resume_modify"]
                if user_obj.photo_filename:
                    user_dict['photo_filepath'] = '%s' % (CommonService.PROFILE_IMAGE_FILE_DIR)
                    user_dict['photo_filename'] = '%s' % (user_obj.photo_filename)
                user_dict['profile_info'] = TutorApplyService.get_doc_profile(user_id)
                birthDate = {}
                if user_dict["birthdate"] == "" or user_dict["birthdate"] is None:
                    birthDate["year"] = None
                    birthDate["month"] = None
                    birthDate["day"] = None
                else:
                    birthDate["year"] = int(str(user_dict["birthdate"])[0:4])
                    birthDate["month"] = int(str(user_dict["birthdate"])[5:7])
                    birthDate["day"] = int(str(user_dict["birthdate"])[8:10])
                del user_dict["birthdate"]
                user_dict["birthdate"] = birthDate
                user_dict["server_year"] = datetime.today().year
                user_dict["tools"] = TutorService.get_tutor_tools(user_id, None)
            else:
                
                user_obj = Tutee.objects.get(user_id=user_id)
                user_dict = model_to_dict(user_obj)
                
                if user_obj.photo_filename:
                    user_dict['photo_filepath'] = '%s' % (CommonService.PROFILE_IMAGE_FILE_DIR)
                    user_dict['photo_filename'] = '%s' % (user_obj.photo_filename)
                user_dict['profile_info'] = TutorApplyService.get_doc_profile(user_id)
                birthDate = {}
                if user_dict["birthdate"] == "" or user_dict["birthdate"] is None:
                    birthDate["year"] = None
                    birthDate["month"] = None
                    birthDate["day"] = None
                else:
                    birthDate["year"] = int(str(user_dict["birthdate"])[0:4])
                    birthDate["month"] = int(str(user_dict["birthdate"])[5:7])
                    birthDate["day"] = int(str(user_dict["birthdate"])[8:10])
                del user_dict["birthdate"]
                user_dict["birthdate"] = birthDate
                user_dict["server_year"] = datetime.today().year
                user_dict["tools"] = TutorService.get_tutor_tools(user_id, None)
                
                
        except Exception as err:
            print '@@@@@get_user_info : ', str(err)
            print '@@@@@get_user_info :', str(err)
            #tutor_dict = None
            #tutee_dict = None
        finally:
            return user_dict
    
    @staticmethod
    def get_tutor_applicant_info(user_id):
        """
        @summary: 튜터 정보
        @param tutor_id: 튜터 id
        @return: dictionary
        """
        user_dict = dict()
        # need to put in objects for foreign keys
        try:
            
            print "getting tutor info"
            user_obj = TutorDoc.objects.get(user_id=user_id)
            if user_obj.photo_filename:
                user_dict['photo_filepath'] = '%s' % (CommonService.PROFILE_IMAGE_FILE_DIR)
                user_dict['photo_filename'] = '%s' % (user_obj.photo_filename)
            user_dict = model_to_dict(user_obj)
            user_dict['profile_info'] = TutorApplyService.get_doc_profile(user_id)
            birthDate = {}
            print user_dict
            if user_dict["birthdate"] == "" or user_dict["birthdate"] is None:
                birthDate["year"] = None
                birthDate["month"] = None
                birthDate["day"] = None
            else:
                birthDate["year"] = int(str(user_dict["birthdate"])[0:4])
                birthDate["month"] = int(str(user_dict["birthdate"])[5:7])
                birthDate["day"] = int(str(user_dict["birthdate"])[8:10])
            del user_dict["birthdate"]
            user_dict["birthdate"] = birthDate
            user_dict["server_year"] = datetime.today().year
            user_dict["tools"] = TutorService.get_tutor_tools(user_id, None)

        except Exception as err:
            print '@@@@@get_user_info :', str(err)
            print '@@@@@get_user_info :', str(err)
            #tutor_dict = None
            #tutee_dict = None
        finally:
            return user_dict

    @staticmethod
    def delete_language_board(_p_id):
        """
        @summary: language board 의 board를 삭제하는  함수
        @author: sj46
        @param _p_id : post id
        @return: language board 삭제된 post 정보
        """
        board = LB.objects.get(id=_p_id)
        board.delete()

        return board

    @staticmethod
    def delete_language_board_comment(_user_id, _cid):
        """
        @summary: language learning club 댓글 삭제하는 함수
        @author: sj46
        @param param1: _user_id : 사용자 id
        @param param1: _cid : 댓글 id
        @return: 댓글 리스트
        """

        comment = LBComment.objects.get(id=_cid)
        if comment.user_id == _user_id:
            sub_comments = LBComment.objects.filter(type='C', id=_cid)
            if sub_comments:
                comment.contents = '!delete!'
                comment.save()
            else:
                comment.delete()
        comment_list = Community.get_language_board_comments(comment.type, comment.lb_id)
        print "deleted language board comment"
        return comment_list

    @staticmethod
    def delete_language_question(_q_id):
        """
        @summary: language question 의 post를 삭제하는  함수
        @author: sj46
        @param param1: _p_id : post id
        @return: language question 삭제된 post 정보
        """
        print "question id" + str(_q_id)
        qna = LQ.objects.get(id=_q_id)
        qna.delete()

        return qna

    @staticmethod
    def delete_language_question_answer(_user_id, _a_id):
        """
        @summary: language question 의 답변을 삭제하는  함수
        @author: sj46
        @param param1: _user_id - 게시글 작성자
        @param param2: _a_id : 답변 id
        @return: language question 삭제된 답변 정보
        """
        print "deleting lq answer"
        print _a_id
        print _user_id
        answer = LQAnswer.objects.get(id=_a_id)
        if answer.user_id == _user_id:
            answer.delete()

        return answer

    @staticmethod
    def delete_language_question_comment(_user_id, _c_id):
        """
        @summary: language learning club 의 comment를 삭제하는  함수
        @author: sj46
        @param param1: _user_id : 사용자 ID
        @param param2: _c_id : 댓글 ID
        @return: 게시글 타입 P: Post C: Comment, 게시글 ID
        """

        comment = LQComment.objects.get(id=_c_id)
        comment_type = comment.type
        ref_id = comment.lq_ref_id
        if comment.user_id == _user_id:
            sub_comments = LQComment.objects.filter(type='C', lq_ref_id=comment.id)
            if sub_comments:
                comment.contents = '!delete!'
                comment.save()
            else:
                comment.delete()

        return comment_type, ref_id

    @staticmethod
    def get_language_question_comments(_type, _lq_ref_id):
        """
        @summary: language question 의  한 항목의 코멘트 리스트를 가져오는 함수
        @author: sj46
        @param param1: _type - 게시글 타입 (Q:질문, A:답변, C:코멘트)
        @param param2: _ref_id : 질문 id
        @return: language question 한 항목에 달린 전체 답변에 대한 리스트
        """

        comments = LQComment.objects.filter(type=_type, lq_ref_id=_lq_ref_id).order_by('created_time')
        comment_list = []
        for comment in comments:
            if Tutee.objects.filter(user_id=comment.user_id):
                comment_dict = model_to_dict(comment, exclude=['created_time'])
                comment_dict["created_time"] = str(comment.created_time)
                if TutorService.is_tutor(comment.user_id):
                    if Tutor.objects.filter(user=comment.user_id):
                        comment_dict["writer_info"] = model_to_dict(Tutor.objects.get(user_id=comment.user_id),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
                else:
                    if Tutee.objects.filter(user=comment.user_id):
                        comment_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=comment.user_id),
                            exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
# sub comment delete
#                 sub_comments = LQComment.objects.filter(type="C", lq_ref_id=comment.id).order_by('reg_date')
#                 comment_dict["sub_comments"] = []
#                 for sub_comment in sub_comments:
#                     sub_comment_dict = model_to_dict(sub_comment, exclude=['reg_date'])
#                     sub_comment_dict["reg_date"] = str(sub_comment.reg_date)
#                     sub_comment_dict["writer_info"] = model_to_dict(Tutee.objects.get(user_id=sub_comment.user_id),
#                                      exclude=['gender', 'birthdate', 'created_time', 'updated_time', 'del_by_admin', 'del_by_user', 'long_intro'])
#                     comment_dict["sub_comments"].append(sub_comment_dict)
                comment_list.append(comment_dict)

        return comment_list

    @staticmethod
    def get_lq_violation(post_id, user_id):
        # get the violation item
        lq_violation = None
        lq_violation = LQViolation.objects.select_related('user_id').filter(user_id = user_id, lq_id = post_id).first()

        return lq_violation

    @staticmethod
    def get_lb_violation(post_id, user_id):
        # get the violation item
        lb_violation = None
        lb_violation = LBViolation.objects.select_related('user').filter(user_id = user_id, lb_id = post_id).first()

        return lb_violation

    @staticmethod
    def get_tp_violation(post_id, user_id):
        # get the violation item
        tp_violation = None
        tp_violation = TPViolation.objects.select_related('user').filter(user_id = user_id, tp_id = post_id).first()

        return tp_violation

    @staticmethod
    def delete_translation_proofreading(_q_id):
        """
        @summary: trans&proof 의 post를 삭제하는  함수
        @author: sj46
        @param param1: _p_id : post id
        @return: trans&proof 삭제된 post 정보
        """

        tp = TP.objects.get(id=_q_id)
        tp.delete()

        return tp

    @staticmethod
    def delete_translation_proofreading_answer(_user_id, _a_id):
        """
        @summary: trans&proof 의 답변을 삭제하는  함수
        @author: sj46
        @param param1: _user_id - 게시글 작성자
        @param param2: _a_id : 답변 id
        @return: trans&proof 삭제된 답변 정보
        """

        answer = TPAnswer.objects.get(id=_a_id)
        if answer.user_id == _user_id:
            answer.delete()

        return answer

    @staticmethod
    def delete_translation_proofreading_comment(_user_id, _c_id):
        """
        @summary: trans&proof 의 comment를 삭제하는  함수
        @author: sj46
        @param param1: _user_id : 사용자 ID
        @param param2: _c_id : 댓글 ID
        @return: 게시글 타입 P: Post C: Comment, 게시글 ID
        """

        comment = TPComment.objects.get(id=_c_id)
        comment_type = comment.type
        ref_id = comment.tp_ref_id
        if comment.user_id == _user_id:
            sub_comments = TPComment.objects.filter(type='C', tp_ref_id=comment.id)
            if sub_comments:
                comment.contents = '!delete!'
                comment.save()
            else:
                comment.delete()

        return comment_type, ref_id

    @staticmethod
    def get_tutor_withrawal_list(page=1):
        """
        @summary: withraw 요청한 tutor 목록 구성
        @author: sj46
        @return: withraw list
        """
        resData = {}
        resData["isSuccess"] = 1

        try:
            tutor_obj = Tutor.objects.filter(withdrawal_pending__gte=1)
            tutor_list = []
            paginator = Paginator(tutor_obj, 15)
            page_item = paginator.page(page)
            page_list = paginator.page_range
            for tutor in page_item.object_list:
                temp = {
                        'user' : tutor.user_id,
                        'payment' : tutor.paymentinfo_id,
                        'withdrawal_pending' : tutor.withdrawal_pending,
                        'total_balance' : tutor.total_balance,
                        'available_balance' : tutor.available_balance,
                        'pending_balance' : tutor.available_balance
                        }
                tutor_list.append(temp)
            resData["TLs"] = tutor_list
            resData['last_page'] = paginator.num_pages
            resData['page'] = page
            resData['pgGroup'] = CommonService.paging_list(page_list, page, 15)
            return resData
        except Exception as e:
            print '@@@@@get_tutor_withrawal_list : ', str(e)
            return 0

    @staticmethod
    def set_tutor_withrawal(user, withdraw_pending):
        """
        @summary: withraw update
        @author: sj46
        @return: withraw list
        """
        resData = {}
        resData["isSuccess"] = 1

        try:
            tutor = Tutor.objects.select_related('paymentinfo').get(user=user)
            tutor.withdrawal_pending -= withdraw_pending
            tutor.save()
            wallet_history = TutorWalletHistory(
                tutor=tutor,
                transaction_type='W',
                type=3,
                type_ref=None,
                total_delta=-withdraw_pending,
                available_delta=-withdraw_pending,
                pending_delta=0,
                total_balance=tutor.total_balance,
                available_balance=tutor.available_balance,
                pending_balance=tutor.pending_balance,
                total_withdrawal_pending=tutor.withdrawal_pending,
                withdrawal_status=2,
                withdrawal_account=tutor.paymentinfo.email,
            )
            wallet_history.save()
        except Exception as e:
            print '@@@@@set_tutor_withrawal : ', str(e)

