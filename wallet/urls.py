from django.conf.urls import url
import views

urlpatterns = [
               url(r'^$', views.tutor_account),
               url(r'buy_coin/$', views.buy_coin),
               url(r'get_wallet_history/$', views.get_wallet_history),
               url(r'get_wallet_history_pop/$', views.get_wallet_history_pop),
               url(r'buy_credits/$', views.buy_credits),
               url(r'pay_creditcard/$', views.pay_creditCard),
               url(r'complete/$', views.result_credit),
               url(r'tutor_account/page/$', views.ajax_tutor_page),
               url(r'tutor_account/tuteetc/$', views.ajax_getTuteeTc),
               url(r'tutor_account/transfer/$', views.transfer_credit),
               url(r'tutor_account/withdraw/$', views.send_withdraw),
               url(r'withdraw/delete/$', views.delete_withdrawal),
               url(r'withdraw/modify/$', views.modify_withdrawal),
               url(r'tutee_account/page/$', views.ajax_tutee_page),
               url(r'tutee_account/gift/$', views.open_gift),
               url(r'tutee_account/gift/send/$', views.send_giftcard),
               url(r'tutee_account/gift/check/$', views.check_giftcard_code),
               url(r'tutee_account/gift/giftcard/$', views.giftCard),
               url(r'tutee_account/receipt/$', views.get_receipt),
               url(r'reservation/$', views.get_reservation),
               url(r'lesson/$', views.get_lesson),
               ]
