# -*- coding: utf-8 -*-

###############################################################################
# filename    : wallet > service.py
# description : Tellpin wallet
# author      : hsrjmk@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20161010 hsrjmk 최초 작성
#
#
###############################################################################
import urllib, urllib2

from collections import OrderedDict
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from django.core.exceptions import ObjectDoesNotExist
from django.core.mail.message import EmailMessage
from django.core.paginator import Paginator
from django.db import transaction
from django.db.models import Q
from django.forms.models import model_to_dict
from django.utils import timezone

from common.models import BuyCreditsFeeInfo
from dashboard.models import Notification, NotificationText
from tellpin.settings import DEFAULT_FROM_EMAIL, TELLPIN_HOME
from tutor.models import Tutee, Tutor
from tutoring.models import LessonReservation, Lesson
from wallet.models import Giftcard, TuteeWalletHistory, TutorWalletHistory, TRANSACTION_TYPE


class WalletService(object):
    def __init__(self):
        pass

    PER_PAGE = 10

    ####################################################################
    #    Wallet 관련 QuerySet
    ####################################################################
    @staticmethod
    def get_tutee_history(user_id, page=1, start=None, end=None, purchase=False):
        """
        @summary: Tutee 거래 내역 조회
        @author: hsrjmk
        @param user_id: user
        @param page: 페이지 번호
        @param start: 검색 시작 날짜
        @param end : 검색 종료 날짜
        @param withDraw : 0 - 모든내역, 1 - withdraw내역
        @return: obj( object )
        """
        tutee = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        utc_time_delta = timedelta(hours=tutee.timezone_id.utc_delta)

        # Revision date time
        if start is not None and end is not None:
            # Convert to utc time
            end_date = datetime.strptime(end, '%Y-%m-%d') - utc_time_delta + timedelta(hours=23, minutes=59, seconds=59)
            start_date = datetime.strptime(start, '%Y-%m-%d') - utc_time_delta
        else:
            end_date = datetime.utcnow()
            start_date = end_date + relativedelta(months=-1)

        # Query wallet history
        queryset = Q(tutee_id=user_id) & Q(created_time__range=(start_date, end_date))
        if purchase:
            queryset &= Q(type=2)
        wallet_history_objs = TuteeWalletHistory.objects.filter(queryset)\
                                                        .select_related('tutee')\
                                                        .order_by('-created_time')

        paginator = Paginator(wallet_history_objs, WalletService.PER_PAGE)
        current_page = paginator.page(page)

        obj = {}
        obj['page'] = page
        obj['has_prev'] = current_page.has_previous()
        obj['has_next'] = current_page.has_next()
        obj['page_range'] = paginator.page_range
        obj['startPage'] = current_page.start_index()
        obj['endPage'] = current_page.end_index()
        obj['startDate'] = (start_date + utc_time_delta).date()
        obj['endDate'] = (end_date + utc_time_delta).date()
        obj['item'] = WalletService.make_tutee_wallet_list(current_page, tutee.timezone_id.utc_delta)
        return obj

    @staticmethod
    def get_tutor_history(user_id, page=1, start=None, end=None, withdraw=False):
        """
        @summary: Tutor 거래 내역 조회
        @author: hsrjmk
        @param user_id: user
        @param page: 페이지 번호
        @param start: 검색 시작 날짜
        @param end : 검색 종료 날짜
        @param withDraw : 0 - 모든내역, 1 - withdraw내역
        @return: obj( object )
        """
        tutor = Tutor.objects.select_related('timezone_id').get(user_id=user_id)
        utc_time_delta = timedelta(hours=tutor.timezone_id.utc_delta)

        # Revision date time
        if start is not None and end is not None:
            # Convert to utc time
            end_date = datetime.strptime(end, '%Y-%m-%d') - utc_time_delta + timedelta(hours=23, minutes=59, seconds=59)
            start_date = datetime.strptime(start, '%Y-%m-%d') - utc_time_delta
        else:
            end_date = datetime.utcnow()
            start_date = end_date + relativedelta(months=-1)

        # Query wallet history
        queryset = Q(tutor_id=user_id) & Q(created_time__range=(start_date, end_date))
        if withdraw:
            queryset &= Q(type=3)
        wallet_history_objs = TutorWalletHistory.objects.filter(queryset)\
                                                        .select_related('tutor')\
                                                        .order_by('-created_time')

        paginator = Paginator(wallet_history_objs, WalletService.PER_PAGE)
        current_page = paginator.page(page)

        obj = {}
        obj['page'] = page
        obj['has_prev'] = current_page.has_previous()
        obj['has_next'] = current_page.has_next()
        obj['page_range'] = paginator.page_range
        obj['startPage'] = current_page.start_index()
        obj['endPage'] = current_page.end_index()
        obj['startDate'] = (start_date + utc_time_delta).date()
        obj['endDate'] = (end_date + utc_time_delta).date()
        obj['item'] = WalletService.make_tutor_wallet_list(current_page, tutor.timezone_id.utc_delta)
        return obj

    @staticmethod
    def get_tutee_account(user_id):
        """
        @summary: Tutee 잔액 조회
        @author: hsrjmk
        @param user_id: user
        @return result: obj
        """
        obj = {}
        try:
            tutee = Tutee.objects.get(user_id=user_id)
            obj['total_balance'] = tutee.total_balance
            obj['total_available'] = tutee.available_balance
            obj['total_pending'] = tutee.pending_balance
        except ObjectDoesNotExist as oe:
            obj['total_balance'] = 0
            obj['total_available'] = 0
            obj['total_pending'] = 0
            print str(oe)
        finally:
            return obj

    @staticmethod
    def get_tutor_account(user_id):
        """
        @summary: language board 관련 pin한 게시 글 id
        @author: hsrjmk
        @param user_id: user
        @return result: LB ids
        """
        obj = {}
        try:
            tutor = Tutor.objects.select_related('paymentinfo').get(user_id=user_id)
            obj['total_balance'] = tutor.total_balance
            obj['total_available'] = tutor.available_balance
            obj['total_pending'] = tutor.pending_balance
            obj['total_withdrawal'] = tutor.withdrawal_pending
            obj['paypal'] = tutor.paymentinfo.paypal
        except ObjectDoesNotExist as oe:
            obj['total_balance'] = 0
            obj['total_available'] = 0
            obj['total_pending'] = 0
            obj['total_withdrawal'] = 0
            obj['paypal'] = None
            print str(oe)
        finally:
            return obj


    #########################################################################################
    #    Get Tutee/Tutor's balance Service API
    #########################################################################################
    @staticmethod
    def get_tutee_balance(user_id):
        """
        @summary: Tutee의 총 잔액 조회
        @author: hsrjmk
        @param user_id: Tutee user id
        @return result: Tutee's total balance, available balance, pending balance
        """
        try:
            tutee = Tutee.objects.get(user_id=user_id)
            return tutee.total_balance, tutee.available_balance, tutee.pending_balance
        except Exception as e:
            print '@@@@@get_tutee_balance : ', str(e)
            return 0, 0, 0

    @staticmethod
    def get_tutee_total_balance(user_id):
        """
        @summary: Tutee의 총 잔액 조회
        @author: hsrjmk
        @param user_id: Tutee user id
        @return result: Tutee's total balance
        """
        try:
            tutee = Tutee.objects.get(user_id=user_id)
            return tutee.total_balance
        except Exception as e:
            print '@@@@@get_tutee_total_balance : ', str(e)
            return 0

    @staticmethod
    def get_tutee_available_balance(user_id):
        """
        @summary: Tutee의 이용가능한 잔액 조회
        @author: hsrjmk
        @param user_id: Tutee user id
        @return result: Tutee's available balance
        """
        try:
            tutee = Tutee.objects.get(user_id=user_id)
            return tutee.available_balance
        except Exception as e:
            print '@@@@@get_tutee_available_balance : ', str(e)
            return 0

    @staticmethod
    def get_tutee_pending_balance(user_id):
        """
        @summary: Tutee의 미정(Pending) 잔액 조회
        @author: hsrjmk
        @param user_id: Tutee user id
        @return result: Tutee's pending balance
        """
        try:
            tutee = Tutee.objects.get(user_id=user_id)
            return tutee.pending_balance
        except Exception as e:
            print '@@@@@get_tutee_pending_balance : ', str(e)
            return 0

    @staticmethod
    def get_tutor_balance(user_id):
        """
        @summary: Tutor의 잔액 조회
        @author: hsrjmk
        @param user_id: Tutor user id
        @return result: Tutor's total balance, available balance, pending balance, withdrawal pending
        """
        try:
            tutor = Tutor.objects.get(user_id=user_id)
            return tutor.total_balance, tutor.available_balance, tutor.pending_balance, tutor.withdrawal_pending
        except Exception as e:
            print '@@@@@get_tutor_balance : ', str(e)
            return 0, 0, 0, 0

    @staticmethod
    def get_tutor_total_balance(user_id):
        """
        @summary: Tutor의 총 잔액 조회
        @author: hsrjmk
        @param user_id: Tutor user id
        @return result: Tutor's total balance
        """
        try:
            tutor = Tutor.objects.get(user_id=user_id)
            return tutor.total_balance
        except Exception as e:
            print '@@@@@get_tutor_total_balance : ', str(e)
            return 0

    @staticmethod
    def get_tutor_available_balance(user_id):
        """
        @summary: Tutor의 이용가능한 잔액 조회
        @author: hsrjmk
        @param user_id: Tutor user id
        @return result: Tutor's available balance
        """
        try:
            tutor = Tutor.objects.get(user_id=user_id)
            return tutor.available_balance
        except Exception as e:
            print '@@@@@get_tutor_available_balance : ', str(e)
            return 0

    @staticmethod
    def get_tutor_pending_balance(user_id):
        """
        @summary: Tutor의 미정(Pending) 잔액 조회
        @author: hsrjmk
        @param user_id: Tutor user id
        @return result: Tutor's pending balance
        """
        try:
            tutor = Tutor.objects.get(user_id=user_id)
            return tutor.pending_balance
        except Exception as e:
            print '@@@@@get_tutor_pending_balance : ', str(e)
            return 0

    @staticmethod
    def get_tutor_withdrawal_pending(user_id):
        """
        @summary: Tutor의 미정(Pending) 잔액 조회
        @author: hsrjmk
        @param user_id: Tutor user id
        @return result: Tutor's withdrawal pending
        """
        try:
            tutor = Tutor.objects.get(user_id=user_id)
            return tutor.withdrawal_pending
        except Exception as e:
            print '@@@@@get_tutor_withdrawal_pending : ', str(e)
            return 0


    #########################################################################################
    #    Charge Service API
    #########################################################################################
    @staticmethod
    def get_credit_info():
        """
        @summary: Tellpin credit 충전 가격 정보 리스트
        @author: hsrjmk
        @param None
        @return: result( list )
        """
        result = []
        for info in BuyCreditsFeeInfo.objects.all():
            result.append(model_to_dict(info))
        return result

    @staticmethod
    def get_paypal_result(tx):
        """
        @summary: Paypal 결제 요청 후, 결제 결과 받는 함수
        @author: hsrjmk
        @param tx: 해당 결제에 대한 고유번호 ( paypal 에서 제공 )
        @return: pay( object )
        """
        #paypal test
#         token = 'RDr-oJx-mlC5Uiqr16Da3kQf3-xU_skpuIIHulI_j54Lt4fma2_dGnl5w_S'
#         paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr'  # paypal TEST URL
        token = 'BpvdOvhehk-q8ikX5NDX_cgUP0pHd1l3JenkEoptV5PgefMjrDUe3WQs4x8'  # paypal Token ( paypal 측에서 제공 )
        paypal_url = 'https://www.paypal.com/cgi-bin/webscr'  # paypal URL

        post_dict = dict()
        post_dict['cmd'] = '_notify-synch'
        post_dict['tx'] = tx
        post_dict['at'] = token
        result = urllib2.urlopen(paypal_url, urllib.urlencode(post_dict))  # paypal 측에 url request를 통해 해당 거래에 대한 정보 받아옴
        content = result.read()
        lines = content.split('\n')
        pay = {}
        pay['result'] = lines[0].strip()

        for line in lines[1:]:
            linesplit = line.split('=', 2)
            if len(linesplit) == 2:
                pay[linesplit[0].strip()] = urllib.unquote(linesplit[1].strip())

        return pay

    @staticmethod
    @transaction.atomic
    def save_paypal_result(user_id, paypal):
        """
        @summary: 결제결과에 따라 해당 유저에 Tutee, TuteeWalletHistory 테이블 insert
        @author: hsrjmk
        @param user_id: 해당 유저 id 번호
        @param paypal: 결제정보 들어있는 객체
        @return: 1( success), 0( fail )
        """
        result = 0
        # 결제결과가 성공이 아닐 경우 0을 return
        if paypal['result'] != 'SUCCESS':
            return result

        try:
            tutee = Tutee.objects.select_related('user').get(user_id=user_id)
        except ObjectDoesNotExist:
            return result

        # 결제결과가 성공일 경우
        credit = int(paypal['item_name'])
        try:
            tutee.total_balance += credit
            tutee.available_balance += credit
            tutee.save()
            if tutee.user.is_tutor:
                tutor = Tutor.objects.get(user_id=user_id)
                tutor.total_balance += credit
                tutor.available_balance += credit
                tutor.save()

            wallet_history = TuteeWalletHistory(
                tutee=tutee,
                transaction_type='B',
                type=2,
                type_ref=None,
                total_delta=credit,
                available_delta=credit,
                pending_delta=0,
                total_balance=tutee.total_balance,
                available_balance=tutee.available_balance,
                pending_balance=tutee.pending_balance,
                payment_type=1,
                payment_code=paypal['txn_id'],
            )
            wallet_history.save()
            result = 1
        except Exception as e:
            print '@@@@@save_paypal_result : ', str(e)
            result = 0
        finally:
            return result

    @staticmethod
    def setRefund(amount, tutor_id, tutee_id, rvid):
        """
        @param amount: amount that the specified user will receive
        @param userid: the id of the user
        @param isTutor: whether the user is the tutor of the lesson
        @param lessonid: the lesson id which will be used in type_ref for the wallethistory
        """
        tutor = Tutor.objects.get(user_id=tutor_id)
        tutee = Tutee.objects.get(user_id=tutee_id)
        rv = LessonReservation.objects.select_related('lesson').get(id = rvid)
        tmpprice = rv.lesson.sg_price
        if tmpprice is  0:
            # package lesson
            tmpprice = rv.lesson.pkg_price
        if amount is tmpprice:    
            # the total amount goes to the tutee
            tutor.pending_balance -= amount
            tutor.save()
            TutorWalletHistory(type = 5, type_ref = rvid, total_delta = 0, 
                           available_delta = 0, pending_delta = -amount, 
                           total_balance = tutor.total_balance, 
                           available_balance = tutor.available_balance, 
                           pending_balance = tutor.pending_balance,
                           tutor_id = tutor_id,
                           transaction_type = "RL").save()
        
            tutee.available_balance += amount
            tutee.pending_balance -= amount
            tutee.save()
            TuteeWalletHistory(type = 5, type_ref = rvid, total_delta = 0, 
                           available_delta = amount, pending_delta = -amount, 
                           total_balance = tutee.total_balance, 
                           available_balance = tutee.available_balance, 
                           pending_balance = tutee.pending_balance,
                           tutee_id = tutee_id,
                           transaction_type = "RL").save()
        else:
            # there is a partial refund
            # the amount is always focused on the tutee
            tutor.total_balance += tmpprice - amount
            tutor.available_balance += tmpprice - amount
            tutor.pending_balance -= tmpprice
            tutor.save()
            TutorWalletHistory(type = 5, type_ref = rvid, total_delta = tmpprice-amount, 
                           available_delta = tmpprice-amount, pending_delta = -tmpprice, 
                           total_balance = tutor.total_balance, 
                           available_balance = tutor.available_balance, 
                           pending_balance = tutor.pending_balance,
                           tutor_id = tutor_id,
                           transaction_type = "RL").save()
        
            tutee.total_balance -= tmpprice - amount
            tutee.available_balance += amount
            tutee.pending_balance -= tmpprice
            tutee.save()
            TuteeWalletHistory(type = 5, type_ref = rvid, total_delta = 0, 
                           available_delta = amount, pending_delta = -tmpprice, 
                           total_balance = tutee.total_balance, 
                           available_balance = tutee.available_balance, 
                           pending_balance = tutee.pending_balance,
                           tutee_id = tutee_id,
                           transaction_type = "RL").save()

    @staticmethod
    def setConfirm(amount, tutor_id, tutee_id, rvid):
        """
        @param amount: amount that the specified user will receive
        @param userid: the id of the user
        @param isTutor: whether the user is the tutor of the lesson
        @param lessonid: the lesson id which will be used in type_ref for the wallethistory
        """
        
        tutor = Tutor.objects.get(user_id=tutor_id)
        tutor.total_balance += amount
        tutor.available_balance += amount
        tutor.pending_balance -= amount
        tutor.save()
        TutorWalletHistory(type = 5, type_ref = rvid, total_delta = amount, 
                           available_delta = amount, pending_delta = -amount, 
                           total_balance = tutor.total_balance, 
                           available_balance = tutor.available_balance, 
                           pending_balance = tutor.pending_balance,
                           tutor_id = tutor_id,
                           transaction_type = "C").save()
        tutee = Tutee.objects.get(user_id=tutee_id)
        tutee.total_balance -= amount
        tutee.pending_balance -= amount
        tutee.save()
        TuteeWalletHistory(type = 5, type_ref = rvid, total_delta = -amount, 
                           available_delta = 0, pending_delta = -amount, 
                           total_balance = tutee.total_balance, 
                           available_balance = tutee.available_balance, 
                           pending_balance = tutee.pending_balance,
                           tutee_id = tutee_id,
                           transaction_type = "C").save()

                               
            


    #########################################################################################
    #    Gift Service API
    #########################################################################################
    @staticmethod
    @transaction.atomic
    def buy_giftcard(user_id, gift_info):
        """
        @summary: Giftcard 구매
        @author: hsrjmk
        @param user_id: Sender user id
        @param gift_info: Gift information
        @return result: Tutor's pending_balance
        """
        result = 1
        try:
            # Create Giftcard
            giftcard = Giftcard(
                tc=gift_info['tc'],
                gift_code=gift_info['serial'],
                status=0,
                sender_name=gift_info['from_name'],
                receiver_email=gift_info['to_email'],
                receiver_name=gift_info['to_name'],
                content=gift_info['content'],
                buy_time=timezone.now(),
                use_time=None,
                user_id=user_id)
            giftcard.save()

            # Update Tutee (and Tutor)
            tutee = Tutee.objects.select_related('user').get(user_id=user_id)
            tutee.total_balance -= gift_info['tc']
            tutee.available_balance -= gift_info['tc']
            tutee.save()
            if tutee.user.is_tutor:
                tutor = Tutor.objects.get(user_id=user_id)
                tutor.total_balance -= gift_info['tc']
                tutor.available_balance -= gift_info['tc']
                tutor.save()

            # Create TuteeWalletHistory
            wallet_history = TuteeWalletHistory(
                tutee=tutee,
                transaction_type='G',
                type=4,
                type_ref=giftcard.id,
                total_delta=-(gift_info['tc']),
                available_delta=-(gift_info['tc']),
                pending_delta=0,
                total_balance=tutee.total_balance,
                available_balance=tutee.available_balance,
                pending_balance=tutee.pending_balance,
                payment_type=0,
                payment_code=gift_info['serial'],
            )
            wallet_history.save()

            # Notification
            notification_text = NotificationText.objects.get(id=1)
            notification = Notification(
                type=7,
                community_type=0,
                ref_id=giftcard.id,
                is_read=False,
                receiver_id=user_id,
                sender_id=user_id,
                noti_text=notification_text,
            )
            notification.save()

            # Send Email to 
            email_info = {}
            email_info['subject'] = '[Tellpin] Your purchased Tellpin Gift Card was sent to %s(%s)' % (gift_info['to_name'], gift_info['to_email'])
            email_info['from_email'] = DEFAULT_FROM_EMAIL
            email_info['to_email'] = [tutee.user.email]
            email_info['content'] = gift_info['serial']
            WalletService.send_email(email_info)
        except Exception as e:
            print '@@@@@buy_giftcard : ', str(e)
            result = 0
        finally:
            return result

    @staticmethod
    def check_giftcard_code(code):
        """
        @summary: Giftcard 유효성 검사
        @author: hsrjmk
        @param code: gift card code
        @return: result( list )
        """
        obj = {}
        try:
            obj['check'] = Giftcard.objects.filter(gift_code=code).exists()
            if obj['check']:
                gift = Giftcard.objects.get(gift_code=code)
                obj['giftTc'] = gift.tc
                obj['giftId'] = gift.id
                obj['giftUse'] = gift.status
        except Exception as e:
            print '@@@@@check_giftcard_code : ', str(e)
            obj['message'] = str(e)
        finally:
            return obj

    @staticmethod
    @transaction.atomic
    def redeem_giftcard(user_id, gift_id):
        """
        @summary: Giftcard를 TC로 전환
        @author: hsrjmk
        @param user_id: User id to redeem
        @param gift_info: Gift information
        @return result: Tutor's pending_balance
        """
        result = 1
        try:
            # Update Giftcard
            giftcard = Giftcard.objects.get(id=gift_id)
            giftcard.status = 1
            giftcard.use_time = datetime.utcnow()
            giftcard.save()

            # Update Tutee (and Tutor)
            tutee = Tutee.objects.select_related('user').get(user_id=user_id)
            tutee.total_balance += giftcard.tc
            tutee.available_balance += giftcard.tc
            tutee.save()
            if tutee.user.is_tutor:
                tutor = Tutor.objects.get(user_id=user_id)
                tutor.total_balance += giftcard.tc
                tutor.available_balance += giftcard.tc
                tutor.save()

            # Create TuteeWalletHistory
            wallet_history = TuteeWalletHistory(
                tutee=tutee,
                transaction_type='DG',
                type=4,
                type_ref=giftcard.id,
                total_delta=giftcard.tc,
                available_delta=giftcard.tc,
                pending_delta=0,
                total_balance=tutee.total_balance,
                available_balance=tutee.available_balance,
                pending_balance=tutee.pending_balance,
                payment_type=0,
                payment_code=giftcard.gift_code,
            )
            wallet_history.save()
        except Exception as e:
            print '@@@@@redeem_giftcard : ', str(e)
            result = 0
        finally:
            return result

    #########################################################################################
    #   Transfer Service API
    #########################################################################################
    @staticmethod
    @transaction.atomic
    def transfer_credit(user_id, transfer_credit):
        """
        @summary: Tutor credit -> Tutee credit 전환
        @author: hsrjmk
        @param user_id: 해당 유저 id 번호
        @param transfer_credit: 변환할 코인 금액
        @return: 1( success ), 0( fail )
        """
        result = 1
        try:
            # Update Tutor
            tutor = Tutor.objects.get(user_id=user_id)
            tutor.total_balance -= transfer_credit
            tutor.available_balance -= transfer_credit
            tutor.save()

            # Create Tutor wallet history
            wallet_history = TutorWalletHistory(
                tutor=tutor,
                transaction_type='T',
                type=5,
                type_ref=None,
                total_delta=-transfer_credit,
                available_delta=-transfer_credit,
                pending_delta=0,
                total_balance=tutor.total_balance,
                available_balance=tutor.available_balance,
                pending_balance=tutor.pending_balance,
                total_withdrawal_pending=tutor.withdrawal_pending,
                withdrawal_status=0,
                withdrawal_account=None,
            )
            wallet_history.save()

            # Update Tutee
            tutee = Tutee.objects.get(user_id=user_id)
            tutee.total_balance += transfer_credit
            tutee.available_balance += transfer_credit
            tutee.save()

            # Create Tutee wallet history
            wallet_history = TuteeWalletHistory(
                tutee=tutee,
                transaction_type='T',
                type=5,
                type_ref=None,
                total_delta=transfer_credit,
                available_delta=transfer_credit,
                pending_delta=0,
                total_balance=tutee.total_balance,
                available_balance=tutee.available_balance,
                pending_balance=tutee.pending_balance,
                payment_type=0,
                payment_code=None,
            )
            wallet_history.save()
        except Exception as e:
            print '@@@@@transfer_credit : ', str(e)
            result = 0
        finally:
            return result

    #########################################################################################
    #   Withdraw Service API
    #########################################################################################
    @staticmethod
    @transaction.atomic
    def withdraw_credit(user_id, withdrawal_credit, paypal):
        """
        @summary: Credit 인출 신청
        @author: hsrjmk
        @param user_id: 해당 유저 id 번호
        @param withdrawal_credit: 인출할 금액
        @param paypal: 페이팔 계정
        @return: 1( success ), 0( fail )
        """
        result = 1
        try:
            # Update Tutor
            tutor = Tutor.objects.get(user_id=user_id)
            tutor.total_balance -= withdrawal_credit
            tutor.available_balance -= withdrawal_credit
            tutor.withdrawal_pending += withdrawal_credit
            tutor.save()

            # Create Tutor wallet history
            wallet_history = TutorWalletHistory(
                tutor=tutor,
                transaction_type='WP',
                type=3,
                type_ref=None,
                total_delta=-withdrawal_credit,
                available_delta=-withdrawal_credit,
                pending_delta=0,
                total_balance=tutor.total_balance,
                available_balance=tutor.available_balance,
                pending_balance=tutor.pending_balance,
                total_withdrawal_pending=tutor.withdrawal_pending,
                withdrawal_status=1,
                withdrawal_account=paypal,
            )
            wallet_history.save()
        except Exception as e:
            result = 0
            print '@@@@@withdraw_credit : ', str(e)
        finally:
            return result

    @staticmethod
    @transaction.atomic
    def modify_withdrawal(wallet_history_id, user_id, old_withdrawal_credit, new_withdrawal_credit, paypal):
        """
        @summary: 인출 신청 수정
        @author: hsrjmk
        @param wallet_history_id: 해당 인출 요청 id 번호
        @param user_id: 해당 유저 id 번호
        @param old_withdrawal_credit: 이전 인출할 금액
        @param new_withdrawal_credit: 신규 인출할 금액
        @param paypal: 페이팔 계정
        @return: 1( success ), 0( fail )
        """
        try:
            # Update Tutor wallet history
            WalletService.cancel_withdrawal(wallet_history_id, user_id, old_withdrawal_credit, paypal)

            # Update Tutor
            tutor = Tutor.objects.get(user_id=user_id)
            tutor.total_balance -= new_withdrawal_credit
            tutor.available_balance -= new_withdrawal_credit
            tutor.withdrawal_pending += new_withdrawal_credit
            tutor.save()

            # Create Tutor wallet history
            wallet_history = TutorWalletHistory(
                tutor=tutor,
                transaction_type='CW',
                type=3,
                type_ref=None,
                total_delta=-new_withdrawal_credit,
                available_delta=-new_withdrawal_credit,
                pending_delta=0,
                total_balance=tutor.total_balance,
                available_balance=tutor.available_balance,
                pending_balance=tutor.pending_balance,
                total_withdrawal_pending=tutor.withdrawal_pending,
                withdrawal_status=1,
                withdrawal_account=paypal,
            )
            wallet_history.save()
        except Exception as e:
            result = 0
            print '@@@@@modify_withdrawal : ', str(e)
        finally:
            return result

    @staticmethod
    @transaction.atomic
    def cancel_withdrawal(wallet_history_id, user_id, withdrawal_credit, paypal):
        """
        @summary: 인출 신청 취소
        @author: hsrjmk
        @param wallet_history_id: 해당 인출 요청 id 번호
        @param user_id: 해당 유저 id 번호
        @param withdrawal_credit: 인출할 금액
        @param paypal: 페이팔 계정
        @return: 1( success ), 0( fail )
        """
        try:
            # Update Tutor wallet history
            old_wallet_history = TutorWalletHistory.objects.get(id=wallet_history_id)
            old_wallet_history.withdrawal_status=3
            old_wallet_history.save()

            # Update Tutor (Restore)
            tutor = Tutor.objects.get(user_id=user_id)
            tutor.total_balance += withdrawal_credit
            tutor.available_balance += withdrawal_credit
            tutor.withdrawal_pending -= withdrawal_credit
            tutor.save()

            # Create Tutor wallet history
            wallet_history = TutorWalletHistory(
                tutor=tutor,
                transaction_type='CW',
                type=3,
                type_ref=None,
                total_delta=withdrawal_credit,
                available_delta=withdrawal_credit,
                pending_delta=0,
                total_balance=tutor.total_balance,
                available_balance=tutor.available_balance,
                pending_balance=tutor.pending_balance,
                total_withdrawal_pending=tutor.withdrawal_pending,
                withdrawal_status=2,
                withdrawal_account=paypal,
            )
            wallet_history.save()
        except Exception as e:
            result = 0
            print '@@@@@cancel_withdrawal : ', str(e)
        finally:
            return result


    #########################################################################################
    #    More information Service API
    #########################################################################################
    @staticmethod
    def get_receipt_detail(wallet_history_id):
        """
        @summary: 구매 영수증 조회
        @author: hsrjmk
        @param wallet_history_id: 해당 TuteeWalletHistory id
        @return: obj( object )
        """
        obj = {}
        try:
            wallet_history = TuteeWalletHistory.objects.select_related('tutee')\
                                                .select_related('tutee__timezone_id')\
                                                .get(id=wallet_history_id)
            obj['receipt'] = model_to_dict(wallet_history)
            obj['receipt']['name'] = wallet_history.tutee.name
            obj['receipt']['created_date'] = wallet_history.created_time + timedelta(hours=wallet_history.tutee.timezone_id.utc_delta)
            obj['receipt']['teac_id'] = wallet_history.id

            obj['buy'] = {}
            try:
                buy = BuyCreditsFeeInfo.objects.get(tc=wallet_history.available_delta)
                obj['buy'] = model_to_dict(buy)
            except ObjectDoesNotExist:
                obj['buy']['id'] = 0
                obj['buy']['tc'] = wallet_history.available_delta
                obj['buy']['usd'] = wallet_history.available_delta / 10
                obj['buy']['creditcard_fee'] = None
                obj['buy']['paypal_fee'] = None
        except Exception as e:
            print '@@@@@get_receipt_detail : ', str(e)
            obj['message'] = str(e)
        finally:
            return obj

    @staticmethod
    def wallet_get_reservation_detail(lesson_id, wallet_history_id, is_tutor, user_id):
        """
        @summary: 강의예약정보 조회
        @author: hsrjmk
        @param reservation_id: Reservation 테이블 pk
        @param wallet_history_id: 해당 TuteeWalletHistory id
        @param is_tutor: 요청분류( Tutor or Tutee )
        @param user_id: 해당 유저 id 번호
        @return: obj( object )
        """
        obj = {}
        try:
            utc_delta = 0

            # Wallet information
            if is_tutor:
                wallet_history = TutorWalletHistory.objects.select_related('tutor')\
                                                    .select_related('tutor__timezone_id')\
                                                    .get(id=wallet_history_id)
                utc_delta = wallet_history.tutor.timezone_id.utc_delta
            else:
                wallet_history = TuteeWalletHistory.objects.select_related('tutee')\
                                                    .select_related('tutee__timezone_id')\
                                                    .get(id=wallet_history_id)
                utc_delta = wallet_history.tutee.timezone_id.utc_delta

            # Reservation information
#             reservation = LessonReservation.objects.filter(id=wallet_history.type_ref)\
#                                                 .select_related('course', 'lesson', 'tutee')
            print lesson_id
            rv_obj = LessonReservation.objects.select_related('lesson', 'course', 'tutor', 'tutee').get(id=lesson_id)
#             lesson = Lesson.objects.select_related('course', 'tutor', 'tutee').get(id=lesson_id)
            if rv_obj.lesson.lesson_type == 1:
                obj['lesson_credit'] = rv_obj.lesson.trial_price
                obj['duration'] = 30
            elif rv_obj.lesson.lesson_type == 2:
                obj['lesson_credit'] = rv_obj.lesson.sg_price
                obj['duration'] = rv_obj.lesson.sg_min
                obj['lesson_is_bundle'] = 0
            else:
                obj['lesson_credit'] = rv_obj.lesson.pkg_total_price
                obj['duration'] = rv_obj.lesson.pkg_min
                obj['lesson_is_bundle'] = 1
                obj['bundle_times'] = rv_obj.lesson.pkg_times
                obj['rv_orders'] = rv_obj.pkg_order

            obj['lesson_id'] = rv_obj.id
            obj['tutee_id'] = rv_obj.tutee_id
            obj['tutee_name'] = rv_obj.tutee.name
            obj['tutor_id'] = rv_obj.tutor.user_id
            obj['tutor_name'] = rv_obj.tutor.name
            obj['lesson_name'] = rv_obj.course.course_name
            obj['payment_id'] = wallet_history.id
            obj['start_time'] = (rv_obj.created_time + timedelta(hours=utc_delta)).strftime('%Y-%m-%d %H:%M:%S')
            obj['end_time'] = (rv_obj.lesson.expired_time + timedelta(hours=utc_delta)).strftime('%Y-%m-%d %H:%M:%S')
#             obj['lesson']['start_time'] = datetime(year=reservation.rv_time[:4],
#                                                    month=reservation.rv_time[4:6],
#                                                    day=reservation.rv_time[6:8],
#                                                    hour=reservation.rv_time[9:11],
#                                                    minute=reservation.rv_time[11:13]) + timedelta(hours=utc_delta)
#             obj['lesson']['end_time'] = obj['lesson']['start_time'] + timedelta(minutes=duration)
            obj['created_time'] = (wallet_history.created_time + timedelta(hours=utc_delta)).strftime('%Y-%m-%d %H:%M:%S')
        except Exception as e:
            print '@@@@@wallet_get_reservation_detail : ', str(e)
            obj['log'] = str(e)
        finally:
            return obj

    @staticmethod
    def get_lesson(lesson_id, wallet_history_id, is_tutor):
        """
        @summary: 강의 정보 조회
        @author: hsrjmk
        @param lesson_id: Lesson 테이블 pk
        @param wallet_history_id: TuteeAccount 테이블 pk
        @param is_tutor: 요청 분류( Tutor or Tutee )
        @return: obj( object )
        """
        # TODO: 강의 정보를 표시하는 부분은 없는 듯...
        obj = {}
        return obj

    @staticmethod
    def get_gift_detail(wallet_history_id, user_id):
        """
        @summary: 선물 정보 조회
        @author: hsrjmk
        @param wallet_history_id: 해당 TuteeWalletHistory id
        @param user_id: 해당 유저 id 번호
        @return: obj( object )
        """
        obj = {}
        try:
            wallet_history = TuteeWalletHistory.objects.select_related('tutee')\
                                                .select_related('tutee__timezone_id')\
                                                .get(id=wallet_history_id)
            giftcard = Giftcard.objects.select_related('user')\
                                        .get(id=wallet_history.type_ref)

            obj['gift'] = model_to_dict(giftcard) 
            obj['created_time'] = wallet_history.created_time + timedelta(hours=wallet_history.tutee.timezone_id.utc_delta)

#             redeem_history = TuteeWalletHistory.objects.select_related('tutee')\
#                                                 .get(payment_code=giftcard.gift_code, transaction_type='DG')
#             if redeem_history.exist():
#                 obj['username'] = redeem_history.tutee.name
        except Exception as e:
            print '@@@@@get_gift_detail : ', str(e)
            obj['message'] = str(e)
        finally:
            return obj


    #########################################################################################
    #    Lesson Reservation Service API
    #########################################################################################
    @staticmethod
    def get_balance_history(user_id):
        """
        @summary: 선물 정보 조회
        @author: hsrjmk
        @param user_id: 해당 유저 id 번호
        @return: obj( object )
        """
        try:
            tutee = Tutee.objects.get(user_id=user_id)

            obj = {}
            obj["available_balance"] = tutee.available_balance
            obj["purchase_pending"] = 0
            obj["sale_pending"] = 0

            obj["total_balance"] = tutee.total_balance
        except Exception as err:
            print '@@@@@get_balance_history : ', str(err)

        return obj

    @staticmethod
    def get_wallet_history(user_id):
        """
        @summary: 사용자의 wallet history를 불러오는 함수
        @author: hsrjmk
        @param user_id: 해당 유저 id
        @return: result( list )
        """
        try:
            result = []
            obj = {}

            # TODO: coin_list를 사용하는지 확인해 볼 것
            obj["coin_list"] = []
            wallet_history_objs = TuteeWalletHistory.objects.filter(tutee_id=user_id).order_by('-created_time')
            if wallet_history_objs.exists():
                for wallet_history in wallet_history_objs:
                    item_obj ={}
                    item_obj["coid"] = wallet_history.id
                    item_obj["payment_id"] = 'P{0:0>11}'.format(wallet_history.id)
                    item_obj["user_id"] = wallet_history.tutee_id
                    item_obj["cointype"] = wallet_history.type
                    item_obj["wallet"] = wallet_history.available_delta
                    item_obj["created_time"] = wallet_history.created_time
                    item_obj["available_balance"] = wallet_history.available_balance
                    item_obj["lesson_number"] = wallet_history.type_ref

                    if wallet_history.type == 1:
                        item_obj["type_name"] = "reservation"
                        item_obj["action"] = "View lesson"
                    if wallet_history.type == 2:
                        item_obj["type_name"] = "Buy Coin"
                        item_obj["action"] = "View Receipt"
                    if wallet_history.type == 3:
                        item_obj["type_name"] = "withdrawal"
                        item_obj["action"] = "View Receipt"

                    obj["coin_list"].append(item_obj)
            else:
                item_obj = {}
                item_obj["available_balance"] = 0
                obj["coin_list"].append(item_obj)
            result.append(obj)
        except Exception as err:
            print '@@@@@get_wallet_history : ', str(err)

        return result

    @staticmethod
    def get_wallet_history_pop(user_id):
        """
        @summary: 유저 wallet history 불러오는 함수
        @author: khyun
        @param user_id: 해당 유저 id
        @return: result( list )
        """
        try:
            result = []
            obj = {}

            # TODO: coin_list를 사용하는지 확인해 볼 것
#             obj["coin_list"] = []
#             wallet_history_objs = TuteeWalletHistory.objects.filter(tutee_id=user_id).order_by('-created_time')
#             if wallet_history_objs.exists():
#                 for wallet_history in wallet_history_objs:
#                     item_obj ={}
#                     item_obj["coid"] = wallet_history.id
#                     item_obj["payment_id"] = 'P{0:0>11}'.format(wallet_history.id)
#                     item_obj["user_id"] = wallet_history.tutee_id
#                     item_obj["cointype"] = wallet_history.type
#                     item_obj["wallet"] = wallet_history.available_delta
#                     item_obj["created_time"] = wallet_history.created_time
#                     item_obj["available_balance"] = wallet_history.available_balance
#                     item_obj["lesson_number"] = wallet_history.type_ref
#
#                     obj["coin_list"].append(item_obj)
#             else:
#                 item_obj = {}
#                 item_obj["available_balance"] = 0
#                 obj["coin_list"].append(item_obj)

            tutee = Tutee.objects.get(user_id=user_id)
            obj["available_balance"] = tutee.available_balance
#             obj["purchase_pending"] = tutee.pending_balance
#             obj["sale_pending"] = 0
        except Exception as err:
            obj["available_balance"] = 0
#             obj["purchase_pending"] = 0
#             obj["sale_pending"] = 0
            print '@@@@@get_wallet_history_pop : ', str(err)

        result.append(obj)
        return result


    #########################################################################################
    #    Admin Method
    #########################################################################################
    @staticmethod
    @transaction.atomic
    def complete_withdrawal(user_id, withdrawal_credit, paypal):
        """
        @summary: 인출 신청 확인 (관리자 only)
        @author: hsrjmk
        @param user_id: 해당 유저 id 번호
        @param withdrawal_credit: 인출할 금액
        @param paypal: 페이팔 계정
        @return: 1( success ), 0( fail )
        """
        try:
            # Update Tutor
            tutor = Tutor.objects.get(user_id=user_id)
            tutor.withdrawal_pending -= withdrawal_credit
            tutor.save()

            # Update Tutor wallet history
            wallet_history = TutorWalletHistory.objects.get(id=id)
            wallet_history.withdrawal_status = 2
            wallet_history.save()
        except Exception as e:
            result = 0
            print '@@@@@complete_withdrawal : ', str(e)
        finally:
            return result


    #########################################################################################
    #    Method
    #########################################################################################
    @staticmethod
    def make_tutee_wallet_list(wallet_history_objs, utc_delta=0):
        """
        @summary: Tutee wallet 상세 목록 만들기
        @author: hsrjmk
        @param wallet_history_objs: TuteeWalletHistory model object
        @return result: Tutee wallet history list
        """
        result = []
        for wallet_history in wallet_history_objs:
            li = {} # List item
            li['user_id'] = wallet_history.tutee.user_id
            li['type'] = OrderedDict(TRANSACTION_TYPE).keys().index(wallet_history.transaction_type)
            li['more_type'] = wallet_history.type
            li['more_id'] = wallet_history.type_ref
            li['more_code'] = wallet_history.payment_code
            li['teac_id'] = wallet_history.id
            li['t_change'] = wallet_history.total_delta
            li['a_change'] = wallet_history.available_delta
            li['p_change'] = wallet_history.pending_delta
            li['total_balance'] = wallet_history.total_balance
            li['total_available'] = wallet_history.available_balance
            li['total_pending'] = wallet_history.pending_balance
            li['payment_method'] = wallet_history.payment_type
            li['created_date'] = (wallet_history.created_time + timedelta(hours=utc_delta)).date()

            result.append(li)
        return result

    @staticmethod
    def make_tutee_purchase_list(wallet_history_objs, utc_delta=0):
        """
        @summary: Tutee wallet 구매 목록 만들기
        @author: hsrjmk
        @param wallet_history_objs: TuteeWalletHistory model object
        @return result: Tutee wallet history list
        """
        result = []
        for wallet_history in wallet_history_objs:
            li = {} # List item
            li['user_id'] = wallet_history.tutee.user_id
            li['type'] = OrderedDict(TRANSACTION_TYPE).keys().index(wallet_history.transaction_type)
            li['more_type'] = wallet_history.type
            li['more_id'] = wallet_history.type_ref
            li['more_code'] = wallet_history.payment_code
            li['teac_id'] = wallet_history.id
            li['t_change'] = wallet_history.total_delta
            li['a_change'] = wallet_history.available_delta
            li['p_change'] = wallet_history.pending_delta
            li['total_balance'] = wallet_history.total_balance
            li['total_available'] = wallet_history.available_balance
            li['total_pending'] = wallet_history.pending_balance
            li['payment_method'] = wallet_history.payment_type
            li['created_date'] = (wallet_history.created_time + timedelta(hours=utc_delta)).date()

            result.append(li)
        return result

    @staticmethod
    def make_tutor_wallet_list(wallet_history_objs, utc_delta=0):
        """
        @summary: Tutor wallet 상세 목록 만들기
        @author: hsrjmk
        @param wallet_history_objs: TutorWalletHistory model object
        @return result: Tutor wallet history list
        """
        result = []
        for wallet_history in wallet_history_objs:
            li = {} # List item
            li['user_id'] = wallet_history.tutor.user_id
            li['type'] = OrderedDict(TRANSACTION_TYPE).keys().index(wallet_history.transaction_type)
            li['more_type'] = wallet_history.type
            li['more_id'] = wallet_history.type_ref
            li['more_account'] = wallet_history.withdrawal_account
            li['teac_id'] = wallet_history.id
            li['t_change'] = wallet_history.total_delta
            li['a_change'] = wallet_history.available_delta
            li['p_change'] = wallet_history.pending_delta
            li['total_balance'] = wallet_history.total_balance
            li['total_available'] = wallet_history.available_balance
            li['total_pending'] = wallet_history.pending_balance
            li['total_withdrawal_pending'] = wallet_history.total_withdrawal_pending
            li['withdrawal_status'] = wallet_history.withdrawal_status
            li['created_date'] = (wallet_history.created_time + timedelta(hours=utc_delta)).date()

            result.append(li)
        return result

    @staticmethod
    def make_tutor_withdraw_list(wallet_history_objs, utc_delta=0):
        """
        @summary: Tutor wallet 인출 목록 만들기
        @author: hsrjmk
        @param wallet_history_objs: TutorWalletHistory model object
        @param utc_delta: Diff utc
        @return result: Tutor wallet history list
        """
        result = []
        for wallet_history in wallet_history_objs:
            li = {} # List item
            li['user_id'] = wallet_history.tutor.user_id
            li['type'] = OrderedDict(TRANSACTION_TYPE).keys().index(wallet_history.transaction_type)
            li['more_type'] = wallet_history.type
            li['more_id'] = wallet_history.type_ref
            li['more_account'] = wallet_history.withdrawal_account
            li['teac_id'] = wallet_history.id
            li['t_change'] = wallet_history.total_delta
            li['a_change'] = wallet_history.available_delta
            li['p_change'] = wallet_history.pending_delta
            li['total_balance'] = wallet_history.total_balance
            li['total_available'] = wallet_history.available_balance
            li['total_pending'] = wallet_history.pending_balance
            li['total_withdrawal_pending'] = wallet_history.total_withdrawal_pending
            li['withdrawal_status'] = wallet_history.withdrawal_status
            li['created_date'] = (wallet_history.created_time + timedelta(hours=utc_delta)).date()

            result.append(li)
        return result

    #########################################################################################
    #    Email Service API
    #########################################################################################
    @staticmethod
    def send_email(email_info):
        """
        @summary: 이메일 전송
        @author: hsrjmk
        @param email_info: 전송할 이메일 정보
        @return result: 
        """
        subject = email_info['subject']
        content = email_info['content']
        from_email = email_info['from_email']
        to_email = email_info['to_email']
        msg = EmailMessage(subject, content, from_email, to_email)
        msg.content_subtype = 'html'
        msg.send()

    @staticmethod
    @transaction.atomic
    def buy_lesson(user_id, lid):
        """
        @summary: 강의 구매
        @param user_id: 구매자 id
        @param lid: lesson id
        @return: True, False
        """
        result = True
        try:
            lesson_obj = Lesson.objects.select_related(
                'tutee'
            ).get(id=lid)

            if lesson_obj.lesson_type == 1:
                lesson_price = lesson_obj.trial_price
                transaction_type = 'L'
            elif lesson_obj.lesson_type == 2:
                lesson_price = lesson_obj.sg_price
                transaction_type = 'L'
            else:
                lesson_price = lesson_obj.pkg_total_price
                transaction_type = 'P'

            lesson_obj.tutee.available_balance -= lesson_price
            lesson_obj.tutee.pending_balance += lesson_price
            lesson_obj.tutee.save()

            tutee_wallet = TuteeWalletHistory(
                tutee_id=user_id,
                transaction_type=transaction_type,
                type=1,
                type_ref=lid,
                total_delta=0,
                available_delta=-lesson_price,
                pending_delta=lesson_price,
                total_balance=lesson_obj.tutee.total_balance,
                available_balance=lesson_obj.tutee.available_balance,
                pending_balance=lesson_obj.tutee.pending_balance
            )

            tutee_wallet.save()

        except Exception as err:
            print '@@@@@buy_lesson : ', str(err)
            result = False
        finally:
            return result
