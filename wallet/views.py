#-*- encoding: utf-8 -*-
###############################################################################
# filename    : wallet > views.py
# description : wallet app views 파일
# author      : msjang@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 msjang 최초 작성
#
#
###############################################################################
import json

from django.http.response import HttpResponse, JsonResponse, \
    HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt

import walletService
from wallet.service import WalletService
from common import common
from common.common import tellpin_login_required
from django.core.serializers.json import DjangoJSONEncoder
from user_auth.service import UserAuthService


# charge wallet
@csrf_exempt
@tellpin_login_required
def buy_coin(request):
    """
    @summary: buy wallet
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData['isSuccess'] = 1
#     user_id = request.user.id
#     
#     post_dict = json.loads(request.body)
#     # TODO: 무슨 역할 하는지 확인 필요함
#     resData["message"] = cs.set_coin(user_id, post_dict)

    return JsonResponse(resData)


# wallet initial data
@csrf_exempt
@tellpin_login_required
def get_wallet_history(request):
    """
    @summary: wallet history list
    @author: msjang
    @param none
    @return: json
    """
    # resoponse json
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id

    # get request parameter
    # post_dict = json.loads(request.body)
#     resData["balance_history"] = cs.get_balance_history(user_id)
#     resData["coin_history"] = cs.get_coin_history(user_id)
    resData["balance_history"] = WalletService.get_balance_history(user_id)
    resData["coin_history"] = WalletService.get_wallet_history(user_id)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def get_wallet_history_pop(request):
    """
    @summary: wallet history pop
    @author: msjang
    @param request
    @return: json
    """
    # resoponse json
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
#     resData["coin_history"] = cs.get_coin_history_pop(user_id)
#     resData["balance_history"] = cs.get_balance_history(user_id)
    resData["coin_history"] = WalletService.get_wallet_history_pop(user_id)
    resData["balance_history"] = WalletService.get_balance_history(user_id)

    return JsonResponse(resData)


@tellpin_login_required
def tutor_account(request):
    """
    @summary: tutor account 정보
    @author: msjang
    @param request
    @return: url - wallet/account.html
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    resData['is_tutor'] = request.user.tellpinuser.is_tutor

    if not resData['is_tutor']:
        resData['history'] = WalletService.get_tutee_history(user_id)
    else:
        resData['history'] = WalletService.get_tutor_history(user_id)

    resData['tutorAccount'] = WalletService.get_tutor_account(user_id)
    resData['tuteeAccount'] = WalletService.get_tutee_account(user_id)
    resData['user_currency'] = request.session.get('currency_exchange')

    return render_to_response('wallet/account.html', {'data': json.dumps(resData, cls=DjangoJSONEncoder)}, RequestContext(request))


@tellpin_login_required
def buy_credits(request):
    """
    @summary: Buy TellpinCoin
    @author: msjang
    @param request
    @return: url - wallet/buy_credits.html
    """
    resData = {}
    resData["isSuccess"] = 1
    resData['creditInfo'] = WalletService.get_credit_info()
    resData['user_currency'] = request.session.get('currency_exchange')

    return render_to_response('wallet/buy_credits.html', {'data': json.dumps(resData, cls=DjangoJSONEncoder)}, RequestContext(request))

@tellpin_login_required
def pay_creditCard(request):
    """
    @summary: Buy TellpinCoin
    @author: msjang
    @param request
    @return: url - wallet/buy_credits.html
    """
    resData = {}
    resData["isSuccess"] = 1
    resData['creditInfo'] = WalletService.get_credit_info()
    resData['user_currency'] = request.session.get('currency_exchange')

    return render_to_response('wallet/pay_creditcard.html', {'data': json.dumps(resData, cls=DjangoJSONEncoder)}, RequestContext(request))


@tellpin_login_required
def result_credit(request):
    """
    @summary: Paypal 결제 결과 받는 함수
    @author: msjang
    @param request
    @return: redirect - /wallet/
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    tx = request.GET['tx']

    paypal = WalletService.get_paypal_result(tx)
    resData['paypal'] = paypal
    resData['isSuccess'] = WalletService.save_paypal_result(user_id, paypal)
    return HttpResponseRedirect('/wallet/')


@csrf_exempt
@tellpin_login_required
def ajax_tutor_page(request):
    """
    @summary: tutor history list
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    post = json.loads(request.body)
    page = post['page']
    start = post['start']
    end = post['end']
    withdraw = post['isType']
    resData['history'] = WalletService.get_tutor_history(user_id, page, start, end, withdraw)
    resData['checkTutor'] = 1
    resData['checkWithdraw'] = withdraw

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def ajax_tutee_page(request):
    """
    @summary: tutee history list
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id

    post = json.loads(request.body)
    page = post['page']
    start = post['start']
    end = post['end']
    purchase = post['isType']
    resData['history'] = WalletService.get_tutee_history(user_id, page, start, end, purchase)
    resData['checkTutor'] = 0
    resData['checkWithdraw'] = purchase

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def ajax_getTuteeTc(request):
    """
    @summary: 해당 유저  받아오는 함수
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    resData['tuteeTc'] = walletService.getTuteeTc(user_id)

    return JsonResponse(resData)


def open_gift(request):
    """
    @summary: gift page
    @author: msjang
    @param request
    @return: url - wallet/gift.html
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    resData['creditInfo'] = WalletService.get_credit_info()
    resData['availableCredit'] = WalletService.get_tutee_available_balance(user_id)
    resData['user_currency'] = request.session.get('currency_exchange')

    return render_to_response('wallet/gift.html', {'data': json.dumps(resData)}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def send_giftcard(request):
    """
    @summary: gift 보내는 함수
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    user_id = request.user.id
    post = json.loads(request.body)

    if UserAuthService.check_password(request.user, post['pw']):
        gift_info = post['gift']
        gift_info['serial'] = common.serialGenerator()
        resData['isSuccess'] = WalletService.buy_giftcard(user_id, gift_info)
    else:
        resData['isSuccess'] = 0

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def transfer_credit(request):
    """
    @summary: tutor -> tutee 코인 전환
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    user_id = request.user.id
    post = json.loads(request.body)

    if UserAuthService.check_password(request.user, post['pw']):
        resData['checkPw'] = 1
        resData['isSuccess'] = WalletService.transfer_credit(user_id, post['tc'])
    else:
        resData['checkPw'] = 0
        resData['isSuccess'] = 0

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def check_giftcard_code(request):
    """
    @summary: giftcard code check
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    user_id = request.user.id
    post = json.loads(request.body)
    code = post['code']
    gift = WalletService.check_giftcard_code(code)

    if gift['check'] == False or gift['giftUse'] == 1:
        resData['isSuccess'] = 0
    else:
        resData['isSuccess'] = WalletService.redeem_giftcard(user_id, gift['giftId'])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def send_withdraw(request):
    """
    @summary: withdraw 신청
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    post = json.loads( request.body )

    if UserAuthService.check_password(request.user, post['pw']):
        resData['checkPw'] = 1
        resData['isSuccess'] = WalletService.withdraw_credit(user_id, post['withdraw'], post['paypal'])
    else:
        resData['checkPw'] = 0
        resData['isSuccess'] = 0

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def giftCard(request):
    """
    @summary: giftcard page
    @author: msjang
    @param request
    @return: url - wallet/gift_card.html
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    post = json.loads(request.body)

    wallet_history_id = post['teac_id']
    resData['gift'] = WalletService.get_gift_detail(wallet_history_id, user_id)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def get_receipt(request):
    """
    @summary: tutee Receipt 정보
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData['isSuccess'] = 1
    post = json.loads(request.body)

    wallet_history_id = post['teac_id']
    resData['receipt'] = WalletService.get_receipt_detail(wallet_history_id)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def get_reservation(request):
    """
    @summary: reservation 정보
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    post = json.loads(request.body)

    reservation_id = post['rvid']
    wallet_history_id = post['teac_id']
    is_tutor = post['type']
    resData['lesson'] = WalletService.wallet_get_reservation_detail(reservation_id, wallet_history_id, is_tutor, user_id)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def get_lesson(request):
    """
    @summary: lesson 정보
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData['isSuccess'] = 1
    post = json.loads(request.body)

    lesson_id = post['lid']
    wallet_history_id = post['teac_id']
    is_tutor = post['type']
    resData['lesson'] = WalletService.get_lesson(lesson_id, wallet_history_id, is_tutor)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def delete_withdrawal(request):
    """
    @summary: withdraw 요청 취소
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    post = json.loads( request.body )

    wallet_history_id = post['teac_id']
    withdrawal_credit = post['withdraw']
    paypal = post['paypal']
    resData['isSuccess'] = WalletService.cancel_withdrawal(wallet_history_id, user_id, withdrawal_credit, paypal)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def modify_withdrawal(request):
    """
    @summary: withdraw 요청 변경
    @author: msjang
    @param none
    @return: json
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    post = json.loads(request.body)

    wallet_history_id = post['teac_id']
    old_withdrawal_credit = post['oldwithdraw']
    new_withdrawal_credit = post['newwithdraw']
    paypal = post['paypal']
    resData['isSuccess'] = WalletService.modify_withdrawal(wallet_history_id, user_id, old_withdrawal_credit, new_withdrawal_credit, paypal)

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def get_tutor_withdrawal(request):
    """
    @summary: withdraw 요청 변경
    @author: msjang
    @param none
    @return: json
    """
    print "########get_tutor_withdrawal"
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    post = json.loads(request.body)

    wallet_history_id = post['teac_id']
    old_withdrawal_credit = post['oldwithdraw']
    new_withdrawal_credit = post['newwithdraw']
    paypal = post['paypal']
    resData['isSuccess'] = WalletService.modify_withdrawal(wallet_history_id, user_id, old_withdrawal_credit, new_withdrawal_credit, paypal)

    return JsonResponse(resData)