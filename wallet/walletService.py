# -*- coding: utf-8 -*-

###############################################################################
# filename    : wallet > coinService.py
# description : Tellpin 코인 app
# author      : msjang@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 msjang 최초 작성
#
#
###############################################################################

# from common.models import BuyCreditsFeeInfo, Payments, TuteeAccount, \
#     TutorAccount, GiftcardIssue, Reservation, Timezone, Notification
# from django.forms.models import model_to_dict
# import urllib2
# import urllib
# from common.models import TellpinUser
from django.core.paginator import Paginator
from datetime import date, timedelta, datetime
from dateutil.relativedelta import relativedelta

from tutor.models import Tutee
from tutor.service import TutorService
from common import common
from django.utils import timezone
from tellpin.settings import DEFAULT_FROM_EMAIL, TELLPIN_HOME
from django.core.mail.message import EmailMessage
from django.db import connection
from user_auth import utils


# Tellpin 코인 충전 가격 정보 리스트
from user_auth.models import TellpinUser


def getCreditInfo():
    """
    @summary: 코인 충전 가격 정보 리스트
    @author: msjang
    @param none
    @return: result( list )
    """
    result = []
    try:
        buys = BuyCreditsFeeInfo.objects.all()
        for i in buys:
            buy = model_to_dict(i)
            result.append(buy)
    except Exception as e:
        result = []
        result['message'] = str(e)
    finally:
        return result


# Paypal 결제 요청 후 결제 결과 받는 함수
# tx : 해당 결제에 대한 고유번호
def getPayResult(tx):
    """
    @summary: sample code 목록
    @author: whcho
    @param tx : 해당 결제에 대한 고유번호 ( paypal 에서 제공 )
    @return: pay( object )
    """
    token = 'BpvdOvhehk-q8ikX5NDX_cgUP0pHd1l3JenkEoptV5PgefMjrDUe3WQs4x8'  # paypal Token ( paypal 측에서 제공 )
    paypalUrl = 'https://www.paypal.com/cgi-bin/webscr'  # paypal URL
    post_dict = dict()
    post_dict['cmd'] = '_notify-synch'
    post_dict['tx'] = tx
    post_dict['at'] = token
    result = urllib2.urlopen(paypalUrl, urllib.urlencode(post_dict))  # paypal 측에 url request를 통해 해당 거래에 대한 정보 받아옴
    content = result.read()
    lines = content.split('\n')
    pay = {}
    pay['result'] = lines[0].strip()

    for line in lines[1:]:
        linesplit = line.split('=', 2)
        if len(linesplit) == 2:
            pay[linesplit[0].strip()] = urllib.unquote(linesplit[1].strip())

    # print pay
    return pay


# 코인충전
# user_id : Tellpinuser 테이블 유저 id 번호
# paypal : getPayResult() 함수 return 값 ( 객체 )
def savePayResult(user_id, paypal):
    """
    @summary: 결제결과에 따라 해당 유저에 tuteeAccount 테이블 insert
    @author: msjang
    @param user_id: 해당 유저 id 번호
    @param paypal: 결제정보 들어있는 객체
    @return: 1( success), 0( fail )
    """
    result = 0
    print type(paypal)
    print paypal
    try:
        # 결제결과가 성공이 아닐 경우 0을 return
        if paypal['result'] != 'SUCCESS':
            result = 0
        # 결제결과가 성공일 경우
        else:
            count = TuteeAccount.objects.filter(user_id=user_id).count()
            # tuteeAccount 테이블에 해당 유저에 대한 raw 가 하나도 없을 경우
            # 마지막 코인 정보에 대한 계산 필요없이 바로 insert
            if count == 0:
                tutee = TuteeAccount(
                    user_id=user_id,
                    a_change=int(paypal['item_name']),
                    p_change=0,
                    t_change=int(paypal['item_name']),
                    total_available=int(paypal['item_name']),
                    total_pending=0,
                    total_balance=int(paypal['item_name']),
                    type=1,
                    more_type=3,
                    more_id=paypal['txn_id'],
                    payment_method=1,
                    created_date=datetime.now())
            # tuteeAccount 테이블에 해당 유저에 대한 raw 가 하나라도 있을 경우
            # 이 유저에 대한 현재 코인정보를 가져와서 계산 후 insert
            else:
                latestTutee = TuteeAccount.objects.filter(user_id=user_id).latest('created_date')
                tutee = TuteeAccount(
                    user_id=user_id,
                    a_change=int(paypal['item_name']),
                    p_change=0,
                    t_change=int(paypal['item_name']),
                    total_available=latestTutee.total_available + int(paypal['item_name']),
                    total_pending=latestTutee.total_pending,
                    total_balance=latestTutee.total_balance + int(paypal['item_name']),
                    type=1,
                    more_type=3,
                    more_id=paypal['txn_id'],
                    payment_method=1,
                    created_date=datetime.now())
            tutee.save()
            result = 1
    except Exception as e:
        print str(e)
        result = 0
    finally:
        return result


# 해당 유저에 TutorAccount 정보
def getTutorAccount(user_id):
    """
    @summary: 해당 유저 Tutor 코인 정보
    @author: msjang
    @param user_id: 해당 유저 id 번호
    @return: obj( object )
    """
    obj = {}
    try:
        count = TutorAccount.objects.filter(user_id=user_id).count()
        # TutorAccount에 raw가 하나도 없을 경우
        if count == 0:
            obj['total_balance'] = 0  # 해당 유저 tutor total balance
            obj['total_available'] = 0
            obj['total_pending'] = 0
            obj['total_withdraw'] = 0
        # TutorAccount에 raw가 있을 경우
        else:
            tutorModel = TutorAccount.objects.filter(user_id=user_id).latest('created_date')
            obj['total_balance'] = tutorModel.total_balance
            obj['total_pending'] = tutorModel.total_pending
            obj['total_available'] = tutorModel.total_available
            obj['total_withdraw'] = tutorModel.total_withdraw_pending
        obj['paypal'] = Payments.objects.get(user_id=user_id).paypal
    except Exception as e:
        obj = {}
        obj['message'] = str(e)
    finally:
        return obj


def getTuteeAccount(user_id):
    """
    @summary: 해당 유저 Tutee 코인 정보
    @author: msjang
    @param user_id: 해당 유저 id 번호
    @return: obj( object )
    """
    obj = {}
    try:
        count = TuteeAccount.objects.filter(user_id=user_id).count()
        if count == 0:
            obj['total_balance'] = 0
            obj['total_available'] = 0
            obj['total_pending'] = 0
        else:
            tuteeModel = TuteeAccount.objects.filter(user_id=user_id).latest('created_date')
            obj['total_balance'] = tuteeModel.total_balance
            obj['total_pending'] = tuteeModel.total_pending
            obj['total_available'] = tuteeModel.total_available
    except Exception as e:
        obj = {}
        obj['message'] = str(e)
    finally:
        return obj


# Tutor History 정보
def getTutorHistory(user_id, page=1, startDay=0, endDay=0, withDraw=0):
    """
    @summary: 해당 유저 Tutor 코인 히스토리
    @author: msjang
    @param user_id: 해당 유저 id 번호
    @param page: 페이지 번호
    @param startDay: 검색 조건 시작 날짜
    @param endDay : 검색 조건 종료 날짜
    @param withDraw : 0 - 모든내역, 1 - withdraw내역 
    @return: obj( object )
    """
    obj = {}
    per_page = 10
    start = (page - 1) * per_page
    end = start + per_page
    try:
        # 개인 UTC 적용
        utcId = TellpinUser.objects.get(id=user_id).timezone
        utc = Timezone.objects.get(id=utcId).utc_time
        utcTime = int(utc[:3])
        if utc[4:] == '30':
            if utc[0] == '-':
                utcTime -= 0.5
            else:
                utcTime += 0.5
        print 'UTC :', utcTime
        if startDay == 0 and endDay == 0:
            endDay = datetime.utcnow()
            print 'endDay', endDay
            startDay = endDay + relativedelta(months=-1)
            endDay = endDay + relativedelta(days=1)
            utcFlag = 0
        else:
            list = endDay.split('-')
            startList = startDay.split('-')
            endDay = datetime(int(list[0]), int(list[1]), int(list[2]))
            startDay = datetime(int(startList[0]), int(startList[1]), int(startList[2]))
            endDay += relativedelta(days=1)
            startDay -= timedelta(hours=utcTime)
            endDay -= timedelta(hours=utcTime)
            utcFlag = 1
        print startDay
        print endDay
        # endDay = endDay.date()
        # startDay = startDay.date()
        if withDraw == 0:
            accounts = TutorAccount.objects.filter(user_id=user_id, created_date__range=(startDay, endDay)).order_by(
                '-created_date')[start: end]
            total = TutorAccount.objects.filter(user_id=user_id, created_date__range=(startDay, endDay)).count()
        else:
            accounts = TutorAccount.objects.filter(user_id=user_id, type__in=[5, 6, 7],
                                                   created_date__range=(startDay, endDay)).order_by('-created_date')[
                       start: end]
            total = TutorAccount.objects.filter(user_id=user_id, type__in=[5, 6, 7],
                                                created_date__range=(startDay, endDay)).count()
        print accounts.query
        endDay = endDay + relativedelta(days=-1)
        if utcFlag == 1:
            startDay += timedelta(hours=utcTime)
            endDay += timedelta(hours=utcTime)
        # 페이징
        pg = TutorService(object).paging(page, total, per_page)
        obj['page_range'] = range(pg['startPage'], pg['endPage'] + 1)
        obj['has_next'] = pg['has_next']
        obj['has_prev'] = pg['has_prev']
        obj['startPage'] = pg['startPage']
        obj['endPage'] = pg['endPage']
        obj['page'] = page
        item = []
        for account in accounts:
            # print account.created_date + timedelta( hours = utcTime )
            account.created_date = str((account.created_date + timedelta(hours=utcTime)).date())
            item.append(model_to_dict(account))
        obj['item'] = item
        obj['startDate'] = str(startDay.date())
        obj['endDate'] = str(endDay.date())
    except Exception as e:
        obj = {}
        obj['message'] = str(e)
    finally:
        return obj


def getTuteeHistory(user_id, page=1, startDay=0, endDay=0, withDraw=0):
    """
    @summary: 해당 유저 Tutee 코인 히스토리
    @author: msjang
    @param user_id: 해당 유저 id 번호
    @param page: 페이지 번호
    @param startDay: 검색 조건 시작 날짜
    @param endDay : 검색 조건 종료 날짜
    @param withDraw : 0 - 모든내역, 1 - withdraw내역
    @return: obj( object )
    """
    obj = {}
    per_page = 10
    start = (page - 1) * per_page
    end = start + per_page
    try:
        # 개인 UTC 적용
        utc_time = Tutee.objects.select_related('timezone_id').get(user_id=user_id).timezone_id.utc_delta
        # utc = Timezone.objects.get(id=utcId).utc_time
        # utcTime = int(utc[:3])
        # if utc[4:] == '30':
        #     if utc[0] == '-':
        #         utcTime -= 0.5
        #     else:
        #         utcTime += 0.5
        # print 'UTC :', utcTime
        if startDay == 0 and endDay == 0:
            endDay = datetime.utcnow()
            startDay = endDay + relativedelta(months=-1)
            endDay = endDay + relativedelta(days=1)
            utcFlag = 0
        else:
            list = endDay.split('-')
            startList = startDay.split('-')
            endDay = datetime(int(list[0]), int(list[1]), int(list[2]))
            startDay = datetime(int(startList[0]), int(startList[1]), int(startList[2]))
            # 개인 UTC 적용
            endDay += relativedelta(days=1)
            startDay -= timedelta(hours=utc_time)
            endDay -= timedelta(hours=utc_time)
            utcFlag = 1
        print startDay
        print endDay
        # startDay = startDay.date()
        # endDay = endDay.date()
        if withDraw == 0:
            accounts = TuteeAccount.objects.filter(user_id=user_id, created_date__range=(startDay, endDay)).order_by(
                '-created_date')[start: end]
            total = TuteeAccount.objects.filter(user_id=user_id, created_date__range=(startDay, endDay)).count()
        else:
            accounts = TuteeAccount.objects.filter(user_id=user_id, created_date__range=(startDay, endDay),
                                                   type=1).order_by('-created_date')[start: end]
            total = TuteeAccount.objects.filter(user_id=user_id, type=1, created_date__range=(startDay, endDay)).count()
        print accounts.query
        endDay = endDay + relativedelta(days=-1)
        if utcFlag == 1:
            startDay += timedelta(hours=utc_time)
            endDay += timedelta(hours=utc_time)
        # 페이징
        pg = TutorService(object).paging(page, total, per_page)
        obj['page_range'] = range(pg['startPage'], pg['endPage'] + 1)
        obj['has_next'] = pg['has_next']
        obj['has_prev'] = pg['has_prev']
        obj['startPage'] = pg['startPage']
        obj['endPage'] = pg['endPage']
        obj['page'] = page
        item = []

        for account in accounts:
            print account.created_date + timedelta(hours=utcTime)
            account.created_date = str((account.created_date + timedelta(hours=utcTime)).date())
            item.append(model_to_dict(account))
        obj['item'] = item
        obj['startDate'] = str(startDay.date())
        obj['endDate'] = str(endDay.date())
    except Exception as e:
        # obj = {}
        obj['message'] = str(e)
    finally:
        return obj


def getTuteeTc(user_id):
    """
    @summary: 해당 유저 사용가능한 Tutee 코인 금액 가져오는 함수
    @author: msjang
    @param user_id: 해당 유저 id 번호
    @return: result( 사용가능한 코인 금액 )
    """
    result = 0
    try:
        result = common.getTuteeTotalAvailable(user_id)
    except Exception as e:
        print str(e)
        result = -1
    finally:
        return result


def saveGift(user_id, obj):
    """
    @summary: Tellpin Coin 선물 함수
    @author: msjang
    @param user_id: 해당 유저 id
    @param obj: 선물정보 들어있는 객체
    @return: 1( success ), 0( fail )
    """
    result = 1
    try:
        giftCard = GiftcardIssue()
        for field in obj:
            setattr(giftCard, field, obj[field])
        setattr(giftCard, 'user_id', user_id)
        setattr(giftCard, 'buy_date', datetime.now())
        giftCard.save()
        giftCardId = GiftcardIssue.objects.filter(user_id=user_id).latest('buy_date').issue_id
        available = common.getTuteeTotalAvailable(user_id)
        balance = common.getTuteeTotalBalance(user_id)
        pending = common.getTuteeTotalPending(user_id)
        print 'now :', datetime.utcnow()
        account = TuteeAccount(
            user_id=user_id,
            a_change=-(obj['tc']),
            p_change=0,
            t_change=-(obj['tc']),
            total_available=available - obj['tc'],
            total_pending=pending,
            total_balance=balance - obj['tc'],
            type=7,
            more_type=4,
            more_id=giftCardId,
            payment_method=0,
            created_date=datetime.now()
        )
        account.save()
        account_id = TuteeAccount.objects.all().latest('teac_id').teac_id
        Notification(from_user=user_id, to_user=user_id, type=7, type_msg=1, ntid=giftCard.issue_id,
                     redirect_url='/wallet/').save()

        serial = obj['serial']
        serial1 = serial[:4]
        serial2 = serial[4:8]
        serial3 = serial[8:12]
        serial4 = serial[12:]
        # serial = serial1 + ' ' + serial2 + ' ' + serial3 + ' ' + serial4

        # 보내는사람한테 보내는 메일
        fromUser = TellpinUser.objects.get(id=user_id)
        if fromUser.profile_photo == '' or fromUser.profile_photo is None:
            userPhoto = 'static/img/user_auth/icon_person.png'
        else:
            userPhoto = 'static/img/upload/profile/' + fromUser.profile_photo
        subject, from_email, to = '[Tellpin] You received a gift from %s' % obj['from_name'], DEFAULT_FROM_EMAIL, obj[
            'to_email']
        '''
        html_content = ''\
        '<table style="width: 542px; height: 345px; position: relative;  background-image: url(\'%sstatic/img/wallet/tellpin_search_coin_gift_card.png\'); background-repeat: no-repeat;">'\
        '    <tr> '\
        '        <td style="padding: 63px 0px 0px 27px; z-index: 2; position: relative;"> '\
        '           <p style="font-size: 22px; margin-bottom: 16px;">To %s</p> '\
        '           <p style="margin-bottom: 20px; margin-left: 31px;">%s</p> '\
        '           <p style="text-align: right;margin-right: 63px;margin-bottom: 10px;">From %s</p> '\
        '           <p style="font-size: 30px; margin-bottom: 20px; margin-left: 5px;">%s Tellpin Credits</p> '\
        '           <p style="margin-left: 31px; margin-bottom: 23px;">Gift Code : %s</p> '\
        '           <p style="margin-left: 35px;"> '\
        '               Gift Cards may be redeemed for Tellpin Credits. <br /> '\
        '               If you want to redeem this code, click <a href="%s">here.</a> '\
        '           </p> '\
        '       </td> '\
        '   </tr> '\
        '</table> ' % ( TELLPIN_HOME, obj['to_name'], obj['content'], obj['from_name'], obj['tc'], serial, TELLPIN_HOME  )
        '''
        html_content = '''
            <center style="background-color: rgb( 238, 238, 238 );">
                <table style="width: 550px; font-size: 13px; background-color: #fff; padding: 20px; border: 2px solid rgb( 213, 213, 213 );">
                    <tr>
                        <td colspan="2" style="padding-bottom: 15px; border-bottom: 1px solid #dcdcdc;">
                            <img src="%sstatic/img/mainpage/tellpin_header_logo_title_beta.png" alt="logo" style="width: 120px;" width="120" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px; padding-bottom: 15px;">
                            %s sent you a Tellpin Gift card for %s Tellpin Credits (TC)! <br />
                            Tellpin Credits are fixed to the US Dollar: 10 TC = 1 USD. With these, you can receive <br />
                            1-on-1 online language lessons from native speakers, make a translation/proofreading request, etc. at <a target="_blank" href="%s">tellpin.com</a>
                        </td>
                    </tr>
                    <tr>
                        <td width="80" style="width: 80px;">
                            <img src="%s%s" alt="user_photo" width="80" height="80" style="border-radius: 20px;" />
                        </td>
                        <td style="vertical-align: middle; padding-left: 15px;">
                            <span style="font-weight: bold; font-size: 16px;">%s</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;">
                            Dear. %s <br /><br />
                            %s
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px; padding-bottom: 10px;">
                            <img src="%sstatic/img/wallet/giftcard.png" alt="giftcard" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span style="font-weight: bold; margin-left: 10px;">%s TC</span>
                            <span style="background-color: rgb( 48, 102, 109 ); color: #fff; border: 1px solid rgb( 48, 102, 109 ); display: inline-block; padding: 3px 5px; margin-left: 13px;">CODE</span><span style="border: 1px solid rgb( 48, 102, 109 ); display: inline-block; padding: 3px 5px;">%s</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;">
                            You must be logged in to a Tellpin account to redeem this card. <br />
                            Please enter the Redeem Code <a target="_blank" href="%swallet/tutee_account/gift/">here.</a> If you have any problems or questions, <br />
                            please contact support@tellpin.com
                        </td>
                    </tr>
                </table>
                </center>
        ''' % (TELLPIN_HOME, obj['from_name'], obj['tc'],
               TELLPIN_HOME, TELLPIN_HOME, userPhoto, obj['from_name'],
               obj['to_name'], obj['content'], TELLPIN_HOME,
               obj['tc'], obj['serial'], TELLPIN_HOME)
        try:
            msg = EmailMessage(subject, html_content, from_email, [to])
            msg.content_subtype = 'html'
            msg.send()
        except Exception as e:
            print str(e)

        subject, from_email, to = '[Tellpin] Your purchased Tellpin Gift Card was sent to %s(%s)' % (
        obj['to_name'], obj['to_email']), DEFAULT_FROM_EMAIL, fromUser.email
        html_content = '''
            <center style="background-color: rgb( 238, 238, 238 );">
                <table style="width: 550px; font-size: 13px; background-color: #fff; padding: 20px; border: 2px solid rgb( 213, 213, 213 );">
                    <tr>
                        <td colspan="2" style="padding-bottom: 15px; border-bottom: 1px solid #dcdcdc;">
                            <img src="%sstatic/img/mainpage/tellpin_header_logo_title_beta.png" alt="logo" style="width: 120px;" width="120" />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 15px; padding-bottom: 15px;">
                            You successfully bought a Gift Card for %s Tellpin Credits (TC). <br />
                            We just sent out the gift to %s as below.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>Gift Card code for reference : %s-%s-%s-%s</span><br />
                            <span>Recipient: %s</span><br />
                            <span>Transaction #: <a target="_blank" href="%swallet/">#%s</a></span><br />
                            <span>Amout : USD %s</span><br />
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 15px;">
                            If you have any questions with the order, please contact support@tellpin.com
                        </td>
                    </tr>
                </table>
            </center>
        ''' % (TELLPIN_HOME, obj['tc'], obj['to_name'],
               serial1, serial2, serial3, serial4,
               obj['to_name'], TELLPIN_HOME, account_id,
               str(obj['tc'] / 10))
        msg = EmailMessage(subject, html_content, from_email, [to])
        msg.content_subtype = 'html'
        msg.send()
    except Exception as e:
        print str(e)
        result = 0
    finally:
        return result


def transferCredit(user_id, tc):
    """
    @summary: Tutor 코인 -> Tutee 코인 전환하는 함수
    @author: msjang
    @param user_id: 해당 유저 id 번호
    @param tc: 변환할 코인 금액
    @return: 1( success ), 0( fail )
    """
    result = 1
    try:
        ttAvailable = common.getTutorTotalAvailable(user_id)
        ttPending = common.getTutorTotalPending(user_id)
        ttBalance = common.getTutorTotalBalance(user_id)
        ttWidthdraw = common.getTutorTotalWithPending(user_id)
        teAvailable = common.getTuteeTotalAvailable(user_id)
        tePending = common.getTuteeTotalPending(user_id)
        teBalance = common.getTuteeTotalBalance(user_id)

        ttAccount = TutorAccount(
            user_id=user_id,
            a_change=-(tc),
            p_change=0,
            t_change=-(tc),
            total_available=ttAvailable - tc,
            total_pending=ttPending,
            total_balance=ttBalance - tc,
            type=4,
            more_type=5,
            more_id='trans',
            total_withdraw_pending=ttWidthdraw
        ).save()
        teAccount = TuteeAccount(
            user_id=user_id,
            a_change=tc,
            p_change=0,
            t_change=tc,
            total_available=teAvailable + tc,
            total_pending=tePending,
            total_balance=teBalance + tc,
            type=7,
            more_type=4,
            more_id='trans',
            payment_method=0,
            created_date=datetime.utcnow()
        ).save()
    except Exception as e:
        print str(e)
        result = 0
    finally:
        return result


def checkGiftCode(code):
    """
    @summary: 기프트 코드 유효한지 검사하는 함수
    @author: msjang
    @param code: 기프트 코드
    @return: obj( object )
    """
    obj = {}
    try:
        obj['check'] = GiftcardIssue.objects.filter(serial=code).count()
        if obj['check'] != 0:
            gift = GiftcardIssue.objects.get(serial=code)
            obj['giftTc'] = gift.tc
            obj['giftId'] = gift.issue_id
            obj['giftUse'] = gift.use_date
    except Exception as e:
        obj['message'] = str(e)
    finally:
        return obj


def checkPassword(user_id, pw):
    """
    @summary: 유저 password 체크하는 함수
    @author: msjang
    @param user_id: 해당 유저 id 번호
    @param pw: 해당 유저 pw
    @return: 1( success ), 0( fail )
    """
    result = 1
    try:
        pw = utils.encrypt_password(pw)
        result = TellpinUser.objects.filter(user=user_id, pw=pw).count()
    except Exception as e:
        result = 0
        print str(e)
    finally:
        return result


def receiveGift(user_id, giftId):
    """
    @summary: TuteeAccount 테이블에 선물로 받은 코인 내역 insert
    @author: msjang
    @param user_id: 해당 유저 id 번호
    @param giftId: 선물받은 GiftcardIssue id
    @return: 1( success ), 0( fail )
    """
    result = 1
    try:
        available = common.getTuteeTotalAvailable(user_id)
        balance = common.getTuteeTotalBalance(user_id)
        pending = common.getTuteeTotalPending(user_id)

        gift = GiftcardIssue.objects.get(issue_id=giftId)
        giftTc = gift.tc
        gift.use_date = datetime.now()
        gift.save()

        account = TuteeAccount(
            user_id=user_id,
            a_change=giftTc,
            p_change=0,
            t_change=giftTc,
            total_available=available + giftTc,
            total_pending=pending,
            total_balance=balance + giftTc,
            type=8,
            more_type=4,
            more_id=giftId,
            payment_method=0,
            created_date=datetime.now()
        ).save()
    except Exception as e:
        print str(e)
        result = 0
    finally:
        return result


def sendWithdraw(user_id, withdraw, paypal, type=1):
    """
    @summary: withdraw 신청 함수
    @author: msjang
    @param user_id: 해당 유저 id 번호
    @param withdraw: withdraw 금액
    @param paypal: paypal 계정
    @param type: 해당 raw 타입
    @return: 1( success ), 0( fail )
    """
    result = 1
    try:
        available = common.getTutorTotalAvailable(user_id)
        balance = common.getTutorTotalBalance(user_id)
        pending = common.getTutorTotalPending(user_id)
        withPending = common.getTutorTotalWithPending(user_id)

        account = TutorAccount(
            user_id=user_id,
            a_change=-(withdraw),
            p_change=0,
            t_change=-(withdraw),
            total_available=available - withdraw,
            total_pending=pending,
            total_balance=balance - withdraw,
            total_withdraw_pending=withPending + withdraw,
            type=5,
            more_type=5,
            more_id=paypal,
            created_date=datetime.now(),
            withdraw_status=1
        ).save()
    except Exception as e:
        result = 0
        print str(e)
    finally:
        return result


def getReceipt(teac_id):
    """
    @summary: TuteeAccount 조회 함수
    @author: msjang
    @param teac_id: TuteeAccount테이블 pk  
    @return: obj( object )
    """
    obj = {}
    try:
        receipt = TuteeAccount.objects.get(teac_id=teac_id)
        obj['receipt'] = model_to_dict(receipt)
        obj['receipt']['name'] = TellpinUser.objects.get(id=receipt.user_id).name
        obj['buy'] = {}
        try:
            buy = BuyCreditsFeeInfo.objects.get(tc=receipt.a_change)
            obj['buy'] = model_to_dict(buy)
        except BuyCreditsFeeInfo.DoesNotExist:
            obj['buy']['id'] = 0
            obj['buy']['tc'] = receipt.a_change
            obj['buy']['usd'] = receipt.a_change / 10
            obj['buy']['creditcard_fee'] = None
            obj['buy']['paypal_fee'] = None
        # utc 적용
        utcId = TellpinUser.objects.get(id=receipt.user_id).timezone
        utc = Timezone.objects.get(id=utcId).utc_time
        print 'utc before', str(obj['receipt']['created_date'])
        utcTime = int(utc[:3])
        if utc[4:] == '30':
            if utc[0] == '-':
                utcTime -= 0.5
            else:
                utcTime += 0.5
        obj['receipt']['created_date'] = str(obj['receipt']['created_date'] + timedelta(hours=utcTime))[:19]
        print 'utc after', obj['receipt']['created_date']
    except Exception as e:
        obj['message'] = str(e)
    finally:
        return obj


def getReservation(rvid, teac_id, type, user_id):
    """
    @summary: 강의예약정보 조회 함수
    @author: msjang
    @param rvid: Reservation 테이블 pk
    @param teac_id: TuteeAccount 테이블 pk
    @param type: 요청분류( Tutor or Tutee )
    @param user_id: 해당 유저 id 번호
    @return: obj( object )
    """
    obj = {}
    try:
        cursor = connection.cursor()
        if type == 'tutor':
            sql = '''
                SELECT u.name AS you_name, l.title AS title, r.rv_time AS time
                     , r.duration AS duration, r.rvid AS rvid
                     , r.is_bundle
                     , r.bundle_times
                     , r.rv_orders
                     , 'tutor' AS type
                     , CONCAT( DATE_ADD( ( SELECT created_date FROM tutor_account WHERE teac_id = %s ), INTERVAL( SELECT y.utc_time FROM timezone y WHERE id = ( SELECT timezone FROM user_auth_tellpinuser WHERE id = r.tutor_id  ) ) HOUR ) ) AS updated_time
                     , ( SELECT name FROM user_auth_tellpinuser WHERE id = r.tutee_id ) AS name
                     , CASE WHEN r.duration = 30 AND r.is_bundle = 0 
                            THEN FLOOR(l.online_30m_1times_cost) 
                            WHEN r.duration = 30 AND r.is_bundle = 1 AND r.bundle_times = 5 
                            THEN FLOOR(l.online_30m_5times_cost / 5) 
                            WHEN r.duration = 30 AND r.is_bundle = 1 AND r.bundle_times = 10 
                            THEN FLOOR(l.online_30m_10times_cost / 10) 
                            WHEN r.duration = 60 AND r.is_bundle = 0 
                            THEN FLOOR(l.online_60m_1times_cost) 
                            WHEN r.duration = 60 AND r.is_bundle = 1 AND r.bundle_times = 5 
                            THEN FLOOR(l.online_60m_5times_cost / 5) 
                            WHEN r.duration = 60 AND r.is_bundle = 1 AND r.bundle_times = 10 
                            THEN FLOOR(l.online_60m_10times_cost / 10) 
                            ELSE 0 
                            END AS cost 
                FROM lesson l, reservation r, user_auth_tellpinuser u
                WHERE l.lid = r.lid
                  AND r.tutor_id = u.id AND r.rvid = %s
            '''
        else:
            sql = '''
                SELECT u.name AS name, l.title AS title, r.rv_time AS time
                     , r.duration AS duration, r.rvid AS rvid
                     , r.is_bundle
                     , r.bundle_times
                     , r.rv_orders
                     , 'tutee' AS type
                     , CONCAT( DATE_ADD( ( SELECT created_date FROM tutor_account WHERE teac_id = %s ), INTERVAL( SELECT y.utc_time FROM timezone y WHERE id = ( SELECT timezone FROM user_auth_tellpinuser WHERE id = r.tutor_id  ) ) HOUR ) ) AS updated_time
                     , ( SELECT name FROM user_auth_tellpinuser WHERE id = r.tutor_id ) AS you_name
                     , CASE WHEN r.duration = 30 AND r.is_bundle = 0 
                            THEN FLOOR(l.online_30m_1times_cost) 
                            WHEN r.duration = 30 AND r.is_bundle = 1 AND r.bundle_times = 5 
                            THEN FLOOR(l.online_30m_5times_cost / 5) 
                            WHEN r.duration = 30 AND r.is_bundle = 1 AND r.bundle_times = 10 
                            THEN FLOOR(l.online_30m_10times_cost / 10) 
                            WHEN r.duration = 60 AND r.is_bundle = 0 
                            THEN FLOOR(l.online_60m_1times_cost) 
                            WHEN r.duration = 60 AND r.is_bundle = 1 AND r.bundle_times = 5 
                            THEN FLOOR(l.online_60m_5times_cost / 5) 
                            WHEN r.duration = 60 AND r.is_bundle = 1 AND r.bundle_times = 10 
                            THEN FLOOR(l.online_60m_10times_cost / 10) 
                            ELSE 0 
                            END AS cost 
                FROM lesson l, reservation r, user_auth_tellpinuser u
                WHERE l.lid = r.lid
                  AND r.tutee_id = u.id AND r.rvid = %s
            '''
        cursor.execute(sql, [teac_id, rvid])
        row = common.dictfetchall(cursor)
        obj = row[0]
        year = obj['time'][:4]
        month = obj['time'][4:6]
        day = obj['time'][6:8]
        hour = obj['time'][9:11]
        min = obj['time'][11:]
        obj['start'] = datetime.strptime(year + '-' + month + '-' + day + ' ' + hour + ':' + min, '%Y-%m-%d %H:%M')
        print 'utc before', obj['start']
        # 개인 UTC 적용
        utcId = TellpinUser.objects.get(id=user_id).timezone
        utc = Timezone.objects.get(id=utcId).utc_time
        utcTime = int(utc[:3])
        if utc[4:] == '30':
            if utc[0] == '-':
                utcTime -= 0.5
            else:
                utcTime += 0.5
        obj['start'] = obj['start'] + timedelta(hours=utcTime)
        print 'utc after', obj['start']
        obj['rvid'] = "R" + str('%011d' % int(obj['rvid']))
        obj['teac_id'] = 'P' + str('%011d' % teac_id)
        if obj['duration'] == 60:
            obj['end'] = obj['start'] + timedelta(hours=1)
        else:
            obj['end'] = obj['start'] + timedelta(minutes=30)
        if type == 'tutee':
            obj['time'] = TuteeAccount.objects.get(teac_id=teac_id).created_date
        else:
            obj['time'] = TutorAccount.objects.get(teac_id=teac_id).created_date
        obj['start'] = str(obj['start'])
        obj['end'] = str(obj['end'])
    except Exception as e:
        print str(e)
        obj['log'] = str(e)
    finally:
        cursor.close()
        return obj


def getLesson(lid, teac_id, type):
    """
    @summary: 강의 정보 조회
    @author: msjang
    @param lid: Lesson 테이블 pk
    @param teac_id: TuteeAccount 테이블 pk
    @param type: 요청 분류( Tutor or Tutee )
    @return: obj( object )
    """
    obj = {}
    try:
        cursor = connection.cursor()
        if type == 'tutor':
            sql = '''
                SELECT u.name AS name, l.title AS title
                     , 'tutor' AS type
                     , ( SELECT name FROM user_auth_tellpinuser WHERE id = r.tutee_id ) AS you_name
                     , CASE WHEN r.duration = 30 AND r.is_bundle = 0 
                            THEN FLOOR(l.online_30m_1times_cost) 
                            WHEN r.duration = 30 AND r.is_bundle = 1 AND r.bundle_times = 5 
                            THEN FLOOR(l.online_30m_5times_cost / 5) 
                            WHEN r.duration = 30 AND r.is_bundle = 1 AND r.bundle_times = 10 
                            THEN FLOOR(l.online_30m_10times_cost / 10) 
                            WHEN r.duration = 60 AND r.is_bundle = 0 
                            THEN FLOOR(l.online_60m_1times_cost) 
                            WHEN r.duration = 60 AND r.is_bundle = 1 AND r.bundle_times = 5 
                            THEN FLOOR(l.online_60m_5times_cost / 5) 
                            WHEN r.duration = 60 AND r.is_bundle = 1 AND r.bundle_times = 10 
                            THEN FLOOR(l.online_60m_10times_cost / 10) 
                            ELSE 0 
                            END AS cost 
                FROM lesson l, user_auth_tellpinuser u
                WHERE l.lid = %s
            '''
        else:
            sql = '''
                SELECT u.name AS name, l.title AS title, r.rv_time AS time
                     , r.duration AS duration, r.rvid AS rvid
                     , r.is_bundle
                     , r.bundle_times
                     , r.rv_orders
                     , 'tutee' AS type
                     , CONCAT( DATE_ADD( ( SELECT created_date FROM tutor_account WHERE teac_id = %s ), INTERVAL( SELECT y.utc_time FROM timezone y WHERE id = ( SELECT timezone FROM user_auth_tellpinuser WHERE id = r.tutor_id  ) ) HOUR ) ) AS updated_time
                     , ( SELECT name FROM user_auth_tellpinuser WHERE id = r.tutor_id ) AS you_name
                     , CASE WHEN r.duration = 30 AND r.is_bundle = 0 
                            THEN FLOOR(l.online_30m_1times_cost) 
                            WHEN r.duration = 30 AND r.is_bundle = 1 AND r.bundle_times = 5 
                            THEN FLOOR(l.online_30m_5times_cost / 5) 
                            WHEN r.duration = 30 AND r.is_bundle = 1 AND r.bundle_times = 10 
                            THEN FLOOR(l.online_30m_10times_cost / 10) 
                            WHEN r.duration = 60 AND r.is_bundle = 0 
                            THEN FLOOR(l.online_60m_1times_cost) 
                            WHEN r.duration = 60 AND r.is_bundle = 1 AND r.bundle_times = 5 
                            THEN FLOOR(l.online_60m_5times_cost / 5) 
                            WHEN r.duration = 60 AND r.is_bundle = 1 AND r.bundle_times = 10 
                            THEN FLOOR(l.online_60m_10times_cost / 10) 
                            ELSE 0 
                            END AS cost 
                FROM lesson l, reservation r, user_auth_tellpinuser u
                WHERE l.lid = r.lid
                  AND r.tutee_id = u.id AND r.rvid = %s
            '''
    except Exception as e:
        obj['log'] = str(e)
    finally:
        cursor.close()
        return obj


def deleteWithdraw(teac_id, user_id, withdraw, paypal):
    """
    @summary: withdraw 신청 취소
    @author: msjang
    @param teac_id: TuteeAccount table pk
    @param user_id: 해당 유저 id 번호
    @param withdraw: withdraw 금액
    @param paypal: 해당 유저 paypal ID
    @return: 1( success ), 0( fail )
    """
    result = 1
    try:
        account = TutorAccount.objects.get(teac_id=teac_id)
        account.withdraw_status = 3
        account.save()

        available = common.getTutorTotalAvailable(user_id)
        balance = common.getTutorTotalBalance(user_id)
        pending = common.getTutorTotalPending(user_id)
        withPending = common.getTutorTotalWithPending(user_id)

        account = TutorAccount(
            user_id=user_id,
            a_change=-(withdraw),
            p_change=0,
            t_change=-(withdraw),
            total_available=available - withdraw,
            total_pending=pending,
            total_balance=balance - withdraw,
            total_withdraw_pending=withPending + withdraw,
            type=7,
            more_type=5,
            more_id=paypal,
            created_date=datetime.now(),
            withdraw_status=3
        ).save()
    except Exception as e:
        print str(e)
        result = 0
    finally:
        return result


def modifyWithdraw(teac_id, user_id, old, new, paypal):
    """
    @summary: withdraw 신청 변경
    @author: msjang
    @param teac_id: TuteeAccount table pk
    @param user_id: 해당 유저 id 번호
    @param old: 변경 전 withdraw 금액
    @param new: 변경하고자 하는 withdraw 금액
    @param paypal: 해당 유저 paypal 계정 정보
    @return: 1( success ), 0( fail )
    """
    result = 1
    try:
        deleteWithdraw(teac_id, user_id, old, paypal)
        available = common.getTutorTotalAvailable(user_id)
        balance = common.getTutorTotalBalance(user_id)
        pending = common.getTutorTotalPending(user_id)
        withPending = common.getTutorTotalWithPending(user_id)

        account = TutorAccount(
            user_id=user_id,
            a_change=-(new),
            p_change=0,
            t_change=-(new),
            total_available=available - new,
            total_pending=pending,
            total_balance=balance - new,
            total_withdraw_pending=withPending + new,
            type=5,
            more_type=5,
            more_id=paypal,
            created_date=datetime.now(),
            withdraw_status=1
        ).save()
    except Exception as e:
        print str(e)
        result = 0
    finally:
        return result
