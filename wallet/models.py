# -*- coding: utf-8 -*-
###############################################################################
# filename    : wallet > models.py
# description : Wallet 모델 정의
# author      : hsrjmk@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160831 hsrjmk 최조 작성
#
###############################################################################
from django.db import models
from tutor.models import Tutee, Tutor
from user_auth.models import TellpinUser


TRANSACTION_TYPE = (
    (None, 'Unknown'),
    ('B', 'Bought Credits'),
    ('P', 'Bought a Lesson Package'),
    ('L', 'Bought a Lesson'),
    ('A', 'Approved a Lesson'),
    ('C', 'Confirmed a Lesson'),
    ('RE', 'Lesson Credits Returned from the tutor / Return Lesson Credits to the tutee'),
    ('RP', 'Service Charge / Gave refund for a Lesson Package / Got refund for a Lesson Package'),
    ('RL', 'Got refund for a Lesson / Gave refund for a Lesson'),
    ('WP', 'Withdrawal pending'),
    ('CW', 'Canceled Withdrawal'),
    ('W', 'Credits I cashed out'),
    ('T', 'Transferred Credits'),
    ('G', 'Gave a Gift'),
    ('DG', 'Redeemed a Gift Card Code'),
    ('RC', 'Credits refunded'),
    ('DC', 'Redeemed a Coupon Code'),
    ('MR', 'Used credits for making a request'),
    ('TC', 'Transferred Credits to the Best Answerer'),
    ('NC', 'Best Answer was not chosen'),
    ('BA', 'Chosen as the Best Answerer'),
    ('D', 'Request was deleted'),
)
HISTORY_TYPE = (
    (0, None),
    (1, 'Lesson'),
    (2, 'Receipt'),
    (3, 'Withdrawal'),
    (4, 'Gift'),
    (5, 'Transfer credits'),
    (6, 'Language board'),
    (7, 'Translation & proofreading'),
    (8, 'Language question'),
    (9, 'Coupon'),
)
PAYMENT_TYPE = (
    ('1', 'Paypal'),
    ('2', 'Credit card'),
    ('3', 'Refund'),
)
WITHDRAWAL_STATUS = (
    ('0', None),
    ('1', 'Pending'),
    ('2', 'Complete'),
    ('3', 'Cancel'),
)
GIFTCARD_STATUS = (
    ('0', 'Unused'),
    ('1', 'Used'),
)


class TuteeWalletHistory(models.Model):
    tutee = models.ForeignKey(Tutee)

    transaction_type = models.CharField("Transaction type", max_length=2, choices=TRANSACTION_TYPE, default=None, null=True)
    type = models.PositiveSmallIntegerField("Type", choices=HISTORY_TYPE, default=0, null=True)
    type_ref = models.IntegerField("Reference id: lesson id or Language question or, etc ...", null=True)

    total_delta = models.IntegerField("Total delta value", default=0)
    available_delta = models.IntegerField("Available delta value", default=0)
    pending_delta = models.IntegerField("Pending delta value", default=0)

    total_balance = models.IntegerField("Total balance value", default=0)
    available_balance = models.IntegerField("Available balance value", default=0)
    pending_balance = models.IntegerField("Pending balance value", default=0)

    payment_type = models.PositiveSmallIntegerField("Payment type", choices=PAYMENT_TYPE, default=1)
    payment_code = models.CharField("Payment code like gift-card code, coupon code, etc", max_length=20, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    class Meta:
        db_table = u'tutee_wallet_history'


class TutorWalletHistory(models.Model):
    tutor = models.ForeignKey(Tutor)

    transaction_type = models.CharField("Transaction type", max_length=2, choices=TRANSACTION_TYPE, default=None, null=True)
    type = models.PositiveSmallIntegerField("Type", choices=HISTORY_TYPE, default=0, null=True)
    type_ref = models.IntegerField("Reference id: lesson id or Language question or, etc ...", null=True)

    total_delta = models.IntegerField("Total delta value", default=0)
    available_delta = models.IntegerField("Available delta value", default=0)
    pending_delta = models.IntegerField("Pending delta value", default=0)

    total_balance = models.IntegerField("Total balance value", default=0)
    available_balance = models.IntegerField("Available balance value", default=0)
    pending_balance = models.IntegerField("Pending balance value", default=0)
    total_withdrawal_pending = models.IntegerField("Total withdrawal pending", default=0)
    withdrawal_status = models.PositiveSmallIntegerField("Withdraw status", choices=WITHDRAWAL_STATUS, default=0)
    withdrawal_account = models.CharField("Withdrawal account id like paypal id", max_length=254, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    class Meta:
        db_table = u'tutor_wallet_history'


class Giftcard(models.Model):
    user = models.ForeignKey(TellpinUser)
    tc = models.IntegerField('Tellpin credit to gift', default=0)
    gift_code = models.CharField("Giftcard code", max_length=16, null=True)
    status = models.PositiveSmallIntegerField("Giftcard status", choices=GIFTCARD_STATUS, default=0)

    sender_name = models.CharField("Sender's name", max_length=100, null=True)
    receiver_email = models.EmailField("Receiver's email", null=True)
    receiver_name = models.CharField("Receiver's name", max_length=100, null=True)
    content = models.CharField("Message in giftcard", max_length=350, null=True)
    buy_time = models.DateTimeField("Bought time", null=True)
    use_time = models.DateTimeField("Used time", null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'giftcard'
