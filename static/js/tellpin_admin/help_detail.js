tellpinAdminApp.controller("helpController", function($scope, $http, $location, $window, $sce, $timeout){
	

	$scope.initHelpData = function(help_id){
		console.log("init help data");
		console.log('help_id');
		$scope.setPostData(help_id);
		
		
	};
	

	$scope.setPostData = function (help_id){
		$http.post("/admin/helpDetail/", {help_id: help_id}).success(function (result){
			var data = result;
			console.log(data);
			$scope.from = data.from;
			$scope.about = data.about;
			$scope.title = data.title;
			$scope.description = data.description;
			$scope.fid = data.fid;
			$scope.filename = data.filename;
			$scope.qid = data.qid;
			var div = document.createElement("div");
			div.innerHTML = $scope.description;
			$scope.description = div.textContent || div.innerText || "";
			
		});
	}
	

		$scope.submitAnswer = function(qid) {
		console.log("submit answer");
		console.log(qid);
		var el = document.getElementById('Answers');

		// get reference to input elements in toppings container element
		var tops = el.getElementsByTagName('input');
		var answer = "";
		for (var i=0, len=tops.length; i<len; i++) {
	        if ( tops[i].type == 'text' ) {
	        	answer = tops[i].value;
	        }
		}
		$http.post("/admin/question_answer/", {qid: qid, answer:answer}).success(function(result) {
			if (result.isSuccess == 1) {
				console.log("saved the answer for the question");
			}
		});

	}
	
});