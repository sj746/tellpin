tellpinAdminApp.controller("lessonClaimController", function($scope, $http, $location, $window, $sce, $timeout){
	$scope.initClaimData = function(reservation_id){
		console.log("init claim data");
		$scope.setClaimData(reservation_id);
	};
	
	$scope.Resolve = function (){
		console.log("claim resolve");
		var el = document.getElementById('Claims');

		// get reference to input elements in toppings container element
		var tops = el.getElementsByTagName('input');
		for (var i=0, len=tops.length; i<len; i++) {
	        if ( tops[i].type == 'radio' ) {
	            if(tops[i].checked){
	            	if (tops[i].value == "PartialRefunded"){
	            		var count = 1;
	            		var tc1, tc2;
	            		for (var k=0, len=tops.length; k<len; k++) {
	            			if(tops[k].type == "text"){
	            				if (tops[k].name == "TCamount1"){
	            					tc1 = tops[k].value;
	            				} else {
	            					tc2 = tops[k].value;
	            				}
	            			}
	            		} // should have tc1 and tc2 for refunding
	            		$http.post("/admin/resolveClaim/", {claim_id: $scope.claim_id, confirm:3, tc1: tc1, tc2: tc2}).success(function (result){
	            			console.log("resolveClaim");
	            		});
	            	} else if (tops[i].value == "Confirmed"){
	            		// transfer all the tutor
	            		$http.post("/admin/resolveClaim/", {claim_id: $scope.claim_id, confirm: 1}).success(function (result){
	            			console.log("resolveClaim");
	            		});
	            	} else {
	            		// return all credit to tutee
	            		$http.post("/admin/resolveClaim/", {claim_id: $scope.claim_id, confirm: 0}).success(function (result){
	            			console.log("resolveClaim");
	            		});
	            	}
	            }
	        }
	    }
	}
	
	$scope.setClaimData = function (reservation_id){
		console.log("setClaimData");
		$http.post("/admin/lessonClaimDetail/", {reservation_id: reservation_id}).success(function (result){
			var data = result; // should contain necessary data on claim
			$scope.problems = result.problems.list;
			$scope.rvid = result.rvid;
			$scope.tutor_id = result.tutor_id;
			$scope.tutee_id = result.tutee_id;
		});
	}
	
	$scope.getMessage = function() {
		$scope.messageModel = "";
		var to_user_id = 0;

		if( $scope.list.is_tutor == 1 ){
			to_user_id = $scope.list.tutee_id;
		}else{
			to_user_id = $scope.list.tutor_id;
		}
		var rvid = $scope.list.rvid;
		$http.post( '/admin/getMessage/', { 'tutee_id' : $scope.list.tutee_id, 'tutor_id': $scope.list.tutor_id, 'rvid':rvid})
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				$scope.message = result.message;
				$scope.$broadcast('dataloaded');
				var elem = angular.element( '#message' );
//				$( '#messageModal' ).modal();
				//$scope.$parent.checkMessage();
			}
		});
		//angular.element( '#message' ).scrollTop = 800;
		$timeout(function(){
			$("#message").scrollTop( $("#message")[0].scrollHeight );

		},450);
	
	}
});

