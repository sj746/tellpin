tellpinAdminApp.controller("profileController", function($scope, $http, $location, $window, $sce, $timeout){
	$scope.strings = {};
	$scope.strings.labels = {
			"short_intro": "Short introduction",
			"resume": "Resume (optional)",
			"long_intro": "Long introduction",
			"teaching_special_tags": "Teaching Specialty Tags",
			"back": "Back",
			"continue": "Continue",
			"ex_video_intro_link": "Example http://www.youtube.com/v/235dz8DF?rel=0&modestbranding=1&autohide=1&showinfo=0&controls=0",
			"modal_work_desc": "Ex) I taught adult English language learners with a focus on intermediate level students.\n\n I mainly translated legal and corporate documents.",
			"modal_education_desc": "Ex) Graduated Magna Cum Laude\n Concentration in writing \nOverall GPA 3.8/4.0",
			"modal_certificate_name": "Ex) TESOL, TOEFL",
			"modal_certificate_organization": "Ex) TESL Canada, ETS",
			"modal_certificate_desc": "Ex) iBT Score 117/120",
		};
	

	$scope.getTutorDetail = function(user_id){
		$(location).attr('href', '/admin/tutor_profile/'+user_id+'/');
	}
	
	$scope.emailResponse = function(user_id){
		var el = document.getElementById('Violation');

		// get reference to input elements in toppings container element
		var tops = el.getElementsByTagName('input');
		var email = "";
		for (var i=0, len=tops.length; i<len; i++) {
	        if ( tops[i].type === 'text' ) {
	            email = tops[i].value;
	        }
	    }
		$http.post("/admin/sendEmail/", {user_id: user_id, contents: email}).success(function(result){
			if (result.isSuccess ==1){
				console.log("sent email");
			}
		});
		// need to send email to user
	}
	
	$scope.declineChanges = function(){
		console.log("decline changes");
		// get reference to element containing problem checkboxes
		var el = document.getElementById('Problems');

		// get reference to input elements in toppings container element
		var tops = el.getElementsByTagName('input');
		var checked = false;
		for (var i=0, len=tops.length; i<len; i++) {
	        if ( tops[i].type === 'checkbox' ) {
	            if(tops[i].checked){
	            	checked = true;
	            }
	        }
	    }
		if (!checked){
			alert("Please select a reason for the decline.");
		} else {
			$http.post("/admin/decline_changes/", {data: $scope.profile}).success( function( result ) {
		    	if ( result.isSuccess == 1 ) {
		    		console.log("saved all the necessary data to tutor db");
		    	}
		    });
		}
	}
	
	$scope.approveChanges = function(){
		console.log("submit changes");
		// get reference to element containing problem checkboxes
		var el = document.getElementById('Problems');

		// get reference to input elements in toppings container element
		var tops = el.getElementsByTagName('input');
		var checked = false;
		for (var i=0, len=tops.length; i<len; i++) {
	        if ( tops[i].type === 'checkbox' ) {
	            if(tops[i].checked){
	            	checked = true;
	            }
	        }
	    }
		if (checked){
			alert("You stated that there a problem with the profile. Please check again.");
		}
		else{
			// need to submit the changes to the table - but how do I fetch all the data?
			$http.post("/admin/approve_changes/", {data: $scope.profile}).success( function( result ) {
		    	if ( result.isSuccess == 1 ) {
		    		console.log("saved all the necessary data to tutor db");
		    	}
		    });
		}
	}
	
	$scope.initProfileData = function(user_id){
		console.log("init profile data");
		$scope.nameInfo = false;
		$scope.setData(user_id);
	};
	
	$scope.setData = function(user_id) {
		$scope.loading = true;
		$http.post( "/admin/violationEditProfile/", {user_id: user_id}).success( function( result ) {
			var data = result;
			console.log(data);
			$scope.violation_type = data.violation_type;
			$scope.violation_content = data.violation_content;
			$scope.editMenu = 0;
			$scope.fromCity = [];
			$scope.livinginCity = [];
			$scope.profile = data.profile;
			$scope.tools = data.tools;
			$scope.currencyInfo = data.currencyInfo;
			for(var i = 1; i < $scope.currencyInfo.length; i++) {
				$scope.currencyInfo[i].code = $scope.currencyInfo[i].code + " " + $scope.currencyInfo[i].symbol; 
			}

			$scope.nationsInfo = data.nationsInfo;
			$scope.languageInfo = data.languageInfo;
			$scope.skillLevelInfo = data.skillLevelInfo;
			$scope.timezoneInfo = data.timezoneInfo;
			$scope.setCurrentTime();

			if($scope.profile.from_country_id == null){
				$scope.profile.from_country_id = 0;
				$scope.profile.from_city_id = 0;
			} else {
				$scope.getCityInfo($scope.profile.from_country_id, 'from');
			}
			if($scope.profile.livingin_country_id == null){
				$scope.profile.livingin_country_id = 0;
				$scope.profile.livingin_city_id = 0;
			} else {
				$scope.getCityInfo($scope.profile.livingin_country_id, "livingin");
			}
			if ($scope.profile.short_intro == null) {
				$scope.profile.short_intro = "";
			}
			if ($scope.profile.long_intro == null) {
				$scope.profile.long_intro = "";
			}
			// birthdate options
			$scope.yearList = [];
			for(var year = $scope.profile.server_year; year >= 1917; year--){
				$scope.yearList.push(year);
			}
			$scope.monthList = [];
			for(var month = 1; month <= 12; month++){
				$scope.monthList.push(month);
			}

			$scope.setDateList();
			
			// gender options
			$scope.genderList = ['Select', 'Male', 'Female'];
			if ($scope.profile.gender == null) {
				$scope.profile.gender = 0;
			}
			//language options
			$scope.languageList = ['English', '한국어', '日本語'];
			if ($scope.profile.display_language == null) {
				$scope.profile.display_language = 0;	
			}

			$scope.nationsInfo.splice(0, 0, {"id": 0, "country": "Select Country"});
			$scope.fromCity.splice(0, 0, {"id": 0,"city":"Select City"});
			$scope.livinginCity.splice(0, 0, {"id": 0,"city":"Select City"});

			//currenty
			$scope.currencyInfo.splice(0, 0, {"id": 0,"code":"Select currency"});
			$scope.languageInfo.splice(0, 0, {"id": 0,"lang_english":"Select language"});
			$scope.skillLevelInfo.splice(0,0, {"id": 0,"level":"Select Level"});
			$scope.skillLevelInfo.splice(7, 1);
			
			// language setting
			if($scope.profile.native1_id == null || $scope.profile.native1_id == 0){
				$scope.profile.native1_id = 0;
			}

			if($scope.profile.lang1_id == null || $scope.profile.lang1_id == 0){
				$scope.profile.lang1_id = 0;
				$scope.profile.lang1_level = 0;
			} else {
				if ($scope.profile.lang1_level == null) {
					$scope.profile.lang1_level = 0;
				}
			}

			$scope.profile.native = [];
			$scope.profile.native.push( $scope.profile.native1_id );
			if ( $scope.profile.native2_id != null ) {
				$scope.profile.native.push( $scope.profile.native2_id );
			}
			if ( $scope.profile.native3_id != null ) {
				$scope.profile.native.push( $scope.profile.native3_id );
			}

			if ($scope.profile.currency_id == null) {
				$scope.profile.currency_id = 0;
			}
			$scope.checkPersonalTools();
			// teaching setting
			$scope.addTeachingLanguage();

			//resume
			if( $scope.profile.profile_info != undefined && $scope.profile.profile_info != null) { //저장된 내용이 있다
				$scope.formdata = [];
				$scope.formdata.short_intro = $scope.profile.profile_info.short_intro;
				$scope.formdata.education = $scope.profile.profile_info.education;
				$scope.formdata.work = $scope.profile.profile_info.work;
				$scope.formdata.certification = $scope.profile.profile_info.certificate;
				$scope.formdata.long_intro = $scope.profile.profile_info.long_intro;
				$scope.formdata.video_intro = $scope.profile.profile_info.video_intro;
				$scope.formdata.resumeFile = new FormData();
				
				for(var i=1; i<=3; i++) {
					if( $scope.profile.profile_info["work"+i] != undefined && $scope.profile.profile_info["work"+i] != null ) {
						$scope.formdata["work"+i] = {};
						$scope.formdata["work"+i].info = true;
						$scope.formdata["work"+i].start_year = {val:$scope.profile.profile_info["work"+i].start_year, name: $scope.profile.profile_info["work"+i].start_year};
						$scope.formdata["work"+i].end_year = { val : $scope.profile.profile_info["work"+i].end_year, name: $scope.profile.profile_info["work"+i].end_year};
						$scope.formdata["work"+i].company = { val : $scope.profile.profile_info["work"+i].company, valid: false};
						$scope.formdata["work"+i].country = Number($scope.profile.profile_info["work"+i].country);
						$scope.formdata["work"+i].country_name = $scope.nationsInfo[$scope.profile.profile_info["work"+i].country].country;
						$scope.formdata["work"+i].city = { val : $scope.profile.profile_info["work"+i].city, valid: false};
						$scope.formdata["work"+i].position = { val : $scope.profile.profile_info["work"+i].position, valid: false};
						$scope.formdata["work"+i].description = { val : $scope.profile.profile_info["work"+i].description, valid: false};
						$scope.formdata["work"+i].work_exp_file = $scope.profile.profile_info["work" + i + "_filename"];
						$scope.formdata["work"+i].work_exp_file_path = $scope.profile.profile_info["work" + i + "_filepath"];
						$scope.formdata["work"+i].file_del_yn = false;
						
					}
					else {
						$scope.formdata["work"+i] = {};
						$scope.formdata["work"+i].info = false;
						$scope.formdata["work"+i].start_year = 0;
						$scope.formdata["work"+i].end_year = 0;
						$scope.formdata["work"+i].company = "";
						$scope.formdata["work"+i].country = 0;
						$scope.formdata["work"+i].country_name = "";
						$scope.formdata["work"+i].city = "";
						$scope.formdata["work"+i].position = "";
						$scope.formdata["work"+i].description = "";
						$scope.formdata["work"+i].work_exp_file = null;
						$scope.formdata["work"+i].work_exp_file_path = null;
						$scope.formdata["work"+i].file_del_yn = false;
					}
				}
				
				
				for(var i=1; i<=3; i++) {
					if( $scope.profile.profile_info["certificate"+i] != undefined && $scope.profile.profile_info["certificate"+i] != null ) {
						$scope.formdata["certificate"+i] = {};
						$scope.formdata["certificate"+i].info = true;
						$scope.formdata["certificate"+i].year = {val:$scope.profile.profile_info["certificate"+i].year, name: $scope.profile.profile_info["certificate"+i].year};
						$scope.formdata["certificate"+i].name = { val : $scope.profile.profile_info["certificate"+i].name, valid: false};
						$scope.formdata["certificate"+i].organization = { val : $scope.profile.profile_info["certificate"+i].organization, valid: false};
						$scope.formdata["certificate"+i].description = { val : $scope.profile.profile_info["certificate"+i].description, valid: false};
						$scope.formdata["certificate"+i].certification_file = $scope.profile.profile_info["certificate" + i + "_filename"];
						$scope.formdata["certificate"+i].certification_file_path = $scope.profile.profile_info["certificate" + i + "_filepath"];
						$scope.formdata["certificate"+i].file_del_yn = false;
					}
					else {
						$scope.formdata["certificate"+i] = {};
						$scope.formdata["certificate"+i].info = false;
						$scope.formdata["certificate"+i].year = 0;
						$scope.formdata["certificate"+i].name = "";
						$scope.formdata["certificate"+i].organization = "";
						$scope.formdata["certificate"+i].description = "";
						$scope.formdata["certificate"+i].certification_file = null;
						$scope.formdata["certificate"+i].certification_file_path = null;
						$scope.formdata["certificate"+i].file_del_yn = false;
					}
				}
				
				for(var i=1; i<=3; i++) {
					if( $scope.profile.profile_info["education"+i] != undefined && $scope.profile.profile_info["education"+i] != null ) {
						$scope.formdata["education"+i] = {};
						$scope.formdata["education"+i].info = true;
						$scope.formdata["education"+i].start_year = {val:$scope.profile.profile_info["education"+i].start_year, name: $scope.profile.profile_info["education"+i].start_year};
						$scope.formdata["education"+i].end_year = { val : $scope.profile.profile_info["education"+i].end_year, name: $scope.profile.profile_info["education"+i].end_year};
						$scope.formdata["education"+i].school = { val : $scope.profile.profile_info["education"+i].school, valid: false};
						$scope.formdata["education"+i].location = Number($scope.profile.profile_info["education"+i].location);
						$scope.formdata["education"+i].location_name = $scope.nationsInfo[$scope.profile.profile_info["education"+i].location].country;
						$scope.formdata["education"+i].degree = $scope.profile.profile_info["education"+i].degree;
						$scope.formdata["education"+i].major = { val : $scope.profile.profile_info["education"+i].major, valid: false};
						$scope.formdata["education"+i].description = { val : $scope.profile.profile_info["education"+i].description, valid: false};;
						$scope.formdata["education"+i].education_file = $scope.profile.profile_info["education" + i + "_filename"];
						$scope.formdata["education"+i].education_file_path = $scope.profile.profile_info["education" + i + "_filepath"];
						$scope.formdata["education"+i].file_del_yn = false;
						
					}
					else {
						$scope.formdata["education"+i] = {};
						$scope.formdata["education"+i].info = false;
						$scope.formdata["education"+i].start_year = 0;
						$scope.formdata["education"+i].end_year = 0;
						$scope.formdata["education"+i].school = "";
						$scope.formdata["education"+i].location = 0;
						$scope.formdata["education"+i].location_name = "";
						$scope.formdata["education"+i].degree = "";
						$scope.formdata["education"+i].major = "";
						$scope.formdata["education"+i].description = "";
						$scope.formdata["education"+i].education_file = null;
						$scope.formdata["education"+i].education_file_path = null;
						$scope.formdata["education"+i].file_del_yn = false;
					}
				}
				
				
			} else { //저장된 내용이 없다.
				$scope.formdata = [];
				$scope.formdata.work_exp = "";
				$scope.formdata.education = "";
				$scope.formdata.certification = "";
				$scope.formdata.resumeFile = new FormData();
				
				//work
				for (var i=1; i<=3; i++) {
					$scope.formdata["work"+i] = {};
					$scope.formdata["work"+i].info = false;
					$scope.formdata["work"+i].start_year = 0;
					$scope.formdata["work"+i].end_year = 0;
					$scope.formdata["work"+i].company = "";
					$scope.formdata["work"+i].country = 0;
					$scope.formdata["work"+i].country_name = "";
					$scope.formdata["work"+i].city = "";
					$scope.formdata["work"+i].position = "";
					$scope.formdata["work"+i].description = "";
					$scope.formdata["work"+i].work_exp_file = null;
					$scope.formdata["work"+i].file_del_yn = false;
				}

				//certification
				for (var i=1; i<=3; i++) {
					$scope.formdata["certificate"+i] = {};
					$scope.formdata["certificate"+i].info = false;
					$scope.formdata["certificate"+i].year = 0;
					$scope.formdata["certificate"+i].name = "";
					$scope.formdata["certificate"+i].organization = "";
					$scope.formdata["certificate"+i].description = "";
					$scope.formdata["certificate"+i].certification_file = null;
					$scope.formdata["certificate"+i].file_del_yn = false;
				}

				//education
				for (var i=1; i<=3; i++) {
					$scope.formdata["education"+i] = {};
					$scope.formdata["education"+i].info = false;
					$scope.formdata["education"+i].start_year = 0;
					$scope.formdata["education"+i].end_year = 0;
					$scope.formdata["education"+i].school = "";
					$scope.formdata["education"+i].location = 0;
					$scope.formdata["education"+i].location_name = "";
					$scope.formdata["education"+i].degree = "";
					$scope.formdata["education"+i].major = "";
					$scope.formdata["education"+i].description = "";
					$scope.formdata["education"+i].education_file = null;
					$scope.formdata["education"+i].file_del_yn = false;
				}
			}

			$scope.start_year = 0;
			$scope.modalYearList = [];
			var d = new Date();
			$scope.thisYear = d.getFullYear()
			for($scope.start_year = $scope.thisYear; $scope.start_year >= d.getFullYear() - 60; $scope.start_year--) {
				$scope.modalYearList.push({val :$scope.start_year, name: $scope.start_year});
			}
			
			$scope.endYearList = [];
			$scope.endYearList.push({val :-1, name: "to now"});

			$scope.degreeList = ["Bachelor’s", "Master’s", "Doctorate", "Postdoctoral", "Other"];

			$scope.workDescPH = $scope.strings.labels.modal_work_desc;
			$scope.eduDescPH = $scope.strings.labels.modal_education_desc;
			$scope.certiNamePH = $scope.strings.labels.modal_certificate_name;
			$scope.certiOrganizationPH = $scope.strings.labels.modal_certificate_organization;
			$scope.certiDescPH = $scope.strings.labels.modal_certificate_desc;
			
			//upload file temp  
			$scope.tempFile = null;

			$scope.loading = false;
		}).error(function(result){
			console.log( 'error');
		});
	}
	
	$scope.setCurrentTime = function(){ 
		var now = new Date();
		$scope.currentUTCTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
		$scope.changeTimezone();
		$timeout($scope.setCurrentTime, 1000);
	};
	
	$scope.changeTimezone = function(){
		if($scope.profile.timezone_id == null){
			var utc_time = minutesToUTC(-(new Date().getTimezoneOffset()));
			for(var idx = 0; idx < $scope.timezoneInfo.length; idx++){
				if(utc_time == $scope.timezoneInfo[idx].utc_time){
					$scope.profile.timezone_id = $scope.timezoneInfo[idx].id;
					break;
				}
			}
//			var displayTime = new Date().format("yyyy.MM.dd hh:mm a/p (UTC");
//			$scope.currentTime = displayTime + minutesToUTC(-(new Date().getTimezoneOffset())) + ")";
		}
//		else{
		var displayTime = new Date($scope.currentUTCTime.getTime() + utcToMilliseconds($scope.timezoneInfo[$scope.profile.timezone_id-1].utc_time));
		$scope.currentTime = displayTime.format("yyyy.MM.dd hh:mm a/p (UTC") + $scope.timezoneInfo[$scope.profile.timezone_id-1].utc_time + ")";
//		}
	}

	$scope.setDefaultFromCity = function(){
		$scope.nationsInfo[$scope.profile.from_field].city_list.splice(0, 1);
		$scope.nationsInfo[$scope.profile.from_field].city_list.splice(0, 0, {"id": 0,"name":"City"});
		$scope.profile.from_city = 0;
	}

	$scope.setDefaultLiveinCity = function(){
		$scope.nationsInfo[$scope.profile.livein].city_list.splice(0, 1);
		$scope.nationsInfo[$scope.profile.livein].city_list.splice(0, 0, {"id": 0,"name":"City"});
		$scope.profile.livein_city = 0;
	}
	
	
	$scope.getCityInfo = function(country_id, field) {
		$http.post("/common/city/", {country_id : country_id}).success( function( result ) {
			if(result.isSuccess == 1){
				if (field == "from") {
					$scope.fromCity = result.city;
					$scope.fromCity.splice(0, 0, {"id": 0,"city":"Select City"});
					$scope.showNationImage();
				} else if (field == "livingin") {
					$scope.livinginCity = result.city;
					$scope.livinginCity.splice(0, 0, {"id": 0,"city":"Select City"});
				} else {
					$scope.fromCity = result.city;
					$scope.fromCity.splice(0, 0, {"id": 0,"city":"Select City"});
					$scope.showNationImage();
				}
			}
		});
	}
	
	$scope.setDateList = function(){
		$scope.dateList = [];
		if($scope.profile.birthdate.month == 02){
			if($scope.profile.birthdate.year == "" || $scope.profile.birthdate.year == null){
				for(var date = 1; date <= 29; date++){
					$scope.dateList.push(date);
				}
			}
			else{
				$http.post("/common/get_max_date/",{
					year: $scope.profile.birthdate.year,
			        month: $scope.profile.birthdate.month}).success(function(response){
			        	for(var date = 1; date <= response.lastDate; date++){
							$scope.dateList.push(date);
						}
				});
			}
		}
		else if($scope.profile.birthdate.month == "" || $scope.profile.birthdate.month == null || $scope.profile.birthdate.month == 1 || $scope.profile.birthdate.month == 3 ||
				$scope.profile.birthdate.month == 5 || $scope.profile.birthdate.month == 7 || $scope.profile.birthdate.month == 8 ||
				$scope.profile.birthdate.month == 10 || $scope.profile.birthdate.month == 12){
			for(var date = 1; date <= 31; date++){
				$scope.dateList.push(date);
			}
		}
		else{
			for(var date = 1; date <= 30; date++){
				$scope.dateList.push(date);
			}
		}
	};
	
	$scope.showNationImage = function(){
		$(".nations-image").css("display","block");
		if($scope.profile.from_field == 0){
			$scope.profile.from_city = 0;
		}
	}
	
	$scope.checkPersonalTools = function(){
		$scope.profile.tools = [];
		if($scope.profile.skype_id != null){
			var toolsObj = new Object();
			toolsObj.name = "Skype";
			toolsObj.account = $scope.profile.skype_id;
			$scope.profile.tools.push(toolsObj);
		}
		if($scope.profile.hangout_id != null){
			var toolsObj = new Object();
			toolsObj.name = "Hangout";
			toolsObj.account = $scope.profile.hangout_id;
			$scope.profile.tools.push(toolsObj);
		}
		if($scope.profile.facetime_id != null){
			var toolsObj = new Object();
			toolsObj.name = "Facetime";
			toolsObj.account = $scope.profile.facetime_id;
			$scope.profile.tools.push(toolsObj);
		}
		if($scope.profile.qq_id != null){
			var toolsObj = new Object();
			toolsObj.name = "QQ";
			toolsObj.account = $scope.profile.qq_id;
			$scope.profile.tools.push(toolsObj);
		}
		if($scope.profile.tools.length == 0) {
			var toolsObj = new Object();
			toolsObj.name = "Select tools";
			toolsObj.account = "";
			$scope.profile.tools.push(toolsObj);
		}
	}
	
	$scope.addTeachingLanguage = function(){
		$scope.teachable = [];

		for(var nth=1; nth <= 3; nth++){
			var fieldName = "native"+ nth + "_id";
			if($scope.profile[fieldName] != null && $scope.profile[fieldName] != 0 && $scope.profile[fieldName] != -1){
				var obj = new Object();
				obj.id = $scope.profile[fieldName];
				if (obj.id != 0) {
					for(var index in $scope.languageInfo){
						if($scope.languageInfo[index].id == obj.id){
							obj.lang_english = $scope.languageInfo[index].lang_english;
						}
					}
//					obj.level = 7;
					if($scope.profile.teaching1_id == obj.id ||  $scope.profile.teaching2_id == obj.id|| $scope.profile.teaching3_id == obj.id){
						obj.state = 1
					}
					else{
						obj.state = 0
					}
					$scope.teachable.push(obj);					
				}
			}
		}
		for(var nth=1; nth <= 6; nth++){
			var fieldName = "lang" + nth + "_id";
			var level = "lang"+nth+"_level";
			if($scope.profile[fieldName] != null && $scope.profile[level] > 5){
				var obj = new Object();
				obj.id = $scope.profile[fieldName];
				for(var index in $scope.languageInfo){
					if($scope.languageInfo[index].id == obj.id){
						obj.lang_english = $scope.languageInfo[index].lang_english;
					}
				}
//				obj.level =  $scope.profile[level];
				if($scope.profile.teaching1_id == obj.id ||  $scope.profile.teaching2_id == obj.id|| $scope.profile.teaching3_id == obj.id){
					obj.state = 1
				}
				else{
					obj.state = 0
				}
				$scope.teachable.push(obj);
			}
		}
	}
	
	
	
	
});
function utcToMilliseconds(utc){
	var ms = ((parseInt(utc.slice(1,3)) * 60) + (parseInt(utc.slice(4,6)) * 60)) * 60000;
	if(utc.slice(0,1) == "-"){
		ms = -(ms);
	}
	return ms;
}

function minutesToUTC(minute){
	var UTCString = "";
	if(minute<0){
		UTCString += "-";
	}
	else{
		UTCString += "+";
	}
	if(Math.abs(minute)/60 > 10){
		UTCString += Math.abs(minute)/60 + ":";
	}
	else{
		UTCString += "0"+ Math.abs(minute)/60 + ":";
	}
	if(Math.abs(minute) % 60 > 10){
		UTCString += Math.abs(minute) % 60;
	}
	else{
		UTCString += "0" + Math.abs(minute) % 60;
	}
	return UTCString;
}


//********* For DateFormat **********
String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};
Date.prototype.format = function(f) {
    if (!this.valueOf()) return " ";
 
    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;
     
    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).zf(2);
            case "MM": return (d.getMonth() + 1).zf(2);
            case "dd": return d.getDate().zf(2);
            case "E": return weekName[d.getDay()];
            case "HH": return d.getHours().zf(2);
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case "mm": return d.getMinutes().zf(2);
            case "ss": return d.getSeconds().zf(2);
            case "a/p": return d.getHours() < 12 ? "AM" : "PM";
            default: return $1;
        }
    });
};
