tellpinAdminApp.controller("postClaimController", function($scope, $http, $location, $window, $sce, $timeout){
	

	$scope.initPostData = function(type_id, post_id){
		console.log("init post data");
		console.log(type_id);
		console.log(post_id);
		$scope.setPostData(type_id, post_id);
		
		
	};
	

	$scope.setPostData = function (type_id, post_id){
		console.log("setPostData");
		$http.post("/admin/postClaimDetail/", {type_id: type_id, post_id: post_id}).success(function (result){
			var data = result; // should contain the necessary data on post
			console.log(data);
			if (type_id == 1){
				$scope.post = data.post;
				$scope.languageInfo = data.languageInfo;
				$scope.nationsInfo = data.nationsInfo;
				$scope.commentCount = $scope.getCommentLBCount($scope.post.comments);
				$scope.lbCommentTextarea = 0;
				$scope.subCommentTextarea = [];
				$scope.lbSubComment = [];
				$scope.shareMenu = false;
				$scope.reportPopup = false;
				$scope.browser = checkBrowser();
				$scope.violation = data.post_violation;
			} else if (type_id == 2){
				$scope.queComments = false; //question's comments div
				$scope.commntTextarea = false; //question's comment textarea
				
				$scope.answerComments = []; //answer's comments div
				$scope.answerCommntTextarea = []; //answer's comment textarea
				
				$scope.shareMenu = false; //question's share menu
				$scope.reportPopup = false; //question's report popup
				$scope.reportAnswerPopup = false; //question's report popup
				$scope.reportAnswerId = 0;
				$scope.answerEditor = false; //answer editor
				$scope.modifyAnswer = false;
				$scope.modifyAnswerIdx = 0;
				
				$scope.violation = data.post_violation;
				
				$scope.answerCommentContent = []; //answer's comment textarea model
				$scope.subCommentContent = [];
				$scope.subCommentTextarea = [];
				$scope.answerSubCommentTextarea = [];
				$scope.answerSubCommentContent =[];
				$scope.subCommentTextarea = [];
				$scope.userInfo = data.userInfo;
				$scope.questionInfo = data.post;
				$scope.languageInfo = data.languageInfo;
				$scope.nationsInfo = data.nationsInfo;
				$scope.commentCount = $scope.getCommentLQCount();
				$scope.browser = checkBrowser();
			} else if (type_id == 3){
				
				$scope.queComments = false; //question's comments div
				$scope.commntTextarea = false; //question's comment textarea
				
				$scope.answerComments = []; //answer's comments div
				$scope.answerCommntTextarea = []; //answer's comment textarea
				
				$scope.shareMenu = false; //question's share menu
				$scope.reportPopup = false; //question's report popup
				$scope.reportAnswerPopup = false; //question's report popup
				$scope.reportAnswerId = 0;
				$scope.answerEditor = false; //answer editor
				$scope.modifyAnswer = false;
				$scope.modifyAnswerIdx = 0;
				
				$scope.violation = data.post_violation;
				
				$scope.answerCommentContent = []; //answer's comment textarea model
				$scope.subCommentContent = [];
				$scope.subCommentTextarea = [];
				$scope.answerSubCommentTextarea = [];
				$scope.answerSubCommentContent =[];
				$scope.subCommentTextarea = [];
				$scope.userInfo = data.userInfo;
				$scope.questionInfo = data.post;
				$scope.languageInfo = data.languageInfo;
				$scope.nationsInfo = data.nationsInfo;
				$scope.browser = checkBrowser();
				$scope.commentTPCount = $scope.getCommentCount();
				
				
			}
			
		});
	}
	$scope.getCommentLQCount = function(){
		var count = $scope.questionInfo.question.comments.length;
//		for(var idx = 0 ; idx < $scope.questionInfo.question.comments.length; idx++){
//			count += $scope.questionInfo.question.comments[idx].sub_comments.length;
//		}
		return count;
	}
	$scope.getCommentLBCount = function(comments){
		var count = comments.length;
//		for(var idx = 0; idx < comments.length; idx++){
//			count += comments[idx].sub_comments.length;
//		}
		return count;
	}
	
	$scope.getCommentTPCount = function(){
		var count = $scope.questionInfo.question.comments.length;
//		for(var idx = 0 ; idx < $scope.questionInfo.question.comments.length; idx++){
//			count += $scope.questionInfo.question.comments[idx].sub_comments.length;
//		}
		return count;
	}
	
	$scope.checkWriteAnswer = function() {
		if($scope.questionInfo.answers.length > 0) {
			for(var i = 0; i < $scope.questionInfo.answers.length; i++) {
				if ($scope.questionInfo.answers[i].user_id == $scope.userInfo.id)
					return true;
				else
					return false;
			}
		} else {
			return false;
		}
	}
	

	$scope.editqna = function(qid) {
		console.log("editqna");
		$("#edit-qna").submit();
	}

	$scope.trustAsHtml = function(html){
		return $sce.trustAsHtml(html);
	}
	$scope.hideShareMenu = function(e){
		if( e.target.id != 'share-button'){
			$scope.shareMenu = false;
		}
	}
	
	$scope.showQueComments = function(){
		if($scope.queComments == true){
			$scope.queComments = false;
		}
		else{
			$scope.queComments = true;
		}
	};
	$scope.showSubCommentTextarea = function($index){
		if($scope.subCommentTextarea[$index] != true){
			$scope.subCommentTextarea[$index] = true;
			setTimeout(function() { //			textarea focus on
				$(".qd-sub-textarea:nth-child("+ ($index + 1) +")").focus();
			}, 0);
			var height = document.documentElement.scrollHeight;
			height = height + 20;
			$(document).scrollTop(0, height);
		}
		else{
			$scope.subCommentTextarea[$index] = false;
		}
	}
	$scope.showCommentTextarea = function(){
		if($scope.user == ''){
			alert("please login.");
		}
		else{
			if($scope.commntTextarea == true){
				$scope.commntTextarea = false;
			}
			else{
				$scope.commntTextarea = true;
				setTimeout(function() {  	//			textarea focus on
					$(".qd-comment-textarea:first-child").focus();
				}, 0);
				var height = document.documentElement.scrollHeight;
				height = height + 30;
				$(document).scrollTop(0, height);
			}
		}
	};
	$scope.showAnswerSubCommentTextarea = function(answerIndex, $index, $event){
		var target = $event.target;
		if($scope.answerSubCommentTextarea[answerIndex][$index] == true){
			$scope.answerSubCommentTextarea[answerIndex][$index] = false;
		}
		else{
			$scope.answerSubCommentTextarea[answerIndex][$index] = true;
		}
		setTimeout(function() { //			textarea focus on
			$(target).parent().parent().parent().find("textarea").focus();
		}, 0);
		var height = document.documentElement.scrollHeight;
		height = height + 30;
		$(document).scrollTop(0, height);
	}
	$scope.hideAnswerSubCommentTextarea = function(answerIndex, $index){
		$scope.answerSubCommentTextarea[answerIndex][$index] = false;
		$scope.answerSubCommentContent[answerIndex][$index] = "";
	}
	$scope.hideCommentTextarea = function(){
		$scope.commntTextarea = false;
		$(".qd-comment-textarea").val('');
	};
	$scope.hideSubCommentTextarea = function($index){
		$scope.subCommentTextarea[$index] = false;
		$(".qd-sub-textarea:nth-child("+ ($index + 1) +")").val('');
	}
	$scope.addSubList = function(answerIndex){
		$scope.answerSubCommentTextarea[answerIndex] = [];
		$scope.answerSubCommentContent[answerIndex] = [];
	}
	$scope.showAnswerComments = function($index){
		if($scope.user == ''){
			alert("please login.");
		}
		else{
			if($scope.answerComments[$index] == true){
				$scope.answerComments[$index] = false;
			}
			else{
				$scope.answerComments[$index] = true;
			}
		}
		
	};
	$scope.showAnswerCommentTextarea = function($index, $event){
		var target = $event.target;
		console.log($(target).parent().parent().find(".qd-comment-textarea"));
		if($scope.answerCommntTextarea[$index] == true){
			$scope.answerCommntTextarea[$index] = false;
		}
		else{
			$scope.answerCommntTextarea[$index] = true;
			setTimeout(function() { //			textarea focus on
				$(target).parent().parent().find(".qd-comment-textarea").focus();
			}, 0);
			var height = document.documentElement.scrollHeight;
			height = height + 30;
			$(document).scrollTop(0, height);
		}
	};
	$scope.hideAnswerCommentTextarea = function($index){
		$scope.answerCommntTextarea[$index] = false;
		$(".qd-comment-textarea").val('');
	};
	
	$scope.showShareMenu = function(){
		$scope.shareMenu = true;
	};
	$scope.showReportPopup = function(){
		$scope.reportPopup = true;
		setTimeout(function() {
			$(".report-textarea").focus();
		}, 0);
	}
	$scope.hideReportPopup = function(){
		$scope.reportPopup = false;
		$scope.reportContent = "";
	}
	$scope.showReportAnswerPopup = function(aid){
		$scope.reportAnswerId = aid;
		$scope.reportAnswerPopup = true;
		setTimeout(function() {
			$(".report-answer-textarea").focus();
		}, 0);
	}
	$scope.hideReportAnswerPopup = function(){
		$scope.reportAnswerId = 0;
		$scope.reportAnswerPopup = false;
		$scope.reportAnswerContent = "";
	}
	$scope.showSharePopup = function(url, width, height){
		$window.open(url, 'title', "toolbar=no, scrollbars=no, resizeable=yes, width="+width+", height="+height);
		return false;
	}

	$scope.postComment = function(lq_id){
		if($scope.commentContent == "" || $scope.commentContent == undefined){
			alert("empty content!!!");
			return;
		}
		$http.post("/questions/addcm/", {type : "Q", lq_id : lq_id, lq_ref_id : lq_id, content : $scope.commentContent }).success(function(response){
			$scope.questionInfo.question.comments = response.comments;
			$scope.commentCount = $scope.getCommentCount();
			$scope.commentContent = "";
			$scope.queComments = true;
			$scope.hideCommentTextarea();
		});
	};

	$scope.postAnswerComment = function($index, lq_id, lq_ref_id){
		$http.post("/questions/addcm/", {type : "A", lq_id : lq_id, lq_ref_id : lq_ref_id, content : $scope.answerCommentContent[$index] }).success(function(response){
			$scope.questionInfo.answers[$index].comments = response.comments;
			$scope.answerComments[$index] = true;
			$scope.answerCommentContent[$index] = "";
			$scope.hideAnswerCommentTextarea($index);
		});
	}



	$scope.deleteAnswer = function(id, aid){
		if(confirm("Are you sure you want to delete the answer?")){
			$http.post("/questions/removeanswer/", {id : id, aid : aid}).success(function(response){
				if(response.isSuccess){
					$scope.questionInfo.answers = response.answers;
					$scope.writeAnswer = false;
//					alert("Success");
				}
			});
		}
		else{
			return;
		}
	}

	$scope.deleteLBComment = function(uid, cid){
		if(confirm("delete?")){
			$http.post("/admin/lb/delete_comment/", {uid: uid, cid : cid}).success(function(response){
				$scope.post.comments = response.comments;
				$scope.commentCount = $scope.getCommentLBCount($scope.post.comments);
			});
		}
		else{
			return;
		}
	}
	
	$scope.removelb = function(pid) {
		console.log("removelb");
		if(confirm("delete?")){
			$http.post("/admin/lb/removelb/", {pid : pid}).success(function(response){
				if(response.isSuccess){
					$window.location.href = ("/admin/admin_home");
				}
			});
		}
		else{
			return;
		}
	}
	
	$scope.deleteComment = function(id){
		if(confirm("Are you sure you want to delete the comment?")){
			$http.post("/questions/removecm/", {cid : id}).success(function(response){
				$scope.questionInfo.question.comments = response.comments;
				$scope.commentCount = $scope.getCommentCount();
				if(response.comments.length == 0){
					$scope.queComments = false;
				}
			});
		}
		else{
			return;
		}
	}
	//////////////////////////////////////////////////// LQ
	$scope.deleteLQAnswer = function(quid, auid, id, aid){
		if(confirm("Are you sure you want to delete the answer?")){
			$http.post("/admin/lq/removeanswer/", {quid:quid, auid:auid, id : id, aid : aid}).success(function(response){
				if(response.isSuccess){
					$scope.questionInfo.answers = response.answers;
					$scope.writeAnswer = false;
				}
			});
		}
		else{
			return;
		}
	}
	
	$scope.deleteAnswerLQComment = function(answerindex, $index, uid, cid){
		if(confirm("delete?")){
			$http.post("/admin/lq/removecm/", {uid:uid, cid : cid}).success(function(response){
				$scope.questionInfo.answers[answerindex].comments = response.comments;
				$scope.questionInfo.answers[answerindex].commentCount = $scope.getAnswerCommentCount(answerindex);
				if(response.comments.length == 0){
					$scope.answerComments[answerindex] = false;
				}
			});
		}
		else{
			return;
		}
	}
	
	$scope.deleteQuestionLQComment = function(uid, id){
		if(confirm("Are you sure you want to delete the comment?")){
			$http.post("/admin/lq/removecm/", {uid: uid, cid : id}).success(function(response){
				$scope.questionInfo.question.comments = response.comments;
				$scope.commentCount = $scope.getCommentCount();
				if(response.comments.length == 0){
					$scope.queComments = false;
				}
			});
		}
		else{
			return;
		}
	}
	
	$scope.deleteQuestionLQ = function(lq_id, best_answer_credit) {
		console.log("deleteqna");
		if(confirm("delete?")){
			$http.post("/admin/lq/removequestion/", {lq_id : lq_id, best_answer_credit : best_answer_credit}).success(function(response){
				if(response.isSuccess){
					$window.location.href = ("/admin/admin_home/");
				}
			});
		}
		else{
			return;
		}
	}
/////////////////////////////////////////////////////////	
	
////////////////////////////////////////////////////TP
	$scope.deleteTPAnswer = function(quid, auid, id, aid){
		if(confirm("Are you sure you want to delete the answer?")){
			$http.post("/admin/tp/removeanswer/", {quid:quid, auid:auid, id : id, aid : aid}).success(function(response){
				if(response.isSuccess){
					$scope.questionInfo.answers = response.answers;
					$scope.writeAnswer = false;
				}
			});
		}
		else{
			return;
		}
	}
	
	$scope.deleteAnswerTPComment = function(answerindex, $index, uid, cid){
		if(confirm("delete?")){
			$http.post("/admin/tp/removecm/", {uid:uid, cid : cid}).success(function(response){
				$scope.questionInfo.answers[answerindex].comments = response.comments;
				$scope.questionInfo.answers[answerindex].commentCount = $scope.getAnswerCommentCount(answerindex);
				if(response.comments.length == 0){
					$scope.answerComments[answerindex] = false;
				}
			});
		}
		else{
			return;
		}
	}
	
	$scope.deleteQuestionTPComment = function(uid, id){
		if(confirm("Are you sure you want to delete the comment?")){
			$http.post("/admin/tp/removecm/", {uid: uid, cid : id}).success(function(response){
				$scope.questionInfo.question.comments = response.comments;
				$scope.commentCount = $scope.getCommentCount();
				if(response.comments.length == 0){
					$scope.queComments = false;
				}
			});
		}
		else{
			return;
		}
	}
	
	$scope.deleteQuestionTP = function(tpid, best_answer_credit) {
		console.log("deleteqna");
		if(confirm("delete?")){
			$http.post("/admin/tp/removetp/", {tpid : tpid, best_answer_credit : best_answer_credit}).success(function(response){
				if(response.isSuccess){
					$window.location.href = ("/admin/admin_home/");
				}
			});
		}
		else{
			return;
		}
	}
	
/////////////////////////////////////////////////////////
	
	
	$scope.deleteSubComment = function($index, cid){
		if(confirm("delete?")){
			$http.post("/questions/removecm/", {cid : cid}).success(function(response){
//				$scope.questionInfo.question.comments[$index].sub_comments = response.comments;
				$scope.commentCount = $scope.getCommentCount();
			});
		}
		else{
			return;
		}
	}
	$scope.deleteAnswerSubComment = function(answerIndex, $index, cid){
		console.log(answerIndex);
		if(confirm("delete?")){
			$http.post("/questions/removecm/", {cid : cid}).success(function(response){
//				$scope.questionInfo.answers[answerIndex].comments[$index].sub_comments = response.comments;
				$scope.questionInfo.answers[answerIndex].commentCount = $scope.getAnswerCommentCount(answerIndex);
			});
		}
		else{
			return;
		}
	}
	$scope.getCommentCount = function(){
		var count = $scope.questionInfo.question.comments.length;
//		for(var idx = 0 ; idx < $scope.questionInfo.question.comments.length; idx++){
//			count += $scope.questionInfo.question.comments[idx].sub_comments.length;
//		}
		return count;
	}
	$scope.getAnswerCommentCount = function($index){
		var count = $scope.questionInfo.answers[$index].comments.length;
//		for(var idx = 0 ; idx < $scope.questionInfo.answers[$index].comments.length; idx++){
//			count += $scope.questionInfo.answers[$index].comments[idx].sub_comments.length;
//		}
		return count;
	}
	
	$scope.showAnswerEditor = function(){
		if($scope.user == ''){
			alert("please login.");
		}
		else{
			$scope.answerEditor = true;
			window.scrollTo(0, document.body.scrollHeight);
		}
	};

	$scope.modifyAnswerEditor = function($index, aid){
		if($scope.user == ''){
			alert("please login.");
		}
		else{
			console.log("modifyAnswerEditor");
			console.log(aid);
			console.log($scope.questionInfo.answers[$index].contents);
			$scope.answerEditor = true;
			$scope.modifyAnswer = true;
			$scope.modifyAnswerIdx = aid;
			tinymce.get("answer_editor").setContent($scope.questionInfo.answers[$index].contents);
		}
	};

	$scope.hideAnswerEditor = function(){
		$scope.answerEditor = false;
		tinymce.get("answer_editor").setContent("");
	};
	$scope.postAnswer = function(lq_id){
		var content = tinymce.get("answer_editor").getContent();
		$http.post("/questions/answer/", {lq_id : lq_id, content : content}).success(function(response){
			console.log(response);
			if(response.isSuccess){
				$scope.questionInfo.answers = response.answers;
				$scope.hideAnswerEditor();
				$scope.writeAnswer = true;
//				alert("Success");
			}
		});
	}

	$scope.editAnswer = function(lq_id){
//		console.log("<<<< editAnswer <<<");
//		console.log($scope.modifyAnswerIdx);
//		console.log(tinymce.get("answer_editor").getContent());
		
		var content = tinymce.get("answer_editor").getContent();
		$http.post("/questions/editanswer/", {lq_id : lq_id, aid : $scope.modifyAnswerIdx, content : content}).success(function(response){
			if(response.isSuccess){
				$scope.questionInfo.answers = response.answers;
				$scope.hideAnswerEditor();
				alert("Success");
				$scope.modifyAnswer = false;
				$scope.modifyAnswerIdx = 0;
			}
		});
	}

	$scope.vote = function(lq_id){
		$http.post("/questions/vote/", {type : "Q", lq_id : lq_id, lq_ref_id : lq_id}).success(function(response){
			if(response.isSuccess){
				$scope.questionInfo.question.like = response.like;
				$scope.questionInfo.question.like_count = response.like_count;
			}
		});
	};
	
	$scope.voteAnswer = function($index, lq_id, lq_ref_id){
		$http.post("/questions/vote/", {type : "A", lq_id : lq_id, lq_ref_id : lq_ref_id}).success(function(response){
			if(response.isSuccess){
				$scope.questionInfo.answers[$index].like = response.like;
				$scope.questionInfo.answers[$index].like_count = response.like_count;
			}
		});
	}
	
	$scope.pinIt = function(lq_id){
		$http.post("/questions/pin/", {lq_id : lq_id}).success(function(response){
			console.log(response);
			if(response.isSuccess){
				$scope.questionInfo.question.pin_count = response.pin_count;
				$scope.questionInfo.question.pin = response.pin;
			}
		});
	}
	$scope.queReport = function(lq_id){
		if($scope.reportContent == "" || $scope.reportContent == undefined){
			alert("empty content!!!");
			return;
		}
		$http.post("/questions/report/", {type:"Q", ref_id : lq_id, content : $scope.reportContent}).success(function(response){
			console.log(response);
			if(response.isSuccess){
				$scope.reportPopup = false;
				$scope.reportContent="";
				alert("report successfully submitted");
			}
		});
	}
	
	$scope.queReportAnswer = function(aid){
		if($scope.reportAnswerContent == "" || $scope.reportAnswerContent == undefined){
			alert("empty content!!!");
			return;
		}

		$http.post("/questions/reportanswer/", {type:"A", ref_id : aid, content : $scope.reportAnswerContent}).success(function(response){
			console.log(response);
			if(response.isSuccess){
				$scope.reportAnswerPopup = false;
				$scope.reportAnswerContent="";
				$scope.reportAnswerId = 0;
				alert("report successfully submitted");
			}
		});
	}
	
	$scope.adoptAnswer = function(lq_id, aid, best_answer_credit){
		if(confirm("Will you choose this answer as the best answer?")){
			$http.post("/questions/bestanswer/", {lq_id : lq_id, aid : aid, best_answer_credit : best_answer_credit}).success(function(response){
				console.log(response);
				$scope.questionInfo.answers = response.answers;
				$scope.questionInfo.question.best_answer_id=aid;
			});
		}
		else{
			return;
		}
	}
	$scope.goURL = function(url){
		$window.location.href = (url);
	}
	$scope.showData = function(){
		console.log($scope.questionInfo);
	}
	$scope.copyURL = function(url){
		clipboard.copyText(url);
		alert("Copy URL to clipboard.");
	}
	
	$scope.goUrlAfterLogin = function(url) {
		
		if(url.length > 0){
			location.href = "/user_auth/ajax_nologin/?url=" + url;
		}
	}
	
	$scope.deleteTP = function(qid, best_answer_credit) {
		console.log("deleteTP");
		if(confirm("delete?")){
			$http.post("/trans/removetp/", {qid : qid, best_answer_credit : best_answer_credit}).success(function(response){
				if(response.isSuccess){
					$window.location.href = ("/trans/");
				}
			});
		}
		else{
			return;
		}
	}
	
	$scope.editTP = function(qid) {
		console.log("editTP");
		$("#edit-tp").submit();
//		console.log($scope.questionInfo.question.user_id);
		
//		$http.post("/trans/editTP/", {user_id: $scope.questionInfo.question.user_id, qid : qid}).success(function(response){
//			console.log(response.isSuccess);
//			if(response.isSuccess){
//				console.log("CCCCCCCCCCCCCCCC");
//				$window.location.href = ("/trans/write/");
//			}
//		});
		
	}
	
	
});