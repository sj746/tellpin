var tellpinAdminApp = angular.module('tellpinAdminApp',[]);
tellpinAdminApp.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

tellpinAdminApp.filter('displayTime', function() {
	  return function(arr, browser) {
		  browser = checkBrowser();
		  console.log(browser);
		  //console.log( '##### browser', browser );
		  var date;
		  if(browser == 'Chrome'){
			  console.log(arr);
			  date = new Date(arr.slice(0,19));
		  }
		  else{
			  isoDateFormat = arr.slice(0,19).replace(" ","T");
			  date = new Date(isoDateFormat);
		  }
		  //var dateUTC = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds(), date.getUTCMilliseconds());
		  var now = new Date();
		  var UTCTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
		  var liveTime = UTCTime.getTime() - date.getTime()

		  var seconds = liveTime/1000;
		  if(seconds < 1){
			 display = "just now";
		  }
		  else if (seconds == 1){
			  display = parseInt(seconds) + " second ago";
		  }
		  else if(seconds > 1 && seconds < 60){
			  display = parseInt(seconds) + " seconds ago"; 
		  }
		  else if(seconds >= 60 && seconds < 120 ){
			  display = parseInt(seconds/60) + " minute ago";
		  }
		  else if(seconds >= 120 && seconds < 3600){
			  display = parseInt(seconds/60) + " minutes ago"; 
		  }
		  else if(seconds >= 3600 && seconds < 7200){
			  display = parseInt(seconds/60/60) + " hour ago";
		  }
		  else if(seconds >= 7200 && seconds < 86400){
			  display = parseInt(seconds/60/60) + " hours ago"; 
		  }
		  else{
			  if( date.getFullYear() == UTCTime.getFullYear() ){
				  if( date.getMonth() == UTCTime.getMonth() ){
					  display = (UTCTime.getDate() - date.getDate());
					  if(display == 1) {
						  display = display + " day ago";
					  } else{
						  display = display + " days ago";
					  }
				  }
				  else{
					  display = (UTCTime.getMonth() - date.getMonth());
					  if(display == 1){
						  display = display + " month ago";
					  }else {
						  display = display + " months ago";  
					  }
				  }
			  }
			  else{
				  display = (UTCTime.getFullYear() - date.getFullYear());
				  if (display == 1){
					  display = display + " year ago";
				  }else{
					  display = display + " years ago";
				  }
			  }
		 }
		 return display;
	  };
});
tellpinAdminApp.filter('slice', function() {
	  return function(arr, start, end) {
	    return arr.slice(start, end);
	  };
});

function checkBrowser(){
	var agt = navigator.userAgent.toLowerCase();
//	console.log(agt);
	if (agt.indexOf("chrome") != -1) return 'Chrome';
	if ( (navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agt.indexOf("msie") != -1) ) return 'Internet Explorer';
	if (agt.indexOf("safari") != -1) return 'Safari';
	if (agt.indexOf("firefox") != -1) return 'Firefox';
}