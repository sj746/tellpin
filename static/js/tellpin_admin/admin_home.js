tellpinAdminApp.controller("adminController", function($scope, $http, $location, $window, $sce, $timeout){
	
	
	$scope.openCity= function (evt, cityName) {
	    // Declare all variables
	    var i, tabcontent, tablinks;

	    // Get all elements with class="tabcontent" and hide them
	    tabcontent = document.getElementsByClassName("tabcontent");
	    for (i = 0; i < tabcontent.length; i++) {
	        tabcontent[i].style.display = "none";
	    }

	    // Get all elements with class="tablinks" and remove the class "active"
	    tablinks = document.getElementsByClassName("tablinks");
	    for (i = 0; i < tablinks.length; i++) {
	        tablinks[i].className = tablinks[i].className.replace(" active", "");
	    }

	    // Show the current tab, and add an "active" class to the link that opened the tab
	    document.getElementById(cityName).style.display = "block";
	    evt.currentTarget.className += " active";
	}
	
	$scope.userViolationList = function(evt, tabName) {
		var i, tabcontent, tablinks;
	    $http.post( "/admin/user_violation_list/", {page: 1}).success( function( result ) {
	    	if ( result.isSuccess == 1 ) {
	    		$scope.TLs = result.TLs;
	    		$scope.last_page = result.last_page;
	    		$scope.page = result.page;
	    		$scope.pgGroup = result.pgGroup;
	    	}
	    });
	    // Get all elements with class="tabcontent" and hide them
	    tabcontent = document.getElementsByClassName("tabcontent");
	    for (i = 0; i < tabcontent.length; i++) {
	        tabcontent[i].style.display = "none";
	    }
	    
	    // Get all elements with class="tablinks" and remove the class "active"
	    tablinks = document.getElementsByClassName("tablinks");
	    for (i = 0; i < tablinks.length; i++) {
	        tablinks[i].className = tablinks[i].className.replace(" active", "");
	    }

	    // Show the current tab, and add an "active" class to the link that opened the tab
	    document.getElementById(tabName).style.display = "block";
	    evt.currentTarget.className += " active";
		
	}
	
	$scope.tutorModifyList = function(evt, tabName) {
		var i, tabcontent, tablinks;
	    $http.post( "/admin/tutor_modify_list/", {page: 1}).success( function( result ) {
	    	if ( result.isSuccess == 1 ) {
	    		$scope.TLs = result.TLs;
	    		$scope.last_page = result.last_page;
	    		$scope.page = result.page;
	    		$scope.pgGroup = result.pgGroup;
	    	}
	    });
	    // Get all elements with class="tabcontent" and hide them
	    tabcontent = document.getElementsByClassName("tabcontent");
	    for (i = 0; i < tabcontent.length; i++) {
	        tabcontent[i].style.display = "none";
	    }
	    
	    // Get all elements with class="tablinks" and remove the class "active"
	    tablinks = document.getElementsByClassName("tablinks");
	    for (i = 0; i < tablinks.length; i++) {
	        tablinks[i].className = tablinks[i].className.replace(" active", "");
	    }

	    // Show the current tab, and add an "active" class to the link that opened the tab
	    document.getElementById(tabName).style.display = "block";
	    evt.currentTarget.className += " active";
		
	}
	
	$scope.sendEmail = function(_email){
		$http.post("/admin/send_email/", {email: _email}).success(function (result){
			if (result.isSuccess==1){
				console.log("sent email");
			}
		});
	}
	
	$scope.claimList= function (evt, tabName){
		var i, tabcontent, tablinks;
		console.log("claimList");
	    $http.post( "/admin/lesson_claim_list/", {page: 1}).success( function( result ) {
	    	console.log(result);
	    	if ( result.isSuccess == 1 ) {
	    		$scope.TLs = result.TLs;
	    		$scope.last_page = result.last_page;
	    		$scope.page = result.page;
	    		$scope.pgGroup = result.pgGroup;
	    	}
	    });
	    // Get all elements with class="tabcontent" and hide them
	    tabcontent = document.getElementsByClassName("tabcontent");
	    for (i = 0; i < tabcontent.length; i++) {
	        tabcontent[i].style.display = "none";
	    }
	    
	    // Get all elements with class="tablinks" and remove the class "active"
	    tablinks = document.getElementsByClassName("tablinks");
	    for (i = 0; i < tablinks.length; i++) {
	        tablinks[i].className = tablinks[i].className.replace(" active", "");
	    }

	    // Show the current tab, and add an "active" class to the link that opened the tab
	    document.getElementById(tabName).style.display = "block";
	    evt.currentTarget.className += " active";
	}
	
	$scope.transactionList= function (evt, tabName){
		var i, tabcontent, tablinks;
	    $http.post( "/admin/transaction_list/", {page: 1}).success( function( result ) {
	    	if ( result.isSuccess == 1 ) {
	    		console.log(result.TLs);
	    		$scope.TLs = result.TLs;
	    		$scope.last_page = result.last_page;
	    		$scope.page = result.page;
	    		$scope.pgGroup = result.pgGroup;
	    	}
	    });
	    // Get all elements with class="tabcontent" and hide them
	    tabcontent = document.getElementsByClassName("tabcontent");
	    for (i = 0; i < tabcontent.length; i++) {
	        tabcontent[i].style.display = "none";
	    }
	    
	    // Get all elements with class="tablinks" and remove the class "active"
	    tablinks = document.getElementsByClassName("tablinks");
	    for (i = 0; i < tablinks.length; i++) {
	        tablinks[i].className = tablinks[i].className.replace(" active", "");
	    }

	    // Show the current tab, and add an "active" class to the link that opened the tab
	    document.getElementById(tabName).style.display = "block";
	    evt.currentTarget.className += " active";
	}

	$scope.helpList= function (evt, tabName){
		var i, tabcontent, tablinks;
	    $http.post( "/admin/help_list/", {page: 1}).success( function( result ) {
	    	if ( result.isSuccess == 1 ) {
	    		$scope.TLs = result.TLs;
	    		$scope.last_page = result.last_page;
	    		$scope.page = result.page;
	    		$scope.pgGroup = result.pgGroup;
	    	}
	    });
	    // Get all elements with class="tabcontent" and hide them
	    tabcontent = document.getElementsByClassName("tabcontent");
	    for (i = 0; i < tabcontent.length; i++) {
	        tabcontent[i].style.display = "none";
	    }
	    
	    // Get all elements with class="tablinks" and remove the class "active"
	    tablinks = document.getElementsByClassName("tablinks");
	    for (i = 0; i < tablinks.length; i++) {
	        tablinks[i].className = tablinks[i].className.replace(" active", "");
	    }

	    // Show the current tab, and add an "active" class to the link that opened the tab
	    document.getElementById(tabName).style.display = "block";
	    evt.currentTarget.className += " active";
	}
	
	$scope.getHelpDetail = function(help_id){
		
		$(location).attr('href', '/admin/help_profile/' + help_id + '/');
	}
	
	$scope.tutorApplyList= function (evt, tabName) {
	    // Declare all variables
		
	    var i, tabcontent, tablinks;
	    $http.post( "/admin/tutor_apply_list/", {page: 1}).success( function( result ) {
	    	if ( result.isSuccess == 1 ) {
	    		$scope.TLs = result.TLs;
	    		$scope.last_page = result.last_page;
	    		$scope.page = result.page;
	    		$scope.pgGroup = result.pgGroup;
	    	}
	    });
	    // Get all elements with class="tabcontent" and hide them
	    tabcontent = document.getElementsByClassName("tabcontent");
	    for (i = 0; i < tabcontent.length; i++) {
	        tabcontent[i].style.display = "none";
	    }
	    
	    // Get all elements with class="tablinks" and remove the class "active"
	    tablinks = document.getElementsByClassName("tablinks");
	    for (i = 0; i < tablinks.length; i++) {
	        tablinks[i].className = tablinks[i].className.replace(" active", "");
	    }

	    // Show the current tab, and add an "active" class to the link that opened the tab
	    document.getElementById(tabName).style.display = "block";
	    evt.currentTarget.className += " active";
	}
	
	$scope.tutorWithrawlList = function(evt, tabName) {
		console.log("tutorWithrawlList");
		$http.post("/admin/request_withraw/", {page : 1}).success( function( result ) {
			if (result.isSuccess == 1) {
				console.log(result);
//				$scope.TLs = result.withraw_tutor;
//	    		$scope.page = $scope.TLs.page;
//	    		$scope.pgGroup = $scope.TLs.pgGroup;

	    		$scope.TLs = result.TLs;
	    		$scope.last_page = result.last_page;
	    		$scope.page = result.page;
	    		$scope.pgGroup = result.pgGroup;
			}
		});

	    tabcontent = document.getElementsByClassName("tabcontent");
	    for (i = 0; i < tabcontent.length; i++) {
	        tabcontent[i].style.display = "none";
	    }
	    
	    // Get all elements with class="tablinks" and remove the class "active"
	    tablinks = document.getElementsByClassName("tablinks");
	    for (i = 0; i < tablinks.length; i++) {
	        tablinks[i].className = tablinks[i].className.replace(" active", "");
	    }

	    // Show the current tab, and add an "active" class to the link that opened the tab
	    document.getElementById(tabName).style.display = "block";
	    evt.currentTarget.className += " active";
	}

	$scope.setWithraw = function( user, withdrawal_pending ) {
		console.log("setWithraw id = " + user);
		if(confirm("Is paypal transfer complete? Proceed after you finish.")){
			if(confirm("Tutor withdrawal history will be updated.")){
				$http.post("/admin/set_withraw/", {user : user, withraw : withdrawal_pending, page : 1}).success( function (result) {
					if (result.isSuccess == 1) {
						console.log(result);
						$scope.TLs = result.TLs;
			    		$scope.last_page = result.last_page;
			    		$scope.page = result.page;
			    		$scope.pgGroup = result.pgGroup;
					}
				});
			}
		}

	}

	$scope.statFilter = function(_page, type) {
		console.log('statfilter()');
		if( $scope.selected_stat == '--------------------------'){
			console.log($scope.selected_stat);
			return;
		}
		var url = ""
		if (type == "tutor_apply_list"){
			url = "tutor_apply_list";
		} else if (type == "tutor_modify_list"){
			url = "tutor_modify_list";
		} else if (type == "lesson_claim_list"){
			url = "lesson_claim_list";
		} else if (type == "post_claim_list"){
			url = "post_claim_list";
		} else if (type == "trasaction_list"){
			url = "transaction_list";
		} else if (type == "help_list"){
			url = "help_list";
		} else if (type == "user_violation"){
			url = "user_violation_list";
		} else if (type == "tutor_withraw_list") {
			url = "request_withraw";
		}
		$http.post( "/admin/" + url+ "/", {page: _page}).success( function( result ) {
	    	if ( result.isSuccess == 1 ) {
	    		$scope.TLs = result.TLs;
	    		$scope.last_page = result.last_page;
	    		$scope.page = result.page;
	    		$scope.pgGroup = result.pgGroup;
	    	}
	    });	
	}
	
	$scope.getViolationDetail = function(user_id){
		$(location).attr('href', '/admin/user_violation/' + user_id + '/');
	}
	
	$scope.getTutorDetail = function(user_id){
		$(location).attr('href', '/admin/tutor_profile/'+user_id+'/');
	}
	
	$scope.getTutorAppDetail = function(user_id){
		$(location).attr('href', '/admin/tutor_app_profile/'+user_id+'/');
	}
	
	$scope.getClaimDetail = function(reservation_id){
		console.log("getting claim detail for "+ reservation_id);
		$(location).attr('href', '/admin/claim_profile/'+reservation_id+'/');
	}
	
	$scope.getPostDetail = function(post_id, post_type){
		console.log(post_id);
		console.log(post_type);
		var type_id = 0;
		if (post_type == "Language Learning Board"){
			type_id = 1;
		} else if (post_type == "Language Question"){
			type_id = 2;
		} else if (post_type == "Translation & Proofreading"){
			type_id = 3;
		}
		$(location).attr('href', '/admin/post_profile/' + type_id + '_' + post_id + '/');
	}
	
	$scope.postList = function(evt, tabName){
		console.log("post claim list");
		 var i, tabcontent, tablinks;
	    $http.post( "/admin/post_list/", {page: 1}).success( function( result ) {
	    	if ( result.isSuccess == 1 ) {
	    		$scope.TLs = result.TLs;
	    		$scope.last_page = result.last_page;
	    		$scope.page = result.page;
	    		$scope.pgGroup = result.pgGroup;
	    	}
	    });
	    // Get all elements with class="tabcontent" and hide them
	    tabcontent = document.getElementsByClassName("tabcontent");
	    for (i = 0; i < tabcontent.length; i++) {
	        tabcontent[i].style.display = "none";
	    }
	    
	    // Get all elements with class="tablinks" and remove the class "active"
	    tablinks = document.getElementsByClassName("tablinks");
	    for (i = 0; i < tablinks.length; i++) {
	        tablinks[i].className = tablinks[i].className.replace(" active", "");
	    }

	    // Show the current tab, and add an "active" class to the link that opened the tab
	    document.getElementById(tabName).style.display = "block";
	    evt.currentTarget.className += " active";
	}

});

