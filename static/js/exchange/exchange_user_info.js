/**
 * tutor_info.js
 */

// youtube get video id
// (?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|playlist\?|watch\?v=|watch\?.+(?:&|&#38;);v=))([a-zA-Z0-9\-_]{11})?(?:(?:\?|&|&#38;)index=((?:\d){1,3}))?(?:(?:\?|&|&#38;)?list=([a-zA-Z\-_0-9]{34}))?(?:\S+)?


tellpinApp.directive('tooltip', function(){
    return {
        link: function(scope, element, attrs){
        	console.log("tooltip");
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
});

tellpinApp.directive('customPopover', function( $compile ){
	var template = $( '.popover-content' ).html();
    return {
    	scope: {
    		usercard: '='
    	},
        link: function(scope, element, attrs){
        	//attrs.$set( 'usercard', scope.$eval( attrs.usercard ));
        	//scope.usercard = JSON.parse( attrs.usercard );
        	var compiled = $compile( template )( scope );
	        // on mouseenter
	        $(element).popover({
	        	trigger: 'hover',
	        	html: true,
	        	placement: 'left',
	        	content: compiled
	        });
        }
    };
});

/*exchangeUserInfoApp.directive( 'scroll', function() {
	return {
		scope: {
			scroll: "="
		},
		link: function( scope, element ) {
			scope.$watchCollection( 'scroll', function( newValue ) {
				if ( newValue ) {
					console.log( $( element ) );
					console.log( $( element )[0].scrollHeight );
					//$( element ).scrollTop( "500px" );
				}
			});
		}
	}
});*/

tellpinApp.controller( "infoCtrl", ["$scope","$http", "$sce", "$location", "$window", function( $scope, $http, $sce, $location, $window ) {
	$scope.init = function( obj ) {		
		angular.element( "[data-toggle='tooltip']" ).tooltip();
		console.log( obj );
		$scope.data = obj;
		$scope.tutor = $scope.data.profile; // 유저 프로필
		$scope.statistics = $scope.data.statistics; // 유저 통계
		$scope.myPage = $scope.data.mypage; // 자기 자신 페이지 확인
		$scope.friends = $scope.data.friends; // 친구 목록
		$scope.tutorList = $scope.data.tutor;
		$scope.tuteelist = $scope.data.tutee;
		$scope.isFriend = $scope.data.isFriend;
		$scope.message = [];
		$scope.messageLength = 250;

		$scope.friendMessage = "I'd like to be your language exchange partner.";
		$scope.list = {};

		if ( $scope.tutor.intro ) {
			$scope.tutor.intro = $scope.tutor.intro.replace( /\n/gi, '<br />' );			
		}
		//community activities
		$scope.languageInfo = $scope.data.languageInfo;
		$scope.all = $scope.data.all;
		$scope.AllShowMore = $scope.data.AllShowMore;
		$scope.LBs = $scope.data.LBs;
		$scope.LBShowMore = $scope.data.LBShowMore;
		$scope.TPs = $scope.data.TPs;
		$scope.TPShowMore = $scope.data.TPShowMore;
		$scope.LQs = $scope.data.LQs;
		$scope.LQShowMore = $scope.data.LQShowMore;
		$scope.selectedCommunityTab = 0;
		$scope.htmlPopover = $sce.trustAsHtml( $( '.popover-content' ).html() );
		$scope.allPage = obj.page;
		$scope.lcPage = 0;
		$scope.tpPage = 0;
		$scope.lqPage = 0;
	}// init

	// 친구 모달 띄우기
	$scope.openFriendModal = function() {
		$( "#friendExModal" ).modal();
		//angular.element( $scope.friendMessage ).select();
	}

	// 메세지 모달
	$scope.openMessage = function() {
		$scope.messageModel = "";
		$http.post( '/exchange/getmessage/', { 'to_user_id' : $scope.tutor.user, })
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				console.log( result.message );
				$scope.message = result.message;
				var elem = angular.element( '#message' );
				$( '#messageModal' ).modal();
				console.log( elem[0].scrollHeight );
			}
		});
		//angular.element( '#message-content' ).scrollTop( angular.element( '#message-content').prop( 'scrollHeight' ));
		//console.log( angular.element( '#message' ).prop( 'scrollHeight' ) );
		angular.element( '#message' ).scrollTop = 800;
	}
	
	// 메세지 전송
	$scope.sendMessage = function() {
		console.log( $scope.messageModel );
		if ( $scope.messageModel.length > $scope.messageLength || !$scope.messageModel ) {
			console.log( 'not save' );
			return;
		}
		else {
			$http.post( '/exchange/newmessage/', { 'to_user_id' : $scope.tutor.user, 'message' : $scope.messageModel })
			.success( function( result ) {
				if ( result.isSuccess == 1 ) {
					$scope.message = result.message;
					$scope.messageModel = "";
					console.log( $scope.message );
				}
			});
		}
	}
	
	$scope.enterCheck = function( event ) {
		if ( event.keyCode == 13 ) {
			angular.element( '#messageModal' ).modal( 'hide' );
			$scope.sendMessage();
		}
	}

	$scope.goURL = function(url){
		$window.location.href = (url);
		$window.event.stopPropagation();
	}

	$scope.goCommunityDetail = function(category, id){
		if(category == 'Board')
			$window.location.href = ('/board/'+ id);
		else if(category == 'TranslationProofreading')
			$window.location.href = ('/trans/'+ id);
		else if(category == 'Question')
			$window.location.href = ('/questions/'+ id);
		$window.event.stopPropagation();
	}

	// 친구 신청
	$scope.sendExFriend = function() {
		$http.post( '/exchange/newfriend/', { 'response_id' : $scope.tutor.user, 'message' : $scope.friendMessage })
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				//alert( "request success!" );
				$scope.isFriend = result.isSuccess;
				angular.element( '#friendExModal' ).modal( 'hide' ); 
			}
		});
	}
	
	// 튜터 프로필 페이지
	$scope.switchProfile = function() {
		$window.location.href = "/tutor/" + $scope.tutor.user;
	}
	
	// 리스트 모달
	$scope.openList = function( type ) {
		if ( type == 'bookmark' ) {
			if ( $scope.list.title == 'Tutors' ) {
				angular.element( '#listModal' ).modal();
				return;
			}
			$scope.list = {};
			$scope.list.title = 'Tutors';
			angular.element(".spin-img").show();
		}
		else if ( type == 'friend' ) {
			if ( $scope.list.title == 'Friends' ) {
				angular.element( '#listModal' ).modal();
				return;
			}
			$scope.list = {};
			$scope.list.title = 'Friends';
			angular.element(".spin-img").show();
		}
		else {
			if ( $scope.list.title == 'Tutees' ) {
				angular.element( '#listModal' ).modal();
				return;
			}
			$scope.list = {};
			$scope.list.title = 'Tutees';
			angular.element(".spin-img").show();
		}
		$http.post( '/exchange/morelist/', { 'uid' : $scope.tutor.user, 'type' : $scope.list.title })
		.success( function( result ) {
			console.log( result );
			$scope.list.array = result.list;
			angular.element(".spin-img").hide();
		});
		angular.element( '#listModal' ).modal();
	}
	
	// 신고 모달 띄우기
	$scope.openReportModal = function( idx ) {
		if ( idx == 1 ) {
			$( "#reportModal" ).modal();
			$scope.reportType = 1;
			$scope.reportTxt = "";
		}
		else {
			$( "#reportModal" ).modal( "hide" );			
		}
	}
	
	// 신고하기
	$scope.sendReport = function() {
		console.log( $scope.reportType + " : " + $scope.reportTxt );
		$http.post( "/tutor/user_report/", { tid : $scope.tutor.user, type : $scope.reportType, content : $scope.reportTxt } )
		.success( function( result ) {
			alert( "Your problem has been received." );
			$( "#reportModal" ).modal( "hide" );
		});
	}
	
	// 유저숨기기 모달 띄우기
	$scope.addHide = function( idx ) {
		if ( idx == 1) 
			$( "#hideModal" ).modal();
		else
			$( "#hideModal" ).modal( "hide" );
	}
	
	// 유저숨기기
	$scope.sendHide = function() {
		$http.post( "/tutor/hide/", { tid : $scope.tutor.user } )
		.success( function( result ) {
			$( "#hideModal" ).modal( "hide" );
			$window.location.href = "/tutor/";
		})
	}
	
	// edit profile 이동
	$scope.clickEditProfile = function() {
		$window.location.href = "/settings/editprofile/";
	}
	
	// Feedback Paging
	$scope.moreFeedback = function( page ) {
		$http.post( '/exchange/pagefeedback/', { 'page' : page, '_user_id' : $scope.tutor.user } )
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				console.log( result.feedback );
				$scope.feedbacks = result.feedback;
			}
		});
	}
	
	// more Comment
	$scope.moreComment = function( item ) {
		$http.post( '/exchange/morecomment/', { 'revid' : item.revid, 'tutor_id' : item.tutor_id, 'tutee_id' : item.tutee_id })
		.success( function( result ) {
			console.log( result );
			if ( result.isSuccess == 1 ) {
				item.moreList = result.moreFeedback;
				$scope.feedbackFlag = 1;
			}
		});
	}
	
	// change flag
	$scope.toggleFlag = function( item ) {
		item.moreList = [];
	}
	
	$scope.clickTab = function( tab ) {
		if ( $scope.selectedCommunityTab == tab ) {
			return;
		} 
		$scope.selectedCommunityTab = tab;
	}
	
	$scope.getMoreCommunity = function( page, type ) {
		$http.post( '/exchange/more/community/', { 'page' : page, 'type' : type, 'id' : $scope.tutor.user } )
		.success( function( result ) {
			if ( type == 'all' ) {
				$scope.all = $scope.all.concat( result.list );
				$scope.AllShowMore = result.AllShowMore;
				$scope.allPage = result.page;
			}
			else if ( type == 'LB' ) {
				$scope.LBs = $scope.LBs.concat( result.list );
				$scope.LBShowMore = result.LBShowMore;
				$scope.lcPage = result.page;
			}
			else if ( type == 'TP' ) {
				$scope.TPs = $scope.TPs.concat( result.list );
				$scope.TPShowMore = result.TPShowMore;
				$scope.tpPage = result.page;
			}
			else {
				$scope.LQs = $scope.LQs.concat( result.list );
				$scope.LQShowMore = result.LQShowMore;
				$scope.lqPage = result.page;
			}
		});
	}
	
	$scope.goUrlAfterLogin = function(url) {
		
		if(url.length > 0){
			location.href = "/user_auth/ajax_nologin/?url=" + url;
		}
	}

}]);