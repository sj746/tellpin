tellpinApp.controller( "exchangeFindController", ["$scope", "$http", "$sce", function( $scope, $http, $sce ) {

	
	angular.element(document).ready(function(){
		console.log($scope.data);
		$scope.tutor_list = [];
		$scope.page=1;
		$scope.f_q = {
			'type' : null,
			'name': null,
			'speaks' : null,
			'learning' : null,
			'gender' : null,
			'age' : null,
			'nation_from' : null,
			'city_from' : null,
			'livein' : null,
			'liveincity' : null,
			'native' : null,
			'online' : null,
			'page' : $scope.page
		}
		$scope.search_filter();
		$scope.more_type = 0;
		$scope.more_count = 0;
		$scope.b_more_click = false;

		angular.element( "[data-toggle='tooltip']" ).tooltip();

	});
	$scope.mouseover = function(){
	}
	$scope.mouseleave = function(){
		console.log('ng-mouse-leave');
	}
	$scope.loading = function(){
		$scope.loading = false;
//		console.log('test 1');
	}
	/*
	//follow
	$scope.follow = function(tid, index){
		console.log(tid);
		console.log(index);
		$http.post("/tutor/follow/", { tid : tid }).success( function( result ) {
			if(result.isSuccess == 1){
				console.log( $scope.tutor_list[index] );
				$scope.tutor_list[index].is_follower = 1;
				$scope.tutor_list[index].follow_cnt += 1;
			}
		});
	}
	//unfollow
	$scope.unfollow = function(tid, index){
		console.log(tid);
		console.log(index);
		$http.post("/tutor/follow/", { tid : tid }).success( function( result ) {
			if(result.isSuccess == 1){
				console.log( $scope.tutor_list[index] );
				$scope.tutor_list[index].is_follower = 0;
				$scope.tutor_list[index].follow_cnt -= 1;
			}
		});
	}*/
	
	//more list
	$scope.getmoreList = function() {
		$("#btn_more").attr("disabled", "disabled");
		$("#btn_more").text("");
		$("#btn_more").append("<img src='/static/img/ajax-loader.gif'>");
		if(false == $scope.b_more_click){
			$scope.b_more_click = true;
			$scope.f_q.page = $scope.page;

			$http.post( "/exchange/search_tutor/", $scope.f_q).success( function( result ) {
				if ( result.isSuccess == 1 ) {
					//$scope.listdata=result.allQuestion;
					console.log('ajax test');
					console.log(result);
					for(var i = 0; i < result.user_list.length; i++) {
						$scope.tutor_list.push(result.user_list[i])
					}
					//$scope.tutor_list = result.tutor_list;
					console.log(result.page_info);
					$scope.page_info = result.page_info;
					if ($scope.page_info.has_next == 1) {
						$scope.page = $scope.page + 1;
					}
				}
				$("#btn_more").removeAttr("disabled");
				$("#btn_more").text("show more");
				$scope.b_more_click=false;
				$scope.loading = false;
				console.log(result.user_list);
			});
		}
	}
	
	$scope.clickNativeOnly=function(){
		//console.log( $("#native_only").is(":checked") );
		//console.log( $("#teaches").val() );
		if( $("#native_only").is(":checked") && $("#speaks").val() == "all"){
			$("#native_only").attr("checked", false);
			alert(' Partner to practice 를 선택해 주세요.');
		}
	}
	$scope.clickMobileNativeOnly=function(){
		//console.log( $("#native_only").is(":checked") );
		//console.log( $("#teaches").val() );
		if( $("#mobile_native_only").is(":checked") && $("#mobile_speaks").val() == "all"){
			$("#mobile_native_only").attr("checked", false);
			alert(' Partner to practice 를 선택해 주세요. ');
		}
	}

	$scope.mobile_search_name=function(){
		$scope.loading = true;
		//alert( $("#search_name").val() );
		//alert('search name: ' + $("#search_name").val());
		//TODO
		//Search Name
		console.log('mobile_search_name ');
		var t_name = $("#mobile_search_name").val();
		console.log('Search name : ' + t_name );

		var obj={
					'type':'name',
					'name':t_name,
					'page':$scope.page
				};
		$scope.f_q.type = 'name';
		$scope.f_q.name = t_name;
		$scope.f_q.page = 0;
		$http.post( "/exchange/search_tutor/", $scope.f_q).success( function( result ) {
			if ( result.isSuccess == 1 ) {
				//$scope.listdata=result.allQuestion;
				console.log('ajax test');
				$scope.tutor_list = result.user_list;
				console.log(result.page_info);
				$scope.page_info = result.page_info;
				if ($scope.page_info.has_next == 1) {
					$scope.page = $scope.page + 1;
				}
			}
			$scope.loading = false;
			console.log(result.user_list);
		});
	}
	$scope.mobile_search_filter=function(){
		//TODO
		//Filter value
		$scope.loading = true;
		
		var teaches = $("#mobile_teaches").val();

		var speaks = $("#mobile_speaks").val();
		var learning = $("#mobile_learning").val();
		var nation_from = $("#mobile_from").val();
		var livein = $("#mobile_livein").val();
		var liveincity = $("#mobile_liveincity").val();
		var native = $("#mobile_native_only").is(":checked");
		var online = $("#mobile_online_only").is(":checked");
		
		var obj = { 
					'type': 'filter',
					'speaks': speaks,
					'learning': learning,
					'nation_from': nation_from,
					'livein': livein,
					'liveincity': liveincity,
					'native': native,
					'online':online,
					'page':$scope.page
		}
		$scope.f_q.type = 'filter';
		$scope.f_q.speaks = speaks;
		$scope.f_q.learning = learning;
		$scope.f_q.nation_from = nation_from;
		$scope.f_q.livein = livein;
		$scope.f_q.liveincity = liveincity;
		$scope.f_q.native = native;
		$scope.f_q.online = online;
		$scope.f_q.page = 0;
		
		console.log(obj);
		$http.post( "/exchange/search_tutor/", $scope.f_q).success( function( result ) {
			if ( result.isSuccess == 1 ) {
				console.log('ajax test');
				console.log(result.user_list);
				$scope.tutor_list = result.user_list;
				console.log(result.page_info);
				$scope.page_info = result.page_info;
				if ($scope.page_info.has_next == 1) {
					$scope.page = $scope.page + 1;
				}
			}
			$scope.loading = false;
			console.log(result.user_list);
		});
	}
	$scope.search_name=function(){
		//alert( $("#search_name").val() );
		//alert('search name: ' + $("#search_name").val());
		//TODO
		//Search Name
		console.log('search_name()');
		$scope.loading = true;
		var t_name = $("#search_name").val();
		console.log('Search name : ' + t_name );

		var obj={
					'type':'name',
					'name':t_name,
					'page':$scope.page
				};
		$scope.f_q.type = 'name';
		$scope.f_q.name = t_name;
		$scope.f_q.page = 0;
		$http.post( "/exchange/search_tutor/", $scope.f_q).success( function( result ) {
			if ( result.isSuccess == 1 ) {
				//$scope.listdata=result.allQuestion;
				console.log('ajax test');
				$scope.tutor_list = result.user_list;
				console.log(result.page_info);
				$scope.page_info = result.page_info;
				if ($scope.page_info.has_next == 1) {
					$scope.page = $scope.page + 1;
				}
			}
			console.log(result.user_list);
			$scope.loading = false;
		});
	}
	$scope.search_filter=function(){
		//TODO
		console.log('search_filter()');
		$scope.loading = true;
		
		//Filter value
		var speaks = $("#speaks").val();
		var learning = $("#learning").val();
		var gender = $("#gender").val();
		var age = $("#age").val();
		var nation_from = $("#from").val();
		var city_from = $("#fromcity").val();
		var livein = $("#livein").val();
		var liveincity = $("#liveincity").val();
		var native = $("#native_only").is(":checked");
		var online = $("#online_only").is(":checked");
		var obj = { 
					'type': 'filter',
					'speaks': speaks,
					'learning': learning,
					'gender': gender,
					'age': age,
					'nation_from': nation_from,
					'city_from': city_from,
					'livein': livein,
					'liveincity': liveincity,
					'native': native,
					'online': online,
					'page':$scope.page,
		}
		console.log(obj);
		$scope.f_q.type = 'filter';
		$scope.f_q.speaks = speaks;
		$scope.f_q.learning = learning;
		$scope.f_q.gender = gender;
		$scope.f_q.age = age;
		$scope.f_q.nation_from = nation_from;
		$scope.f_q.city_from = city_from;
		$scope.f_q.livein = livein;
		$scope.f_q.liveincity = liveincity;
		$scope.f_q.native = native;
		$scope.f_q.online = online;
		$scope.f_q.page = 0;
		console.log($scope.f_q);
		$http.post( "/exchange/search_tutor/", $scope.f_q).success( function( result ) {
			if ( result.isSuccess == 1 ) {
				//$scope.listdata=result.allQuestion;
				console.log('ajax result:');
				console.log(result);
				$scope.me = result.me;
				$scope.tutor_list = result.user_list;
				$scope.page_info = result.page_info;
				if ($scope.page_info.has_next == 1) {
					$scope.page = $scope.page + 1;
				}
			}
			$scope.loading = false;
//			console.log(result.tutor_list);
		});
	}
	$scope.test = function(){
		alert('111');
	}
	$scope.online_check = function(){
		var checked = $("#check_online").is(":checked");
		if( checked ){
			$("#check_audio").attr("disabled", false);
			$("#check_video").attr("disabled", false);
		}else{
			$("#check_audio").attr("disabled", true);
			$("#check_audio").attr("checked", false);
			$("#check_video").attr("disabled", true);
			$("#check_video").attr("checked", false);
		}
	}
	$scope.showSchedulePopup = function(tid){
		$("body").css("overflow-y","hidden");
		$("body").css("overflow-x","hidden");
		$scope.$emit("sendTid", tid);
		$http.post( "/tutor/tutor_schedule/", {tid: tid}).success( function( response ) {
			if ( response.isSuccess == 1 ) {
				$scope.scheduleInfo = response;
				console.log($scope.scheduleInfo);
				$scope.tutor = new Object();
				$scope.tutor.user = tid;
				$scope.schedulePopup = true;
			}
		});
		
	}
	$scope.trustSrc = function( src ) {
		src = "https://www.youtube.com/embed/"+src.split("/")[3];
		return $sce.trustAsResourceUrl( src );
		//return src;
	}
	$scope.toggle =  function(){
		$("#messageModal").modal('toggle');
	}
	$scope.hideSchedulePopup = function(){
		$("body").css("overflow-y","auto");
		$("body").css("overflow-x","auto");
		$scope.schedulePopup = false;
	}
	$scope.showIntroPopup = function(src){
		$scope.introPopup = true;
		$scope.introSrc = src;
	}
	$scope.hideIntroPopup = function(){
		$scope.introPopup = false;
	}
	$scope.open_filter_pop = function(){
		console.log('test');
		$("#messageModal").modal();
	}
	$scope.go_tutor_info = function(tid){
		console.log(tid);
		$(location).attr('href', '/user/'+tid+'/');
	}

	$scope.getCityInfo = function(in_field) {
		var field = in_field;
		if (field == "livein") {
			$("#" + field + "city").html('').append("<option value='all'>Living in City</option>");
		} else {
			$("#" + field + "city").html('').append("<option value='all'>From City</option>");	
		}
		
		if($("#" + field).val()==""){
			return;
		}

		$http.post("/common/city/", {country_id : $("#" + field).val()}).success( function( result ) {
			if(result.isSuccess == 1){
				console.log(result);
				for( var i = 0; i < result.city.length ; i++){
					$("#" + field + "city").append("<option value='"+ result.city[i].id +"'>"+ result.city[i].city +"</option> ");
				}
			}
			$("#" + field + "city").trigger("chosen:updated");
		});
		$("#" + field + "city").trigger("chosen:updated");
	}

	$scope.openMessageModal = function (msg_type, id) {
		console.log("openMessageModal");
		event.stopPropagation();
		$scope.$emit('messageEventEmit', { type: msg_type, user_id: id });
//		$window.event.stopPropagation();
	};
	
	$scope.openFriendModal = function( obj ) {
		console.log("openFriendModal");
		event.stopPropagation();
		$scope.requestFriend = obj;
		$( '#exFriendModal' ).modal();
	}
	
	$scope.sendFriendMessage = function() {
		$http.post( '/exchange/newfriend/', { 'response_id' : $scope.requestFriend.user, 'message' : $scope.friendMessage })
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				console.log(result);
				//alert( "request success!" );
				$scope.requestFriend.is_friend = true;
				angular.element( '#exFriendModal' ).modal( 'hide' ); 
			}
		});
	}
	
}]);

$(document).ready(function(){
	$('#search_name').keypress(function(event){
		  if(event.keyCode == 13){
		    //angular.element.scope().search_name();
		    angular.element($(".container")).scope().search_name();
		  }
	});
});
function unchecked(){
	console.log( 'unchecked()' );
	console.log( $("#speaks").val() );
	if ( $("#speaks").val() == "all"){
		$("#native_only").attr("checked", false);
	}
}
function mobile_unchecked(){
	console.log( 'mobile unchecked()' );
	console.log( $("#mobile_speaks").val() );
	if ( $("#mobile_speaks").val() == "all"){
		$("#mobile_native_only").attr("checked", false);
	}
}

function toggle(div_id) {
	var el = document.getElementById(div_id);
	
	if ( el.style.display == 'none' ) {	el.style.display = 'block';}
	else {el.style.display = 'none';}
	
}
function blanket_size(popUpDivVar) {
	if (typeof window.innerWidth != 'undefined') {
		viewportheight = window.innerHeight;
	} else {
		viewportheight = document.documentElement.clientHeight;
	}
	if ((viewportheight > document.body.parentNode.scrollHeight) && (viewportheight > document.body.parentNode.clientHeight)) {
		blanket_height = viewportheight;
	} else {
		if (document.body.parentNode.clientHeight > document.body.parentNode.scrollHeight) {
			blanket_height = document.body.parentNode.clientHeight;
		} else {
			blanket_height = document.body.parentNode.scrollHeight;
		}
	}
	var blanket;
	if(popUpDivVar == 'add-pop'){
		blanket = document.getElementById('blanket');
	}else if(popUpDivVar == 'add-pop2'){
		blanket = document.getElementById('blanket2');
	}else if(popUpDivVar == 'add-pop3'){
		blanket = document.getElementById('blanket3');
	}else if(popUpDivVar == 'add-pop4'){
		blanket = document.getElementById('blanket4');
	}else if(popUpDivVar == 'add-pop5'){
		blanket = document.getElementById('blanket5');
	}
	blanket.style.height = blanket_height + 'px';
	var popUpDiv = document.getElementById(popUpDivVar);
	popUpDiv_height=blanket_height/2-200;//200 is half popup's height
	popUpDiv.style.top = '250px';
	//popUpDiv.style.top = popUpDiv_height + 'px';
}
function window_pos(popUpDivVar) {
	if (typeof window.innerWidth != 'undefined') {
		viewportwidth = window.innerHeight;
	} else {
		viewportwidth = document.documentElement.clientHeight;
	}
	if ((viewportwidth > document.body.parentNode.scrollWidth) && (viewportwidth > document.body.parentNode.clientWidth)) {
		window_width = viewportwidth;
	} else {
		if (document.body.parentNode.clientWidth > document.body.parentNode.scrollWidth) {
			window_width = document.body.parentNode.clientWidth;
		} else {
			window_width = document.body.parentNode.scrollWidth;
		}
	}
	var popUpDiv = document.getElementById(popUpDivVar);
	//width
	window_width=window_width/2-200;//200 is half popup's width
	//left css
	/* if(popUpDivVar == 'add-pop3'){
		popUpDiv.style.left = window_width + 'px';
	} */
}
function popup(windowname) {
	//var selected = $(":radio[name='category_type']:checked").attr('id') ;
	$("#new_name").val('');
	$("#new_limit").val('0');
	$('input:radio[name="new_display"]:radio[value="Y"]').prop("checked", "checked");
	$("#new_type").val('1');
	blanket_size(windowname);
	window_pos(windowname);
	if(windowname == 'add-pop'){
		toggle('blanket');
	}else if(windowname == 'add-pop2'){
		toggle('blanket2');
	}else if(windowname == 'add-pop3'){
		toggle('blanket3');
	}else if(windowname == 'add-pop4'){
		toggle('blanket4');
	}else if(windowname == 'add-pop5'){
		toggle('blanket5');
	}
	
	toggle(windowname);		
}
function dayToAlphabet(day){
	var dayList = ["mon", "tue", "wed", "thu","fri","sat","sun"];
	var alphabetList =["A", "B", "C", "D", "E","F","G"];
	for(var idx=0; idx<7; idx++){
		if( dayList[idx] == day){
			return alphabetList[idx];
		}
	}
}
function indexToValue(index){
	var value = "";
	if(index%2 == 0){
		if(index/2 <10){
			value = "0"+ index/2+"00";
		}
		else{
			value = index/2+"00";
		}
	}
	else{
		if((index-1)/2 <10){
			value = "0"+ (index-1)/2 +"30";
		}
		else{
			value = (index-1)/2 +"30";
		}
	}
	return value;
}