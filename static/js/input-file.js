$(document).ready(function(){
	  var fileTarget = $('.tp-upload-file .upload-hidden');
	  
	  fileTarget.on('change', function(){  // 값이 변경되면
	    if(window.FileReader){  // modern browser
	    	var fileType = ["image/jpg", "image/jpeg", "image/png", "image/gif", "image/bmp"];
	    	var filename = "Find File";
	    	var i = 0;

	    	for (i = 0; i < fileType.length; i++) {
	    		if ($(this)[0].files[0].type == fileType[i]) {
	    			filename = $(this)[0].files[0].name;
	    			break;
	    		}
	    	}
	    	if (i == fileType.length) {
	    		filename = "jpg, jpeg, png, gif only";
	    	}
//	    	console.log($(this)[0].files[0]);
	    } 
	    else {  // old IE
	    	var filename = $(this).val().split('/').pop().split('\\').pop();  // 파일명만 추출
	    }
	    
	    // 추출한 파일명 삽입
	    $(this).siblings('.upload-name').val(filename);
	  });
	});