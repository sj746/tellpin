var helpCenterApp = angular.module('helpCenterApp',['ngSanitize', 'angular-clipboard','infinite-scroll']);
helpCenterApp.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});
helpCenterApp.filter('slice', function() {
	  return function(arr, start, end) {
	    return arr.slice(start, end);
	  };
});
helpCenterApp.filter('displayTime', function() {
	  return function(arr, browser) {
		  var date;
		  if(browser == 'Chrome'){
			  date = new Date(arr.slice(0,19));
		  }
		  else{
			  isoDateFormat = arr.slice(0,19).replace(" ","T");
			  date = new Date(isoDateFormat);
		  }
		  var now = new Date();
		  var UTCTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
		  var liveTime = UTCTime.getTime() - date.getTime()
		  var seconds = liveTime/1000;
		  if(seconds < 1){
			 display = "just now";
		  }
		  else if(seconds > 1 && seconds < 60){
			  display = parseInt(seconds) + " seconds ago"; 
		  }
		  else if(seconds > 60 && seconds < 3600){
			  display = parseInt(seconds/60) + " minutes ago"; 
		  }
		  else if(seconds > 3600 && seconds < 86400){
			  display = parseInt(seconds/60/60) + " hours ago"; 
		  }
		  else{
			  if( date.getFullYear() == UTCTime.getFullYear() ){
				  if( date.getMonth() == UTCTime.getMonth() ){
					  display = (UTCTime.getDate() - date.getDate()) + " days ago";
				  }
				  else{
					  display = (UTCTime.getMonth() - date.getMonth()) + " months ago";
				  }
			  }
			  else{
				  display = (UTCTime.getFullYear() - date.getFullYear()) + " years ago";
			  }
		 }
		 return display;
	  };
});
function checkBrowser(){
	var agt = navigator.userAgent.toLowerCase();
	if (agt.indexOf("chrome") != -1) return 'Chrome';
	if (agt.indexOf("msie") != -1) return 'Internet Explorer';
	if (agt.indexOf("safari") != -1) return 'Safari';
	if (agt.indexOf("firefox") != -1) return 'Firefox'; 
}
helpCenterApp.controller("helpCenterController", function($scope, $http, $location, $window, $sce, $timeout){
	$scope.initHelpCenter = function(data){
		console.log(data);
		$scope.topQuestions = data.top_qeustions;
		$scope.gnas = data.gnas;
		$scope.pnas = data.pnas;
		$scope.forTutors = data.forTutors;
		$scope.forTutees = data.forTutees;
		$scope.tps = data.tps;
		$scope.tss = data.tss;
		$scope.communities = data.communities;
		$scope.browser = checkBrowser();
	}
	$scope.goURL = function(url){
		$window.location.href = (url);
	}
	$scope.showHelpMenu = function(){
		$scope.helpMenu = true;
	}
	$scope.changeML = function(ml_id){
		$http.post( "/help/ml/", {ml: ml_id}).success( function( response ) {
			if ( response.isSuccess == 1 ) {
				$window.location.href = response.url;
			}
		});
	}
	$scope.searchFaq = function(){
		$("#search-faq").submit();
	}
});

$(document).ready(function(){
	$('#search-data-faq').keypress(function(event){
		console.log("keypress");
		  if(event.keyCode == 13){
		    angular.element($(".help-content")).scope().searchFaq();
		  }
	});
});