helpCenterApp.controller("helpCenterListController", function($scope, $http, $location, $window, $sce, $timeout){
	$scope.initTopicList = function(data){
		console.log(data);
		$scope.topicName = data.topic_name;
		$scope.topicList = data.topic_list;
	}
	$scope.goURL = function(url){
		$window.location.href = (url);
	}
});
helpCenterApp.controller("helpCenterSubController", function($scope, $http, $location, $window, $sce, $timeout){
	$scope.initTopicList = function(data){
		console.log(data);
		$scope.subTopicName = data.sub_topic_name;
		$scope.subTopicList = data.topic_list;
	}
	$scope.goURL = function(url){
		$window.location.href = (url);
	}
});
helpCenterApp.controller("helpCenterSearchController", function($scope, $http, $location, $window, $sce, $timeout){
	$scope.initSearch = function(data){
		console.log(data);
		$scope.faqs = data.faqs;
		$scope.searchWord = data.searchWord;
		$scope.topicList = [];
		$scope.topicList.push({"id": 1, "name":"Guide & Announcements", "url":"gna"});
		$scope.topicList.push({"id": 2, "name":"Profile & Account", "url":"pna"});
		$scope.topicList.push({"id": 3, "name":"For (Wannabe) Tutors", "url":"tutor"});
		$scope.topicList.push({"id": 4, "name":"For Tutees", "url":"tutee"});
		$scope.topicList.push({"id": 5, "name":"Tellpin Player", "url":"tellpinplayer"});
		$scope.topicList.push({"id": 6, "name":"Tellpin Search", "url":"tellpinsearch"});
		$scope.topicList.push({"id": 7, "name":"Community", "url":"community"});
		
		$scope.subTopicList = [];
		$scope.subTopicList.push({"id": 1, "name":"My Profile", "url":"myprofile"});
		$scope.subTopicList.push({"id": 2, "name":"Password", "url":"password"});
		$scope.subTopicList.push({"id": 3, "name":"Privacy", "url":"privacy"});
	}
});
helpCenterApp.controller("helpCenterDetailController", function($scope, $http, $location, $window, $sce, $timeout){
	$scope.initTopicDetail = function(data){
		console.log(data);
		$scope.faq = data.faq;
	}
	$scope.goURL = function(url){
		$window.location.href = (url);
	}
	$scope.trustAsHtml = function(html){
		return $sce.trustAsHtml(html);
	}
});