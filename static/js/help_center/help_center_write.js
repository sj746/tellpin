helpCenterApp.controller("myQuestionWriteController", function($scope, $http, $location, $window, $sce, $timeout){
	$scope.initWrite = function(data){
		$scope.categoryList = [];
		$scope.categoryList.push({"id": 0, "name":"Select Question Category"});
		$scope.categoryList.push({"id": 1, "name":"Profile & Account"});
		$scope.categoryList.push({"id": 2, "name":"Tutoring"});
		$scope.categoryList.push({"id": 3, "name":"Taking lessons"});
		$scope.categoryList.push({"id": 4, "name":"Payment"});
		$scope.categoryList.push({"id": 5, "name":"Withdrawal"});
		$scope.categoryList.push({"id": 6, "name":"Tellpin Search, Tellpin Player"});
		$scope.categoryList.push({"id": 7, "name":"Community"});
		$scope.category = 0;
		if(data != undefined){
			if(data.ref_id == undefined){
				console.log(data);
				$scope.qnaID = data.qna_id;
				$scope.qna = data.qna;
				$scope.category = $scope.qna.question_category;
				$scope.title =  $scope.qna.title;
				$("#qna-write-textarea").html($scope.qna.contents);
				if ($scope.attach_file != null) {
					$scope.qna.attach_file.filename = $scope.qna.attach_file.filename.substring(15, $scope.qna.attach_file.filename.length);
					$scope.attach = $scope.qna.attach_file;
				}
			}
			else{
				$scope.refID = data.ref_id;
			}
		}
	}
	$scope.submitQna = function(){
		var data = new FormData();
		data.append("category", $scope.category);
		data.append("title", $scope.title);
		data.append("content", tinymce.get("qna-write-textarea").getContent());
		data.append("file", $scope.attachedFile);
		data.append("ref_id", $scope.refID);
		$http.post( "/help/qna/save/", data, {
			withCredentials: true,
	        headers: {'Content-Type': undefined },
	        transformRequest: angular.identity
		}).success( function( response ) {
			if ( response.isSuccess == 1 ) {
				console.log(response);
				$window.location.href = (response.qnaURL);
			}
		});
	}
	$scope.updateQna = function(){
		var data = new FormData();
		data.append("category", $scope.category);
		data.append("qnaID", $scope.qnaID);
		data.append("title", $scope.title);
		data.append("content", tinymce.get("qna-write-textarea").getContent());
		if($scope.attach && $scope.attach != "undefined") {
			data.append("keep", true);
		} else {
			data.append("keep", false);
		}
		
		data.append("file", $scope.attachedFile);
		$http.post( "/help/qna/update/", data, {
			withCredentials: true,
	        headers: {'Content-Type': undefined },
	        transformRequest: angular.identity
		}).success( function( response ) {
			if ( response.isSuccess == 1 ) {
				console.log(response);
				$window.location.href = (response.qnaURL);
			}
		});
	}
	$scope.getWriteFile = function(files){
		$scope.attachedFile = files[0];
		console.log($scope.attachedFile.name);
		
		//{filepath: "/static/img/upload/1462256919461&&캡처332.PNG", filename: "캡처332.PNG"}
	}
	
	$scope.deleteAttachFile = function(qna_id) {
		if(confirm("Delete?")){
			$scope.attach = null;
		}
		
		
//		$http.post( "/help/qna/ff/").success(function(response) {
//			if ( response.isSuccess == 1 ) {
//				
//			}
//			
//		}).error(function(response) {
//			//console.log(response);
//		});
		
	}

});


function changeBlue(obj) {
	$(obj).css("color", "blue");
}
