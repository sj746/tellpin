helpCenterApp.controller("myQuestionListController", function($scope, $http, $location, $window, $sce, $timeout){
	$scope.initMyQuestions = function(data){
		console.log(data);
		$scope.myQuestions = data.my_questions;
		$scope.statusList = new Array();
		$scope.statusList.push({"name":"Any", "id":0});
		$scope.statusList.push({"name":"Open", "id":1});
		$scope.statusList.push({"name":"Awaiting your reply", "id":2});
		$scope.statusList.push({"name":"Closed", "id":3});
		$scope.status = 0;
	}
	$scope.changeQna = function(){
		$http.post( "/help/qna/status/", {status: $scope.status}).success( function( response ) {
			if ( response.isSuccess == 1 ) {
				$scope.myQuestions = response.my_questions;
			}
		});
	}
});

helpCenterApp.controller("myQuestionDetailController", function($scope, $http, $location, $window, $sce, $timeout){
	$scope.initMyQuestion = function(data){
		console.log(data);
		$scope.myQuestion = data.my_question;
		$scope.userID = data.user_id;
		$scope.categoryList = [];
		$scope.categoryList.push({"id": 0, "name":"Select Question Category"});
		$scope.categoryList.push({"id": 1, "name":"Profile & Account"});
		$scope.categoryList.push({"id": 2, "name":"Tutoring"});
		$scope.categoryList.push({"id": 3, "name":"Taking lessons"});
		$scope.categoryList.push({"id": 4, "name":"Payment"});
		$scope.categoryList.push({"id": 5, "name":"Withdrawal"});
		$scope.categoryList.push({"id": 6, "name":"Tellpin Player"});
	}
	$scope.followUpQuetion = function(ref_id){
		$("#follow-up-question").submit();
	}
	$scope.editQna = function($index){
		$("#edit-qna-"+$index).submit();
	}
	$scope.deleteQna = function(qna_id){
		if(confirm("Delete?")){
			$http.post( "/help/qna/delete/", {qnaID: qna_id}).success( function( response ) {
				if ( response.isSuccess == 1 ) {
					$window.location.href = (response.qnaURL);
				}
			});
		}
		else{
			return;
		}
	}
	$scope.changeBlue = function(obj) {
		console.log(obj);
		$(obj).css("color", "blue");
	}
});


helpCenterApp.filter('sliceAttachName', function() {
	  return function(value) {
		  return value.substring(15, value.length);;
	  };
});

function changeBlue(obj) {
	$(obj).css("color", "blue");
}

