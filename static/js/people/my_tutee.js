tellpinApp.controller( 'peopleTuteeCtrl', ['$scope', '$window', '$http', function( $scope, $window, $http ) {
	$scope.init = function( obj ) {
//		console.log( obj );
		$scope.friends = obj.tutees;
		console.log($scope.friends);
		
		for(var i = 0; i < obj.onoff_check.length; i++){
			$scope.friends[i].is_login = obj.onoff_check[i].is_login;
		}
	}

	$scope.openModal = function( id, fr_id ) {
		$scope.id = fr_id;
		console.log( fr_id );
		angular.element( '#' + id ).modal();
	}

	$scope.hideModal = function( id ) {
		angular.element( '#' + id ).modal( 'hide' );
	}

	// 개인 메세지창	
	$scope.openMessageModal = function (msg_type, id) {
		$scope.$emit('messageEventEmit', { type: msg_type, user_id: id });
		$window.event.stopPropagation();
	};
}]);