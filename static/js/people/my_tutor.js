tellpinApp.controller( 'peopleTutorCtrl', ['$scope', '$window', '$http', function( $scope, $window, $http ) {
	$scope.init = function( obj ) {
		console.log( obj );
		$scope.tutor = obj.tutor;

		for(var i = 0; i < obj.onoff_check.length; i++){
			$scope.tutor[i].is_login = obj.onoff_check[i].is_login;
		}
		
		console.log( $scope.tutor );
		$scope.messageSelect_kind = true;
		$scope.messageSelect_often = true;
		$scope.messageSelect_why = true;
	}
	
	$scope.openMessage = function( tutor ) {
		$scope.user = tutor;
		console.log( $scope.messageSelect );
		$scope.messageSelect_kind = true;
		$scope.messageSelect_often = true;
		$scope.messageSelect_why = true;
		$scope.messageKind = undefined;
		$scope.messageOften = undefined;
		$scope.messageWhy = undefined;
		$scope.messageModel = "";
		$scope.messageValid = "";
		angular.element( '#messageModal' ).modal();
	}
	
	$scope.openMessageModal = function (msg_type, id) {
		$scope.$emit('messageEventEmit', { type: msg_type, user_id: id });
		$window.event.stopPropagation();
	};
	
	$scope.sendMessage = function() {
		console.log( $scope.messageKind );
		$scope.messageValid = "";
		if ( !$scope.messageKind)
			$scope.messageValid += "select messageKind\n";
		
		if ( !$scope.messageOften )
			$scope.messageValid += "select messageOften\n";
		
		if ( !$scope.messageWhy )
			$scope.messageValid += "select messageWhy\n";
		
		if ( !$scope.messageModel )
			$scope.messageValid += "input message text";
		
		if ( $scope.messageValid != "" ) {
			alert( $scope.messageValid );
			return;
		}
		var message = "What kind of language skills are you particularly interested in?\r\n"
			+ $scope.messageKind
			+ "\r\nHow often are you planning to take lessons?\r\n"
			+ $scope.messageOften
			+ "\r\nWhy are you planning to take a lesson?\r\n"
			+ $scope.messageWhy
			+ "\r\n"
			+ $scope.messageModel;
		console.log( message );
		console.log( $scope.user.id );
		$http.post( "/tutor/sendmessage/", { tid : $scope.user.id, content : message } )
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				alert( "Message send success!");
			}
			else {
				alert( "Message send fail!" );
			}				
			$( "#messageModal" ).modal( "hide" );
		});
	}
	
	$scope.openHideModal = function( tutor ) {
		$scope.user = tutor
		angular.element( '#hideModal' ).modal();
	}
	
	$scope.closeHideModal = function() {
		angular.element( '#hideModal' ).modal( 'hide' );
	}
	
	$scope.sendHide = function() {
		console.log( $scope.user.id );
		$http.post( '/people/tutor/hide/', { 'hide_id' : $scope.user.id } )
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				$scope.tutor = result.tutor;
				$scope.closeHideModal();
			}
			else {
				alert( 'failed' );
			}
		});
	}
	
}]);