tellpinApp.controller( 'peopleFriendCtrl', ['$scope', '$window', '$http', function( $scope, $window, $http ) {
	$scope.init = function( obj ) {
		console.log( obj );
		$scope.friends = obj.friends;
		
		for(var i = 0; i < obj.onoff_friends.length; i++){
			$scope.friends[i].is_login = obj.onoff_friends[i].is_login;
		}
		
		$scope.requestFriends = obj.requestFriends;
		
		for(var i = 0; i < obj.onoff_requestFriends.length; i++){
			$scope.requestFriends[i].is_login = obj.onoff_requestFriends[i].is_login;
		}
		
		$scope.messageSelect_kind = true;
		$scope.messageSelect_often = true;
		$scope.messageSelect_why = true;
		$scope.tab = 'receiveRequest'; // sendRequest : 내가 요청 보냄, receiveRequest : 내가 요청 받음
	}
	
//	$scope.openHideModal = function( tutor ) {
//		$scope.user = tutor
//		angular.element( '#hideModal' ).modal();
//	}
	
//	$scope.closeHideModal = function() {
//		angular.element( '#hideModal' ).modal( 'hide' );
//	}
	
	$scope.sendHide = function() {
		console.log( $scope.user.id );
		$http.post( '/people/tutor/hide/', { 'hide_id' : $scope.user.id } )
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				$scope.tutor = result.tutor;
			}
			else {
				alert( 'failed' );
			}
		});
	}
	
	// 요청한 친구목록
	$scope.getRequestFriends = function() {
		if ( $scope.tab != 'sendRequest' ) {
			$http.post( '/people/friend/request/' )
			.success( function( result ) {
				console.log( result );
				$scope.requestFriends = result.requestFriends;
				$scope.tab = 'sendRequest';
				
				for(var i = 0; i < result.onoff_requestFriends.length; i++){
					$scope.requestFriends[i].is_login = result.onoff_requestFriends[i].is_login;
				}
			});			
		} 
		else {
			return;
		}
	}
	
	// 요청받은 친구목록
	$scope.getReceiveFriends = function() {
		if ( $scope.tab != 'receiveRequest' ) {
			$http.post( '/people/friend/receive/' )
			.success( function( result ) {
				console.log( result );
				$scope.requestFriends = result.receiveFriends;
				$scope.tab = 'receiveRequest';
				
				for(var i = 0; i < result.onoff_receiveFriends.length; i++){
					$scope.requestFriends[i].is_login = result.onoff_receiveFriends[i].is_login;
				}
			});			
		}
		else {
			return;
		}
	}
	
	// 개인 메세지창	
	$scope.openMessageModal = function (msg_type, id) {
		$scope.$emit('messageEventEmit', { type: msg_type, user_id: id });
		$window.event.stopPropagation();
	};
	
	// 친구 요청 승인
	$scope.acceptFriend = function( fr_id, type ) {
		console.log( type );
		if ( $scope.requestFriends.length < 1 ) return;
		$http.post( '/people/friend/accept/', { 'id' : fr_id, 'type' : type } )
		.success( function( result ) {
			console.log( result );
			if ( result.isSuccess == 1 ) {
				$scope.requestFriends = result.requestFriends;
				$scope.friends = result.friends;
				
				for(var i = 0; i < result.onoff_friends.length; i++){
					$scope.friends[i].is_login = result.onoff_friends[i].is_login;
				}
				
				for(var i = 0; i < result.onoff_requestFriends.length; i++){
					$scope.requestFriends[i].is_login = result.onoff_requestFriends[i].is_login;
				}
			}
		});
	}
	
	// 친구 요청 거절
	$scope.declineFriend = function( modal, type ) {
		if ( $scope.requestFriends.length < 1 ) return;
		$http.post( '/people/friend/decline/', { 'fr_id' : $scope.id, 'type' : type } )
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				$scope.requestFriends = result.requestFriends;
				
				for(var i = 0; i < result.onoff_requestFriends.length; i++){
					$scope.requestFriends[i].is_login = result.onoff_requestFriends[i].is_login;
				}
			}
			$scope.hideModal( modal );
		});
	}
	
	// 친구 삭제
	$scope.deleteFriend = function( modal ) {
		$http.post( '/people/friend/delete/', { 'fr_id' : $scope.id } )
		.success( function( result ) {
			if ( result.isSuccess == 1) {
				$scope.friends = result.friends;

				for(var i = 0; i < result.onoff_friends.length; i++){
				$scope.friends[i].is_login = result.onoff_friends[i].is_login;
				}
			}
			$scope.hideModal( modal );
		});
	}
	
	// 친구 요청 취소
	$scope.cancelRequestFriend = function( modal ) {
		$http.post( '/people/friend/cancel/', { 'fr_id' : $scope.id } )
		.success( function( result ) {
			if ( result.isSuccess == 1) {
				$scope.requestFriends = result.requestFriends;

				for(var i = 0; i < result.onoff_requestFriends.length; i++){
					$scope.requestFriends[i].is_login = result.onoff_requestFriends[i].is_login;
				}
			}
			$scope.hideModal( modal );
		});
	}
	
	$scope.ignoreAllRequest = function( modal ) {
		if ( $scope.requestFriends.length < 1 ) {
			return;
		}
		else {
			$scope.openModal( modal );
		}
	}
	
	$scope.openModal = function( id, fr_id ) {
		console.log("openModalopenModal");
		$scope.id = fr_id;
		console.log( fr_id );
		console.log( id );
		angular.element( '#' + id ).modal();
	}
	
	$scope.hideModal = function( id ) {
		angular.element( '#' + id ).modal( 'hide' );
	}
	
}]);