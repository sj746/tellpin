// youtube get video id
// (?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|playlist\?|watch\?v=|watch\?.+(?:&|&#38;);v=))([a-zA-Z0-9\-_]{11})?(?:(?:\?|&|&#38;)index=((?:\d){1,3}))?(?:(?:\?|&|&#38;)?list=([a-zA-Z\-_0-9]{34}))?(?:\S+)?
/*var tutorApp = angular.module( "tutorApp", ['ngSanitize'] );

tutorApp.config( function( $interpolateProvider) {
	$interpolateProvider.startSymbol('[[');
	$interpolateProvider.endSymbol(']]');
});*/


tellpinApp.controller( "infoCtrl", ["$scope","$http", "$sce", "$location", "$window", function( $scope, $http, $sce, $location, $window ) {
	$scope.init = function( obj ) {		
		$scope.data = obj;
		console.log($scope.data);
		$scope.tutor = $scope.data.profile;
		$scope.tutor.description = $scope.tutor.long_intro.replace( /\n/gi, '<br />' );
		$scope.tutor.video_intro = $scope.youtubeRegex( $scope.tutor.video_intro );
		$scope.lessons = $scope.data.lesson_list;
		$scope.resumeList = $scope.data.resume_list;
		$scope.trial = $scope.data.trial;
//		$scope.display = $scope.data.display;
		$scope.myPage = $scope.data.mypage;
		//console.log( $scope.tutor );
		//console.log( $scope.education );
		$scope.statistics = $scope.data.statistics;
		if ( $scope.statistics.AR.all_time ) {
			$scope.statistics.AR.all_time = $scope.statistics.AR.all_time.toFixed( 1 );
			$scope.statistics.AR.last_month = $scope.statistics.AR.last_month.toFixed( 1 );
		}
		//$scope.videoInfo = $scope.data.video_list;
		//$scope.videoList = $scope.data.video_list.entries;
		//$scope.lastPage = $scope.videoPage( $scope.videoInfo.startPage, $scope.videoInfo.endPage );
		//$scope.videoCount = $scope.data.video_list.total;
		//$scope.videoMobileIndex = 1; // 비디오리스트 인덱스
		//$scope.videoEntryIndex = 1; // 비디오 화면 인덱스

/*
		$scope.ratingInfo = $scope.data.Ratings;
		$scope.ratingList = $scope.data.Ratings.entries;
		$scope.ratingLastPage = $scope.videoPage( $scope.ratingInfo.startPage, $scope.ratingInfo.endPage );
		$scope.moreFlag = new Array( $scope.ratingList.length );
		for ( var i = 0; i < $scope.moreFlag.length; i++ ) {
			$scope.moreFlag[i] = true;
		}
		console.log( $scope.moreFlag );
*/

		// add video src
		//for ( var i = 0; i < $scope.videoList.length; i++ ) {
		//	$scope.videoList[i].video_src = "/static/img/upload/" + $scope.videoList[i].video_src; 
		//}
		// resume split
		//console.log( $scope.resumeList );
		$scope.resumeList = $scope.resumeSplit( $scope.resumeList );
		//console.log( $scope.resumeList );
		$scope.messageSelect_kind = true;
		$scope.messageSelect_often = true;
		$scope.messageSelect_why = true;
		$scope.followBtn = false;
		if ( $scope.tutor.is_bookmark == 0 ) {
			$scope.followBtntxt = "Follow";			
		}
		else {
			$scope.followBtntxt = "Following";
		}

		//community activities
		$scope.languageInfo = $scope.data.languageInfo;
		$scope.all = $scope.data.all;
		$scope.AllShowMore = $scope.data.AllShowMore;
		$scope.LBs = $scope.data.LBs;
		$scope.LBShowMore = $scope.data.LBShowMore;
		$scope.TPs = $scope.data.TPs;
		$scope.TPShowMore = $scope.data.TPShowMore;
		$scope.LQs = $scope.data.LQs;
		$scope.LQShowMore = $scope.data.LQShowMore;
		$scope.selectedCommunityTab = 0;
		$scope.allPage = obj.page;
		$scope.lcPage = 0;
		$scope.tpPage = 0;
		$scope.lqPage = 0;
		
		if ($scope.tutor.currency == null) {
			$scope.tutor.currency = "KRW";
		}

		if ($scope.tutor.schedule_comment == null) {
			$scope.tutor.schedule_comment = "comment";
		}
		angular.element( "[data-toggle='tooltip']" ).tooltip();
//		$scope.get_tool_list_width('id_middle_how_left');
//		$scope.get_tool_list_width('id_middle_how_right');
	}// init

	$scope.goURL = function(url){
		$window.location.href = (url);
		$window.event.stopPropagation();
	}

	$scope.goCommunityDetail = function(category, id){
		if(category == 'Board')
			$window.location.href = ('/board/'+ id);
		else if(category == 'TranslationProofreading')
			$window.location.href = ('/trans/'+ id);
		else if(category == 'Question')
			$window.location.href = ('/questions/'+ id);
		$window.event.stopPropagation();
	}

	$scope.openMessageModal = function() {
		console.log( $scope.messageSelect );
		$scope.messageSelect_kind = true;
		$scope.messageSelect_often = true;
		$scope.messageSelect_why = true;
		$scope.messageKind = undefined;
		$scope.messageOften = undefined;
		$scope.messageWhy = undefined;
		$scope.messageModel = "";
		$scope.messageValid = "";
		$("#messageModal").modal({
		    backdrop: 'static'
		});
	}
	
	// Language level string split
	// input : "a:bcd", return "bcd"
	$scope.levelSplit = function( str ) {
		return str.split( ":" )[1];
	}
	
	// 강의 선택시 모달 출력
	$scope.clickLesson = function( obj ) {
		console.log( "click : " + obj );
		$scope.detailLesson = obj;
		console.debug( $scope.detailLesson );
		$scope.clickLessonLid = obj.id;
		$scope.detailLesson.isBundle = $scope.bundleCheck( $scope.detailLesson );
		console.log( $scope.clickLessonLid );
		$( "#lessonModal" ).modal();
	}
	
	// 비디오레슨 영역 페이징
	$scope.videoPage = function( start, end ) {
		var temp = [];
		for ( var i = start; i <= end; i++ ) {
			temp.push( i );
		}
		return temp;
	}
	
	// Resume 정보관련 엔터단위로 split 처리
	$scope.resumeSplit = function( arr ) {
		for( var i = 0; i < arr.length; i++ ) {
			var edu = arr[i].education;
			var cer = arr[i].certification;
			var work = arr[i].work_exp;
			if ( arr[i].education ) {
				arr[i].education = edu.split( "\n" ).slice();				
			}
			if ( arr[i].certification ) {
				arr[i].certification = cer.split( "\n").slice();				
			}
			if ( arr[i].work_exp ) {
				arr[i].work_exp = work.split( "\n" ).slice();				
			}
		}
		return arr;
	}
	
	// 비디오 페이징
	$scope.getMoreVideo = function( idx ) {
		$http.post( "/tutor/videopage/", { page : idx, tid : $scope.tutor.user } )
		.success( function ( result ) {
			//console.log( result );
			$scope.videoInfo = result.video_list;
			$scope.videoList = result.video_list.entries;
			$scope.lastPage = $scope.videoPage( $scope.videoInfo.startPage, $scope.videoInfo.endPage );
			$scope.videoCount = result.video_list.total;
		});
	}
	
	// 모바일 비디오 페이징
	$scope.getMoreMobileVideo = function( idx, type ) {
		console.log( $scope.videoMobileIndex );
		if ( type == "prev" ) {
			if ( idx < 1 ) {
				if ( $scope.videoInfo.page == 1 || $scope.videoEntryIndex == 1 )
					return;
				else {
					$scope.getMoreVideo( $scope.videoInfo.page - 1 );
					$scope.videoMobileIndex = 3;
					$scope.videoEntryIndex--;
				}
			}
			else {
				$scope.videoMobileIndex--;
				$scope.videoEntryIndex--;
			}
		}
		else {
			console.log( "next" );
			if ( idx > 3 ) {
				if ( $scope.videoInfo.page == $scope.videoInfo.last_page )
					return;
				else {
					$scope.getMoreVideo( $scope.videoInfo.page + 1);
					$scope.videoMobileIndex = 1;
					$scope.videoEntryIndex++;
				}
			}
			else {
				if ( $scope.videoCount == $scope.videoEntryIndex ) {
					return;					
				} 
				else {
					$scope.videoMobileIndex++;
					$scope.videoEntryIndex++;					
				}
			}
		}
	}
	
	$scope.setBookmark = function( tid ) {
		if ( $scope.tutor.is_bookmark == 1 ) {
			$scope.removeBookmark( tid );
		}
		else {
			$http.post( "/tutor/follow/", { tid : tid } )
			.success( function( result ) {
				$scope.followBtn = false;
				$scope.followBtntxt = "Following";
				$scope.tutor.is_bookmark = result.follow;
			});			
		}
	}
	
	$scope.removeBookmark = function( tid ) {
		$http.post( "/tutor/follow/", { tid : tid } )
		.success( function( result ) {
			$scope.tutor.is_bookmark = result.follow;
			$scope.followBtntxt = "Follow";
			angular.element(".following-btn").css("display", "inline");
			angular.element(".unfollow-btn").css("display", "none");
		});
	}

	$scope.removeBookmarkHover = function() {
		angular.element(".unfollow-btn").css("display", "none");
		angular.element(".following-btn").css("display", "inline");
	}
	
	// 댓글 더 불러오기
	$scope.getMoreRating = function( idx ) {
		$http.post( "/tutor/ratingpage/", { page : idx, tid : $scope.tutor.user } )
		.success( function ( result ) {
			console.log( result );
			$scope.ratingInfo = result.Ratings;
			$scope.ratingList = result.Ratings.entries;
			$scope.ratingLastPage = $scope.videoPage( $scope.ratingInfo.startPage, $scope.ratingInfo.endPage );
			$scope.moreFlag = new Array( $scope.ratingList.length );
			for ( var i = 0; i < $scope.moreFlag.length; i++ ) {
				$scope.moreFlag[i] = true;
			}
			console.log( $scope.moreFlag );
		});
	}
	
	// 댓글 더보기 기능
	$scope.moreFlagToggle = function( idx, rtid, user_id ) {
		if ( $scope.moreFlag[idx] ) {
			$scope.moreFlag[idx] = false;
			if ( $scope.ratingList[idx].moreRating ) {
				return;
			}
			$http.post( '/tutor/rating/more/', { 'rtid' : rtid, 'tutee_id' : user_id, 'tutor_id' : $scope.tutor.user } )
			.success( function( result ) {
				if ( result.isSuccess ==  1 ) {
					console.log( result );
					$scope.ratingList[idx].moreRating = result.moreRating;
				}
			});
		}
		else {
			$scope.moreFlag[idx] = true;			
		}
	}
	
	// 신고 모달 띄우기
	$scope.openReportModal = function( idx ) {
		if ( idx == 1 ) {
			$( "#reportModal" ).modal();
			$scope.reportType = 1;
			$scope.reportTxt = "";
		}
		else {
			$( "#reportModal" ).modal( "hide" );			
		}
	}
	
	// 신고하기
	$scope.sendReport = function() {
		console.log( $scope.reportType + " : " + $scope.reportTxt );
		$http.post( "/tutor/report/", { tid : $scope.tutor.user, type : $scope.reportType, content : $scope.reportTxt } )
		.success( function( result ) {
			alert( "Your problem has been received." );
			$( "#reportModal" ).modal( "hide" );
		});
	}
	
	// 유저숨기기 모달 띄우기
	$scope.addHide = function( idx ) {
		if ( idx == 1) 
			$( "#hideModal" ).modal();
		else
			$( "#hideModal" ).modal( "hide" );
	}
	
	// 유저숨기기
	$scope.sendHide = function() {
		$http.post( "/tutor/hide/", { tid : $scope.tutor.user } )
		.success( function( result ) {
			$( "#hideModal" ).modal( "hide" );
			$window.location.href = "/tutor/";
		})
	}
	
	$scope.clickEditProfile = function() {
		$window.location.href = "/settings/editprofile/";
	}

	// 튜티 프로필페이지 이동
	$scope.switchProfile = function() {
		$window.location.href = "/user/" + $scope.tutor.user;
	}
	
	$scope.sendMessage = function() {
		console.log( $scope.messageKind );
		$scope.messageValid = "";
		if ( !$scope.messageKind)
			$scope.messageValid += "select messageKind\n";
		
		if ( !$scope.messageOften )
			$scope.messageValid += "select messageOften\n";
		
		if ( !$scope.messageWhy )
			$scope.messageValid += "select messageWhy\n";
		
		if ( !$scope.messageModel )
			$scope.messageValid += "input message text";
		
		if ( $scope.messageValid != "" ) {
			alert( $scope.messageValid );
			return;
		}
		var message = "1) What kind of language skills are you particularly interested in?\r\n"
			+ $scope.messageKind
			+ "\r\n2) How often are you planning to take lessons?\r\n"
			+ $scope.messageOften
			+ "\r\n3) Why are you planning to take a lesson?\r\n"
			+ $scope.messageWhy
			+ "\r\n"
			+ $scope.messageModel;
		console.log( message );
		$http.post( "/tutor/sendmessage/", { tid : $scope.tutor.user, content : message } )
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				alert( "Your message has been sent.");
			}
			else {
				alert( "Message send fail!" );
			}				
			$( "#messageModal" ).modal( "hide" );
		});
	}

	// check bundle
	$scope.bundleCheck = function( lesson ) {
		//console.log( lesson );
		if ( lesson.pkg_30min_times == 1 || lesson.pkg_60min_times == 1 
				|| lesson.pkg_90min_times == 1 )
			return true;
		else
			return false;
	}

	$scope.youtubeRegex = function( url ) {
		if (url == null || url.trim() == "")
			return null;
		var regExp = /(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|playlist\?|watch\?v=|watch\?.+(?:&|&#38;);v=))([a-zA-Z0-9\-_]{11})?(?:(?:\?|&|&#38;)index=((?:\d){1,3}))?(?:(?:\?|&|&#38;)?list=([a-zA-Z\-_0-9]{34}))?(?:\S+)?/g;
		var result = regExp.exec( url );
		console.log( result );
		if (result == null || result[1].trim() == "")
			return null; 
		
		var src = "https://www.youtube.com/embed/" + result[1] + "?wmode=opaque";
		return src;
	}

	$scope.trustSrc = function( src ) {
		return $sce.trustAsResourceUrl( src );
	}
	
	$scope.messageUpdate = function( idx ) {
		if ( idx == 0 ) {
			$scope.messageSelect_kind = false;			
		}
		else if ( idx == 1 ) {
			$scope.messageSelect_often = false;			
		}
		else {
			$scope.messageSelect_why = false;			
		}
	}
	
	$scope.showSchedulePopup = function(tid){
		/*$("body").css("overflow-y","hidden");
		$("body").css("overflow-x","hidden");*/
		$http.post( "/tutor/tutor_schedule/", {tid: tid}).success( function( response ) {
			if ( response.isSuccess == 1 ) {
				$scope.scheduleInfo = response;
				$scope.schedulePopup = true;
				var scrollWith = angular.element( "body" ).innerWidth(); 
				angular.element( "body" ).addClass( "modal-open" );
				var scrollWithOut = angular.element( "body" ).innerWidth();
				var scrollWidth = scrollWithOut - scrollWith;
				scrollWith += "px";
				scrollWidth += "px";
				console.log( scrollWidth );
			}
		});
	}

	$scope.hideSchedulePopup = function(){
		$scope.schedulePopup = false;
		//$("body").css("overflow-y","auto");
		//$("body").css("overflow-x","auto");
		angular.element( "body" ).removeClass( "modal-open" );
	}

	//click trial button
	$scope.bookTrialLesson = function(){
		var trial = $scope.tutor.trial_price;
		var balance = 0;
		console.log('bookTrialLesson');
		$http.post("/wallet/get_wallet_history_pop/").success( function( result ){
			console.log('test1');
			if(result.isSuccess == 1){
				console.log(result);
				balance = result.balance_history.available_balance;
				$scope.coin_history = result.coin_history[0];
				$scope.balance_history = result.balance_history;
//				$scope.refresh_data();
				
				if( balance < trial ){
					//TODO 
					//show popup
					//$("#checkBalance").modal();
					$window.location.href = "/wallet/buy_credits/";
				}else{
					$("#form_trial").submit();
				}
			}
		});
	}
	$scope.refresh_data = function(){
		var coin = $("#id_coin").val();
		$("#buy_coin").text(coin+" C");
		var prev = $scope.coin_history[0].available_balance;
		var after = $("#after_coin").text();
		console.log(coin+', '+prev+', '+after);
		$("#after_coin").text( parseInt(prev) + parseInt(coin) + " C");
		$("#purchase_amount").text( parseInt(coin)/10 + " USD" );
		$("#total_amount").text( parseInt(coin)/10.0 + 0.34 + " USD");
	}
	
	$scope.openBuyCoinModal = function(){
		var coin = $("#id_coin").val();
		console.log(coin);
		
		$http.post("/coin/buy_coin/", {coin: coin}).success( function( result ) {
			if(result.isSuccess == 1){
				
				//데이터 갱신
				$http.post("/coin/get_coin_history/").success( function( result ) {
					if(result.isSuccess == 1){
						console.log('refresh()');
						$scope.coin_history = result.coin_history[0].coin_list;
						$scope.refresh_data();
						$("#checkBalance").modal('hide');
					}
				});				
			}
		});
	}
	
	$scope.showTooltip = function() {
		console.log( "tooltip" );
		angular.element( "#intermediate" ).tooltip( "show" );
	}
	
	$scope.clickTab = function( tab ) {
		if ( $scope.selectedCommunityTab == tab ) {
			return;
		} 
		$scope.selectedCommunityTab = tab;
	}
	
	$scope.getMoreCommunity = function( page, type ) {
		$http.post( '/exchange/more/community/', { 'page' : page, 'type' : type, 'id' : $scope.tutor.user } )
		.success( function( result ) {
			if ( type == 'all' ) {
				$scope.all = $scope.all.concat( result.list );
				$scope.AllShowMore = result.AllShowMore;
				$scope.allPage = result.page;
			}
			else if ( type == 'LB' ) {
				$scope.LBs = $scope.LBs.concat( result.list );
				$scope.LBShowMore = result.LBShowMore;
				$scope.lbPage = result.page;
			}
			else if ( type == 'TP' ) {
				$scope.TPs = $scope.TPs.concat( result.list );
				$scope.TPShowMore = result.TPShowMore;
				$scope.tpPage = result.page;
			}
			else {
				$scope.LQs = $scope.LQs.concat( result.list );
				$scope.LQShowMore = result.LQShowMore;
				$scope.lqPage = result.page;
			}
		});
	}
	
	$scope.goUrlAfterLogin = function(url) {
			if(url.length > 0){
				location.href = "/user_auth/ajax_nologin/?url=" + url;
			}
		}
	
}]);

//for schedule
tellpinApp.filter('scheduleHeaderFiler', function() {
	  return function(arr) {
		  start = arr[0];
		  end = arr[arr.length-1];
		  var monthNames = ["January", "February", "March", "April", "May", "June",
		                    "July", "August", "September", "October", "November", "Decembe"];
		  var headerString = monthNames[ parseInt(start.slice(4,6))-1] + " " + start.slice(6,8) + "-"+end.slice(6,8)+", "+ start.slice(0,4);
		  headerString = start.slice(0,4) + "-" + start.slice(4,6) + "-" + start.slice(6,8) + " ~ "+ end.slice(0,4) + "-" + end.slice(4,6) + "-" + end.slice(6,8);
	    return headerString
	  };
});

tellpinApp.filter('selectedTableFilter', function() {
	  return function(arr) {
	    return arr.slice(0,4)+"."+arr.slice(4,6)+"."+arr.slice(6,8);
	  };
});
tellpinApp.filter('scheduleDateFilter', function() {
	  return function(arr, index) {
		  if(arr.slice(4,5) == "0"){
			  return arr.slice(5, 6)+"/"+arr.slice(6,8);
		  }
		  else{
			  return arr.slice(4, 6)+"/"+arr.slice(6,8);
		  }
	  };
});
tellpinApp.filter('capitalize', function() {
	  return function(arr) {
		  return arr.charAt(0).toUpperCase() + arr.slice(1,3);
	  };
});

tellpinApp.controller( "scheduleController", function( $scope, $http, $location, $window, $filter ) {
	$scope.showData = function(){
		console.log($scope.schedule);
	}
	$scope.setWeekday = function(){
		$scope.weekList = [];
		for(var idx = 0; idx <7 ; idx++){
			$scope.weekList.push(dayToAlphabet($scope.schedule.dow_list[idx]));
		}
	}
	$scope.setReservedTime = function(){
		$scope.reservedTime = [];
		for(var idx = 0; idx < $scope.reserved.length ; idx++){
			var bundleSize = $scope.reserved[idx].duration / 30;
			var day = $scope.reserved[idx].rv_time.slice(0,1);
			var index = -1;
			if($scope.reserved[idx].rv_time.slice(3,5) == "30"){
				index = parseInt($scope.reserved[idx].rv_time.slice(1,3))*2 +1;
			}
			else{
				index = parseInt($scope.reserved[idx].rv_time.slice(1,3))*2;
			}
			for(var i = 0 ; i < bundleSize; i++){
				$scope.reservedTime.push(day+indexToValue(index+i));
			}
		}
	}
	$scope.area = [];
	$scope.initAreaValue = function(day, index){
		$scope.bundleCount = $scope.bundle;
		var time = day;
		if(index%2 == 0){
			if(index/2 < 10){
				time = time + "0" + index/2 + "00";
			}
			else{
				time = time + index/2 + "00";
			}
		}
		else{
			if(index/2 < 10){
				time = time + "0" + ((index-1)/2) + "30";
			}
			else{
				time = time + ((index-1)/2) + "30";
			}
		}
		return time;
	}
	$scope.initAreaState = function(day, index){
		
		var dow = {"mon":"A" , "tue":"B", "wed":"C", "thu":"D", "fri":"E", "sat":"F", "sun":"G"}
		
		$scope.setCurrentTime();
		if( $scope.scheduleInfo.today == $scope.schedule.mark_day ) {
			if (day == dow[$scope.schedule.dow_list[0]]) {
				if ( index < $scope.currentTime * 2 + parseInt($scope.currentMin / 30) + 1 ){
					
					if($scope.reservedTime.indexOf($scope.area[index][day].value) != -1){
						return 3;
					}
					
					return 0;
				}
			}
		}
		
		if($scope.reservedTime.indexOf($scope.area[index][day].value) != -1){
			return 3;
		}
		else{
			if($scope.schedule.available_time.indexOf($scope.area[index][day].value) != -1){
				for(var i = 0 ; i<$scope.schedule.appointed.length; i++){
					if($scope.schedule.appointed[i].delta_time == $scope.area[index][day].value){
						if($scope.schedule.appointed[i].type == 2){
							return 0;
						}
					}
				}
				return 1;
			}
			else{
				for(var i = 0 ; i<$scope.schedule.appointed.length; i++){
					if($scope.schedule.appointed[i].delta_time == $scope.area[index][day].value){
						if($scope.schedule.appointed[i].type == 1){
							return 1;
						}
					}
				}
				return 0;
			}
		}
	};
// duration에 따라 예약가능한 영역인지 확인
	$scope.checkAvailableTime = function(index, day){
			if($scope.area[index][day].state == 1){
				$scope.area[index][day].isHover = 1;
			}
	};
	$scope.resetIsHover = function(index, day){
		if($scope.duration == 60){
			if($scope.area[index][day].isHover == 1 && $scope.area[index+1][day].isHover == 1){
				$scope.area[index][day].isHover = 0;
				$scope.area[index+1][day].isHover = 0;
			}
		}
		else{
			if($scope.area[index][day].isHover = 1){
				$scope.area[index][day].isHover = 0;
			}
		}
	};

	$scope.getPrevWeek = function(tid){
		$http.post("/tutor/tutor_schedule/prev/", {tid: tid, date : $scope.schedule.mark_day}).success(function(response){
			$scope.schedule= response.schedule;
			$scope.reserved = response.reserved;
			$scope.setReservedTime();
			$scope.initAllArea();
		});
	};
	$scope.getNextWeek = function(tid){
		$http.post("/tutor/tutor_schedule/next/", {tid: tid, date : $scope.schedule.mark_day}).success(function(response){
			$scope.schedule= response.schedule;
			$scope.reserved = response.reserved;
			$scope.setReservedTime();
			$scope.initAllArea();
		});
	};
	$scope.initAllArea = function(){
		for(var index=0; index < $scope.area.length; index++){
			$scope.area[index].A.state = $scope.initAreaState('A', index);
			$scope.area[index].B.state = $scope.initAreaState('B', index);
			$scope.area[index].C.state = $scope.initAreaState('C', index);
			$scope.area[index].D.state = $scope.initAreaState('D', index);
			$scope.area[index].E.state = $scope.initAreaState('E', index);
			$scope.area[index].F.state = $scope.initAreaState('F', index);
			$scope.area[index].G.state = $scope.initAreaState('G', index);
		}
	};
	$scope.getTbodyHeight = function(){
		var height = window.innerHeight - 70 - 140;
		if(height > 722){
			height = "722px";
		}
		$(".schedule-table tbody").height(height);
	}
	
	$scope.goToReservation = function (){
		var $form = $('<form></form>');
	    $form.attr('action', '/tutor/reservation/which/');
	    $form.attr('method', 'post');
	    $form.appendTo('body');
	
		var tid = $('<input type="hidden" value="' +$scope.tutor.user+ '" name="tid">');
	    var accessType = $('<input type="hidden" value="0" name="accessType">');
	
	    $form.append(tid).append(accessType);
	    $form.submit();
	};
	
	$scope.hoverIn = function(){
	    $(".tr-schedule-template table tbody tr").hover(function() {
			$(this).prev().find(".time").css("background", "#ececec");
			
		}, function() {
			$(this).prev().find(".time").css("background", "none");
		});
	};
	
	$scope.initCurrentTime = function(utcTime) {
		$scope.userUTCTime = utcTime;
	}
	
	$scope.setCurrentTime = function(){
		
		var now = new Date();
		$scope.currentUTCTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
		$scope.changeTimezone();
		//$timeout($scope.setCurrentTime, 1000);
		//$scope.updateTimeTable();
	};
	
	$scope.changeTimezone = function(){
		if($scope.userUTCTime == ''){
			$scope.userUTCTime = minutesToUTC(-(new Date().getTimezoneOffset()));
		}
		var displayTime = new Date($scope.currentUTCTime.getTime() + utcToMilliseconds($scope.userUTCTime));
		$scope.currentTime = displayTime.format("HH");
		$scope.currentMin = displayTime.format("mm");
		
		$scope.displayTime = displayTime;
	}
	
});
function dayToAlphabet(day){
	var dayList = ["mon", "tue", "wed", "thu","fri","sat","sun"];
	var alphabetList =["A", "B", "C", "D", "E","F","G"];
	for(var idx=0; idx<7; idx++){
		if( dayList[idx] == day){
			return alphabetList[idx];
		}
	}
}
function indexToValue(index){
	var value = "";
	if(index%2 == 0){
		if(index/2 <10){
			value = "0"+ index/2+"00";
		}
		else{
			value = index/2+"00";
		}
	}
	else{
		if((index-1)/2 <10){
			value = "0"+ (index-1)/2 +"30";
		}
		else{
			value = (index-1)/2 +"30";
		}
	}
	return value;
}