tellpinApp.directive( 'chosen', function() {
	var linker = function( scope, element, attr ) {
		scope.$watch( 'profile.native', function() {
			element.trigger( 'chosen:updated' );
		});
		element.chosen( { max_selected_options : 3 } );
	};
	
	return {
		restrict: 'A',
		link: linker
	}
});

tellpinApp.controller("editController", function($scope, $http, $location, $window, $sce, $timeout){

	$scope.strings = {};
	$scope.strings.labels = {
		"short_intro": "Short introduction",
		"resume": "Resume (optional)",
		"long_intro": "Long introduction",
		"teaching_special_tags": "Teaching Specialty Tags",
		"back": "Back",
		"continue": "Continue",
		"ex_video_intro_link": "Example http://www.youtube.com/v/235dz8DF?rel=0&modestbranding=1&autohide=1&showinfo=0&controls=0",
		"modal_work_desc": "Ex) I taught adult English language learners with a focus on intermediate level students.\n\n I mainly translated legal and corporate documents.",
		"modal_education_desc": "Ex) Graduated Magna Cum Laude\n Concentration in writing \nOverall GPA 3.8/4.0",
		"modal_certificate_name": "Ex) TESOL, TOEFL",
		"modal_certificate_organization": "Ex) TESL Canada, ETS",
		"modal_certificate_desc": "Ex) iBT Score 117/120",
	};

	$scope.teachable = [];

	$scope.initProfileData = function(data){
		$scope.nameInfo = false;
		$scope.setData();
	};
	
	$scope.setData = function() {
		$scope.loading = true;
		$http.post( "/user/ajax_editprofile/").success( function( result ) {
			var data = result;
			console.log(result);
			$scope.editMenu = 0;
			$scope.fromCity = [];
			$scope.livinginCity = [];
			$scope.profile = data.profile;
			
			$scope.tools = data.tools;
			$scope.currencyInfo = data.currencyInfo;
			for(var i = 1; i < $scope.currencyInfo.length; i++) {
				$scope.currencyInfo[i].code = $scope.currencyInfo[i].code + " " + $scope.currencyInfo[i].symbol; 
			}

			$scope.nationsInfo = data.nationsInfo;
			$scope.languageInfo = data.languageInfo;
			$scope.skillLevelInfo = data.skillLevelInfo;
			$scope.timezoneInfo = data.timezoneInfo;
			$scope.setCurrentTime();

			if($scope.profile.from_country_id == null){
				$scope.profile.from_country_id = 0;
				$scope.profile.from_city_id = 0;
			} else {
				$scope.getCityInfo($scope.profile.from_country_id, 'from');
			}
			if($scope.profile.livingin_country_id == null){
				$scope.profile.livingin_country_id = 0;
				$scope.profile.livingin_city_id = 0;
			} else {
				$scope.getCityInfo($scope.profile.livingin_country_id, "livingin");
			}
			if ($scope.profile.short_intro == null) {
				$scope.profile.short_intro = "";
			}
			if ($scope.profile.long_intro == null) {
				$scope.profile.long_intro = "";
			}
			// birthdate options
			$scope.yearList = [];
			for(var year = $scope.profile.server_year; year >= 1917; year--){
				$scope.yearList.push(year);
			}
			$scope.monthList = [];
			for(var month = 1; month <= 12; month++){
				$scope.monthList.push(month);
			}

			$scope.setDateList();
			
			// gender options
			$scope.genderList = ['Select', 'Male', 'Female'];
			if ($scope.profile.gender == null) {
				$scope.profile.gender = 0;
			}
			//language options
			$scope.languageList = ['English', '한국어', '日本語'];
			if ($scope.profile.display_language == null) {
				$scope.profile.display_language = 0;	
			}

			$scope.nationsInfo.splice(0, 0, {"id": 0, "country": "Select Country"});
			$scope.fromCity.splice(0, 0, {"id": 0,"city":"Select City"});
			$scope.livinginCity.splice(0, 0, {"id": 0,"city":"Select City"});

			//currenty
			$scope.currencyInfo.splice(0, 0, {"id": 0,"code":"Select currency"});
			$scope.languageInfo.splice(0, 0, {"id": 0,"lang_english":"Select language"});
			$scope.skillLevelInfo.splice(0,0, {"id": 0,"level":"Select Level"});
			$scope.skillLevelInfo.splice(7, 1);
			
			// language setting
			if($scope.profile.native1_id == null || $scope.profile.native1_id == 0){
				$scope.profile.native1_id = 0;
			}

			if($scope.profile.lang1_id == null || $scope.profile.lang1_id == 0){
				$scope.profile.lang1_id = 0;
				$scope.profile.lang1_level = 0;
			} else {
				if ($scope.profile.lang1_level == null) {
					$scope.profile.lang1_level = 0;
				}
			}

			$scope.profile.native = [];
			$scope.profile.native.push( $scope.profile.native1_id );
			if ( $scope.profile.native2_id != null ) {
				$scope.profile.native.push( $scope.profile.native2_id );
			}
			if ( $scope.profile.native3_id != null ) {
				$scope.profile.native.push( $scope.profile.native3_id );
			}

			if ($scope.profile.currency_id == null) {
				$scope.profile.currency_id = 0;
			}
			$scope.checkPersonalTools();
			// teaching setting
			$scope.addTeachingLanguage();

			//resume
			if( $scope.profile.profile_info != undefined && $scope.profile.profile_info != null) { //저장된 내용이 있다
				$scope.formdata = [];
				$scope.formdata.short_intro = $scope.profile.profile_info.short_intro;
				$scope.formdata.education = $scope.profile.profile_info.education;
				$scope.formdata.work = $scope.profile.profile_info.work;
				$scope.formdata.certification = $scope.profile.profile_info.certificate;
				$scope.formdata.long_intro = $scope.profile.profile_info.long_intro;
				$scope.formdata.video_intro = $scope.profile.profile_info.video_intro;
				$scope.formdata.resumeFile = new FormData();
				
				for(var i=1; i<=3; i++) {
					if( $scope.profile.profile_info["work"+i] != undefined && $scope.profile.profile_info["work"+i] != null ) {
						$scope.formdata["work"+i] = {};
						$scope.formdata["work"+i].info = true;
						$scope.formdata["work"+i].start_year = {val:$scope.profile.profile_info["work"+i].start_year, name: $scope.profile.profile_info["work"+i].start_year};
						$scope.formdata["work"+i].end_year = { val : $scope.profile.profile_info["work"+i].end_year, name: $scope.profile.profile_info["work"+i].end_year};
						$scope.formdata["work"+i].company = { val : $scope.profile.profile_info["work"+i].company, valid: false};
						$scope.formdata["work"+i].country = Number($scope.profile.profile_info["work"+i].country);
						$scope.formdata["work"+i].country_name = $scope.nationsInfo[$scope.profile.profile_info["work"+i].country].country;
						$scope.formdata["work"+i].city = { val : $scope.profile.profile_info["work"+i].city, valid: false};
						$scope.formdata["work"+i].position = { val : $scope.profile.profile_info["work"+i].position, valid: false};
						$scope.formdata["work"+i].description = { val : $scope.profile.profile_info["work"+i].description, valid: false};
						$scope.formdata["work"+i].work_exp_file = $scope.profile.profile_info["work" + i + "_filename"];
						$scope.formdata["work"+i].work_exp_file_path = $scope.profile.profile_info["work" + i + "_filepath"];
						$scope.formdata["work"+i].file_del_yn = false;
						
					}
					else {
						$scope.formdata["work"+i] = {};
						$scope.formdata["work"+i].info = false;
						$scope.formdata["work"+i].start_year = 0;
						$scope.formdata["work"+i].end_year = 0;
						$scope.formdata["work"+i].company = "";
						$scope.formdata["work"+i].country = 0;
						$scope.formdata["work"+i].country_name = "";
						$scope.formdata["work"+i].city = "";
						$scope.formdata["work"+i].position = "";
						$scope.formdata["work"+i].description = "";
						$scope.formdata["work"+i].work_exp_file = null;
						$scope.formdata["work"+i].work_exp_file_path = null;
						$scope.formdata["work"+i].file_del_yn = false;
					}
				}
				
				
				for(var i=1; i<=3; i++) {
					if( $scope.profile.profile_info["certificate"+i] != undefined && $scope.profile.profile_info["certificate"+i] != null ) {
						$scope.formdata["certificate"+i] = {};
						$scope.formdata["certificate"+i].info = true;
						$scope.formdata["certificate"+i].year = {val:$scope.profile.profile_info["certificate"+i].year, name: $scope.profile.profile_info["certificate"+i].year};
						$scope.formdata["certificate"+i].name = { val : $scope.profile.profile_info["certificate"+i].name, valid: false};
						$scope.formdata["certificate"+i].organization = { val : $scope.profile.profile_info["certificate"+i].organization, valid: false};
						$scope.formdata["certificate"+i].description = { val : $scope.profile.profile_info["certificate"+i].description, valid: false};
						$scope.formdata["certificate"+i].certification_file = $scope.profile.profile_info["certificate" + i + "_filename"];
						$scope.formdata["certificate"+i].certification_file_path = $scope.profile.profile_info["certificate" + i + "_filepath"];
						$scope.formdata["certificate"+i].file_del_yn = false;
					}
					else {
						$scope.formdata["certificate"+i] = {};
						$scope.formdata["certificate"+i].info = false;
						$scope.formdata["certificate"+i].year = 0;
						$scope.formdata["certificate"+i].name = "";
						$scope.formdata["certificate"+i].organization = "";
						$scope.formdata["certificate"+i].description = "";
						$scope.formdata["certificate"+i].certification_file = null;
						$scope.formdata["certificate"+i].certification_file_path = null;
						$scope.formdata["certificate"+i].file_del_yn = false;
					}
				}
				
				for(var i=1; i<=3; i++) {
					if( $scope.profile.profile_info["education"+i] != undefined && $scope.profile.profile_info["education"+i] != null ) {
						$scope.formdata["education"+i] = {};
						$scope.formdata["education"+i].info = true;
						$scope.formdata["education"+i].start_year = {val:$scope.profile.profile_info["education"+i].start_year, name: $scope.profile.profile_info["education"+i].start_year};
						$scope.formdata["education"+i].end_year = { val : $scope.profile.profile_info["education"+i].end_year, name: $scope.profile.profile_info["education"+i].end_year};
						$scope.formdata["education"+i].school = { val : $scope.profile.profile_info["education"+i].school, valid: false};
						$scope.formdata["education"+i].location = Number($scope.profile.profile_info["education"+i].location);
						$scope.formdata["education"+i].location_name = $scope.nationsInfo[$scope.profile.profile_info["education"+i].location].country;
						$scope.formdata["education"+i].degree = $scope.profile.profile_info["education"+i].degree;
						$scope.formdata["education"+i].major = { val : $scope.profile.profile_info["education"+i].major, valid: false};
						$scope.formdata["education"+i].description = { val : $scope.profile.profile_info["education"+i].description, valid: false};;
						$scope.formdata["education"+i].education_file = $scope.profile.profile_info["education" + i + "_filename"];
						$scope.formdata["education"+i].education_file_path = $scope.profile.profile_info["education" + i + "_filepath"];
						$scope.formdata["education"+i].file_del_yn = false;
						
					}
					else {
						$scope.formdata["education"+i] = {};
						$scope.formdata["education"+i].info = false;
						$scope.formdata["education"+i].start_year = 0;
						$scope.formdata["education"+i].end_year = 0;
						$scope.formdata["education"+i].school = "";
						$scope.formdata["education"+i].location = 0;
						$scope.formdata["education"+i].location_name = "";
						$scope.formdata["education"+i].degree = "";
						$scope.formdata["education"+i].major = "";
						$scope.formdata["education"+i].description = "";
						$scope.formdata["education"+i].education_file = null;
						$scope.formdata["education"+i].education_file_path = null;
						$scope.formdata["education"+i].file_del_yn = false;
					}
				}
				
				
			} else { //저장된 내용이 없다.
				$scope.formdata = [];
				$scope.formdata.work_exp = "";
				$scope.formdata.education = "";
				$scope.formdata.certification = "";
				$scope.formdata.resumeFile = new FormData();
				
				//work
				for (var i=1; i<=3; i++) {
					$scope.formdata["work"+i] = {};
					$scope.formdata["work"+i].info = false;
					$scope.formdata["work"+i].start_year = 0;
					$scope.formdata["work"+i].end_year = 0;
					$scope.formdata["work"+i].company = "";
					$scope.formdata["work"+i].country = 0;
					$scope.formdata["work"+i].country_name = "";
					$scope.formdata["work"+i].city = "";
					$scope.formdata["work"+i].position = "";
					$scope.formdata["work"+i].description = "";
					$scope.formdata["work"+i].work_exp_file = null;
					$scope.formdata["work"+i].file_del_yn = false;
				}

				//certification
				for (var i=1; i<=3; i++) {
					$scope.formdata["certificate"+i] = {};
					$scope.formdata["certificate"+i].info = false;
					$scope.formdata["certificate"+i].year = 0;
					$scope.formdata["certificate"+i].name = "";
					$scope.formdata["certificate"+i].organization = "";
					$scope.formdata["certificate"+i].description = "";
					$scope.formdata["certificate"+i].certification_file = null;
					$scope.formdata["certificate"+i].file_del_yn = false;
				}

				//education
				for (var i=1; i<=3; i++) {
					$scope.formdata["education"+i] = {};
					$scope.formdata["education"+i].info = false;
					$scope.formdata["education"+i].start_year = 0;
					$scope.formdata["education"+i].end_year = 0;
					$scope.formdata["education"+i].school = "";
					$scope.formdata["education"+i].location = 0;
					$scope.formdata["education"+i].location_name = "";
					$scope.formdata["education"+i].degree = "";
					$scope.formdata["education"+i].major = "";
					$scope.formdata["education"+i].description = "";
					$scope.formdata["education"+i].education_file = null;
					$scope.formdata["education"+i].file_del_yn = false;
				}
			}

			$scope.start_year = 0;
			$scope.modalYearList = [];
			var d = new Date();
			$scope.thisYear = d.getFullYear()
			for($scope.start_year = $scope.thisYear; $scope.start_year >= d.getFullYear() - 60; $scope.start_year--) {
				$scope.modalYearList.push({val :$scope.start_year, name: $scope.start_year});
			}
			
			$scope.endYearList = [];
			$scope.endYearList.push({val :-1, name: "to now"});

			$scope.degreeList = ["Bachelor’s", "Master’s", "Doctorate", "Postdoctoral", "Other"];

			$scope.workDescPH = $scope.strings.labels.modal_work_desc;
			$scope.eduDescPH = $scope.strings.labels.modal_education_desc;
			$scope.certiNamePH = $scope.strings.labels.modal_certificate_name;
			$scope.certiOrganizationPH = $scope.strings.labels.modal_certificate_organization;
			$scope.certiDescPH = $scope.strings.labels.modal_certificate_desc;
			
			//upload file temp  
			$scope.tempFile = null;

			console.log($scope.formdata);
			$scope.loading = false;
		}).error(function(result){
			console.log( 'erorr');
		});
	}

	$scope.getCityInfo = function(country_id, field) {
		console.log("getCityInfo");
		$http.post("/common/city/", {country_id : country_id}).success( function( result ) {
			if(result.isSuccess == 1){
				if (field == "from") {
					$scope.fromCity = result.city;
					$scope.fromCity.splice(0, 0, {"id": 0,"city":"Select City"});
					$scope.showNationImage();
				} else if (field == "livingin") {
					$scope.livinginCity = result.city;
					$scope.livinginCity.splice(0, 0, {"id": 0,"city":"Select City"});
				} else {
					$scope.fromCity = result.city;
					$scope.fromCity.splice(0, 0, {"id": 0,"city":"Select City"});
					$scope.showNationImage();
				}
			}
		});
	}

	$scope.showNationImage = function(){
		$(".nations-image").css("display","block");
		if($scope.profile.from_field == 0){
			$scope.profile.from_city = 0;
		}
	}

	$scope.clickNameInfoPopup = function() {
		if ($scope.nameInfo == false) {
			$scope.nameInfo = true
		} else {
			$scope.nameInfo = false;
		}

		if( $scope.profile.level != 3 ){
			$(".name-info-popup").css("top", "260px");
		}
	}

	$scope.getOldTools = function($index){
		$scope.oldMessenger = $scope.profile.tools[$index].name;
	}

	$scope.checkToolsValidation = function($index){
		for(var idx=0; idx < $scope.profile.tools.length; idx ++){
			if($index != idx){
				if($scope.profile.tools[$index].name == $scope.profile.tools[idx].name){
					alert("Messenger duplicated");
					$scope.profile.tools[$index].name = $scope.oldMessenger;
				}
			}
		}
	}

	$scope.addTools = function(){
		var toolsObj = new Object();
		toolsObj.name = "Select tools";
		toolsObj.account = "";
		$scope.profile.tools.push(toolsObj);
	}

	$scope.deleteTools = function($index){
		$scope.profile.tools.splice($index,1);
	}

	$scope.checkPersonalTools = function(){
		console.log($scope.profile.tools.length);
		$scope.profile.tools = [];
		if($scope.profile.skype_id != null){
			var toolsObj = new Object();
			toolsObj.name = "Skype";
			toolsObj.account = $scope.profile.skype_id;
			$scope.profile.tools.push(toolsObj);
		}
		if($scope.profile.hangout_id != null){
			var toolsObj = new Object();
			toolsObj.name = "Hangout";
			toolsObj.account = $scope.profile.hangout_id;
			$scope.profile.tools.push(toolsObj);
		}
		if($scope.profile.facetime_id != null){
			var toolsObj = new Object();
			toolsObj.name = "Facetime";
			toolsObj.account = $scope.profile.facetime_id;
			$scope.profile.tools.push(toolsObj);
		}
		if($scope.profile.qq_id != null){
			var toolsObj = new Object();
			toolsObj.name = "QQ";
			toolsObj.account = $scope.profile.qq_id;
			$scope.profile.tools.push(toolsObj);
		}
		if($scope.profile.tools.length == 0) {
			var toolsObj = new Object();
			toolsObj.name = "Select tools";
			toolsObj.account = "";
			$scope.profile.tools.push(toolsObj);
		}
	}

	$scope.setDefaultAudio = function(){
		if($scope.profile.audio == null){
			$scope.profile.audio = 0;
		}
	}

	$scope.addNativeLanguage = function(nth){
		var fieldName = "native"+nth + "_id";
		var temp = nth - 1;
		var fieldName2 = "native" + temp;
		fieldName2 = fieldName2 + "_id"; 
		if ($scope.profile[fieldName2] == 0 || $scope.profile[fieldName2] == null) {
			alert("select Language");
			return;
		}
		$scope.profile[fieldName] = 0;
	}

	$scope.deleteNativeLanguage = function(nth){
		var fieldName = "native"+nth + "_id";
		if(nth == "2"){
			var nextFiledName = "native"+(nth + 1);
			if($scope.profile[nextFiledName] != null){
				$scope.profile[fieldName] = $scope.profile[nextFiledName];
				$scope.profile[nextFiledName] = null;
			}
			else{
				$scope.profile[fieldName] = null;
			}
		}
		else{
			$scope.profile[fieldName] = null;
		}
		$scope.addTeachingLanguage();
	}

	$scope.addLearningLanguage = function(nth){
		var fieldName = "lang"+nth + "_id";
		var temp = nth - 1;
		var fieldName2 = "lang" + temp + "_id";
		var level = "lang" + nth + "_level";
		var skill_level_2 = "lang" + temp + "_level";
		if ($scope.profile[fieldName2] == 0 || $scope.profile[fieldName2] == null) {
			alert("select Language");
			return;
		} else if ($scope.profile[skill_level_2] == 0 || $scope.profile[skill_level_2] == null) {
			alert("select level");
			return;
		}
		$scope.profile[fieldName] = 0;
		$scope.profile[level] = 0;
	}

	$scope.deleteLanguage = function(nth){
		var fieldName = "lang" + nth + "_id";
		var level = "lang" + nth +"_level";
		if (nth == "6") {
			$scope.profile[fieldName] = null;
			$scope.profile[nextLevel] = null;
		} else {
			for (var i = 6; i > nth; i-- ) {
				var nextFiledName = "lang"+ i + "_id";
				var nextLevel = "lang" + i + "_level";
				console.log(nextFiledName);
				$scope.profile[fieldName] = $scope.profile[nextFiledName];
				$scope.profile[nextFiledName] = null;
				$scope.profile[level] = $scope.profile[nextLevel];
				$scope.profile[nextLevel] = null;
				
			}
		}
		$scope.addTeachingLanguage();
		console.log($scope.profile);
	}

	$scope.checkLangValid = function() {
		console.log($scope.profile);
		return;
		if($scope.profile.native1_id == "" || $scope.profile.native1_id == null){
			alert("native1");
			return false;
		} else {
			$scope.profile.native1 =  $scope.languageInfo[$scope.profile.native1_id].lang_english;
		}
		if($scope.profile.native2_id != null) {
			$scope.profile.native2 =  $scope.languageInfo[$scope.profile.native2_id].lang_english;
		}
		if($scope.profile.native3_id != null) {
			$scope.profile.native3 =  $scope.languageInfo[$scope.profile.native3_id].lang_english;
		}
		if($scope.profile.lang1_id !=  null){
			$scope.profile.lang1 =  $scope.languageInfo[$scope.profile.lang1_id].lang_english;
			if($scope.profile.lang1_level == null || $scope.profile.lang1_level == 0){
				alert("lang1_id_skill_level");
				return false;
			}
		}
		if($scope.profile.lang2_id !=  null){
			$scope.profile.lang2 =  $scope.languageInfo[$scope.profile.lang2_id].lang_english;
			if($scope.profile.lang2_level == null || $scope.profile.lang2_level == 0){
				alert("lang2_id_skill_level");
				return false;
			}
		}
		if($scope.profile.lang3_id !=  null){
			$scope.profile.lang3 =  $scope.languageInfo[$scope.profile.lang3_id].lang_english;
			if($scope.profile.lang3_level == null || $scope.profile.lang3_level == 0){
				alert("lang3_id_skill_level");
				return false;
			}
		}
		if($scope.profile.lang4_id !=  null){
			$scope.profile.lang4 =  $scope.languageInfo[$scope.profile.lang4_id].lang_english;
			if($scope.profile.lang4_level == null){
				alert("lang4_id_skill_level");
				return false;
			}
		}
		if($scope.profile.lang5_id !=  null){
			$scope.profile.lang5 =  $scope.languageInfo[$scope.profile.lang5_id].lang_english;
			if($scope.profile.lang5_level == null){
				alert("lang5_id_skill_level");
				return false;
			}
		}
		if($scope.profile.lang6_id !=  null){
			$scope.profile.lang6 =  $scope.languageInfo[$scope.profile.lang6_id].lang_english;
			if($scope.profile.lang6_level == null){
				alert("lang6_id_skill_level");
				return false;
			}
		}
		if($scope.profile.teaching1_id !=  null){
			$scope.profile.teaching1 =  $scope.languageInfo[$scope.profile.teaching1_id].lang_english;
		} else{
			alert("Please select at least one language");
			return false;
		}
		if($scope.profile.teaching2_id !=  null) {
			$scope.profile.teaching2 =  $scope.languageInfo[$scope.profile.teaching2_id].lang_english;
		}
		if($scope.profile.teaching3_id !=  null) {
			$scope.profile.teaching3 =  $scope.languageInfo[$scope.profile.teaching3_id].lang_english;
		}
	}

	$scope.getOldLanguage = function(field){
		$scope.oldValue = $scope.profile[field];
	}

 	$scope.changeLanguge = function(field){
		if($scope.checkLanguageValidation(field)){
			if($scope.profile.teaching1_id == $scope.oldValue){
				$scope.profile.teaching1_id = null;
			}
			else if($scope.profile.teaching2_id == $scope.oldValue){
				$scope.profile.teaching2_id = null;
			}
			else if($scope.profile.teaching3_id == $scope.oldValue){
				$scope.profile.teaching3_id = null;
			}
			if(field == "native1_id" || field == "native2_id" || field == "native3_id") {
				$scope.addTeachingLanguage();
			}
		}
	}

	$scope.checkLanguageValidation = function(field){
		var fieldName = field;
		var res = true;
		console.log(field);
		if($scope.profile.native1_id == $scope.profile[fieldName]){
			if(fieldName != "native1_id"){
				res = false;
			}
		}
		if($scope.profile.native2_id == $scope.profile[fieldName]){
			if(fieldName != "native2_id"){
				res = false;
			}
		}
		if($scope.profile.native3_id == $scope.profile[fieldName]){
			if(fieldName != "native3_id"){
				res = false;
			}
		}
		if($scope.profile.lang1_id == $scope.profile[fieldName]){
			if(fieldName != "lang1_id"){
				res = false;
			}
		}
		if($scope.profile.lang2_id == $scope.profile[fieldName]){
			if(fieldName != "lang2_id"){
				res = false;
			}
		}
		if($scope.profile.lang3_id == $scope.profile[fieldName]){
			if(fieldName != "lang3_id"){
				res = false;
			}
		}
		if($scope.profile.lang4_id == $scope.profile[fieldName]){
			if(fieldName != "lang4_id"){
				res = false;
			}
		}
		if($scope.profile.lang5_id == $scope.profile[fieldName]){
			if(fieldName != "lang5_id"){
				res = false;
			}
		}
		if($scope.profile.lang6_id == $scope.profile[fieldName]){
			if(fieldName != "lang6_id"){
				res = false;
			}
		}
		if(!res){
			$scope.profile[fieldName] = 0;
			var temp = fieldName.split('_');
			$scope.profile[temp[0] + "_level"] = 0;
			alert("Language duplicated");
		}
		return res;
	}

	$scope.addTeachingLanguage = function(){
		$scope.teachable = [];
//		console.log($scope.teachable);

		for(var nth=1; nth <= 3; nth++){
			var fieldName = "native"+ nth + "_id";
			if($scope.profile[fieldName] != null && $scope.profile[fieldName] != 0 && $scope.profile[fieldName] != -1){
				var obj = new Object();
				obj.id = $scope.profile[fieldName];
				if (obj.id != 0) {
					for(var index in $scope.languageInfo){
						if($scope.languageInfo[index].id == obj.id){
							obj.lang_english = $scope.languageInfo[index].lang_english;
						}
					}
//					obj.level = 7;
					if($scope.profile.teaching1_id == obj.id ||  $scope.profile.teaching2_id == obj.id|| $scope.profile.teaching3_id == obj.id){
						obj.state = 1
					}
					else{
						obj.state = 0
					}
					$scope.teachable.push(obj);					
				}
			}
		}
		for(var nth=1; nth <= 6; nth++){
			var fieldName = "lang" + nth + "_id";
			var level = "lang"+nth+"_level";
			if($scope.profile[fieldName] != null && $scope.profile[level] > 5){
				var obj = new Object();
				obj.id = $scope.profile[fieldName];
				for(var index in $scope.languageInfo){
					if($scope.languageInfo[index].id == obj.id){
						obj.lang_english = $scope.languageInfo[index].lang_english;
					}
				}
//				obj.level =  $scope.profile[level];
				if($scope.profile.teaching1_id == obj.id ||  $scope.profile.teaching2_id == obj.id|| $scope.profile.teaching3_id == obj.id){
					obj.state = 1
				}
				else{
					obj.state = 0
				}
				$scope.teachable.push(obj);
			}
		}
	}

	$scope.selectTeachingLanguage = function($index){
		if($scope.teachable[$index].state == 1){
			$scope.teachable[$index].state = 0;
			if($scope.profile.teaching1_id == $scope.teachable[$index].id){
				$scope.profile.teaching1_id = null;
			}
			else if($scope.profile.teaching2_id == $scope.teachable[$index].id){
				$scope.profile.teaching2_id = null;
			}
			else if($scope.profile.teaching3_id == $scope.teachable[$index].id){
				$scope.profile.teaching3_id = null;
			}
		}
		else{
			if($scope.profile.teaching1_id == null){
				$scope.profile.teaching1_id = $scope.teachable[$index].id;
				$scope.teachable[$index].state = 1;
			}
			else if($scope.profile.teaching2_id == null){
				$scope.profile.teaching2_id = $scope.teachable[$index].id;
				$scope.teachable[$index].state = 1;
			}
			else if($scope.profile.teaching3_id == null){
				$scope.profile.teaching3_id = $scope.teachable[$index].id;
				$scope.teachable[$index].state = 1;
			}
			else{
				alert("Exceeded 3 language");
			}	
		}

		var regi = $scope.profile;
		var teach = $scope.teachable;
		console.log($scope.teachable);
		var arr = [];
		for(var i=0; i<teach.length; i++){
			if( teach[i].state == 1 ){
				arr.push(i);
			}
		}

		regi.teaching1_id = null;
		regi.teaching2_id = null;
		regi.teaching3_id = null;

		if( arr.length > 0){
			regi.teaching1_id = teach[arr[0]].id;
			regi.teaching1_id.state = 1;
		}
		if( arr.length > 1 ){
			regi.teaching2_id = teach[arr[1]].id;
			regi.teaching2_id.state = 1;
		}
		if( arr.length > 2 ){
			regi.teaching3_id = teach[arr[2]].id;
			regi.teaching3_id.state = 1;
		}
		console.log(regi);
	}

	$scope.setCurrentTime = function(){ 
		var now = new Date();
		$scope.currentUTCTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
		$scope.changeTimezone();
		$timeout($scope.setCurrentTime, 1000);
	};
	
	$scope.changeTimezone = function(){
		if($scope.profile.timezone_id == null){
			var utc_time = minutesToUTC(-(new Date().getTimezoneOffset()));
			for(var idx = 0; idx < $scope.timezoneInfo.length; idx++){
				if(utc_time == $scope.timezoneInfo[idx].utc_time){
					$scope.profile.timezone_id = $scope.timezoneInfo[idx].id;
					break;
				}
			}
//			var displayTime = new Date().format("yyyy.MM.dd hh:mm a/p (UTC");
//			$scope.currentTime = displayTime + minutesToUTC(-(new Date().getTimezoneOffset())) + ")";
		}
//		else{
		var displayTime = new Date($scope.currentUTCTime.getTime() + utcToMilliseconds($scope.timezoneInfo[$scope.profile.timezone_id-1].utc_time));
		$scope.currentTime = displayTime.format("yyyy.MM.dd hh:mm a/p (UTC") + $scope.timezoneInfo[$scope.profile.timezone_id-1].utc_time + ")";
//		}
	}

	$scope.setDefaultFromCity = function(){
		$scope.nationsInfo[$scope.profile.from_field].city_list.splice(0, 1);
		$scope.nationsInfo[$scope.profile.from_field].city_list.splice(0, 0, {"id": 0,"name":"City"});
		$scope.profile.from_city = 0;
	}

	$scope.setDefaultLiveinCity = function(){
		$scope.nationsInfo[$scope.profile.livein].city_list.splice(0, 1);
		$scope.nationsInfo[$scope.profile.livein].city_list.splice(0, 0, {"id": 0,"name":"City"});
		$scope.profile.livein_city = 0;
	}

	$scope.setDateList = function(){
		$scope.dateList = [];
		if($scope.profile.birthdate.month == 02){
			if($scope.profile.birthdate.year == "" || $scope.profile.birthdate.year == null){
				for(var date = 1; date <= 29; date++){
					$scope.dateList.push(date);
				}
			}
			else{
				$http.post("/common/get_max_date/",{
					year: $scope.profile.birthdate.year,
			        month: $scope.profile.birthdate.month}).success(function(response){
			        	for(var date = 1; date <= response.lastDate; date++){
							$scope.dateList.push(date);
						}
				});
			}
		}
		else if($scope.profile.birthdate.month == "" || $scope.profile.birthdate.month == null || $scope.profile.birthdate.month == 1 || $scope.profile.birthdate.month == 3 ||
				$scope.profile.birthdate.month == 5 || $scope.profile.birthdate.month == 7 || $scope.profile.birthdate.month == 8 ||
				$scope.profile.birthdate.month == 10 || $scope.profile.birthdate.month == 12){
			for(var date = 1; date <= 31; date++){
				$scope.dateList.push(date);
			}
		}
		else{
			for(var date = 1; date <= 30; date++){
				$scope.dateList.push(date);
			}
		}
	};

	$scope.clickChangePhoto = function(){
		$("#profile-photo").click();
	};

	$scope.submitProfilePhoto = function(files){
		var fileName = new FormData();
		fileName.append("profile",files[0]);
		$http.post("/tutor/apply/upload_file/", fileName,{
			withCredentials: true,
	        headers: {'Content-Type': undefined },
	        transformRequest: angular.identity
		}).success(function(response){
			console.log(response);
			$scope.profile.photo_filepath = response.photo_filepath;
			$scope.profile.photo_filename = response.photo_filename;
		});
	};

	$scope.getWriteFile = function(files){
		$scope.tempFile = files[0];
	}

	$(window).scroll(function(){
	    $.cookie("scroll", $(document).scrollTop() );
	});

	// 저장된 쿠키값 가져와서 스크롤위치 이동
	$(document).ready(function(){
	    if ( $.cookie("scroll") !== null) {
	         $(document).scrollTop( $.cookie("scroll") );
	    }
	});

	$scope.checkAll = function(){
		console.log("checkAll");
		var result = true;
		var name_regex = /^[가-힣a-zA-Z0-9\s]{3,50}$/;
		var nameObj = $("#name");

		if ($scope.profile.photo_filename == "" || $scope.profile.photo_filename == null) {
			alert("Photo");
			return false;
		}
		if (!(name_regex.test(nameObj[0].value))) {
//			$scope.nameInfoPopup();
			$scope.showTooltip("profile-name-input", "profile-name-tooltip", "wrong name   !!!");
			$scope.scrollTop = $(".profile-name-input").offset().top;
			result = false;
		}

		if($scope.profile.name == "" ){
			$scope.showTooltip("profile-name-input", "profile-name-tooltip", "This cannot be empty.");
			$scope.scrollTop = $(".profile-name-input").offset().top;
			result = false;
		}
		
//		if( !$scope.nameCheck( $scope.profile.name ) ) {
//			$scope.showTooltip("profile-name-input", "profile-name-tooltip", "This cannot involved special characher.");
//			$scope.scrollTop = $(".profile-name-input").offset().top;
//			result = false;
//		}

		if($scope.profile.from_country_id == null || $scope.profile.from_city_id== ""){
			alert("from");
			return false;
		}
		if($scope.profile.livingin_country_id == "" || $scope.profile.livingin_city_id == null){
			alert("livein");
			return false;
		}
		if($scope.profile.gender == "" || $scope.profile.gender== null){
			alert("gender");
			return false;
		}
		if($scope.profile.currency_id == null || $scope.profile.currency_id == 0){
			alert("currency");
			return false;
		}
		if($scope.profile.birthdate.year != null){
			if($scope.profile.birthdate.month == null || $scope.profile.birthdate.day == null){
				if($scope.profile.birthdate.month == null){
					if($scope.scrollTop == -1){
						$scope.scrollTop = $(".profile-year-select").offset().top;
					}
					$scope.showTooltip("profile-month-select", "profile-month-tooltip", "This cannot be empty.");
					return false;
				}
				if($scope.profile.birthdate.day == null){
					if($scope.scrollTop == -1){
						$scope.scrollTop = $(".profile-year-select").offset().top;
					}
					$scope.showTooltip("profile-day-select", "profile-day-tooltip", "This cannot be empty.");
					result = false;
				}
			}
		} else {
			$scope.showTooltip("profile-year-select", "profile-year-tooltip", "This cannot be empty.");
			return false;
		}
		if($scope.profile.birthdate.month != null){
			if($scope.profile.birthdate.year == null){
				if($scope.scrollTop == -1){
					$scope.scrollTop = $(".profile-year-select").offset().top;
				}
				$scope.showTooltip("profile-year-select", "profile-year-tooltip", "This cannot be empty.");
				result = false;
			}
			if($scope.profile.birthdate.day == null){
				if($scope.scrollTop == -1){
					$scope.scrollTop = $(".profile-year-select").offset().top;
				}
				$scope.showTooltip("profile-day-select", "profile-day-tooltip", "This cannot be empty.");
				result = false;
			}
		} else {
			$scope.showTooltip("profile-month-select", "profile-month-tooltip", "This cannot be empty.");
			return false;
		}
		if($scope.profile.birthdate.day != null){
			if($scope.profile.birthdate.year == null){
				if($scope.scrollTop == -1){
					$scope.scrollTop = $(".profile-year-select").offset().top;
				}
				$scope.showTooltip("profile-year-select", "profile-year-tooltip", "This cannot be empty.");
				result = false;
			}
			if($scope.profile.birthdate.month == null){
				if($scope.scrollTop == -1){
					$scope.scrollTop = $(".profile-year-select").offset().top;
				}
				$scope.showTooltip("profile-month-select", "profile-month-tooltip", "This cannot be empty.");
				result = false;
			}
		} else {
			$scope.showTooltip("profile-day-select", "profile-day-tooltip", "This cannot be empty.");
			return false;
		}
		if(result){
			return true;
		}
		else{
			return false;
		}
	}

	$scope.saveProfile = function(){
		console.log("saveProfile");
		if($scope.checkAll()){
			console.log( $scope.profile );
			$http.post("/tutor/saveprofile/",{profile: $scope.profile}).success(function(response){
				if(response.isSuccess == 1){
					$("#my-icon").attr("src",response.photo);
					$("#my-name").text(response.name);
					$window.location.href = ("/settings/editprofile/");
//					alert("Success");
				}
			});
		}
		else{
			//angular.element( '.profile-tooltip' ).css( 'display', 'none' );
			$(document).scrollTop($scope.scrollTop-200);
			return;
		}
	}

	$scope.checkUserIntro = function() {
		console.log("checkUserIntro");
		if ( $scope.profile.user_shortIntro.length < 1 ) {
			$scope.showTooltip("userprofile-short-intro", "profile-short-tooltip", "This field cannot be empty.");
			$scope.scrollTop = $(".userprofile-short-intro").offset().top;
			return false;
		}
		if ( $scope.profile.user_longIntro.length < 1 ) {
			$scope.showTooltip("userprofile-long-intro", "profile-long-tooltip", "This field be empty.");
			$scope.scrollTop = $(".userprofile-long-intro").offset().top;
			return false;
		}
		return true;
	}

	$scope.saveUserIntro = function(){
		console.log("saveUserIntro");
		if($scope.checkUserIntro()){
			console.log( $scope.profile );
			$http.post("/tutor/saveuserintro/",{profile: $scope.profile}).success(function(response){
				if(response.isSuccess == 1){
//					alert("Success");
					$window.location.href = ("/settings/editprofile/");
				}
			});
		}
		else{
			//angular.element( '.profile-tooltip' ).css( 'display', 'none' );
			$(document).scrollTop($scope.scrollTop-200);
			return;
		}
	}

	$scope.checkTutorIntro = function() {
		console.log("checkTutorIntro");
		if ( $scope.profile.short_intro.length < 1 ) {
			$scope.showTooltip("profile-short-intro", "profile-short-tooltip", "This field cannot be empty.");
			$scope.scrollTop = $(".profile-short-intro").offset().top;
			return false;
		}
		if ( $scope.profile.long_intro.length < 1 ) {
			$scope.showTooltip("profile-long-intro", "profile-long-tooltip", "This field be empty.");
			$scope.scrollTop = $(".profile-long-intro").offset().top;
			return false;
		}
		return true;
	}

	$scope.pendingTutorIntro = function(){
		console.log("pendingTutorIntro");
		if($scope.checkTutorIntro()){
			console.log( $scope.profile );
			$http.post("/tutor/savetutorintro/",{profile: $scope.profile}).success(function(response){
				if(response.isSuccess == 1){
//					alert("Success");
					$window.location.href = ("/settings/editprofile/");
				}
			});
		}
		else{
			//angular.element( '.profile-tooltip' ).css( 'display', 'none' );
			$(document).scrollTop($scope.scrollTop-200);
			return;
		}
	}

	$scope.pendingLanguage = function(){
		console.log("pendingLanguage");
		var result =  true;
		$scope.checkLangValid();
		if($scope.profile.native1_id == null || $scope.profile.native1_id == 0){
			alert("Please select at least one Native language");
//			$scope.showTooltip("profile-native1-select", "profile-native-tooltip", "Please select at least one language");
//			$scope.scrollTop = $(".profile-native1-select").offset().top;
			result = false;
		}
		
		
		if($scope.profile.lang1_id == null || $scope.profile.lang1_id == 0){
			alert("Please select at least one Learning language");
//			$scope.showTooltip("profile-learning1-select", "profile-learning-tooltip", "Please select at least one language");
//			$scope.scrollTop = $(".profile-learning1-select").offset().top;
			result = false;
		}

		if(result){
			$http.post("/tutor/saveuserlanguage/",{profile: $scope.profile}).success(function(response){
				if(response.isSuccess == 1){
//					alert("Success");
					$window.location.href = ("/settings/editprofile/");
				}
			});
		}
		else{
			//angular.element( '.profile-tooltip' ).css( 'display', 'none' );
			$(document).scrollTop($scope.scrollTop);
			return;
		}
	}

	$scope.checkTools = function() {
		console.log("checkTools");
		if($scope.profile.online == 1){
			var count = 0;
			for(var idx=0; idx < $scope.profile.tools.length; idx ++){
				if($scope.profile.tools[idx].account != "" && $scope.profile.tools[idx].account != null && $scope.profile.tools[idx].name != -1){
					count++;
				}
				else{
					if($scope.profile.tools[idx].name == -1){
						alert("Select messenger");
						return false;
					}
					else if($scope.profile.tools[idx].account == ""){
						alert("input messenger account");
						return false;
					}
				}
			}
		}
		for(var idx=0; idx < $scope.profile.tools.length; idx ++){
			if($scope.profile.tools[idx].name != -1 && $scope.profile.tools[idx].name != null){
				if($scope.profile.tools[idx].account == null || $scope.profile.tools[idx].account == ""){
					alert("Enter a messenger ID");
					return false;
				}
			}
		}
		return true;
	}

	$scope.saveTools = function(){
		console.log("saveTools");
		if($scope.checkTools()){
			$http.post("/tutor/savetools/",{profile: $scope.profile}).success(function(response){
				if(response.isSuccess == 1){
//					alert("Success");
					$window.location.href = ("/settings/editprofile/");
				}
			});
		}else{
			//angular.element( '.profile-tooltip' ).css( 'display', 'none' );
			$(document).scrollTop($scope.scrollTop-200);
			return;
		}
	}

	$scope.showData = function(){
		console.log($scope.profile);
	}
	$scope.showTooltip = function(element, tooltip, message){
		var top = $("."+element).offset().top + $("."+element).outerHeight() + 3 + "px";
		var left = $("."+element).offset().left + "px";
		$("#"+tooltip).text(message);
		$("#"+tooltip).css({"top": top, "left": left});
		$("#"+tooltip).css("display","block");
	}
	$scope.hideTooltip = function(tooltip){
		$("#"+tooltip).css("display","none");
	}
//---------------
//	resume data
//---------------
	$scope.modifyEndYearList = function ( field ) {
		$scope.endYearList = [];
		$scope.start_year = field.start_year.val;
		for (var endYear = field.end_year.val; endYear >= $scope.start_year; endYear--){
			$scope.endYearList.push({val: endYear, name: endYear});
		}
		$scope.endYearList.splice(0, 0, {val: -1, name: "to now"});
//		$scope.endYearList.push({val :field.end_year.val, name: field.end_year.name});
	}
	$scope.setEndYearList = function( field ){
		$scope.endYearList = [];
		$scope.start_year = field.start_year.val;
		for (var endYear = $scope.thisYear; endYear >= $scope.start_year; endYear--){
			$scope.endYearList.push({val: endYear, name: endYear});
		}
		$scope.endYearList.splice(0, 0, {val: -1, name: "to now"});
		field.end_year = $scope.endYearList[0];
	}
	
	$scope.countingText = function(elId, obj, start, end){
		console.log(obj.val.length);
		if((0 < obj.val.length && obj.val.length <= start) || obj.val.length > end) {
			$("#"+elId).addClass("has-error");
			obj.valid = true;
		} else {
			$("#"+elId).removeClass("has-error");
			obj.valid = false;
		}
	}
	$scope.clearText = function(elId, obj) {
		$("#"+elId).removeClass("has-error");
		obj.val= "";
		obj.valid = false;
	}
	
	$scope.togglePH = function(flag, txt) {
		var ph;
		if(flag) {
			ph = txt;
		} else {
			ph = "";
		}
		return ph;
	}

	$scope.openWorkModal = function( field ){
		console.log("openWorkModal");
		console.log(field);
		$scope.work_modal = $scope.formdata[field];
		$scope.work_modal.start_year =  $scope.modalYearList[0];
		$scope.work_modal.end_year = $scope.endYearList[0];
		$scope.work_modal.country = $scope.formdata[field].country;
		$scope.work_modal.company = {val:"", valid:false};
		$("#work-company").removeClass("has-error");
		$scope.work_modal.city = {val:"", valid:false};
		$("#work-city").removeClass("has-error");
		$scope.work_modal.position = {val:"", valid:false};
		$("#work-position").removeClass("has-error");
		$scope.work_modal.description = {val:"", valid:false};
		$("#work-desc").removeClass("has-error");
		
		//upload file temp  
		$scope.tempFile = null;
		$(".upload-name").val("Find File");
		$("#write-file1").val("");
		
		angular.element( '#worktModal' ).modal();
	}

	$scope.submitWorkModal = function( field ){
		console.log("submitWorkModal");
		
		if(!$scope.workModalValidChk() ) {
			return;
		}
		//work_exp list 추가
		$scope.work_modal.info = true;
		$scope.work_modal.country_name = $scope.nationsInfo[$scope.work_modal.country].country;
		if( $scope.work_modal.end_year.val < 0 ) {
			var d = new Date();
			$scope.work_modal.end_year.val = d.getFullYear()
		}
		
		//attach file
		if($scope.tempFile) {
			$scope.work_modal.file_del_yn = false;
			$scope.work_modal.work_exp_file = $scope.tempFile;
		}
		console.log($scope.work_modal);
		
		angular.element( '#worktModal' ).modal("hide");
	}

	$scope.workModalValidChk = function() {

		if($scope.work_modal.company.valid || $scope.work_modal.company.val == undefined || $scope.work_modal.company.val == "") {
			alert("Company");
			$("#modal-text-company").focus();
			return false;
		}

		if($scope.work_modal.country < 0) {
			alert("Country");
			$("#modal-sel-country").focus();
			return false;
		}

		if($scope.work_modal.city.valid || $scope.work_modal.city.val == undefined || $scope.work_modal.city.val == "") {
			alert("City");
			$("#modal-text-city").focus();
			return false;
		}

		if($scope.work_modal.position.valid || $scope.work_modal.position.val == undefined || $scope.work_modal.position.val == "") {
			alert("Position");
			$("#modal-text-position").focus();
			return false;
		}

		if($scope.work_modal.description.valid || $scope.work_modal.description.val == undefined || $scope.work_modal.description.val == "") {
			alert("Description");
			$("#modal-text-desc").focus();
			return false;
		}

		return true;
	}

	$scope.openEducationModal = function( field ){
		console.log("openEducationModal");
		$scope.education_modal = $scope.formdata[field];
		$scope.education_modal.start_year =  $scope.modalYearList[0];
		$scope.education_modal.end_year = $scope.endYearList[0];
		$scope.education_modal.location = $scope.formdata[field].location;
		
		$scope.education_modal.school = {val:"", valid:false};
		$("#edu-school").removeClass("has-error");
		$scope.education_modal.major = {val:"", valid:false};
		$("#edu-major").removeClass("has-error");
		$scope.education_modal.description = {val:"", valid:false};
		$("#edu-desc").removeClass("has-error");
		
		//upload file temp  
		$scope.tempFile = null;
		$(".upload-name").val("Find File");
		$("#write-file2").val("");
		
		console.log($scope.education_modal);
		angular.element( '#educationModal' ).modal();
	}

	$scope.submitEducationModal = function(){
		console.log("submitEducationModal");
		
		if(!$scope.eduModalValidChk() ) {
			return;
		}

		$scope.education_modal.info = true;
		$scope.education_modal.location_name = $scope.nationsInfo[$scope.education_modal.location].country;
		if( $scope.education_modal.end_year.val < 0 ) {
			var d = new Date();
			$scope.education_modal.end_year.val = d.getFullYear()
		}

		//attach file
		if($scope.tempFile){
			$scope.education_modal.education_file = $scope.tempFile;
		}
		angular.element( '#educationModal' ).modal("hide");
	}
	
	$scope.eduModalValidChk = function() {
		if($scope.education_modal.school.valid || $scope.education_modal.school.val == undefined || $scope.education_modal.school.val == "") {
			alert("School");
			$("#edu-modal-text-shcool").focus();
			return false;
		}
		
		if($scope.education_modal.location < 0) {
			alert("Location");
			$("#edu-location").focus();
			return false;
		}

		return true;
	}

	$scope.openCertificateModal = function( field ){
		console.log("openCertificateModal");
		$scope.certificate_modal = $scope.formdata[field];
		$scope.certificate_modal.year =  $scope.modalYearList[0];
		$scope.certificate_modal.name = {val:"", valid:false};
		$("#certi-name").removeClass("has-error");
		$scope.certificate_modal.organization = {val:"", valid:false};
		$("#certi-organization").removeClass("has-error");
		$scope.certificate_modal.description = {val:"", valid:false};
		$("#certi-desc").removeClass("has-error");
		
		//upload file temp  
		$scope.tempFile = null;
		$(".upload-name").val("Find File");
		$("#write-file3").val("");
		
		console.log($scope.certificate_modal);
		angular.element( '#certificateModal' ).modal();
	}

	$scope.submitCertificateModal = function(){
		console.log("submitCertificateModal");
		
		if (! $scope.certiModalValidChk() ) {
			return;
		}
		
		$scope.certificate_modal.info = true;
		//attach file 
		if($scope.tempFile){
			$scope.certificate_modal.certification_file = $scope.tempFile;
		}
		
		angular.element( '#certificateModal' ).modal("hide");
	}
	
	$scope.certiModalValidChk = function() {
		if($scope.certificate_modal.name.valid || $scope.certificate_modal.name.val == undefined || $scope.certificate_modal.name.val == "") {
			alert("Name");
			$("#certi-modal-text-name").focus();
			return false;
		}
		
		if($scope.certificate_modal.organization.valid || $scope.certificate_modal.organization.val == undefined || $scope.certificate_modal.organization.val == "") {
			alert("Organization or Institution");
			$("#certi-modal-text-organization").focus();
			return false;
		}
		
//		if($scope.certificate_modal.description.valid || $scope.certificate_modal.description.val == undefined || $scope.certificate_modal.description.val == "") {
//			alert("Description");
//			$("#certi-modal-text-desc").focus();
//			return false;
//		}
		
		return true;
	}

	$scope.editResumeData = function( field ) {
		
		console.log("editResumeData(" + field + ")");
		for(var i=1; i<=3; i++) {
			if (field == "work"+i) {
				$scope.work_modal = $scope.formdata["work"+i];
				$scope.modifyEndYearList($scope.formdata["work"+i]);
				if( typeof $scope.work_modal.work_exp_file == "string") {
					$(".upload-name").val($scope.work_modal.work_exp_file);
				} else if (typeof $scope.work_modal.work_exp_file == "object" ) {
					if($scope.work_modal.work_exp_file) {
						$(".upload-name").val($scope.work_modal.work_exp_file.name);
					} else {
						$(".upload-name").val("Find File");
					}
				} else {
					$(".upload-name").val($scope.work_modal.work_exp_file);
				}
				angular.element( '#worktModal' ).modal();
				break;
			}
			
			if (field == "education"+i) {
				$scope.education_modal = $scope.formdata["education"+i];
				$scope.modifyEndYearList($scope.formdata["education"+i]);
				if( typeof $scope.education_modal.education_file == "string") {
					$(".upload-name").val($scope.education_modal.education_file);
				} else if (typeof $scope.education_modal.education_file == "object" ) {
					if($scope.education_modal.education_file) {
						$(".upload-name").val($scope.education_modal.education_file.name);
					} else {
						$(".upload-name").val("Find File");
					}
				} else {
					$(".upload-name").val($scope.education_modal.education_file);
				}
				angular.element( '#educationModal' ).modal();
				break;
			}
			
			if (field == "certificate"+i) {
				$scope.certificate_modal = $scope.formdata["certificate"+i];
				if( typeof $scope.certificate_modal.certification_file == "string") {
					$(".upload-name").val($scope.certificate_modal.certification_file);
				} else if (typeof $scope.certificate_modal.certification_file == "object" ) {
					if($scope.certificate_modal.certification_file) {
						$(".upload-name").val($scope.certificate_modal.certification_file.name);
					} else {
						$(".upload-name").val("Find File");
					}
				} else {
					$(".upload-name").val($scope.certificate_modal.certification_file);
				}
				angular.element( '#certificateModal' ).modal();
				break;
			}
		}
		
	};
	
	$scope.delResumeData = function( field, index ){
		console.log("delResumeData(" + field + ")" + index);
		if (field == "work") {
			$scope.formdata[field+index].info = false;
			$scope.formdata[field+index].start_year = 0;
			$scope.formdata[field+index].end_year = 0;
			$scope.formdata[field+index].company = "";
			$scope.formdata[field+index].country = 0;
			$scope.formdata[field+index].country_name = "";
			$scope.formdata[field+index].city = "";
			$scope.formdata[field+index].position = "";
			$scope.formdata[field+index].description = "";
			$scope.formdata[field+index].work_exp_file = null;
			$scope.formdata[field+index].work_exp_file_path = "";
		} else if (field == "certificate") {
			$scope.formdata[field+index].info = false;
			$scope.formdata[field+index].year = 0;
			$scope.formdata[field+index].name = "";
			$scope.formdata[field+index].organization = "";
			$scope.formdata[field+index].description = "";
			$scope.formdata[field+index].certification_file = null;
			$scope.formdata[field+index].certification_file_path = "";
		} else if (field == "education") {
			$scope.formdata[field+index].info = false;
			$scope.formdata[field+index].year = 0;
			$scope.formdata[field+index].name = "";
			$scope.formdata[field+index].organization = "";
			$scope.formdata[field+index].description = "";
			$scope.formdata[field+index].education_file = null;
			$scope.formdata[field+index].education_file_path = "";
		}
	}

	$scope.uploadResumeFile = function(files){
		$scope.tempFile = files[0];
	}

	$scope.deleteResumeFile = function(modal, field, fileNum) {
		$scope[modal][field] = null;
		$scope[modal].file_del_yn = true;
		$(".upload-name").val("Find File");
		$("#write-file" + fileNum).val("");
	}

	$scope.checkValidation = function ( ) {
		var formElements = document.getElementById("profile-form").elements;
		for (var index = 0; index < formElements.length; index++) {
			console.log(formElements[index]);
			if(formElements[index].checkValidity() == false) {
				alert("Please fill in the required field.");
			    formElements[index].focus();
			    return false;
			}
		}

	    return true;
	};

	$scope.pendingResumeTutor = function ( ) {
		for( var i = 1; i <= 3; i++) {
			if( $scope.formdata["work"+i].info ) {
				$scope.formdata.resumeFile.append("work"+ i + "_file", $scope.formdata["work" + i ].work_exp_file);
			}
			
			if( $scope.formdata["education"+i].info ) {
				$scope.formdata.resumeFile.append("education" + i + "_file", $scope.formdata["education" + i ].education_file);
			}
			
			if( $scope.formdata["certificate"+i].info ) {
				$scope.formdata.resumeFile.append("certification" + i + "_file", $scope.formdata["certificate" + i ].certification_file);
			}
			
		}
		if ( $scope.checkValidation() ) {
			console.log("file upload start");
			$http.post("/tutor/apply/profile/upload_file/", $scope.formdata.resumeFile,{
				withCredentials: true,
		        headers: {'Content-Type': undefined },
		        transformRequest: angular.identity
			}).success(function(response){
				console.log(response.message);
				if (response.isSuccess == 1) {
					objData = {};
					objData.education = [];
					objData.work_exp = [];
					objData.certification = [];
					for(var i = 1; i <= 3; i++) {
						var obj = {};
						obj["work" + i] = {info: "", start_year: "", end_year: "", company: "", country: "", country_name: "", city: "", position: "", description:"" , file_del_yn: ""};
						obj["work" + i].info = $scope.formdata["work"+i].info;
						obj["work" + i].start_year = $scope.formdata["work" + i].start_year.val;
						obj["work" + i].end_year = $scope.formdata["work" + i].end_year.val;
						obj["work" + i].company = $scope.formdata["work" + i].company.val;
						obj["work" + i].country = $scope.formdata["work" + i].country;
						obj["work" + i].country_name = $scope.formdata["work" + i].country_name;
						obj["work" + i].city = $scope.formdata["work" + i].city.val;
						obj["work" + i].position = $scope.formdata["work" + i].position.val;
						obj["work" + i].description = $scope.formdata["work" + i].description.val;
						obj["work" + i].file_del_yn = $scope.formdata["work" + i].file_del_yn;
						objData.work_exp.push( obj );
						
						obj = {};
						obj["certificate" + i] = {info : "", year: "", name: "", organization: "", description: "", file_del_yn: ""};
						obj["certificate" + i].info = $scope.formdata["certificate" + i].info;
						obj["certificate" + i].year = $scope.formdata["certificate" + i].year.val;
						obj["certificate" + i].name = $scope.formdata["certificate" + i].name.val;
						obj["certificate" + i].organization = $scope.formdata["certificate" + i].organization.val;
						obj["certificate" + i].description = $scope.formdata["certificate" + i].description.val;
						obj["certificate" + i].file_del_yn = $scope.formdata["certificate" + i].file_del_yn;
						objData.certification.push( obj );

						obj = {};
						obj["education" + i] = {info: "", start_year: "", end_year: "", school: "", location: "", location_name: "", degree: "", major: "", description:"" , file_del_yn: ""};
						obj["education" + i].info = $scope.formdata["education"+i].info;
						obj["education" + i].start_year = $scope.formdata["education" + i].start_year.val;
						obj["education" + i].end_year = $scope.formdata["education" + i].end_year.val;
						obj["education" + i].school = $scope.formdata["education" + i].school.val;
						obj["education" + i].location = $scope.formdata["education" + i].location;
						obj["education" + i].location_name = $scope.formdata["education" + i].location_name;
						obj["education" + i].degree = $scope.formdata["education" + i].degree;
						obj["education" + i].major = $scope.formdata["education" + i].major.val;
						obj["education" + i].description = $scope.formdata["education" + i].description.val;
						obj["education" + i].file_del_yn = $scope.formdata["education" + i].file_del_yn;
						objData.education.push( obj );
					}
					
					//upload file list
					// console.log(response.upload_file_list);
					objData.upload_file_list = response.upload_file_list;
					console.log(objData)

					$http.post("/tutor/apply/profile/save", objData ).success(function(response) {
						console.log(response.message);
						if(response.isSuccess == 1){
							$window.location.href = ("/settings/editprofile/");
						}
					});
				}

			});
		}
	}

});

function minutesToUTC(minute){
	var UTCString = "";
	if(minute<0){
		UTCString += "-";
	}
	else{
		UTCString += "+";
	}
	if(Math.abs(minute)/60 > 10){
		UTCString += Math.abs(minute)/60 + ":";
	}
	else{
		UTCString += "0"+ Math.abs(minute)/60 + ":";
	}
	if(Math.abs(minute) % 60 > 10){
		UTCString += Math.abs(minute) % 60;
	}
	else{
		UTCString += "0" + Math.abs(minute) % 60;
	}
	return UTCString;
}
function utcToMilliseconds(utc){
	var ms = ((parseInt(utc.slice(1,3)) * 60) + (parseInt(utc.slice(4,6)) * 60)) * 60000;
	if(utc.slice(0,1) == "-"){
		ms = -(ms);
	}
	return ms;
}
function hideNationImage(){
	$(".nations-image").css("display","none");
}
//********* For DateFormat **********
String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};
Date.prototype.format = function(f) {
    if (!this.valueOf()) return " ";
 
    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;
     
    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).zf(2);
            case "MM": return (d.getMonth() + 1).zf(2);
            case "dd": return d.getDate().zf(2);
            case "E": return weekName[d.getDay()];
            case "HH": return d.getHours().zf(2);
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case "mm": return d.getMinutes().zf(2);
            case "ss": return d.getSeconds().zf(2);
            case "a/p": return d.getHours() < 12 ? "AM" : "PM";
            default: return $1;
        }
    });
};
// *****************************************