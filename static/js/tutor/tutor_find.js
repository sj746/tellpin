
tellpinApp.controller( "tutorFindController", ["$scope", "$http", "$sce", "$window", function( $scope, $http, $sce, $window ) {

	angular.element(document).ready(function(){
		console.log($scope.data);
		$scope.tutor_list = [];
		$scope.page=1;
		$scope.f_q = {
			'type' : null,
			'name': null,
			'teaches' : null,
			'avail' : null,
			'hour' : null,
			'speaks' : null,
			'nation_from' : null,
			'livein' : null,
			'liveincity' : null,
			'meet' : null,
//			'tags' : null,
			'native' : null,
			'online' : null,
			'trial' : null,
			'video' : null,
			'page' : $scope.page
		}
		$scope.searchName();
		$scope.more_type = 0;
		$scope.more_count = 0;
		$scope.b_more_click = false;
		angular.element( "[data-toggle='tooltip']" ).tooltip();

	});

	$scope.initLoading = function(){
		console.log('initLoading');
		$scope.loading = true;
	}

//	$http.post("/tutor/apply/getApplyStatus/").success(function(response) {
//		if ( response.isSuccess == 1) {
//			console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
//			$scope.applyStatus = response.status;
//			
//		}
//	});

	$scope.openMessageModal = function (msg_type, id) {
		$scope.$emit('messageEventEmit', { type: msg_type, user_id: id });
		$window.event.stopPropagation();
	};

	$scope.setBookmark = function(tid, index){
		$http.post("/tutor/follow/", { tid : tid }).success( function( result ) {
			if(result.isSuccess == 1){
				$scope.tutor_list[index].is_bookmark = 1;
				$scope.tutor_list[index].follow_cnt += 1;
			}
		});
	}

	$scope.removeBookmark = function(tid, index){
		$http.post("/tutor/follow/", { tid : tid }).success( function( result ) {
			if(result.isSuccess == 1){
				$scope.tutor_list[index].is_bookmark = 0;
				$scope.tutor_list[index].follow_cnt -= 1;
			}
		});
	}
	
	$scope.showMoreTutorList = function() {
		$("#btn_more").attr("disabled", "disabled");
		$("#btn_more").text("");
		$("#btn_more").append("<img src='/static/img/ajax-loader.gif'>");
		if(!$scope.b_more_click){
			$scope.b_more_click = true;
			$scope.f_q.page = $scope.page;
			$http.post( "/tutor/search_tutor/", $scope.f_q).success( function( result ) {
				if ( result.isSuccess == 1 ) {
					//$scope.listdata=result.allQuestion;
//					console.log('ajax test');
					for(var i = 0; i < result.tutor_list.length; i++) {
						$scope.tutor_list.push(result.tutor_list[i])
					}
					$scope.page_info = result.page_info;
					if ($scope.page_info.has_next == 1) {
						$scope.page = $scope.page + 1;
					}
				}
	
				$("#btn_more").removeAttr("disabled");
				$("#btn_more").text("show more");
				$scope.b_more_click=false;
				$scope.loading = false;
//				console.log(result.tutor_list);
			});
		}
	}
	
	$scope.checkNativeOnly=function(){
		//console.log( $("#native_only").is(":checked") );
		//console.log( $("#teaches").val() );
		if( $("#native_only").is(":checked") && $("#teaches").val() == "all"){
			alert(' Teaches language를 선택해 주세요.');
			$("#native_only").attr("checked", false);
		}
	}

	$scope.searchName=function(){
		//alert( $("#search_name").val() );
		//alert('search name: ' + $("#search_name").val());
		//TODO
		//Search Name
		$scope.loading = true;
		var t_name = $("#search_name").val();
		console.log('Search name : ' + t_name );

		var obj={
					'type':'name',
					'name':t_name,
					'page':$scope.page
				};
		$scope.f_q.type = 'name';
		$scope.f_q.name = t_name;
		$scope.f_q.page = 0;
		console.log('page:');
		console.log($scope.f_q.page);
		$http.post( "/tutor/search_tutor/", $scope.f_q).success( function( result ) {
			console.log(result);
			if ( result.isSuccess == 1 ) {
				//$scope.listdata=result.allQuestion;
				console.log('ajax test');
				$scope.tutor_list = result.tutor_list;
				console.log(result.page_info);
				$scope.page_info = result.page_info;
				if ($scope.page_info.has_next == 1) {
					$scope.page = $scope.page + 1;
				}
			}
			console.log(result.tutor_list);
			$scope.loading = false;
		});
	}

	$scope.clickSearchTutor=function(){
		console.log("clickSearchTutor");
		//TODO
		$scope.loading = true;
		
		//Filter value
		var teaches = $("#teaches").val();
		var avail = [];
		var list ={};
		list["time0"] = $("#time_0").is(":checked");
		list["time6"] = $("#time_6").is(":checked");
		list["time12"] = $("#time_12").is(":checked");
		list["time18"] = $("#time_18").is(":checked");
		list["day_mon"] = $("#day_mon").is(":checked");
		list["day_tue"] = $("#day_tue").is(":checked");
		list["day_wed"] = $("#day_wed").is(":checked");
		list["day_thu"] = $("#day_thu").is(":checked");
		list["day_fri"] = $("#day_fri").is(":checked");
		list["day_sat"] = $("#day_sat").is(":checked");
		list["day_sun"] = $("#day_sun").is(":checked");
		avail.push( list );

		var hour = $("#hourlyrate").val();
		var speaks = $("#speaks").val();
		var nation_from = $("#from").val();
		var livein = $("#livein").val();
		var liveincity = $("#liveincity").val();
		var meet = [];
		list = {};
		list["check_online"] = $("#check_online").is(":checked");
		list["check_offline"] = $("#check_offline").is(":checked");
		list["check_video"] = $("#check_video").is(":checked");
		list["check_audio"] = $("#check_audio").is(":checked");
		if( !$("#check_online").is(":checked") && !$("#check_offline").is(":checked") && !$("#check_video").is(":checked") && !$("#check_audio").is(":checked"))
			list["all"] = 'all';
		else
			list["all"] = '';
		meet.push( list );
		
		/*meet.push( $("#check_online").is(":checked") );
		meet.push( $("#check_audio").is(":checked") );
		meet.push( $("#check_video").is(":checked") );
		meet.push( $("#check_offline").is(":checked") );*/
//		var tags = [];
//		for(var i=0; i<$scope.search_data[0].tag_list.length; i++){
//			
//			if ( $("#sptag-"+$scope.search_data[0].tag_list[i].stid).is(":checked") ){
//				var list = {};
//				list["id"] = $scope.search_data[0].tag_list[i].stid;
//				list["checked"] = $("#sptag-"+$scope.search_data[0].tag_list[i].stid).is(":checked");
//				tags.push( list );
//			}
//		}

		var native = $("#native_only").is(":checked");
		var online = $("#online_only").is(":checked");
		var trial = $("#check_trial").is(":checked");
		var video = $("#check_video").is(":checked");
		/*console.log('teaches: ' + teaches);
		console.log('avail: ' + avail);
		console.log('hour: ' + hour);
		console.log('speaks: ' + speaks);
		console.log('nation_from: ' + nation_from);
		console.log('livein: ' + livein);
		console.log('liveincity: ' + liveincity);
		console.log('meet: ' + meet);
		console.log('tags: ' + tags);
		console.log('native: ' + native);
		console.log('online: ' + online);*/

		var obj = { 
					'type': 'filter',
					'teaches': teaches ,
					'avail': avail,
					'hour': hour,
					'speaks': speaks,
					'nation_from': nation_from,
					'livein': livein,
					'liveincity': liveincity,
					'meet': meet,
//					'tags': tags,
					'native': native,
					'online': online,
					'trial': trial,
					'video': video,
					'page':$scope.page
		}
		console.log(obj);
		$scope.f_q.type = 'filter';
		$scope.f_q.teaches = teaches;
		$scope.f_q.avail = avail;
		$scope.f_q.hour = hour;
		$scope.f_q.speaks = speaks;
		$scope.f_q.nation_from = nation_from;
		$scope.f_q.livein = livein;
		$scope.f_q.liveincity = liveincity;
		$scope.f_q.meet = meet;
//		$scope.f_q.tags = tags;
		$scope.f_q.native = native;
		$scope.f_q.online = online;
		$scope.f_q.trial = trial;
		$scope.f_q.video = video;
		$scope.f_q.page = 0;
		$http.post( "/tutor/search_tutor/", $scope.f_q).success( function( result ) {
			if ( result.isSuccess == 1 ) {
				//$scope.listdata=result.allQuestion;
				console.log('ajax test');
				$scope.tutor_list = result.tutor_list;
				console.log(result.page_info);
				$scope.page_info = result.page_info;
				if ($scope.page_info.has_next == 1) {
					$scope.page = $scope.page + 1;
				}
			}
			$scope.loading = false;
			console.log(result.tutor_list);
		});
	}

//	$scope.online_check = function(){
//		var checked = $("#check_online").is(":checked");
//		if( checked ){
//			$("#check_audio").attr("disabled", false);
//			$("#check_video").attr("disabled", false);
//		}else{
//			$("#check_audio").attr("disabled", true);
//			$("#check_audio").attr("checked", false);
//			$("#check_video").attr("disabled", true);
//			$("#check_video").attr("checked", false);
//		}
//	}
	$scope.showSchedulePopup = function(tid){
		event.preventDefault();
		event.stopPropagation();
		console.log(tid);
		$("body").css("overflow-y","hidden");
		$("body").css("overflow-x","hidden");
		$scope.$emit("sendTid", tid);
		$http.post( "/tutor/tutor_schedule/", {tid: tid}).success( function( response ) {
			if ( response.isSuccess == 1 ) {
				$scope.scheduleInfo = response;
				console.log($scope.scheduleInfo);
				$scope.tutor = new Object();
				$scope.tutor.user = tid;
				$scope.schedulePopup = true;
			} 
		});
		
	}

	$scope.hideSchedulePopup = function(){
		$("body").css("overflow-y","auto");
		$("body").css("overflow-x","auto");
		$scope.schedulePopup = false;
	}

	$scope.showIntroPopup = function(src){
		event.stopPropagation();
		$scope.introPopup = true;
		$scope.introSrc = src;
	}
	$scope.hideIntroPopup = function(){
		$scope.introPopup = false;
	}

	$scope.trustSrc = function( src ) {
		src = "https://www.youtube.com/embed/"+src.split("/")[3];
		return $sce.trustAsResourceUrl( src );
		//return src;
	}

	$scope.clickTutorInfo = function(tid){
//		event.preventDefault();
		console.log(tid);
		$(location).attr('href', '/tutor/'+tid+'/');
	}

	$scope.getCityInfo = function(in_field) {
		var field = in_field;
		if (field == "livein") {
			$("#" + field + "city").html('').append("<option value='all'>Living in City</option>");
		} else {
			$("#" + field + "city").html('').append("<option value='all'>From City</option>");	
		}
		
		if($("#" + field).val()==""){
			return;
		}

		$http.post("/common/city/", {country_id : $("#" + field).val()}).success( function( result ) {
			if(result.isSuccess == 1){
				console.log(result);
				for( var i = 0; i < result.city.length ; i++){
					$("#" + field + "city").append("<option value='"+ result.city[i].id +"'>"+ result.city[i].city +"</option> ");
				}
			}
			$("#" + field + "city").trigger("chosen:updated");
		});
		$("#" + field + "city").trigger("chosen:updated");
	}

	$scope.goURL = function(url){
		console.log('goUrl()');
		console.log(url);
		if(url.length > 0){
			$window.location.href = (url);
		}
	}

	$scope.mobileSearchName=function(){
		$scope.loading = true;
		//alert( $("#search_name").val() );
		//alert('search name: ' + $("#search_name").val());
		//TODO
		//Search Name
		console.log('mobile_search_name ');
		var t_name = $("#mobile_search_name").val();
		console.log('Search name : ' + t_name );

		var obj={
					'type':'name',
					'name':t_name,
					'page':$scope.page
				};
		$scope.f_q.type = 'name';
		$scope.f_q.name = t_name;
		$scope.f_q.page = 0;
		$http.post( "/tutor/search_tutor/", $scope.f_q).success( function( result ) {
			if ( result.isSuccess == 1 ) {
				//$scope.listdata=result.allQuestion;
//				console.log('ajax test');
				$scope.tutor_list = result.tutor_list;
//				console.log(result.page_info);
				$scope.page_info = result.page_info;
				if ($scope.page_info.has_next == 1) {
					$scope.page = $scope.page + 1;
				}
			}
			$scope.loading = false;
//			console.log(result.tutor_list);
		});
	}

	$scope.mobileSearchTutor=function(){
		//TODO
		//Filter value
		$scope.loading = true;
		
		var teaches = $("#mobile_teaches").val();
		var avail = [];
		var list ={};
		list["time0"] = $("#mobile__time_0").is(":checked");
		list["time6"] = $("#mobile_time_6").is(":checked");
		list["time12"] = $("#mobile_time_12").is(":checked");
		list["time18"] = $("#mobile_time_18").is(":checked");
		list["day_mon"] = $("#mobile_day_mon").is(":checked");
		list["day_tue"] = $("#mobile_day_tue").is(":checked");
		list["day_wed"] = $("#mobile_day_wed").is(":checked");
		list["day_thu"] = $("#mobile_day_thu").is(":checked");
		list["day_fri"] = $("#mobile_day_fri").is(":checked");
		list["day_sat"] = $("#mobile_day_sat").is(":checked");
		list["day_sun"] = $("#mobile_day_sun").is(":checked");
		avail.push( list );
		
		var hour = $("#mobile_hourlyrate").val();
		var speaks = $("#mobile_speaks").val();
		var nation_from = $("#mobile_from").val();
		var livein = $("#mobile_livein").val();
		var liveincity = $("#mobile_liveincity").val();
		var meet = [];
		list = {};
		list["check_online"] = $("#video_avail").is(":checked");
		list["check_offline"] = false;
		list["check_video"] = $("#video_avail").is(":checked");
		list["check_audio"] = false; //조회조건에 넣지 않음.
		if( !$("#video_avail").is(":checked") )
			list["all"] = 'all';
		else
			list["all"] = '';
		meet.push( list );
		
		/*meet.push( $("#check_online").is(":checked") );
		meet.push( $("#check_audio").is(":checked") );
		meet.push( $("#check_video").is(":checked") );
		meet.push( $("#check_offline").is(":checked") );*/
//		var tags = [];
		/*
		for(var i=0; i<$scope.search_data[0].tag_list.length; i++){
			
			if ( $("#sptag-"+$scope.search_data[0].tag_list[i].stid).is(":checked") ){
				var list = {};
				list["id"] = $scope.search_data[0].tag_list[i].stid;
				list["checked"] = $("#sptag-"+$scope.search_data[0].tag_list[i].stid).is(":checked");
				tags.push( list );
			}
		}*/
		var native = $("#mobile_native_only").is(":checked");
		var online = $("#mobile_online_only").is(":checked");
		/*console.log('teaches: ' + teaches);
		console.log('avail: ' + avail);
		console.log('hour: ' + hour);
		console.log('speaks: ' + speaks);
		console.log('nation_from: ' + nation_from);
		console.log('livein: ' + livein);
		console.log('liveincity: ' + liveincity);
		console.log('meet: ' + meet);
		console.log('tags: ' + tags);
		console.log('native: ' + native);
		console.log('online: ' + online);*/

		var obj = { 
					'type': 'filter',
					'teaches': teaches ,
					'avail': avail,
					'hour': hour,
					'speaks': speaks,
					'nation_from': nation_from,
					'livein': livein,
					'liveincity': liveincity,
					'meet': meet,
//					'tags': tags,
					'native': native,
					'online': online,
					'page':$scope.page
		}
		$scope.f_q.type = 'filter';
		$scope.f_q.teaches = teaches;
		$scope.f_q.avail = avail;
		$scope.f_q.hour = hour;
		$scope.f_q.speaks = speaks;
		$scope.f_q.nation_from = nation_from;
		$scope.f_q.livein = livein;
		$scope.f_q.liveincity = liveincity;
		$scope.f_q.meet = meet;
//		$scope.f_q.tags = tags;
		$scope.f_q.native = native;
		$scope.f_q.online = online;
		$scope.f_q.page = 0;
		
		console.log(obj);
		$http.post( "/tutor/search_tutor/", $scope.f_q).success( function( result ) {
			if ( result.isSuccess == 1 ) {
				//$scope.listdata=result.allQuestion;
				console.log('ajax test');
				$scope.tutor_list = result.tutor_list;
				console.log(result.page_info);
				$scope.page_info = result.page_info;
				if ($scope.page_info.has_next == 1) {
					$scope.page = $scope.page + 1;
				}
			}
			$scope.loading = false;
			console.log(result.tutor_list);
		});
	}

//	$scope.mobileCheckNativeOnly=function(){
//		//console.log( $("#native_only").is(":checked") );
//		//console.log( $("#teaches").val() );
//		if( $("#native_only").is(":checked") && $("#teaches").val() == "all"){
//			alert(' Teaches language를 선택해 주세요.');
//			$("#native_only").attr("checked", false);
//		}
//	}

	$scope.mobileToggleModal =  function(){
		$("#messageModal").modal('toggle');
	}

	$scope.mobileOpenFilterModal = function(){
		console.log('test');
		$("#messageModal").modal();
	}

	$scope.mobileChangeLiveinField = function(){

		$("#mobile_liveincity").html('').append("<option value='all'>Live in City</option");
		
		if($("#mobile_livein").val()==""){
			return;
		}
		
		$http.post("/common/city/", { nid : $("#mobile_livein").val() }).success( function( result ) {
			console.log(result);
			if(result.isSuccess == 1){
				console.log(result.city.citylist);
				console.log(result.city.citylist.length);
				for( var i = 0; i < result.city.citylist.length ; i++){
					$("#mobile_liveincity").append("<option value='"+ result.city.citylist[i].city_id +"'>"+ result.city.citylist[i].city +"</option> ");
				}
			}
		});
	}	

}]);

tellpinApp.controller("scheduleController", function( $scope, $http, $location, $window, $filter ) {
	$scope.setWeekday = function(){
		$scope.weekList = [];
		for(var idx = 0; idx <7 ; idx++){
			$scope.weekList.push(dayToAlphabet($scope.schedule.dow_list[idx]));
		}
	}
	$scope.setReservedTime = function(){
		$scope.reservedTime = [];
		for(var idx = 0; idx < $scope.reserved.length ; idx++){
			var bundleSize = $scope.reserved[idx].duration / 30;
			var day = $scope.reserved[idx].rv_time.slice(0,1);
			var index = -1;
			if($scope.reserved[idx].rv_time.slice(3,5) == "30"){
				index = parseInt($scope.reserved[idx].rv_time.slice(1,3))*2 +1;
			}
			else{
				index = parseInt($scope.reserved[idx].rv_time.slice(1,3))*2;
			}
			for(var i = 0 ; i < bundleSize; i++){
				$scope.reservedTime.push(day+indexToValue(index+i));
			}
		}
	}
	$scope.area = [];
	$scope.initAreaValue = function(day, index){
		$scope.bundleCount = $scope.bundle;
		var time = day;
		if(index%2 == 0){
			if(index/2 < 10){
				time = time + "0" + index/2 + "00";
			}
			else{
				time = time + index/2 + "00";
			}
		}
		else{
			if(index/2 < 10){
				time = time + "0" + ((index-1)/2) + "30";
			}
			else{
				time = time + ((index-1)/2) + "30";
			}
		}
		return time;
	}
	$scope.initAreaState = function(day, index){
		var dow = {"mon":"A" , "tue":"B", "wed":"C", "thu":"D", "fri":"E", "sat":"F", "sun":"G"}
				
		$scope.setCurrentTime();
		if( $scope.scheduleInfo.today == $scope.schedule.mark_day ) {
			if (day == dow[$scope.schedule.dow_list[0]]) {
				if ( index < $scope.currentTime * 2 + parseInt($scope.currentMin / 30) + 1 ){
					
					if($scope.reservedTime.indexOf($scope.area[index][day].value) != -1){
						return 3;
					}
					
					return 0;
				}
			}
		}
		
		
		if($scope.reservedTime.indexOf($scope.area[index][day].value) != -1){
			return 3;
		}
		else{
			if($scope.schedule.available_time.indexOf($scope.area[index][day].value) != -1){
				for(var i = 0 ; i<$scope.schedule.appointed.length; i++){
					if($scope.schedule.appointed[i].delta_time == $scope.area[index][day].value){
						if($scope.schedule.appointed[i].type == 2){
							return 0;
						}
					}
				}
				return 1;
			}
			else{
				for(var i = 0 ; i<$scope.schedule.appointed.length; i++){
					if($scope.schedule.appointed[i].delta_time == $scope.area[index][day].value){
						if($scope.schedule.appointed[i].type == 1){
							return 1;
						}
					}
				}
				return 0;
			}
		}
	};
// duration에 따라 예약가능한 영역인지 확인
	$scope.checkAvailableTime = function(index, day){
			if($scope.area[index][day].state == 1){
				$scope.area[index][day].isHover = 1;
			}
	};
	$scope.resetIsHover = function(index, day){
		if($scope.duration == 60){
			if($scope.area[index][day].isHover == 1 && $scope.area[index+1][day].isHover == 1){
				$scope.area[index][day].isHover = 0;
				$scope.area[index+1][day].isHover = 0;
			}
		}
		else{
			if($scope.area[index][day].isHover = 1){
				$scope.area[index][day].isHover = 0;
			}
		}
	};
	$scope.getPrevWeek = function(tid){
		$http.post("/tutor/tutor_schedule/prev/", { tid: tid, date : $scope.schedule.mark_day}).success(function(response){
			$scope.schedule= response.schedule;
			$scope.reserved = response.reserved;
			$scope.setReservedTime();
			$scope.initAllArea();
		});
	};
	$scope.getNextWeek = function(tid){
		$http.post("/tutor/tutor_schedule/next/", { tid: tid, date : $scope.schedule.mark_day}).success(function(response){
			$scope.schedule= response.schedule;
			$scope.reserved = response.reserved;
			$scope.setReservedTime();
			$scope.initAllArea();
		});
	};
	$scope.initAllArea = function(){
		for(var index=0; index < $scope.area.length; index++){
			$scope.area[index].A.state = $scope.initAreaState('A', index);
			$scope.area[index].B.state = $scope.initAreaState('B', index);
			$scope.area[index].C.state = $scope.initAreaState('C', index);
			$scope.area[index].D.state = $scope.initAreaState('D', index);
			$scope.area[index].E.state = $scope.initAreaState('E', index);
			$scope.area[index].F.state = $scope.initAreaState('F', index);
			$scope.area[index].G.state = $scope.initAreaState('G', index);
		}
	};
	$scope.getTbodyHeight = function(){
		var height = window.innerHeight - 70 - 140;
		if(height > 722){
			height = "722px";
		}
		$(".schedule-table tbody").height(height);

	}

	$scope.goToReservation = function (){
		var $form = $('<form></form>');
	    $form.attr('action', '/tutor/reservation/which/');
	    $form.attr('method', 'post');
	    $form.appendTo('body');
		var tid = $('<input type="hidden" value="' +$scope.tutor.user + '" name="tid">');
	    var accessType = $('<input type="hidden" value="0" name="accessType">');
	
	    $form.append(tid).append(accessType);
	    $form.submit();
	};
	
	$scope.hoverIn = function(){
	    $(".tr-schedule-template table tbody tr").hover(function() {
			$(this).prev().find(".time").css("background", "#ececec");
			
		}, function() {
			$(this).prev().find(".time").css("background", "none");
		});
	};
	
	$scope.initCurrentTime = function(utcTime) {
		$scope.userUTCTime = utcTime;
	}
	
	$scope.setCurrentTime = function(){
		
		var now = new Date();
		$scope.currentUTCTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
		$scope.changeTimezone();
		//$timeout($scope.setCurrentTime, 1000);
		//$scope.updateTimeTable();
	};
	
	$scope.changeTimezone = function(){
		if($scope.userUTCTime == ''){
			$scope.userUTCTime = minutesToUTC(-(new Date().getTimezoneOffset()));
		}
		var displayTime = new Date($scope.currentUTCTime.getTime() + utcToMilliseconds($scope.userUTCTime));
		$scope.currentTime = displayTime.format("HH");
		$scope.currentMin = displayTime.format("mm");
		
		$scope.today = displayTime.getFullYear().toString() + (displayTime.getMonth()+1).toString() + (displayTime.getDate()+1).toString();
		$scope.displayTime = displayTime;
	}
	
});
/*
body.loading .loading{
	display: block;
}*/

$(document).ready(function(){
	$('#search_name').keypress(function(event){
		  if(event.keyCode == 13){
		    //angular.element.scope().searchName();
//			  console.log('test');
//			  console.log(angular.element($(".container")).scope().tutor_list);
		    angular.element($(".container")).scope().searchName();
		  }
	});
});

function toggle(div_id) {
	var el = document.getElementById(div_id);
	
	if ( el.style.display == 'none' ) {	el.style.display = 'block';}
	else {el.style.display = 'none';}
	
}
function blanket_size(popUpDivVar) {
	if (typeof window.innerWidth != 'undefined') {
		viewportheight = window.innerHeight;
	} else {
		viewportheight = document.documentElement.clientHeight;
	}
	if ((viewportheight > document.body.parentNode.scrollHeight) && (viewportheight > document.body.parentNode.clientHeight)) {
		blanket_height = viewportheight;
	} else {
		if (document.body.parentNode.clientHeight > document.body.parentNode.scrollHeight) {
			blanket_height = document.body.parentNode.clientHeight;
		} else {
			blanket_height = document.body.parentNode.scrollHeight;
		}
	}
	var blanket;
	if(popUpDivVar == 'add-pop'){
		blanket = document.getElementById('blanket');
	}else if(popUpDivVar == 'add-pop2'){
		blanket = document.getElementById('blanket2');
	}else if(popUpDivVar == 'add-pop3'){
		blanket = document.getElementById('blanket3');
	}else if(popUpDivVar == 'add-pop4'){
		blanket = document.getElementById('blanket4');
	}else if(popUpDivVar == 'add-pop5'){
		blanket = document.getElementById('blanket5');
	}
	blanket.style.height = blanket_height + 'px';
	var popUpDiv = document.getElementById(popUpDivVar);
	popUpDiv_height=blanket_height/2-200;//200 is half popup's height
	popUpDiv.style.top = '250px';
	//popUpDiv.style.top = popUpDiv_height + 'px';
}
function window_pos(popUpDivVar) {
	if (typeof window.innerWidth != 'undefined') {
		viewportwidth = window.innerHeight;
	} else {
		viewportwidth = document.documentElement.clientHeight;
	}
	if ((viewportwidth > document.body.parentNode.scrollWidth) && (viewportwidth > document.body.parentNode.clientWidth)) {
		window_width = viewportwidth;
	} else {
		if (document.body.parentNode.clientWidth > document.body.parentNode.scrollWidth) {
			window_width = document.body.parentNode.clientWidth;
		} else {
			window_width = document.body.parentNode.scrollWidth;
		}
	}
	var popUpDiv = document.getElementById(popUpDivVar);
	//width
	window_width=window_width/2-200;//200 is half popup's width
	//left css
	/* if(popUpDivVar == 'add-pop3'){
		popUpDiv.style.left = window_width + 'px';
	} */
}
function popup(windowname) {
	//var selected = $(":radio[name='category_type']:checked").attr('id') ;
	$("#new_name").val('');
	$("#new_limit").val('0');
	$('input:radio[name="new_display"]:radio[value="Y"]').prop("checked", "checked");
	$("#new_type").val('1');
	blanket_size(windowname);
	window_pos(windowname);
	if(windowname == 'add-pop'){
		toggle('blanket');
	}else if(windowname == 'add-pop2'){
		toggle('blanket2');
	}else if(windowname == 'add-pop3'){
		toggle('blanket3');
	}else if(windowname == 'add-pop4'){
		toggle('blanket4');
	}else if(windowname == 'add-pop5'){
		toggle('blanket5');
	}
	
	toggle(windowname);		
}

function change_teaches(){
	console.log( 'change_teaches() ');
	if( $("#teaches").val() == "all" ){
		$("#native_only").attr("checked", false);
	}
}
