tellpinApp.run(function($rootScope, $location){
    $rootScope.$on("addReservation", function(event, data){ 
        $rootScope.$broadcast("addSelectedList", data);
    });
    $rootScope.$on("deleteTime", function(event, data){
        $rootScope.$broadcast("syncSchedule", data); 
    });
    $rootScope.$on("getReservation", function(event){ 
        $rootScope.$broadcast("checkReservation"); 
    });
    $rootScope.$on("setReservationData", function(event, data){
        $rootScope.$broadcast("successReservation", data); 
    });
    $rootScope.$on("removeReservation", function(event, data){
        $rootScope.$broadcast("removeSelectedList", data); 
    });
    $rootScope.$on("changeSchedule", function(event, data){
        $rootScope.$broadcast("changeWeekList", data); 
    });
    $rootScope.$on("sendSelectedList", function(event, data){
        $rootScope.$broadcast("updateSchedule", data); 
    });
    $rootScope.$on("setSelectedList", function(event, data){
        $rootScope.$broadcast("getSelectedList", data); 
    });
});
tellpinApp.controller("navigationController", function( $scope, $http, $location, $window ) {
		angular.element(document).ready(function () {
			if($scope.accessType == 0){
				$("#tr-navigation .pc-box").css({"width":"calc(88% / 6)", "margin-right":"2%"});
				$("#tr-navigation .mobile-box").eq(0).css("margin-left","calc(50% - 115px)");
				
			}
			else if($scope.accessType == 1){
				$("#tr-navigation .pc-box").css({"width":"calc(90% / 5)", "margin-right":"2%"});
				$("#tr-navigation .mobile-box").eq(1).css("margin-left","calc(50% - 95px)");
			}
			else{
				$("#tr-navigation .pc-box").css({"width":"23%", "margin-right":"2%"});
				$("#tr-navigation .mobile-box").eq(2).css("margin-left","calc(50% - 75px)");
			}
		});
		$scope.initAccessType = function(SessionAccessType, accessType){
			if(SessionAccessType == ""){
				console.log(accessType);
				return parseInt(accessType);
			}else{
				return parseInt(SessionAccessType);
			}
		}
});
tellpinApp.controller("whichController", function( $scope, $http, $location, $window ) {
	$scope.goBack = function(){
		$window.history.back();
	}
	$scope.selectLesson = function(course){
		$scope.selectedLesson = course;
	}
	$scope.submitWhich = function(){
		$("#which-form").append("<input type='hidden' name='selected_lid' value='"+$scope.selectedLesson.id+"'/>");
		$("#which-form").submit();
	};
	$scope.goNext = function(url){
		$window.location.href = url;
	}
	$scope.init = function(data) {
		console.log(data);
		$scope.reservation = data;
	}

	$scope.setInitValue = function() {
		console.log("setInitValue");
		if ($scope.reservation.selected_lid != undefined) {
			for (i = 0; i < $scope.reservation.course.length; i++ ) {
				if ($scope.reservation.selected_lid == $scope.reservation.course[i].id){
					$scope.selectLesson($scope.reservation.course[i]);
					$scope.selectedLesson = $scope.reservation.course[i];
					break;
				}
			}
		} else {
			$scope.selectedLesson = $scope.reservation.course[0];
		}
	}
});

tellpinApp.controller("dtController", function( $scope, $http, $location, $window ) {
	$scope.goBack = function(){
//		console.log($scope.reservation.course.id);
		$scope.goURL("/tutor/reservation/which/");
	}

	$scope.goURL = function(url){
		$window.location.href = (url);
		$window.event.stopPropagation();
	}

	$scope.showData = function(){
		console.log($scope.reservation);
	};
	$scope.setOnlineBundleRowSpan = function(){
		if ($scope.reservation.duration) {
			$scope.duration = $scope.reservation.duration;	
		} else {
			$scope.duration = "60";
		}
		
		if ($scope.reservation.bundle) {
			$scope.bundle = $scope.reservation.bundle;	
		} else {
			$scope.bundle = "1";
		}
		

		$scope.pkg_30min_times = 1;
		$scope.pkg_60min_times = 1;
		$scope.pkg_90min_times = 1;

		if( $scope.reservation.course.pkg_30min_times != 0){
			$scope.pkg_30min_times++;
		}
		if( $scope.reservation.course.pkg_60min_times != 0){
			$scope.pkg_60min_times++;
		}
		if( $scope.reservation.course.pkg_90min_times != 0){
			$scope.pkg_90min_times++;
		}
	}

	$scope.selectBundle = function(duration, bundle, price){
//		$scope.onlineType = onlineType;
		$scope.duration = duration;
		$scope.bundle = bundle;
		$scope.selected_lesson_price = price;
		console.log($scope.selected_lesson_price);
	};

	$scope.submitDT = function(){
		console.log("submitDT");
//		$("#dt-form").append("<input type='hidden' name='online_type' value='"+$scope.onlineType+"'/>");
		$("#dt-form").append("<input type='hidden' name='duration' value='"+$scope.duration+"'/>");
		$("#dt-form").append("<input type='hidden' name='bundle' value='"+$scope.bundle+"'/>");
		$("#dt-form").submit();
	};

	$scope.initDT = function(data){
		console.log(data);
		$scope.reservation = data;
		$scope.selected_lesson_price = $scope.reservation.course.sg_60min_price;
		$scope.currency = $scope.reservation.user_currency[0];
		$scope.currency_user = $scope.reservation.user_currency[1];
	}
	
	$scope.clickContinue = function(){
		console.log("clickContinue");
		var trial = $scope.selected_lesson_price;
		var balance = 0;
		$http.post("/wallet/get_wallet_history_pop/").success( function( result ){
			if(result.isSuccess == 1){
				console.log(result);
				balance = result.balance_history.available_balance;
				$scope.coin_history = result.coin_history[0];
				$scope.balance_history = result.balance_history;
//				$scope.refresh_data();
				if( balance < trial ){
					//TODO 
					//show popup
					//$("#checkBalance").modal();
					console.log( balance );
					console.log( trial );
					$window.location.href = "/wallet/buy_credits/";
				}else{
					//console.log( balance );
					//console.log( trial );
					$scope.submitDT();
				}
			}
		});
	}

	$scope.openBuyCoinModal = function(){
		var coin = $("#id_coin").val();
		console.log(coin);
		
		$http.post("/wallet/buy_coin/", {coin: coin}).success( function( result ) {
			if(result.isSuccess == 1){
				
				//데이터 갱신
				$http.post("/wallet/get_wallet_history/").success( function( result ) {
					if(result.isSuccess == 1){
						console.log('refresh()');
						$scope.coin_history = result.coin_history[0].coin_list;
						$scope.refresh_data();
						$("#checkBalance").modal('hide');
					}
				});				
			}
		});
	}
	$scope.refresh_data = function(){
		var coin = $("#id_coin").val();
		$("#buy_coin").text(coin+" C");
		var prev = $scope.coin_history.available_balance;
		var after = $("#after_coin").text();
		console.log(coin+', '+prev+', '+after);
		$("#after_coin").text( parseInt(prev) + parseInt(coin) + " C");
		$("#purchase_amount").text( parseInt(coin)/10 + " USD" );
		$("#total_amount").text( parseInt(coin)/10.0 + 0.34 + " USD");
	}
	
});


tellpinApp.controller("howController", function( $scope, $http, $location, $window ) {

	$scope.initHow = function(data) {
		$scope.reservation = data;
		console.log($scope.reservation);
//		if ($scope.reservation.tool == undefined)
//			$scope.reservation.tool = "0";
		if ($scope.reservation.r_tool_info == undefined)
			$scope.reservation.r_tool_info = "";
		if ($scope.reservation.message == undefined)
			$scope.reservation.message = "";
		console.log(data);
		
	}
	$scope.goBack = function(){
		$scope.goURL("/tutor/reservation/dt/");
//		$http.post( "/tutor/reservation/dt_back/").success( function( response ) {
//			console.log(response.isSuccess);
//			if ( response.isSuccess == 1 ) {
//				$scope.goURL("/tutor/reservation/dt/");
//			}
//		});
	}

	$scope.submitHow = function(){
		var toolInput = $("#tool-info-input");
		if($(toolInput).val() == ""){
			alert("Please fill in the required field.");
			$(toolInput).focus();
			return;
		} 
		if($("#tools-id").val() == "0") {
			alert("Please select the required field.");
			$("#tools-id").focus();
			return;
		}
		console.log($scope.reservation);
		$("form:first").submit();
	}
});


tellpinApp.controller("confirmController", function( $scope, $http, $location, $window, $filter ) {
	$scope.initConfirm = function( data ) {
		$scope.reservation = data;
		console.log($scope.reservation);
	}
	$scope.showData = function(){
		console.log($scope.reservation);
	}
	$scope.submitConfirm = function(){
		if($("#password-input").val() == ""){
			alert("Please type in your password.");
			return;
		}
		$("#confirm-form").submit();
	};
	$scope.getTotalPrice = function(duration, bundle){

		if (duration == "30") {
			if (bundle == "1") {
				$scope.totalPrice = $scope.reservation.course.sg_30min_price;
			} else {
				$scope.totalPrice = $scope.reservation.course.pkg_30min_price * bundle;
			}
		} else if (duration == "60") {
			if (bundle == "1") {
				$scope.totalPrice = $scope.reservation.course.sg_60min_price;
			} else {
				$scope.totalPrice = $scope.reservation.course.pkg_60min_price * bundle;
			}
		} else {//90
			if (bundle == "1") {
				$scope.totalPrice = $scope.reservation.course.sg_90min_price;
			} else {
				$scope.totalPrice = $scope.reservation.course.pkg_90min_price * bundle;
			}
		}
	};
	$scope.goBack = function(){
		$scope.goURL("/tutor/reservation/when/");
	}

	$scope.goURL = function(url){
		$window.location.href = (url);
		$window.event.stopPropagation();
	}

	$scope.initRvTime = function(rv_time){
		var rvList = [];
		var rv = rv_time.split("$");
		var bundleSize = $scope.duration/30;
		
		for(var index=0; index < rv.length ; index++){
			var rvObj = new Object();
			rvObj.date = $filter('selectedTableFilter')(rv[index].slice(0,8));
			
			if(rv[index].slice(11,13) == "30"){
				rvObj.index = parseInt(rv[index].slice(9,11))*2 +1;
			}
			else{
				rvObj.index = parseInt(rv[index].slice(9,11))*2;
			}
			rvObj.startTime = setDisplayTime(rvObj.index);
			rvObj.endTime = setDisplayTime(rvObj.index+bundleSize);
			rvList.push(rvObj);
		}
		return rvList;
	}
	
	$scope.togglePH = function(flag, txt) {
		var ph;
		if(flag) {
			ph = txt;
		} else {
			ph = "";
		}
		return ph;
	}
});

tellpinApp.controller("completeController", function( $scope, $http, $location, $window ) {
	$scope.showData = function(){
		console.log($scope.accessType);
	}
	$scope.goNext = function(url){
		$window.location.href = url;
	}
});

tellpinApp.filter('capitalize', function() {
	  return function(arr) {
		  return arr.charAt(0).toUpperCase() + arr.slice(1,3);
	  };
});

//When we meet 예약 테이블
tellpinApp.controller("whenController", function( $scope, $http, $location, $window ) {
	$scope.initWhen = function(data) {
		$scope.reservation = data;
		console.log(data);
	}

	$scope.setReserveTime = function() {
		console.log("setReserveTime");
		if ($scope.rv_time != undefined) {
			
		}
	}
	$scope.goBack = function(){
		$scope.goURL("/tutor/reservation/how/");
	}

	$scope.goURL = function(url){
		$window.location.href = (url);
		$window.event.stopPropagation();
	}

	$scope.submitWhen = function(){
		$scope.$emit("getReservation");
	};
	$scope.$on("successReservation",function(event, data){
		$("#when-form").append("<input type='hidden' name='rv_time' value='"+data+"'/>");
		$("#when-form").submit();
	});
	$scope.$on("changeWeekList",function(event, data){
		$scope.reservation.schedule = data.schedule;
		$scope.reservation.reserved = data.reserved;
	});
});

tellpinApp.controller("selectedListController", function( $scope, $http, $location, $window ) {
	$scope.selectedList = [];
	$scope.$on("addSelectedList",function(event, data){
		var reservation = new Object();
		var dateIndex = -1;
		for(var idx = 0 ; idx < $scope.reservation.schedule.dow_list.length; idx++){
			if(data.day == dayToAlphabet($scope.reservation.schedule.dow_list[idx])){
				dateIndex = idx;
				break;
			}
		}
		reservation.date = $scope.reservation.schedule.week_list[dateIndex];
		reservation.day = data.day;
		reservation.index = data.index;
		reservation.duration = data.duration;
		
		var bundleSize = data.duration/30;
		
		reservation.startTime = setDisplayTime(data.index);
		reservation.endTime = setDisplayTime(data.index+bundleSize);
		
		for(var idx = 0 ; idx < bundleSize; idx++){
			if(idx == 0){
				reservation.value = data.value;
			}
			else{
				reservation["value"+idx] = data["value"+idx];
			}
			
		}
		$scope.selectedList.push(reservation);
		$scope.$emit("setSelectedList", $scope.selectedList);
	});
	
	$scope.$on("removeSelectedList",function(event, data){
		$scope.selectedList = data;
	});
	$scope.$on("changeWeekList",function(event, data){
		$scope.$emit("sendSelectedList", $scope.selectedList);
	});
	$scope.deleteReservation = function($index){
		data = $scope.selectedList[$index];
		$scope.$emit("deleteTime", data);
		$scope.selectedList.splice($index, 1);
	};
	$scope.$on("checkReservation",function(event){
		var rv_time = "";
		if($scope.selectedList.length > 0){
			for(var index = 0 ; index < $scope.selectedList.length; index++){
				if(($scope.selectedList.length-1) == index){
					rv_time += $scope.selectedList[index].date + $scope.selectedList[index].value;
				}
				else{
					rv_time += $scope.selectedList[index].date + $scope.selectedList[index].value+"$";
				}
			}
			$scope.$emit("setReservationData", rv_time);
		}
		else{
			alert("Please select reservation time");
			return;
		}
		
	});
});
tellpinApp.controller( "scheduleController", function( $scope, $http, $location, $window, $filter, $timeout ) {
	$scope.area = [];
	$scope.setWeekday = function(){
		$scope.weekList = [];
		for(var idx = 0; idx <7 ; idx++){
			$scope.weekList.push(dayToAlphabet($scope.schedule.dow_list[idx]));
		}
	}
	$scope.setReservedTime = function(){
		$scope.reservedTime = [];
		for(var idx = 0; idx < $scope.reserved.length ; idx++){
			var bundleSize = $scope.reserved[idx].duration / 30;
			var day = $scope.reserved[idx].rv_time.slice(0,1);
			var index = -1;
			if($scope.reserved[idx].rv_time.slice(3,5) == "30"){
				index = parseInt($scope.reserved[idx].rv_time.slice(1,3))*2 +1;
			}
			else{
				index = parseInt($scope.reserved[idx].rv_time.slice(1,3))*2;
			}
			for(var i = 0 ; i < bundleSize; i++){
				$scope.reservedTime.push(day+indexToValue(index+i));
			}
		}
	}
	$scope.setSelectTime = function() {
		console.log("== setSelectTime ==");
		console.log($scope.rv_time);
		$scope.selectTime = [];
		$scope.selectday = [];
		$scope.selectindex = [];
		$scope.selectvalue = [];
		
		var bundleArea = $scope.duration/30;

		if ($scope.rv_time != undefined) {
			$scope.selectTime = $scope.rv_time.split('$');
			console.log($scope.selectTime);
			for (var i = 0; i < $scope.selectTime.length; i++) {
				$scope.selectday[i] = $scope.selectTime[i].slice(8, 9);
				$scope.selectvalue[i] = $scope.selectTime[i].slice(8, 13);
				$scope.selectindex[i] = $scope.selectTime[i].slice(9, 13);

				if ($scope.selectindex[i].slice(2, 4) == "00") {
					$scope.selectindex[i] = parseInt($scope.selectindex[i].slice(0, 2)) * 2;
				} else {
					$scope.selectindex[i] = parseInt($scope.selectindex[i].slice(0, 2)) * 2 + 1;
				}

//				console.log($scope.selectindex);
//				console.log($scope.selectday);
//				console.log($scope.selectindex[i]);
				var data = {index: $scope.selectindex[i], day: $scope.selectday[i], duration: $scope.duration, state: 2}
				for(var idx = 0; idx < bundleArea; idx ++){
					if(idx == 0){
						data["value"] = $scope.selectvalue[i];
					}
					else{
						var value1timeindex= $scope.selectindex[i] + 1;
						var value1index = getIndexToTime(value1timeindex);
						if ($scope.selectvalue[i].slice(3, 5) == "00") {
							$scope.selectvalue[i+1] = $scope.selectvalue[i].substring(0,3) + "30"; //G0000 ~ G0030
						} else {
							$scope.selectvalue[i+1] = $scope.selectvalue[i].substring(0,1) + value1index; //G0030 ~ G0100
						}
						data["value"+idx] = $scope.selectvalue[i+1];
					}
//					$scope.area[$scope.selectindex[i]+idx][$scope.selectday[i]].state = 2;
				}
				$scope.dateString = $filter('selectedTableFilter')($scope.reservation.schedule.week_list[dayToIndex($scope.selectday[i])]) + " "
				+setDisplayTime($scope.selectindex[i])+"-"+setDisplayTime($scope.selectindex[i]+bundleArea);
				console.log($scope.dateString);
				$timeout(function() {
					$scope.$emit("addReservation", data);
					return 1;
				}, 100, true );
//				$scope.$emit("addReservation", data);
			}
		}
	}
	
	$scope.initAreaValue = function(day, index){
//		console.log("initAreaValue");
		if ($scope.accessType == 3) {//추가예약
			$scope.bundleCount = 1;
			$scope.bundle = 1;
		} else {
			$scope.bundleCount = $scope.bundle;
		}

		var time = day;
		if(index%2 == 0){
			if(index/2 < 10){
				time = time + "0" + index/2 + "00";
			}
			else{
				time = time + index/2 + "00";
			}
		}
		else{
			if(index/2 < 10){
				time = time + "0" + ((index-1)/2) + "30";
			}
			else{
				time = time + ((index-1)/2) + "30";
			}
		}
		return time;
	}
	$scope.initAreaState = function(day, index){
		var dow = {"mon":"A" , "tue":"B", "wed":"C", "thu":"D", "fri":"E", "sat":"F", "sun":"G"}
		$scope.setCurrentTime();
		if( $scope.reservation.today == $scope.schedule.mark_day ) {
			if (day == dow[$scope.schedule.dow_list[0]]) {
				if ( index < $scope.currentTime * 2 + parseInt($scope.currentMin / 30) + 1 ){
					if($scope.reservedTime.indexOf($scope.area[index][day].value) != -1){
						return 3;
					}
					return 0;
				}
			}
		}
		
		
		if($scope.reservedTime.indexOf($scope.area[index][day].value) != -1){
			return 3;
		}
		else if($scope.selectedList){
			var bundleSize = $scope.duration/30;
			for(var i=0; i < $scope.selectedList.length; i++){
				for(var idx = 0; idx < bundleSize ; idx++){
					if(idx == 0){
						if($scope.area[index][day].value == $scope.selectedList[i].value){
							if($scope.schedule.week_list.indexOf($scope.selectedList[i].date) != -1){
								return 2;
							}
						}
					}
					else{
						if($scope.area[index][day].value == $scope.selectedList[i]["value"+idx]){
							if($scope.schedule.week_list.indexOf($scope.selectedList[i].date) != -1){
								return 2;
							}
						}
					}
				}
			}
			if($scope.schedule.available_time.indexOf($scope.area[index][day].value) != -1){
				for(var i = 0 ; i<$scope.schedule.appointed.length; i++){
					if($scope.schedule.appointed[i].delta_time == $scope.area[index][day].value){
						if($scope.schedule.appointed[i].type == 2){
							return 0;
						}
					}
				}
				return 1;
			}
			else{
				for(var i = 0 ; i<$scope.schedule.appointed.length; i++){
					if($scope.schedule.appointed[i].delta_time == $scope.area[index][day].value){
						if($scope.schedule.appointed[i].type == 1){
							return 1;
						}
					}
				}
				return 0;
			}
		}
		else{
			if($scope.schedule.available_time.indexOf($scope.area[index][day].value) != -1){
				for(var i = 0 ; i<$scope.schedule.appointed.length; i++){
					if($scope.schedule.appointed[i].delta_time == $scope.area[index][day].value){
						if($scope.schedule.appointed[i].type == 2){
							return 0;
						}
					}
				}
				return 1;
			}
			else{
				for(var i = 0 ; i<$scope.schedule.appointed.length; i++){
					if($scope.schedule.appointed[i].delta_time == $scope.area[index][day].value){
						if($scope.schedule.appointed[i].type == 1){
							return 1;
						}
					}
				}
				return 0;
			}
		}
		
	};
// duration에 따라 예약가능한 영역인지 확인
	$scope.checkAvailableTime = function(index, day){
		var bundleArea = $scope.duration/30;
		var available = true;
		if(index+bundleArea <= 48){
			for(var idx = 0; idx < bundleArea; idx ++){
				if($scope.area[index+idx][day].state != 1){
					available = false;
				}
			}
		}
		else{
			available = false;
		}
		if(available){
			for(var idx = 0; idx < bundleArea; idx ++){
				$scope.area[index+idx][day].isHover = 1;
			}
		}
	};
	
	$scope.resetIsHover = function(index, day){
		var bundleArea = $scope.duration/30;
		var available = true;
		if(index+bundleArea <= 48){
			for(var idx = 0; idx < bundleArea; idx ++){
				if($scope.area[index+idx][day].isHover != 1){
					available = false;
				}
			}
		}
		else{
			available = false;
		}
		if(available){
			for(var idx = 0; idx < bundleArea; idx ++){
				$scope.area[index+idx][day].isHover = 0;
			}
		}
	};
	
	$scope.deleteReservationOnSchedule = function($index){
		data = $scope.selectedList[$index];
		$scope.$emit("deleteTime", data);
		$scope.selectedList.splice($index, 1);
	};
//	예약가능한 영역을 클릭할 때
	$scope.selectArea = function(index, day){
//		console.log("index = " + index);
//		console.log("day = " + day);
//		console.log("onoff = " + onoff);
//		console.log("bundle = " + $scope.bundle);

		var bundleArea = $scope.duration/30;
		console.log("bundleArea = " + bundleArea);
		var clickable = true;
		var onoff = false;
		var state = -1;
//		console.log("state = " + $scope.area[index][day].state);
//		console.log("selectedList_in = ");
//		console.log($scope.selectedList);

		for(var idx = 0; idx < bundleArea; idx ++) {
			if(idx == 0){
				if($scope.area[index+idx][day].state == 1){
					onoff=true;
					state = $scope.area[index+idx][day].state;
				}
				else if($scope.area[index+idx][day].state == 2){
					onoff=false;
					state = $scope.area[index+idx][day].state;
				}
				else{
					clickable = false;
					break;
				}
			}
			else{
				if(state != $scope.area[index+idx][day].state){
					clickable = false;
					break;
				}
			}
		}
		if(clickable){
			if(onoff){
				console.log("$scope.bundle = " + $scope.bundle);
//				if($scope.bundle > 0){
					var data = {index: index, day: day, duration: $scope.duration, state: 2}
					for(var idx = 0; idx < bundleArea; idx ++){
						if(idx == 0){
							data["value"] = $scope.area[index+idx][day].value;
						}
						else{
							data["value"+idx] = $scope.area[index+idx][day].value;
						}
						$scope.area[index+idx][day].state = 2;
					}
					$scope.bundle -= 1;
		//			for trial
					$scope.dateString = $filter('selectedTableFilter')($scope.reservation.schedule.week_list[dayToIndex(day)]) + " "
					+setDisplayTime(index)+"-"+setDisplayTime(index+bundleArea);
		//			-----------
					$scope.$emit("addReservation", data);
					if ($scope.bundle < 0) {
						if ($scope.bundleCount > 1) {
							$scope.deleteReservationOnSchedule($scope.bundleCount);
							alert("You cannot select more than "+ $scope.bundleCount +" lessons.");
						} else {
							$scope.deleteReservationOnSchedule(0);
						}
					}
//				}
//				else{
//					console.log("55555");
//					if ($scope.bundleCount > 1) {
//						alert("You cannot select more than "+ $scope.bundleCount +" lessons.");
//					}
//					else {
//						console.log("111");
//						alert("You cannot select more than "+ $scope.bundleCount +" lesson.");
//					}
//					return;
//				}
			}
			else{
				var data = {index: index, day: day, duration: $scope.duration, state: 1}
				for(var idx = 0; idx < $scope.selectedList.length; idx ++){
					var check = false;
					if($scope.selectedList[idx].value == $scope.area[index][day].value){
						check = true;
						if($scope.schedule.week_list.indexOf($scope.selectedList[idx].date) != -1){
							for(var i = 1; i < bundleArea; i++ ){
								if($scope.selectedList[idx]["value"+i] != $scope.area[index+i][day].value){
									check = false;
									break;
								}
							}
						}
						else{
							check = false;
						}
					}
					if(check){
						$scope.selectedList.splice(idx,1);
						for(var idx = 0; idx < bundleArea; idx ++){
							$scope.area[index+idx][day].state = 1;
						}
						$scope.bundle += 1;
			//			for trial
						$scope.dateString = "";
			//			-----------
						$scope.$emit("removeReservation",$scope.selectedList);
					}
				}
			}
		}
	};
	$scope.$on("syncSchedule",function(event, data){
		var bundleSize = data.duration/30;
		for(var idx=0; idx < bundleSize; idx++ ){
			if($scope.schedule.week_list.indexOf(data.date) != -1){
				$scope.area[data.index+idx][data.day].state = 1;
			}
		}
		$scope.bundle += 1;
	});
	$scope.getPrevWeek = function(){
		$http.post("/tutor/reservation/when/prevweek/", {date : $scope.schedule.mark_day}).success(function(response){
			$scope.schedule= response.schedule;
			$scope.reserved = response.reserved;
			$scope.setReservedTime();
			$scope.$emit("changeSchedule", response);
		});
	};
	$scope.getNextWeek = function(){
		$http.post("/tutor/reservation/when/nextweek/", {date : $scope.schedule.mark_day}).success(function(response){
			console.log(response);
			$scope.schedule= response.schedule;
			$scope.reserved = response.reserved;
			$scope.setReservedTime();
			$scope.$emit("changeSchedule", response);
		});
	};
	$scope.$on("getSelectedList",function(event, data){
		$scope.selectedList=data;
	});
	$scope.$on("updateSchedule",function(event, data){
		$scope.selectedList=data;
		$scope.initAllArea();
	});
	$scope.initAllArea = function(){
		for(var index=0; index < $scope.area.length; index++){
			$scope.area[index].A.state = $scope.initAreaState('A', index);
			$scope.area[index].B.state = $scope.initAreaState('B', index);
			$scope.area[index].C.state = $scope.initAreaState('C', index);
			$scope.area[index].D.state = $scope.initAreaState('D', index);
			$scope.area[index].E.state = $scope.initAreaState('E', index);
			$scope.area[index].F.state = $scope.initAreaState('F', index);
			$scope.area[index].G.state = $scope.initAreaState('G', index);
		}
	};
	$scope.showData = function(){
		console.log($scope.reservation);
	};
	
	$scope.initCurrentTime = function(utcTime) {
		$scope.userUTCTime = utcTime;
	}

	$scope.setCurrentTime = function(){
		var now = new Date();
		$scope.currentUTCTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
		$scope.changeTimezone();
		//$timeout($scope.setCurrentTime, 1000);
		//$scope.updateTimeTable();
	};
	
	$scope.changeTimezone = function(){
		if($scope.userUTCTime == ''){
			$scope.userUTCTime = minutesToUTC(-(new Date().getTimezoneOffset()));
		}
		var displayTime = new Date($scope.currentUTCTime.getTime() + utcToMilliseconds($scope.userUTCTime));
		$scope.currentTime = displayTime.format("HH");
		$scope.currentMin = displayTime.format("mm");
		
		//$scope.today = displayTime.getFullYear().toString() + (displayTime.getMonth()+1).toString() + (displayTime.getDate()+1).toString();
		$scope.displayTime = displayTime;
	}

});

