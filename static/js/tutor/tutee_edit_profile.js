tellpinApp.directive( 'chosen', function() {
	var linker = function( scope, element, attr ) {
		scope.$watch( 'profile.native', function() {
			element.trigger( 'chosen:updated' );
		});
		element.chosen( { max_selected_options : 3 } );
	};
	
	return {
		restrict: 'A',
		link: linker
	}
});

tellpinApp.controller("editController", function($scope, $http, $location, $window, $sce, $timeout){
	$scope.initProfileData = function(){
		$scope.nameInfo = false;
		$scope.setData();

	};

	$scope.setData = function() {
		$scope.loading = true;
		console.count("호출횟수");
		$http.post( "/user/ajax_editprofile/").success( function( result ) {
			var data = result;
			console.log(result);
			$scope.editMenu = 0;
			$scope.fromCity = [];
			$scope.livinginCity = [];
			$scope.userprofile = data.profile;
			$scope.tools = data.tools;
			$scope.currencyInfo = data.currencyInfo;
			for(var i = 1; i < $scope.currencyInfo.length; i++) {
				$scope.currencyInfo[i].code = $scope.currencyInfo[i].code + " " + $scope.currencyInfo[i].symbol; 
			}
			if ($scope.userprofile.currency_id == null) {
				$scope.userprofile.currency_id = 0;
			}
			$scope.nationsInfo = data.nationsInfo;
			$scope.languageInfo = data.languageInfo;
			$scope.skillLevelInfo = data.skillLevelInfo;
			$scope.timezoneInfo = data.timezoneInfo;
			$scope.setCurrentTime();

			if($scope.userprofile.from_country_id == null){
				$scope.userprofile.from_country_id = 0;
				$scope.userprofile.from_city_id = 0;
			} else {
				$scope.getCityInfo($scope.userprofile.from_country_id, "from");
			}
			if($scope.userprofile.livingin_country_id == null){
				$scope.userprofile.livingin_country_id = 0;
				$scope.userprofile.livingin_city_id = 0;
			} else {
				$scope.getCityInfo($scope.userprofile.livingin_country_id, "livingin");
			}
			if ($scope.userprofile.short_intro == null) {
				$scope.userprofile.short_intro = "";
			}
			if ($scope.userprofile.long_intro == null) {
				$scope.userprofile.long_intro = "";
			}
			// birthdate options
			$scope.yearList = [];
			for(var year = $scope.userprofile.server_year; year >= 1917; year--){
				$scope.yearList.push(year);
			}
			$scope.monthList = [];
			for(var month = 1; month <= 12; month++){
				$scope.monthList.push(month);
			}

			$scope.setDateList();
			
			// gender options
			$scope.genderList = ['Select', 'Male', 'Female'];
			if ($scope.userprofile.gender == null) {
				$scope.userprofile.gender = 0;
			}
			//language options
			$scope.languageList = ['English', '한국어', '日本語'];
			if ($scope.userprofile.display_language == null) {
				$scope.userprofile.display_language = 0;	
			}

			$scope.nationsInfo.splice(0, 0, {"id": 0, "country": "Select Country"});
			$scope.fromCity.splice(0, 0, {"id": 0,"city":"Select City"});
			$scope.livinginCity.splice(0, 0, {"id": 0,"city":"Select City"});

			//currenty
			$scope.currencyInfo.splice(0, 0, {"id": 0,"code":"Select currency"});
			$scope.languageInfo.splice(0, 0, {"id": 0,"lang_english":"Select language"});
			$scope.skillLevelInfo.splice(0,0, {"id": 0,"level":"Select Level"});
			$scope.skillLevelInfo.splice(7, 1);
			
			// language setting
			if($scope.userprofile.native1_id == null || $scope.userprofile.native1_id == 0){
				$scope.userprofile.native1_id = 0;
			}

			if($scope.userprofile.lang1_id == null || $scope.userprofile.lang1_id == 0){
				$scope.userprofile.lang1_id = 0;
				$scope.userprofile.lang1_level = 0;
			} else {
				if ($scope.userprofile.lang1_level == null) {
					$scope.userprofile.lang1_level = 0;
				}
			}

			$scope.userprofile.native = [];
			$scope.userprofile.native.push( $scope.userprofile.native1_id );
			if ( $scope.userprofile.native2_id != null ) {
				$scope.userprofile.native.push( $scope.userprofile.native2_id );
			}
			if ( $scope.userprofile.native3_id != null ) {
				$scope.userprofile.native.push( $scope.userprofile.native3_id );
			}

			$scope.checkPersonalTools();
			$scope.loading = false;
		}).error(function(result){
			console.log( 'erorr');
//			console.log(result);
		});
//		console.log("end");
	}

	$scope.getCityInfo = function(country_id, field) {
		$http.post("/common/city/", {country_id : country_id}).success( function( result ) {
			if(result.isSuccess == 1){
				if (field == "from") {
					$scope.fromCity = result.city;
					$scope.fromCity.splice(0, 0, {"id": 0,"city":"Select City"});
					$scope.showNationImage();
				} else if (field == "livingin") {
					$scope.livinginCity = result.city;
					$scope.livinginCity.splice(0, 0, {"id": 0,"city":"Select City"});
				} else {
					$scope.fromCity = result.city;
					$scope.fromCity.splice(0, 0, {"id": 0,"city":"Select City"});
					$scope.showNationImage();
				}
			}
		});
	}

	$scope.showNationImage = function(){
		$(".nations-image").css("display","block");
		if($scope.userprofile.from_field == 0){
			$scope.userprofile.from_city = 0;
		}
	}

	$scope.clickNameInfoPopup = function() {
		if ($scope.nameInfo == false) {
			$scope.nameInfo = true
		} else {
			$scope.nameInfo = false;
		}

		if( $scope.userprofile.level != 3 ){
			$(".name-info-popup").css("top", "260px");
		}
	}

	$scope.addNativeLanguage = function(nth){
		var fieldName = "native"+nth + "_id";
		var temp = nth - 1;
		var fieldName2 = "native" + temp;
		fieldName2 = fieldName2 + "_id"; 
		if ($scope.userprofile[fieldName2] == 0 || $scope.userprofile[fieldName2] == null) {
			alert("select Language");
			return;
		}
		$scope.userprofile[fieldName] = 0;
	}

	$scope.deleteNativeLanguage = function(nth){
		var fieldName = "native"+nth + "_id";
		if(nth == "2"){
			var nextFiledName = "native"+(nth + 1) + "_id";
			if($scope.userprofile[nextFiledName] != null){
				$scope.userprofile[fieldName] = $scope.userprofile[nextFiledName];
				$scope.userprofile[nextFiledName] = null;
			}
			else{
				$scope.userprofile[fieldName] = null;
			}
		}
		else{
			$scope.userprofile[fieldName] = null;
		}
	}

	$scope.getOldLanguage = function(field){
		$scope.oldValue = $scope.userprofile[field];
	}

	$scope.addLearningLanguage = function(nth){
		var fieldName = "lang"+nth + "_id";
		var temp = nth - 1;
		var fieldName2 = "lang" + temp + "_id";
		var level = "lang" + nth + "_level";
		var skill_level_2 = "lang" + temp + "_level";
		if ($scope.userprofile[fieldName2] == 0 || $scope.userprofile[fieldName2] == null) {
			alert("select Language");
			return;
		} else if ($scope.userprofile[skill_level_2] == 0 || $scope.userprofile[skill_level_2] == null) {
			alert("select level");
			return;
		}
		$scope.userprofile[fieldName] = 0;
		$scope.userprofile[level] = 0;
	}

	$scope.deleteLanguage = function(nth){
		console.log("deleteLanguage");
		console.log(nth);
		var fieldName = "lang" + nth + "_id";
		var level = "lang" + nth +"_level";
		if (nth == "6") {
			$scope.userprofile[fieldName] = null;
			$scope.userprofile[nextLevel] = null;
		} else {
			for (var i = 6; i > nth; i-- ) {
				var nextFiledName = "lang"+ i + "_id";
				var nextLevel = "lang" + i + "_level";
				console.log(nextFiledName);
				$scope.userprofile[fieldName] = $scope.userprofile[nextFiledName];
				$scope.userprofile[nextFiledName] = null;
				$scope.userprofile[level] = $scope.userprofile[nextLevel];
				$scope.userprofile[nextLevel] = null;
				
			}
		}
		console.log($scope.userprofile);
	}

	$scope.checkLanguageValidation = function(field){
		var fieldName = field;
		var res = true;
		console.log(field);
		if($scope.userprofile.native1_id == $scope.userprofile[fieldName]){
			if(fieldName != "native1_id"){
				res = false;
			}
		}
		if($scope.userprofile.native2_id == $scope.userprofile[fieldName]){
			if(fieldName != "native2_id"){
				res = false;
			}
		}
		if($scope.userprofile.native3_id == $scope.userprofile[fieldName]){
			if(fieldName != "native3_id"){
				res = false;
			}
		}
		if($scope.userprofile.lang1_id == $scope.userprofile[fieldName]){
			if(fieldName != "lang1_id"){
				res = false;
			}
		}
		if($scope.userprofile.lang2_id == $scope.userprofile[fieldName]){
			if(fieldName != "lang2_id"){
				res = false;
			}
		}
		if($scope.userprofile.lang3_id == $scope.userprofile[fieldName]){
			if(fieldName != "lang3_id"){
				res = false;
			}
		}
		if($scope.userprofile.lang4_id == $scope.userprofile[fieldName]){
			if(fieldName != "lang4_id"){
				res = false;
			}
		}
		if($scope.userprofile.lang5_id == $scope.userprofile[fieldName]){
			if(fieldName != "lang5_id"){
				res = false;
			}
		}
		if($scope.userprofile.lang6_id == $scope.userprofile[fieldName]){
			if(fieldName != "lang6_id"){
				res = false;
			}
		}
		if(!res){
			$scope.userprofile[fieldName] = 0;
			var temp = fieldName.split('_');
			$scope.userprofile[temp[0] + "_level"] = 0;
			alert("Language duplicated");
		}
		return res;
	}


	$scope.getOldTools = function($index){
		$scope.oldMessenger = $scope.userprofile.tools[$index].name;
	}

	$scope.checkToolsValidation = function($index){
		for(var idx=0; idx < $scope.userprofile.tools.length; idx ++){
			if($index != idx){
				if($scope.userprofile.tools[$index].name == $scope.userprofile.tools[idx].name){
					alert("Messenger duplicated");
					$scope.userprofile.tools[$index].name = $scope.oldMessenger;
				}
			}
		}
	}

	$scope.addTools = function(){
		var toolsObj = new Object();
		toolsObj.name = "Select tools";
		toolsObj.account = "";
		$scope.userprofile.tools.push(toolsObj);
	}

	$scope.deleteTools = function($index){
		$scope.userprofile.tools.splice($index,1);
	}

	$scope.checkPersonalTools = function(){
		console.log($scope.userprofile.tools.length);
		$scope.userprofile.tools = [];
		if($scope.userprofile.skype_id != null){
			var toolsObj = new Object();
			toolsObj.name = "Skype";
			toolsObj.account = $scope.userprofile.skype_id;
			$scope.userprofile.tools.push(toolsObj);
		}
		if($scope.userprofile.hangout_id != null){
			var toolsObj = new Object();
			toolsObj.name = "Hangout";
			toolsObj.account = $scope.userprofile.hangout_id;
			$scope.userprofile.tools.push(toolsObj);
		}
		if($scope.userprofile.facetime_id != null){
			var toolsObj = new Object();
			toolsObj.name = "Facetime";
			toolsObj.account = $scope.userprofile.facetime_id;
			$scope.userprofile.tools.push(toolsObj);
		}
		if($scope.userprofile.qq_id != null){
			var toolsObj = new Object();
			toolsObj.name = "QQ";
			toolsObj.account = $scope.userprofile.qq_id;
			$scope.userprofile.tools.push(toolsObj);
		}
		if($scope.userprofile.tools.length == 0) {
			var toolsObj = new Object();
			toolsObj.name = "Select tools";
			toolsObj.account = "";
			$scope.userprofile.tools.push(toolsObj);
		}
	}

//	$scope.showNative = function() {
//		if ( $scope.userprofile.native.length > 0 ) {
//			angular.element( '#profile-native-tooltip' ).css( 'display', 'none' );
//		}
//	}
	
	$scope.setCurrentTime = function(){ 
		var now = new Date();
		$scope.currentUTCTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
		$scope.changeTimezone();
		$timeout($scope.setCurrentTime, 1000);
	};
	
	$scope.changeTimezone = function(){
		if($scope.userprofile.timezone_id == null){
			var utc_time = minutesToUTC(-(new Date().getTimezoneOffset()));
			for(var idx = 0; idx < $scope.timezoneInfo.length; idx++){
				if(utc_time == $scope.timezoneInfo[idx].utc_time){
					$scope.userprofile.timezone_id = $scope.timezoneInfo[idx].id;
					break;
				}
			}
//			var displayTime = new Date().format("yyyy.MM.dd hh:mm a/p (UTC");
//			$scope.currentTime = displayTime + minutesToUTC(-(new Date().getTimezoneOffset())) + ")";
		}
//		else{
		var displayTime = new Date($scope.currentUTCTime.getTime() + utcToMilliseconds($scope.timezoneInfo[$scope.userprofile.timezone_id-1].utc_time));
		$scope.currentTime = displayTime.format("yyyy.MM.dd hh:mm a/p (UTC") + $scope.timezoneInfo[$scope.userprofile.timezone_id-1].utc_time + ")";
//		}
	}

	$scope.setDateList = function(){
		console.log("setDateList");
		$scope.dateList = [];
		if($scope.userprofile.birthdate.month == 02){
			if($scope.userprofile.birthdate.year == "" || $scope.userprofile.birthdate.year == null){
				for(var date = 1; date <= 29; date++){
					$scope.dateList.push(date);
				}
			}
			else{
				$http.post("/common/get_max_date/",{
					year: $scope.userprofile.birthdate.year,
			        month: $scope.userprofile.birthdate.month}).success(function(response){
			        	for(var date = 1; date <= response.lastDate; date++){
							$scope.dateList.push(date);
						}
				});
			}
		}
		else if($scope.userprofile.birthdate.month == "" || $scope.userprofile.birthdate.month == null || $scope.userprofile.birthdate.month == 1 || $scope.userprofile.birthdate.month == 3 ||
				$scope.userprofile.birthdate.month == 5 || $scope.userprofile.birthdate.month == 7 || $scope.userprofile.birthdate.month == 8 ||
				$scope.userprofile.birthdate.month == 10 || $scope.userprofile.birthdate.month == 12){
			for(var date = 1; date <= 31; date++){
				$scope.dateList.push(date);
			}
		}
		else{
			for(var date = 1; date <= 30; date++){
				$scope.dateList.push(date);
			}
		}
	};

	$scope.clickChangePhoto = function(){
		$("#profile-photo").click();
	};
	$scope.submitProfilePhoto = function(files){
		console.log(files)
		var fileName = new FormData();
		fileName.append("profile",files[0]);
		$http.post("/tutor/apply/upload_file/", fileName,{
			withCredentials: true,
	        headers: {'Content-Type': undefined },
	        transformRequest: angular.identity
		}).success(function(response){
			console.log(response);
			$scope.userprofile.photo_filepath = response.photo_filepath;
			$scope.userprofile.photo_filename = response.photo_filename;
		});
	};
	
	$scope.getOldLanguage = function(field){
		$scope.oldLanguage = $scope.userprofile[field];
	}

	$scope.checkLangValid = function() {
		console.log($scope.userprofile);
		return;
		if($scope.userprofile.native1_id == "" || $scope.userprofile.native1_id == null){
			alert("native1");
			return false;
		} else {
			$scope.userprofile.native1 =  $scope.languageInfo[$scope.userprofile.native1_id].lang_english;
		}
		if($scope.userprofile.native2_id != null) {
			$scope.userprofile.native2 =  $scope.languageInfo[$scope.userprofile.native2_id].lang_english;
		}
		if($scope.userprofile.native3_id != null) {
			$scope.userprofile.native3 =  $scope.languageInfo[$scope.userprofile.native3_id].lang_english;
		}
		if($scope.userprofile.lang1_id !=  null){
			$scope.userprofile.lang1 =  $scope.languageInfo[$scope.userprofile.lang1_id].lang_english;
			if($scope.userprofile.lang1_level == null || $scope.userprofile.lang1_level == 0){
				alert("lang1_id_skill_level");
				return false;
			}
		}
		if($scope.userprofile.lang2_id !=  null){
			$scope.userprofile.lang2 =  $scope.languageInfo[$scope.userprofile.lang2_id].lang_english;
			if($scope.userprofile.lang2_level == null || $scope.userprofile.lang2_level == 0){
				alert("lang2_id_skill_level");
				return false;
			}
		}
		if($scope.userprofile.lang3_id !=  null){
			$scope.userprofile.lang3 =  $scope.languageInfo[$scope.userprofile.lang3_id].lang_english;
			if($scope.userprofile.lang3_level == null || $scope.userprofile.lang3_level == 0){
				alert("lang3_id_skill_level");
				return false;
			}
		}
		if($scope.userprofile.lang4_id !=  null){
			$scope.userprofile.lang4 =  $scope.languageInfo[$scope.userprofile.lang4_id].lang_english;
			if($scope.userprofile.lang4_level == null){
				alert("lang4_id_skill_level");
				return false;
			}
		}
		if($scope.userprofile.lang5_id !=  null){
			$scope.userprofile.lang5 =  $scope.languageInfo[$scope.userprofile.lang5_id].lang_english;
			if($scope.userprofile.lang5_level == null){
				alert("lang5_id_skill_level");
				return false;
			}
		}
		if($scope.userprofile.lang6_id !=  null){
			$scope.userprofile.lang6 =  $scope.languageInfo[$scope.userprofile.lang6_id].lang_english;
			if($scope.userprofile.lang6_level == null){
				alert("lang6_id_skill_level");
				return false;
			}
		}
	}

	$(window).scroll(function(){
	    $.cookie("scroll", $(document).scrollTop() );
	});

	// 저장된 쿠키값 가져와서 스크롤위치 이동
	$(document).ready(function(){
	    if ( $.cookie("scroll") !== null) {
	         $(document).scrollTop( $.cookie("scroll") );
	    }
	});

	$scope.checkName = function(){
		console.log("checkName");
		var result = true;
		var name_regex = /^[가-힣a-zA-Z0-9\s]{3,50}$/;
		var nameObj = $("#name");
		
		if (!(name_regex.test(nameObj[0].value))) {
			$scope.showTooltip("profile-name-input", "profile-name-tooltip", "wrong name   !!!");
			$scope.scrollTop = $(".profile-name-input").offset().top;
			result = false;
		}

		if($scope.userprofile.name == "" ){
			$scope.showTooltip("profile-name-input", "profile-name-tooltip", "This cannot be empty.");
			$scope.scrollTop = $(".profile-name-input").offset().top;
			result = false;
		}
		return result;
	}

	$scope.saveProfile = function(){
		console.log("saveProfile");
		if($scope.checkName()){
			console.log( $scope.userprofile );
			$http.post("/tutor/saveprofile/",{profile: $scope.userprofile}).success(function(response){
				if(response.isSuccess == 1){
					$("#my-icon").attr("src",response.photo);
					$("#my-name").text(response.name);
//					alert("Success");
					$window.location.href = ("/settings/editprofile/");
				}
			});
		}
		else{
			//angular.element( '.profile-tooltip' ).css( 'display', 'none' );
			$(document).scrollTop($scope.scrollTop-200);
			return;
		}
	}

	$scope.saveLanguage = function(){
		console.log("saveLanguage");
		var result =  true;
		$scope.checkLangValid();
		console.log($scope.userprofile);
		if($scope.userprofile.native1_id == null){
			alert("Please select at least one Native language");
//			$scope.showTooltip("profile-native1-select", "profile-native-tooltip", "Please select at least one language");
//			$scope.scrollTop = $(".profile-native1-select").offset().top;
			result = false;
		}

		if($scope.userprofile.lang1_id == null){
			alert("Please select at least one Learning language");
//			$scope.showTooltip("profile-learning1-select", "profile-native-tooltip", "Please select at least one language");
//			$scope.scrollTop = $(".profile-native1-select").offset().top;
			result = false;
		}
		if(result){
			$http.post("/tutor/saveuserlanguage/",{profile: $scope.userprofile}).success(function(response){
				if(response.isSuccess == 1){
//					alert("Success");
					$window.location.href = ("/settings/editprofile/");
				}
			});
		}
		else{
			//angular.element( '.profile-tooltip' ).css( 'display', 'none' );
			$(document).scrollTop($scope.scrollTop-200);
			return;
		}
	}

	$scope.checkUserIntro = function() {
		console.log("checkUserIntro");
		console.log($scope.userprofile.short_intro);
		if ( $scope.userprofile.short_intro.length < 1 ) {
			$scope.showTooltip("userprofile-short-intro", "profile-short-tooltip", "This field cannot be empty.");
			$scope.scrollTop = $(".userprofile-short-intro").offset().top;
			return false;
		}
		if ( $scope.userprofile.long_intro.length < 1 ) {
			$scope.showTooltip("userprofile-long-intro", "profile-long-tooltip", "This field be empty.");
			$scope.scrollTop = $(".userprofile-long-intro").offset().top;
			return false;
		}
		return true;
	}

	$scope.saveUserIntro = function(){
		console.log("saveUserIntro");
		if($scope.checkUserIntro()){
			console.log( $scope.userprofile );
			$scope.userprofile.user_shortIntro = $scope.userprofile.short_intro;
			$scope.userprofile.user_longIntro = $scope.userprofile.long_intro;

			$http.post("/tutor/saveuserintro/",{profile: $scope.userprofile}).success(function(response){
				if(response.isSuccess == 1){
//					alert("Success");
					$window.location.href = ("/settings/editprofile/");
				}
			});
		}
		else{
			//angular.element( '.profile-tooltip' ).css( 'display', 'none' );
			$(document).scrollTop($scope.scrollTop-200);
			return;
		}
	}

	$scope.checkTools = function() {
		console.log("checkTools");
		console.log($scope.userprofile);
		var count = 0;
		for(var idx=0; idx < $scope.userprofile.tools.length; idx ++){
			if($scope.userprofile.tools[idx].account != "" && $scope.userprofile.tools[idx].account != null && $scope.userprofile.tools[idx].name != -1){
				count++;
			}
			else{
				if($scope.userprofile.tools[idx].name == -1){
					alert("Select messenger");
					return false;
				}
				else if($scope.userprofile.tools[idx].account == ""){
					alert("input messenger account");
					return false;
				}
			}
		}
		for(var idx=0; idx < $scope.userprofile.tools.length; idx ++){
			if($scope.userprofile.tools[idx].name != -1 && $scope.userprofile.tools[idx].name != null){
				if($scope.userprofile.tools[idx].account == null || $scope.userprofile.tools[idx].account == ""){
					alert("Enter a messenger ID");
					return false;
				}
			}
		}
		return true;
	}

	$scope.saveTools = function(){
		console.log("saveTools");
		console.log($scope.userprofile);
		if($scope.checkTools()){
			$http.post("/tutor/savetools/",{profile: $scope.userprofile}).success(function(response){
				if(response.isSuccess == 1){
//					alert("Success");
					$window.location.href = ("/settings/editprofile/");
				}
			});
		}else{
			//angular.element( '.profile-tooltip' ).css( 'display', 'none' );
//			$(document).scrollTop($scope.scrollTop-200);
			return;
		}
	}

//	$scope.nameCheck = function( name) {
//		var blank_pattern = /^\s+|\s+$/g;
//		if ( name.replace( blank_pattern, '' ) == "" ) {
//			return false;
//		}
//		/*
//		var blank_pattern = /[\s]/g;
//		if ( blank_pattern.test( name ) == true ) {
//			return false;
//		}*/
//		
//		var specialChars = /[`~!@#$%^&*|\\\'\|;:\/?{}()]/gi;
////		console.log( specialChars.test( name ) );
//		if ( specialChars.test( name ) == true ) {
//			return false;
//		}
//		return true;
//	}

//	$scope.checkAll = function(){
//		console.log("checkAll");
//		var result = true;
//
//		var name_regex = /^[가-힣a-zA-Z0-9\s]{3,50}$/;
//		var nameObj = $("#name");
//		
//		if (!(name_regex.test(nameObj[0].value))) {
//			$scope.showTooltip("profile-name-input", "profile-name-tooltip", "wrong name   !!!");
//			$scope.scrollTop = $(".profile-name-input").offset().top;
//			return false;
//		}
//
//		if($scope.userprofile.name == "" ){
//			$scope.showTooltip("profile-name-input", "profile-name-tooltip", "This cannot be empty.");
//			$scope.scrollTop = $(".profile-name-input").offset().top;
//			return false;
//		}
//
//		if($scope.userprofile.from_city == null || $scope.userprofile.from_field== ""){
//			alert("from");
//			return false;
//		}
//		if($scope.userprofile.livein == "" || $scope.userprofile.livein_city == null){
//			alert("livein");
//			return false;
//		}
//		if($scope.userprofile.gender == "" || $scope.userprofile.gender== null){
//			alert("gender");
//			return false;
//		}
//		if($scope.userprofile.currency == null){
//			alert("currency");
//			return false;
//		}
//		if($scope.userprofile.birthdate.year != null){
//			if($scope.userprofile.birthdate.month == null || $scope.userprofile.birthdate.day == null){
//				if($scope.userprofile.birthdate.month == null){
//					if($scope.scrollTop == -1){
//						$scope.scrollTop = $(".profile-year-select").offset().top;
//					}
//					$scope.showTooltip("profile-month-select", "profile-month-tooltip", "This cannot be empty.");
//					result = false;
//				}
//				if($scope.userprofile.birthdate.day == null){
//					if($scope.scrollTop == -1){
//						$scope.scrollTop = $(".profile-year-select").offset().top;
//					}
//					$scope.showTooltip("profile-day-select", "profile-day-tooltip", "This cannot be empty.");
//					result = false;
//				}
//			}
//		} else {
//			alert("year");
//			return false;
//		}
//		if($scope.userprofile.birthdate.month != null){
//			if($scope.userprofile.birthdate.year == null){
//				if($scope.scrollTop == -1){
//					$scope.scrollTop = $(".profile-year-select").offset().top;
//				}
////				$scope.showTooltip("profile-year-select", "profile-year-tooltip", "This cannot be empty.");
//				result = false;
//			}
//			if($scope.userprofile.birthdate.day == null){
//				if($scope.scrollTop == -1){
//					$scope.scrollTop = $(".profile-year-select").offset().top;
//				}
////				$scope.showTooltip("profile-day-select", "profile-day-tooltip", "This cannot be empty.");
//				result = false;
//			}
//		} else {
//			alert("month");
//			return false;
//		}
//		if($scope.userprofile.birthdate.day != null){
//			if($scope.userprofile.birthdate.year == null){
//				if($scope.scrollTop == -1){
//					$scope.scrollTop = $(".profile-year-select").offset().top;
//				}
////				$scope.showTooltip("profile-year-select", "profile-year-tooltip", "This cannot be empty.");
//				result = false;
//			}
//			if($scope.userprofile.birthdate.month == null){
//				if($scope.scrollTop == -1){
//					$scope.scrollTop = $(".profile-year-select").offset().top;
//				}
////				$scope.showTooltip("profile-month-select", "profile-month-tooltip", "This cannot be empty.");
//				result = false;
//			}
//		} else {
//			alert("day");
//			return false;
//		}
//
////
////			$scope.showTooltip("profile-native1-select", "profile-native-tooltip", "Please select at least one language");
////			$scope.scrollTop = $(".profile-native1-select").offset().top;
////			result = false;
////		}
////		if ( $scope.userprofile.short_intro.length < 1 ) {
////			$scope.showTooltip("profile-short-intro", "profile-short-tooltip", "This field cannot be empty.");
////			$scope.scrollTop = $(".profile-short-intro").offset().top;
////			result = false;
////		}
////		if ( $scope.userprofile.long_intro.length < 1 ) {
////			$scope.showTooltip("profile-long-intro", "profile-long-tooltip", "This field be empty.");
////			$scope.scrollTop = $(".profile-long-intro").offset().top;
////			result = false;
////		}
//		if(result){
//			return true;
//		}
//		else{
//			return false;
//		}
//	}

	$scope.showData = function(){
		console.log($scope.userprofile);
	}

	$scope.showTooltip = function(element, tooltip, message){
		var top = $("."+element).offset().top + $("."+element).outerHeight() + 3 + "px";
		var left = $("."+element).offset().left + "px";
		$("#"+tooltip).text(message);
		$("#"+tooltip).css({"top": top, "left": left});
		$("#"+tooltip).css("display","block");
	}
	$scope.hideTooltip = function(tooltip){
		$("#"+tooltip).css("display","none");
	}
});

function minutesToUTC(minute){
	var UTCString = "";
	if(minute<0){
		UTCString += "-";
	}
	else{
		UTCString += "+";
	}
	if(Math.abs(minute)/60 > 10){
		UTCString += Math.abs(minute)/60 + ":";
	}
	else{
		UTCString += "0"+ Math.abs(minute)/60 + ":";
	}
	if(Math.abs(minute) % 60 > 10){
		UTCString += Math.abs(minute) % 60;
	}
	else{
		UTCString += "0" + Math.abs(minute) % 60;
	}
	return UTCString;
}
function utcToMilliseconds(utc){
	var ms = ((parseInt(utc.slice(1,3)) * 60) + (parseInt(utc.slice(4,6)) * 60)) * 60000;
	if(utc.slice(0,1) == "-"){
		ms = -(ms);
	}
	return ms;
}
function hideNationImage(){
	$(".nations-image").css("display","none");
}
//********* For DateFormat **********
String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};
Date.prototype.format = function(f) {
    if (!this.valueOf()) return " ";
 
    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;
     
    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).zf(2);
            case "MM": return (d.getMonth() + 1).zf(2);
            case "dd": return d.getDate().zf(2);
            case "E": return weekName[d.getDay()];
            case "HH": return d.getHours().zf(2);
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case "mm": return d.getMinutes().zf(2);
            case "ss": return d.getSeconds().zf(2);
            case "a/p": return d.getHours() < 12 ? "AM" : "PM";
            default: return $1;
        }
    });
};

// *****************************************