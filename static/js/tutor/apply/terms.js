var user_id = "";
tellpinApp.controller( "navigationCtrl", [ '$scope', function( $scope ) {
	$scope.accessType = 0;
	angular.element(document).ready(function () {
		if($scope.accessType == '0'){
			$("#tr-navigation div").css({"width":"calc(88% / 5)", "margin-right":"2%"});
			
		}
		else{
			$("#tr-navigation div").css({"width":"23%", "margin-right":"2%"});
		}
    });
}]);

tellpinApp.controller( "termsCtrl", [ '$scope', '$http', '$window', function( $scope, $http, $window ) {
//	$scope.urls = {
//		"personal": "/tutor/apply",
//		"profile": "tutor/apply/profile",
//		"payment": "tutor/apply/payment",
//		"terms": "tutor/apply/terms",
//		"complete": "tutor/apply/complete",
//	};
	
	$scope.strings = {};
	$scope.strings.labels = {
		"terms": 'Terms sample\n' +
				'\n1. Terms Applicable to Specific Content and Areas of the Site\n' +
				'Some areas of the Site or Content provided on or through the Site may have additional rules, guidelines, license agreements, user agreements or other terms and conditions that apply to your access or use of that area of the Site or Content (including terms and conditions applicable to a corporation or other organization and its users). If there is a conflict or inconsistency between these Terms of Use and the rules, guidelines, license agreement, user agreement or other terms and conditions for a specific area of the Site or for specific Content, the latter shall have precedence with respect to your access and use of that area of the Site or Content.' +
				'\n\n2. Use of Software\n' +
				'Your use of Software is subject to all agreements such as a license agreement or user agreement that accompanies or is included with the Software, ordering documents, exhibits, and other terms and conditions that apply ("License Terms"). In the event that Software is provided on or through the Site and is not licensed for your use through License Terms specific to the Software, you may use the Software subject to the following: (a) the Software may be used solely for your personal, informational, noncommercial purposes; (b) the Software may not be modified or altered in any way; and (c) the Software may not be redistributed.' +
				'\n\n3. Use of Materials\n' +
				'You may download, store, display on your computer, view, listen to, play and print Materials that Oracle publishes or broadcasts on the Site or makes available for download through the Site subject to the following: (a) the Materials may be used solely for your personal, informational, noncommercial purposes; (b) the Materials may not be modified or altered in any way; and (c) the Materials may not be redistributed.' +
				'\n\n4. Use of Community Services\n' +
				'Community Services are provided as a convenience to users and Oracle is not obligated to provide any technical support for, or participate in, Community Services. While Community Services may include information regarding Oracle products and services, including information from Oracle employees, they are not an official customer support channel for Oracle.' +
				'You may use Community Services subject to the following: (a) Community Services may be used solely for your personal, informational, noncommercial purposes; (b) Content provided on or through Community Services may not be redistributed; and (c) personal data about other users may not be stored or collected except where expressly authorized by Oracle.' +
				'\n\n5. Reservation of Rights\n' +
				'The Site and Content provided on or through the Site are the intellectual property and copyrighted works of Oracle or a third party provider. All rights, title and interest not expressly granted with respect to the Site and Content provided on or through the Site are reserved. All Content is provided on an "As Is" and "As Available" basis, and Oracle reserves the right to terminate the permissions granted to you in Sections 2, 3 and 4 above and your use of the Content at any time.',
		"back": "Back",
	};
	
	$scope.go_back = function ( ) {
		console.log("Go back");
		//$window.history.back();
		$scope.goURL("/tutor/apply/payment/")
	};
	$scope.goURL = function(url){
		$window.location.href = (url);
		$window.event.stopPropagation();
	}
	
	$scope.checkSubmit = function ( ) {
		if(!$scope.termCheck) {
			alert("To continue applying, you must agree to the Terms & Conditions and privacy policy.");
			return;
		}
		
		$http.post("/tutor/apply/submit/" ).success(function(response) {
			if ( response.isSuccess == 1) {
				//alert(response.message);
				$("#terms-form").submit();
			}
		});
		
		//$("#terms-form").submit();
	}
	
	$scope.openPrivacyModal = function() {
		console.log("openPrivacyModal");
		angular.element( '#privacyModal' ).modal();
	}
	
	$scope.submitPrivacyModal = function() {
		console.log("submitPrivacyModal");
		angular.element( '#privacyModal' ).modal("hide");
	}
//	$scope.makeInitial = function( ) {
//		$window.location.reload();
//	};
	
}]);