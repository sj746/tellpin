var user_id = "";
tellpinApp.controller( "navigationCtrl", [ '$scope', function( $scope ) {
	$scope.accessType = 0;
	angular.element(document).ready(function () {
		if($scope.accessType == '0'){
			$("#tr-navigation div").css({"width":"calc(88% / 5)", "margin-right":"2%"});
			
		}
		else{
			$("#tr-navigation div").css({"width":"23%", "margin-right":"2%"});
		}
    });
}]);

tellpinApp.controller( "paymentCtrl", [ '$scope', '$http', '$window', function( $scope, $http, $window ) {
//	$scope.urls = {
//		"personal": "/tutor/apply",
//		"profile": "tutor/apply/profile",
//		"payment": "tutor/apply/payment",
//		"terms": "tutor/apply/terms",
//		"complete": "tutor/apply/complete",
//	};
	
//	$scope.strings = {};
//	$scope.strings.labels = {
//		"payment_account": "Payment Account",
////		"discription": "If you want to withdraw coins into money, you must fill this information out.\n" +
////						"This information must correspond with your payment information with the payment partner (Paypal, Alipay, etc).\n" +
////						"Otherwise you will encounter problems when withdrawing funds."
////						,
//		"street_address": "Street Address",
//		"city": "City",
//		"state_province_region": "State / Province / Region",
//		"zip_postal_code": "Zip / Postal code",
//		"country_region": "Country / Region",
//		"phone_num": "Phone number",
//		"email_addr": "Email Address",
//		"back": "Back",
//		"continue": "Continue",
//		"first_name": "First Name",
//		"last_name": "Last Name",
//	};

	
	$scope.init = function() {
		$scope.country_list = $scope.data.country;
		console.log($scope.data);
		$scope.country_list.splice(0, 0, {id: -1, country: "Select One"});
		if( $scope.data.payment_info != undefined && $scope.data.payment_info != null) {
			$scope.formdata = [];
			$scope.formdata.first_name = $scope.data.payment_info.first_name;
			$scope.formdata.last_name = $scope.data.payment_info.last_name;
			$scope.formdata.street_address = $scope.data.payment_info.street_address;
			$scope.formdata.city = $scope.data.payment_info.city;
			$scope.formdata.state_province_region = $scope.data.payment_info.state_province_region;
			$scope.formdata.zip_code = $scope.data.payment_info.zip_code;
			$scope.formdata.country_region = $scope.data.payment_info.country_region;
			$scope.formdata.phone = $scope.data.payment_info.phone;
			$scope.formdata.email = $scope.data.payment_info.email;
		} else {
			$scope.formdata = [];
			$scope.formdata.street_address = "";
			$scope.formdata.city = "";
			$scope.formdata.state_province_region = "";
			$scope.formdata.zip_code = "";
			$scope.formdata.country_region = -1;
			$scope.formdata.phone = "";
			$scope.formdata.email = "";
		}
	}
	

	
	// 처음 해당 유저의 Setting 정보 가져오기
	/*
	$http.post( "tutor/apply/getPaymentInfo/" ).success( function( result ) {
		if ( result.isSuccess == 1 ) {
			var payment = result.payment;
			var tutor = result.tutor;
			$scope.country_list = result.country;
//			console.log(result);
//			console.log($scope.country_list);

			$scope.formdata.name = tutor.name;
			$scope.formdata.first_name = payment.first_name;
			$scope.formdata.last_name = payment.last_name;
			$scope.formdata.street_address = payment.street_address;
			$scope.formdata.city = tutor.city_name;
			$scope.formdata.state_province_region = payment.state_province_region;
			$scope.formdata.zip_code = payment.zip_code;
			$scope.formdata.country = tutor.livein_country;
			$scope.formdata.phone = payment.phone;
			$scope.formdata.email = payment.email;
						
//			console.log($scope.formdata);
		}
	});
	*/
	
	$scope.go_back = function ( ) {
		console.log("go back");
		//$window.history.back();
		$scope.goURL("/tutor/apply/profile/")
	};
	$scope.goURL = function(url){
		$window.location.href = (url);
		$window.event.stopPropagation();
	}

	$scope.checkValidationData = function ( ) {
		var formElements = document.getElementById("payment-form").elements;
//		var email_regex = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
		var email_regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/; 
		var phone_regex = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?[0-9]{3,4}-?[0-9]{4}$/;

		var phoneObj = $("#phone");

		var emailObj = $("#email");

		for (var index = 0; index < formElements.length; index++) {
			if(formElements[index].checkValidity() == false) {
				alert("Please fill in the required field.");
//			    alert(formElements[index].validationMessage);
			    formElements[index].focus();
			    return false;
			}
		}

		//check phone number pattern
		if (!(phone_regex.test(phoneObj[0].value))) {
			alert("wrong phone number!!!");
			phoneObj.focus();
			return false;
		}

		//check email pattern
		if (!(email_regex.test(emailObj[0].value))) {
			alert("wrong Email address!!!");
			emailObj.focus();
			return false;
		}

		// Check select country field validation
		var countryObj = $("#country");
		if ( isNaN(parseInt(countryObj.val())) ) {
			alert("Please fill in the required country field.");
			countryObj.focus();
			return false;
		}
	    return true;
	};
	
	$scope.checkContinue = function ( ) {
		if ( $scope.checkValidationData() ) {
			objData = {};
			objData.first_name = $scope.formdata.first_name;
			objData.last_name = $scope.formdata.last_name;
			objData.street_address = $scope.formdata.street_address;
			objData.city = $scope.formdata.city;
			objData.state_province_region = $scope.formdata.state_province_region;
			objData.zip_code = $scope.formdata.zip_code;
			objData.country_region = $scope.formdata.country_region;
			objData.phone = $scope.formdata.phone;
			objData.email = $scope.formdata.email;
			
			$http.post("/tutor/apply/payment/save", objData ).success(function(response) {
				if ( response.isSuccess == 1) {
					//alert(response.message);
					$("#payment-form").submit();
				}
			});
			
			//$("#payment-form").submit();
		}
	};
	
//	$scope.makeInitial = function() {
//		$window.location.reload();
//	};
	
}]);