var user_id = "";
tellpinApp.controller( "navigationCtrl", [ '$scope', function( $scope ) {
	$scope.accessType = 0;
	angular.element(document).ready(function () {
		if($scope.accessType == '0'){
			$("#tr-navigation div").css({"width":"calc(90% / 5)", "margin-right":"2%"});
			
		}
		else{
			$("#tr-navigation div").css({"width":"23%", "margin-right":"2%"});
		}
    });
}]);

tellpinApp.controller( "personalCtrl", [ '$scope', '$http', '$window', '$timeout', function( $scope, $http, $window, $timeout) {
	$scope.teachable = [];
	$scope.init = function(data){
		$scope.registration = data;
		$scope.registration.fromCity = [];
		$scope.registration.livinginCity = [];
		$scope.registration.personal.tools = [];
		console.log($scope.registration);
		$scope.showData();
		$scope.addTeachingLanguage();
	}
	$scope.showData = function(){
//		console.log("======= showData =======");
		if($scope.registration.personal.online == 0 || $scope.registration.personal.online == null) {
			$scope.registration.personal.online = 1;
			$scope.registration.personal.audio = 0;
			$scope.checkPersonalTools();
		}
		$scope.registration.nations.splice(0, 0, {"id": 0, "country": "Select Country"});
		$scope.registration.languages.splice(0, 0, {"id": 0,"lang_english":"Select language"});
		$scope.registration.fromCity.splice(0, 0, {"id": 0,"city":"Select City"});
		$scope.registration.livinginCity.splice(0, 0, {"id": 0,"city":"Select City"});
		$scope.registration.levels.splice(0, 0, {"id": 0,"level":"Select Level"});
		$scope.registration.levels.splice(7, 1);
		
		if ($scope.registration.personal.photo_filepath == null) {
			$scope.registration.personal.photo_filepath = "/static/img/upload/profile";
		}

		$scope.setCurrentTime();

		if ($scope.registration.personal.from_country_id == null) {
			$scope.registration.personal.from_country_id = 0;
		} else {
			$scope.getCityInfo($scope.registration.personal.from_country_id, "from");
		}

		if ($scope.registration.personal.from_city_id == null) {
			$scope.registration.personal.from_city_id = 0;
		}
		
		if ($scope.registration.personal.livingin_country_id == null) {
			$scope.registration.personal.livingin_country_id = 0;
		} else {
			$scope.getCityInfo($scope.registration.personal.livingin_country_id, "livingin");
		}
		
		if ($scope.registration.personal.livingin_city_id == null) {
			$scope.registration.personal.livingin_city_id = 0;
		}
		
		if ($scope.registration.personal.lang1_id == null) {
			$scope.registration.personal.lang1_id = 0;
		}
		
		if ($scope.registration.personal.lang1_level == null) {
			$scope.registration.personal.lang1_level = 0;
		}
	}

	$scope.getCityInfo = function(country_id, field) {
		$http.post("/common/city/", {country_id : country_id}).success( function( result ) {
			if(result.isSuccess == 1){
				if (field == "from") {
					$scope.registration.fromCity = result.city;
					$scope.registration.fromCity.splice(0, 0, {"id": 0,"city":"Select City"});
				} else if (field == "livingin") {
					$scope.registration.livinginCity = result.city;
					$scope.registration.livinginCity.splice(0, 0, {"id": 0,"city":"Select City"});
				} else {
					$scope.registration.fromCity = result.city;
					$scope.registration.fromCity.splice(0, 0, {"id": 0,"city":"Select City"});
				}
			}
		});
	}

	$scope.setCurrentTime = function(){ 
		var now = new Date();
		$scope.currentUTCTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
		$scope.changeTimezone();
		$timeout($scope.setCurrentTime, 1000);
	};
	
	$scope.changeTimezone = function(){
		if($scope.registration.personal.timezone_id == null){
			var utc_time = minutesToUTC(-(new Date().getTimezoneOffset()));
			for(var idx = 0; idx < $scope.registration.timezoneInfo.length; idx++){
				if(utc_time == $scope.registration.timezoneInfo[idx].utc_time){
					$scope.registration.personal.timezone_id = $scope.registration.timezoneInfo[idx].id;
					break;
				}
			}
		}
		var displayTime = new Date($scope.currentUTCTime.getTime() + utcToMilliseconds($scope.registration.timezoneInfo[$scope.registration.personal.timezone_id-1].utc_time));
		$scope.currentTime = displayTime.format("yyyy.MM.dd hh:mm a/p (UTC") + $scope.registration.timezoneInfo[$scope.registration.personal.timezone_id-1].utc_time + ")";
	}

	$scope.addNativeLanguage = function(nth){
		var fieldName = "native"+nth + "_id";
		var temp = nth - 1;
		var fieldName2 = "native" + temp;
		fieldName2 = fieldName2 + "_id"; 
		if ($scope.registration.personal[fieldName2] == 0 || $scope.registration.personal[fieldName2] == null) {
			alert("select Language");
			return;
		}
		$scope.registration.personal[fieldName] = 0;
	}

	$scope.deleteNativeLanguage = function(nth){
		var fieldName = "native"+nth + "_id";
		if(nth == "2"){
			var nextFiledName = "native"+(nth + 1) + "_id";
			if($scope.registration.personal[nextFiledName] != null){
				$scope.registration.personal[fieldName] = $scope.registration.personal[nextFiledName];
				$scope.registration.personal[nextFiledName] = null;
			}
			else{
				$scope.registration.personal[fieldName] = null;
			}
		}
		else{
			$scope.registration.personal[fieldName] = null;
		}
		$scope.addTeachingLanguage();
	}

	$scope.setDefaultAudio = function(){
		if($scope.registration.personal.audio == null){
			$scope.registration.personal.audio = 0;
		}
	}
	
	$scope.addLearningLanguage = function(nth){
		var fieldName = "lang"+nth + "_id";
		var temp = nth - 1;
		var fieldName2 = "lang" + temp + "_id";
		var level = "lang" + nth + "_level";
		var skill_level_2 = "lang" + temp + "_level";
		if ($scope.registration.personal[fieldName2] == 0 || $scope.registration.personal[fieldName2] == null) {
			alert("select Language");
			return;
		} else if ($scope.registration.personal[skill_level_2] == 0 || $scope.registration.personal[skill_level_2] == null) {
			alert("select level");
			return;
		}
		$scope.registration.personal[fieldName] = 0;
		$scope.registration.personal[level] = 0;
	}

	$scope.deleteLearningLanguage = function(nth){
		var fieldName = "lang" + nth + "_id";
		var level = "lang" + nth +"_level";
		if (nth == "6") {
			$scope.registration.personal[fieldName] = null;
			$scope.registration.personal[nextLevel] = null;
		} else {
			for (var i = 6; i > nth; i-- ) {
				var nextFiledName = "lang"+ i + "_id";
				var nextLevel = "lang" + i + "_level";
				console.log(nextFiledName);
				$scope.registration.personal[fieldName] = $scope.registration.personal[nextFiledName];
				$scope.registration.personal[nextFiledName] = null;
				$scope.registration.personal[level] = $scope.registration.personal[nextLevel];
				$scope.registration.personal[nextLevel] = null;
				
			}

		}
		$scope.addTeachingLanguage();
		console.log($scope.registration.personal);
	}

	$scope.addTeachingLanguage = function(){
		$scope.teachable = [];
//		console.log($scope.teachable);

		for(var nth=1; nth <= 3; nth++){
			var fieldName = "native"+ nth + "_id";
			if($scope.registration.personal[fieldName] != null && $scope.registration.personal[fieldName] != 0 && $scope.registration.personal[fieldName] != -1){
				var obj = new Object();
				obj.id = $scope.registration.personal[fieldName];
				if (obj.id != 0) {
					for(var index in $scope.registration.languages){
						if($scope.registration.languages[index].id == obj.id){
							obj.lang_english = $scope.registration.languages[index].lang_english;
						}
					}
//					obj.level = 7;
					if($scope.registration.personal.teaching1_id == obj.id ||  $scope.registration.personal.teaching2_id == obj.id|| $scope.registration.personal.teaching3_id == obj.id){
						obj.state = 1
					}
					else{
						obj.state = 0
					}
					$scope.teachable.push(obj);					
				}
			}
		}
		for(var nth=1; nth <= 6; nth++){
			var fieldName = "lang" + nth + "_id";
			var level = "lang"+nth+"_level";
			if($scope.registration.personal[fieldName] != null && $scope.registration.personal[level] > 5){
				var obj = new Object();
				obj.id = $scope.registration.personal[fieldName];
				for(var index in $scope.registration.languages){
					if($scope.registration.languages[index].id == obj.id){
						obj.lang_english = $scope.registration.languages[index].lang_english;
					}
				}
//				obj.level =  $scope.registration.personal[level];
				if($scope.registration.personal.teaching1_id == obj.id ||  $scope.registration.personal.teaching2_id == obj.id|| $scope.registration.personal.teaching3_id == obj.id){
					obj.state = 1
				}
				else{
					obj.state = 0
				}
				$scope.teachable.push(obj);
			}
		}
	}

	$scope.selectTeachingLanguage = function($index){
		if($scope.teachable[$index].state == 1){
			$scope.teachable[$index].state = 0;
			if($scope.registration.personal.teaching1_id == $scope.teachable[$index].id){
				$scope.registration.personal.teaching1_id = null;
			}
			else if($scope.registration.personal.teaching2_id == $scope.teachable[$index].id){
				$scope.registration.personal.teaching2_id = null;
			}
			else if($scope.registration.personal.teaching3_id == $scope.teachable[$index].id){
				$scope.registration.personal.teaching3_id = null;
			}
		}
		else{
			if($scope.registration.personal.teaching1_id == null){
				$scope.registration.personal.teaching1_id = $scope.teachable[$index].id;
				$scope.teachable[$index].state = 1;
			}
			else if($scope.registration.personal.teaching2_id == null){
				$scope.registration.personal.teaching2_id = $scope.teachable[$index].id;
				$scope.teachable[$index].state = 1;
			}
			else if($scope.registration.personal.teaching3_id == null){
				$scope.registration.personal.teaching3_id = $scope.teachable[$index].id;
				$scope.teachable[$index].state = 1;
			}
			else{
				alert("Exceeded 3 language");
			}	
		}

		var regi = $scope.registration.personal;
		var teach = $scope.teachable;
		var arr = [];
		for(var i=0; i<teach.length; i++){
			if( teach[i].state == 1 ){
				arr.push(i);
			}
		}

		regi.teaching1_id = null;
		regi.teaching2_id = null;
		regi.teaching3_id = null;

		if( arr.length > 0){
			regi.teaching1_id = teach[arr[0]].id;
			regi.teaching1_id.state = 1;
		}
		if( arr.length > 1 ){
			regi.teaching2_id = teach[arr[1]].id;
			regi.teaching2_id.state = 1;
		}
		if( arr.length > 2 ){
			regi.teaching3_id = teach[arr[2]].id;
			regi.teaching3_id.state = 1;
		}
	}

	$scope.getOldLanguage = function(field){
		$scope.oldValue = $scope.registration.personal[field];
	}

 	$scope.changeLanguge = function(field){
		if($scope.checkLanguageValidation(field)){
			if($scope.registration.personal.teaching1_id == $scope.oldValue){
				$scope.registration.personal.teaching1_id = null;
			}
			else if($scope.registration.personal.teaching2_id == $scope.oldValue){
				$scope.registration.personal.teaching2_id = null;
			}
			else if($scope.registration.personal.teaching3_id == $scope.oldValue){
				$scope.registration.personal.teaching3_id = null;
			}
			if(field == "native1_id" || field == "native2_id" || field == "native3_id") {
				$scope.addTeachingLanguage();
			}
		}
	}

	$scope.checkLanguageValidation = function(field){
		var fieldName = field;
		var res = true;
		if($scope.registration.personal.native1_id == $scope.registration.personal[fieldName]){
			if(fieldName != "native1_id"){
				res = false;
			}
		}
		if($scope.registration.personal.native2_id == $scope.registration.personal[fieldName]){
			if(fieldName != "native2_id"){
				res = false;
			}
		}
		if($scope.registration.personal.native3_id == $scope.registration.personal[fieldName]){
			if(fieldName != "native3_id"){
				res = false;
			}
		}
		if($scope.registration.personal.lang1_id == $scope.registration.personal[fieldName]){
			if(fieldName != "lang1_id"){
				res = false;
			}
		}
		if($scope.registration.personal.lang2_id == $scope.registration.personal[fieldName]){
			if(fieldName != "lang2_id"){
				res = false;
			}
		}
		if($scope.registration.personal.lang3_id == $scope.registration.personal[fieldName]){
			if(fieldName != "lang3_id"){
				res = false;
			}
		}
		if($scope.registration.personal.lang4_id == $scope.registration.personal[fieldName]){
			if(fieldName != "lang4_id"){
				res = false;
			}
		}
		if($scope.registration.personal.lang5_id == $scope.registration.personal[fieldName]){
			if(fieldName != "lang5_id"){
				res = false;
			}
		}
		if($scope.registration.personal.lang6_id == $scope.registration.personal[fieldName]){
			if(fieldName != "lang6_id"){
				res = false;
			}
		}
		if(!res){
			$scope.registration.personal[fieldName] = 0;
			var temp = fieldName.split('_');
			$scope.registration.personal[temp[0] + "_level"] = 0;
			alert("Language duplicated");
		}
		return res;
	}

	$scope.checkPersonalTools = function(){
		console.log("checkPersonalTools");
		$scope.registration.personal.tools = [];
		if($scope.registration.personal.skype_id != null){
			var toolsObj = new Object();
			toolsObj.name = "Skype";
			toolsObj.account = $scope.registration.personal.skype_id;
			$scope.registration.personal.tools.push(toolsObj);
		}
		if($scope.registration.personal.hangout_id != null){
			var toolsObj = new Object();
			toolsObj.name = "Hangout";
			toolsObj.account = $scope.registration.personal.hangout_id;
			$scope.registration.personal.tools.push(toolsObj);
		}
		if($scope.registration.personal.facetime_id != null){
			var toolsObj = new Object();
			toolsObj.name = "Facetime";
			toolsObj.account = $scope.registration.personal.facetime_id;
			$scope.registration.personal.tools.push(toolsObj);
		}
		if($scope.registration.personal.qq_id != null){
			var toolsObj = new Object();
			toolsObj.name = "QQ";
			toolsObj.account = $scope.registration.personal.qq_id;
			$scope.registration.personal.tools.push(toolsObj);
		}
		if($scope.registration.personal.tools.length == undefined || $scope.registration.personal.tools.length == 0) {
			$scope.registration.personal.tools = [];
			var toolsObj = new Object();
			toolsObj.name = "Select tools";
			toolsObj.account = "";
			$scope.registration.personal.tools.push(toolsObj);
		}
	}

	$scope.getOldTools = function($index){
		$scope.oldMessenger = $scope.registration.personal.tools[$index].name;
	}

	$scope.checkToolsValidation = function($index){
		for(var idx=0; idx < $scope.registration.personal.tools.length; idx ++){
			if($index != idx){
				if($scope.registration.personal.tools[$index].name == $scope.registration.personal.tools[idx].name){
					alert("Messenger duplicated");
					$scope.registration.personal.tools[$index].name = $scope.oldMessenger;
				}
			}
		}
	}
	$scope.addTools = function(){
		var toolsObj = new Object();
		toolsObj.name = "Select tools";
		toolsObj.account = "";
		$scope.registration.personal.tools.push(toolsObj);
	}
	
	$scope.deleteTools = function($index){
		$scope.registration.personal.tools.splice($index,1);
	}

	$scope.submitPersonal = function(){
		console.log("submitPersonal");
		if($scope.checkAllData() ){
			
			//var data = JSON.stringify($scope.registration.personal);
			$http.post("/tutor/apply/personal/save", $scope.registration.personal ).success(function(response) {
				if ( response.isSuccess == 1) {
//					alert(response.message);
					var data = JSON.stringify($scope.registration.personal);
					$("#personal-form").append("<input type='hidden' name='personal' value='"+data+"'/>");
					$("#personal-form").submit();
				} else {
					console.log("errorResponse")
					console.log(response);
				}
			});
		}
		else{
			return;
		}
	}

//	Vaildation
	$scope.checkAllData = function(){
		console.log("checkAllData");
		if($scope.registration.personal.name == "" || $scope.registration.personal.name == null){
			alert("Name");
			return false;
		}
		if($scope.registration.personal.photo_filename == "" || $scope.registration.personal.photo_filename== null){
			alert("Photo");
			return false;
		}
		if($scope.registration.personal.from_country_id == "" || $scope.registration.personal.from_country_id== null){
			alert("from_country");
			return false;
		}
		if($scope.registration.personal.livingin_country_id == "" || $scope.registration.personal.livingin_country_id== null){
			alert("livingin_country");
			return false;
		}
		if($scope.registration.personal.native1_id == "" || $scope.registration.personal.native1_id == null){
			alert("native1");
			return false;
		} else {
			$scope.registration.personal.native1 =  $scope.registration.languages[$scope.registration.personal.native1_id].lang_english;
		}
		if($scope.registration.personal.native2_id != null) {
			$scope.registration.personal.native2 =  $scope.registration.languages[$scope.registration.personal.native2_id].lang_english;
		}
		if($scope.registration.personal.native3_id != null) {
			$scope.registration.personal.native3 =  $scope.registration.languages[$scope.registration.personal.native3_id].lang_english;
		}
		if($scope.registration.personal.lang1_id !=  null){
			$scope.registration.personal.lang1 =  $scope.registration.languages[$scope.registration.personal.lang1_id].lang_english;
			if($scope.registration.personal.lang1_level == null || $scope.registration.personal.lang1_level == 0){
				alert("lang1_id_skill_level");
				return false;
			}
		}
		if($scope.registration.personal.lang2_id !=  null){
			$scope.registration.personal.lang2 =  $scope.registration.languages[$scope.registration.personal.lang2_id].lang_english;
			if($scope.registration.personal.lang2_level == null || $scope.registration.personal.lang2_level == 0){
				alert("lang2_id_skill_level");
				return false;
			}
		}
		if($scope.registration.personal.lang3_id !=  null){
			$scope.registration.personal.lang3 =  $scope.registration.languages[$scope.registration.personal.lang3_id].lang_english;
			if($scope.registration.personal.lang3_level == null || $scope.registration.personal.lang3_level == 0){
				alert("lang3_id_skill_level");
				return false;
			}
		}
		if($scope.registration.personal.lang4_id !=  null){
			$scope.registration.personal.lang4 =  $scope.registration.languages[$scope.registration.personal.lang4_id].lang_english;
			if($scope.registration.personal.lang4_level == null){
				alert("lang4_id_skill_level");
				return false;
			}
		}
		if($scope.registration.personal.lang5_id !=  null){
			$scope.registration.personal.lang5 =  $scope.registration.languages[$scope.registration.personal.lang5_id].lang_english;
			if($scope.registration.personal.lang5_level == null){
				alert("lang5_id_skill_level");
				return false;
			}
		}
		if($scope.registration.personal.lang6_id !=  null){
			$scope.registration.personal.lang6 =  $scope.registration.languages[$scope.registration.personal.lang6_id].lang_english;
			if($scope.registration.personal.lang6_level == null){
				alert("other3_skill_level");
				return false;
			}
		}
		if($scope.registration.personal.teaching1_id !=  null){
			$scope.registration.personal.teaching1 =  $scope.registration.languages[$scope.registration.personal.teaching1_id].lang_english;
			//if($scope.registration.personal.teaching1_skill_level == null){
			//	alert("teaching1_skill_level");
			//	return false;
			//}
		} else{
			alert("Please select at least one language");
			return false;
		}
		if($scope.registration.personal.teaching2_id !=  null) {
			$scope.registration.personal.teaching2 =  $scope.registration.languages[$scope.registration.personal.teaching2_id].lang_english;
		}
		if($scope.registration.personal.teaching3_id !=  null) {
			$scope.registration.personal.teaching3 =  $scope.registration.languages[$scope.registration.personal.teaching3_id].lang_english;
		}
		
		if( ($scope.registration.personal.online == 0||$scope.registration.personal.online == null) && ($scope.registration.personal.offline == 0 || $scope.registration.personal.offline == null)){
			alert("online or offline");
			return false;
		}
		if($scope.registration.personal.online == 1){
			var count = 0;
			for(var idx=0; idx < $scope.registration.personal.tools.length; idx ++){
				if($scope.registration.personal.tools[idx].account != "" && $scope.registration.personal.tools[idx].account != null && $scope.registration.personal.tools[idx].name != -1){
					count++;
				}
				else{
					if($scope.registration.personal.tools[idx].name == -1){
						alert("Select messenger");
						return false;
					}
					else if($scope.registration.personal.tools[idx].account == ""){
						alert("input messenger account");
						return false;
					}
				}
			}
		}
		for(var idx=0; idx < $scope.registration.personal.tools.length; idx ++){
			if($scope.registration.personal.tools[idx].name != -1 && $scope.registration.personal.tools[idx].name != null){
				if($scope.registration.personal.tools[idx].account == null || $scope.registration.personal.tools[idx].account == ""){
					alert("Enter a messenger ID");
					return false;
				}
			}
		}
		return true;
	}

	// 이미지 파일 첨부	
	$scope.attachFile = function ( ) {
		console.log("Attach picture");
		console.log($scope.learnList);
		
		
		$("#filename").click();
	};

	$scope.submitPhoto = function(files){
		var fileName = new FormData();
		fileName.append("profile",files[0]);
		$http.post("/tutor/apply/upload_file/", fileName,{
			withCredentials: true,
	        headers: {'Content-Type': undefined },
	        transformRequest: angular.identity
		}).success(function(response){
			console.log(response);
			$scope.registration.personal.photo_filepath  = response.photo_filepath;
			$scope.registration.personal.photo_filename = response.photo_filename;
		});
	}

//	$scope.urls = {
//		"personal": "tutor/apply/",
//		"profile": "tutor/apply/profile/",
//		"payment": "tutor/apply/payment/",
//		"terms": "tutor/apply/terms/",
//		"complete": "tutor/apply/complete/",
//	};
	
	// Label resource
//	$scope.strings = {};
//	$scope.strings.labels = {
//		"full_name": "Full Name",
//		"profile_pic": "Profile Photo",
//		"where_are_you_from": "From",
//		"where_do_you_live": "Living in",
//		"language": "Language",
//		"level": "Level",
//		"what_language_will_you_teach": "What language will you teach?",
//		"add": "+ Add",
//		"back": "Back",
//		"continue": "Continue",
//	};

//	$http.post( "/tools/getlanguageinfo/" ).success( function( result ) {
//		//console.log(result);
//		if ( result.isSuccess == 1 ) {
//			$scope.selected_languages = [];
//			$scope.selected_levels = [];
//
//			var option = {
//				language2 : null,
//				language1 : null,
//				id : null
//			};
//			var option2 = {
//			   level_id : null,
//			   level_name : null
//			};
//			//language list
//			$scope.languageList = result.languagelist;
//			//level list
//			$scope.levelList = result.levellist;
//			//my language list
//			$scope.learnList = result.mylearnlanglist;
//			for(var i=0; i<result.mylearnlanglist.length; i++){
//				if ( result.mylearnlanglist[i].learn_speak == 1){
//					$scope.learnList[i].learn_speak = true;
//				}else{
//					$scope.learnList[i].learn_speak = false;
//				}
//			}
//			//user info
//			$scope.userInfo = result.userinfo;
//			user_id = result.user_id;
//			//selected language option
//			for(var i = 0; i < $scope.languageList.length; i++){
//				for(var j=0; j < $scope.learnList.length; j++){
//					if($scope.learnList[j].language_id == $scope.languageList[i].id){
//						option = $scope.languageList[i];
//						$scope.selected_languages.push(option);
//						break;
//					}
//				}
//			}
//			
//			//console.log('learnList');
//			//console.log($scope.learnList);
//			//selected level option
//			for(var i = 0; i < $scope.learnList.length; i++){
//				for(var j=0; j < $scope.levelList.length; j++){
//					if($scope.learnList[i].level == $scope.levelList[j].level_id){
//						option2 = $scope.levelList[j];
//						$scope.selected_levels.push(option2);
//						break;
//					}
//					if( j == $scope.levelList.length-1){
//						$scope.selected_levels.push( null );
//					}
//				}
//			}
//			
//		}
//	});

	// 이전 화면	
	$scope.go_back = function ( ) {
		console.log("Go back");
		$window.history.back();
	};

}]);