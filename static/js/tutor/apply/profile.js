var user_id = "";

tellpinApp.controller( "navigationCtrl", [ '$scope', function( $scope ) {
	$scope.accessType = 0;
	angular.element(document).ready(function () {
		if($scope.accessType == '0'){
			$("#tr-navigation div").css({"width":"calc(88% / 5)", "margin-right":"2%"});
			
		}
		else{
			$("#tr-navigation div").css({"width":"23%", "margin-right":"2%"});
		}
    });
}]);

tellpinApp.controller( "profileCtrl", [ '$scope', '$http', '$window', function( $scope, $http, $window ) {
//	$scope.urls = {
//		"personal": "/tutor/apply",
//		"profile": "tutor/apply/profile",
//		"payment": "tutor/apply/payment",
//		"terms": "tutor/apply/terms",
//		"complete": "tutor/apply/complete",
//	};

	$scope.strings = {};
	$scope.strings.labels = {
		"short_intro": "Short introduction",
		"resume": "Resume (optional)",
		"long_intro": "Long introduction",
		"teaching_special_tags": "Teaching Specialty Tags",
		"back": "Back",
		"continue": "Continue",
		"ex_video_intro_link": "Example http://www.youtube.com/v/235dz8DF?rel=0&modestbranding=1&autohide=1&showinfo=0&controls=0",
		"modal_work_desc": "Ex) I taught adult English language learners with a focus on intermediate level students.\n\n I mainly translated legal and corporate documents.",
		"modal_education_desc": "Ex) Graduated Magna Cum Laude\n Concentration in writing \nOverall GPA 3.8/4.0",
		"modal_certificate_name": "Ex) TESOL, TOEFL",
		"modal_certificate_organization": "Ex) TESL Canada, ETS",
		"modal_certificate_desc": "Ex) iBT Score 117/120",
	};

	$scope.init = function(){
		$scope.nationsInfo = $scope.data.nationsInfo;
//		console.log($scope.nationsInfo);
		$scope.nationsInfo.splice(0, 0, {id: -1, country: "Select One"});
		console.log($scope.data.profile_info);
		
		if( $scope.data.profile_info != undefined && $scope.data.profile_info != null) { //저장된 내용이 있다
			console.log($scope.data.profile_info);
			$scope.formdata = [];
			$scope.formdata.short_intro = $scope.data.profile_info.short_intro;
			$scope.formdata.education = $scope.data.profile_info.education;
			$scope.formdata.work = $scope.data.profile_info.work;
			$scope.formdata.certification = $scope.data.profile_info.certificate;
			$scope.formdata.long_intro = $scope.data.profile_info.long_intro;
			$scope.formdata.video_intro = $scope.data.profile_info.video_intro;
			$scope.formdata.resumeFile = new FormData();
			
			for(var i=1; i<=3; i++) {
				if( $scope.data.profile_info["work"+i] != undefined && $scope.data.profile_info["work"+i] != null ) {
					$scope.formdata["work"+i] = {};
					$scope.formdata["work"+i].info = true;
					$scope.formdata["work"+i].start_year = {val:$scope.data.profile_info["work"+i].start_year, name: $scope.data.profile_info["work"+i].start_year};
					$scope.formdata["work"+i].end_year = { val : $scope.data.profile_info["work"+i].end_year, name: $scope.data.profile_info["work"+i].end_year};
					$scope.formdata["work"+i].company = { val : $scope.data.profile_info["work"+i].company, valid: false};
					$scope.formdata["work"+i].country = Number($scope.data.profile_info["work"+i].country);
					$scope.formdata["work"+i].country_name = $scope.nationsInfo[$scope.data.profile_info["work"+i].country].country;
					$scope.formdata["work"+i].city = { val : $scope.data.profile_info["work"+i].city, valid: false};
					$scope.formdata["work"+i].position = { val : $scope.data.profile_info["work"+i].position, valid: false};
					$scope.formdata["work"+i].description = { val : $scope.data.profile_info["work"+i].description, valid: false};
					$scope.formdata["work"+i].work_exp_file = $scope.data.profile_info["work" + i + "_filename"];
					$scope.formdata["work"+i].work_exp_file_path = $scope.data.profile_info["work" + i + "_filepath"];
					$scope.formdata["work"+i].file_del_yn = false;
					
				}
				else {
					$scope.formdata["work"+i] = {};
					$scope.formdata["work"+i].info = false;
					$scope.formdata["work"+i].start_year = 0;
					$scope.formdata["work"+i].end_year = 0;
					$scope.formdata["work"+i].company = "";
					$scope.formdata["work"+i].country = 0;
					$scope.formdata["work"+i].country_name = "";
					$scope.formdata["work"+i].city = "";
					$scope.formdata["work"+i].position = "";
					$scope.formdata["work"+i].description = "";
					$scope.formdata["work"+i].work_exp_file = null;
					$scope.formdata["work"+i].work_exp_file_path = null;
					$scope.formdata["work"+i].file_del_yn = false;
				}
			}
			
			
			for(var i=1; i<=3; i++) {
				if( $scope.data.profile_info["certificate"+i] != undefined && $scope.data.profile_info["certificate"+i] != null ) {
					$scope.formdata["certificate"+i] = {};
					$scope.formdata["certificate"+i].info = true;
					$scope.formdata["certificate"+i].year = {val:$scope.data.profile_info["certificate"+i].year, name: $scope.data.profile_info["certificate"+i].year};
					$scope.formdata["certificate"+i].name = { val : $scope.data.profile_info["certificate"+i].name, valid: false};
					$scope.formdata["certificate"+i].organization = { val : $scope.data.profile_info["certificate"+i].organization, valid: false};
					$scope.formdata["certificate"+i].description = { val : $scope.data.profile_info["certificate"+i].description, valid: false};
					$scope.formdata["certificate"+i].certification_file = $scope.data.profile_info["certificate" + i + "_filename"];
					$scope.formdata["certificate"+i].certification_file_path = $scope.data.profile_info["certificate" + i + "_filepath"];
					$scope.formdata["certificate"+i].file_del_yn = false;
				}
				else {
					$scope.formdata["certificate"+i] = {};
					$scope.formdata["certificate"+i].info = false;
					$scope.formdata["certificate"+i].year = 0;
					$scope.formdata["certificate"+i].name = "";
					$scope.formdata["certificate"+i].organization = "";
					$scope.formdata["certificate"+i].description = "";
					$scope.formdata["certificate"+i].certification_file = null;
					$scope.formdata["certificate"+i].certification_file_path = null;
					$scope.formdata["certificate"+i].file_del_yn = false;
				}
			}
			
			for(var i=1; i<=3; i++) {
				if( $scope.data.profile_info["education"+i] != undefined && $scope.data.profile_info["education"+i] != null ) {
					$scope.formdata["education"+i] = {};
					$scope.formdata["education"+i].info = true;
					$scope.formdata["education"+i].start_year = {val:$scope.data.profile_info["education"+i].start_year, name: $scope.data.profile_info["education"+i].start_year};
					$scope.formdata["education"+i].end_year = { val : $scope.data.profile_info["education"+i].end_year, name: $scope.data.profile_info["education"+i].end_year};
					$scope.formdata["education"+i].school = { val : $scope.data.profile_info["education"+i].school, valid: false};
					$scope.formdata["education"+i].location = Number($scope.data.profile_info["education"+i].location);
					$scope.formdata["education"+i].location_name = $scope.nationsInfo[$scope.data.profile_info["education"+i].location].country;
					$scope.formdata["education"+i].degree = $scope.data.profile_info["education"+i].degree;
					$scope.formdata["education"+i].major = { val : $scope.data.profile_info["education"+i].major, valid: false};
					$scope.formdata["education"+i].description = { val : $scope.data.profile_info["education"+i].description, valid: false};;
					$scope.formdata["education"+i].education_file = $scope.data.profile_info["education" + i + "_filename"];
					$scope.formdata["education"+i].education_file_path = $scope.data.profile_info["education" + i + "_filepath"];
					$scope.formdata["education"+i].file_del_yn = false;
					
				}
				else {
					$scope.formdata["education"+i] = {};
					$scope.formdata["education"+i].info = false;
					$scope.formdata["education"+i].start_year = 0;
					$scope.formdata["education"+i].end_year = 0;
					$scope.formdata["education"+i].school = "";
					$scope.formdata["education"+i].location = 0;
					$scope.formdata["education"+i].location_name = "";
					$scope.formdata["education"+i].degree = "";
					$scope.formdata["education"+i].major = "";
					$scope.formdata["education"+i].description = "";
					$scope.formdata["education"+i].education_file = null;
					$scope.formdata["education"+i].education_file_path = null;
					$scope.formdata["education"+i].file_del_yn = false;
				}
			}
			
			
		} else { //저장된 내용이 없다.
			$scope.formdata = [];
			$scope.formdata.work_exp = "";
			$scope.formdata.education = "";
			$scope.formdata.certification = "";
			$scope.formdata.short_intro = "";
			$scope.formdata.long_intro = "";
			$scope.formdata.video_intro = "";
//			$scope.formdata.tags = [];
			$scope.formdata.resumeFile = new FormData();
			
			//work
			for (var i=1; i<=3; i++) {
				$scope.formdata["work"+i] = {};
				$scope.formdata["work"+i].info = false;
				$scope.formdata["work"+i].start_year = 0;
				$scope.formdata["work"+i].end_year = 0;
				$scope.formdata["work"+i].company = "";
				$scope.formdata["work"+i].country = 0;
				$scope.formdata["work"+i].country_name = "";
				$scope.formdata["work"+i].city = "";
				$scope.formdata["work"+i].position = "";
				$scope.formdata["work"+i].description = "";
				$scope.formdata["work"+i].work_exp_file = null;
				$scope.formdata["work"+i].file_del_yn = false;
			}

			//certification
			for (var i=1; i<=3; i++) {
				$scope.formdata["certificate"+i] = {};
				$scope.formdata["certificate"+i].info = false;
				$scope.formdata["certificate"+i].year = 0;
				$scope.formdata["certificate"+i].name = "";
				$scope.formdata["certificate"+i].organization = "";
				$scope.formdata["certificate"+i].description = "";
				$scope.formdata["certificate"+i].certification_file = null;
				$scope.formdata["certificate"+i].file_del_yn = false;
			}

			//education
			for (var i=1; i<=3; i++) {
				$scope.formdata["education"+i] = {};
				$scope.formdata["education"+i].info = false;
				$scope.formdata["education"+i].start_year = 0;
				$scope.formdata["education"+i].end_year = 0;
				$scope.formdata["education"+i].school = "";
				$scope.formdata["education"+i].location = 0;
				$scope.formdata["education"+i].location_name = "";
				$scope.formdata["education"+i].degree = "";
				$scope.formdata["education"+i].major = "";
				$scope.formdata["education"+i].description = "";
				$scope.formdata["education"+i].education_file = null;
				$scope.formdata["education"+i].file_del_yn = false;
			}
		}

		$scope.start_year = 0;
		$scope.yearList = [];
		var d = new Date();
		$scope.thisYear = d.getFullYear()
		for($scope.start_year = $scope.thisYear; $scope.start_year >= d.getFullYear() - 60; $scope.start_year--) {
			$scope.yearList.push({val :$scope.start_year, name: $scope.start_year});
		}
		
		$scope.endYearList = [];
		$scope.endYearList.push({val :-1, name: "to now"});

		$scope.degreeList = ["Bachelor’s", "Master’s", "Doctorate", "Postdoctoral", "Other"];

		$scope.workDescPH = $scope.strings.labels.modal_work_desc;
		$scope.eduDescPH = $scope.strings.labels.modal_education_desc;
		$scope.certiNamePH = $scope.strings.labels.modal_certificate_name;
		$scope.certiOrganizationPH = $scope.strings.labels.modal_certificate_organization;
		$scope.certiDescPH = $scope.strings.labels.modal_certificate_desc;
		
		//upload file temp  
		$scope.tempFile = null;
	}

	$scope.modifyEndYearList = function ( field ) {

		$scope.endYearList = [];
		$scope.start_year = field.start_year.val;
		for (var endYear = field.end_year.val; endYear >= $scope.start_year; endYear--){
			$scope.endYearList.push({val: endYear, name: endYear});
		}
		$scope.endYearList.splice(0, 0, {val: -1, name: "to now"});
//		$scope.endYearList.push({val :field.end_year.val, name: field.end_year.name});
	}
	$scope.setEndYearList = function( field ){
		$scope.endYearList = [];
		$scope.start_year = field.start_year.val;
		for (var endYear = $scope.thisYear; endYear >= $scope.start_year; endYear--){
			$scope.endYearList.push({val: endYear, name: endYear});
		}
		$scope.endYearList.splice(0, 0, {val: -1, name: "to now"});
		field.end_year = $scope.endYearList[0];
	}
	
	$scope.countingText = function(elId, obj, start, end){
		console.log(obj.val.length);
		if((0 < obj.val.length && obj.val.length <= start) || obj.val.length > end) {
			$("#"+elId).addClass("has-error");
			obj.valid = true;
		} else {
			$("#"+elId).removeClass("has-error");
			obj.valid = false;
		}
	}
	$scope.clearText = function(elId, obj) {
		$("#"+elId).removeClass("has-error");
		obj.val= "";
		obj.valid = false;
	}
	
	$scope.togglePH = function(flag, txt) {
		var ph;
		if(flag) {
			ph = txt;
		} else {
			ph = "";
		}
		return ph;
	}
	

	$scope.openWorkModal = function( field ){
		console.log("openWorkModal");
		console.log(field);
		$scope.work_modal = $scope.formdata[field];
//		if (field == "work1") {
//			$scope.work_modal = $scope.formdata.work1;
//		} else if (field == "work2") {
//			$scope.work_modal = $scope.formdata.work2;
//		} else if (field == "work3"){
//			$scope.work_modal = $scope.formdata.work3;
//		}
		$scope.work_modal.start_year =  $scope.yearList[0];
		$scope.work_modal.end_year = $scope.endYearList[0];
		$scope.work_modal.country = $scope.formdata[field].country;
		$scope.work_modal.company = {val:"", valid:false};
		$("#work-company").removeClass("has-error");
		$scope.work_modal.city = {val:"", valid:false};
		$("#work-city").removeClass("has-error");
		$scope.work_modal.position = {val:"", valid:false};
		$("#work-position").removeClass("has-error");
		$scope.work_modal.description = {val:"", valid:false};
		$("#work-desc").removeClass("has-error");
		
		//upload file temp  
		$scope.tempFile = null;
		$(".upload-name").val("Find File");
		$("#write-file1").val("");
		
		angular.element( '#worktModal' ).modal();
	}

	$scope.submitWorkModal = function( field ){
		console.log("submitWorkModal");
		
		if(!$scope.workModalValidChk() ) {
			return;
		}
		//work_exp list 추가
		
		$scope.work_modal.info = true;
		$scope.work_modal.country_name = $scope.nationsInfo[$scope.work_modal.country].country;
		if( $scope.work_modal.end_year.val < 0 ) {
			var d = new Date();
			$scope.work_modal.end_year.val = d.getFullYear()
		}
		
		//attach file
		if($scope.tempFile) {
			$scope.work_modal.file_del_yn = false;
			$scope.work_modal.work_exp_file = $scope.tempFile;
		}
		console.log($scope.work_modal);
		
		angular.element( '#worktModal' ).modal("hide");
	}

	$scope.workModalValidChk = function() {
		
		if($scope.work_modal.company.valid || $scope.work_modal.company.val == undefined || $scope.work_modal.company.val == "") {
			alert("Company");
			$("#modal-text-company").focus();
			return false;
		}
		
		if($scope.work_modal.country < 0) {
			alert("Country");
			$("#modal-sel-country").focus();
			return false;
		}
		
		if($scope.work_modal.city.valid || $scope.work_modal.city.val == undefined || $scope.work_modal.city.val == "") {
			alert("City");
			$("#modal-text-city").focus();
			return false;
		}
		
		if($scope.work_modal.position.valid || $scope.work_modal.position.val == undefined || $scope.work_modal.position.val == "") {
			alert("Position");
			$("#modal-text-position").focus();
			return false;
		}
		
		if($scope.work_modal.description.valid || $scope.work_modal.description.val == undefined || $scope.work_modal.description.val == "") {
			alert("Description");
			$("#modal-text-desc").focus();
			return false;
		}
		
		return true;
	}

	$scope.openEducationModal = function( field ){
		console.log("openEducationModal");
		$scope.education_modal = $scope.formdata[field];
//		if (field == "education1") {
//			$scope.education_modal = $scope.formdata.education1;
//		} else if (field == "education2") {
//			$scope.education_modal = $scope.formdata.education2;
//		} else if (field == "education3"){
//			$scope.education_modal = $scope.formdata.education3;
//		}
		
		$scope.education_modal.start_year =  $scope.yearList[0];
		$scope.education_modal.end_year = $scope.endYearList[0];
		$scope.education_modal.location = $scope.formdata[field].location;
		
		$scope.education_modal.school = {val:"", valid:false};
		$("#edu-school").removeClass("has-error");
		$scope.education_modal.major = {val:"", valid:false};
		$("#edu-major").removeClass("has-error");
		$scope.education_modal.description = {val:"", valid:false};
		$("#edu-desc").removeClass("has-error");
		
		//upload file temp  
		$scope.tempFile = null;
		$(".upload-name").val("Find File");
		$("#write-file2").val("");
		
		console.log($scope.education_modal);
		angular.element( '#educationModal' ).modal();
	}
	
	$scope.submitEducationModal = function(){
		console.log("submitEducationModal");
		
		if(!$scope.eduModalValidChk() ) {
			return;
		}
		
		$scope.education_modal.info = true;
		$scope.education_modal.location_name = $scope.nationsInfo[$scope.education_modal.location].country;
		if( $scope.education_modal.end_year.val < 0 ) {
			var d = new Date();
			$scope.education_modal.end_year.val = d.getFullYear()
		}
		
		//attach file
		if($scope.tempFile){
			$scope.education_modal.education_file = $scope.tempFile;
		}
		
		
		angular.element( '#educationModal' ).modal("hide");
	}
	
	$scope.eduModalValidChk = function() {
		if($scope.education_modal.school.valid || $scope.education_modal.school.val == undefined || $scope.education_modal.school.val == "") {
			alert("School");
			$("#edu-modal-text-shcool").focus();
			return false;
		}
		
		if($scope.education_modal.location < 0) {
			alert("Location");
			$("#edu-location").focus();
			return false;
		}
		
		
//		if($scope.education_modal.major.valid || $scope.education_modal.major.val == undefined || $scope.education_modal.major.val == "") {
//			alert("Major");
//			$("#edu-modal-text-major").focus();
//			return false;
//		}
		
//		if($scope.education_modal.description.valid || $scope.education_modal.description.val == undefined || $scope.education_modal.description.val == "") {
//			alert("Description");
//			$("#edu-modal-text-desc").focus();
//			return false;
//		}
		
		return true;
	}

	$scope.openCertificateModal = function( field ){
		console.log("openCertificateModal");
		$scope.certificate_modal = $scope.formdata[field];
//		if (field == "certificate1") {
//			$scope.certificate_modal = $scope.formdata.certificate1;
//		} else if (field == "certificate2") {
//			$scope.certificate_modal = $scope.formdata.certificate2;
//		} else if (field == "certificate3"){
//			$scope.certificate_modal = $scope.formdata.certificate3;
//		}
		
		$scope.certificate_modal.year =  $scope.yearList[0];
		
		$scope.certificate_modal.name = {val:"", valid:false};
		$("#certi-name").removeClass("has-error");
		$scope.certificate_modal.organization = {val:"", valid:false};
		$("#certi-organization").removeClass("has-error");
		$scope.certificate_modal.description = {val:"", valid:false};
		$("#certi-desc").removeClass("has-error");
		
		//upload file temp  
		$scope.tempFile = null;
		$(".upload-name").val("Find File");
		$("#write-file3").val("");
		
		console.log($scope.certificate_modal);
		angular.element( '#certificateModal' ).modal();
	}

	$scope.submitCertificateModal = function(){
		console.log("submitCertificateModal");
		
		if (! $scope.certiModalValidChk() ) {
			return;
		}
		
		$scope.certificate_modal.info = true;
		//attach file 
		if($scope.tempFile){
			$scope.certificate_modal.certification_file = $scope.tempFile;
		}
		
		angular.element( '#certificateModal' ).modal("hide");
	}
	
	$scope.certiModalValidChk = function() {
		if($scope.certificate_modal.name.valid || $scope.certificate_modal.name.val == undefined || $scope.certificate_modal.name.val == "") {
			alert("Name");
			$("#certi-modal-text-name").focus();
			return false;
		}
		
		if($scope.certificate_modal.organization.valid || $scope.certificate_modal.organization.val == undefined || $scope.certificate_modal.organization.val == "") {
			alert("Organization or Institution");
			$("#certi-modal-text-organization").focus();
			return false;
		}
		
//		if($scope.certificate_modal.description.valid || $scope.certificate_modal.description.val == undefined || $scope.certificate_modal.description.val == "") {
//			alert("Description");
//			$("#certi-modal-text-desc").focus();
//			return false;
//		}
		
		return true;
	}

	$scope.editResumeData = function( field ) {
		
		console.log("editResumeData(" + field + ")");
		for(var i=1; i<=3; i++) {
			if (field == "work"+i) {
				$scope.work_modal = $scope.formdata["work"+i];
				$scope.modifyEndYearList($scope.formdata["work"+i]);
				if( typeof $scope.work_modal.work_exp_file == "string") {
					$(".upload-name").val($scope.work_modal.work_exp_file);
				} else if (typeof $scope.work_modal.work_exp_file == "object" ) {
					if($scope.work_modal.work_exp_file) {
						$(".upload-name").val($scope.work_modal.work_exp_file.name);
					} else {
						$(".upload-name").val("Find File");
					}
				} else {
					$(".upload-name").val($scope.work_modal.work_exp_file);
				}
				angular.element( '#worktModal' ).modal();
				break;
			}
			
			if (field == "education"+i) {
				$scope.education_modal = $scope.formdata["education"+i];
				$scope.modifyEndYearList($scope.formdata["education"+i]);
				if( typeof $scope.education_modal.education_file == "string") {
					$(".upload-name").val($scope.education_modal.education_file);
				} else if (typeof $scope.education_modal.education_file == "object" ) {
					if($scope.education_modal.education_file) {
						$(".upload-name").val($scope.education_modal.education_file.name);
					} else {
						$(".upload-name").val("Find File");
					}
				} else {
					$(".upload-name").val($scope.education_modal.education_file);
				}
				angular.element( '#educationModal' ).modal();
				break;
			}
			
			if (field == "certificate"+i) {
				$scope.certificate_modal = $scope.formdata["certificate"+i];
				if( typeof $scope.certificate_modal.certification_file == "string") {
					$(".upload-name").val($scope.certificate_modal.certification_file);
				} else if (typeof $scope.certificate_modal.certification_file == "object" ) {
					if($scope.certificate_modal.certification_file) {
						$(".upload-name").val($scope.certificate_modal.certification_file.name);
					} else {
						$(".upload-name").val("Find File");
					}
				} else {
					$(".upload-name").val($scope.certificate_modal.certification_file);
				}
				angular.element( '#certificateModal' ).modal();
				break;
			}
		}
		
	};
	
	$scope.delResumeData = function( field, index ){
		console.log("delResumeData(" + field + ")" + index);
		if (field == "work") {
			$scope.formdata[field+index].info = false;
			$scope.formdata[field+index].start_year = 0;
			$scope.formdata[field+index].end_year = 0;
			$scope.formdata[field+index].company = null;
			$scope.formdata[field+index].country = 0;
			$scope.formdata[field+index].country_name = "";
			$scope.formdata[field+index].city = null;
			$scope.formdata[field+index].position = null;
			$scope.formdata[field+index].description = null;
			$scope.formdata[field+index].work_exp_file = null;
			$scope.formdata[field+index].work_exp_file_path = "";
		} else if (field == "certificate") {
			$scope.formdata[field+index].info = false;
			$scope.formdata[field+index].year = 0;
			$scope.formdata[field+index].name = "";
			$scope.formdata[field+index].organization = "";
			$scope.formdata[field+index].description = "";
			$scope.formdata[field+index].certification_file = null;
			$scope.formdata[field+index].certification_file_path = "";
		} else if (field == "education") {
			$scope.formdata[field+index].info = false;
			$scope.formdata[field+index].year = 0;
			$scope.formdata[field+index].name = "";
			$scope.formdata[field+index].organization = "";
			$scope.formdata[field+index].description = "";
			$scope.formdata[field+index].education_file = null;
			$scope.formdata[field+index].education_file_path = "";
		}
	}
	
	$scope.uploadResumeFile = function(files){
		$scope.tempFile = files[0];
	}

	$scope.deleteResumeFile = function(modal, field, fileNum) {
		$scope[modal][field] = null;
		$scope[modal].file_del_yn = true;
		$(".upload-name").val("Find File");
		$("#write-file" + fileNum).val("");
	}

	$scope.go_back = function ( ) {
		console.log("Go back");
		//$window.history.back();
		$scope.goURL("/tutor/apply/")
	};
	$scope.goURL = function(url){
		$window.location.href = (url);
		$window.event.stopPropagation();
	}
	
	$scope.checkValidation = function ( ) {
		var formElements = document.getElementById("profile-form").elements;
		for (var index = 0; index < formElements.length; index++) {
			console.log(formElements[index]);
			if(formElements[index].checkValidity() == false) {
				alert("Please fill in the required field.");
			    formElements[index].focus();
			    return false;
			}
		}
		
		// Check special tag field validation
//		var count = 0;
//		for (var index = 0; index < $scope.formdata.tags.length; index++) {
//			if ( $scope.formdata.tags[index].state ) {
//				count++;
//			}
//		}
//		if ( count <= 0 ) {
//			alert("Please select one tag or more.");
//			return false;
//		}

	    return true;
	};
	
	$scope.checkContinue = function ( ) {
//		for (var i = 0; i < $scope.formdata.tags.length; i++) {
//			if ($scope.formdata.tags[i].state) {
//				$("#profile-form").append("<input type='hidden' id='tags' name='tags[]' value='" + $scope.formdata.tags[i].id + "'/>");
//			}
//		}
		
		for( var i = 1; i <= 3; i++) {
			if( $scope.formdata["work"+i].info ) {
				$scope.formdata.resumeFile.append("work"+ i + "_file", $scope.formdata["work" + i ].work_exp_file);
			}
			
			if( $scope.formdata["education"+i].info ) {
				$scope.formdata.resumeFile.append("education" + i + "_file", $scope.formdata["education" + i ].education_file);
			}
			
			if( $scope.formdata["certificate"+i].info ) {
				$scope.formdata.resumeFile.append("certification" + i + "_file", $scope.formdata["certificate" + i ].certification_file);
			}
			
		}
		if ( $scope.checkValidation() ) {
			console.log("file upload start");
			$http.post("/tutor/apply/profile/upload_file/", $scope.formdata.resumeFile,{
				withCredentials: true,
		        headers: {'Content-Type': undefined },
		        transformRequest: angular.identity
			}).success(function(response){
				console.log(response.message);
				if (response.isSuccess == 1) {
					objData = {};
					objData.education = [];
					objData.work_exp = [];
					objData.certification = [];
					for(var i = 1; i <= 3; i++) {
						var obj = {};
						obj["work" + i] = {info: "", start_year: "", end_year: "", company: "", country: "", country_name: "", city: "", position: "", description:"" , file_del_yn: ""};
						obj["work" + i].info = $scope.formdata["work"+i].info;
						obj["work" + i].start_year = $scope.formdata["work" + i].start_year.val;
						obj["work" + i].end_year = $scope.formdata["work" + i].end_year.val;
						obj["work" + i].company = $scope.formdata["work" + i].company.val;
						obj["work" + i].country = $scope.formdata["work" + i].country;
						obj["work" + i].country_name = $scope.formdata["work" + i].country_name;
						obj["work" + i].city = $scope.formdata["work" + i].city.val;
						obj["work" + i].position = $scope.formdata["work" + i].position.val;
						obj["work" + i].description = $scope.formdata["work" + i].description.val;
						obj["work" + i].file_del_yn = $scope.formdata["work" + i].file_del_yn;
						objData.work_exp.push( obj );
						
						obj = {};
						obj["certificate" + i] = {info : "", year: "", name: "", organization: "", description: "", file_del_yn: ""};
						obj["certificate" + i].info = $scope.formdata["certificate" + i].info;
						obj["certificate" + i].year = $scope.formdata["certificate" + i].year.val;
						obj["certificate" + i].name = $scope.formdata["certificate" + i].name.val;
						obj["certificate" + i].organization = $scope.formdata["certificate" + i].organization.val;
						obj["certificate" + i].description = $scope.formdata["certificate" + i].description.val;
						obj["certificate" + i].file_del_yn = $scope.formdata["certificate" + i].file_del_yn;
						objData.certification.push( obj );

						obj = {};
						obj["education" + i] = {info: "", start_year: "", end_year: "", school: "", location: "", location_name: "", degree: "", major: "", description:"" , file_del_yn: ""};
						obj["education" + i].info = $scope.formdata["education"+i].info;
						obj["education" + i].start_year = $scope.formdata["education" + i].start_year.val;
						obj["education" + i].end_year = $scope.formdata["education" + i].end_year.val;
						obj["education" + i].school = $scope.formdata["education" + i].school.val;
						obj["education" + i].location = $scope.formdata["education" + i].location;
						obj["education" + i].location_name = $scope.formdata["education" + i].location_name;
						obj["education" + i].degree = $scope.formdata["education" + i].degree;
						obj["education" + i].major = $scope.formdata["education" + i].major.val;
						obj["education" + i].description = $scope.formdata["education" + i].description.val;
						obj["education" + i].file_del_yn = $scope.formdata["education" + i].file_del_yn;
						objData.education.push( obj );
					}

					objData.short_intro = $scope.formdata.short_intro;
					objData.long_intro = $scope.formdata.long_intro;
					objData.video_intro = $scope.formdata.video_intro;
					
					//upload file list
					// console.log(response.upload_file_list);
					objData.upload_file_list = response.upload_file_list;
					console.log(objData);

					$http.post("/tutor/apply/profile/save", objData ).success(function(response) {
						console.log(response.message);
						$("#profile-form").submit();
					});
				}

			});
		}
	}
	
}]);