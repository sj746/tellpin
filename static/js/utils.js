

//validation check
  function check_validation() {
	  
	  if ( check_email() && check_name() && check_password() ) {
		  return true;
	  }
	  else {
		  return false;
	  }
  }
  function check_email() {
	  var email = $("#id_email").val();
	  //console.log( 'check email'+ email );
  	  var regex=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;   
  
	  if(regex.test(email) === false) {  
	      $("#id_email").focus();
	      alert("잘못된 이메일 형식입니다."); 
	      return false;  
	  } else {  
	      return true;
	  }  
  }
  function check_name() {
	  if( $("#id_name").val() == "" ) {
		  $("#id_name").focus();
		  alert("이름을 입력해 주십시오.");
		  return false;
	  }else {
		  return true;
	  }
  }
  function check_password() {
	  if ( $("#id_pwd").val().length < 4 ){
		  $("#id_pwd").focus();
		  alert("비밀번호는 4자리 이상으로 입력해 주십시오.");
		  return false;
	  }else {
		  return true;
	  }
  }
  function pswvalidation() {
	//최소한 한개의 숫자, 문자(소,대문자 구분x), 특수문자.  
	  var paswd1= /^(?=.*\d)(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,16}$/;
	  var paswd2= /^(?=.*\d)(?=.*[a-zA-Z]).{6,16}$/;
	  var paswd3= /^(?=.*[a-zA-Z])(?!.*\s).{6,16}$/;
	  var paswd4= /^(?=.*\d)(?!.*\s).{6,16}$/;
	  var paswd5= /^(?!.*\s).{6,16}$/;
	  var password = $("#id_password1").val();
	  
	  if($("#id_password1").val() == "") {
		  $("#pswvalidcheck").text("");
		  return;
	  }
	  //console.log($("#id_password1").text());
	  
	  if(password.match(paswd1)) {
		  $("#pswvalidcheck").text("유효한 비밀번호입니다.");
		  $("#pswvalidcheck").css("color", "green");
		  $("#pswvalidcheck").css("font-weight", "bold");
		  $("#pswvalidcheck").css("font-size", "13px");
		  
	  } else if (password.match(paswd2)) {
		  $("#pswvalidcheck").text("특수문자가 포함되어야 합니다.");
		  $("#pswvalidcheck").css("color", "red");
		  $("#pswvalidcheck").css("font-weight", "bold");
		  $("#pswvalidcheck").css("font-size", "13px");
	  } else if (password.match(paswd3)) {
		  $("#pswvalidcheck").text("숫자가 포함되어야합니다.");
		  $("#pswvalidcheck").css("color", "red");
		  $("#pswvalidcheck").css("font-weight", "bold");
		  $("#pswvalidcheck").css("font-size", "13px");
	  } else if (password.match(paswd4)) {
		  $("#pswvalidcheck").text("문자가 포함되어야합니다.");
		  $("#pswvalidcheck").css("color", "red");
		  $("#pswvalidcheck").css("font-weight", "bold");
		  $("#pswvalidcheck").css("font-size", "13px");
	  } else if (password.match(paswd5)) {
		  $("#pswvalidcheck").text("문자가 포함되어야합니다.");
		  $("#pswvalidcheck").css("color", "red");
		  $("#pswvalidcheck").css("font-weight", "bold");
		  $("#pswvalidcheck").css("font-size", "13px");
	  } else {
		  $("#pswvalidcheck").text("최소 6글자 이상이어야 합니다.");
		  $("#pswvalidcheck").css("color", "red");
		  $("#pswvalidcheck").css("font-weight", "bold");
		  $("#pswvalidcheck").css("font-size", "13px");
	  }
  }
  function check_password2() {
	  // 6 ~ 16자리 패스워드. 문자. 숫자 . 특수문자 조합 체크. 
	  //?=.*[] 최소한 한개.
	  
	  //최소한 한개의 숫자, 문자(소,대문자 구분x), 특수문자.  
	  var paswd= /^(?=.*\d)(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,16}$/;  
	  var password = $("#id_password1").val();
	  
	  if(password.match(paswd)) {
		return true;  
	  }else {   
	    $("#id_password1").focus();
		alert('비밀번호는 최소 6자리의 숫자(0~9), 문자(a~Z), 특수문자(#,!,% 등)의 조합으로 입력해 주십시오.');
		return false;  
	  } 
	  
	  //Input Password and Submit [7 to 15 characters which contain only characters, numeric digits, underscore and first character must be a letter]
	  /* var passw=  /^[A-Za-z]\w{7,14}$/;  
	  if(inputtxt.value.match(passw))   {   
		  alert('Correct, try another...')  
		  return true;  
	  }else{   
		  alert('Wrong...!')  
		  return false;  
	  } 
	  //Input Password and Submit [6 to 20 characters which contain at least one numeric digit, one uppercase and one lowercase letter]
	   var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;  
	  if(inputtxt.value.match(passw))   
	  {   
	  alert('Correct, try another...')  
	  return true;  
	  }  
	  else  
	  {   
	  alert('Wrong...!')  
	  return false;  
	  }  
	  //Input Password and Submit [7 to 15 characters which contain at least one numeric digit and a special character]
	  var paswd=  /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/;  
	  if(inputtxt.value.match(paswd))   
	  {   
	  	alert('Correct, try another...')  
		return true;  
	  }else{   
		alert('Wrong...!')  
		return false;  
	  } 
	  //Input Password and Submit [8 to 15 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character]
	  var decimal=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;  
	  if(inputtxt.value.match(decimal))   
	  {   
		alert('Correct, try another...')  
		return true;  
	  }  
	  else  
	  {   
		alert('Wrong...!')  
		return false;  
	  }
	  */
  }
  function check_password_equal() {
	  var pwd1 = $("#id_password1").val();
	  var pwd2 = $("#id_password2").val();
	  
	  if( pwd1 == pwd2 ) {
		  return true;
	  }else {
		  alert('비밀번호가 일치 하지 않습니다.');
		  return false;
	  }
	  
  }