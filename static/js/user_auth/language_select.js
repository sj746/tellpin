var langApp = angular.module("langApp", ["ngSanitize"]);

langApp.config( function ( $interpolateProvider ) {
	$interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

langApp.controller( "langCtrl", ['$scope', '$sce', function ( $scope, $sce ) {
	$scope.learnFocused = false;
	$scope.speakFocused = false;
	$scope.popArr = ["English", "Korea", "Spanish", "Italy"]; // Popular Language
	$scope.othArr = ["abc", "def", "ghi", "jkl"]; // Others Language
	$scope.learnSelArr = [];
	$scope.speakSelArr = [];
	$scope.learnElemArr = [];
	$scope.speakElemArr = [];
	$scope.learnModel = "";
	$scope.speakModel = "";
	$scope.classModel = new Object();
	
	$( "#searchTxt1" ).on( "focus", function() {
		$scope.learnFocused = true;
		$scope.$apply();
	});
	
	$( "#searchTxt1" ).on( "blur", function() {
		$scope.learnFocused = false;
		$scope.$apply();
	});
	
	$( "#searchTxt2" ).on( "focus", function() {
		$scope.speakFocused = true;
		$scope.$apply();
	});
	
	$( "#searchTxt2" ).on( "blur", function() {
		$scope.speakFocused = false;
		$scope.$apply();
	});
	
	$scope.learnSelectLang = function($event, type, lang ) {
		// type = 0 Popular Language
		// type = 1 Others Language
		if ( type == 0 ) {
			for ( var i = 0; i < $scope.learnSelArr.length; i++ ) {
				if ( $scope.learnSelArr[i] == lang ) 
					return false;
			}
			$scope.learnSelArr.push( lang );
			
		}
		else {
			for ( var i = 0; i < $scope.learnSelArr.length; i++ ) {
				if ( $scope.learnSelArr[i] == lang )
					return false;
			}
			$scope.learnSelArr.push( lang );			
		} 
		$scope.learnElemArr.push(angular.element($event.currentTarget));
		angular.element($event.currentTarget).addClass("select-elem");
		angular.element($event.currentTarget).addClass("elem-select");
		angular.element($event.currentTarget).removeClass("group-option");
		$scope.learnModel = "";
		$scope.learnFocused = false;
	};
	
	$scope.speakSelectLang = function($event, type, lang ) {
		// type = 0 Popular Language
		// type = 1 Others Language
		if ( type == 0 ) {
			for ( var i = 0; i < $scope.speakSelArr.length; i++ ) {
				if ( $scope.speakSelArr[i] == lang ) 
					return false;
			}
			$scope.speakSelArr.push( lang );
			
		}
		else {
			for ( var i = 0; i < $scope.speakSelArr.length; i++ ) {
				if ( $scope.speakSelArr[i] == lang )
					return false;
			}
			$scope.speakSelArr.push( lang );			
		} 
		$scope.speakElemArr.push(angular.element($event.currentTarget));
		angular.element($event.currentTarget).addClass("select-elem");
		angular.element($event.currentTarget).addClass("elem-select");
		angular.element($event.currentTarget).removeClass("group-option");
		$scope.speakModel = "";
		$scope.speakFocused = false;
	};
	
	$scope.learnDeleteLang = function ( index ) {
		$scope.learnElemArr[index].addClass("group-option");
		$scope.learnElemArr[index].removeClass("select-elem");
		$scope.learnElemArr[index].removeClass("elem-select");
		$scope.learnSelArr.splice( index, 1 );
		$scope.learnElemArr.splice( index, 1 );
	};
	
	$scope.speakDeleteLang = function ( index ) {
		$scope.speakElemArr[index].addClass("group-option");
		$scope.speakElemArr[index].removeClass("select-elem");
		$scope.speakElemArr[index].removeClass("elem-select");
		$scope.speakSelArr.splice( index, 1 );
		$scope.speakElemArr.splice( index, 1 );
	};
	
	$scope.learnSelectItems = function ( lang ) {
		return lang.indexOf($scope.learnModel) == -1 ? false : true;
	};
	
	$scope.speakSelectItems = function ( lang ) {
		return lang.indexOf($scope.speakModel) == -1 ? false : true;
	};
	
	$scope.learnSelectClass = function ( str ) {

		for ( var i = 0; i < $scope.learnSelArr.length; i++ ) {
			if ( $scope.learnSelArr[i] == str ) {
				$scope.classModel['group-option'] = false;
				$scope.classModel['select-elem'] = true
				$scope.classModel['elem-select'] = true;
				return $scope.classModel;
			}
		}
		$scope.classModel['group-option'] = true;
		$scope.classModel['select-elem'] = false;
		$scope.classModel['elem-select'] = false;
		
		return $scope.classModel;
	}
	
	$scope.speakSelectClass = function ( str ) {

		for ( var i = 0; i < $scope.speakSelArr.length; i++ ) {
			if ( $scope.speakSelArr[i] == str ) {
				$scope.classModel['group-option'] = false;
				$scope.classModel['select-elem'] = true
				$scope.classModel['elem-select'] = true;
				return $scope.classModel;
			}
		}
		$scope.classModel['group-option'] = true;
		$scope.classModel['select-elem'] = false;
		$scope.classModel['elem-select'] = false;
		
		return $scope.classModel;
	}
	
	$scope.learnKeyBackspace = function ( $event ) {
		if ( $scope.learnSelArr.length == 0 ) return;
		if ( $scope.learnModel == "" && $event.keyCode == 8 ) {
			$scope.learnSelArr.pop();
		}
	}
	
	$scope.speakKeyBackspace = function ( $event ) {
		if ( $scope.speakSelArr.length == 0 ) return;
		if ( $scope.speakModel == "" && $event.keyCode == 8 ) {
			$scope.speakSelArr.pop();
		}
	}
}]);