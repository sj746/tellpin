tellpinApp.controller( "detailsCtrl", ['$scope', '$http', '$timeout', '$window', '$interval', function( $scope, $http, $timeout, $window, $interval, $filter) {
	$scope.area = [];
	$scope.selectDate = [];

	angular.element(document).ready(function(){
	});

	var newLessonMessagePolling;

	$scope.init = function(){
		$scope.ratings = 5;
		$scope.list = $scope.data.rv_data;
		$scope.getMessage();
		console.log( $scope.list );
		$scope.setCurrentTime();
		$scope.$parent.checkMessage();
		// 초기화 시 실시간 채팅을 시작함
		$scope.startNewLessonMessagePolling();
	}

	$scope.$on('$destroy', function() {
		$scope.stopNewLessonMessagePolling();
		console.log('is destroy');
	});
	
	$scope.goURL = function(url){
		$window.location.href = (url);
		$window.event.stopPropagation();
	}
	$scope.anyProblem = function(item){
		if ($scope.list.user == $scope.list.tutee_id) {
			location.href = "/tuteelessons/lesson/problem/"+item.rvid;	
		} else {
			location.href = "/tutorlessons/lesson/problem/"+item.rvid;
		}

		return;
	}
	$scope.getMessage = function() {
		$scope.messageModel = "";
		var to_user_id = 0;
		
		if( $scope.list.is_tutor == 1 ){
			console.log( $scope.list.tutee_id );
			to_user_id = $scope.list.tutee_id;
		}else{
			console.log( $scope.list.tutor_id );
			to_user_id = $scope.list.tutor_id;
		}
		var rvid = $scope.list.rvid;
		console.log( $scope.list.is_tutor );
		console.log( $scope.list.tutor_id);
		console.log( $scope.list.tutee_id);
		console.log('to_user_id: '+to_user_id);
		$http.post( '/tutor/getmessage/', { 'to_user_id' : to_user_id, 'rvid':rvid})
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
//				console.log( result.message );
				$scope.message = result.message;
				$scope.$broadcast('dataloaded');
				var elem = angular.element( '#message' );
//				$( '#messageModal' ).modal();
				console.log( 'get message success : ' );
				console.log( elem[0].scrollHeight );
				console.log(result);
				$scope.$parent.checkMessage();
			}
		});
		//angular.element( '#message' ).scrollTop = 800;
		$timeout(function(){
			console.log('timeout()');
			console.log( $("#message")[0].scrollHeight );

			console.log('timeout2()');
			$("#message").scrollTop( $("#message")[0].scrollHeight );

			console.log('timeout3()');
		},450);
	
	}
	$scope.$on('dataloaded', function(){
		$("#message").scrollTop($("#message")[0].scrollHeight);
	})
	// 메세지 전송
	$scope.sendMessage = function() {
		var to_user_id = 0;
		if( $scope.list.is_tutor == 1 ){
			to_user_id = $scope.list.tutee_id;
		}else{
			to_user_id = $scope.list.tutor_id;
		}
		console.log('to_user_id: '+to_user_id);
		console.log( $scope.messageModel );
		if ( $scope.messageModel.length > $scope.messageLength || !$scope.messageModel ) {
			console.log( 'not save' );
			return;
		}
		else {
			$http.post( '/tutor/newmessage/', { 'to_user_id' : to_user_id, 'message' : $scope.messageModel, 'rvid': $scope.list.rvid })
			.success( function( result ) {
				if ( result.isSuccess == 1 ) {
					$scope.message = result.message;
					$scope.messageModel = "";
					console.log( $scope.message );
					
					$timeout(function(){
						console.log('timeout()');
						console.log( $("#message")[0].scrollHeight );

						console.log('timeout2()');
						$("#message").scrollTop( $("#message")[0].scrollHeight );

						console.log('timeout3()');
					},450);
				}
			});
		}
	}
	
	// 실시간 채팅을 시작하는 함수
	$scope.startNewLessonMessagePolling = function() {
		if (angular.isDefined(newLessonMessagePolling)) { return; }
		
//		newLessonMessagePolling = $interval( function() {
//			if( $scope.list.is_tutor == 1 ){
//				to_user_id = $scope.list.tutee_id;
//			}else{
//				to_user_id = $scope.list.tutor_id;
//			}
//			
//			var rvid = $scope.list.rvid;
//			
//			console.log(to_user_id);
//			console.log(rvid)
//			
//			$http.post('/tutor/checkNewMessage/', {'to_user_id': to_user_id, 'rvid': rvid})
//			.success( function(result) {
//				if (result.newMsg) {
//					var to_user_id = 0;
//					
//					if( $scope.list.is_tutor == 1 ){
//						to_user_id = $scope.list.tutee_id;
//					}else{
//						to_user_id = $scope.list.tutor_id;
//					}
//					var rvid = $scope.list.rvid;
//					$http.post( '/tutor/getmessage/', { 'to_user_id' : to_user_id, 'rvid':rvid})
//					.success( function( result ) {
//						if ( result.isSuccess == 1 ) {
//							$scope.message = result.message;
//							$scope.$broadcast('dataloaded');
//							var elem = angular.element( '#message' );
//							$scope.$parent.checkMessage();
//						}
//					});
//					$timeout(function(){
//						$("#message").scrollTop( $("#message")[0].scrollHeight );
//					},450);
//				}
//			});
//			
//		}, 3000)
		
	}
	
	// 실시간 채팅을 종료하는 함수
	$scope.stopNewLessonMessagePolling = function() {
		if (angular.isDefined(newLessonMessagePolling)) { return; }
		
		$interval.cancel(newLessonMessagePolling);
		newLessonMessagePolling = undefined;
	}
	
	//Remaining Time Caculate
	$scope.setCurrentTime = function(){ 
		var now = new Date();
		$scope.currentUTCTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
		$scope.changeTimezone();
		$timeout($scope.setCurrentTime, 1000);
	};
	//Remaining Time Caclute ( timer )
	$scope.changeTimezone = function(){
		if ($scope.list.from_date_time != null){
			var displayTime = new Date($scope.currentUTCTime.getTime());
			var t_time = $scope.list.from_date_time;
			var startTime = new Date(t_time.substr(0,4), parseInt(t_time.substr(5,2))-1, t_time.substr(8,2), t_time.substr(11,2), t_time.substr(14,2), t_time.substr(17,2));
			
			
	//		console.log('lesson time: ' + startTime);
	//		console.log('current time: ' + displayTime);
	//		console.log(t_time.substr(0,4), t_time.substr(5,2), t_time.substr(8,2), t_time.substr(11,2), t_time.substr(14,2), t_time.substr(17,2));
			
			if( $scope.list.from_date_time != null){
				$scope.delta = Math.floor( (startTime - displayTime) / 1000 ) ;
				$scope.currentTime = displayTime.format("yyyy.MM.dd hh:mm:ss a/p ");
				
	//			console.log($scope.list.from_date_time);
	//			console.log( $scope.delta );
				if ($scope.delta < 0){
					$scope.remainingTime = "ㅡ";
					$(".time").css("display", "none");
				}else{
					$scope.remainingTime = Math.floor($scope.delta/(60*60*24)) +"일 " + Math.floor(($scope.delta%(60*60*24))/60/60) + "시간 " + Math.floor(($scope.delta%(60*60*24)%(60*60))/60) + "분 " + Math.floor((($scope.delta%(60*60*24))%(60*60))%60) + "초";
				}
			}
		}else{
			$scope.remainingTime = "ㅡ";
		}
	}
	//button action 
	$scope.clickBtnAction = function( action, type ) {
		//TODO 
		//상황별 액션 처리. 
		//0: Not Scheduled, 1:Requested, 2: Need Confirmation, 3:Declined, 4:Canceled
		//5: Confirmed, 6: In Problem, 7: Refunded, 8: Rescheduled, 9: Approved / Upcoming

		console.log("clickBtnAction");
		console.log("action = " + action, ", type = " + type);
		console.log("status = " + $scope.list.status_name);
		if( $scope.list.is_tutor == 1 ) {
			//feedback
			if (action == "Leave Feedback") {
				if (type == undefined) {
					$scope.openPopFeedback();
					return;
				} else {
					$("#modalFeedback").modal('hide');
					var _data = {};
					_data["ratings"] = 0;
					_data["comment"] = $("#feedback_comment").val();
					$http.post( "/tutoring/commit/", { command : $scope.list.status_name, action:action, rvid : $scope.list.rvid, isTutor: $scope.list.is_tutor, type:'details', data:_data }).success(function(response) {
						if( response.isSuccess != '0') {
							console.log(response);
							$scope.list = response.rv_data;
							//$scope.rv_list = response.rv_data.entries
							//$scope.rv_data = response.rv_data
						}
					});
					return;
				}
			} else if (action == "Decline") {
				if (type == undefined) {
					$scope.openDecline();
					return;
				} else {
					$("#modalDecline").modal('hide');
					$http.post( "/tutoring/commit/", { command : $scope.list.status_name, action:action, rvid : $scope.list.rvid, isTutor: $scope.list.is_tutor, type:'details' }).success(function(response) {
						if( response.isSuccess != '0') {
							console.log(response);
							$scope.list = response.rv_data;
						}
					});
					return;
				}
			} else if (action == "Request to change" || action == "Edit request") {
				if (type == undefined) {
					$scope.openRequestChange();
					return;					
				} else {
					var _data = {};
					if ($scope.selectDate.date == undefined && $scope.selectDate.value == undefined){
						alert("select Change Date!!!!");
						return;
					}
					$("#modalRequestChange").modal('hide');
					_data["comment"] = $("#change_message").val();
					var new_date = $scope.selectDate.date + $scope.selectDate.value;
					console.log(_data["comment"]);
					console.log($scope.selectDate.date);
					console.log($scope.selectDate.value);
					$http.post( "/tutoring/commit/", { command : $scope.list.status_name, action:'Request to change', rvid : $scope.list.rvid, isTutor: $scope.list.is_tutor, type:'details', data:_data, new_date: new_date }).success(function(response) {
						if( response.isSuccess != '0') {
							console.log(response);
							$scope.list = response.rv_data;
						}
					});
					return;
				}
			}
		} else{//tutee
			//confirm
			if (action == "Confirm" || "Leave Feedback") {
				if (type == undefined) {
					$scope.openPopConfirm();
					return;
				} else{
					$("#modalConfirm").modal('hide');
					var _data = {};
					_data["ratings"] = $scope.ratings;
					_data["comment"] = $("#confirm_comment").val();
					$http.post( "/tutoring/commit/", { command : $scope.list.status_name, action:action, rvid : $scope.list.rvid, isTutor: $scope.list.is_tutor, type:'details', data:_data }).success(function(response) {
						if( response.isSuccess != '0') {
							console.log(response);
							$scope.list = response.rv_data;
						}
					});
					return;
				}
			} else if (action == "Request to change" || action == "Edit request") {
				if (type == undefined) {
					$scope.openRequestChange();
					return;
				} else {
					var _data = {};
					if ($scope.selectDate.date == undefined && $scope.selectDate.value == undefined){
						alert("select Change Date!!!!");
						return;
					}
					$("#modalRequestChange").modal('hide');
					_data["comment"] = $("#change_message").val();
					var new_date = $scope.selectDate.date + $scope.selectDate.value;
					$http.post( "/tutoring/commit/", { command : $scope.list.status_name, action:'Request to change', rvid : $scope.list.rvid, isTutor: $scope.list.is_tutor, type:'details', data:_data, new_date: new_date }).success(function(response) {
						if( response.isSuccess != '0') {
							console.log(response);
							$scope.list = response.rv_data;
						}
					});
					return;
				}
			} else if (action == "Reschedule") {
				var _data = {};
				$("#modalRequestChange").modal('hide');
				$http.post( "/tutoring/commit/", { command : action, action:action, rvid : $scope.list.rvid, isTutor: $scope.list.is_tutor, type:'details', data:_data }).success(function(response) {
					if( response.rv_data.add_schedule != undefined) {
						console.log(response);

						location.href = response.rv_data.add_schedule;	
						return;
					}
				});
				return;
			}
			//tutee
//			else if (action == "Cancel") {
//				if (type == undefined) {
//					$scope.openCancel();
//					return;
//				} else {
//					$("#modalDecline").modal('hide');
//					$http.post( "/tutoring/commit/", { command : $scope.list.status_name, action:action, rvid : $scope.list.rvid, isTutor: $scope.list.is_tutor, type:'details' }).success(function(response) {
//						if( response.isSuccess != '0') {
//							console.log(response);
//							$scope.list = response.rv_data;
//						}
//						
//					});
//					return;
//				}
//			}
		}
		if (action == "Edit statement" || action == "Make statement") {
			if (type == undefined) {
				$scope.openProblemState();
			} else {
				var _data={};
				$("#modalProblemState").modal('hide');
				_data["claim_detail"] = $("#problem_message").val();
				_data["claim_reportor_id"] = $scope.list.user;
				if ($scope.list.user == $scope.list.tutor_id) {
					_data["claim_reportee_id"] = $scope.list.tutee_id;
				} else{
					_data["claim_reportee_id"] = $scope.list.tutor_id;
				}
				$http.post( "/tutoring/commit/", { command : $scope.list.status_name, action:"problem_statement", rvid : $scope.list.rvid, isTutor: $scope.list.is_tutor, type:'details', data:_data }).success(function(response) {
					if( response.isSuccess != '0') {
						console.log(response);
						$scope.list = response.rv_data;
					}
				});
			}
			return;
		}
		if (action == "Canceled" && type == 1) {
			var _data = {};
			$("#modalRequestChange").modal('hide');
			_data["comment"] = $("#change_message").val();
			$http.post( "/tutoring/commit/", { command : $scope.list.status_name, action:action, rvid : $scope.list.rvid, isTutor: $scope.list.is_tutor, type:'details', data:_data }).success(function(response) {
				if( response.isSuccess != '0') {
					console.log(response);
					$scope.list = response.rv_data;
				}
			});
			return;
		}
		if( $scope.list.status == 6 && action == "Report a problem"){
			//TODO 페이지 이동
			location.href = "/tutorlessons/lesson/problem/"+$scope.list.rvid;
			return;
		}
		if( $scope.list.status == 6 && action == "Reply"){
			//TODO 페이지 이동
			location.href = "/tutorlessons/lesson/problem/"+$scope.list.rvid;
			return;
		}
		$http.post( "/tutoring/commit/", { command : $scope.list.status_name, action:action, rvid : $scope.list.rvid, isTutor: $scope.list.is_tutor, type:'details' }).success(function(response) {
			if( response.isSuccess != '0') {
				console.log(response);
				console.log('test1');
				if( response.rv_data.add_schedule != undefined) {
//					if (response.rv_data.is_tutor == 0) {
						location.href = response.rv_data.add_schedule;						
//					}
					return;
				}
//				if (response.rv_data.is_tutor == 1) {
//					location.href = "/tutorlessons/lesson/" + $scope.list.rvid;	
//				} else {
//					location.href = "/tuteelessons/lesson/" + $scope.list.rvid;
//				}
				console.log('test2');
				console.log(response.rv_data);
				console.log('test3');
//				$scope.list = response.rv_data[0];
				$scope.list = response.rv_data;
			}
			
		}).error(function(response){
			console.log( '/tutoring/commit/ erorr!!!');
			alert(response);
		});
	}

	//Confirm popup
	$scope.openPopConfirm = function(){
		console.log("openPopConfirm");
		$("#modalConfirm").modal();
	}
	//Feedback popup
	$scope.openPopFeedback = function(){
		console.log("openPopFeedback");
		document.getElementById('feedback_comment').value = "";
		$("#modalFeedback").modal();
	}
	//tutor only decline popup
	$scope.openDecline = function(){
		console.log("openCancel");
		$("#modalDecline").modal();
	}
	//tutee only cancel popup
	$scope.closeDecline = function() {
		console.log("closeCancel");
		angular.element( '#modalDecline' ).modal( 'hide' );
	}
	$scope.openCancel = function(){
		console.log("openCancel");
		$("#modalCancel").modal();
	}
	$scope.closeCancel = function() {
		console.log("closeCancel");
		angular.element( '#modalCancel' ).modal( 'hide' );
	}
	$scope.openRequestChange = function() {
		console.log("openRequestChange");
		document.getElementById('change_message').value = "";
		$scope.request_date = null;
		$("#modalRequestChange").modal();
		$scope.request_options = null;
	}
	$scope.openProblemState = function() {
		console.log("openProblemState");
		document.getElementById('problem_message').value = "";
		$("#modalProblemState").modal();
	}
	$scope.closeRequestChange = function() {
		console.log("closeRequestChange");
		$scope.requestdate = false;
		angular.element( '#modalRequestChange' ).modal( 'hide' );
	}
	$scope.change_select = function() {
		console.log("change_select");
		if ($scope.request_options == null) {
			$scope.request_options = 1;	
		} else {
			$scope.request_options = null;
		}
		console.log($scope.request_options);
	}
	//click ratings
	$scope.clickRatings = function(num){
		$scope.ratings = num;
		console.log(num);
		$(".ratings > img").attr('src', '/static/img/tutor/icon_star_empty.png');
		for(var i=0; i<num; i++){
			$(".star"+(i+1)).attr('src', '/static/img/tutor/icon_star_full.png');
		}
	}
	$scope.checkLengthConfirmModal = function(){
		var len = $(".confirm-area").val().length;
		var txt = $(".txt-len").text( len + ' / 400 characters');
	}
	$scope.checkTextLengthModal = function(){
		var len = $(".confirm-area2").val().length;
		var txt = $(".txt-len2").text( len + ' / 400 characters');
	}

	$scope.requestChangeDate = function( change ) {
		console.log("requestChangeDate");
		console.log(change);
		return;
		if (change == 0) {
			location.href = "/tutorlessons/lesson/"+response.rvid;
			return;
		} else {
			$http.post("/tutoring/change_lesson_date/", { tid: tid, date : $scope.schedule.mark_day}).success(function(response){
				console.log(response);
				location.href = "/tutorlessons/lesson/"+response.rvid;
				return;
			});
		}
	}

	$scope.openChangeDateModal = function(tid, rvid) {
		console.log("openChangeDateModal");
		$scope.closeRequestChange();
//		event.preventDefault();
//		event.stopPropagation();
		console.log(tid);
		$("body").css("overflow-y","hidden");
		$("body").css("overflow-x","hidden");
		$scope.$emit("sendTid", tid);
		$http.post( "/tutoring/tutor_schedule_pop/", {tid: tid, rvid:rvid}).success( function( response ) {
			if ( response.isSuccess == 1 ) {
				console.log("!!!!!!!!!!!!!!!!!!!!");
				$scope.scheduleInfo = response;
				console.log($scope.scheduleInfo);
				$scope.tutor = new Object();
				$scope.tutor.user = tid;
				$scope.schedulePopup = true;
				$("#modalChangeDate").modal();
				$scope.initChangeDateModal();
			} 
		});
	}
	$scope.closeChangeDateModal = function() {
		console.log("closeChangeDateModal");
		angular.element( '#modalChangeDate' ).modal( 'hide' );
		$scope.schedulePopup = false;
		$scope.requestdate = false;
		$scope.openRequestChange();
	}
	$scope.initChangeDateModal = function() {
		console.log("initChangeDateModal");
		console.log($scope.scheduleInfo);
		$scope.requestdate = false;
		$scope.schedule = $scope.scheduleInfo.schedule;
		$scope.reserved = $scope.scheduleInfo.reserved;
		$scope.setWeekday();
		$scope.setReservedTime();
//		$scope.initCurrentTime('{{request.session.utc_time}}');
	}
	$scope.setWeekday = function(){
		$scope.weekList = [];
		for(var idx = 0; idx <7 ; idx++){
			$scope.weekList.push(dayToAlphabet($scope.schedule.dow_list[idx]));
		}
	}
	$scope.setReservedTime = function(){
		console.log("setReservedTime");
		$scope.reservedTime = [];
		console.log($scope.reserved);
		for(var idx = 0; idx < $scope.reserved.length ; idx++){
			var bundleSize = $scope.reserved[idx].duration / 30;
			var day = $scope.reserved[idx].rv_time.slice(0,1);
			var index = -1;
			if($scope.reserved[idx].rv_time.slice(3,5) == "30"){
				index = parseInt($scope.reserved[idx].rv_time.slice(1,3))*2 +1;
			}
			else{
				index = parseInt($scope.reserved[idx].rv_time.slice(1,3))*2;
			}
			for(var i = 0 ; i < bundleSize; i++){
				$scope.reservedTime.push(day+indexToValue(index+i));
			}
		}
	}
	$scope.initAreaValue = function(day, index){
		$scope.bundleCount = 1;
		var time = day;
		if(index%2 == 0){
			if(index/2 < 10){
				time = time + "0" + index/2 + "00";
			}
			else{
				time = time + index/2 + "00";
			}
		}
		else{
			if(index/2 < 10){
				time = time + "0" + ((index-1)/2) + "30";
			}
			else{
				time = time + ((index-1)/2) + "30";
			}
		}
		return time;
	}
	$scope.initAreaState = function(day, index){
		var dow = {"mon":"A" , "tue":"B", "wed":"C", "thu":"D", "fri":"E", "sat":"F", "sun":"G"}
	
		$scope.setCurrentTimeToModal();
		if( $scope.scheduleInfo.today == $scope.schedule.mark_day ) {
			if (day == dow[$scope.schedule.dow_list[0]]) {
				if ( index < $scope.currentTime * 2 + parseInt($scope.currentMin / 30) + 1 ){
					
					if($scope.reservedTime.indexOf($scope.area[index][day].value) != -1){
						return 3;
					}
					
					return 0;
				}
			}
		}

		if($scope.reservedTime.indexOf($scope.area[index][day].value) != -1){
			return 3;
		}
		else{
			if($scope.schedule.available_time.indexOf($scope.area[index][day].value) != -1){
				for(var i = 0 ; i<$scope.schedule.appointed.length; i++){
					if($scope.schedule.appointed[i].delta_time == $scope.area[index][day].value){
						if($scope.schedule.appointed[i].type == 2){
							return 0;
						}
					}
				}
				return 1;
			}
			else{
				for(var i = 0 ; i<$scope.schedule.appointed.length; i++){
					if($scope.schedule.appointed[i].delta_time == $scope.area[index][day].value){
						if($scope.schedule.appointed[i].type == 1){
							return 1;
						}
					}
				}
				return 0;
			}
		}
	};

	$scope.checkAvailableTime = function(index, day){
//		console.log("checkAvailableTime");
		var bundleArea = $scope.scheduleInfo.rv_data.duration/30;
		var available = true;
		if(index+bundleArea <= 48){
			for(var idx = 0; idx < bundleArea; idx ++){
				if($scope.area[index+idx][day].state != 1){
					available = false;
				}
			}
		}
		else{
			available = false;
		}
		if(available){
			for(var idx = 0; idx < bundleArea; idx ++){
				$scope.area[index+idx][day].isHover = 1;
			}
		}
	};

	$scope.hoverIn = function(){
//		console.log("hoverIn");
		var bundleArea = $scope.scheduleInfo.rv_data.duration/30;
	    $(".tr-schedule-template table tbody tr").hover(function() {
			$(this).prev().find(".time").css("background", "#ececec");
			
		}, function() {
			$(this).prev().find(".time").css("background", "none");
		});
	};

	$scope.resetIsHover = function(index, day){
//		console.log("resetIsHover");

		if($scope.scheduleInfo.rv_data.duration == 60){
			if($scope.area[index][day].isHover == 1 && $scope.area[index+1][day].isHover == 1){
				$scope.area[index][day].isHover = 0;
				$scope.area[index+1][day].isHover = 0;
			}
		}
		else{
			if($scope.area[index][day].isHover = 1){
				$scope.area[index][day].isHover = 0;
			}
		}
	};
	$scope.getPrevWeek = function(tid){
		console.log("getPrevWeek");
		$http.post("/tutor/tutor_schedule/prev/", { tid: tid, date : $scope.schedule.mark_day}).success(function(response){
			$scope.schedule= response.schedule;
			$scope.reserved = response.reserved;
			$scope.setReservedTime();
			$scope.initAllArea();
		});
	};
	$scope.getNextWeek = function(tid){
		console.log("getNextWeek");
		$http.post("/tutor/tutor_schedule/next/", { tid: tid, date : $scope.schedule.mark_day}).success(function(response){
			$scope.schedule= response.schedule;
			$scope.reserved = response.reserved;
			$scope.setReservedTime();
			$scope.initAllArea();
		});
	};
	$scope.initAllArea = function(){
		console.log("initAllArea");
		for(var index=0; index < $scope.area.length; index++){
			$scope.area[index].A.state = $scope.initAreaState('A', index);
			$scope.area[index].B.state = $scope.initAreaState('B', index);
			$scope.area[index].C.state = $scope.initAreaState('C', index);
			$scope.area[index].D.state = $scope.initAreaState('D', index);
			$scope.area[index].E.state = $scope.initAreaState('E', index);
			$scope.area[index].F.state = $scope.initAreaState('F', index);
			$scope.area[index].G.state = $scope.initAreaState('G', index);
		}
	};
	$scope.getTbodyHeight = function(){
		console.log("getTbodyHeight");
		var height = window.innerHeight - 70 - 140;
		if(height > 722){
			height = "722px";
		}
		$(".schedule-table tbody").height(height);

	}
	$scope.selectArea = function(index, day){
		console.log("$scope.scheduleInfo = " + $scope.scheduleInfo);
		console.log("index = " + index);
		console.log("day = " + day);

		var bundleArea = $scope.scheduleInfo.rv_data.duration/30;
		console.log("bundleArea = " + bundleArea);
		var clickable = true;
		var onoff = false;
		var state = -1;
		console.log("state = " + $scope.area[index][day].state);

		for(var idx = 0; idx < bundleArea; idx ++) {
			if (idx == 0){
				if($scope.area[index+idx][day].state == 1){
					console.log("day");
					onoff=true;
					state = $scope.area[index+idx][day].state;
				} else if($scope.area[index+idx][day].state == 2){
					onoff=false;
					state = $scope.area[index+idx][day].state;
				} else{
					clickable = false;
					break;
				}
			} else{
				if (state != $scope.area[index+idx][day].state) {
					clickable = false;
					break;
				}
			}
		}
		if (clickable) {
			if(onoff) {
				console.log("@@@@@@@@@@@@@@");
				$scope.initAllArea();
				var data = {index: index, day: day, duration: $scope.scheduleInfo.rv_data.duration, state: 2}
				for(var idx = 0; idx < bundleArea; idx ++){
					if(idx == 0){
						data["value"] = $scope.area[index+idx][day].value;
					}
					else{
						data["value"+idx] = $scope.area[index+idx][day].value;
					}
					$scope.area[index+idx][day].state = 2;
				}
				$scope.setSelectDate(data);

			} else{
				console.log("##################");
				var data = {index: index, day: day, duration: $scope.scheduleInfo.rv_data.duration, state: 1}
				for(var idx = 0; idx < bundleArea; idx ++){
					$scope.area[index+idx][day].state = 1;
				}
			}
		}
	};

	$scope.setSelectDate = function(data) {
		$scope.selectDate = new Object();
		var dateIndex = -1;
		for(var idx = 0 ; idx < $scope.schedule.dow_list.length; idx++){
			if(data.day == dayToAlphabet($scope.schedule.dow_list[idx])){
				dateIndex = idx;
				break;
			}
		}
		$scope.selectDate.date = $scope.schedule.week_list[dateIndex];
		$scope.selectDate.day = data.day;
		$scope.selectDate.index = data.index;
		$scope.selectDate.duration = data.duration;
		
		var bundleSize = data.duration/30;
		
		$scope.selectDate.startTime = setDisplayTime(data.index);
		$scope.selectDate.endTime = setDisplayTime(data.index+bundleSize);
		
		for(var idx = 0 ; idx < bundleSize; idx++){
			if(idx == 0){
				$scope.selectDate.value = data.value;
			}
			else{
				$scope.selectDate["value"+idx] = data["value"+idx];
			}
		}
		console.log($scope.selectDate);
	}

	$scope.changeDate = function( change ) {
		console.log("changeDate");
		$scope.closeChangeDateModal();
		$scope.requestdate = true;
		$scope.request_date = $scope.selectDate.date.substring(0,4) + "-" + $scope.selectDate.date.substring(4,6) + "-" + $scope.selectDate.date.substring(6,8) + "-" + $scope.selectDate.startTime;
//		if (change == 0) {
//			location.href = "/tutorlessons/lesson/"+245;
//			return;
//		} else {
//			var $form = $('<form></form>');
//			    $form.attr('action', '/tuteelessons/lesson/' + 245 + '/');
//			    $form.attr('method', 'post');
//			    $form.appendTo('body');
//			
//				var tid = $('<input type="hidden" value="' +$scope.tutor.tid+ '" name="tid">');
//			    var accessType = $('<input type="hidden" value="0" name="accessType">');
//			
//			    $form.append(tid).append(accessType);
//			    $form.submit();
//		}
	}

	$scope.initCurrentTime = function(utcTime) {
		$scope.userUTCTime = utcTime;
	}
	
	$scope.setCurrentTimeToModal = function(){
		var now = new Date();
		$scope.currentUTCTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
		$scope.changeTimezoneToModal();
		//$timeout($scope.setCurrentTimeToModal, 1000);
		//$scope.updateTimeTable();
	};
	
	$scope.changeTimezoneToModal = function(){
		if($scope.userUTCTime == ''){
			$scope.userUTCTime = minutesToUTC(-(new Date().getTimezoneOffset()));
		}
		var displayTime = new Date($scope.currentUTCTime.getTime() + utcToMilliseconds($scope.userUTCTime));
		$scope.currentTime = displayTime.format("HH");
		$scope.currentMin = displayTime.format("mm");
		
		$scope.today = displayTime.getFullYear().toString() + (displayTime.getMonth()+1).toString() + (displayTime.getDate()+1).toString();
		$scope.displayTime = displayTime;
	}


}]);


