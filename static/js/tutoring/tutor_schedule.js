tellpinApp.filter('scheduleHeaderFiler', function() {
	  return function(month) {
		  var monthNames = ["January", "February", "March", "April", "May", "June",
		                    "July", "August", "September", "October", "November", "Decembe"];
	    return monthNames[month-1]
	  };
});
tellpinApp.controller( "tsController", function( $scope, $http , $window, $interval, $timeout) {
	$scope.strings = {};
	$scope.strings.labels = {
			"schedule_comment": "Ex) I will be on vacation from May 26 till June 10. See you after June 11.\nFeel free to send me messages. I'll reply to your message shortly.\n" +
					"I'm in a hospital now, so I'm taking a break temporarily. I'll be back when I get well.\nMy current semester is ending in a week. I'll be totally free from Dec.20.",
		};
	$scope.initDisplay = function(display){
		$scope.display=display;
	}
	$scope.initSchedule = function(){
		$scope.scheduleView = 0;
		$scope.startArea=new Object();
		$scope.startArea.index = -1;
		$scope.endArea = new Object();
		$scope.endArea.index = -1;
		$scope.weekList = ["Mon", "Tue", "Wed", "Thu", "Fri", " Sat" ,"Sun"];
		if( $scope.schedule.week_schedule.length == 0 ){
			for(var idx=0 ; idx<7 ; idx++){
				var obj = new Object();
				$scope.schedule.week_schedule.push(obj);
				if(idx == 0){
					$scope.schedule.week_schedule[idx].day_of_week = 7;
				}
				else{
					$scope.schedule.week_schedule[idx].day_of_week = idx;
				}
			}
		}
		else{
			$scope.schedule.week_schedule.splice(0,0,$scope.schedule.week_schedule[6]);
			$scope.schedule.week_schedule.splice(7,1);
		}
		$scope.fromTimes = new Array(48);
		for(var idx = 0; idx < $scope.fromTimes.length; idx ++){
			$scope.fromTimes[idx] = parseInt(idx);
		}
		$scope.toTimes = new Array(48);
		for(var idx = 0; idx < $scope.toTimes.length; idx ++){
			$scope.toTimes[idx] = parseInt(idx+1);
		}
		
		$scope.start = new Date( );
		$scope.end = new Date( ); 
		$scope.end = $scope.end.setDate($scope.start.getDate() + 30);
		$scope.startDateOptions = {
				formatYear: 'yy',
				minDate: $scope.start
		}
		$scope.endDateOptions = {
				formatYear : 'yy',
				minDate: $scope.start
		}
		
		
		//변경될 스케줄 정보를 담아두는 리스트
		$scope.AddList = [];
		//삭제 이력 리스트
		$scope.DelList = [];
		//초기 area 상태 
		$scope.initTableState = {bFlag: true, area: []};
		//apply button flag
		$scope.applyButton = true;
		//apply update loading gif
		$scope.updateLoading = false;
	}
	
	$scope.openCalendarStartDate = function() {
		$scope.startDateOptions.maxDate = $scope.end;
		$scope.startDate = true;
	}
	
	$scope.openCalendarEndDate = function() {
		$scope.endDateOptions.minDate = $scope.start;
		$scope.endDate = true;
	}
//	$scope.today = function() {
//	    $scope.dt = new Date();
//	};
//	$scope.today();
//
//	$scope.clear = function() {
//		$scope.dt = null;
//	};
	
	$scope.showData = function(){
		console.log($scope.schedule);
	}
	$scope.setHoverDay = function(date){
		if(date == 0){
			$scope.hoverDay = -1;
		}
		else{
			$scope.hoverDay = date;
		}
	}
	$scope.setHoverTime = function($index){
		$scope.hoverTime = $index;
	}

	$scope.addTime = function($event, date, day){
		$scope.isMobile = 0;
		var target = angular.element($event.target);
		var top = 0;
		var left = 0;
		var parentLeft = $(".col-xs-10").offset().left;
		if($(target)[0].tagName == 'A'){
			top = $(target).parent().parent().offset().top + 5 +"px";
			left = $(target).parent().parent().offset().left - parentLeft - 57 + "px";
		}
		else{
			top = $(target).parent().parent().parent().offset().top + 5 +"px";
			left = $(target).parent().parent().parent().offset().left - parentLeft - 57 + "px";
		}
		
		$("#tutor-add-popup").css("top", top);
		$("#tutor-add-popup").css("left", left);
		$scope.addDate = date;
		$scope.addDayIndex = day;
	}
	$scope.addMobileTime = function(date, day){
		$scope.isMobile = 1;
		$scope.addDate = date;
		$scope.addDayIndex = day;
	}
	$scope.cancelTime = function(){
		$scope.addDate = -1;
		$scope.fromTime = null;
		$scope.toTime = null;
	}
	$scope.addSchedule = function(){
		if($scope.fromTime == $scope.toTime || $scope.fromTime < $scope.toTime){
			$scope.toTime += 1;
		}
		else if($scope.fromTime > $scope.toTime){
			var time = $scope.toTime;
			$scope.toTime = $scope.fromTime +1;
			$scope.fromTime = time;
		}
		var day = $scope.area[$scope.fromTime][$scope.selectedDayIndex].day;
		
		//삭제 정보 저장
		if($scope.selectedFunction != 1) {
			$scope.DelList.push({day_of_week : day, fromTime : $scope.fromTime, toTime : $scope.toTime, fn: $scope.selectedFunction});
		}
		
		/*
		$http.post("/tutoring/tutor/schedule/add_schedule/", {day_of_week : day, fromTime : $scope.fromTime, toTime : $scope.toTime, fn: $scope.selectedFunction}).success( function( response ) {
			$scope.schedule.week_schedule = response.week_schedule;
			$scope.schedule.week_schedule.splice(0,0,$scope.schedule.week_schedule[6]);
			$scope.schedule.week_schedule.splice(7,1);
			$scope.removeAllSelectedState($scope.selectedDayIndex);
			$scope.fromTime = -1;
			$scope.toTime = -1;
			$scope.selectedDayIndex = -1;
		});
		*/
		//$scope.AddList.push({day_of_week : day, fromTime : $scope.fromTime, toTime : $scope.toTime, fn: $scope.selectedFunction});
		$scope.removeAllSelectedState($scope.selectedDayIndex);
		$scope.fromTime = -1;
		$scope.toTime = -1;
		$scope.selectedDayIndex = -1;
		
	}
	
	//전체 스케줄 상태 체크
	$scope.checkScheduleTable = function() {
		$scope.AddList = [];
		var bSelected = true;
		
		for( var i = 0; i < 7; i++) { //일주일
			var from = -1;
			var to = -1;
			
			//체크된 부분
			for( var j = 0; j < 48; j++) { //하루 24시간
				
				if(bSelected) {
					if($scope.area[j][i].state == 1){
						from = j;
						bSelected = false;
					}
				} else {
					if($scope.area[j][i].state == 0){
						to = j;
						$scope.AddList.push({day_of_week:$scope.area[j][i].day, fromTime : from, toTime : to, fn: 1});
						bSelected = true;
						
					}
				}
			}
		}
		
		$scope.setSchedule($scope.AddList, $scope.DelList);
		
	};
	
	$scope.setSchedule = function(Alist, Dlist) {
		$http.post("/tutoring/tutor/schedule/set_schedule/", {addlist : Alist, dellist : Dlist}).success( function( response ) {
			$scope.schedule.week_schedule = response.week_schedule;
			$scope.schedule.week_schedule.splice(0,0,$scope.schedule.week_schedule[6]);
			$scope.schedule.week_schedule.splice(7,1);
			
			$scope.DelList = [];
			
			alert("Update Schedule");
			$scope.applyButton = true;
			$scope.updateLoading = false;
			
		});
	};
	
	$scope.applySchedule = function() {
		if( !$scope.applyButton ) return;
		
		$scope.applyButton = false;
		$scope.updateLoading = true;
		$scope.checkScheduleTable();
		$scope.initTableState.area = [];
		$scope.initTableState.bFlag = true;
	};
	$scope.cancelSchedule = function() {
		//삭제 이력 초기화
		$scope.DelList = [];
		
		//테이블 정보 초기화
		if(!$scope.initTableState.bFlag) {
			$scope.area = JSON.parse(JSON.stringify($scope.initTableState.area));
		}
	}
	
	$scope.addCalendarSchedule = function(){
		var intFrom = parseInt($scope.fromTime);
		var intTo = parseInt($scope.toTime);
		if(intFrom == intTo){
			alert("from = to");
			return;
		}
		if(intFrom > intTo){
			alert("from > to");
			return;
		}
		var dateString = String($scope.year);
		if($scope.month < 10){
			dateString += "0"+ $scope.month;
		}
		else{
			dateString += $scope.month;
		}
		
		if($scope.addDate < 10){
			dateString += "0"+ $scope.addDate;
		}
		else{
			dateString +=$scope.addDate;
		}
		dateString += getIndexToDay($scope.addDayIndex);
		console.log(dateString);//20160625F(sat)
		$http.post("/tutoring/tutor/schedule/add_delta_schedule/", { date : dateString, fromTime : intFrom, toTime : intTo, fn: 1}).success( function( response ) {
			$scope.monthSchedule = response.month_schedule;
			for(var i = 0; i < 48; i++){
				$scope.timeRow[parseInt($scope.addDate)][i] = $scope.setTimeRow(parseInt($scope.addDate), $scope.addDayIndex, i);
			}
			$scope.fromTime = -1;
			$scope.toTime = -1;
			$scope.addDate = -1;
		});
	}
	$scope.deleteSchedule = function(date, day, index){//날짜, 요일, 인덱스
		if(confirm("Delete " + getIndexToTimeFor24(index) + " - " + getIndexToTimeFor24(index+1) + " from your schedule ?")){
			var dateString = String($scope.year);
			if($scope.month < 10){
				dateString += "0"+ $scope.month;
			}
			else{
				dateString += $scope.month;
			}
			
			if(date < 10){
				dateString += "0"+ date;
			}
			else{
				dateString += date;
			}
			dateString += getIndexToDay(day);
			$http.post("/tutoring/tutor/schedule/delete_delta_schedule/", { date : dateString, fromTime : index, toTime : index+1, fn: 2}).success( function( response ) {
				$scope.monthSchedule = response.month_schedule;
				for(var idx = 0; idx < 48; idx++){
					$scope.timeRow[date][idx] = $scope.setTimeRow(date, day, idx);
				}
				$scope.addDate = -1;
			});
		}
		else{
			return;
		}
	}
	$scope.goPreviousMonth = function(){
		$http.post("/tutoring/tutor/schedule/get_prev_calendar/",{year: $scope.year, month: $scope.month}).success( function( response ) {
			$scope.month = response.month;
			$scope.year = response.year;
			$scope.monthInfo = response.month_info;
			$scope.monthSchedule = response.month_schedule;
			$scope.setMobileCalendar();
			setTimeout(function () {
				$scope.$apply();
			});
			$(".mobile-calendar-scroll-area").scrollTop(0);
			
		});
	}
	$scope.goNextMonth = function(){
		$http.post("/tutoring/tutor/schedule/get_next_calendar/",{year: $scope.year, month: $scope.month}).success( function( response ) {
			$scope.month = response.month;
			$scope.year = response.year;
			$scope.monthInfo = response.month_info;
			$scope.monthSchedule = response.month_schedule;
			$scope.setMobileCalendar();
			setTimeout(function () {
				$scope.$apply();
			});
			$(".mobile-calendar-scroll-area").scrollTop(0);
			
		});
	}
	
//	Copy Schedule
	$scope.area = [];
	$scope.initAreaValue = function(day, index){
		var time = "time";
		if(index%2 == 0){
			if(index/2 < 10){
				time = time + "0" + index/2 + "00";
			}
			else{
				time = time + index/2 + "00";
			}
		}
		else{
			if(index/2 < 10){
				time = time + "0" + ((index-1)/2) + "30";
			}
			else{
				time = time + ((index-1)/2) + "30";
			}
		}
		return time;
	}
	$scope.initAreaState = function(day, index){
		var time = $scope.area[index][day].value;
		$scope.area[index][day].day = $scope.schedule.week_schedule[day].day_of_week;
		$scope.area[index][day].index = index;
		if($scope.schedule.week_schedule[day][time] != undefined){
			return $scope.schedule.week_schedule[day][time];
		}
		else{
			return 0;
		}
		
	};
//	예약가능한 영역을 클릭할 때
	$scope.selectArea = function(index, day){
		if( $scope.initTableState.bFlag ) { //첫 테이블 정보 저장
			$scope.initTableState.area = JSON.parse(JSON.stringify($scope.area)); //값 복사 
			$scope.initTableState.bFlag = false;
		}
		
		if($scope.fromTime == undefined || $scope.fromTime == -1){
			//처음 영역을 클릭했을 때
			$scope.fromTime = index;
			$scope.selectedDayIndex = day;
			if($scope.area[index][day].state == 0){
				//빈 영역을 먼저 클릭 -> 스케줄 추가
				$scope.selectedFunction = 1;
				$scope.area[index][day].selectedState = 1;
			}
			else{
				//선택되어 있는 영역을 클릭 -> 스케줄 제거
				$scope.selectedFunction = 0;
				$scope.area[index][day].selectedState = 2;
			}
		}
		else{
			//처음 클릭 후 두번째 (영역을 마무리 할때) 
			if($scope.selectedDayIndex == day){
				//같은 날 영역을 클릭 
				$scope.toTime = index;
				//$scope.addSchedule();
				
				$scope.setAreaState(index, day);
				$scope.addSchedule();
				
			}
			else{
				//다른 영역을 클릭
				$scope.area[$scope.fromTime][$scope.selectedDayIndex].selectedState = 0;
				$scope.fromTime = index;
				$scope.selectedDayIndex = day;
				if($scope.area[index][day].state == 0){
					$scope.selectedFunction = 1;
					$scope.area[index][day].selectedState = 1;
				}
				else{
					$scope.selectedFunction = 0;
					$scope.area[index][day].selectedState = 2;
				}
			}
		}
	};
	$scope.removeAllSelectedState = function(day){
		for(var i = 0; i < 48 ; i++){
			$scope.area[i][day].selectedState = 0;
		}
	}
	$scope.checkArea = function(index, day){
		if($scope.fromTime != undefined && $scope.fromTime != -1){
			//첫 선택이 된 것이 없다면
			$scope.removeAllSelectedState($scope.selectedDayIndex);
		}
		
		if($scope.selectedDayIndex == day){
			if($scope.selectedFunction == 1){
				//추가되는 영역
				if($scope.fromTime > index){
					for(var i = index; i <= $scope.fromTime; i++){
						$scope.area[i][day].selectedState = 1;
					}
				}
				else{
					for(var i = $scope.fromTime ; i <= index; i++){
						$scope.area[i][day].selectedState = 1;
					}
				}
			}
			else{
				//제거되는 영역
				if($scope.fromTime > index){
					for(var i = index; i <= $scope.fromTime; i++){
						$scope.area[i][day].selectedState = 2;
					}
				}
				else{
					for(var i = $scope.fromTime; i <= index; i++){
						$scope.area[i][day].selectedState = 2;
					}
				}
			}
		}
		else{
			if($scope.selectedDayIndex != undefined && $scope.selectedDayIndex != -1){
				$scope.removeAllSelectedState($scope.selectedDayIndex);
				if($scope.selectedFunction == 1){
					//추가하는 상태
					//첫 선택한 영역 표시
					$scope.area[$scope.fromTime][$scope.selectedDayIndex].selectedState = 1;
				}
				else{
					//제거하는 상태
					//첫 선택한 영역 표시
					$scope.area[$scope.fromTime][$scope.selectedDayIndex].selectedState = 2;
				}
			}
		}
	};
	
	//선택된 영역 표시
	$scope.setAreaState = function(index, day) {
		if($scope.selectedFunction == 1){
			//추가되는 영역
			if($scope.fromTime > index){
				for(var i = index; i <= $scope.fromTime; i++){
					$scope.area[i][day].state = 1;
				}
			}
			else{
				for(var i = $scope.fromTime ; i <= index; i++){
					$scope.area[i][day].state = 1;
				}
			}
		}
		else{
			//제거되는 영역
			if($scope.fromTime > index){
				for(var i = index; i <= $scope.fromTime; i++){
					$scope.area[i][day].state = 0;
				}
			}
			else{
				for(var i = $scope.fromTime; i <= index; i++){
					$scope.area[i][day].state = 0;
				}
			}
		}
	};
	
	$scope.initAllArea = function(){
		for(var index=0; index < $scope.area.length; index++){
			$scope.area[index].A.state = $scope.initAreaState('A', index);//A : monday
			$scope.area[index].B.state = $scope.initAreaState('B', index);
			$scope.area[index].C.state = $scope.initAreaState('C', index);
			$scope.area[index].D.state = $scope.initAreaState('D', index);
			$scope.area[index].E.state = $scope.initAreaState('E', index);
			$scope.area[index].F.state = $scope.initAreaState('F', index);
			$scope.area[index].G.state = $scope.initAreaState('G', index);//G : sunday
		}
	};
	
	$scope.showWeek = function(){
		$scope.cancelTime();
		$scope.scheduleView = 0;
	}
	$scope.showCalendar = function(){
		$http.post("/tutoring/tutor/schedule/get_calendar/").success( function( response ) {
			$scope.timeRow = {};
			$scope.month = response.month;
			$scope.year = response.year;
			$scope.monthInfo = response.month_info;
			$scope.monthSchedule = response.month_schedule;
			$scope.scheduleView = 1;
		});
	}
	$scope.setTimeRow = function(day, weekend, index){
//		console.log("setTimeRow");
//		console.log($scope.monthSchedule);
		for(var i=0; i < $scope.monthSchedule.length; i++){
			if($scope.monthSchedule[i].date == day){
				if(getIndexToTime(index) == $scope.monthSchedule[i].time){
					if($scope.monthSchedule[i].type == 1){
						return 1;
					}
					else{
						return 0;
					}
				}
			}
//			console.log(i);
		}
//		console.log($scope.schedule.week_schedule[weekend]["time"+getIndexToTime(index)]);
		return $scope.schedule.week_schedule[weekend]["time"+getIndexToTime(index)];
	}
	$scope.getTbodyHeight = function(){
		var height = window.innerHeight - 365;
		var width = ($("#tr-shchedule").width()-92)/7;
		$scope.tdWidth = width;
		if(height < 721){
			$("#tr-shchedule tbody").height(height);
		}
	}
	$scope.setMobileCalendar = function(){
		$scope.mobileMonthInfo =[]
		for(var i=0; i<$scope.monthInfo.length; i++){
			$scope.mobileMonthInfo = $scope.mobileMonthInfo.concat($scope.monthInfo[i]);
		}
		var height = window.innerHeight - 275;
		$(".mobile-calendar-scroll-area").height(height);
	}

	$scope.showDeleteButtons = function(date){
		if($scope.deleteDate == -1 || $scope.deleteDate == undefined){
			$scope.deleteDate = date;
		}
		else if($scope.deleteDate == date){
			$scope.deleteDate = -1
		}
		else{
			$scope.deleteDate = date;
		}
	}
	
	$scope.showDateRangePopup = function(){
		$scope.dateRangePopup = 1;
		$scope.dateRangeContent = 0;
		$("#from-date").val("");
		$("#to-date").val("");
	}
	$scope.getDateRange = function(){
		var fromDate = $("#from-date").val();
		var toDate = $("#to-date").val();
		if(fromDate == "") {
			alert("from-date");
			return;
		}
		if(toDate == "") {
			alert("to-date");
			return;
		}
		console.log(fromDate);
		$scope.fromDateToTime = new Date(fromDate);
		$scope.toDateToTime = new Date(toDate);
		if($scope.fromDateToTime.getTime() > $scope.toDateToTime.getTime()){
			alert("from > to");
			return;
		}
		$scope.dateRangeContent = 1;
	}
	$scope.deleteDateRange = function(){
		$scope.dateRangeContent = 2;
		$scope.dynamic = 0;
		var num = 50;
		$scope.type = "info";
		$scope.progress = $interval(function(){
			if($scope.dynamic > 50){
				$scope.dynamic += parseInt(num * 0.1);
				num = parseInt(num * 0.9);
			}
			else{
				$scope.dynamic += 5;
			}
		}, 500);
		var fromDateString = $scope.fromDateToTime.format("yyyyMMdd");
		var toDateString = $scope.toDateToTime.format("yyyyMMdd");
		$http.post("/tutoring/tutor/schedule/delete_range_schedule/", { year : $scope.year , month : $scope.month, fromDate : fromDateString, toDate : toDateString}).success( function( response ) {
			$scope.monthSchedule = response.month_schedule;
			$scope.monthInfo = angular.copy($scope.monthInfo);
			setTimeout(function () {
				$scope.$apply();
			});
			$interval.cancel($scope.progress);
			$scope.type = "success";
			$scope.dynamic = 100;
			setTimeout(function(){
				$scope.dateRangePopup = 0;
			},1000);
			
		});
	}
	$scope.deleteDateRangeForMobile = function(){
		var fromDate = $("#mobile-from-date").val();
		var toDate = $("#mobile-to-date").val();
		$scope.fromDateToTime = new Date(fromDate);
		$scope.toDateToTime = new Date(toDate);
		if($scope.fromDateToTime.getTime() > $scope.toDateToTime.getTime()){
			alert("from > to");
			return;
		}
		var fromDateString = $scope.fromDateToTime.format("yyyyMMdd");
		var toDateString = $scope.toDateToTime.format("yyyyMMdd");
		$http.post("/tutoring/tutor/schedule/delete_range_schedule_mobile/", {fromDate : fromDateString, toDate : toDateString}).success( function( response ) {
			$("#from-date").val("");
			$("#to-date").val("");
			alert("Delete");
		});
	}
	
	$scope.showAvailability = function() {
		$http.post("/tutoring/tutor/schedule/available/").success( function( response ) {
			if (response.isSuccess == true) {
				$scope.is_available = response.is_available;
				$scope.schedule_comment = response.schedule_comment;
				$scope.scheduleView = 3;			
			}
		});
	}
	$scope.clickAvailable = function( available ) {
		if (available == "on") {
			$scope.is_available = 1;
		} else if (available == "off") {
			$scope.is_available = 0;
		}
		
	}
	$scope.saveAvailability = function() {
		$http.post("/tutoring/tutor/schedule/available/change/", {is_available : $scope.is_available}).success( function( response ) {
			if (response.isSuccess == true) {
				alert("Success");
				$scope.is_available = response.is_available;
				$scope.schedule_comment = response.schedule_comment;
				$scope.scheduleView = 3;			
			}
		});
	}
	$scope.saveScheduleCommnet = function() {
//		if ($scope.schedule_comment == null || $scope.schedule_comment == "") {
//			alert("Text");
//			return;
//		}
//		if ($scope.is_available == 0) {
//			$scope.schedule_comment += "I'm not looking for new students.\n" + $scope.schedule_comment;
//		}
		$http.post("/tutoring/tutor/schedule/available/comment/", {schedule_comment : $scope.schedule_comment}).success( function( response ) {
			if (response.isSuccess == true) {
				alert("Success");
				$scope.is_available = response.is_available;
				$scope.schedule_comment = response.schedule_comment;
				$scope.scheduleView = 3;			
			}
		});
	}
	
});
