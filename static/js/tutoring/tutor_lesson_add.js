/**
 * 	tutor_lesson_add.js
 */



tellpinApp.controller( "newLessonCtrl", ["$scope", "$http", "$location", "$window", function( $scope, $http, $location, $window ) {
	$scope.init = function( obj ) {
		console.log( obj );
		$scope.selectedColor = {};
		//$scope.display = obj.display;
		$scope.picker = false;
		$scope.speakList = obj.speak_list;
		$scope.toList = obj.toList;
		$scope.fromList = obj.fromList;
		$scope.courseImg = obj.courseImg;
		$scope.tabType = obj.tabType;
		$scope.currency = obj.user_currency[0];
		$scope.currency_user = obj.user_currency[1];
		$scope.setImg = {};
		for ( var i = 0; i < $scope.fromList.length; i++ ) {
			$scope.fromList[i].level_name = $scope.splitString( $scope.fromList[i].level );
		}
		for ( var i = 0; i < $scope.toList.length; i++ ) {
			$scope.toList[i].level_name = $scope.splitString( $scope.toList[i].level );
		}
//		$scope.toList = obj.toList;
		$scope.nameValid = false;
		$scope.descValid = false;

		$scope.packageTimeList = [{id:0, name: "N/a"}, {id:5, name: "5"}, {id:10, name: "10"}, {id:20, name: "20"}, {id:30, name: "30"}];

		if (obj.course) {
			$scope.course = obj.course;
			$scope.setImg.filepath = $scope.course.filepath;
			$scope.setImg.id = $scope.course.course_bgimg;
		} else {
			$scope.course = new Object();
			$scope.course.course_lang = 0;
			$scope.course.course_name = "";
			$scope.course.course_desc = "";
			$scope.course.course_level_from = 1;
			$scope.course.course_level_to = 1;
			$scope.course.course_bgimg = 0;
			
			$scope.course.course_has_30min = 0;
			$scope.course.course_has_60min = 1;
			$scope.course.course_has_90min = 0;
			
			$scope.course.sg_30min_price = 0;
			$scope.course.sg_60min_price = 0;
			$scope.course.sg_90min_price = 0;
			
			$scope.course.pkg_30min_times = $scope.packageTimeList[0].id;
			$scope.course.pkg_60min_times = $scope.packageTimeList[0].id;
			$scope.course.pkg_90min_times = $scope.packageTimeList[0].id;

			$scope.course.pkg_30min_price = 0;
			$scope.course.pkg_60min_price = 0;
			$scope.course.pkg_90min_price = 0;

			$scope.setImg = $scope.courseImg[0];
		}
	
		console.log($scope.course);
	}

	$scope.splitString = function( str ) {
		if ( str.split( ":" )[1] ) {
			return str.split( ":" )[1];
		}
		else {
			return str;
		}
	}

	$scope.showTooltip = function(element, tooltip, message){
		var top = $("."+element).offset().top + $("."+element).outerHeight() + 3 + "px";
		var left = $("."+element).offset().left + "px";
		$("#"+tooltip).text(message);
		$("#"+tooltip).css({"top": top, "left": left});
		$("#"+tooltip).css("display","block");
	}
	$scope.hideTooltip = function(tooltip){
		$("#"+tooltip).css("display","none");
	}
	
	$scope.costValidCheck = function( course ) {
		console.log("costValidCheck");
		var success = "";
		var fail = "fill the cost";
		if ( $scope.course.course_has_30min == 1 ) {
			if ( $scope.course.pkg_30min_times == 0){
				 if ($scope.course.sg_30min_price < 30 || !$scope.course.sg_30min_price || $scope.course.sg_30min_price > 1000 ) {
					 $scope.showTooltip("sg_30_price", "sg_30min_price-tooltip", "Please enter a value between 30 and 1000 TC.");
					 $(document).scrollTop(300);
					 return fail;
				 }
			} else {//package
				if ($scope.course.pkg_30min_price <= 0 || !$scope.course.pkg_30min_price ) {
					 $scope.showTooltip("pkg_30_price", "pkg_30min_price-tooltip", "Please enter a value between 30 and 1000 TC.");
					 $(document).scrollTop(300);
					return fail;
				}
			}
		}
		if ( $scope.course.course_has_60min == 1 ) {
			if ( $scope.course.pkg_60min_times == 0){
				 if ($scope.course.sg_60min_price < 30 || !$scope.course.sg_60min_price || $scope.course.sg_60min_price > 1000) {
					 $scope.showTooltip("sg_60_price", "sg_60min_price-tooltip", "Please enter a value between 30 and 1000 TC.");
					 $(document).scrollTop(300);
					 return fail;
				 }
			} else {//package
				if ($scope.course.pkg_60min_price <= 0 || !$scope.course.pkg_60min_price ) {
					 $scope.showTooltip("pkg_60_price", "pkg_60min_price-tooltip", "Please enter a value between 30 and 1000 TC.");
					 $(document).scrollTop(300);
					return fail;
				}
			}
		}
		if ( $scope.course.course_has_90min == 1 ) {
			if ( $scope.course.pkg_90min_times == 0){
				 if ($scope.course.sg_90min_price < 30 || !$scope.course.sg_90min_price || $scope.course.sg_90min_price > 1000) {
					 $scope.showTooltip("sg_90_price", "sg_90min_price-tooltip", "Please enter a value between 30 and 1000 TC.");
					 $(document).scrollTop(300);
					 return fail;
				 }
			} else {//package
				if ($scope.course.pkg_90min_price <= 0 || !$scope.course.pkg_90min_price ) {
					 $scope.showTooltip("pkg_90_price", "pkg_90min_price-tooltip", "fill the cost   !!!");
					 $(document).scrollTop(300);
					return fail;
				}
			}
		}
		
		console.log( "success ");

		return success;
	}
	
	$scope.saveLesson = function() {
		console.log("saveLesson");
		console.log($scope.course);
		if (  $scope.checkLength( 'name' ) && $scope.checkLength( 'desc' ) )
			return;
		
		$scope.courseValid = "";
		$scope.costValid = "";
		if ( !$scope.course.course_lang )
			$scope.courseValid += "select teach language\n";
		if ( !$scope.course.course_name || $scope.course.course_name == "" )
			$scope.courseValid += "input lesson title\n";
		if ( !$scope.course.course_desc || $scope.course.course_desc == "" )
			$scope.courseValid += "input lesson description\n";
		if ( !$scope.course.course_level_from )
			$scope.courseValid += "select lesson From level\n";
		if ( !$scope.course.course_level_to )
			$scope.courseValid += "select lesson To level\n";
		
		if ( $scope.course.course_level_from > $scope.course.course_level_to )
			$scope.courseValid += "To level higher than From level";
		
		$scope.costValid = $scope.costValidCheck( $scope.course ); 
		console.log( $scope.costValid );
		$scope.courseValid += $scope.costValid;
		if ( $scope.courseValid != "" ) {
			alert( $scope.courseValid );
			console.log( $scope.course );
			return;
		}

		$scope.course.course_bgimg = $scope.setImg.id;
		
		console.log( $scope.course );
		$http.post( "/tutoring/tutor/make_lesson/", { course :  $scope.course, type : "normal"  } )
		.success( function ( result ) {
			console.log( result );
			if ( result.isSuccess == 1) {
				//alert("save");
				$window.location.href = "/tutorlessons/courses/";				
			}
			else if ( result.isSuccess == 2 ) {
				alert( 'You can create up to six lessons.' );
			}
		});
	}
	
	$scope.lessonBack = function() {
		$window.location.href = "/tutorlessons/courses/";
	}
	
	$scope.changeTime = function( time ) {
		if (time == "30" && $scope.course.pkg_30min_times == 0) {
			$scope.course.pkg_30min_times = 0;
			$scope.course.pkg_30min_price = 0;
		} else if (time == "60" && $scope.course.pkg_60min_times == 0) {
			$scope.course.pkg_60min_times = 0;
			$scope.course.pkg_60min_price = 0;
		} else if (time == "90" && $scope.course.pkg_90min_times == 0) {
			$scope.course.pkg_90min_times = 0;
			$scope.course.pkg_90min_price = 0;
		} else {
			console.log("changeTime else!!!");
		}
	}
	
	$scope.checkHasCourse = function( col ) {
		var temp = "course_has_" + col + "min";
		if ($scope.course[temp] == false) {
			if (col == "30") {
				$scope.course.course_has_30min = false;
				$scope.course.sg_30min_price = 0;
				$scope.course.pkg_30min_times = 0;
				$scope.course.pkg_30min_price = 0;
			} else if (col == "90") {
				$scope.course.course_has_90min = false;
				$scope.course.sg_90min_price = 0;
				$scope.course.pkg_90min_times = 0;
				$scope.course.pkg_90min_price = 0;
			} else {
				console.log("checkHasCourse else!!!")
			}
		}
	}
	
	$scope.getCourseImg = function( type ) {
		if ( $scope.tabType == type ) return;
		$http.post( '/tutorlessons/newcourse/img/', { 'type' : type } )
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				$scope.courseImg = result.courseImg;
				$scope.tabType = result.tabType;
			}
		});
	}
	
	$scope.selectImg = function( obj ) {
		$scope.setImg = obj;
	}
	
	// lesson name, description 길이 검사
	$scope.checkLength = function( type ) {
		// lesson name
		if ( type == 'name' ) {
			if ( $scope.course.course_name && ( $scope.course.course_name.length < 5 || $scope.course.course_name.length > 25 ) ) {
				$scope.nameValid = true;
			}
			else {
				$scope.nameValid = false;
			}
			return $scope.nameValid;
		}
		else {
			if ( $scope.course.course_desc && ( $scope.course.course_desc.length < 10 || $scope.course.course_desc.length > 1000 ) ) {
				$scope.descValid = true;
			}
			else {
				$scope.descValid = false;
			}
			return $scope.descValid;
		}
	}
}]);