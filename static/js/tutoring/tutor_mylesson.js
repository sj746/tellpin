/**
 * 	tutor_mylesson.js
 */

tellpinApp.controller( "myLessonCtrl", ["$scope", "$window", "$http", function( $scope, $window, $http ) {
	$scope.init = function( obj ) {
		console.log( obj );
		$scope.isTutor = obj.is_tutor;
		console.log( $scope.isTutor );
		$scope.course = obj.course_list;
		$scope.display = obj.display;
		$scope.lessonHoverToggle = new Array( $scope.course.length );
		$scope.has_trial = obj.has_trial;
		$scope.trial_price = obj.trial_price;
		$scope.displayTrial = $scope.trial_price; // 화면 표시될 trial 금액
		for ( var i = 0; i < $scope.course.length; i++ ) {
			$scope.lessonHoverToggle[i] = false;
		}
//		for ( var i = 0; i < $scope.course.length; i++ ) {
//			$scope.course[i].from_level = $scope.levelSplit( $scope.course[i].from_level );
//			$scope.course[i].to_level = $scope.levelSplit( $scope.course[i].to_level );
//		}
		if ( $scope.has_trial == 0 ) {
			$scope.isTrial = false;
		}
		else {
			$scope.isTrial = true;
		}
//		$scope.lesson = {};
		/*angular.element( ".lesson-area" ).masonry({
			itemSelector : ".lesson-common",
			columnWidth : 354
		});*/
		angular.element( '#trialModal' ).on( 'hidden', function() {
			console.log( 'hidden' );
			if ( $scope.trial_price == 0 || $scope.trial_price == undefined ) {
				$scope.trial_price = $scope.oldTrial;
			} 
		});
	}
	
	$scope.levelSplit = function( str ) {
		return str.split( ":" )[1];
	}
	
	$scope.removeModal = function( course_id ) {
		$scope.removeId = course_id;
		console.log( $scope.removeId );
		angular.element( "#lessonDeleteModal" ).modal();
	}
	
	// 강의 Hover 처리
	$scope.lessonHover = function( type, idx ) {
		if ( type == "on" ) {
			$scope.lessonHoverToggle[idx] = true;			
		}
		else {
			$scope.lessonHoverToggle[idx] = false;
		}
	}
	
	$scope.removeModalClose = function() {
		angular.element( "#lessonDeleteModal" ).modal( "hide" );
	}
	
	$scope.deleteLesson = function() {
		$http.post( "/tutoring/tutor/deletelesson/", { course_id : $scope.removeId } )
		.success( function( result ) {
			console.log( result );
			if ( result.isSuccess == 0 ) {
				alert( "you can't remove lesson" );
			}
			else {
				$scope.course = result.course_list;
			}
			angular.element( "#lessonDeleteModal" ).modal( "hide" );
		});
		
	}
	
	$scope.trialMobileModal = function() {
		angular.element( "#trialMobileModal" ).modal();
	}
	
	$scope.addLesson = function() {
		$window.location.href = "/tutorlessons/newcourse/";		
	}
	
	$scope.trialModal = function() {
		$scope.trial_price = $scope.displayTrial;
		angular.element( "#trialModal" ).modal();
	}
	
	$scope.$watch( "has_trial", function( newValue, oldValue ) {
		if ( newValue == oldValue ) 
			return;
		
		if ( $scope.has_trial == 0 && $scope.isTrial == true) {
			console.log("$scope.has_trial == 0");
			$scope.toggleTrial();
		}
		if ( $scope.has_trial == 1 && $scope.isTrial == false ) {
			if ( $scope.trial_price == 0 || ( $scope.trial_price > 700 || $scope.trial_price < 10 )  ) {
				alert( "input trial cost!" );
				$scope.has_trial = 0;
			}else{
				console.log("$scope.has_trial == 1");
				$scope.toggleTrial();
			}
		}
	});
	
	$scope.closeTrialMobile = function() {
		angular.element( "#trialMobileModal" ).modal( "hide" );
	}
	
	$scope.toggleTrial = function() {
		$scope.saveTrial();
	}
	
	$scope.saveTrial = function() {
		console.log("saveTrial");
		if ( $scope.trial_price > 700 || $scope.trial_price < 10 ) {
			alert( "Please enter a value between 10 and 700 TC.");
			return;
		}

		console.log( $scope.trial_price );
		$http.post( '/tutoring/tutor/newtrial/', { trial_price : $scope.trial_price, has_trial : $scope.has_trial } )
		.success( function( result ) {
			if ( result.isSuccess.isSuccess == true ) {
				console.log( result );
				$scope.has_trial = result.isTrial.has_trial;
				$scope.displayTrial = result.isTrial.trial_price;
				if ($scope.has_trial == 1) {
					$scope.isTrial = true;
				} else {
					$scope.isTrial = false;
				}
			}
			angular.element( "#trialModal" ).modal( "hide" );
			angular.element( "#trialMobileModal" ).modal( "hide" );
			//$window.location.reload();
		});
	}
	
	$scope.changeTrial = function() {
//		$scope.lesson.language = 0;
		console.log($scope.trial_price);
		if ( $scope.trial_price == 0 || $scope.trial_price == undefined) {
			alert( "Please enter a value between 10 and 700 TC.");
			$scope.trial_price = $scope.oldTrial;
			return;
		}

		console.log( $scope.trial_price );
		console.log( $scope.has_trial );
		$http.post( '/tutoring/tutor/changetrial/', { trial_price : $scope.trial_price, has_trial : "1" } )
		.success( function( result ) {
			if ( result.isSuccess.isSuccess == true ) {
				console.log( result );
				$scope.has_trial = result.isTrial.has_trial;
				$scope.displayTrial = result.isTrial.trial_price;
				if ($scope.has_trial == 1) {
					$scope.isTrial = true;
				} else {
					$scope.isTrial = false;
				}
			}
			angular.element( "#trialModal" ).modal( "hide" );
			angular.element( "#trialMobileModal" ).modal( "hide" );
			//$window.location.reload();
		});
	}
	
	$scope.bundleCheck = function( course ) {
		if ( course.pkg_30min_times > 0 || course.pkg_60min_times > 0 || course.pkg_90min_times > 0 )
			return true;
		else
			return false;
	}
}]);