
tellpinApp.controller( "problems_listCtrl", function( $scope, $http, $timeout, $sce, $window ) {
	angular.element(document).ready(function(){

	});

	$scope.problemString = {};
	$scope.problemString = {
			"1":"Tutor was absent",
			"2":"Tutee was absent",
			"3":"Other",
			"4":"Return all Tellpin Credits of the lesson fee to the tutee",
			"5":"Reschedule the lesson",
			"6":"Transfer all Tellpin Credits of the losson fee to the tutor",
			"7":"Return partial Tellpin Credits of the lesson fee to the tutee",
			"8":"Return all Tellpin Credits of the lesson fee to the tutee",
			"9":"Transfer all Tellpin Credits of the losson fee to the tutor",
	};

	$scope.tutorStrings = {};
	$scope.tutorStrings = {
		"Problemreported1": "You reported that there was a problem with this lesson and are awaiting the tutee's response. " +
							"Please keep negotiating with the tutee till you reach an agreement on how to resolve the problem. " +
							"To have a conversation with the tutee, please use the Lessen Messenger on the right side . \n" +
							"- Have you changed your mind? To edit your statement, click on 'Edit statement'. \n" +
							"If a mutually agreed-upon solution doesn't exist by ",
		"Problemreported2": ", the lesson will be regarded as 'In Dispute'. Tellpin staff will arbitrate the dispute and come up with the final solution to resolve the problem within 5 working days. ",

		"Problemreported3": "The tutee reported that there was a problem with this lesson. To have a conversation with the tutee, please use the Lessen Messenger on the right side. \n"+
							"- If you agree with the tutee's request, click on 'Accept' \n" +
							"- If you don't agree with the tutee's request and are willing to make a statement, click on 'Make statement'.  You can keep negotiating with the tutee till you reach an agreement by ",
	};
	
	$scope.tuteeStrings = {};
	$scope.tuteeStrings = {
			"Problemreported1": "The tutor reported that there was a problem with this lesson. To have a conversation with the tutor, please use the Lessen Messenger on the right side. \n" +
								"- If you agree with the tutor's request, click on 'Accept'. \n" +
								"- If you don't agree with the tutor's request and are willing to make a statement, click on 'Make statement'. You can keep negotiating with the tutee till you reach an agreement by ",

			"Problemreported2": "You reported that there was a problem with this lesson and are awaiting the tutor's response. Please keep negotiating with the tutee till you reach an agreement on how to resolve the problem. To have a conversation with the tutor, please use the Lessen Messenger on the right side \n" +
								"- Have you changed your mind? To edit your statement, click on 'Edit statement'. \n" +
								"If a mutually agreed-upon solution doesn't exist by ",
			"Problemreported3": ", the lesson will be regarded as 'In Dispute'. Tellpin staff will arbitrate the dispute and come up with the final solution to resolve the problem within 5 working days.",
	};

	$scope.init = function(){
		console.log( $scope.data.rv_data[0] );
		$scope.list = $scope.data.rv_data[0];
//		$scope.reporter = $scope.data.rv_data[0].reporter_detail;
//		$scope.reporter.accept_message = $sce.trustAsHtml($scope.reporter.accept_message);
//		console.log($scope.reporter.accept_message);
		$scope.problems = $scope.data.rv_data[0].list;
		console.log($scope.problems);

		for (var i = 0; i < $scope.problems.length; i++) {
			$scope.problems[i].claim_text = $scope.problemString[$scope.problems[i].claim_text];
			$scope.problems[i].resolve_type = $scope.problemString[$scope.problems[i].resolve_type];
		}
		
		$scope.getStatusString();
	}

	$scope.getStatusString = function() {
		console.log("statusString");
		if ($scope.list.is_tutor) {//tutor
			if ($scope.problems.length == 1) {
				if ($scope.problems[0].reportor == $scope.list.tutor_id) {
					return statusString = 	$scope.tutorStrings.Problemreported1 + $scope.problems[0].status_time + $scope.tutorStrings.Problemreported2;
				} else if ($scope.problems[0].reportor != $scope.list.tutor_id) {
					return statusString = 	$scope.tutorStrings.Problemreported3 + $scope.problems[0].status_time;
				}
			} else {
//				if ($scope.problems.length%/2 == 0) {
//					
//				} else {
//					
//				}
				
			}

		} else {//tutee
			if ($scope.problems.length == 1) {
				if ($scope.problems[0].reportor == $scope.list.tutee_id) {
					return statusString = 	$scope.tuteeStrings.Problemreported2 + $scope.problems[0].status_time + $scope.tuteeStrings.Problemreported3;
				} else if ($scope.problems[0].reportor != $scope.list.tutee_id) {
					return statusString = 	$scope.tuteeStrings.Problemreported1 + $scope.problems[0].status_time;
				}
			} else {
				
			}
		}
 		
	}

	$scope.clickAccept = function(){
		console.log("clickAccept");
		var _accept_type = $scope.reporter.accept_id;
		var _rvid = $scope.list.rvid;
		console.log(_accept_type);
		console.log(_rvid);
		//accept 처리
		$http.post( "/tutorlessons/problem/accept/", {type: _accept_type, rvid: _rvid}).success(function(response) {
			if(response.isSuccess == 1){
				location.reload();
			}
			
		});
		
	}
	$scope.clickDecline = function(){
		console.log("clickDecline");
		var _rvid = $scope.list.rvid;
		console.log(_rvid);
		
		location.href = "/tutorlessons/problem/decline/"+_rvid;
//		//accept 처리
//		$http.post( "/tutor/problem/decline/", {rvid: _rvid}).success(function(response) {
//			if(response.isSuccess == 1){
//				console.log(response);
//				location.href = "/tutor/tutoring/details/problem/"+$scope.list.rvid;
//			}
//			console.log(response.isSuccess);
//		});
	}

	$scope.goBack = function(){
		$window.history.back();
	}
});
