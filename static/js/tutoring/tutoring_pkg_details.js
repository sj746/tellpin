tellpinApp.controller( "pkgdetailsCtrl", function( $scope, $http, $timeout, $window ) {

	$scope.init = function( data ){
		console.log(data);
		$scope.ratings = 5;
		$scope.list = data.rv_data;
		$scope.getMessage();
		console.log( $scope.list );
		$scope.setCurrentTime();
		//$scope.$parent.checkMessage();
		
	}
	$scope.goURL = function(url){
		$window.location.href = (url);
		$window.event.stopPropagation();
	}
	$scope.anyProblem = function(item){

		if( item.status == 2 || item.status == 6){
			//TODO 페이지 이동
			location.href = "/tutorlessons/lesson/problem/"+item.rvid;
			return;
		}
	}
	$scope.getMessage = function() {
		$scope.messageModel = "";
		var to_user_id = 0;
		
		if( $scope.list.is_tutor == 1 ){
			console.log( $scope.list.tutee_id );
			to_user_id = $scope.list.tutee_id;
		}else{
			console.log( $scope.list.tutor_id );
			to_user_id = $scope.list.tutor_id;
		}
		var rvid = $scope.list.rvid;
		console.log( $scope.list.is_tutor );
		console.log( $scope.list.tutor_id);
		console.log( $scope.list.tutee_id);
		console.log('to_user_id: '+to_user_id);
		$http.post( '/tutor/getmessage/', { 'to_user_id' : to_user_id, 'rvid':rvid})
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
//				console.log( result.message );
				$scope.message = result.message;
				$scope.$broadcast('dataloaded');
				var elem = angular.element( '#message' );
//				$( '#messageModal' ).modal();
				console.log( 'get message success : ' );
				console.log( elem[0].scrollHeight );
				console.log(result);
				$scope.$parent.checkMessage();
			}
		});
		//angular.element( '#message' ).scrollTop = 800;
		$timeout(function(){
			console.log('timeout()');
			console.log( $("#message")[0].scrollHeight );

			console.log('timeout2()');
			$("#message").scrollTop( $("#message")[0].scrollHeight );

			console.log('timeout3()');
		},450);
	
	}
	$scope.$on('dataloaded', function(){
		$("#message").scrollTop($("#message")[0].scrollHeight);
	})
	// 메세지 전송
	$scope.sendMessage = function() {
		var to_user_id = 0;
		if( $scope.list.is_tutor == 1 ){
			to_user_id = $scope.list.tutee_id;
		}else{
			to_user_id = $scope.list.tutor_id;
		}
		console.log('to_user_id: '+to_user_id);
		console.log( $scope.messageModel );
		if ( $scope.messageModel.length > $scope.messageLength || !$scope.messageModel ) {
			console.log( 'not save' );
			return;
		}
		else {
			$http.post( '/tutor/newmessage/', { 'to_user_id' : to_user_id, 'message' : $scope.messageModel, 'rvid': $scope.list.rvid })
			.success( function( result ) {
				if ( result.isSuccess == 1 ) {
					$scope.message = result.message;
					$scope.messageModel = "";
					console.log( $scope.message );
					
					$timeout(function(){
						console.log('timeout()');
						console.log( $("#message")[0].scrollHeight );

						console.log('timeout2()');
						$("#message").scrollTop( $("#message")[0].scrollHeight );

						console.log('timeout3()');
					},450);
				}
			});
		}
	}
	
	
	//Remaining Time Caculate
	$scope.setCurrentTime = function(){ 
		var now = new Date();
		$scope.currentUTCTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
		$scope.changeTimezone();
		$timeout($scope.setCurrentTime, 1000);
	};
	//Remaining Time Caclute ( timer )
	$scope.changeTimezone = function(){
		if ($scope.list.from_date_time != null){
			var displayTime = new Date($scope.currentUTCTime.getTime());
			var t_time = $scope.list.from_date_time;
			var startTime = new Date(t_time.substr(0,4), parseInt(t_time.substr(5,2))-1, t_time.substr(8,2), t_time.substr(11,2), t_time.substr(14,2), t_time.substr(17,2));
			
			
	//		console.log('lesson time: ' + startTime);
	//		console.log('current time: ' + displayTime);
	//		console.log(t_time.substr(0,4), t_time.substr(5,2), t_time.substr(8,2), t_time.substr(11,2), t_time.substr(14,2), t_time.substr(17,2));
			
			if( $scope.list.from_date_time != null){
				$scope.delta = Math.floor( (startTime - displayTime) / 1000 ) ;
				$scope.currentTime = displayTime.format("yyyy.MM.dd hh:mm:ss a/p ");
				
	//			console.log($scope.list.from_date_time);
	//			console.log( $scope.delta );
				if ($scope.delta < 0){
					$scope.remainingTime = "ㅡ";
					$(".time").css("display", "none");
				}else{
					$scope.remainingTime = Math.floor($scope.delta/(60*60*24)) +"일 " + Math.floor(($scope.delta%(60*60*24))/60/60) + "시간 " + Math.floor(($scope.delta%(60*60*24)%(60*60))/60) + "분 " + Math.floor((($scope.delta%(60*60*24))%(60*60))%60) + "초";
				}
			}
		}else{
			$scope.remainingTime = "ㅡ";
		}
	}
	//button action 
	$scope.clickBtnAction = function( action, rvid ){
		//TODO 
		//상황별 액션 처리. 
		//0: Not Scheduled, 1:Requested, 2: Need Confirmation, 3:Declined, 4:Canceled
		//5: Confirmed, 6: In Problem, 7: Refunded, 8: Rescheduled, 9: Approved / Upcoming
	
		console.log("clickBtnAction");
		console.log("action = " + action, ", rvid = " + rvid);
//		console.log("pkg_status = " + $scope.list.pkg_status);
		if( $scope.list.is_tutor == 1 ){
			//feedback
			if( $scope.list.status == 5 && action == "Leave a feedback" && type == undefined){
				$scope.openPopFeedback();
				return;
			}else if($scope.list.status == 5 && action == "Leave a feedback"){
				$("#myModal2").modal('hide');
				var _data = {};
				_data["ratings"] = 0;
				_data["comment"] = $("#feedback_comment").val();
				$http.post( "/tutor/tutoring/commit/", { command : action, action : action, rvid : rvid, isTutor: $scope.list.is_tutor, type:'details', data:_data }).success(function(response) {
					if( response.isSuccess != '0') {
						console.log(response);
						$scope.list = response.rv_data[0];
						//$scope.rv_list = response.rv_data.entries
						//$scope.rv_data = response.rv_data
					}
					
				});
				return;
			}
		}
		else{//tutee
			//confirm
			if( $scope.list.status == 2 && action == "Confirm" && type == undefined){
				$scope.openPopConfirm();
				return;
			}else if($scope.list.status == 2 && action == "Confirm"){
				$("#myModal").modal('hide');
				var _data = {};
				_data["ratings"] = $scope.ratings;
				_data["comment"] = $("#confirm_comment").val();
				$http.post( "/tutor/tutoring/commit/", { command : action, action : action, rvid : rvid, isTutor: $scope.list.is_tutor, type:'details', data:_data }).success(function(response) {
					if( response.isSuccess != '0') {
						console.log(response);
						$scope.list = response.rv_data[0];
						
						//$scope.rv_list = response.rv_data.entries
						//$scope.rv_data = response.rv_data
					}
					
				});
				return;
			}else if(action == "refund") {
				if (true == $scope.checkPassword) {
					$scope.closeModal();
					$http.post( "/tutor/tutoring/commit/", { command : action, action : action, rvid : rvid, isTutor: $scope.list.is_tutor, type:'details', data:_data }).success(function(response) {
						if( response.isSuccess != '0') {
							console.log(response);
							$scope.list = response.rv_data[0];
						}
					});
					return;
				}
			}
		}
		if( $scope.list.status == 2 && action == "Report a Problem"){
			//TODO 페이지 이동
			location.href = "/tutorlessons/lesson/problem/"+$scope.list.rvid;
			return;
		}
		if( $scope.list.status == 6 && action == "Reply"){
			//TODO 페이지 이동
			location.href = "/tutorlessons/lesson/problem/"+$scope.list.rvid;
			return;
		}
//		var display = $scope.list.is_tutor == 1 ? 1 : 2;
		$http.post( "/tutoring/commit/", { command : action, action : action, rvid : rvid, isTutor: $scope.list.is_tutor, type:'details' }).success(function(response) {
			if( response.isSuccess != '0') {
				console.log('is Success!!');
				console.log(response);
				console.log('test1');
				if( response.rv_data.add_schedule != undefined) {
					console.log('?');
					location.href = response.rv_data.add_schedule;
					console.log('??');
					return;
				}
				console.log('test2'); 
				console.log(response.rv_data.add_schedule);
				console.log('test3');
				$scope.list = response.rv_data[0];
				
				//$scope.rv_list = response.rv_data.entries
				//$scope.rv_data = response.rv_data
			}
			
		}).error(function(response){
			console.log( 'erorr');
			alert(response);
		});
	}
	//Confirm popup
	$scope.openPopConfirm = function(){
		$("#myModal").modal();
	}
	//Feedback popup
	$scope.openPopFeedback = function(){
		$("#myModal2").modal();
	}
	//click ratings
	$scope.clickRatings = function(num){
		$scope.ratings = num;
		console.log(num);
		$(".ratings > img").attr('src', '/static/img/tutor/icon_star_empty.png');
		for(var i=0; i<num; i++){
			$(".star"+(i+1)).attr('src', '/static/img/tutor/icon_star_full.png');
		}
	}
	$scope.checkLengthConfirmModal = function(){
		var len = $(".confirm-area").val().length;
		var txt = $(".txt-len").text( len + ' / 400 characters');
	}
	$scope.checkLengthFeedbackModal = function(){
		var len = $(".confirm-area2").val().length;
		var txt = $(".txt-len2").text( len + ' / 400 characters');
	}
	
	$scope.openModal = function( modal ) {
		console.log("opnemodal");
		if (modal == 'refund') {
			angular.element( '#refundModal' ).modal();
//			$("#password").val();
			$(".current-password-error").text('');
		}
	}
	
	$scope.closeModal = function() {
		console.log("closeModal");
		$("#refundModal").modal('hide');
	}

	$scope.checkPassword = function(){

		//event.preventDefault();
		var password = $("#password").val();

		console.log( password );
		//TODO
		obj = {
				'pwd': password
		}
		$http.post("/user_auth/ajax_check_password/", {obj:obj}).success(function(response) {
			if(response.isSuccess){
				console.log('success');
				$(".current-password-error").text('');
				return true;
			}else{
				console.log('fail');
				$(".current-password-error").text('Wrong password!!');
				return false;
			}
		});
	}

});
