
tellpinApp.controller( "problemCtrl", function( $scope, $http, $timeout, $window ) {
	angular.element(document).ready(function(){
	});

	$scope.tutorStrings = {};
	$scope.tutorStrings = {
		"NeedConfirmation1": "The lesson time has passed. As long as there isn't any problem, the tutee will confirm that the lesson is completed. If you'd like to report a problem, click on 'Report a problem'. If you do not respond by",
		"NeedConfirmation2": ", the lesson will be automatically regarded as completed.",

		"Problemreported1": "You reported that there was a problem with this lesson and are awaiting the tutee's response. " +
							"Please keep negotiating with the tutee till you reach an agreement on how to resolve the problem. " +
							"To have a conversation with the tutee, please use the Lessen Messenger on the right side . \n" +
							"- Have you changed your mind? To edit your statement, click on 'Edit statement'. \n" +
							"If a mutually agreed-upon solution doesn't exist by ",
		"Problemreported2": ", the lesson will be regarded as 'In Dispute'. Tellpin staff will arbitrate the dispute and come up with the final solution to resolve the problem within 5 working days. ",
		"Problemreported3": "The tutee reported that there was a problem with this lesson. To have a conversation with the tutee, please use the Lessen Messenger on the right side. \n"+
							"- If you agree with the tutee's request, click on 'Accept' \n" +
							"- If you don't agree with the tutee's request and are willing to make a statement, click on 'Make statement'.  You can keep negotiating with the tutee till you reach an agreement by ",
	};
	
	$scope.tuteeStrings = {};
	$scope.tuteeStrings = {
			"NeedConfirmation1": "The lesson time has passed. As long as there isn't any problem, please click 'Confirm' button and leave a feedback for your tutor. If there was a problem, click on 'Report a problem'. If you do not respond by ",
			"NeedConfirmation2": ", the lesson will be automatically regarded as completed.",

			"Problemreported1": "The tutor reported that there was a problem with this lesson. To have a conversation with the tutor, please use the Lessen Messenger on the right side. \n" +
								"- If you agree with the tutor's request, click on 'Accept'. \n" +
								"- If you don't agree with the tutor's request and are willing to make a statement, click on 'Make statement'. You can keep negotiating with the tutee till you reach an agreement by ",
			"Problemreported2": "You reported that there was a problem with this lesson and are awaiting the tutor's response. Please keep negotiating with the tutee till you reach an agreement on how to resolve the problem. To have a conversation with the tutor, please use the Lessen Messenger on the right side \n" +
								"- Have you changed your mind? To edit your statement, click on 'Edit statement'. \n" +
								"If a mutually agreed-upon solution doesn't exist by ",
			"Problemreported3": ", the lesson will be regarded as 'In Dispute'. Tellpin staff will arbitrate the dispute and come up with the final solution to resolve the problem within 5 working days.",
	};

	$scope.init = function(){
		console.log( 'init() ');
		console.log( $scope.data );
		$scope.list = $scope.data.rv_data[0];
		$scope.makeStatusString();
		console.log($scope.list);
		$scope.problem = null;
	}

	$scope.makeStatusString = function() {
		console.log("makeStatusString");
		if ($scope.list.is_tutor) {
			if ($scope.list.status_name == "Need confirmation") {//Need confirmation
				return $scope.statusString = $scope.tutorStrings.NeedConfirmation1 + $scope.list.status_time + $scope.tutorStrings.NeedConfirmation2;
			} else if ($scope.list.status_name == "Problem reported") {//Problem reported
				return $scope.statusString = $scope.tutorStrings.Problemreported1 + $scope.list.status_time + $scope.tutorStrings.Problemreported2;
			} else {
				return $scope.statusString;
			}
		} else{//tutee
			if ($scope.list.status_name == "Need confirmation") {//Need confirmation
				return $scope.statusString = $scope.tuteeStrings.NeedConfirmation1 + $scope.list.status_time + $scope.tuteeStrings.NeedConfirmation2;
			} else if ($scope.list.status_name == "Problem reported") {//Problem reported
				return $scope.statusString = $scope.tuteeStrings.Problemreported1 + $scope.list.status_time;
			} else {
				return $scope.statusString;
			}
		}
	}

	$scope.clickSubmitProblem = function(){
		if( !checkValidationSubmit($scope.list.max_coin) ) {
			return;
		}
		if(confirm("Are you sure you want to report a problem?")){
			var claim_text=$("#what_problem option:selected").val();
			var claim_detail=$("#details").val();
			var claim_fid=$scope.upload_file;
			var resolve_type=$("#resolve option:selected").val();
			var resolve_tc = $("#amount").val();
			var display = $scope.list.is_tutor == 1 ? 1 : 2;
			var reportor = $scope.data.reporter_id;
			console.log(claim_text);
			console.log(claim_detail);
			console.log(claim_fid);
			console.log(resolve_type);
			console.log(resolve_tc);
			console.log(reportor);
			_data={};
			_data["claim_text"]=claim_text;
			_data["claim_detail"]=claim_detail;
			_data["claim_fid"]=claim_fid;
			_data["resolve_type"]=resolve_type;
			_data["resolve_tc"]=resolve_tc;
			_data["display"]=display;
			_data["reportor"]=reportor;
			_data["reportee"]=$scope.list.reportee;
			_data["is_reporter"]=$scope.list.is_reporter;
			console.log(_data);
			$http.post( "/tutoring/commit/", { command : "Need confirmation", action: "Report a problem", rvid : $scope.list.rvid, isTutor: display, type:'details', data:_data }).success(function(response) {
				if(response.isSuccess == 1){
					location.href = "/tutorlessons/lesson/problem/"+$scope.list.rvid;
				}
				
			});
		}
	}

	$scope.attachFile = function(){
		console.log('attachfile');
		$("#filename").click();
	}
	$scope.deleteFile = function(){
		console.log('deletefile');
		$("#filename").val("");
		$scope.file_name = "";
		$scope.upload_file = null;
	}
	$scope.checkValidCoin = function(){
		amount = $("#amount").val();
		
		if( amount < 1 || amount > $scope.list.max_coin){
			$("#amount").val('');
		}
	}
	$scope.submitPersonalFile = function(files){
		console.log('submitPersonalFile()');
		console.log(files);
		$scope.file_name = files[0].name;
		
		var fileName = new FormData();
		fileName.append("problem",files[0]);
		$http.post("/tutor/apply/upload_file/", fileName,{
			withCredentials: true,
	        headers: {'Content-Type': undefined },
	        transformRequest: angular.identity
		}).success(function(response){
			console.log(response);
			$scope.upload_file = response.photo_filename;
			
//			$scope.registration.personal.profile_path = response.profile_path;
//			$scope.registration.personal.profile_filename = response.profile_filename;
		});
	}

	$scope.checklength = function(){
		var len = $(".confirm-area").val().length;
		var txt = $(".txt-len").text( len + ' / 1000 characters');
	}

	$scope.changeProblem = function() {
		console.log("changeProblem");
		$scope.problem = $("#what_problem option:selected").val();
		console.log($scope.problem);
	}

	$scope.goBack = function(){
		$window.history.back();
	}
});

function howOptionList(){
	console.log("howOptionList");
	var _id = $("#resolve").val();
	console.log(_id);
	
	if( _id == 2 || _id == 3){
		$(".party-agree").css('display', 'inline');
	}else{
		$(".party-agree").css('display', 'none');
	}
	if( _id == 7){
		$("#amount-coin").css('display', 'block');
	}
	else{
		$("#amount-coin").css('display', 'none');
	}
}

function changeCoin(max_coin){
	var coin = $("#amount").val();
	coin = parseInt(coin);
	console.log(coin);
	console.log(max_coin);
	
	if(coin > 0 && coin <= max_coin){
		$(".min-max").css('display', 'none');
		return true;
	}else{
		$(".min-max").css('display', 'inline');
		return false;
	}
}
function checkValidationSubmit(max_coin){
	var how_id = $("#resolve").val();
	max_coin = parseInt(max_coin);
	if( how_id == 3){
		if( !coin_change(max_coin) ){
			console.log('invalid');
			$("#amount").focus();
			return false;
		}
	}
	var what_problem = $("#what_problem").val();
	if( what_problem == 0 ){
		console.log( "please, select one " );
		$("#what_problem").focus();
		return false;
	}
	
	var resolve = $("#resolve").val();
	if( what_problem == 0 ){
		console.log( "please, select one " );
		$("#resolve").focus();
		return false;
	}
	console.log('valid');
	return true;
}