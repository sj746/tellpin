/**
 * tutoring.js
 */


tellpinApp.controller( "tutoringCtrl", function( $scope, $http ) {
	
	var feedback_item = null;
	var confirm_item = null;
	angular.element(document).ready(function(){
		//console.log($scope.data);
	});
	
//	var STATUS = {
//			"All":null,
//			"Not scheduled":0,
//			"Requested":1,
//			"Upcoming":2,
//			"Need Confirmation":2,
//			"Declined":3,
//			"Unscheduled":4,
//			"Confirmed":5,
//			"In Problem":6,
//			"Refunded":7,
//			"Rescheduled":8,
//			"Approved / Upcoming":9
//			}

	var STATUS = {
			"Requested":1,
			"Approved/Upcoming":2,
			"Problem reported":3,
			"In dispute":4,
			"In progress":5,
			"Need confirmation":6,
			"Confirmed":7,
			"Canceled":8,
			"Refunded":9,
			"Expired":10,
			"Approved/Upcoming - Request to change":21,
			"Approved/Upcoming - Change date/time":22,
			"Approved/Upcoming - Cancel":23,
			"None":9999,
			"All":0
			}

	//tutor feedback 남기기 전 action require, 남긴 후 no action needed (confirmed 상태: 5)
//	var TUTOR_ACTION_STATUS = {
//			"Action required":[1, 5, 6, 8 ],
//			"Waiting for action":[0, 2],
//			"No Action needed":[3, 4, 7, 9, 5]
//	}
//	var TUTEE_ACTION_STATUS = {
//			"Action required":[0, 2, 4, 6 ],
//			"Waiting for action":[1, 8],
//			"No Action needed":[3, 5, 7, 9]
//	}

	var TUTOR_ACTION_STATUS = {
			"Action required":[1, 2, 5],
			"Waiting for action":[3, 4, 6],
			"No Action needed":[7, 8, 9, 10]
	}

	var TUTEE_ACTION_STATUS = {
			"Action required":[2, 3, 4, 5, 6],
			"Waiting for action":[1],
			"No Action needed":[7, 8, 9, 10]
	}

	$scope.init = function(isTutor, display, response ) {
		console.log('init');
		console.log(response);
		$scope.is_tutor = response.is_tutor;
		$scope.display = response.display;
		$scope.ratings = 5;
		var _filter = "All";
		var _user_name = "";
		$scope._search_type = "stat";
		
		if(response.filter){
			_filter = response.filter;
		}
		if(response.user_name){
			_user_name = response.user_name;
			$scope._search_type = "name";
			$("#search_name").val(_user_name);
		}
		//console.log(response);
		//내가 강사 인가??
		if( $scope.is_tutor == true) {
			// 강사 군
			//보여줄 리스트는?
			if( $scope.display == 1 || $scope.display == 15) {
				$scope.btn_display = $scope.display;
				$scope.rv_list = response.rv_data.entries;
				$scope.rv_data = response.rv_data;
				//$scope.get_rv_tutor();
			} else {
				//$scope.get_rv_tutee();
				$scope.btn_display = $scope.display;
				$scope.rv_list = response.rv_data.entries;
				$scope.rv_data = response.rv_data;
			}
			
		} else {
			//강사는 아니군
			//$scope.get_rv_tutee();
			$scope.btn_display = 2;
			$scope.rv_list = response.rv_data.entries;
			$scope.rv_data = response.rv_data;
		}
		console.log( _filter );
		$scope.selected_stat=_filter;
		
		//$scope.statFilter(1);
		//$scope.setCurrentTime();
	}
	
	$scope.menu = function(m, e) {
		$( "li" ).each(function( index ) {
			$( this ).removeClass("active");
			  //console.log( index + ": " + $( this ).text() );
		});
		$(e.target).parent().addClass("active");
		
		if(m == 0) {
			location.href = "/tutorlessons/"
			//$scope.get_rv_tutor();
		} else if( m == 1) {
			location.href = "/tutorlessons/schedule/"
			//$scope.get_rv_tutee()
		} else if( m == 6) {
			location.href = "/tuteelessons/"
			//$scope.get_rv_tutee()
		}
	}
	
	$scope.clickMessage = function(rvid){
		event.preventDefault();
		console.log(rvid);
		location.href="/tutorlessons/status/"+rvid;
	}
	//button action 
	$scope.clickBtnAction = function( action, type, item ){
		//TODO 
		//상황별 액션 처리. 
		//0: Not Scheduled, 1:Requested, 2: Need Confirmation, 3:Declined, 4:Canceled
		//5: Confirmed, 6: In Problem, 7: Refunded, 8: Rescheduled, 9: Approved / Upcoming
		
		console.log( action );
		console.log( type );
		console.log( item );
		console.log( feedback_item );
		console.log( confirm_item );
		
		
		if( $scope.btn_display == 1 ){
			if( item==undefined && action == "Leave a feedback" ){
				item = feedback_item;
			}
			//feedback
			if( item.status_id == 5 && action == "Leave a feedback" && type == undefined){
				feedback_item = item;
				$scope.openPopFeedback();
				return;
			}else if(item.status_id == 5 && action == "Leave a feedback"){
				$("#myModal2").modal('hide');
				var _data = {};
				_data["ratings"] = 0;
				_data["comment"] = $("#feedback_comment").val();
				$http.post( "/tutor/tutoring/commit/", { command : action, rvid : item.rvid, display: display, type:'details', data:_data }).success(function(response) {
					if( response.isSuccess != '0') {
						console.log(response);
						location.reload();
					}
					
				});
				return;
			}
			
		}
		else{
			if( item==undefined && action == "Confirm" ){
				item = confirm_item;
			}
			//confirm
			if( item.status_id == 2 && action == "Confirm" && type == undefined){
				confirm_item = item;
				$scope.openPopConfirm();
				return;
			}else if(item.status_id == 2 && action == "Confirm"){
				$("#myModal").modal('hide');
				var _data = {};
				_data["ratings"] = $scope.ratings;
				_data["comment"] = $("#confirm_comment").val();
				$http.post( "/tutor/tutoring/commit/", { command : action, rvid : item.rvid, display: display, type:'details', data:_data }).success(function(response) {
					if( response.isSuccess != '0') {
						console.log(response);
						//$scope.list = response.rv_data[0];
						location.reload();
					}
					
				});
				return;
			}
		}
		
		if( item.status_id == 2 && action == "Report a Problem"){
			//TODO 페이지 이동
			location.href = "/tutorlessons/lesson/problem/"+item.rvid;
			return;
		}
		if( item.status_id == 6 && action == "Reply"){
			//TODO 페이지 이동
			location.href = "/tutorlessons/lesson/problem/"+item.rvid;
			return;
		}
		var display = $scope.btn_display == 1 ? 1 : 2;

		var is_confirm = false;
		
		var dialog2 = $("#confirm_dialog").dialog({
			
			width: 500,
			modal: true,
			buttons: [
			          {
			        	  text: "Confirm",
			        	  click: function(){
			        		  //$(this).dialog( "close" );
			        		  console.log( ' click confirm button ');
			        		  console.log( $("#dialog_pwd").val() );
			        		  console.log( $(this).parent().children().find("input[id=dialog_pwd]").val() );
			        		  
			        		  obj = {
			        				  'pwd':  $(this).parent().children().find("input[id=dialog_pwd]").val()
			        		  }
			        		  $http.post("/user_auth/ajax_check_password/", {obj:obj}).success(function(response) {
			        			 
			        			  console.log(response);
			        			  if(response.isSuccess){
			        				  console.log('success');
			        				  
			        				  $http.post( "/tutor/tutoring/commit/", { command : action, rvid : item.rvid, display: display, type:'details' }).success(function(response) {
			        						
			        						if( response.isSuccess != '0') {
			        							console.log(response);
			        							if( response.rv_data.add_schedule != undefined) {
			        								location.href = response.rv_data.add_schedule;
			        								return;
			        							}
			        							console.log(response.rv_data.add_schedule);
			        							//$scope.list = response.rv_data[0];
			        							location.reload();
			        						}
			        						
			        					}).error(function(response){
			        						console.log( 'erorr');
			        						alert(response);
			        					});
			        			  }else{
			        				  console.log('fail');
			        				  $(".error-text").text(response.message);
			        			  }
			        			  
			        		  });
			        		  
			        	  }
			          }
			]
		});
		return;
		$http.post( "/tutor/tutoring/commit/", { command : action, rvid : item.rvid, display: display, type:'details' }).success(function(response) {
			
			if( response.isSuccess != '0') {
				console.log(response);
				if( response.rv_data.add_schedule != undefined) {
					location.href = response.rv_data.add_schedule;
					return;
				}
				console.log(response.rv_data.add_schedule);
				//$scope.list = response.rv_data[0];
				location.reload();
			}
			
		}).error(function(response){
			console.log( 'erorr');
			alert(response);
		});
	}
//	$scope.commitResev = function(_command, _rvid) {
//		
//		$http.post( "/tutor/tutoring/commit/", { command : _command, rvid : _rvid , status : STATUS[$scope.selected_stat], page:$scope.rv_data.page, display: $scope.btn_display}).success(function(response) {
//			if( response.isSuccess != '0') {
//				if( response.rv_data.add_shcedule != undefined) {
//					location.href = response.rv_data.add_shcedule
//				}
//				$scope.rv_list = response.rv_data.entries
//				$scope.rv_data = response.rv_data
//			}
//			
//		});
//	}
//	//Remaining Time Caculate
//	$scope.setCurrentTime = function(){ 
//		var now = new Date();
//		$scope.currentUTCTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
//		$scope.changeTimezone();
//		$timeout($scope.setCurrentTime, 1000);
//	};
//	//Remaining Time Caclute ( timer )
//	$scope.changeTimezone = function(){
//		if ($scope.list.from_date_time != null){
//			var displayTime = new Date($scope.currentUTCTime.getTime());
//			var t_time = $scope.item.from_date_time;
//			var startTime = new Date(t_time.substr(0,4), parseInt(t_time.substr(5,2))-1, t_time.substr(8,2), t_time.substr(11,2), t_time.substr(14,2), t_time.substr(17,2));
//			
//			
//	//		console.log('lesson time: ' + startTime);
//	//		console.log('current time: ' + displayTime);
//	//		console.log(t_time.substr(0,4), t_time.substr(5,2), t_time.substr(8,2), t_time.substr(11,2), t_time.substr(14,2), t_time.substr(17,2));
//			
//			if( $scope.list.from_date_time != null){
//				$scope.delta = Math.floor( (startTime - displayTime) / 1000 ) ;
//				$scope.currentTime = displayTime.format("yyyy.MM.dd hh:mm:ss a/p ");
//				
//	//			console.log($scope.list.from_date_time);
//	//			console.log( $scope.delta );
//				if ($scope.delta < 0){
//					$scope.remainingTime = "ㅡ";
//				}else{
//					$scope.remainingTime = Math.floor($scope.delta/(60*60*24)) +"일 " + Math.floor(($scope.delta%(60*60*24))/60/60) + "시간 " + Math.floor(($scope.delta%(60*60*24)%(60*60))/60) + "분 " + Math.floor((($scope.delta%(60*60*24))%(60*60))%60) + "초";
//				}
//			}
//		}else{
//			$scope.remainingTime = "ㅡ";
//		}
//	}
	$scope.goUrl = function(_url){
		console.log(_url);
		location.href = _url;
	}
	
	$scope.fieldFilter = function(_ordering) {
		$scope.ordering = _ordering;
	}
	
	$scope.statFilter = function(_page) {
		console.log('statfilter()');
		if( $scope.selected_stat == '--------------------------'){
			console.log($scope.selected_stat);
			return;
		}

		console.log($scope.selected_stat);
		console.log(STATUS[$scope.selected_stat]);

		console.log(_page);
		
		var _status;
		if($scope.selected_stat == "Action required" || $scope.selected_stat == "Waiting for action" || $scope.selected_stat == "No Action needed"){
			
			if($scope.btn_display==1){
				//tutor
				_status = TUTOR_ACTION_STATUS[$scope.selected_stat];
			}else{
				//tutee
				_status = TUTEE_ACTION_STATUS[$scope.selected_stat];
			}
		}else{
			_status = STATUS[$scope.selected_stat];	
		}
		if( $scope.selected_stat == "New Message" ){
			_status = "New Message";
		}
		
		//2016-06-16 khyun ( name paging )
		if( $scope._search_type == "name" ){
			var search_name = $("#search_name").val();
			
			$http.post( "/tutorlessons/stutusfilter/", { search_name: search_name, page:_page, display: $scope.btn_display } ).success(function(response) {
				console.log('name search filter');
				console.log(response);
				if( response.isSuccess != '0') {
					$scope.rv_list = response.rv_data.entries
					$scope.rv_data = response.rv_data
				}
				
			});
		}
		else{
			$http.post( "/tutorlessons/stutusfilter/", { status : _status, page:_page, display: $scope.btn_display, filterType: $scope.selected_stat, search_name: null}).success(function(response) {
				console.log('normal filter');
				console.log(response);
				if( response.isSuccess != '0') {
					$scope.rv_list = response.rv_data.entries
					$scope.rv_data = response.rv_data
				}
				
			});
		}
		
	}
	$scope.searchName = function(){
		var search_name = $("#search_name").val();
		
		$http.post( "/tutorlessons/stutusfilter/", { search_name: search_name, display: $scope.btn_display, page:1 } ).success(function(response) {
			console.log(response);
			if( response.isSuccess != '0') {
				$scope.rv_list = response.rv_data.entries
				$scope.rv_data = response.rv_data
			}
			
		});
	}
	//Confirm popup
	$scope.openPopConfirm = function(){
		$("#myModal").modal();
	}
	//Feedback popup
	$scope.openPopFeedback = function(){
		$("#myModal2").modal();
	}
	//click ratings
	$scope.clickRatings = function(num){
		$scope.ratings = num;
		console.log(num);
		$(".ratings > img").attr('src', '/static/img/tutor/icon_star_empty.png');
		for(var i=0; i<num; i++){
			$(".star"+(i+1)).attr('src', '/static/img/tutor/icon_star_full.png');
		}
	}
	$scope.checkLengthConfirmModal = function(){
		var len = $(".confirm-area").val().length;
		var txt = $(".txt-len").text( len + ' / 400 characters');
	}
	$scope.checkLengthFeedbackModal = function(){
		var len = $(".confirm-area2").val().length;
		var txt = $(".txt-len2").text( len + ' / 400 characters');
	}

	
	
});

$(document).ready(function(){
	$('#search_name').keypress(function(event){
		  if(event.keyCode == 13){
			  angular.element($(".tutoring-container")).scope()._search_type="name";
		    //angular.element.scope().search_name();
//			  console.log('test');
//			  console.log(angular.element($(".container")).scope().tutor_list);
			  angular.element($(".tutoring-container")).scope().searchName();
		  }
	});
});