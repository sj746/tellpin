tellpinApp.controller("tellpinSearchDecCtrl", function( $scope) {

	$scope.init = function(display) {
		$scope.display = display;
		$scope.playerUpdate1 = false;
		$scope.searchUpdate1 = false;
		
	}

	$scope.clickPlayer = function( type ) {
		console.log( type );
		if ( type == 'playerUpdate1' ) {
			$scope.playerUpdate1 = !$scope.playerUpdate1;
		}
		if ( type == 'searchUpdate1' ) {
			$scope.searchUpdate1 = !$scope.searchUpdate1;
		}
		if ( type == 'playerUpdate2' ) {
			$scope.playerUpdate2 = !$scope.playerUpdate2;
		}
	}

});

function play_video(){
	$(".introPopup").css("display", "block");
	$("#i-player").load();
	$("#i-player").get(0).play();
}
function end_video(){
	$(".introPopup").css("display", "none");
	$("#i-player").get(0).pause();
}