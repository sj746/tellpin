/**
 * set_page.js
 */

//var setApp = angular.module( "setApp", ["angular-sortable-view"]);
var user_id = "";

tellpinApp.controller( "setCtrl", function( $scope, $http, $location, $window, $timeout ) {
	$scope.selection = true;	// 체크 상태 toggle
	$scope.allToggleSelection = [];
	$scope.pinToggleSelection = [];
	$scope.allSelection = [];	// All 카테고리 단어에서 선택된 단어 담는 배열
	$scope.pinSelection = [];	// Pin 카테고리 단어에서 선택된 단어 담는 배열
	$scope.allSelectedIndex = [];
	$scope.pinSelectedIndex1 = [];
	$scope.pinSelectedIndex2 = [];
	$scope.pinSelectedIndex3 = [];
	$scope.pinCategory = 1;		// Pin 카테고리 번호
	$scope.allHistory;
	$scope.pinHistory = [];
	$scope.pinHistory1; 
	$scope.pinHistory2;
	$scope.pinHistory3;
	$scope.pinName1;
	$scope.pinName2;
	$scope.pinName3;
	$scope.updateLoading = false;
	$scope.myLanguageList;
	$scope.selectedLanguage;
	$scope.myDictionaryList;
	$scope.selectedDictionary;
	$scope.mySiteList;
	$scope.selectedSite;
	$scope.selectedPlayMenu = 'All';
	$scope.playFrom;
	$scope.playTo;
	$scope.playInterval=5000;
	$scope.deletingType;
	$scope.renameType;
	$scope.renameTxt="";
	$scope.playHover = 1;
	$scope.pauseHover = 3;
	$scope.repeatHover = 1;
	$scope.stopHover = 1;
	/*$http.post( "/tools/gethistory/" ).success( function( result ) {
		if ( result.isSuccess == 1 ) {
			$scope.allHistory = result.historylist;
			$scope.pinHistory1 = result.mypin1;
			$scope.pinHistory2 = result.mypin2;
			$scope.pinHistory3 = result.mypin3;
		}
	});
	*/
	$scope.pinFlag1 = true;		// #Pin1
	$scope.pinFlag2 = false;	// #Pin2
	$scope.pinFlag3 = false;	// #Pin3
	
	$scope.rangeFlag1 = true;
	$scope.rangeFlag2 = false;
	// 기본 Pin 카테고리 1로 설정
	//$scope.pinHistory = $scope.pinHistory1;
	
	$scope.allScrollAction = false;
	$scope.pin1ScrollAction = false;
	$scope.pin2ScrollAction = false;
	$scope.pin3ScrollAction = false;
	
	
	$scope.init = function( obj ) {
		$scope.allScrollAction = false;
		$scope.pin1ScrollAction = false;
		$scope.pin2ScrollAction = false;
		$scope.pin3ScrollAction = false;
		$scope.allHistory = obj.historylist;
		$scope.pinHistory = obj.mypin1;
		$scope.pinHistory1 = obj.mypin1;
		$scope.pinHistory2 = obj.mypin2;
		$scope.pinHistory3 = obj.mypin3;
		$scope.allStart = $scope.allHistory.length;
		$scope.pin1Start = $scope.pinHistory1.length;
		$scope.pin2Start = $scope.pinHistory2.length;
		$scope.pin3Start = $scope.pinHistory3.length;
		$scope.slider = {
				value: 1,
				options: {
					floor: 1,
				    ceil: 4,
					step: 1
				}
		}
	}
	
	$scope.checkSlider = function() {
		alert( $scope.slider.value );
	}
	
	// History 무한스크롤
	$scope.getMoreHistory = function( type ) {
		console.log( '$$$$$$ Infinite' );
		console.log( '###### type', type );
		var page;
		if ( type == 1 ) {
			$scope.allScrollAction = true;
			page = $scope.allHistory.length;
		}
		else if ( type == 2 ) {
			$scope.pin1ScrollAction = true;			
			page = $scope.pinHistory1.length;
		}
		else if ( type == 3 ) {
			$scope.pin2ScrollAction = true;
			page = $scope.pinHistory2.length;
		}
		else {
			$scope.pin3ScrollAction = true;
			page = $scope.pinHistory3.length;
		}
		console.log( page );
		$http.post( '/tellpinsearch/more/history/', { 'start' : page, 'type' : type } )
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				if ( type == 1 ) {
					$scope.allScrollAction = false;
					$scope.allStart += 10;
					$scope.allHistory = $scope.allHistory.concat( result.historylist );
				}
				else if ( type == 2 ) {
					$scope.pin1ScrollAction = false;
					$scope.pin1Start += 10;
					$scope.pinHistory1 = $scope.pinHistory1.concat( result.mypin1 );
				}
				else if ( type == 3 ) {
					$scope.pin2ScrollAction = false;
					$scope.pin2Start += 10;
					$scope.pinHistory2 = $scope.pinHistory2.concat( result.mypin2 );
				}
				else {
					$scope.pin3ScrollAction = false;
					$scope.pin3Start += 10;
					$scope.pinHistory3 = $scope.pinHistory3.concat( result.mypin3 );
				}
			}
		});
	}
	
	$http.post( "/tools/getAutoSearchPlayerData/").success( function( result ) {
		if ( result.isSuccess == 1 ) {
			$scope.myLanguageList = result.myLanguageList;
			$scope.myDictionaryList = result.myDictionaryList;
			$scope.mySiteList = result.mySiteList;
			$scope.selectedLanguage = $scope.myLanguageList[0]; 
			$scope.selectedDictionary = $scope.myDictionaryList[0];
			$scope.selectedSite = $scope.mySiteList[0];
		}
	});
	
	$scope.changeMyLanguage = function (selectedLang) {
		$http.post( "/tools/getAutoSearchPlayerDic/", { "id": selectedLang.id }).success( function( result ) {
			if ( result.isSuccess == 1 ) {
				$scope.myDictionaryList = result.myDictionaryList;
				$scope.mySiteList = result.mySiteList;
				$scope.selectedDictionary = $scope.myDictionaryList[0];
				$scope.selectedSite = $scope.mySiteList[0];
			}
		});
	}
	
	$scope.changeMyDictionary = function (selectedDic) {
		$http.post( "/tools/getAutoSearchPlayerSite/", { "id": selectedDic.category_id }).success( function( result ) {
			if ( result.isSuccess == 1 ) {
				$scope.mySiteList = result.mySiteList;
				$scope.selectedSite = $scope.mySiteList[0];
			}
		});
	}
	
	/*$http.post( "/tools/gethistory/").success( function( result ) {
		if ( result.isSuccess == 1 ) {
			console.log(result);
			$scope.allHistory = result.historylist;
			$scope.pinHistory = result.mypin1;
			$scope.pinHistory1 = result.mypin1;
			$scope.pinHistory2 = result.mypin2;
			$scope.pinHistory3 = result.mypin3;
		}
	});*/
	
	$http.post( "/tools/get_pin_name/").success( function( result ) {
		if ( result.isSuccess == 1 ) {
			console.log('get_pin_name',result);
			
			if(result.pinName1 == null){
				$scope.pinName1 = 'Pin List #1'
			} else {
				$scope.pinName1 = result.pinName1;
			}
			
			if(result.pinName2 == null){
				$scope.pinName2 = 'Pin List #2'
			} else {
				$scope.pinName2 = result.pinName2;
			}
			
			if(result.pinName3 == null){
				$scope.pinName3 = 'Pin List #3'
			} else {
				$scope.pinName3 = result.pinName3;
			}
		}
	});
	

	$scope.toggleSelection = function ( type, idx ) {
		temp=[];
		if(type == 'all'){
			temp = $scope.allSelectedIndex;
		} else if(type == 'pin1') {
			temp = $scope.pinSelectedIndex1;
		} else if(type == 'pin2') {
			temp = $scope.pinSelectedIndex2;
		} else if(type == 'pin3') {
			temp = $scope.pinSelectedIndex3;
		}
		
		// 해당 단어가 존재하지 않을 경우 배열에 push
		if ( temp.indexOf(idx) < 0 ) {
			temp.push(idx);
			
			if(type == 'all'){
				$(".cover-all-content").css("display", "block");
				$(".paste-btn").css("display", "block");
			}
		}
		// 해당 단어가 존재할 경우 선택제거
		else {
			temp.splice(temp.indexOf(idx), 1);
			
			if(type == 'all'){
				if(temp==null || temp.length < 1){
					$(".cover-all-content").css("display", "none");
					$(".paste-btn").css("display", "none");
				}
			}
		}
		
		if(type == 'all'){
			$scope.allSelectedIndex = temp;
		} else if(type == 'pin1') {
			$scope.pinSelectedIndex1 = temp;
		} else if(type == 'pin2') {
			$scope.pinSelectedIndex2 = temp;
		} else if(type == 'pin3') {
			$scope.pinSelectedIndex3 = temp;
		}
	}
	
	// 전체선택 전체 해제
	$scope.allWordSelect = function ( type ) {
		var indexList;
		var dataList;
		if(type == 'all'){
			indexList = $scope.allSelectedIndex;
			dataList = $scope.allHistory;
		} else if(type == 'pin1') {
			indexList = $scope.pinSelectedIndex1;
			dataList = $scope.pinHistory1;
		} else if(type == 'pin2') {
			indexList = $scope.pinSelectedIndex2;
			dataList = $scope.pinHistory2;
		} else if(type == 'pin3') {
			indexList = $scope.pinSelectedIndex3;
			dataList = $scope.pinHistory3;
		}
		
		if (  ( indexList.length < dataList.length ) && ( indexList.length >= 0 ) ) {
			indexList = [];
			for ( var i = 0; i < dataList.length; i++ ) {
				indexList.push( i );
				
				if(type == 'all'){
					$(".cover-all-content").css("display", "block");
					$(".paste-btn").css("display", "block");
				}
			}
		}
		else {
			indexList = [];
			
			if(type == 'all'){
				$(".cover-all-content").css("display", "none");
				$(".paste-btn").css("display", "none");
			}
		}
		
		if(type == 'all'){
			$scope.allSelectedIndex = indexList;
		} else if(type == 'pin1') {
			$scope.pinSelectedIndex1 = indexList;
		} else if(type == 'pin2') {
			$scope.pinSelectedIndex2 = indexList;
		} else if(type == 'pin3') {
			$scope.pinSelectedIndex3 = indexList;
		}
	}
	
	// -표시 누르면 선택 해제
	$scope.allCancelSelect = function (type) {
		if(type == 'all'){
			$scope.allSelectedIndex = [];
			$(".cover-all-content").css("display", "none");
			$(".paste-btn").css("display", "none");
		} else if(type == 'pin1') {
			$scope.pinSelectedIndex1 = [];
		} else if(type == 'pin2') {
			$scope.pinSelectedIndex2 = [];
		} else if(type == 'pin3') {
			$scope.pinSelectedIndex3 = [];
		}
	}
	
	$scope.checkAllSelection = function(type, str ) {
		var indexList;
		var dataList;
		
		if(type == 'all'){
			indexList = $scope.allSelectedIndex;
			dataList = $scope.allHistory;
		} else if(type == 'pin1') {
			indexList = $scope.pinSelectedIndex1;
			dataList = $scope.pinHistory1;
		} else if(type == 'pin2') {
			indexList = $scope.pinSelectedIndex2;
			dataList = $scope.pinHistory2;
		} else if(type == 'pin3') {
			indexList = $scope.pinSelectedIndex3;
			dataList = $scope.pinHistory3;
		}
		
		if ( str == "check" ) {
			if ( ( dataList && indexList.length == dataList.length ) && ( indexList.length != 0 ) )
				return true;
			return false;
		}
		else {
			if ( ( indexList.length > 0 && indexList.length < dataList.length ) )
				return false;
			return true;
		}
	}

	
	// 카테고리 변경 시 기본 카테고리 번호 바꿔주기
	$scope.pinChanged = function ( number ) {
		console.log('pinChangeed()');
		if ( number == 1 )
			$scope.pinCategory = 1;
		else if ( number == 2 )
			$scope.pinCategory = 2;
		else
			$scope.pinCategory = 3;
		
		$scope.pinSelection = [];
		$scope.pinToggleSelection = [];
	}
	
	
	// All -> Pin 추가하는 함수
	$scope.copyWord = function (type) {
		if(type == 1){
			dataList = $scope.pinHistory1;
		} else if(type == 2){
			dataList = $scope.pinHistory2;
		} else if(type == 3){
			dataList = $scope.pinHistory3;
		} 

		var wordList=[]; 
		for( var i=0; i < dataList.length; i++){
			wordList.push(dataList[i].word);
		}
		
		for( var i=0; i < $scope.allSelectedIndex.length; i++){
			if(wordList.indexOf( $scope.allHistory[$scope.allSelectedIndex[i]].word ) == -1){
				dataList.push($scope.allHistory[$scope.allSelectedIndex[i]]);
			}
		}
		
		var temp = [];
		var pinFlag = false;
		
		
		for( var i = 0; i < dataList.length; i++ ) {
			if ( i == 0 ) {
				temp.push( dataList[i] );
			}
			else {
				for ( var j = 0; j < temp.length; j++ ) {
					if ( dataList[i].word == temp[j].word ) {
						pinFlag = true; // 중복단어가 있을 경우
					}
					console.log( j );
				}
				if ( !pinFlag ) {
					temp.push( dataList[i] );
				}
				pinFlag = false;
			}
		}
		
		console.log( temp );
		if(type == 1){
			$scope.pinHistory1 = temp;
		} else if(type == 2){
			$scope.pinHistory2 = temp;
		} else if(type == 3){
			$scope.pinHistory3 = temp;
		} 
		
		$scope.log_save('pin');
		$scope.allCancelSelect('all');
		$(".cover-all-content").css("display", "none");
		$(".paste-btn").css("display", "none");
	}
	
	// 선택 된 단어들 txt파일 만들어서 다운로드
	$scope.makeTxt = function ( type ) {
		//console.log( data );
		$scope.removeFilePath;
		/*data.sort(function(a, b) {
			var textA = a.toUpperCase();
			var textB = b.toUpperCase();
			return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
		});*/
		var indexList;
		var dataList;
		//console.log( $scope.allSelectedIndex );
		
		if(type == 'all'){
			indexList = $scope.allSelectedIndex.sort( function( a, b ) { return a - b} );
			dataList = $scope.allHistory;
		} else if(type == 'pin1') {
			indexList = $scope.pinSelectedIndex1.sort( function( a, b ) { return a - b} );
			dataList = $scope.pinHistory1;
		} else if(type == 'pin2') {
			indexList = $scope.pinSelectedIndex2.sort( function( a, b ) { return a - b} );
			dataList = $scope.pinHistory2;
		} else if(type == 'pin3') {
			indexList = $scope.pinSelectedIndex3.sort( function( a, b ) { return a - b} );
			dataList = $scope.pinHistory3;
		}
		//console.log( indexList );
		//console.log( Number(9));
		var word = [];
		
		for ( var i = 0; i < indexList.length; i++ ) {
			word.push( dataList[indexList[i]] );
		}
		
		// 브라우저체크 ie -> if, other -> else
		var wordlist = '';
		for ( var i = 0; i < word.length; i++ ) {
			wordlist += word[i].word;
			if ( i <  word.length - 1 ) {
				wordlist += '\r\n';
			} 
		}
		console.log( wordlist );
		var blob = new Blob( [ wordlist ], { type : 'text/plain;charset=utf-8' });
		saveAs( blob, 'word.txt' );
		/*
		$http.post( "/tools/make/", { word : word } )
		.success( function ( response ) {
			//console.log(response);
			
		===================================
			a 태그 생성 후  해당 태그에 
			href 속성과 download 속성을 설정해주고
			클릭 해줌으로써 다운로드 실행
		===================================
			 
			var atag = document.createElement("a");
			atag.setAttribute( "href", response );
			atag.setAttribute( 'id', 'wordTxt' );
			atag.setAttribute( "download", "word.txt" );
			document.body.appendChild( atag );
			atag.click();
			$( '#wordTxt' ).remove();
			$scope.removeFilePath = response;
			//console.log( $scope.removeFilePath );
		});			
		*/
	}
	
	// 페이지 변환 시에 서버에 만들어진 txt 파일
	// 삭제하는 함수
	$scope.$on( "$locationChangeStart", function( event ) {
		//console.log("페이지이동");
		$http.post( "/tools/remove/")
		.success( function ( result ) {
			
		});
	});

	$scope.log_save = function(type){

		if( type == 'all'){
			var obj = { 'myhistory': $scope.allHistory}
			
			$http.post('/tools/setmyhistory/', obj).success( function(result){
				
				if ( result.isSuccess == "1"){
					console.log('result',result);
					//alert(result.message);
				}
			});	
		} else {
			var obj = { 'mypin1': $scope.pinHistory1, 'mypin2': $scope.pinHistory2, 'mypin3': $scope.pinHistory3 }
			
			$http.post('/tools/setmypin/', obj).success( function(result){
				
				if ( result.isSuccess == "1"){
					console.log(result);
					//alert(result.message);
				}
			});	
		}
	}
	
	$scope.log_delete = function(){
		var indexList;
		var dataList;
		
		if($scope.deletingType == 1){
			indexList = $scope.allSelectedIndex;
			dataList = $scope.allHistory;
		} else if($scope.deletingType == 2){
			indexList = $scope.pinSelectedIndex1;
			dataList = $scope.pinHistory1;
		} else if($scope.deletingType == 3){
			indexList = $scope.pinSelectedIndex2;
			dataList = $scope.pinHistory2;
		} else if($scope.deletingType == 4){
			indexList = $scope.pinSelectedIndex3;
			dataList = $scope.pinHistory3;
		}

		for( var i=0; i < indexList.length; i++){
			dataList.splice(indexList[i], 1, null);
		}
		
		var tempList=[];
		for( var i=0; i < dataList.length; i++){
			if(dataList[i] != null){
				tempList.push(dataList[i]);
			}
		}
		
		if($scope.deletingType == 1){
			$scope.allSelectedIndex = [];
			$scope.allHistory = tempList;
		} else if($scope.deletingType == 2){
			$scope.pinSelectedIndex1 = [];
			$scope.pinHistory1 = tempList;
		} else if($scope.deletingType == 3){
			$scope.pinSelectedIndex2 = [];
			$scope.pinHistory2 = tempList;
		} else if($scope.deletingType == 4){
			$scope.pinSelectedIndex3 = [];
			$scope.pinHistory3 = tempList;
		}

		if($scope.deletingType == 1){
			$scope.log_save('all');
			$(".cover-all-content").css("display", "none");
			$(".paste-btn").css("display", "none");
		} else if($scope.deletingType > 1) {
			$scope.log_save('pin');
		}
		$scope.addDelete(0);
	}
	
	$scope.changeMenu = function () {
		if ( $scope.rangeFlag2 == true ){
			$scope.setRange();
		}
	}
	
	$scope.rangeChanged = function ( number ) {
		if ( number == 1 ){
			$scope.rangeFlag1 = true;
			$scope.rangeFlag2 = false;
		}
		else if ( number == 2 ){
			$scope.rangeFlag1 = false;
			$scope.rangeFlag2 = true;
			
			$scope.setRange();
		}
	}
	
	$scope.setRange  = function () {
		if($scope.selectedPlayMenu == 'All' ){
			$scope.playTo = $scope.allHistory.length;
		} 
		else if($scope.selectedPlayMenu == $scope.pinName1 ){
			$scope.playTo = $scope.pinHistory1.length;
		}
		else if($scope.selectedPlayMenu == $scope.pinName2 ){
			$scope.playTo = $scope.pinHistory2.length;
		}
		else if($scope.selectedPlayMenu == $scope.pinName3 ){
			$scope.playTo = $scope.pinHistory3.length;
		}
		
		if($scope.playTo < 1){
			$scope.playFrom = 0;
		} else {
			$scope.playFrom = 1;
		}
	}
	
	$scope.help = function ( number ) {
		if(number == 1){
			$(".search-help-box").css("display", "block");
		} else {
			$(".search-help-box").css("display", "none");
		}
	}
	
	$scope.setPinName = function () {
		if($scope.renameTxt.length==0){
			return;
		}
		
		if($scope.renameType == 1){
			var obj = { 'pin1': $scope.renameTxt }
		} else if($scope.renameType == 2){
			var obj = { 'pin2': $scope.renameTxt }
		} else if($scope.renameType == 3){
			var obj = { 'pin3': $scope.renameTxt }
		}
		
		$http.post('/tools/set_pin_name/', obj).success( function(result){
			if ( result.isSuccess == "1"){
				console.log('result',result);
				if(result.pinType == 'pin1'){
					$scope.pinName1 = $scope.renameTxt;
				} else if(result.pinType == 'pin2'){
					$scope.pinName2 = $scope.renameTxt;
				} else if(result.pinType == 'pin3'){
					$scope.pinName3 = $scope.renameTxt;
				}
			} else { 
				//alert(result.message);
			}
			$( '#renameModal' ).modal( 'hide' );
		});	
		
	}
	
	var timer;
	var search;
	var searchCount=0;
	$scope.isRepeat=false;
	$scope.searchPlayerStart = function() {
		urlArray = $scope.selectedSite.search_url.split("@^tskw^@");
		wordArray=[];
		searchUrlArray=[];
		start=1;
		end=1;

		//단어리스트
		if($scope.selectedPlayMenu == 'All'){
			wordArray = $scope.allHistory;
		} else if($scope.selectedPlayMenu == $scope.pinName1){
			wordArray = $scope.pinHistory1;
		} else if($scope.selectedPlayMenu == $scope.pinName2){
			wordArray = $scope.pinHistory2;
		} else if($scope.selectedPlayMenu == $scope.pinName3){
			wordArray = $scope.pinHistory3;
		}
		
		if ( wordArray.length < 1 ) {
			return;
		}
		
		// 실행속도
		if ( $scope.slider.value == 1 ) {
			$scope.playInterval = 10000;
		} 
		else if ( $scope.slider.value == 2 ) {
			$scope.playInterval = 7000;
		}
		else if ( $scope.slider.value == 3 ) {
			$scope.playInterval = 5000;
		}
		else {
			$scope.playInterval = 3000;
		}
		//검색리스트 범위
		if($scope.playFrom != null && $scope.rangeFlag2 == true){
			start = $scope.playFrom;
		}
		if($scope.playTo != null && $scope.rangeFlag2 == true){
			end = $scope.playTo;
		} else { 
			end = wordArray.length;
		}
		
		for(var i=start-1 ; i < end ; i++){
			if(urlArray.length == 2){
				searchUrlArray.push(urlArray[0] + encodeURIComponent(wordArray[i].word.toLowerCase() ) + urlArray[1]);
			} else if(urlArray.length == 1) {
				searchUrlArray.push(searchURL = urlArray + encodeURIComponent( wordArray[i].wordtoLowerCase() ) );
			}
		}
		
		//$('#startButton').css('display', 'none');
		//$('#pauseButton').css('display', 'inline');
		$scope.playHover = 3;
		$scope.pauseHover = 1;
		
		clearInterval(timer);
		search = $window.open(searchUrlArray[searchCount], 'search', 'width=900, height=600');
		timer = setInterval(function() {
			if(search == null || search.closed){
				//$('#startButton').css('display', 'inline'); 
				//$('#pauseButton').css('display', 'none');
				$scope.playHover = 1;
				$scope.pauseHover = 3;
				clearInterval(timer);
			}else{
				searchCount++;
				if(searchCount < searchUrlArray.length){
					search = $window.open(searchUrlArray[searchCount], 'search', 'width=900, height=600');
				} else {
					searchCount = 0;
					if($scope.isRepeat){
						search = $window.open(searchUrlArray[searchCount], 'search', 'width=900, height=600');
					} else {
						clearInterval(timer);
						//$('#startButton').css('display', 'inline');
						//$('#pauseButton').css('display', 'none');
						$scope.playHover = 1;
						$scope.pauseHover = 3;
					}
				}
			}
		}, $scope.playInterval);
	}
	
	$scope.searchPlayerPause = function() {
		//$('#startButton').css('display', 'inline'); 
		//$('#pauseButton').css('display', 'none');
		$scope.playHover = 1;
		$scope.pauseHover = 3;
		clearInterval(timer);
	}
	
	$scope.searchPlayerStop = function() {
		//$('#startButton').css('display', 'inline'); 
		//$('#pauseButton').css('display', 'none');
		$scope.playHover = 1;
		$scope.pauseHover = 3;
		clearInterval(timer);
		searchCount = 0;
		if(search!=null){
			search.close();
		}
	}
	
	$scope.repeatType = function() {
		if ( $scope.isRepeat ) {
			$('#repeatButton').attr('src', '/static/img/tellpin_search/button_repeat_pressed.png');
			$scope.repeatHover = 2;
		}
		else {
			$('#repeatButton').attr('src', '/static/img/tellpin_search/button_repeat_pressed.png');
			$scope.repeatHover = 1;
		}
	}
	
	$scope.searchPlayerRepeat = function() {
		if($scope.isRepeat){
			$('#repeatButton').attr('src', '/static/img/tellpin_search/button_repeat_normal.png');
			$scope.isRepeat = false;
			$scope.repeatHover = 1;
		} else{
			$('#repeatButton').attr('src', '/static/img/tellpin_search/button_repeat_mint.png');
			$scope.repeatHover = 2;
			$scope.isRepeat = true;
		}
		console.log( $scope.repeatHover );			
	}
	
	$scope.checkFromToValue = function(type) {
		if($scope.rangeFlag2 == true){
			if(type == 1){
				if(Number($scope.playFrom) > Number($scope.playTo)){
					$scope.playFrom = 1;
					//alert("To 값보다 작게 입력해주세요.");
					$('.invalid').css('display', 'inline');
				} else{
					$('.invalid').css('display', 'none');
				}
			} else {
				max=0;
				//console.log( $scope.selectedPlayMenu );
				if($scope.selectedPlayMenu == 'All'){
					max = $scope.allHistory.length;
				} else if($scope.selectedPlayMenu == $scope.pinName1){
					max = $scope.pinHistory1.length;
				} else if($scope.selectedPlayMenu == $scope.pinName2){
					max = $scope.pinHistory2.length;
				} else if($scope.selectedPlayMenu == $scope.pinName3){
					max = $scope.pinHistory3.length;
				}
				console.log( max );
				console.log( $scope.playFrom );
				console.log( $scope.playTo );
				if( ( Number($scope.playFrom) > Number($scope.playTo) ) || ( Number($scope.playTo) > max ) ){
					$scope.playTo = max;
					//alert("From 값보다 크거나 단어장 수보다 작게 입력해주세요.");
					$('.invalid').css('display', 'inline');
				} else {
					$('.invalid').css('display', 'none');
				}
			}
		}
	}
	
	
	$scope.addDelete = function( idx ) {
		if ( idx >= 1) { 
			$scope.deletingType = idx;
			$( "#deleteModal" ).modal();
		} else
			$( "#deleteModal" ).modal( "hide" );
	}
	
	$scope.addRename = function( idx ) {
		$scope.renameFlag = false;
		if ( idx >= 1) { 
			$scope.renameType = idx;
			if ( idx == 1 ) {
				$scope.renameTxt = $scope.pinName1;
			}
			else if ( idx == 2 ) {
				$scope.renameTxt = $scope.pinName2;
			}
			else {
				$scope.renameTxt = $scope.pinName3;
			}
			$( "#renameModal" ).modal();
			$timeout( function() {
				console.log( 'focus' );
				$( '#renameFlag' ).select();
			}, 470);
			
			
		} else
			$( "#renameModal" ).modal( "hide" );
	}
	
	
});

tellpinApp.controller( "settingCtrl", [ '$scope', '$http', '$window', '$timeout', function( $scope, $http, $window, $timeout ) {
	$scope.settingTab = localStorage.getItem( "tab" ) || "category"; // 상단 라디오 버튼 model
	localStorage.clear();
	$scope.editFlag = 1;	// Display edit 상태 flag
	$scope.editFlag2 = 2;
	$scope.allCategory;	// allCategory list
	$scope.myCategory;	// myCategory list
	$scope.recSites;	// recommend site list
	$scope.learnLang;	// Menu language list
	$scope.myLang;	// Select language list
	$scope.mySite = [];	// my site list
	$scope.selectAllCategory = [];	// allCategory toggle 확인용
	$scope.selectedAll = [];	// allCategory 실제 선택된 category 객체 배열
	$scope.selectSiteCategory = [];	// toggle 확인용
	$scope.selectedSite = [];	// 실제 site 객체 배열
	$scope.data;	// 서버 저장용 전송 data
	$scope.dictionaryList;	// dictionary list
	$scope.selectMyCategory;
	$scope.categoryUpdate = false;
	$scope.alphabetSort = [
        { name : "Alphabetical", sort : "asc" },
        { name : "Unalphabetical", sort : "desc" }
    ];
	$scope.allCategorySortOption = $scope.alphabetSort[0];
	$scope.myCategorySortOption = $scope.alphabetSort[0];
	$scope.recSitesSortOption = $scope.alphabetSort[0];
	$scope.mySiteSortOption = $scope.alphabetSort[0];
	            
	$scope.learnList = [];
	// 처음 해당 유저의 Setting 정보 가져오기
	
	
	// My Category 클릭 시 추천 사이트 목록 & My Site 목록 가져오기
	$scope.getRecSites = function( category, idx ) {
		//console.log( category );
		//console.log( $scope.selectMyCategory );
		//console.log( $scope.myCategory.length );
		
		$scope.selectSiteCategory = [];
		$scope.selectedSite = [];
		
		if ( $scope.oldCategory == category ) {
			return;
		}
		
		// 추천 사이트 목록 가져옴
		$http.post( "/tools/getsiteinfo/", { "category": category }).success( function( result ) {
			//console.log("getRecSites");
			//console.log(result);
			if ( result.isSuccess == 1 ) {
				//console.log( result );
				$scope.recSites = result.recommendlist;
				$scope.oldCategory = category;
			}
		});
		
		if ( !$scope.myCategory[idx].sitelist )
			$scope.myCategory[idx].sitelist = [];
		$scope.mySite = $scope.myCategory[idx].sitelist;		
		console.log( $scope.mySite );
	};
	
	// All Category checkbox check toggle
	$scope.toggleSelection = function toggleSelection( category ) {
		var idx = $scope.selectAllCategory.indexOf( category.category_id );
		
		if ( idx > -1 ) {
			$scope.selectAllCategory.splice( idx, 1 );
			$scope.selectedAll.splice( idx, 1 );
		}
		else {
			$scope.selectAllCategory.push( category.category_id );
			$scope.selectedAll.push( category );
		}
		
		//console.log( $scope.selectedAll );
	};
	
	$scope.init = function() {
		$http.post( "/tools/getlanguageinfo/" ).success( function( result ) {
			console.log(result);
			if ( result.isSuccess == 1 ) {
				$scope.selected_languages = [];
				$scope.selected_levels = [];
				
				var option = {
						language2 : null,
						language1 : null,
						id : null
				};
				var option2 = {
						level_id : null,
						level_name : null
				};
				
				$scope.resetLearnList = angular.copy( result.mylearnlanglist ); // reset data 용
				//language list
				$scope.languageList = result.languagelist;
				
				//level list
				$scope.levelList = result.levellist;
				//my language list
				$scope.learnList = result.mylearnlanglist;
				$scope.native = [];
				$scope.learn = [];
				$scope.other = [];
				
				// 언어리스트 disable 처리
				for( var i = 0; i < result.mylearnlanglist.length; i++ ) {
					for ( var j = 0; j < result.languagelist.length; j++ ) {
						if ( result.mylearnlanglist[i].language_id == result.languagelist[j].id ) {
							result.languagelist[j].disable = 'true';
							break;
						}
					}
				}
				
				for ( var i = 0; i < result.mylearnlanglist.length; i++ ) {
					if ( result.mylearnlanglist[i].level == 7 ) {
						$scope.native.push( result.mylearnlanglist[i] );
					}
					else if ( result.mylearnlanglist[i].level == 1 ) {
						$scope.learn.push( result.mylearnlanglist[i] );
					}
					else {
						$scope.other.push( result.mylearnlanglist[i] );
					}
				}
				
				for(var i=0; i<result.mylearnlanglist.length; i++){
					if ( result.mylearnlanglist[i].learn_speak == 1){
						$scope.learnList[i].learn_speak = true;
					}else{
						$scope.learnList[i].learn_speak = false;
					}/*
				if ( result.mylearnlanglist[i].learn_primary == 1){
					$scope.learnList[i].learn_primary = true;
				}else{
					$scope.learnList[i].learn_primary = false;
				}
				if ( result.mylearnlanglist[i].primary == 1){
					$scope.learnList[i].primary = true;
				}else{
					$scope.learnList[i].primary = false;
				}*/
				}
				//user info
				$scope.userInfo = result.userinfo;
				$scope.user_id = result.user_id;
				user_id = result.user_id;
				//selected language option
				for(var i = 0; i < $scope.languageList.length; i++){
					for(var j=0; j < $scope.learnList.length; j++){
						if($scope.learnList[j].language_id == $scope.languageList[i].id){
							option = $scope.languageList[i];
							$scope.selected_languages.push(option);
							break;
						}
					}
				}
				
				//console.log('learnList');
				//console.log($scope.learnList);
				//selected level option
				for(var i = 0; i < $scope.learnList.length; i++){
					for(var j=0; j < $scope.levelList.length; j++){
						if($scope.learnList[i].level == $scope.levelList[j].level_id){
							option2 = $scope.levelList[j];
							$scope.selected_levels.push(option2);
							break;
						}
						if( j == $scope.levelList.length-1){
							$scope.selected_levels.push( null );
						}
					}
				}
				
			}
		});
		
		$scope.dataReset();
		
		
	}
	
	$scope.resetLanguage = function() {
		var tmpLang = [];
		angular.copy( $scope.resetLearnList, tmpLang );
		$scope.native = [];
		$scope.learn = [];
		$scope.other = [];
		console.log( $scope.resetLearnList );
		for ( var i = 0; i < tmpLang.length; i++ ) {
			if ( tmpLang[i].level == 7 ) {
				$scope.native.push( tmpLang[i] );
			}
			else if ( tmpLang[i].level == 1 ) {
				$scope.learn.push( tmpLang[i] );
			}
			else {
				$scope.other.push( tmpLang[i] );
			}
		}
	}
	
	$scope.dataReset = function() {
		$http.post( "/tools/getdata/" ).success( function( result ) {
			console.log( result );
			$scope.reset = angular.copy( result.data );
			if ( result.isSuccess == 1 ) {
				$scope.allCategory = result.categorylist;
				$scope.dictionaryList = result.dictionarylist;
				if ( result.data ) {
					$scope.learnLang = result.data;
					$scope.myLang = $scope.learnLang[0];				
					$scope.myCategory = $scope.myLang.categorylist;
					$scope.selectMyCategory = $scope.myCategory[0].category_id;
					$scope.getRecSites( $scope.selectMyCategory, 0 );
				}
				if ( $scope.myCategory ) {
					
				}
				
			}
		});
		
		$scope.customCategoryID = 0;
		$scope.customSiteID = 0;
	}
	
	$scope.addLanguage = function( type ) {
		var item = {};
		item.language_id = 0;
		item.learn_primary = 0;
		item.primary = 0;
		item.user_id = $scope.user_id;
		if ( type == 'native' ) {
			if ( $scope.native.length >= 3 ) {
				alert( 'You can select up to a maximum of three languages.' );
				return;
			}
			item.level = 7;
			item.learn_speak = 0;
			$scope.native.push( item );
		}
		else if ( type == 'learn' ) {
			if ( $scope.learn.length >= 3 ) {
				alert( 'You can select up to a maximum of three languages.' );
				return;
			}
			item.level = 1;
			item.learn_speak = 1;
			$scope.learn.push( item );
		}
		else {
			if ( $scope.other.length >= 3 ) {
				alert( 'You can select up to a maximum of three languages.' );
				return;
			}
			item.level = 2;
			item.learn_speak = 0;
			$scope.other.push( item );
		}
	}
	
	$scope.removeLanguage = function( type, index ) {
		if ( type == 'native' ) {
			$scope.native.splice( index, 1 );
		}
		else if ( type == 'learn' ) {
			$scope.learn.splice( index, 1 );
		}
		else {
			$scope.other.splice( index, 1 );
		}
	}
	
	$scope.changeLanguage = function( newvalue, oldvalue ) {
		console.log( newvalue );
		console.log( oldvalue );
		var tmp = newvalue.language_id;
		newvalue.language_id = oldvalue;
		for ( var i = 0; i < $scope.native.length; i++ ) {
			if ( tmp == $scope.native[i].language_id ) {
				alert( 'native' );
				return;
			}
		}
		for ( var i = 0; i < $scope.learn.length; i++ ) {
			if ( tmp == $scope.learn[i].language_id ) {
				alert( 'learn' );
				return;
			}
		}
		for ( var i = 0; i < $scope.other.length; i++ ) {
			if ( tmp == $scope.other[i].language_id ) {
				alert( 'other' );
				return;
			}
		}
		newvalue.language_id = tmp;
	}

	$scope.moveMyCategory = function() {
		var flag = true;
		var myCategorylength = $scope.myCategory.length;
		for ( var i = 0; i < $scope.selectedAll.length; i++ ) {
			for ( var j = 0; j < myCategorylength; j++ ) {
				//console.log( j );
				if ( $scope.selectedAll[i].category_id == $scope.myCategory[j].category_id )
					flag = false;
			}
			if ( flag ) {
				$scope.myCategory.push( $scope.selectedAll[i] );
				console.log( $scope.myCategory );
			}
			flag = true;
		}
	}
	
	//Display tab 
	//+Add another language
	/*$scope.add_language = function(){

		if($scope.learnList.length > 10){
			console.log($scope.learnList.length);
			alert("더이상 추가 하실 수 없습니다.");
			return;
		}
		
		var learn={
			learn_speak: null,
			user_id: null,
			level: null,
			language_id: null,
			primary: null,
			learn_primary: null
		};
		//TODO 수정.
		learn.user_id = user_id;
		$scope.learnList.push(learn);
	}
	*/
	//display language save button
	$scope.save_language = function(language){
		console.log(language);
		$scope.userInfo.display_language_id = language.id;
		$scope.userInfo.display_language_name = language.language1;
	}
	//I live in save button
	$scope.save_live_in = function(){
		$scope.userInfo.country = $("#country").val();
		$scope.userInfo.state = $("#state").val();
	}
	//language select option change
	$scope.language_change = function(language, learn, oldvalue, index){
		//console.log(language);
		//console.log($scope.oldvalue);
		//console.log(oldvalue);
		//console.log(learn);
		//console.log($scope.learnList);
		for(var i=0; i<$scope.learnList.length ; i++){
			if( $scope.learnList[i].language_id == language.id ){
				
				
				if(oldvalue != "" && oldvalue != null && oldvalue != 'undefined'){
					$scope.selected_languages[index] = oldvalue;
					learn.language_id = oldvalue.id;
					//console.log('if');
					//console.log(oldvalue);
				}else{
					$scope.selected_languages[index] = null;
					learn.language_id = null;
					//console.log('else');
					//console.log(oldvalue);
				}
				alert('다른 언어를 선택해 주세요.');
				return;
			}
		}
		
		
		
		learn.language_id = language.id;
	}
	//level select option change
	/*$scope.level_change = function(level, learn){
		console.log(level.level_name);
		
		if(level.level_name == 'Native'){
			//TODO 
			//I'm learnig uncheck
			learn.learn_speak = false;
			//main learning language uncheck
			learn.learn_primary = 0;
		}else{
			//TODO
			//Main native language uncheck
			learn.primary = 0;
		}
		
		learn.level = level.level_id;
	}*/
	$scope.check_valid = function(){
		var valid = true;
		for(var i=0; i < $scope.learnList.length; i++){
			if( $scope.learnList[i].language_id == null ){
				valid = false;
			}
		}
		return valid;
	}
	$scope.language_delete = function(target){
		console.log($scope.learnList);
		console.log(target);
		$scope.learnList.splice(target,1);
		
		console.log($scope.learnList);
		$scope.check_select();
	}
	$scope.check_select = function(){
		$scope.selected_languages = [];
		$scope.selected_levels = [];
		
		
		for(var i = 0; i < $scope.learnList.length; i++){
			for(var j=0; j < $scope.languageList.length; j++){
				console.log('----------------------------------');
				console.log(j + ', ' + $scope.learnList[i].language_id);
				if($scope.learnList[i].language_id == $scope.languageList[j].id){
					console.log('if');
					console.log($scope.learnList[i].language_id);
					console.log($scope.languageList[j].id);
					option = $scope.languageList[j];
					$scope.selected_languages.push(option);
					break;
				}
			}
		}
		for(var i = 0; i < $scope.learnList.length; i++){
			for(var j=0; j < $scope.levelList.length; j++){
				if($scope.learnList[i].level == $scope.levelList[j].level_id){
					option2 = $scope.levelList[j];
					$scope.selected_levels.push(option2);
					break;
				}
				if( j == $scope.levelList.length-1){
					$scope.selected_levels.push( null );
				}
			}
		}
	}
	$scope.display_save = function(){
		$scope.userLanguage = [];
		$scope.userLanguage = $scope.userLanguage.concat( $scope.native, $scope.learn, $scope.other );
		for ( var i = 0; i < $scope.userLanguage.length; i++ ) {
			if ( $scope.userLanguage[i].language_id == 0 ) {
				alert( 'select language' );
				return;
			}
		}
		console.log( $scope.userLanguage ); 
		/*if( !$scope.check_valid() ){
			alert('언어를 선택해 주세요.');
			return;
		}*/
		/*var radio1 = $(':radio[name="primary"]:checked').attr('id');
		var radio2 = $(':radio[name="primary2"]:checked').attr('id');
		
		//my language list
		for(var i=0; i<$scope.learnList.length; i++){
			
			//I'm learning
			if ( $scope.learnList[i].learn_speak == true){
				$scope.learnList[i].learn_speak = 1;
			}else{
				$scope.learnList[i].learn_speak = 0;
			}
			
			$scope.learnList[i].primary=0;
			$scope.learnList[i].learn_primary=0;
			
			//Main native language
			if ( i == radio1 ){
				$scope.learnList[i].primary = 1;
			}
			//Main learning language
			if ( i == radio2 ){
				$scope.learnList[i].learn_primary = 1;
			}
			
		}*/
		
		var obj= {'learnList':$scope.userLanguage, 'userInfo': $scope.userInfo };

		console.log(obj);
		$scope.updateLoading = true;
		$http.post( "/tools/setlanguageinfo/", obj  ).success( function( result ) {
			$scope.updateLoading = false;
			if ( result.isSuccess == 1 ) {
				console.log(result);
				alert(result.message);
				$window.location.href="/tellpinsearch/settings/";
			}
		});
		
		//user info
		//console.log( $scope.userInfo );
	}
	
	
	$scope.toggleSiteSelection = function toggleSelection( site ) {
		var idx = $scope.selectSiteCategory.indexOf( site.site_id );
		
		if ( idx > -1 ) {
			$scope.selectSiteCategory.splice( idx, 1 );
			$scope.selectedSite.splice( idx, 1 );
		}
		else {
			$scope.selectSiteCategory.push( site.site_id );
			$scope.selectedSite.push( site );
		}
		//console.log( $scope.selectedSite );
	};
	
	$scope.moveMySite = function() {
		var flag = true;
		var mySitelength = $scope.mySite.length;
		for ( var i = 0; i < $scope.selectedSite.length; i++ ) {
			for ( var j = 0; j < mySitelength; j++ ) {
				console.log( j );
				if ( $scope.selectedSite[i].site_id == $scope.mySite[j].site_id )
					flag = false;
			}
			if ( flag ) {
				$scope.mySite.push( $scope.selectedSite[i] );
			}
			flag = true;
		}
	}
	
	$scope.makeInitial = function() {
		/*$http.post( "/tools/getdata/" ).success( function( result ) {
			if ( result.isSuccess == 1 ) {
				$scope.learnLang = result.data;
				$scope.myLang = $scope.learnLang[0];
				$scope.allCategory = result.categorylist;
				$scope.myCategory = $scope.myLang.categorylist;
				$scope.dictionaryList = result.dictionarylist;
				
			}
		});*/
		//$window.location.reload();
		console.log( $scope.reset );
		console.log( $scope.learnLang );
		angular.copy( $scope.reset, $scope.learnLang );
		$scope.myLang = $scope.learnLang[0];				
		$scope.myCategory = $scope.myLang.categorylist;
		$scope.selectMyCategory = $scope.myCategory[0].category_id;
		$scope.getRecSites( $scope.selectMyCategory, 0 );
		$scope.customCategoryID = 0;
		$scope.customSiteID = 0;
	};
	
	$scope.changeMenu = function changeMenu( lang ) {
		for ( var i = 0; i < $scope.learnLang.length; i++ ) {
			if ( lang.id == $scope.learnLang[i].id ) {
				$scope.myLang = $scope.learnLang[i];
				break;
			}
		}
		//$scope.myLang = lang;
		$scope.myCategory = $scope.myLang.categorylist;
		console.log( lang );
		$scope.recSites = [];
		$scope.mySite = [];
		$scope.selectAllCategory = [];	// allCategory toggle 확인용
		$scope.selectedAll = [];	// allCategory 실제 선택된 category 객체 배열
		$scope.selectSiteCategory = [];	// toggle 확인용
		$scope.selectedSite = [];	// 실제 site 객체 배열
		$scope.customCategoryID = 0;
		$scope.customSiteID = 0;
		$http.post( "/tools/getcategory/", { "language" : $scope.myLang.id } ).success( function( result ) {
			if ( result.isSuccess == 1 ) {
				//console.log( result.dictionarylist );
				$scope.allCategory = result.categorylist;
				$scope.dictionaryList = result.dictionarylist;
				if ( $scope.myCategory[0] ) {
					
					$scope.selectMyCategory = $scope.myCategory[0].category_id;
					$scope.getRecSites( $scope.selectMyCategory, 0 );
				}
			}
			
		});
	}
	
	$scope.openModal = function( id ) {
		if ( id == 'siteModal' ) {
			$scope.customSiteName = '';
			$scope.selectMyCategory = $scope.myCategory[0].category_id;
			$scope.customSiteUrl = '';
			angular.element( '#' + id ).modal();
			$timeout( function() {
				$( '#customSite' ).focus();
			}, 470);
		}
		else if ( id == 'categoryModal' ) {
			$scope.customCategoryName = '';
			angular.element( '#' + id ).modal();
			$timeout( function() {
				$( '#customCategory' ).focus();
			}, 460);
		}
	}
	
	$scope.hideModal = function( id ) {
		angular.element( '#' + id ).modal( 'hide' );
	}
	
	$scope.addCategory = function() {
		console.log( $scope.myCategory );
		var category = {};
		category.category_id = --$scope.customCategoryID;
		category.category_name = $scope.customCategoryName;
		category.dictionary = 0;
		category.language_id = $scope.myLang.id;
		category.sitelist = [];
		console.log( category );
		if ( !$scope.customCategoryName || $scope.customCategoryName == '' ) {
			alert( 'Input category name' );
			$( '#customCategory' ).focus();
			return;
		}
		else {
			$scope.myCategory.push( category );			
		}
		$scope.hideModal( 'categoryModal' );
	}
	
	$scope.addSite = function() {
		console.log( $scope.myCategory );
		console.log( $scope.mySite );
		var site = {};
		if ( !$scope.customSiteUrl || $scope.customSiteUrl.indexOf( 'TEST' ) < 0  ) {
			alert( 'The brower\'s address was not valid.\nPlease try again.' );
		}
		else {
			site.search_url = $scope.customSiteUrl.replace( 'TEST', '@^tskw^@' );
			site.site_id = --$scope.customSiteID;
			site.site_name = $scope.customSiteName;
			var siteUrlCheck = /https?:\/\/(?:www\.)?([-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b)*(\/[\/\d\w\.-]*)*(?:[\?])*(.+)*/gi;
			siteArr = siteUrlCheck.exec( $scope.customSiteUrl );
			site.site_url = siteArr[1];
			
			for ( var i = 0; i < $scope.myCategory.length; i++ ) {
				if ( $scope.myCategory[i].category_id == $scope.selectMyCategory ) {
					$scope.myCategory[i].sitelist.push( site );
					break;
				}
			}
			console.log( site );
			$scope.hideModal( 'siteModal' );
		}
	}
	
	$scope.saveCategory = function saveCategory() {
		console.log( $scope.learnLang );
		$scope.categoryUpdate = true;
		$http.post( "/tools/setdata/", { "data" : $scope.learnLang } ).success( function( result ) {
			console.log( $scope.learnLang );
			if ( result.isSuccess == 1 ) {
				$scope.categoryUpdate = false;
				$window.alert( "Save Success." );
			}
		});
		
	}
	
	$scope.removeCategory = function removeCategory( index ) {
		if($scope.myCategory.length > 1){
			$scope.myCategory.splice( index, 1 );
			$scope.recSites = [];
			$scope.mySite = [];
		} else {
			$window.alert( "You can't get rid of 1 column" );
		}
	}
	
	$scope.removeSite = function removeSite( index ) {
		if($scope.mySite.length >= 1){
			$scope.mySite.splice( index, 1 );
		} else {
			$window.alert( "You can't get rid of 1 column" );
		}
	}
	
}]);

function InpuOnlyNumber(obj){
    if (event.keyCode >= 48 && event.keyCode <= 57) { //숫자키만 입력
    	$('.invalid').css('display', 'none');
        return true;
    } else {
    	//$('.invalid').css('display', 'inline');
        event.returnValue = false;
    }
}


