/**
 * set_page.js
 */

//var setApp = angular.module( "setApp", ["angular-sortable-view"]);
var user_id = "";

tellpinApp.controller( "pinListCtrl", function( $scope, $http, $location, $window, $timeout ) {
	$scope.selection = true;	// 체크 상태 toggle
	$scope.allToggleSelection = [];
	$scope.pinToggleSelection = [];
	$scope.allSelection = [];	// All 카테고리 단어에서 선택된 단어 담는 배열
	$scope.pinSelection = [];	// Pin 카테고리 단어에서 선택된 단어 담는 배열
	$scope.allSelectedIndex = [];
	$scope.pinSelectedIndex1 = [];
	$scope.pinSelectedIndex2 = [];
	$scope.pinSelectedIndex3 = [];
	$scope.pinCategory = 1;		// Pin 카테고리 번호
	$scope.allHistory;
	$scope.pinHistory = [];
	$scope.pinHistory1; 
	$scope.pinHistory2;
	$scope.pinHistory3;
	$scope.pinName1;
	$scope.pinName2;
	$scope.pinName3;
	$scope.updateLoading = false;
	$scope.myLanguageList;
	$scope.selectedLanguage;
	$scope.myDictionaryList;
	$scope.selectedDictionary;
	$scope.mySiteList;
	$scope.selectedSite;
	$scope.selectedPlayMenu = null;
	$scope.playFrom;
	$scope.playTo;
	$scope.playInterval=5000;
	$scope.deletingType;
	$scope.renameType;
	$scope.renameTxt="";
	$scope.playHover = 1;
	$scope.pauseHover = 3;
	$scope.repeatHover = 1;
	$scope.stopHover = 1;
	/*$http.post( "/tools/gethistory/" ).success( function( result ) {
		if ( result.isSuccess == 1 ) {
			$scope.allHistory = result.historylist;
			$scope.pinHistory1 = result.mypin1;
			$scope.pinHistory2 = result.mypin2;
			$scope.pinHistory3 = result.mypin3;
		}
	});
	*/
	$scope.pinFlag1 = true;		// #Pin1
	$scope.pinFlag2 = false;	// #Pin2
	$scope.pinFlag3 = false;	// #Pin3
	
	$scope.rangeFlag1 = true;
	$scope.rangeFlag2 = false;
	// 기본 Pin 카테고리 1로 설정
	//$scope.pinHistory = $scope.pinHistory1;
	
	$scope.allScrollAction = false;
	$scope.pin1ScrollAction = false;
	$scope.pin2ScrollAction = false;
	$scope.pin3ScrollAction = false;
	
	
	$scope.init = function( obj ) {
		$scope.allScrollAction = false;
		$scope.pin1ScrollAction = false;
		$scope.pin2ScrollAction = false;
		$scope.pin3ScrollAction = false;
		$scope.allHistory = obj.historylist;
		$scope.pinHistory = obj.mypin1;
		$scope.pinHistory1 = obj.mypin1;
		$scope.pinHistory2 = obj.mypin2;
		$scope.pinHistory3 = obj.mypin3;
		$scope.allStart = $scope.allHistory.length;
		$scope.pin1Start = $scope.pinHistory1.length;
		$scope.pin2Start = $scope.pinHistory2.length;
		$scope.pin3Start = $scope.pinHistory3.length;
		$scope.slider = {
				value: 1,
				options: {
					floor: 1,
				    ceil: 4,
					step: 1
				}
		}
		$scope.playerSpeed = 1;
	}
	
	$scope.checkSlider = function() {
		alert( $scope.slider.value );
	}
	
	// History 무한스크롤
	$scope.getMoreHistory = function( type ) {
		console.log( '$$$$$$ Infinite' );
		console.log( '###### type', type );
		var page;
		if ( type == 1 ) {
			$scope.allScrollAction = true;
			page = $scope.allHistory.length;
		}
		else if ( type == 2 ) {
			$scope.pin1ScrollAction = true;			
			page = $scope.pinHistory1.length;
		}
		else if ( type == 3 ) {
			$scope.pin2ScrollAction = true;
			page = $scope.pinHistory2.length;
		}
		else {	// for 'All'
			$scope.pin3ScrollAction = true;
			page = $scope.pinHistory3.length;
		}
		console.log( page );
		$http.post( '/tellpinsearch/more/history/', { 'start' : page, 'type' : type } )
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				if ( type == 1 ) {
					$scope.allScrollAction = false;
					$scope.allStart += 10;
					$scope.allHistory = $scope.allHistory.concat( result.historylist );
				}
				else if ( type == 2 ) {
					$scope.pin1ScrollAction = false;
					$scope.pin1Start += 10;
					$scope.pinHistory1 = $scope.pinHistory1.concat( result.mypin1 );
				}
				else if ( type == 3 ) {
					$scope.pin2ScrollAction = false;
					$scope.pin2Start += 10;
					$scope.pinHistory2 = $scope.pinHistory2.concat( result.mypin2 );
				}
				else {
					$scope.pin3ScrollAction = false;
					$scope.pin3Start += 10;
					$scope.pinHistory3 = $scope.pinHistory3.concat( result.mypin3 );
				}
			}
		});
	}
	
	$http.post( "/tools/getAutoSearchPlayerData/").success( function( result ) {
		if ( result.isSuccess == 1 ) {
			$scope.myLanguageList = result.myLanguageList;
			$scope.myDictionaryList = result.myDictionaryList;
			$scope.mySiteList = result.mySiteList;
			$scope.selectedLanguage = $scope.myLanguageList[0]; 
			$scope.selectedDictionary = $scope.myDictionaryList[0];
			$scope.selectedSite = $scope.mySiteList[0];
		}
	});
	
	$scope.changeMyLanguage = function (selectedLang) {
		$http.post( "/tools/getAutoSearchPlayerDic/", { "id": selectedLang.id }).success( function( result ) {
			if ( result.isSuccess == 1 ) {
				$scope.myDictionaryList = result.myDictionaryList;
				$scope.mySiteList = result.mySiteList;
				$scope.selectedDictionary = $scope.myDictionaryList[0];
				$scope.selectedSite = $scope.mySiteList[0];
			}
		});
	}
	
	$scope.changeMyDictionary = function (selectedDic) {
		$http.post( "/tools/getAutoSearchPlayerSite/", { "id": selectedDic.category_id }).success( function( result ) {
			if ( result.isSuccess == 1 ) {
				$scope.mySiteList = result.mySiteList;
				$scope.selectedSite = $scope.mySiteList[0];
			}
		});
	}
	
	/*$http.post( "/tools/gethistory/").success( function( result ) {
		if ( result.isSuccess == 1 ) {
			console.log(result);
			$scope.allHistory = result.historylist;
			$scope.pinHistory = result.mypin1;
			$scope.pinHistory1 = result.mypin1;
			$scope.pinHistory2 = result.mypin2;
			$scope.pinHistory3 = result.mypin3;
		}
	});*/
	
	$http.post( "/tools/get_pin_name/").success( function( result ) {
		if ( result.isSuccess == 1 ) {
			console.log('get_pin_name',result);
			
			if(result.pinName1 == null){
				$scope.pinName1 = 'Pin List #1'
			} else {
				$scope.pinName1 = result.pinName1;
			}
			
			if(result.pinName2 == null){
				$scope.pinName2 = 'Pin List #2'
			} else {
				$scope.pinName2 = result.pinName2;
			}
			
			if(result.pinName3 == null){
				$scope.pinName3 = 'Pin List #3'
			} else {
				$scope.pinName3 = result.pinName3;
			}
		}
	});
	

	$scope.toggleSelection = function ( type, idx ) {
		temp=[];
		if(type == 'all'){
			temp = $scope.allSelectedIndex;
		} else if(type == 'pin1') {
			temp = $scope.pinSelectedIndex1;
		} else if(type == 'pin2') {
			temp = $scope.pinSelectedIndex2;
		} else if(type == 'pin3') {
			temp = $scope.pinSelectedIndex3;
		}
		
		// 해당 단어가 존재하지 않을 경우 배열에 push
		if ( temp.indexOf(idx) < 0 ) {
			temp.push(idx);
			
			if(type == 'all'){
				$(".cover-all-content").css("display", "block");
				$(".paste-btn").css("display", "block");
			}
		}
		// 해당 단어가 존재할 경우 선택제거
		else {
			temp.splice(temp.indexOf(idx), 1);
			
			if(type == 'all'){
				if(temp==null || temp.length < 1){
					$(".cover-all-content").css("display", "none");
					$(".paste-btn").css("display", "none");
				}
			}
		}
		
		if(type == 'all'){
			$scope.allSelectedIndex = temp;
		} else if(type == 'pin1') {
			$scope.pinSelectedIndex1 = temp;
		} else if(type == 'pin2') {
			$scope.pinSelectedIndex2 = temp;
		} else if(type == 'pin3') {
			$scope.pinSelectedIndex3 = temp;
		}
	}
	
	// 전체선택 전체 해제
	$scope.allWordSelect = function ( type ) {
		var indexList;
		var dataList;
		if(type == 'all'){
			indexList = $scope.allSelectedIndex;
			dataList = $scope.allHistory;
		} else if(type == 'pin1') {
			indexList = $scope.pinSelectedIndex1;
			dataList = $scope.pinHistory1;
		} else if(type == 'pin2') {
			indexList = $scope.pinSelectedIndex2;
			dataList = $scope.pinHistory2;
		} else if(type == 'pin3') {
			indexList = $scope.pinSelectedIndex3;
			dataList = $scope.pinHistory3;
		}
		
		if (  ( indexList.length < dataList.length ) && ( indexList.length >= 0 ) ) {
			indexList = [];
			for ( var i = 0; i < dataList.length; i++ ) {
				indexList.push( i );
				
				if(type == 'all'){
					$(".cover-all-content").css("display", "block");
					$(".paste-btn").css("display", "block");
				}
			}
		}
		else {
			indexList = [];
			
			if(type == 'all'){
				$(".cover-all-content").css("display", "none");
				$(".paste-btn").css("display", "none");
			}
		}
		
		if(type == 'all'){
			$scope.allSelectedIndex = indexList;
		} else if(type == 'pin1') {
			$scope.pinSelectedIndex1 = indexList;
		} else if(type == 'pin2') {
			$scope.pinSelectedIndex2 = indexList;
		} else if(type == 'pin3') {
			$scope.pinSelectedIndex3 = indexList;
		}
	}
	
	// -표시 누르면 선택 해제
	$scope.allCancelSelect = function (type) {
		if(type == 'all'){
			$scope.allSelectedIndex = [];
			$(".cover-all-content").css("display", "none");
			$(".paste-btn").css("display", "none");
		} else if(type == 'pin1') {
			$scope.pinSelectedIndex1 = [];
		} else if(type == 'pin2') {
			$scope.pinSelectedIndex2 = [];
		} else if(type == 'pin3') {
			$scope.pinSelectedIndex3 = [];
		}
	}
	
	$scope.checkAllSelection = function(type, str ) {
		var indexList;
		var dataList;
		
		if(type == 'all'){
			indexList = $scope.allSelectedIndex;
			dataList = $scope.allHistory;
		} else if(type == 'pin1') {
			indexList = $scope.pinSelectedIndex1;
			dataList = $scope.pinHistory1;
		} else if(type == 'pin2') {
			indexList = $scope.pinSelectedIndex2;
			dataList = $scope.pinHistory2;
		} else if(type == 'pin3') {
			indexList = $scope.pinSelectedIndex3;
			dataList = $scope.pinHistory3;
		}
		
		if ( str == "check" ) {
			if ( ( dataList && indexList.length == dataList.length ) && ( indexList.length != 0 ) )
				return true;
			return false;
		}
		else {
			if ( ( indexList.length > 0 && indexList.length < dataList.length ) )
				return false;
			return true;
		}
	}

	
	// 카테고리 변경 시 기본 카테고리 번호 바꿔주기
	$scope.pinChanged = function ( number ) {
		console.log('pinChangeed()');
		if ( number == 1 )
			$scope.pinCategory = 1;
		else if ( number == 2 )
			$scope.pinCategory = 2;
		else
			$scope.pinCategory = 3;
		
		$scope.pinSelection = [];
		$scope.pinToggleSelection = [];
	}
	
	
	// All -> Pin 추가하는 함수
	$scope.copyWord = function (type) {
		if(type == 1){
			dataList = $scope.pinHistory1;
		} else if(type == 2){
			dataList = $scope.pinHistory2;
		} else if(type == 3){
			dataList = $scope.pinHistory3;
		} 

		var wordList=[]; 
		for( var i=0; i < dataList.length; i++){
			wordList.push(dataList[i].word);
		}
		
		for( var i=0; i < $scope.allSelectedIndex.length; i++){
			if(wordList.indexOf( $scope.allHistory[$scope.allSelectedIndex[i]].word ) == -1){
				dataList.push($scope.allHistory[$scope.allSelectedIndex[i]]);
			}
		}
		
		var temp = [];
		var pinFlag = false;
		
		
		for( var i = 0; i < dataList.length; i++ ) {
			if ( i == 0 ) {
				temp.push( dataList[i] );
			}
			else {
				for ( var j = 0; j < temp.length; j++ ) {
					if ( dataList[i].word == temp[j].word ) {
						pinFlag = true; // 중복단어가 있을 경우
					}
					console.log( j );
				}
				if ( !pinFlag ) {
					temp.push( dataList[i] );
				}
				pinFlag = false;
			}
		}
		
		console.log( temp );
		if(type == 1){
			$scope.pinHistory1 = temp;
		} else if(type == 2){
			$scope.pinHistory2 = temp;
		} else if(type == 3){
			$scope.pinHistory3 = temp;
		} 
		
		$scope.log_save('pin');
		$scope.allCancelSelect('all');
		$(".cover-all-content").css("display", "none");
		$(".paste-btn").css("display", "none");
	}
	
	// 선택 된 단어들 txt파일 만들어서 다운로드
	$scope.makeTxt = function ( type ) {
		//console.log( data );
		$scope.removeFilePath;
		/*data.sort(function(a, b) {
			var textA = a.toUpperCase();
			var textB = b.toUpperCase();
			return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
		});*/
		var indexList;
		var dataList;
		//console.log( $scope.allSelectedIndex );
		
		if(type == 'all'){
			indexList = $scope.allSelectedIndex.sort( function( a, b ) { return a - b} );
			dataList = $scope.allHistory;
		} else if(type == 'pin1') {
			indexList = $scope.pinSelectedIndex1.sort( function( a, b ) { return a - b} );
			dataList = $scope.pinHistory1;
		} else if(type == 'pin2') {
			indexList = $scope.pinSelectedIndex2.sort( function( a, b ) { return a - b} );
			dataList = $scope.pinHistory2;
		} else if(type == 'pin3') {
			indexList = $scope.pinSelectedIndex3.sort( function( a, b ) { return a - b} );
			dataList = $scope.pinHistory3;
		}
		//console.log( indexList );
		//console.log( Number(9));
		var word = [];
		
		for ( var i = 0; i < indexList.length; i++ ) {
			word.push( dataList[indexList[i]] );
		}
		
		// 브라우저체크 ie -> if, other -> else
		var wordlist = '';
		for ( var i = 0; i < word.length; i++ ) {
			wordlist += word[i].word;
			if ( i <  word.length - 1 ) {
				wordlist += '\r\n';
			} 
		}
		console.log( wordlist );
		var blob = new Blob( [ wordlist ], { type : 'text/plain;charset=utf-8' });
		saveAs( blob, 'word.txt' );
		/*
		$http.post( "/tools/make/", { word : word } )
		.success( function ( response ) {
			//console.log(response);
			
		===================================
			a 태그 생성 후  해당 태그에 
			href 속성과 download 속성을 설정해주고
			클릭 해줌으로써 다운로드 실행
		===================================
			 
			var atag = document.createElement("a");
			atag.setAttribute( "href", response );
			atag.setAttribute( 'id', 'wordTxt' );
			atag.setAttribute( "download", "word.txt" );
			document.body.appendChild( atag );
			atag.click();
			$( '#wordTxt' ).remove();
			$scope.removeFilePath = response;
			//console.log( $scope.removeFilePath );
		});			
		*/
	}
	
	// 페이지 변환 시에 서버에 만들어진 txt 파일
	// 삭제하는 함수
	$scope.$on( "$locationChangeStart", function( event ) {
		//console.log("페이지이동");
		$http.post( "/tools/remove/")
		.success( function ( result ) {
			
		});
	});
	
	$scope.displayStep = function() {
		console.log( $scope.playerSpeed );
	}

	$scope.log_save = function(type){

		if( type == 'all'){
			var obj = { 'myhistory': $scope.allHistory}
			
			$http.post('/tools/setmyhistory/', obj).success( function(result){
				
				if ( result.isSuccess == "1"){
					console.log('result',result);
					//alert(result.message);
				}
			});	
		} else {
			var obj = { 'mypin1': $scope.pinHistory1, 'mypin2': $scope.pinHistory2, 'mypin3': $scope.pinHistory3 }
			
			$http.post('/tools/setmypin/', obj).success( function(result){
				
				if ( result.isSuccess == "1"){
					console.log(result);
					//alert(result.message);
				}
			});	
		}
	}
	
	$scope.log_delete = function(){
		var indexList;
		var dataList;
		
		if($scope.deletingType == 1){
			indexList = $scope.allSelectedIndex;
			dataList = $scope.allHistory;
		} else if($scope.deletingType == 2){
			indexList = $scope.pinSelectedIndex1;
			dataList = $scope.pinHistory1;
		} else if($scope.deletingType == 3){
			indexList = $scope.pinSelectedIndex2;
			dataList = $scope.pinHistory2;
		} else if($scope.deletingType == 4){
			indexList = $scope.pinSelectedIndex3;
			dataList = $scope.pinHistory3;
		}

		for( var i=0; i < indexList.length; i++){
			dataList.splice(indexList[i], 1, null);
		}
		
		var tempList=[];
		for( var i=0; i < dataList.length; i++){
			if(dataList[i] != null){
				tempList.push(dataList[i]);
			}
		}
		
		if($scope.deletingType == 1){
			$scope.allSelectedIndex = [];
			$scope.allHistory = tempList;
		} else if($scope.deletingType == 2){
			$scope.pinSelectedIndex1 = [];
			$scope.pinHistory1 = tempList;
		} else if($scope.deletingType == 3){
			$scope.pinSelectedIndex2 = [];
			$scope.pinHistory2 = tempList;
		} else if($scope.deletingType == 4){
			$scope.pinSelectedIndex3 = [];
			$scope.pinHistory3 = tempList;
		}

		if($scope.deletingType == 1){
			$scope.log_save('all');
			$(".cover-all-content").css("display", "none");
			$(".paste-btn").css("display", "none");
		} else if($scope.deletingType > 1) {
			$scope.log_save('pin');
		}
		$scope.openDeleteConfirmModal(0);
	}
	
	$scope.changeMenu = function () {
		if ( $scope.rangeFlag2 == true ){
			$scope.setRange();
		}
	}
	
	$scope.changeRange = function ( number ) {
		if ( number == 1 ){
			$scope.rangeFlag1 = true;
			$scope.rangeFlag2 = false;
		}
		else if ( number == 2 ){
			$scope.rangeFlag1 = false;
			$scope.rangeFlag2 = true;
			
			$scope.setRange();
		}
	}
	
	$scope.setRange  = function () {
		if($scope.selectedPlayMenu == $scope.pinName1 ){
			$scope.playTo = $scope.pinHistory1.length;
		}
		else if($scope.selectedPlayMenu == $scope.pinName2 ){
			$scope.playTo = $scope.pinHistory2.length;
		}
		else if($scope.selectedPlayMenu == $scope.pinName3 ){
			$scope.playTo = $scope.pinHistory3.length;
		}
		else {	// for 'All'
			$scope.playTo = $scope.allHistory.length;
		}
		
		if($scope.playTo < 1){
			$scope.playFrom = 0;
		} else {
			$scope.playFrom = 1;
		}
	}
	
	$scope.showHelpBox = function ( number ) {
		if(number == 1){
			$(".search-help-box").css("display", "block");
		} else {
			$(".search-help-box").css("display", "none");
		}
	}
	
	$scope.setPinName = function () {
		if($scope.renameTxt.length==0){
			return;
		}
		
		if($scope.renameType == 1){
			var obj = { 'pin1': $scope.renameTxt }
		} else if($scope.renameType == 2){
			var obj = { 'pin2': $scope.renameTxt }
		} else if($scope.renameType == 3){
			var obj = { 'pin3': $scope.renameTxt }
		}
		
		$http.post('/tools/set_pin_name/', obj).success( function(result){
			if ( result.isSuccess == "1"){
				console.log('result',result);
				if(result.pinType == 'pin1'){
					$scope.pinName1 = $scope.renameTxt;
				} else if(result.pinType == 'pin2'){
					$scope.pinName2 = $scope.renameTxt;
				} else if(result.pinType == 'pin3'){
					$scope.pinName3 = $scope.renameTxt;
				}
			} else { 
				//alert(result.message);
			}
			$( '#renameModal' ).modal( 'hide' );
		});	
		
	}
	
	var timer;
	var search;
	var searchCount=0;
	$scope.isRepeat=false;
	$scope.searchPlayerStart = function() {
		console.log("searchPlayerStart");
		var pin_list = document.getElementById("sel_pin").value;
		urlArray = $scope.selectedSite.search_url.split("@^tskw^@");
		$scope.playInterval = ( 10000 - $scope.playerSpeed ) + 3000;
		wordArray=[];
		searchUrlArray=[];
		start=1;
		end=1;

		//단어리스트
		if (pin_list == "1") {
			wordArray = $scope.pinHistory1;
		} else if (pin_list == "2") {
			wordArray = $scope.pinHistory2;
		} else if (pin_list == "3") {
			wordArray = $scope.pinHistory3;
		} else {
			wordArray = $scope.allHistory;
		}
		//단어리스트
//		if($scope.selectedPlayMenu == $scope.pinName1){
//			wordArray = $scope.pinHistory1;
//		} else if($scope.selectedPlayMenu == $scope.pinName2){
//			wordArray = $scope.pinHistory2;
//		} else if($scope.selectedPlayMenu == $scope.pinName3){
//			wordArray = $scope.pinHistory3;
//		} else {	// for 'All'
//			wordArray = $scope.allHistory;
//		}
		
		if ( wordArray.length < 1 ) {
			return;
		}
		
		// 실행속도
		if ( $scope.playerSpeed == 1 ) {
			$scope.playInterval = 10000;
		} 
		else if ( $scope.playerSpeed == 2 ) {
			$scope.playInterval = 7000;
		}
		else if ( $scope.playerSpeed == 3 ) {
			$scope.playInterval = 5000;
		}
		else {
			$scope.playInterval = 3000;
		}
		//검색리스트 범위
		if($scope.playFrom != null && $scope.rangeFlag2 == true){
			start = $scope.playFrom;
		}
		if($scope.playTo != null && $scope.rangeFlag2 == true){
			end = $scope.playTo;
		} else { 
			end = wordArray.length;
		}
		
		for(var i=start-1 ; i < end ; i++){
			if(urlArray.length == 2){
				searchUrlArray.push(urlArray[0] + encodeURIComponent(wordArray[i].word.toLowerCase() ) + urlArray[1]);
			} else if(urlArray.length == 1) {
				searchUrlArray.push(searchURL = urlArray + encodeURIComponent( wordArray[i].wordtoLowerCase() ) );
			}
		}
		
		//$('#startButton').css('display', 'none');
		//$('#pauseButton').css('display', 'inline');
		$scope.playHover = 3;
		$scope.pauseHover = 1;
		
		clearInterval(timer);
		search = $window.open(searchUrlArray[searchCount], 'search', 'width=900, height=600');
		timer = setInterval(function() {
			if(search == null || search.closed){
				//$('#startButton').css('display', 'inline'); 
				//$('#pauseButton').css('display', 'none');
				$scope.playHover = 1;
				$scope.pauseHover = 3;
				clearInterval(timer);
			}else{
				searchCount++;
				if(searchCount < searchUrlArray.length){
					search = $window.open(searchUrlArray[searchCount], 'search', 'width=900, height=600');
				} else {
					searchCount = 0;
					if($scope.isRepeat){
						search = $window.open(searchUrlArray[searchCount], 'search', 'width=900, height=600');
					} else {
						clearInterval(timer);
						//$('#startButton').css('display', 'inline');
						//$('#pauseButton').css('display', 'none');
						$scope.playHover = 1;
						$scope.pauseHover = 3;
					}
				}
			}
		}, $scope.playInterval);
	}
	
	$scope.searchPlayerPause = function() {
		//$('#startButton').css('display', 'inline'); 
		//$('#pauseButton').css('display', 'none');
		$scope.playHover = 1;
		$scope.pauseHover = 3;
		clearInterval(timer);
	}
	
	$scope.searchPlayerStop = function() {
		//$('#startButton').css('display', 'inline'); 
		//$('#pauseButton').css('display', 'none');
		$scope.playHover = 1;
		$scope.pauseHover = 3;
		clearInterval(timer);
		searchCount = 0;
		if(search!=null){
			search.close();
		}
	}
	
	$scope.repeatType = function() {
		if ( $scope.isRepeat ) {
			$('#repeatButton').attr('src', '/static/img/tellpin_search/button_repeat_pressed.png');
			$scope.repeatHover = 2;
		}
		else {
			$('#repeatButton').attr('src', '/static/img/tellpin_search/button_repeat_pressed.png');
			$scope.repeatHover = 1;
		}
	}
	
	$scope.searchPlayerRepeat = function() {
		if($scope.isRepeat){
			$('#repeatButton').attr('src', '/static/img/tellpin_search/button_repeat_normal.png');
			$scope.isRepeat = false;
			$scope.repeatHover = 1;
		} else{
			$('#repeatButton').attr('src', '/static/img/tellpin_search/button_repeat_mint.png');
			$scope.repeatHover = 2;
			$scope.isRepeat = true;
		}
		console.log( $scope.repeatHover );			
	}
	
	$scope.checkFromToValue = function(type) {
		if($scope.rangeFlag2 == true){
			if(type == 1){
				if(Number($scope.playFrom) > Number($scope.playTo)){
					$scope.playFrom = 1;
					//alert("To 값보다 작게 입력해주세요.");
					$('.invalid').css('display', 'inline');
				} else{
					$('.invalid').css('display', 'none');
				}
			} else {
				max=0;
				//console.log( $scope.selectedPlayMenu );
				if($scope.selectedPlayMenu == $scope.pinName1){
					max = $scope.pinHistory1.length;
				} else if($scope.selectedPlayMenu == $scope.pinName2){
					max = $scope.pinHistory2.length;
				} else if($scope.selectedPlayMenu == $scope.pinName3){
					max = $scope.pinHistory3.length;
				} else {	// for 'All'
					max = $scope.allHistory.length;
				}

				console.log( max );
				console.log( $scope.playFrom );
				console.log( $scope.playTo );
				if( ( Number($scope.playFrom) > Number($scope.playTo) ) || ( Number($scope.playTo) > max ) ){
					$scope.playTo = max;
					//alert("From 값보다 크거나 단어장 수보다 작게 입력해주세요.");
					$('.invalid').css('display', 'inline');
				} else {
					$('.invalid').css('display', 'none');
				}
			}
		}
	}

	$scope.openDeleteConfirmModal = function( idx ) {
		if ( idx >= 1) { 
			$scope.deletingType = idx;
			$( "#deleteModal" ).modal();
		} else
			$( "#deleteModal" ).modal( "hide" );
	}
	
	$scope.openModifyPinNameModal = function( idx ) {
		$scope.renameFlag = false;
		if ( idx >= 1) { 
			$scope.renameType = idx;
			if ( idx == 1 ) {
				$scope.renameTxt = $scope.pinName1;
			}
			else if ( idx == 2 ) {
				$scope.renameTxt = $scope.pinName2;
			}
			else {
				$scope.renameTxt = $scope.pinName3;
			}
			$( "#renameModal" ).modal();
			$timeout( function() {
				console.log( 'focus' );
				$( '#renameFlag' ).select();
			}, 470);
			
			
		} else
			$( "#renameModal" ).modal( "hide" );
	}
});

// Setting Controller
tellpinApp.controller( "settingCtrl", [ '$scope', '$http', '$window', '$timeout', function( $scope, $http, $window, $timeout ) {
	$scope.init = function( obj ) {
		console.log( obj );
		$scope.languageInfo = obj.languageList;
		$scope.languageList = angular.copy( obj.languageList );
		$scope.userInfo = obj.userInfo;
		$scope.lanIndex = 0;
		$scope.setCategory( obj );
		$scope.selectRestoreCategory = [];
		$scope.selectRestoreSite = [];
		$scope.languageList.splice( 0, 0, {
			'id' : 2,
			'name' : 'English'
		});
		$scope.learningLanList();
//		$scope.otherLanList();
		$scope.nativeLanList();
		$scope.languageList.splice( ++$scope.lanIndex, 0, {"id": -1, "name": "-----------------------------------------", 'disabled' : true });
		$scope.deleteLanguageList( ++$scope.lanIndex );
		$scope.selectLanguage = $scope.languageList[0].id;
	}
	
	$scope.setCategory = function( obj ) {
		$scope.noDictList = [];
		$scope.dictList = [];
		$scope.siteList = [];
		$scope.siteList = obj.toolData.site;
		$scope.categoryList = obj.toolData.category;
		$scope.customCategoryID = 0;
		$scope.customSiteID = 0;
		for( var i = 0; i < obj.toolData.category.length; i++ ) {
			if ( obj.toolData.category[i].category_id > 16 && obj.toolData.category[i].category_id < 10001 ) {
				$scope.dictList.push( obj.toolData.category[i] );
			}
			else {
				$scope.noDictList.push( obj.toolData.category[i] );
			}
		}
		if ( $scope.dictList.length == 0 ) {
			$scope.selectCategory = $scope.noDictList[0].category_id;
		}
		else {
			$scope.selectCategory = $scope.dictList[0].category_id;
		}
	}
	
	$scope.nativeLanList = function() {
//		console.log("nativeLanList");
		if ($scope.userInfo.native1_id != null && $scope.userInfo.native1_id != 0) {
			if ( $scope.userInfo.native1_id == 2 )
				return;
			$scope.lanIndex++;
			$scope.lanMakeList("native1_id", $scope.lanIndex);
		}
		if ($scope.userInfo.native2_id != null && $scope.userInfo.native2_id != 0) {
			if ( $scope.userInfo.native2_id == 2 )
				return;
			$scope.lanIndex++;
			$scope.lanMakeList("native2_id", $scope.lanIndex);
		}
		if ($scope.userInfo.native3_id != null && $scope.userInfo.native3_id != 0) {
			if ( $scope.userInfo.native3_id == 2 )
				return;
			$scope.lanIndex++;
			$scope.lanMakeList("native3_id", $scope.lanIndex);
		}
	}
	$scope.learningLanList = function() {
//		console.log("learningLanList");
		if ($scope.userInfo.lang1_id != null && $scope.userInfo.lang1_id != 0) {
			if ( $scope.userInfo.lang1_id == 2 )
				return;
			$scope.lanIndex++;
			$scope.lanMakeList("lang1_id", $scope.lanIndex);
		}
		if ($scope.userInfo.lang2_id != null && $scope.userInfo.lang2_id != 0) {
			if ( $scope.userInfo.lang2_id == 2 )
				return;
			$scope.lanIndex++;
			$scope.lanMakeList("lang2_id", $scope.lanIndex);
		}
		if ($scope.userInfo.lang3_id != null && $scope.userInfo.lang3_id != 0) {
			if ( $scope.userInfo.lang3_id == 2 )
				return;
			$scope.lanIndex++;
			$scope.lanMakeList("lang3_id", $scope.lanIndex);
		}
		if ($scope.userInfo.lang4_id != null && $scope.userInfo.lang4_id != 0) {
			if ( $scope.userInfo.lang4_id == 2 )
				return;
			$scope.lanIndex++;
			$scope.lanMakeList("lang4_id", $scope.lanIndex);
		}
		if ($scope.userInfo.lang5_id != null && $scope.userInfo.lang5_id != 0) {
			if ( $scope.userInfo.lang5_id == 2 )
				return;
			$scope.lanIndex++;
			$scope.lanMakeList("lang5_id", $scope.lanIndex);
		}
		if ($scope.userInfo.lang6_id != null && $scope.userInfo.lang6_id != 0) {
			if ( $scope.userInfo.lang6_id == 2 )
				return;
			$scope.lanIndex++;
			$scope.lanMakeList("lang6_id", $scope.lanIndex);
		}
	}
//	$scope.otherLanList = function() {
////		console.log("otherLanList");
//		if ($scope.userInfo.other1 != null && $scope.userInfo.other1 != 0) {
//			if ( $scope.userInfo.other3 == 2 )
//				return;
//			$scope.lanIndex++;
//			$scope.lanMakeList("other1", $scope.lanIndex);
//		}
//		if ($scope.userInfo.other2 != null && $scope.userInfo.other2 != 0) {
//			if ( $scope.userInfo.other3 == 2 )
//				return;
//			$scope.lanIndex++;
//			$scope.lanMakeList("other2", $scope.lanIndex);
//		}
//		if ($scope.userInfo.other3 != null && $scope.userInfo.other3 != 0) {
//			if ( $scope.userInfo.other3 == 2 )
//				return;
//			$scope.lanIndex++;
//			$scope.lanMakeList("other3", $scope.lanIndex);
//		}
//	}	
	$scope.lanMakeList = function(name, index) {
//		console.log(name);
//		console.log($scope.userInfo[name]);
//		console.log(index);
		$scope.languageList.splice(index, 0, {"id": $scope.languageInfo[$scope.userInfo[name]-1].id, "name": $scope.languageInfo[$scope.userInfo[name]-1].name});
	}
	
	// 중복되는 언어리스트 제거
	$scope.deleteLanguageList = function( index ) {
		if ( $scope.userInfo.native1 != null && $scope.userInfo.native1 != 0 ) {
			for ( var i = index; i < $scope.languageList.length; i++ ) {
				if ( $scope.languageList[i].id == $scope.userInfo.native1 ) {
					$scope.languageList.splice( i, 1 );
					break;
				}
			}			
		}
		if ( $scope.userInfo.native2 != null && $scope.userInfo.native2 != 0 ) {
			for ( var i = index; i < $scope.languageList.length; i++ ) {
				if ( $scope.languageList[i].id == $scope.userInfo.native2 ) {
					$scope.languageList.splice( i, 1 );
					break;
				}
			}			
		}
		if ( $scope.userInfo.native3 != null && $scope.userInfo.native3 != 0 ) {
			for ( var i = index; i < $scope.languageList.length; i++ ) {
				if ( $scope.languageList[i].id == $scope.userInfo.native3 ) {
					$scope.languageList.splice( i, 1 );
					break;
				}
			}			
		}
		if ( $scope.userInfo.other1 != null && $scope.userInfo.other1 != 0 ) {
			for ( var i = index; i < $scope.languageList.length; i++ ) {
				if ( $scope.languageList[i].id == $scope.userInfo.other1 ) {
					$scope.languageList.splice( i, 1 );
					break;
				}
			}			
		}
		if ( $scope.userInfo.other2 != null && $scope.userInfo.other2 != 0 ) {
			for ( var i = index; i < $scope.languageList.length; i++ ) {
				if ( $scope.languageList[i].id == $scope.userInfo.other2 ) {
					$scope.languageList.splice( i, 1 );
					break;
				}
			}			
		}
		if ( $scope.userInfo.other3 != null && $scope.userInfo.other3 != 0 ) {
			for ( var i = index; i < $scope.languageList.length; i++ ) {
				if ( $scope.languageList[i].id == $scope.userInfo.other3 ) {
					$scope.languageList.splice( i, 1 );
					break;
				}
			}			
		}
		if ( $scope.userInfo.learning1 != null && $scope.userInfo.learning1 != 0 ) {
			for ( var i = index; i < $scope.languageList.length; i++ ) {
				if ( $scope.languageList[i].id == $scope.userInfo.learning1 ) {
					$scope.languageList.splice( i, 1 );
					break;
				}
			}			
		}
		if ( $scope.userInfo.learning2 != null && $scope.userInfo.learning2 != 0 ) {
			for ( var i = index; i < $scope.languageList.length; i++ ) {
				if ( $scope.languageList[i].id == $scope.userInfo.learning2 ) {
					$scope.languageList.splice( i, 1 );
					break;
				}
			}			
		}
		if ( $scope.userInfo.learning3 != null && $scope.userInfo.learning3 != 0 ) {
			for ( var i = index; i < $scope.languageList.length; i++ ) {
				if ( $scope.languageList[i].id == $scope.userInfo.learning3 ) {
					$scope.languageList.splice( i, 1 );
					break;
				}
			}			
		}
	}
	
	// 언어변경
	$scope.changeLanguage = function( language ) {
		console.log( language );
		if ( language == -1 ) {
			return;
		}
		$http.post( '/tellpinsearch/settings/change/language/',
				{ 'language_id' : language })
				.success( function( result ) {
					console.log( result );
					$scope.selectRestoreCategory = [];
					$scope.selectRestoreSite = [];
					$scope.noDictList = [];
					$scope.dictList = [];
					$scope.siteList = [];
					$scope.siteList = result.toolData.site;
					for( var i = 0; i < result.toolData.category.length; i++ ) {
						if ( result.toolData.category[i].category_id > 16 && result.toolData.category[i].category_id < 10001  ) {
							$scope.dictList.push( result.toolData.category[i] );
						}
						else {
							$scope.noDictList.push( result.toolData.category[i] );
						}
					}
					if ( $scope.dictList.length == 0 ) {
						$scope.selectCategory = $scope.noDictList[0].category_id;
					}
					else {
						$scope.selectCategory = $scope.dictList[0].category_id;
					}
				});
	}
	
	// 카테고리에 해당하는 사이트 목록
	$scope.getSiteList = function( category ) {
		console.log( category );
		$scope.selectCategory = category;
		$http.post( '/tellpinsearch/settings/change/category/',
				{ 'category_id' : category,
			      'lang_id' : $scope.selectLanguage } )
				.success( function( result ) {
					$scope.siteList = [];
					$scope.siteList = result.site;
				});
	}
	
	$scope.customChangeCategory = function( category ) {
		console.log( category );
		for ( var i = 0; i < $scope.categoryList.length; i++ ) {
			if ( $scope.categoryList[i].category_id == category ) {
				$scope.selectMyCategoryName = $scope.categoryList[i].category_name;
				break;
			}
		}
	}
	
	$scope.openModal = function( id ) {
		$scope.customCategoryName = '';
		for ( var i = 0; i < $scope.languageList.length; i++ ) {
			if ( $scope.languageList[i].id == $scope.selectLanguage ) {
				$scope.selectLanguageName = $scope.languageList[i].name;
				break;
			}
		}
		if ( id == 'siteModal' ) {
			$scope.customSiteName = '';
			$scope.selectMyCategoryName = '';
			$scope.selectMyCategory = $scope.categoryList[0].category_id;
			$scope.customSiteUrl = '';
			for ( var i = 0; i < $scope.categoryList.length; i++ ) {
				if ( $scope.categoryList[i].category_id == $scope.selectMyCategory ) {
					$scope.selectMyCategoryName = $scope.categoryList[i].category_name;
					break;
				}
			}
			angular.element( '#' + id ).modal();
			$timeout( function() {
				$( '#customSite' ).focus();
			}, 470);
		}
		else if ( id == 'categoryModal' ) {
			angular.element( '#' + id ).modal();
			$timeout( function() {
				$( '#customCategory' ).focus();
			}, 460);
		}
		else if ( id == 'categoryRestoreModal' ) {
			$http.post( '/tellpinsearch/settings/restorelist/category/',
					{ 'lang_id' : $scope.selectLanguage })
					.success( function( result ) {
						console.log( result );
						if ( result.restoreCategory ) {
							$scope.restoreCategoryList = result.restoreCategory;
							angular.element( '#' + id ).modal();
						}
					});
		}
		else if ( id == 'siteRestoreModal' ) {
			for ( var i = 0; i < $scope.categoryList.length; i++ ) {
				if ( $scope.categoryList[i].category_id == $scope.selectCategory ) {
					$scope.selectRestoreCategoryName = $scope.categoryList[i].category_name;
					break;
				}
			}
			$http.post( '/tellpinsearch/settings/restorelist/site/',
					{ 'lang_id' : $scope.selectLanguage,
				      'category_id' : $scope.selectCategory })
				  .success( function( result ) {
					  console.log( result );
					  if ( result.restoreSite ) {
						$scope.restoreSiteList = result.restoreSite;  
					  }
			});
			angular.element( '#' + id ).modal();
		}
		else if ( id == 'resetModal' ) {
			angular.element( '#' + id ).modal();
		}
	}
	
	$scope.hideModal = function( id ) {
		angular.element( '#' + id ).modal( 'hide' );
	}
	
	$scope.addCategory = function() {
		console.log( $scope.myCategory );
		var category = {};
		category.category_id = --$scope.customCategoryID;
		category.category_name = $scope.customCategoryName;
		category.lang_id = $scope.selectLanguage;
		category.type = 1;
		category.sitelist = [];
		console.log( category );
		if ( !$scope.customCategoryName || $scope.customCategoryName == '' ) {
			alert( 'Input category name' );
			$( '#customCategory' ).focus();
			return;
		}
		else {
			//$scope.myCategory.push( category );
			$http.post( '/tellpinsearch/settings/new/category/',
					{ 'lang_id' : $scope.selectLanguage,
				      'category' : category } )
					.success( function( result ) {
						console.log( result );
						$scope.setCategory( result );
					});
		}
		$scope.hideModal( 'categoryModal' );
	}
	
	$scope.removeCategory = function( obj ) {
		console.log( obj );
		var category = {};
		category.category_id = obj.category_id;
		category.category_name = obj.category_name;
		category.lang_id = $scope.selectLanguage;
		category.type = 2;
		console.log( category );
		$http.post( '/tellpinsearch/settings/remove/category/',
				{ 'lang_id' : $scope.selectLanguage,
			      'category' : category })
				.success( function( result ) {
					if ( result.toolData ) {
						$scope.setCategory( result );
					}
				});
	}
	
	$scope.addSite = function() {
		var site = {};
		if ( !$scope.customSiteUrl || $scope.customSiteUrl.indexOf( 'TEST' ) < 0  ) {
			alert( 'The brower\'s address was not valid.\nPlease try again.' );
		}
		else {
			var siteUrlCheck = /https?:\/\/(?:www\.)?([-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b)*(\/[\/\d\w\.-]*)*(?:[\?])*(.+)*/gi;
			site.lang_id = $scope.selectLanguage;
			site.category_id = $scope.selectMyCategory;
			site.site_id = --$scope.customSiteID;
			site.site_name = $scope.customSiteName;
			site.search_url = $scope.customSiteUrl.replace( 'TEST', '@^tskw^@' );
			siteArr = siteUrlCheck.exec( $scope.customSiteUrl );
			console.log( siteArr );
			if ( !siteArr ) {
				alert( 'The brower\'s address was not valid.\nPlease try again.' );
				return;
			}
			site.site_url = siteArr[1];
			site.type = 1;
			console.log( site );
			
			$http.post( '/tellpinsearch/settings/new/site/',
					{ 'site' : site,
				      'lang_id' : $scope.selectLanguage } )
					.success( function( result ) {
						if ( result.site ) {
							$scope.siteList = [];
							$scope.siteList = result.site;
							$scope.hideModal( 'siteModal' );
						}
					});
		}
	}
	
	$scope.removeSite = function( obj ) {
		console.log( obj );
		$http.post( '/tellpinsearch/settings/remove/site/',
				{ 'site' : obj,
			      'lang_id' : $scope.selectLanguage })
				.success( function( result ) {
					if ( result.site ) {
						$scope.siteList = [];
						$scope.siteList = result.site;
					}
				});
	}
	
	$scope.restoreCategory = function() {
		console.log( $scope.selectRestoreCategory );
		$http.post( 'tellpinsearch/settings/restore/category/',
				{ 'category' : $scope.selectRestoreCategory,
				  'lang_id' : $scope.selectLanguage })
		.success( function( result ) {
			if ( result.toolData ) {
				$scope.setCategory( result );
				$scope.hideModal( 'categoryRestoreModal' );
			}
		});
	}
	
	$scope.insertRestoreCategory = function( category_id ) {
		var idx = $scope.selectRestoreCategory.indexOf( category_id );
		if ( idx == -1 ) {
			$scope.selectRestoreCategory.push( category_id );
		}
		else {
			$scope.selectRestoreCategory.splice( idx, 1 );			
		}
	}
	
	$scope.insertRestoreSite = function( site_id ) {
		var idx = $scope.selectRestoreSite.indexOf( site_id );
		if ( idx == -1 ) {
			$scope.selectRestoreSite.push( site_id );
		}
		else {
			$scope.selectRestoreSite.splice( idx, 1 );			
		}
	}
	
	$scope.restoreSite = function() {
		console.log( $scope.selectRestoreSite );
		$http.post( 'tellpinsearch/settings/restore/site/',
				{ 'category_id' : $scope.selectCategory,
				  'lang_id' : $scope.selectLanguage,
				  'site' : $scope.selectRestoreSite })
		.success( function( result ) {
			if ( result.site ) {
				$scope.siteList = [];
				$scope.siteList = result.site;
				$scope.hideModal( 'siteRestoreModal' );
			}
		});
	}
	
	$scope.resetLanguage = function() {
		$http.post( 'tellpinsearch/settings/reset/',
				{ 'lang_id' : $scope.selectLanguage })
		.success( function( result ) {
			if ( result.toolData ) {
				$scope.setCategory( result );
				$scope.hideModal( 'resetModal' );
			}
		});
	}
	
}]);

function InpuOnlyNumber(obj){
    if (event.keyCode >= 48 && event.keyCode <= 57) { //숫자키만 입력
    	$('.invalid').css('display', 'none');
        return true;
    } else {
    	//$('.invalid').css('display', 'inline');
        event.returnValue = false;
    }
}


