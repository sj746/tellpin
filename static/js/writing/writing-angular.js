// writing AngularJS 
var mainApp = angular.module('writtingApp', [ "ngFileUpload", "angular-sortable-view" ]);
mainApp.config(function($interpolateProvider, $locationProvider, $httpProvider) {
	$locationProvider.html5Mode(true);
	$interpolateProvider.startSymbol('[[');
	$interpolateProvider.endSymbol(']]');

});

mainApp.directive('myRepeatDirective', function($timeout) {
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {
			// console.log(attrs);
			// angular.element(element).css('background', 'blue');

			// console.log($(element).attr('id'));
			$timeout(function() {
				scope.$emit('ngRepeatFinished');
				// console.log($(element).attr('id'));
				// console.log(scope.currentId);
				if (scope.iptType == "paragraph") {
					scope.setTextBlock("editor" + scope.currentId);
				}
				if ( scope.iptType == "video" ) {
						scope.uploadVideo( scope.currentId);
				}
				if(scope.iptType == "material") {
					$('#link-url-Modal').modal('show');
				}
				
				if ( scope.iptType == "image" ) {
					scope.uploadImage( scope.currentId );
				}
			});

		}
	}
});


// writingApp ctrl
mainApp.controller('WritingFormCtrl', function($scope, $http, $location, Upload, $timeout ) {

//###############################################################################	
//new editor 
	
	$scope.textbook = {
			paragraphList : [],
			bChange : false
	}
	
	//component menu button 활성화
	$scope.btnAction = function($event) {
		console.log($event);
		if($($event.target).parent().find(".se_wrap_addButton").hasClass("menubtn_active")) {
			$($event.target).parent().find(".se_wrap_addButton").addClass("menubtn_no_active");
			$($event.target).parent().find(".se_wrap_addButton").removeClass("menubtn_active");
    	} else {
    		$(".se_wrap_addButton").removeClass("menubtn_active");
    		$(".se_wrap_addButton").addClass("menubtn_no_active");
    		$($event.target).parent().find(".se_wrap_addButton").addClass("menubtn_active");
    		$($event.target).parent().find(".se_wrap_addButton").removeClass("menubtn_no_active");
    	}
	}
	
	//component menu button 클릭
	$scope.clickcr = function($event, idx) {
		$scope.iptType = $event.target.dataset.role;
		
		//새 블럭 추가
		$scope.currentId = $scope.addNewBlock(idx);
		
		//버튼 비활성화
		$($event.target).parent().removeClass("menubtn_active");
		$($event.target).parent().addClass("menubtn_no_active");
		console.log($scope.currentId);
		console.log( $scope.textbook.paragraphList );
		
		$scope.textbook.paragraphList[$scope.currentId].type = $scope.iptType;
		
		if($scope.iptType == "paragraph") {
			//$scope.setTextBlock("editor" + id);
		}
		
	}
	
	//새 block 추가 
	$scope.addNewBlock = function(idx) {
		//id 생성
		var newID = generateID();
		
		//Block
		var block = {
				id : newID,
				type : null, 
				content : null
		};
		
		//paragraph list 추가
		//$scope.textbook.paragraphList.push(block);
		console.log(idx);
		if(idx < 0) {
			$scope.textbook.paragraphList.unshift(block);
		} else {
			$scope.textbook.paragraphList.splice(idx+1, 0, block);
		}
		
		//return newID;
		return ++idx;
	}
	
	//block 제거 
	$scope.deleteBlock = function(elem, idx) {
		$scope.textbook.paragraphList.splice(idx, 1);
	}
	$scope.deleteBlockById = function(id) {
		for(var i = 0; i < $scope.textbook.paragraphList.length; i++) {
			if($scope.textbook.paragraphList[i].id == id) {
				$scope.textbook.paragraphList.splice(i, 1);
				return;
			}
		}
	}
	
	$("#link-url-Modal").on('hide.bs.modal', function() {
		if(!$scope.textbook.bChange) {
			$scope.deleteBlockById($scope.currentId);
			$scope.$apply();
		}
		$scope.textbook.bChange = false;
	});
	
	
	//block TinyMce 세팅
	$scope.setTextBlock = function(id) {
		//tinymce 적용
		$("#" + id).addClass("editable");
		// tinymce setup
		tinymce.init({
			selector : "#" +id,
			forced_root_block : 'p',
			inline : true,
			plugins : [ "autolink anchor" ],
			link_title : false,
			menubar : true, 
			// toolbar: "|
			// myimage",data:text/mce-internal,%3Cimg%20src%3D%22../../static/img/landingpage/logo.png%22%20alt%3D%22%22%20/%3E
			toolbar : false,
			resize : true,
			object_resizing : false,
			media_filter_html : false,
			// force_br_newlines : true,
			setup : function(editor) {
				console.log('tinymce init setup');
				editor.addButton('myimage', {
					icon : "mce-ico mce-i-image",
					onclick : function() {
						$("#img-browser").click();
					}
				});
				editor.addButton('myvideo', {
					icon : "mce-ico mce-i-mce-ico mce-i-media",
					onclick : function() {
						$("#embedModal > .modal-dialog").css("top", "380px");
						$("#embedModal").modal();
					}
				});
				
			}
		});
	}
	
	$scope.videoUrlSource; // 사용자 링크할 동영상 주소
	$scope.videoBlock;	// video div 영역
	$scope.imageBlock; // image div 영역
	$scope.thumbnailPath;
	$scope.isThumbnail = false;
	
	$scope.uploadImage = function( blockId ) {
		$scope.imageBlock = blockId;
		$( "#img-upload-modal" ).modal();
	}
	
	$scope.uploadVideo = function( blockId ) {
		$scope.videoBlock = blockId;
		$( "#video-upload-modal" ).modal();
	}
	
	// url 업로드
	$scope.urlUpload = function() {
		var src = $scope.videoUrlSource;
		var patt = new RegExp(
		'^(https?):\\/\\/([^:\\/\\s]+)(:([^\\/]*))?((\\/[^\\s/\\/]+)*)?\\/([^#\\s\\?]*)(\\?([^#\\s]*))?(#(\\w*))?$');
		var result = ( src.match( patt ) );
		var player;
		var thumbnail;
		
		if (result) {
			var v_id;
			if (result[2] == "youtu.be") { // youtube
				player = "<iframe width='100%' height='394px' src='https://www.youtube.com/embed/"
						+ result[7]
						+ "?rel=0' frameborder='0' allowfullscreen></iframe>";
				//thumbnail = "<img width='100%' height='394px' src='http://img.youtube.com/vi/" + result[7]
				//		+ "/0.jpg' />";
				// player += "<div id='player'> <div>";
				thumbnail = "http://img.youtube.com/vi/" + result[7];

			} else if (result[2] == "tvpot.daum.net") { // 다음
				player = "<iframe width='100%' height='394px' src='http://videofarm.daum.net/controller/video/viewer/Video.html?vid="
						+ result[7]
						+ "&play_loc=undefined&alert=true' frameborder='0' scrolling='no' ></iframe>";
				//thumbnail = "<img width='100%' height='394px' src=https://t1.daumcdn.net/tvpot/thumb/"
				//		+ result[7] + "/thumb.png />";
				thumbnail = "https://t1.daumcdn.net/tvpot/thumb/" + result[7] + "/thumb.png";
			} else if (result[2] == "www.viki.com") { // viki
				v_id = result[7].split('-');

				// VIKI appID : '65535a' // 개발자 사이트에 appID
				var rs;
				$.ajax({
					url : "http://api.viki.io/v4/videos/" + v_id[0]
							+ ".json?app=65535a",
					async : false,
					method : 'get',
					dataType : 'json',
					success : function(data) {
						rs = data;
						// console.debug(rs);
						rs = data.images.poster.url;
					}
				});
				console.debug(rs);

				player = "<iframe src='http://www.viki.com/player/"
						+ v_id[0]
						+ "' width='100%' height='394px' frameBorder='0'></iframe>";
				//thumbnail = "<img width='100%' height='394px' src='" + rs + "' />";
				thumbnail = rs
			} else if (result[2] == "vimeo.com") { // vimeo
				var url = "https://vimeo.com/api/oembed.json?url="+source;
						//+ result[7];
				var rs = $.ajax({
					url : url,
					async : false,
					dataType : 'json',
					success : function(data) {
						console.debug(data);
						//return data.thumbnail_url;
					}
				});

				//console.debug(rs.responseJSON.thumbnail_url);
				var thumbnail_url = rs.responseJSON.thumbnail_url;
				player = rs.responseJSON.html;
					    // "<iframe src='https://player.vimeo.com/video/"
						//+ result[7]
						//+ "?color=91010b&title=0&byline=0&portrait=0&badge=0' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
				thumbnail = thumbnail_url;
						
			} else if (result[2] == "v.youku.com") { // youku
				// console.debug(result[7]);
				var _id = result[7].split('_');
				v_id = _id[1].split('.');
				// console.debug(v_id);
				player = "<iframe height=394 width=100% src='http://player.youku.com/embed/"
						+ v_id[0] + "' frameborder=0 allowfullscreen></iframe>";
				
				//thumbnail = '<img width="100%" height="394px" src="http://events.youku.com/global/api/video-thumb.php?vid=" + v_id[0] alt="" />'
				thumbnail = "http://events.youku.com/global/api/video-thumb.php?vid=" + v_id[0];
			}

		} else {
			player = src;
		}
		
		console.log( src );
		console.log( $scope.videoBlock );
		
		//angular.element( "#" + $scope.videoBlock ).append( player );
		//angular.element( "#" + $scope.videoBlock ).append( thumbnail );
		$scope.textbook.paragraphList[$scope.videoBlock].content = thumbnail;
	
		$( "#video-upload-modal" ).modal( "hide" );
		$( "#video-url-Modal" ).modal( "hide" );
	}
	// image file upload
	$scope.uploadImages = function( file, errFiles ) {
		$scope.u_image = file;
		$scope.image_errFile = errFiles && errFiles[0];
		
		if ( file ) {
			$( "#imageProgressModal" ).modal();
			file.upload = Upload.upload({
				url: "/writingform/imageupload/",
				data: { file : file }
			});
			
			file.upload.then( function( response ) {
				$timeout( function() {
					file.result = response.data;
				});
			}, function( response ) {
				if ( response.status > 0 )
					$scope.errorMsg = response.status + ": " + response.data;
			}, function( evt ) {
				file.progress = Math.min( 100, parseInt( 100.0 * evt.loaded / evt.total ));
			});
		}
	}
	
	$scope.successImage = function() {
		//var elem = '<div style="width:300px; height:300px; background:url('+ $scope.u_image.result +');" ></div>';
		//var elem = '<img src="'+ $scope.u_image.result +'" draggable=false>';
		
		$( elem ).off( "dragstart" );
		
		//angular.element( "#" + $scope.imageBlock ).append( elem );
		$scope.textbook.paragraphList[$scope.imageBlock].content = $scope.u_image.result;
		
		$( "#imageProgressModal" ).modal( "hide" );
		$( "#image-url-Modal" ).modal( "hide" );
		$( "#img-upload-modal" ).modal( "hide" );
	}
	
	// insert image url
	 $scope.addImageUrl = function() {
		var source = $("#embed-input").val();
		
		var elem = '<img src="'+ source +'"></img>';
		angular.element( "#" + $scope.imageBlock ).append( elem );
		
		$("#embedModal").modal('hide');
		$('#img-upload-modal').modal('hide');

	}
	
	// 동영상 파일 업로드
	$scope.uploadFiles = function( file, errFiles ) {
		$scope.f = file;
		$scope.errFile = errFiles && errFiles[0];
		
		console.log( file );
		
		$( "#videoProgressModal" ).modal();
		if ( file ) {
			file.upload = Upload.upload({
				url: "/writingform/videoupload/",
				data: { file : file }
			});
			
			file.upload.then( function ( response ) {
				$timeout( function () {
					file.result = response.data;
					console.log( response.data );
					$scope.makeThumbnail( response.data );
				});
			}, function ( response ) {
				if ( response.status > 0 ) {
					$scope.errorMsg = response.status + ": " + response.data;
				}
			}, function ( evt ) {
				file.progress = Math.min( 100, parseInt( 100.0 * evt.loaded / evt.total ));
			});
			
		}
	}
	
	// 썸네일 이미지 만들기
	$scope.makeThumbnail = function( src ) {
		console.log("thumbnail : " + src );
		$http({
			url: "/writingform/makethumbnail/",
			method: "POST",
			data: { src : src }
		})
		.success( function( result ) {
			$scope.thumbnailPath = result;
			$scope.isThumbnail = true;
		});
	}
	
	$scope.successVideo = function() {
		//var elem = '<img src="'+ $scope.f.result +'"></img>';
		//var elem = '<video controls src="'+ $scope.f.result +'"></video>';
		//angular.element( "#" + $scope.videoBlock ).append( elem );
		$scope.textbook.paragraphList[$scope.videoBlock].content = $scope.thumbnailPath;
		
		$( "#videoProgressModal" ).modal( "hide" );
		$( "#video-upload-modal" ).modal( "hide" );
		$( "#video-url-Modal" ).modal( "hide" );
	}
	
	// insert link-url
	 $scope.insertLinkUrl = function () {
		// console.debug($('#url-preview').html());
		if ($('#invalid-url').length > 0) {
			$('#url-preview').empty();
			$('#link-url-Modal').modal('hide');
			$scope.deleteBlockById($scope.currentId);
			return;
		}
		console.log(angular.element($scope.UrlPreview));
		$("#editor" + $scope.currentId).append($scope.UrlPreview);
		$scope.textbook.bChange = true;
		$('#link-url-Modal').modal('hide');
	}
	 
	// add link url
	 $scope.linkUrl;
	 $scope.addLinkUrl = function () {
	 	var source = $("#link-url-input").val();
	 	$('#url-preview').empty();
	 	var loading = document.createElement("img");
	 	loading.setAttribute("style","text-align:center; height:136px; margin-top:22px;")
	 	loading.src = "http://static.se2.naver.com/static/full/20141105/ico_loading.gif";
	 	$('#url-preview').append(loading);
	 	var data = {'url' : source};
	 	$http.post("/writingform/urlpreview/", data).success(function(response) {
	 		if (response != "invalid") {
				//var data = JSON.parse(result);
				$('#url-preview').empty();

				// To-Do
				// 결과물을 보기 좋게 뿌려보자

				var link = document.createElement('div');
				link.setAttribute('style','display: inline-block; cursor: pointer; width:100%;');
				link.setAttribute('contenteditable', false);
				link.setAttribute('href', response['url']);
				link.onclick = function() {
					location.href = response['url'];
				};
				$scope.linkUrl = response['url'];
				var basic_info = document.createElement('div');
				basic_info.setAttribute('style','float:right; width:80%;');
				var tumnail = document.createElement('div');
				tumnail.setAttribute('style', 'float:left; width:20%;');
				var title = document.createElement("h2");
				title.innerHTML = response['title'];
				title.setAttribute('style','float:right; text-align:left; padding: 5px; width:100%; overflow:hidden; text-overflow:ellipsis; white-space:nowrap;');
				var description = document.createElement('h3');
				if (response['description']) {
					description.innerHTML = response['description'];
				}
				description.setAttribute('style','float:right; text-align:left; padding: 5px; width:100%; overflow:hidden; text-overflow:ellipsis; white-space:nowrap;');
				var img = document.createElement("img");
				img.src = response['url'] + response['image'];
				img.setAttribute('style', 'width: 100%; height:100%');
				var url_source = document.createElement("p");
				url_source.innerHTML = response['url'];
				url_source.setAttribute('style','float:right; width:50%; overflow:hidden; text-overflow:ellipsis; white-space:nowrap;');

				basic_info.appendChild(title);
				basic_info.appendChild(description);
				tumnail.appendChild(img);

				link.appendChild(basic_info);
				link.appendChild(tumnail);
				link.appendChild(url_source);
				$('#url-preview').append(link);
				
				$scope.UrlPreview = $('#url-preview').clone().attr("id", "url_preview" + $scope.currentId); 

			} else {
				$('#url-preview').html('<div id="invalid-url" class="alert alert-danger" role="alert">invalid url</div>');
			}
	 	})
	 	.error(function(response) {
	 		alert(response);
	 	});
	 }

	
	
	

	
	
//###############################################################################	
	
	//전송할 데이터
	$scope.data = {
			'clubs_index': [],
			'clubs_id' : [],
			'notecast' : null,
			'language_id' : null,
			'title' : null,
			'content' : null,
	};
	
	//default 
	var defalutClub = "공유할 Club 선택";
	$scope.selectedClub = defalutClub;
	
	
	$scope.clubs = [];  //출력되는 club 리스트 
	$http.post("/writingform/clubs/").success(function(data) {
		$scope.clubs = data.clubs;
//		for(var i = 0; i < $scope.clubs.length; i++) {
//			console.debug($scope.clubs[i])
//		}
	});
	
	//클럽 선택
	//선택한 클럽을 데이터에 넣는다.
	$scope.choiceClub = function($index) {
		if(!checkDuplicationList($index, $scope.data.clubs_index)) {
			$scope.data.clubs_index.push($index);
			//console.debug($(".list-club").eq($index));
			$(".list-club").eq($index).find(".added-check").addClass("active");
		} else {
			$scope.data.clubs_index.splice($scope.data.clubs_index.indexOf($index), 1);
			$(".list-club").eq($index).find(".added-check").removeClass("active");
		}
		var selected = "";
		for(var i = 0; i < $scope.data.clubs_index.length; i++) {
			if(i == $scope.data.clubs_index.length - 1) {
				selected += $scope.clubs[$scope.data.clubs_index[i]].comment;
			} else {
				selected += $scope.clubs[$scope.data.clubs_index[i]].comment + " / ";
			}
			 
		}
		if(selected == "") {
			selected = defalutClub;
		}
		$scope.selectedClub = selected;
		
	};
	
	
	//default notecast
	var defaultNotecast = "Notecast 선택";
	$scope.selectedNotecast = defaultNotecast;
	
	$scope.notecasts = [];  //출력되는 노트캐스트 리스트
	$http.post("/writingform/notecasts/").success(function(data) {
		$scope.notecasts = data.notecasts;
//		for(var i = 0; i < $scope.notecasts.length; i++) {
//			console.debug($scope.notecasts[i])
//		}
	});
	
	$scope.choiceNotecast = function($index) {
		$scope.data.notecast = $scope.notecasts[$index];
		$(".list-notecast").each(function(idx) {
			$(".list-notecast").eq(idx).find(".added-check").removeClass("active");
		});
		
		if($scope.data.notecast) {
			$(".list-notecast").eq($index).find(".added-check").addClass("active");
		} 
		console.debug($scope.data.notecast);
		$scope.selectedNotecast = $scope.data.notecast.title;
	};

	//language
	$scope.languages = {
			selectedOption: null,
			availableOptions: [],
			defaultvalue: null,
	};
	
	$http.post("/writingform/getlanguage/").success(function(data) {
		$scope.languages.availableOptions = data.languages;
//		console.debug(data.languages);
		$scope.languages.defaultvalue = data.languages[0].id;
		$scope.languages.selectedOption = $scope.languages.defaultvalue;
	});
	
	
	$scope.writingform_submit = function() {
		//console.debug($scope.writing_title);
		$scope.writing_content = $('#editor-container').html();
		//console.debug($scope.writing_content);
		
		// club, notecast, language 정보를 담는다
		// TO-DO
		var list = [];
		for(var i = 0; i < $scope.data.clubs_index.length; i++) {
			if(!checkDuplicationList($scope.clubs[$scope.data.clubs_index[i]].id, $scope.data.clubs_id)) {
				list.push($scope.clubs[$scope.data.clubs_index[i]].id);
			}
			
		}
		$scope.data.clubs_id = list;
		console.debug($scope.data.clubs_index);
		console.debug($scope.data.clubs_id);
		
		
		if($scope.selectedNotecast == defaultNotecast) {
			//alert("Notecast를 선택해주세요");
			//return;
		}
		
		//console.debug($scope.languages.selectedOption);
		
		
		// 먼저 리소스를 편집하기 위해 추가했던 element 혹은 class를 모두 제거한다.
		$('.media-button').remove();

		// class로 정의했던 스타일은 inline style로 다시 새겨준다.
		if ($('.video-container').length) {
			console.debug("video class working");
			$('.video-container')
					.each(
							function(index) {
								$(this)
										.attr(
												"style",
												"position: relative; padding-bottom: 56.25%; padding-top: 25px; width: 100%; height: auto; overflow: hidden;");
								if ($(this).children("iframe").length) {
									$(this)
											.children("iframe")
											.attr("style",
													"position: absolute; top: 0; left: 0; width: 96%; height: 100%;");
								} else if ($(this).children("object")) {
									$(this)
											.children("object")
											.attr("style",
													"position: absolute; top: 0; left: 0; width: 96%; height: 100%;");
								} else if ($(this).children("embed")) {
									$(this)
											.children("embed")
											.attr("style",
													"position: absolute; top: 0; left: 0; width: 96%; height: 100%;");
								}
								$(this).removeClass('media-holder');
								$(this).removeClass('video-container');
							});

		} else if ($('.image-container').length) {
			console.debug("image class working");
			$('.image-container')
					.each(
							function(index) {
								$(this)
										.attr(
												"style",
												"position: relative; overflow: hidden; vertical-align: middle; text-align: center; max-width: 100%; max-height: auto;");
								$(this).removeClass('media-holder');
								$(this).removeClass('image-container');
							});

		} else {
			console.debug("nothing class");
		}

		// 남아있는 편집공간 내 html을 그대로 content로 전송한다.
		//var title = $('#textarea').val();
		$scope.data.content = $('#editor-container').html();

		//console.debug(content);
		
		console.debug($scope.data.title);
		console.debug($scope.data.content);
		$scope.writing_content = $('#editor-container').html();
		
		
		
		//submit
		$http.post("/writingform/post_write/", {post_note : $scope.data}).success(function(response){
			if(response) {
				console.debug(response);
				var note_id = response;
				window.location.replace("/post/"+note_id+"/");
				//goBack();
			}
		});
		
	};
	
	$scope.testfunction = function() {
		console.debug("test");
	};
	
});

//###############################################################################
function checkDuplicationList(item, List) {
	var isTrue = false;
	for (var i = 0; i < List.length; i++) {
		if (item == List[i]) {
			isTrue = true;
		}
	}
	return isTrue;
}

//ID generator
function generateID() {
	// Math.random should be unique because of its seeding algorithm.
	// Convert it to base 36 (numbers + letters), and grab the first 9
	// characters
	// after the decimal.
	return '_' + Math.random().toString(36).substr(2, 9);
};

