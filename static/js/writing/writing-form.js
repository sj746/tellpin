//
//
//  writing-form.js
//  tinymce, medium-editor
//
//ID generator
function generateID() {
	// Math.random should be unique because of its seeding algorithm.
	// Convert it to base 36 (numbers + letters), and grab the first 9
	// characters
	// after the decimal.
	return '_' + Math.random().toString(36).substr(2, 9);
};

// mouse wheel event
if (window.addEventListener)
	window.addEventListener('DOMMouseScroll', wheel, false);
window.onmousewheel = document.onmousewheel = wheel;

function wheel(event) {
	$('#sidemenu').css('display', "none");

}

// inserted image, video
var TestList = new Array();

// refresh
$(window).on('beforeunload', function(e) {
	var agent = navigator.userAgent.toLowerCase();

	for (var i = 0; i < TestList.length; i++) {
		$.ajax({
			url : '/writingform/delete_image/',
			data : {
				'image' : TestList[i]
			},
			type : 'POST',
			success : function() {
				// nothing
			}
		})

	}

});

// $(window).on('unload', function(e) {
// $.ajax({
// url : '/writingform/delete_image/',
// success : function() {
// alert("x");
// }
// });
// });

// html element load
$(document)
		.ready(
				function() {

					// textarea auto height
					/*$('.wrap').on('keyup', 'textarea', function(e) {
						$(this).css('height', 'auto');
						$(this).height(this.scrollHeight - 30);
					});*/
					$('.wrap').find('textarea').keyup();

					// category-list-footer
					$('#footer-notecast-wrap').focusout(function(e) {
						$("#plusnotecast").css('display', 'none');
						$("#out").css('display', '');
					});
					$('#footer-notecast-wrap').mouseover(function(e) {
						$("#plusnotecast").css('display', 'block');
						$("#plusnotecast").focus();
						$("#out").css('display', 'none');
						$("#plusnotecast").val("");
					});

					// popover
					$('[data-toggle="popover"]')
							.popover(
									{
										content : "<table>"
												+ "<thead><tr><th width=50%></th><th width=50%></th></tr></thead>"
												+ "<tr><td width=50%>사용자정의URL: </td><td><input type='text' placeholder='/POST/123/' style='float:right; height:30px; border-color: transparent; backgroud-color: transparent;'></td></tr>"
												+ "<tr><td width=50%>노트 출처: </td><td><input type='text' placeholder='http://' style='float:right; height:30px; border-color: transparent; backgroud-color: transparent;' ></td></tr>"
												+ "</table>",
										html : true,
										placement : "bottom"
									});

					// dropzone
					var dropzone = document.getElementById('upload-form');

					var upload = function(files) {
						var formData = new FormData(), xhr = new XMLHttpRequest(), x;

						for (x = 0; x < files.length; x = x + 1) {
							// console.debug(files[x].name);
							formData.append('image', files[x]);
						}

						xhr.onload = function() {
							var data = this.responseText;
							// console.debug(data);
							tinymce.get("editor-container").execCommand(
									'mceInsertContent',
									false,
									"<img src='" + data
											+ "' style='max-width:100%'/>");

							$('#img-upload-modal').modal('hide');
						};

						xhr.open('post', '/writingform/upload/');
						xhr.send(formData);
					};

					dropzone.ondrop = function(e) {
						e.preventDefault();
						upload(e.dataTransfer.files);
					}

					dropzone.ondragover = function() {
						return false;
					}
					dropzone.ondragleave = function() {
						return false;
					}

				});

// tinymce setup
//tinymce.init({
//	selector : "#editor-container",
//	forced_root_block : 'p',
//	inline : true,
//	plugins : [ "autolink anchor" ],
//	link_title : false,
//	menubar : true, 
//	// toolbar: "|
//	// myimage",data:text/mce-internal,%3Cimg%20src%3D%22../../static/img/landingpage/logo.png%22%20alt%3D%22%22%20/%3E
//	toolbar : false,
//	resize : true,
//	object_resizing : false,
//	media_filter_html : false,
//	// force_br_newlines : true,
//	setup : function(editor) {
//		editor.addButton('myimage', {
//			icon : "mce-ico mce-i-image",
//			onclick : function() {
//				$("#img-browser").click();
//			}
//		});
//		editor.addButton('myvideo', {
//			icon : "mce-ico mce-i-mce-ico mce-i-media",
//			onclick : function() {
//				$("#embedModal > .modal-dialog").css("top", "380px");
//				$("#embedModal").modal();
//			}
//		});
//		editor.on('focus', function(e) {
//			resourceMenuActivation();
//		});
//		editor.on('change', function(e) {
//			//메뉴 버튼 활성화
//			resourceMenuActivation();
//			//포스트 버튼 활성화
//			postButtonActivation();
//			
//		});
//		editor.on('click', function(e) {
//			//메뉴 버튼 활성화
//			resourceMenuActivation();
//		});
//
//		editor.on('focusout', function(e) {
//
//		});
//		editor.on('keyup', function(e) {
//			resourceMenuActivation();
//			//포스트 버튼 활성화
//			postButtonActivation();
//		});
//
//	}
//
//});
// resource menu activation 
function resourceMenuActivation() {
	var obj = $(tinymce.get("editor-container").selection.getNode()).offset();
	$('#sidemenu').css('left', obj.left - 70 + "px");
	$('#sidemenu').css('top', obj.top - 10 - $(document).scrollTop() + "px");
	$('#sidemenu').css('display', "block");
}
// post activation
function postButtonActivation() {
	// post 버튼 활성화
	var title = $('textarea').val();
	if (tinymce.get("editor-container")) {
		var content = tinymce.get("editor-container").getContent();
		if (content.match(/^\S.*/g) && title.match(/^\S.*/g)) {
			$(".submit-writing-btn").removeAttr("disabled");
		} else {
			$(".submit-writing-btn").attr("disabled", "disabled");
		}
	} else {
		console.debug("tinymce init...");
	}
}

// resource move
function dragResource(event) {
	event.dataTransfer.setData("text", event.target.id);
}
function dropResource(event) {
	// console.debug(event.currentTarget);
	console.debug(event.target);
	var id = event.dataTransfer.getData("text");
	console.debug(id);
	var elm = document.getElementById(id);
	event.target.appendChild(elm);
	event.preventDefault();

}
function DragOver(event) {
	event.preventDefault();
}

function makeResourceDiv(type, source) {
	// create random id
	var id = generateID();

	if (type == 'img') {

		// url 주소에서 도메인만 출처로 남길 수 있도록 코드 작성
		// var patt = /(http(s)?:\/\/)?\w+(\.\w+)+/gi;
		var domain = "";
		var patt = new RegExp(
				'^(https?):\\/\\/([^:\\/\\s]+)(:([^\\/]*))?((\\/[^\\s/\\/]+)*)?\\/([^#\\s\\?]*)(\\?([^#\\s]*))?(#(\\w*))?$');
		var result = (source.match(patt));
		console.debug(result);
		if(!result) {
			domain = source;
		} else {
			domain = result[1] + "://" + result[2];
		}
		

		tinymce
				.get("editor-container")
				.execCommand(
						'mceInsertContent',
						false,
						"<p class='fake'><br></p><div id='"
								+ id
								+ "' class='image-container media-holder' contenteditable='false' draggable='true'>"
								+ "<img src='"
								+ source
								+ "' style='max-width:100%'/>"
								+ "<div id='rm"
								+ id
								+ "' class='media-button icon_close media-killer'></div><div class='media-button icon_drag media-mover'>X</div>"
								+ "<div id='source-url' style='text-align:right; font-size: small; max-width:160px; overflow:hidden; textoverflow:ellipsis; white-space:nowrap;'>from : <a href='"
								+ domain + "' target='_blank'>" + domain
								+ "</a></div>" + "</div><p><br></p>");
	} else if (type == 'video') {
		// To-Do
		// 동영상 플레이어 종류 : youtube, vimeo, viki, daum, tudou, youku,
		// 완료 : youtube, daum, viki, vimeo, youku
		// 남은 플레이어 : tudou (더 찾아보기)
		var patt = new RegExp(
				'^(https?):\\/\\/([^:\\/\\s]+)(:([^\\/]*))?((\\/[^\\s/\\/]+)*)?\\/([^#\\s\\?]*)(\\?([^#\\s]*))?(#(\\w*))?$');
		var result = (source.match(patt));

		if (result) {
			console.debug(result);
			var domain = result[2];
		}

		var player;
		//https://youtu.be/xLCmIhKH2Ik
		if (result) {
			var v_id;
			if (result[2] == "youtu.be") { // youtube
				player = "<iframe src='https://www.youtube.com/embed/"
						+ result[7]
						+ "?rel=0' frameborder='0' allowfullscreen></iframe>";
				player += "<img src='http://img.youtube.com/vi/" + result[7]
						+ "/0.jpg' style='display:none'>";
				// player += "<div id='player'> <div>";

			} else if (result[2] == "tvpot.daum.net") { // 다음
				player = "<iframe src='http://videofarm.daum.net/controller/video/viewer/Video.html?vid="
						+ result[7]
						+ "&play_loc=undefined&alert=true' frameborder='0' scrolling='no' ></iframe>";
				player += "<img src=https://t1.daumcdn.net/tvpot/thumb/"
						+ result[7] + "/thumb.png style='display:none'>";
			} else if (result[2] == "www.viki.com") { // viki
				v_id = result[7].split('-');

				// VIKI appID : '65535a' // 개발자 사이트에 appID
				var rs;
				$.ajax({
					url : "http://api.viki.io/v4/videos/" + v_id[0]
							+ ".json?app=65535a",
					async : false,
					method : 'get',
					dataType : 'json',
					success : function(data) {
						rs = data;
						// console.debug(rs);
						rs = data.images.poster.url;
					}
				});
				console.debug(rs);

				player = "<iframe src='http://www.viki.com/player/"
						+ v_id[0]
						+ "' width='640' height='360' frameBorder='0'></iframe>";
				player += "<img src='" + rs + "' style='display:none;'>";
			} else if (result[2] == "vimeo.com") { // vimeo
				var url = "https://vimeo.com/api/oembed.json?url="+source;
						//+ result[7];

				var rs = $.ajax({
					url : url,
					async : false,
					dataType : 'json',
					success : function(data) {
						console.debug(data);
						//return data.thumbnail_url;
					}
				});

				//console.debug(rs.responseJSON.thumbnail_url);
				var thumbnail_url = rs.responseJSON.thumbnail_url;
				player = rs.responseJSON.html;
					    // "<iframe src='https://player.vimeo.com/video/"
						//+ result[7]
						//+ "?color=91010b&title=0&byline=0&portrait=0&badge=0' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
				player += "<ima src='" + thumbnail_url
						+ "' style='display:none;'>";
			} else if (result[2] == "v.youku.com") { // youku
				// console.debug(result[7]);
				var _id = result[7].split('_');
				v_id = _id[1].split('.');
				// console.debug(v_id);
				player = "<iframe height=498 width=510 src='http://player.youku.com/embed/"
						+ v_id[0] + "' frameborder=0 allowfullscreen></iframe>";
			}

		} else {
			player = source;
		}

		tinymce
				.get("editor-container")
				.execCommand(
						'mceInsertContent',
						false,
						"<p class='fake'><br></p><div id='"
								+ id
								+ "' class='video-container media-holder' contenteditable='false' draggable='true'>"
								+ player
								+ "<div class='media-button icon_close media-killer'></div><div class='media-button icon_drag media-mover'>X</div>"
								+ "<div id='source-url' style='position: absolute; right: 0; bottom:0; text-align:right; font-size: small;'>from : <a href='"
								+ domain + "' target='_blank'>" + domain
								+ "</a></div>" + "</div><p><br></p>");

		// player = "<img src='http://img.youtube.com/vi/" + result[7] +
		// "/0.jpg'>";
		tinymce.get("editor-container").execCommand('mceInsertContent', false,
				"<p></p>");
	}

	// media-holder
	$('#' + id).on("mouseover", function(e) {
		$('#' + id).addClass('show-controls');
	});
	$('#' + id).on("mouseout", function(e) {
		$('#' + id).removeClass('show-controls');
	});

	// To-Do
	// 삽입한 리소스 제거 버튼이 클릭될 수 있도록 수정
	$('#rm' + id).on('click', function(e) {
		console.debug("cehck;");
		$('#' + id).remove();
	});

	var elem = document.getElementById(id);
	$(elem).on('dragstart', function(e) {
		console.debug("debug dragstart " + e.target.id);
		event.dataTransfer.setData("text", e.target.id);
	});

	// To-Do
	// IE 브라우저에서 dragstart 이벤트가 발생할 수 있도록 수정

}

// insert Image
function loadImage(target) {
	var form = $("#upload-form")[0];
	var formData = new FormData(form);
	$.ajax({
		url : '/writingform/upload/',
		processData : false,
//		processData / boolean
//		data 지정한 개체를 쿼리 문자열로 변환할지 여부를 설정합니다. 
//		기본값은 true로, 자동으로 "application / x - www - form - urlencoded"형식으로 변환합니다. 
//		DOMDocument 자체 등의 다른 형식으로 데이터를 보내기 위하여 자동 변환하고 싶지 않은 경우는 false를 지정합니다.
		contentType : false,
		async : false,
		data : formData,
		type : 'POST',
		success : function(result) {

			// To-Do
			// 서버에 저장된 이미지 경로(파일명) 저장했다가 도중에 종료될 경우 삭제하도록 서버에 요청

			TestList.push(result);

			console.debug(TestList);
			tinymce.get("editor-container")
					.execCommand(
							'mceInsertContent',
							false,
							"<img src='" + result
									+ "' style='max-width:100%'/>");
			// makeResourceDiv('img', result);
			$('#img-upload-modal').modal('hide');
		}
	});
}

// insert Video
function loadVideo() {
	var form = $("#video-upload-form")[0];
	var formData = new FormData(form);
	// create random id
	var id = generateID();
	var domain = "www.tellpin.com";

	$
			.ajax({
				url : '/writingform/upload/',
				processData : false,
				contentType : false,
				async : false,
				data : formData,
				type : 'POST',
				success : function(result) {
					//console.debug(result);
					//TestList.push(result);
					tinymce
							.get("editor-container")
							.execCommand(
									'mceInsertContent',
									false,
									"<p class='fake'><br></p><div id='"
											+ id
											+ "' class='video-container media-holder' contenteditable='false' draggable='true'>"
											+ "<video width='100%' controls> <source src='"
											+ result
											+ "' type='video/mp4'> </video>" 
											+ "<div class='media-button icon_close media-killer'></div><div class='media-button icon_drag media-mover'>X</div>"
											+ "<div id='source-url' style='position: absolute; right: 0; bottom:0; text-align:right; font-size: small;'>from : <a href='"
											+ domain + "' target='_blank'>" + domain
											+ "</a></div>" + "</div><p><br></p>");
					$('#video-upload-modal').modal('hide');
				}
			});
}

// insert image url
function addImageUrl() {
	var source = $("#embed-input").val();

	makeResourceDiv('img', source);

	$("#embedModal").modal('hide');
	$('#img-upload-modal').modal('hide');

}

// insert video url
function addVideoUrl() {
	var source = $("#video-url-input").val();

	makeResourceDiv('video', source);

	$("#video-url-Modal").modal('hide');
	$('#video-upload-modal').modal('hide');

}

// add link url
//var linkUrl;
//function addLinkUrl() {
//	var source = $("#link-url-input").val();
//	$('#url-preview').empty();
//	var loading = document.createElement("img");
//	loading.setAttribute("style",
//			"text-align:center; height:136px; margin-top:22px;")
//	loading.src = "http://static.se2.naver.com/static/full/20141105/ico_loading.gif";
//	$('#url-preview').append(loading);
//	$
//			.ajax({
//				url : '/writingform/urlpreview/',
//				data : {
//					'url' : source
//				},
//				type : 'POST',
//				success : function(result) {
//					if (result != "invalid") {
//						var data = JSON.parse(result);
//						$('#url-preview').empty();
//
//						// To-Do
//						// 결과물을 보기 좋게 뿌려보자
//
//						var link = document.createElement('div');
//						link
//								.setAttribute('style',
//										'display: inline-block; cursor: pointer; width:100%;');
//						link.setAttribute('contenteditable', false);
//						link.onclick = function() {
//							location.href = source;
//						};
//						linkUrl = source;
//
//						var basic_info = document.createElement('div');
//						basic_info.setAttribute('style',
//								'float:right; width:80%;');
//
//						var tumnail = document.createElement('div');
//						tumnail.setAttribute('style', 'float:left; width:20%;');
//						var title = document.createElement("h2");
//						title.innerHTML = data['title'];
//						title
//								.setAttribute(
//										'style',
//										'float:right; text-align:left; padding: 5px; width:100%; overflow:hidden; text-overflow:ellipsis; white-space:nowrap;');
//						var description = document.createElement('h3');
//						if (data['description']) {
//							description.innerHTML = data['description'];
//						}
//						description
//								.setAttribute(
//										'style',
//										'float:right; text-align:left; padding: 5px; width:100%; overflow:hidden; text-overflow:ellipsis; white-space:nowrap;');
//						var img = document.createElement("img");
//						img.src = data['image'];
//						img.setAttribute('style', 'width: 100%; height:100%');
//						var url_source = document.createElement("p");
//						url_source.innerHTML = data['url'];
//						url_source
//								.setAttribute(
//										'style',
//										'float:right; width:50%; overflow:hidden; text-overflow:ellipsis; white-space:nowrap;');
//
//						basic_info.appendChild(title);
//						basic_info.appendChild(description);
//						tumnail.appendChild(img);
//
//						link.appendChild(basic_info);
//						link.appendChild(tumnail);
//						link.appendChild(url_source);
//						$('#url-preview').append(link);
//
//					} else {
//						$('#url-preview')
//								.html(
//										'<div id="invalid-url" class="alert alert-danger" role="alert">invalid url</div>');
//					}
//				}
//			});
//}

// insert link-url
//function insertLinkUrl() {
//	// console.debug($('#url-preview').html());
//	if ($('#invalid-url').length > 0) {
//		$('#url-preview').empty();
//	}
//	tinymce.get("editor-container").insertContent(
//			"<a href='" + linkUrl + "'>" + linkUrl + "</a>");
//	tinymce.get("editor-container").insertContent(
//			"<p>" + $('#url-preview').html() + "</p> <p></p>");
//
//	$('#link-url-Modal').modal('hide');
//}


// upload document file 
function uploadDocument() {
	var form = $("#document-upload-form")[0];
	
	var extArray = new Array("pdf", "ppt", "pptx");
	var allowSubmit = false;
	var file = $('#file-browser').val();
	console.debug($('#file-browser').val());
	if(!file) return;
	var fileExt = file.substring(file.lastIndexOf('.')+1);
	console.debug(fileExt);
	for (var i = 0; i < extArray.length; i++) {
		if (extArray[i] == fileExt) {
			allowSubmit = true;
			break;
		}
	} 
	if(!allowSubmit) {
		alert("pdf, ppt, pptx 파일만 업로드할 수 있습니다.");
		return;
	} 
		
	
	var formData = new FormData(form);
	$.ajax({
		url : '/writingform/fileupload/',
		processData : false,
		contentType : false,
		async : false,
		data : formData,
		type : 'POST',
		success : function(result) {
			// To-Do
			console.debug(result);
		}
	});
}

// history back
function goBack() {
	window.history.back();
}

// url move
function goUrl(url) {
	window.location.href = url;
}

// note submit
function submitData() {

	// club, notecast, language 정보를 담는다
	// TO-DO
	// 먼저 리소스를 편집하기 위해 추가했던 element 혹은 class를 모두 제거한다.
	$('.media-button').remove();

	// class로 정의했던 스타일은 inline style로 다시 새겨준다.
	if ($('.video-container').length) {
		console.debug("video class working");
		$('.video-container')
				.each(
						function(index) {
							$(this)
									.attr(
											"style",
											"position: relative; padding-bottom: 56.25%; padding-top: 25px; width: 100%; height: auto; overflow: hidden;");
							if ($(this).children("iframe").length) {
								$(this)
										.children("iframe")
										.attr("style",
												"position: absolute; top: 0; left: 0; width: 96%; height: 100%;");
							} else if ($(this).children("object")) {
								$(this)
										.children("object")
										.attr("style",
												"position: absolute; top: 0; left: 0; width: 96%; height: 100%;");
							} else if ($(this).children("embed")) {
								$(this)
										.children("embed")
										.attr("style",
												"position: absolute; top: 0; left: 0; width: 96%; height: 100%;");
							}
							$(this).removeClass('media-holder');
							$(this).removeClass('video-container');
						});

	} else if ($('.image-container').length) {
		console.debug("image class working");
		$('.image-container')
				.each(
						function(index) {
							$(this)
									.attr(
											"style",
											"position: relative; overflow: hidden; vertical-align: middle; text-align: center; max-width: 100%; max-height: auto;");
							$(this).removeClass('media-holder');
							$(this).removeClass('image-container');
						});

	} else {
		console.debug("nothing class");
	}

	// 남아있는 편집공간 내 html을 그대로 content로 전송한다.
	var title = $('#textarea').val();
	var content = $('#editor-container').html();

	console.debug(content);
	if (title == "") {
		alert("no title");
		return;
	}

	var formData = new FormData();
	formData.append('title', title);
	formData.append('content', content);

	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "/writingform/post_write/");
	xmlHttp.onreadystatechange = function() {
		var result;
		if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
			result = xmlHttp.responseText;
			console.debug(result);
			alert("확인 : database insert");
			// goBack();
		}
	}
	xmlHttp.send(formData);

}
//########################################################################################################
//ajax progress