/**
 *	videoFileUpload.js 
 */

var videoApp = angular.module( "fileUpload", [ "ngFileUpload" ]);

videoApp.config( function ( $interpolateProvider ) {
	$interpolateProvider.startSymbol( "[[" );
	$interpolateProvider.endSymbol( "]]" );
});

videoApp.controller( "videoCtrl", [ "$scope", "Upload", "$timeout", function( $scope, Upload, $timeout ) {
	$scope.uploadFiles = function( file, errFiles ) {
		$scope.f = file;
		$scope.errFile = errFiles && errFiles[0];
		
		console.log( file );
		
		if ( file ) {
			file.upload = Upload.upload({
				url: "/writingform/videoupload/",
				data: { file : file }
			});
			
			file.upload.then( function ( response ) {
				console.log ( response );
				$timeout( function () {
					file.result = response.data;
				});
			}, function ( response ) {
				if ( response.status > 0 )
					$scope.errorMsg = response.status + ": " + response.data;
			}, function ( evt ) {
				file.progress = Math.min( 100, parseInt( 100.0 * evt.loaded / evt.total ));
			});
		}
		else {
			console.log( "upload fail" );
		}
	}
	
	$scope.uploadVideo = function ( src ) {
		var elem = '<div id="videoArea"><video controls src="'+ $scope.f.result +'"></video></div>';
		
		angular.element( "body" ).append( elem );
		angular.element( "#fileModal" ).modal( "hide" );
	}
}]);










