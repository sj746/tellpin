tellpinApp.controller( 'mycontentCtrl', [ '$scope', '$window', '$http', function( $scope, $window, $http ) {
	$scope.init = function( obj ) {
		console.log( obj );
		$scope.item = obj.all;
		$scope.type = 'lbAll';
		for( var i = 0; i < $scope.item.list.length; i++ ) {
			$scope.item.list[i].writeDay = $scope.item.list[i].updated_time;
		}
		console.log( $scope.item );
		$scope.tp = obj.tp;
		$scope.lq = obj.lq;
		$scope.lb = obj.lb;
		$scope.browser = checkBrowser();
	}
	
	$scope.getMyContentList = function( type ) {
		if ( $scope.type == type ) {
			return;
		}
		$scope.item.page = 1;
		$http.post( '/mycontent/list/', { 'type' : type, 'page' : $scope.item.page } )
		.success( function( result ) {
			console.log( result );
			if ( result.isSuccess == 1 ) {
				$scope.item = result.item;
				for( var i = 0; i < $scope.item.list.length; i++ ) {
					$scope.item.list[i].writeDay = $scope.item.list[i].updated_time;
				}
				$scope.type = type;
			}
		});
	}
	
	$scope.showMore = function( page ) {
		$http.post( '/mycontent/list/', { 'type' : $scope.type, 'page' : page } )
		.success( function( result ) {
			console.log( result );
			if ( result.isSuccess == 1 ) {
				for( var i = 0; i < result.item.list.length; i++ ) {
					result.item.list[i].writeDay = new Date( result.item.list[i].updated_time );
					$scope.item.list.push( result.item.list[i] );
				}
				$scope.item.page = result.item.page;
				$scope.item.has_next = result.item.has_next;
			}
		});
	}
	
	$scope.moveUrl = function( obj ) {
		if ($scope.type == 'lbAll'
				|| $scope.type == 'lbAdd'
				|| $scope.type == 'lbVote'
				|| $scope.type == 'lbPin') {
			$window.location.href = "/board/" + obj.pid + "/";
		} else if ($scope.type == 'tpAll'
				|| $scope.type == 'tpQuestion'
				|| $scope.type == 'tpAnswer'
				|| $scope.type == 'tpComment'
				|| $scope.type == 'tpVote'
				|| $scope.type == 'tpPin') {
			$window.location.href = "/trans/" + obj.qid + "/";
		}
		else {
			$window.location.href = "/questions/" + obj.qid + "/";
		}
	}
}]);