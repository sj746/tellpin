

tellpinApp.controller("headerController",function($scope, $http, $window, $timeout, $q){
	var livein_name;
	$scope.time_location = function(time_location){
//		console.log(time_location);
		livein_name = time_location.substring(time_location.lastIndexOf('0)') + 2);
//		console.log("size = " + livein_name.length);
//		console.log("livein_name = " + livein_name);
	}
	$scope.time_location_nologin = function(){
		livein_name = "";
	};
	
	$scope.openPeople = function() {
		PeopleMneuLeft = $scope.setUserMenu("people-menu");
		$scope.setLeftUserMenu("people-menu", PeopleMneuLeft);
		$scope.selectedMenu = 3;
	}
	
	$scope.setHeaderMenu = function(utc_time){
//		console.log( 'setHeaderMenu' );
		communityLeft = $scope.setSubMenu('community');
		toolsLeft =  $scope.setSubMenu('tools');
		lessonsLeft = $scope.setSubMenu('lessons');
		myMenuLeft = $scope.setUserMenu("my-menu");
		PeopleMneuLeft = $scope.setUserMenu("people-menu");
		alarmMneuLeft = $scope.setUserMenu("alarm-menu");
		$(".tellpin-sub-menu").css("display","block"); //처음 페이지를 로드할때 보였다 사라지는 버그를 해결하기위한 코드
		$(".user-sub-menu").css("display","block"); //처음 페이지를 로드할때 보였다 사라지는 버그를 해결하기위한 코드
		$(".timezone-sub-menu").css("display","block");
		
		angular.element($window).bind('load resize', function(e) {
			$scope.setLeftSubMenu('community', communityLeft);
			$scope.setLeftSubMenu('tools', toolsLeft);
			$scope.setLeftSubMenu('lessons', lessonsLeft);
			$scope.setLeftUserMenu("my-menu", myMenuLeft);
			$scope.setLeftUserMenu("people-menu", PeopleMneuLeft);
			$scope.setLeftUserMenu("alarm-menu", alarmMneuLeft);
		});
		$scope.show_alarm=1;
		$scope.userUTCTime = utc_time;
		$scope.setCurrentTime();
		$scope.alarm_state = 0;
		$scope.click_alarm_tab('lesson');
		
//		console.log( 'header.js' );
	}
	
	$scope.setHeaderMenu_nologin = function() {
		communityLeft = $scope.setSubMenu('community');
		toolsLeft =  $scope.setSubMenu('tools');
		//lessonsLeft = $scope.setSubMenu('lessons');
		//myMenuLeft = $scope.setUserMenu("my-menu");
		//PeopleMneuLeft = $scope.setUserMenu("people-menu");
		//alarmMneuLeft = $scope.setUserMenu("alarm-menu");
		$(".tellpin-sub-menu").css("display","block"); //처음 페이지를 로드할때 보였다 사라지는 버그를 해결하기위한 코드
		$(".user-sub-menu").css("display","block"); //처음 페이지를 로드할때 보였다 사라지는 버그를 해결하기위한 코드
		$(".timezone-sub-menu").css("display","block");
		
		angular.element($window).bind('load resize', function(e) {
			$scope.setLeftSubMenu('community', communityLeft);
			$scope.setLeftSubMenu('tools', toolsLeft);
			//$scope.setLeftSubMenu('lessons', lessonsLeft);
			//$scope.setLeftUserMenu("my-menu", myMenuLeft);
			//$scope.setLeftUserMenu("people-menu", PeopleMneuLeft);
			//$scope.setLeftUserMenu("alarm-menu", alarmMneuLeft);
		});
		//$scope.show_alarm=1;
		//$scope.click_alarm_tab('lesson');
	};
	

	//resend
	$scope.resend_email2 = function(){
		console.log('resend_email2()');
		var obj = {
				'email':  $("#user_email2").text().trim()
		}
		
		console.log(obj);
		$http.post("/user_auth/resend_email/", {obj:obj}).success(function(response){
	       alert("resend_success");
		});
	}
	$scope.click_didnt = function(){
		console.log('didnt');
		$(".span-didnt-receive").toggle();
		$(".span-success-receive").toggle();
	}
	$scope.settingTopArea = function(level){
		if( level != 3 ){
			$(".base").css("margin-top", "75px");
			$(".tellpin-sub-menu").css("top", "70px");
			$(".user-sub-menu").css("top", "70px");
			$(".timezone-sub-menu").css("top", "60px");
		}else{
			$(".notification").css("display", "block");
		}
	}
	$scope.setLeftUserMenu =function(menuName, relativeLeft){
		var left = $("."+menuName).offset().left;
		$(".user-"+menuName).css("left", left+relativeLeft);
	}
	$scope.setSubMenu = function(menuName){
		var width = $(".tellpin-"+menuName).width();
		var left = $(".tellpin-"+menuName).offset().left;
		var subWidth = $("."+menuName+"-sub-menu").width();
		var relativeLeft = (width - subWidth)/2;
		$("."+menuName+"-sub-menu").css("left", left+relativeLeft);
		return relativeLeft;
	}
	$scope.setUserMenu = function(menuName){
		var width = $("."+menuName).width();
		var left = $("."+menuName).offset().left;
		var subWidth = $(".user-"+menuName).width();
		var relativeLeft = (width - subWidth)/2;
		$(".user-"+menuName).css("left", left+relativeLeft - 39.3125);
		return relativeLeft;
	}
	$scope.setLeftSubMenu =function(menuName, relativeLeft){
		var left = $(".tellpin-"+menuName).offset().left;
		$("."+menuName+"-sub-menu").css("left", left+relativeLeft);
	}
	$scope.goURL = function(url){
		console.log('goUrl()');
		console.log(url);
		if(url.length > 0){
			$window.location.href = (url);
		}
	}
	$scope.setCurrentTime = function(){ 
		var now = new Date();
		$scope.currentUTCTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
		$scope.changeTimezone();
		$timeout($scope.setCurrentTime, 1000);
	};
	
	$scope.changeTimezone = function(){
		if($scope.userUTCTime == ''){
			$scope.userUTCTime = minutesToUTC(-(new Date().getTimezoneOffset()));
		}
		var displayTime = new Date($scope.currentUTCTime.getTime() + utcToMilliseconds($scope.userUTCTime));
		$scope.currentTime = displayTime.format("HH:mm");
		$scope.localTime = displayTime;
	}
	
	$scope.compareTime = function() {
		var date_time = new Date();
		var display_time = $scope.localTime;
		var offset = date_time.getTimezoneOffset();

		if (date_time)
		    date_time = date_time.toString().substring(0, 22);
        if (display_time)
		    display_time = display_time.toString().substring(0, 22);

		var compare_result = date_time.localeCompare(display_time);
		return compare_result;	//0 = equal
	}
	
	$scope.settooltiptitle = function() {
		var str1 = "Your system clock is not aligned with your Tellpin account.\n";
		var str2 = "Your timezone is set to" + livein_name + ".\n";
		var str3 = "Please correct this at Edit Profile"
		var str_title = str1 + str2 + str3;
//		console.log(str_title);
		return str_title;
	}
	$scope.click_alarm_tab = function(tab){
		$(".alarm-row-content").css("visibility", "hidden");

		$scope.noti_data = $scope.get_noti();
		console.log( "CLICK_ALARM_TAB" );
		
		if(tab == 'lesson'){
			//TODO
			//get lesson data
			
			$scope.show_alarm=1;
			$("#alarm-tab-community").removeClass("div-active");
			$("#alarm-tab-lesson").addClass("div-active");
		}else if(tab == 'community'){
			//TODO
			//get coummunity data
			
			
			$scope.show_alarm=2;
			$("#alarm-tab-lesson").removeClass("div-active");
			$("#alarm-tab-community").addClass("div-active");

		}
		
	}
	//setInterval($scope.click_alarm_tab, 1000000);
	$scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
		
			$(".alarm-row-content").css("visibility", "visible");
	});

	$scope.get_noti = function(){
		var noti = get_notification();

//		console.log('get_noti');
		noti.then(function(result){
//			console.log('success');
//			console.log( result );
			$scope.alarm_list = result.res_data;
			$scope.alarm_lesson_list = result.res_data.lesson_list;
			$scope.alarm_community_list = result.res_data.community_list;
		}, function(reason){
			console.log('fail');
			console.log( reason );
		});

	}
	//call by javascript get_notification() 
	$scope.get_notification = function(){
		
		return $q(function(resolve, reject){
			$http.post("/user_auth/get_notification/").success( function( result ) {
				if(result.isSuccess == 1){
					resolve(result);
				}else{
					reject(result);
				}
			});
		});
	}
	$scope.set_notification_status = function(noti_id){
		
		console.log('set_notification_status()');
		console.log(noti_id);
		$http.post("/user_auth/set_notification_status/", {noid:noti_id} ).success( function( result ) {
			if(result.isSuccess == 1){
				console.log(result);
			}else{
				console.log(result);
			}
		});
	}
//	$scope.goUrl = function(re_url){
//		console.log(re_url);
//		//TODO status 처리
//
//		if(re_url.length > 0){
//			location.href = re_url;
//		}
//		//go url
//	}
	$scope.send_email_pop2 = function(){
		console.log('send');
		$(".tutor-schedule-backdrop2").css("display", "block");
		$("#pop-profile-template2").css("display", "block");
		//$scope.show_mail_pop = true;
		
	}
	$scope.hide_mail_pop2 = function(){
		console.log('hide');
		$(".tutor-schedule-backdrop2").css("display", "none");
		$("#pop-profile-template2").css("display", "none");
		//$scope.show_mail_pop = false;
	}
	//step 3
	$scope.confirm_email2 = function(){
		console.log('confirm_email()');
		
		//TODO 
		//send email
		//post email 보내고 종료. 해당 함수에서 redirect
		
		//window.location.href = "/user_auth/complete2/" + $("#user_email2").text().trim();te
		
		$http.post("/user_auth/complete2/"+$("#user_email2").text().trim()).success(function(result){
			if( result.isSuccess == 1 ){
				console.log(result.isSuccess);
				$scope.hide_mail_pop2();
			}
		});
		
	}
	//show change input 
	$scope.show_change2 = function(){
		console.log('show_change()');
		$(".div-submit-email").toggle('display');
		$(".span-didnt-receive").css('display', 'block');
		$(".span-success-receive").css('display', 'none');
	}
	//click submit button
	$scope.change_email2 = function(){
		//console.log('change_email2');
		//TODO
		//user email validation check
		if( !email_check() ){
			return;
		}
		//TODO
		//user email save - post 
		var obj = {
				'email': $("#change_email2").val()
		}
	
		$http.post("/user_auth/ajax_check_email/", { obj:obj }).success(function(response){
			result = response;
			console.log(result);
			console.log(result.isSuccess);
			if( result.isSuccess ){
				$("#user_email2").text( $("#change_email2").val() );
				$(".div-submit-email").toggle();
				$(".span-didnt-receive").toggle();
				$(".span-success-receive").css('display', 'block');
				$scope.resend_email2();
			}else{
				alert(result.message);
			}
		});
		
		
	}
});

