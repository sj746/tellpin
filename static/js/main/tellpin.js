var tellpinApp = angular.module('tellpinApp',['ngSanitize', 'angular-clipboard', 'ui.bootstrap', 'simplecolorpicker', 'toggle-switch', 'angular-medium-editor', 'infinite-scroll', 'rzModule' ]);
tellpinApp.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});


tellpinApp.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});

tellpinApp.directive('communityRenderAction', function($timeout){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){

        	//element[0].style.width = (380 - attrs.$$element[0].offsetWidth) + "px";
	        element[0].style.width = (370 - element[0].parentElement.children[0].offsetWidth) + "px";

        	scope.$watch('communityList', function(newValue, oldValue) {
		        element[0].style.width = (370 - element[0].parentElement.children[0].offsetWidth) + "px";
		    });
        	
        }
    };
});

tellpinApp.directive('selectOnClick', ['$window', function ($window) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on('click', function () {
                if (!$window.getSelection().toString()) {
                    // Required for mobile Safari
                    this.setSelectionRange(0, this.value.length)
                }
            });
        }
    };
}]);

tellpinApp.directive('tooltip', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
});

tellpinApp.directive('whenScrolled', function() {
    return function(scope, elm, attr) {
        var raw = elm[0];
        elm.bind('scroll', function() {
        	//console.log( 'aaaaaa', raw.scrollTop + raw.offsetHeight );
        	//console.log( 'bbbbbb', raw.scrollHeight );
    		if (raw.scrollTop + raw.offsetHeight == ( raw.scrollHeight + 2 )) {
    			scope.$apply(attr.whenScrolled);
    		}        		

        });
    };
});

tellpinApp.directive('krInput', ['$parse', function($parse) {
    return {
        priority : 2,
        restrict : 'A',
        compile : function(element) {
            element.on('compositionstart', function(e) {
                e.stopImmediatePropagation();
            });
        },
    };
}]);

tellpinApp.directive('focusMe', function($timeout, $parse) {
	  return {
		    //scope: true,   // optionally create a child scope
		    link: function(scope, element, attrs) {
		      var model = $parse(attrs.focusMe);
		      scope.$watch(model, function(value) {
		        console.log('value=',value);
		        if(value === true) { 
		          $timeout(function() {
		            element[0].focus(); 
		          });
		        }
		      });
		      // to address @blesh's comment, set attribute value to 'false'
		      // on blur event:
		      /*element.bind('blur', function() {
		         console.log('blur');
		         scope.$apply(model.assign(scope, false));
		      });*/
		    }
		  };
});

tellpinApp.directive('focus', function( $timeout ) {
    return function(scope, element, attrs) {
        		$timeout( function() {
        			element[0].focus();        			
        		}, 50);
    }
});

//tellpin filters
tellpinApp.filter('slice', function() {
	  return function(arr, start, end) {
	    return arr.slice(start, end);
	  };
});
tellpinApp.filter('getUTCTime', function() {
	  return function(time, utc) {
		  
		  
	    return arr.slice(start, end);
	  };
});
tellpinApp.filter('scheduleHeaderFiler', function() {
	  return function(arr) {
		  start = arr[0];
		  end = arr[arr.length-1];
		  var monthNames = ["January", "February", "March", "April", "May", "June",
		                    "July", "August", "September", "October", "November", "December"];
		  var headerString = monthNames[ parseInt(start.slice(4,6))-1] + " " + start.slice(6,8) + "-"+end.slice(6,8)+", "+ start.slice(0,4);
		  headerString = start.slice(0,4) + "-" + start.slice(4,6) + "-" + start.slice(6,8) + " ~ "+ end.slice(0,4) + "-" + end.slice(4,6) + "-" + end.slice(6,8);
	    return headerString
	  };
});

tellpinApp.filter('selectedTableFilter', function() {
	  return function(arr) {
	    return arr.slice(0,4)+"."+arr.slice(4,6)+"."+arr.slice(6,8);
	  };
});
tellpinApp.filter('scheduleDateFilter', function() {
	  return function(arr, index) {
		  if(arr.slice(4,5) == "0"){
			  return arr.slice(5, 6)+"/"+arr.slice(6,8);
		  }
		  else{
			  return arr.slice(4, 6)+"/"+arr.slice(6,8);
		  }
	  };
});
tellpinApp.filter('capitalize', function() {
	  return function(arr) {
		  return arr.charAt(0).toUpperCase() + arr.slice(1,3);
	  };
});
tellpinApp.filter('applyTimzone', function() {
	  return function(rvtime) {
	    return rvtime;
	  };
});
tellpinApp.filter('getText', function() {
	  return function(content) {
		  if(!content) return;
		  content = content.replace( /(<\/p>)/g, " " );
		  content = content.replace(/(<([^>]+)>)/g, "");
		  return content.replace(/&nbsp;/g, " ");
	  };
});
tellpinApp.filter('displayTime', function() {
	  return function(arr, browser) {
		  browser = checkBrowser();
		  //console.log( '##### browser', browser );
		  var date;
		  if(browser == 'Chrome'){
			  date = new Date(arr.slice(0,19));
		  }
		  else{
			  console.log(arr);
			  isoDateFormat = arr.slice(0,19).replace(" ","T");
			  date = new Date(isoDateFormat);
		  }
		  //var dateUTC = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),  date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds(), date.getUTCMilliseconds());
		  var now = new Date();
		  var UTCTime = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
		  var liveTime = UTCTime.getTime() - date.getTime()

		  var seconds = liveTime/1000;
		  if(seconds < 1){
			 display = "just now";
		  }
		  else if (seconds == 1){
			  display = parseInt(seconds) + " second ago";
		  }
		  else if(seconds > 1 && seconds < 60){
			  display = parseInt(seconds) + " seconds ago"; 
		  }
		  else if(seconds >= 60 && seconds < 120 ){
			  display = parseInt(seconds/60) + " minute ago";
		  }
		  else if(seconds >= 120 && seconds < 3600){
			  display = parseInt(seconds/60) + " minutes ago"; 
		  }
		  else if(seconds >= 3600 && seconds < 7200){
			  display = parseInt(seconds/60/60) + " hour ago";
		  }
		  else if(seconds >= 7200 && seconds < 86400){
			  display = parseInt(seconds/60/60) + " hours ago"; 
		  }
		  else{
			  if( date.getFullYear() == UTCTime.getFullYear() ){
				  if( date.getMonth() == UTCTime.getMonth() ){
					  display = (UTCTime.getDate() - date.getDate());
					  if(display == 1) {
						  display = display + " day ago";
					  } else{
						  display = display + " days ago";
					  }
				  }
				  else{
					  display = (UTCTime.getMonth() - date.getMonth());
					  if(display == 1){
						  display = display + " month ago";
					  }else {
						  display = display + " months ago";  
					  }
				  }
			  }
			  else{
				  display = (UTCTime.getFullYear() - date.getFullYear());
				  if (display == 1){
					  display = display + " year ago";
				  }else{
					  display = display + " years ago";
				  }
			  }
		 }
		 return display;
	  };
});
tellpinApp.filter('shortLanguage', function() {
	  return function(language) {
		  return language.toUpperCase().slice(0,3)
	  };
});
tellpinApp.filter('getDay', function() {
	  return function(index) {
		  var weekList = ["Mon", "Tue", "Wed", "Thu", "Fri", " Sat" ,"Sun"];
		  if(index == 0){
			  return weekList[6];
		  }
		  else{
			  return weekList[index-1]; 
		  }
	  };
});
tellpinApp.filter('getTime', function() {
	  return function(value) {
		  var timeString = value.slice(0,2)+":"+value.slice(2,7)+":"+value.slice(7,9);
		  return timeString;
	  };
});
tellpinApp.filter('displayDate', function() {
	  return function(value) {
		  var date = new Date(value);
		  return date.format("MM/dd/yyyy");
	  };
});


tellpinApp.filter('getIndexToTime', function() {
	  return function(index) {
		  return getIndexToTimeFor24(index);
	  };
});

tellpinApp.filter('nl2p', function () {
    return function(text){
        text = String(text).trim();
        return (text.length > 0 ? '<p>' + text.replace(/[\r\n]+/g, '</p><p>') + '</p>' : null);
    }
});

tellpinApp.filter('nl2p2', function() {
    return function(text){
        text = String(text).trim();
        return (text.length > 0 ? '<p>' + text.replace(/[@]+/g, '</p><p>') + '</p>' : null);
    }
});

//functions
function minutesToUTC(minute){
	var UTCString = "";
	if(minute<0){
		UTCString += "-";
	}
	else{
		UTCString += "+";
	}
	if(Math.abs(minute)/60 > 10){
		UTCString += Math.floor(Math.abs(minute)/60) + ":";
	}
	else{
		UTCString += "0"+ Math.floor(Math.abs(minute)/60) + ":";
	}
	if(Math.abs(minute) % 60 > 10){
		UTCString += Math.abs(minute) % 60;
	}
	else{
		UTCString += "0" + Math.abs(minute) % 60;
	}
	return UTCString;
}
function utcToMilliseconds(utc){
	var ms = ((parseInt(utc.slice(1,3)) * 60) + parseInt(utc.slice(4,6))) * 60000;
	if(utc.slice(0,1) == "-"){
		ms = -(ms);
	}
	return ms;
}
var get_notification_data;
//get notification
function get_notification(){
	//angular.element($(".container")).scope().search_name();
	var noti = angular.element($("#tellpin-header")).scope().get_notification();
		
	return noti;
	
}
//set notification status
function set_noti_status(noti_id){
	console.log("set_noti_status()");
	console.log(noti_id);
	var noti = angular.element($("#tellpin-header")).scope().set_notification_status(noti_id);
	
	return noti;
}

function getIndexToDay(index){
	var alphabetList = ["A", "B", "C", "D", "E", "F", "G"];
	if(index == 0){
		return alphabetList[6];
	}
	else{
		return alphabetList[index-1];
	}
}

function getIndexToTime(index){
	var timeString = "";
	if(index%2 == 0){
		if(index/2 < 10){
			timeString = "0"+index/2+"00";
		}
		else{
			timeString = index/2+"00";
		}
	}
	else{
		if((index-1) / 2 < 10){
			timeString = "0"+(index-1)/2+"30";
		}
		else{
			timeString = (index-1)/2+"30";
		}
	}
	return timeString;
}
function getIndexToTimeFor24(index){
	var timeString = "";
	if(index%2 == 0){
		if(index/2 < 10){
			timeString = "0"+index/2+":00";
		}
		else{
			timeString = index/2+":00";
		}
	}
	else{
		if((index-1) / 2 < 10){
			timeString = "0"+(index-1)/2+":30";
		}
		else{
			timeString = (index-1)/2+":30";
		}
	}
	return timeString;
}
function dayToAlphabet(day){
	var dayList = ["mon", "tue", "wed", "thu","fri","sat","sun"];
	var alphabetList =["A", "B", "C", "D", "E","F","G"];
	for(var idx=0; idx<7; idx++){
		if( dayList[idx] == day){
			return alphabetList[idx];
		}
	}
}

function dayToIndex(day){
	switch(day){
	case 'A':
		return 0;
	case 'B':
		return 1;
		break;
	case 'C':
		return 2;
	case 'D':
		return 3;
	case 'E':
		return 4;
	case 'F':
		return 5;
	case 'G':
		return 6;
	default : 
		return;
	}
}
//function UTCToIndex(utc){
//	var index = (parseInt(utc.slice(1,3)) * 2);
//	if(utc.slice(4,6) == "30"){
//		index += 1;
//	}
//	if(utc.slice(0,1) == '-'){
//		index = -index;
//	}
//	return index;
//}
//function areaForUTC(idx, d, utcIndex){
//	var index = utcIndex - ;
//	var day = d;
//	if( index < 0){
//		if(day - 1 < 0){
//			day = 6;
//		}
//		else{
//			day -= 1 ;
//		}
//		index = 47 - index;
//	}
//	else if( index > 47){
//		if(day + 1 > 6){
//			day = 0;
//		}
//		else{
//			day += 1;
//		}
//		index = index - 48;
//	}
//	var obj = new Object();
//	obj.index = index;
//	obj.day = day;
//	return obj;
//}
//function utcForArea(idx, d, utcIndex){
//	var index = idx - utcIndex;
//	var day = d;
//	if( index < 0){
//		if(day - 1 < 0){
//			day = 6;
//		}
//		else{
//			day -= 1 ;
//		}
//		index = 47 - index;
//	}
//	else if( index > 47){
//		if(day + 1 > 6){
//			day = 0;
//		}
//		else{
//			day += 1;
//		}
//		index = index - 48;
//	}
//	var obj = new Object();
//	obj.index = index;
//	obj.day = day;
//	return obj;
//}
function setDisplayTime(index){
	var time;
	if(index == 0){
		time = "12:00 AM";
	}
	else if(index%2 == 0){ 
		if(index/2 < 12){ //AM
			if(index/2 < 10){ // 한 자리 수
				time = "0" + index/2 + ":00 AM";
			}
			else{ // 두 자리 수
				time = index/2 + ":00 AM";
			}
		}
		else{ //PM
			if(index == 24){
				time = "12:00 PM";
			}
			else if(index/2 == 12){
				time = (index/2-12) + ":00 PM";
			}
			else if((index/2)-12 < 10){ 
				time = "0" + (index/2-12) + ":00 PM";
			}
			else{
				time = (index/2-12) + ":00 PM";
			}
		}
	}
	else{ //half
		if(index/2 < 12){
			if(((index-1)/2) < 10){
				time = "0" + (index-1)/2 + ":30 AM";
			}
			else{
				time = (index-1)/2 + ":30 AM";
			}
		}
		else{
			if(index-1 == 24){
				time =((index-1)/2) + ":30 PM";
			}
			else if( ((index-1)/2)-12 < 10){
				time ="0" + ((index-1)/2-12) + ":30 PM";
			}
			else{
				time = ((index-1)/2-12) + ":30 PM";
			}
		}
	}
	return time;
}

function indexToValue(index){
	var value = "";
	if(index%2 == 0){
		if(index/2 <10){
			value = "0"+ index/2+"00";
		}
		else{
			value = index/2+"00";
		}
	}
	else{
		if((index-1)/2 <10){
			value = "0"+ (index-1)/2 +"30";
		}
		else{
			value = (index-1)/2 +"30";
		}
	}
	return value;
}
function checkBrowser(){
	var agt = navigator.userAgent.toLowerCase();
//	console.log(agt);
	if (agt.indexOf("chrome") != -1) return 'Chrome';
	if ( (navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agt.indexOf("msie") != -1) ) return 'Internet Explorer';
	if (agt.indexOf("safari") != -1) return 'Safari';
	if (agt.indexOf("firefox") != -1) return 'Firefox';
}

//dateformat
String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};
Date.prototype.format = function(f) {
    if (!this.valueOf()) return " ";
 
    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;
     
    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).zf(2);
            case "MM": return (d.getMonth() + 1).zf(2);
            case "dd": return d.getDate().zf(2);
            case "E": return weekName[d.getDay()];
            case "HH": return d.getHours().zf(2);
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case "mm": return d.getMinutes().zf(2);
            case "ss": return d.getSeconds().zf(2);
            case "a/p": return d.getHours() < 12 ? "오전" : "오후";
            default: return $1;
        }
    });
};

function goUrlAfterLogin(url) {
	if(url.length > 0){
		location.href = "/user_auth/ajax_nologin/?url=" + url;
	}
}