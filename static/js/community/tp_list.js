tellpinApp.controller( "tpListController", function($scope, $http, $location, $window, $sce, $timeout,clipboard) {
	$scope.initTP = function(data){
		console.log(data);

		$scope.languageInfo = data.languageInfo;
		$scope.langIndex = 1;
		$scope.userInfo = data.userInfo;
		$scope.languageInfoForFrom = angular.copy($scope.languageInfo);
		$scope.languageInfoForTo = angular.copy($scope.languageInfo);
		$scope.languageInfoForIn = angular.copy($scope.languageInfo);
		
		$scope.languageInfoForFrom.splice(0, 0, {"id": 0, "lang_english": "All languages I speak"});
		$scope.languageInfoForTo.splice(0, 0, {"id": 0, "lang_english": "All languages I speak"});
		$scope.languageInfoForIn.splice(0, 0, {"id": 0, "lang_english": "All languages I speak"});

		if( $scope.userInfo != undefined && $scope.userInfo != null) {
			$scope.nativeLanList();
			$scope.learningLanList();
//			$scope.otherLanList();
		}

		$scope.checkedTPRadio = 0;
		$scope.fromLang = 0;
		$scope.toLang = 0;
		$scope.inLang=0;

//		console.log($scope.langIndex);
		$scope.languageInfoForFrom.splice($scope.langIndex + 1, 0, {"id": $scope.langIndex + 1, "lang_english": "-----------------------------------------"});
		$scope.languageInfoForTo.splice($scope.langIndex + 1, 0, {"id": $scope.langIndex + 1, "lang_english": "-----------------------------------------"});
		$scope.languageInfoForIn.splice($scope.langIndex + 1, 0, {"id": $scope.langIndex + 1, "lang_english": "-----------------------------------------"});

		$scope.browser = checkBrowser();
		$scope.startForPaging = 0;
		$scope.scrollTopBt = false;
		
//		$scope.TPs = data.TPs;
//		$scope.countryInfo = data.nationsInfo;
		$scope.ajaxGetTpList();
	}
	
	$scope.ajaxGetTpList = function() {
		console.log("ajaxGetTpList");
		$http.post( "/trans/ajaxGetTpList/").success( function( response ) {
			if ( response.isSuccess == 1 ) {
				console.log(response.TPs);
				$scope.start = 15;
				$scope.TPs = response.TPs;
				$scope.countryInfo = response.nationsInfo;
				$scope.scrollAction = false;
			}
		});
	}

	$scope.nativeLanList = function() {
//		console.log("nativeLanList");
		if ($scope.userInfo.native1_id != null && $scope.userInfo.native1_id != 0) {
//			$scope.langIndex++;
			$scope.lanMakeList("native1_id", $scope.langIndex);
		}
		if ($scope.userInfo.native2_id != null && $scope.userInfo.native2_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("native2_id", $scope.langIndex);
		}
		if ($scope.userInfo.native3_id != null && $scope.userInfo.native3_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("native3_id", $scope.langIndex);
		}
	}
	$scope.learningLanList = function() {
		if ($scope.userInfo.lang1_id != null && $scope.userInfo.lang1_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang1_id", $scope.langIndex);
		}
		if ($scope.userInfo.lang2_id != null && $scope.userInfo.lang2_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang2_id", $scope.langIndex);
		}
		if ($scope.userInfo.lang3_id != null && $scope.userInfo.lang3_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang3_id", $scope.langIndex);
		}
		if ($scope.userInfo.lang4_id != null && $scope.userInfo.lang4_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang4_id", $scope.langIndex);
		}
		if ($scope.userInfo.lang5_id != null && $scope.userInfo.lang5_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang5_id", $scope.langIndex);
		}
		if ($scope.userInfo.lang6_id != null && $scope.userInfo.lang6_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang6_id", $scope.langIndex);
		}
	}
//	$scope.otherLanList = function() {
//		if ($scope.userInfo.other1 != null && $scope.userInfo.other1 != 0) {
//			$scope.langIndex++;
//			$scope.lanMakeList("other1", $scope.langIndex);
//		}
//		if ($scope.userInfo.other2 != null && $scope.userInfo.other2 != 0) {
//			$scope.langIndex++;
//			$scope.lanMakeList("other2", $scope.langIndex);
//		}
//		if ($scope.userInfo.other3 != null && $scope.userInfo.other3 != 0) {
//			$scope.langIndex++;
//			$scope.lanMakeList("other3", $scope.langIndex);
//		}
//	}	
	$scope.lanMakeList = function(name, index) {
//		console.log(name);
//		console.log($scope.userInfo[name]);
//		console.log(index);
		
		$scope.languageInfoForFrom.splice(index, 0, {"id": $scope.languageInfo[$scope.userInfo[name]-1].id, "lang_english": $scope.languageInfo[$scope.userInfo[name]-1].lang_english});
		$scope.languageInfoForTo.splice(index, 0, {"id": $scope.languageInfo[$scope.userInfo[name]-1].id, "lang_english": $scope.languageInfo[$scope.userInfo[name]-1].lang_english});
		$scope.languageInfoForIn.splice(index, 0, {"id": $scope.languageInfo[$scope.userInfo[name]-1].id, "lang_english": $scope.languageInfo[$scope.userInfo[name]-1].lang_english});
	}

	$(window).scroll(function(){
	    var position = $scope.getScrollLocation();
	    if (position.Y > 200)
	    	$scope.scrollTopBt = true;
	    else
	    	$scope.scrollTopBt = false;
	});

	$scope.getScrollLocation = function(){
		var position = document.documentElement;
		var body = document.body;
		var now = {};
		now.X = document.all ?(!position.scrollLeft ? body.scrollLeft : position.scrollLeft) : (window.pageXOffset ? window.pageXOffset:window.scrollX);
		now.Y = document.all ?(!position.scrollTop ? body.scrollTop : position.scrollTop) : (window.pageYOffset ? window.pageYOffset:window.scrollY);

		return now;
	}

	$scope.moveScrollTop = function(){
		$(document).scrollTop(0);
	}

	$scope.myContents = function() {
		console.log("myContents");
		$http.post( "/trans/mytp/" ).success( function( response ) {
			if ( response.isSuccess == 1 ) {
				$scope.TPs=response.TPs;
			}
			console.log($scope.TPs);
		});
	}

	$scope.share = function($event){
		var target = $event.target;
		var top = $(target).offset().top + 25 ;
		var left = $(target).offset().left;
		$("#share").css('left', left);
		$("#share").css('top', top);
		$("#share").css('display', 'block');
	}
	$scope.showSharePopup = function(url, width, height){
		$window.open(url, 'title', "toolbar=no, scrollbars=no, resizeable=yes, width="+width+", height="+height);
		return false;
	}
	$scope.goURL = function(url){
		$window.location.href = (url);
		event.stopPropagation();
	}
	$scope.copyURL = function(url){
		clipboard.copyText(url);
		alert("Copy URL to clipboard.");
	}
	$scope.close = function(e){
		if( e.target.id != 'show_pop'){
			$("#share").css('display', 'none');
		}
	}
	$scope.getBoth =function( field ){
		if (("fromLang" == field) && $scope.fromLang != 0){
			if ($scope.fromLang == $scope.langIndex + 1) {
				$scope.fromLang = 0;
//				alert("Please select language.");
				return;
			}
			if ($scope.fromLang == $scope.toLang) {
				$scope.fromLang = 0;
				alert("Language duplicated.");
			}
		} else if ("toLang" == field && $scope.toLang != 0) {
			if ($scope.toLang == $scope.langIndex + 1) {
				$scope.toLang = 0;
//				alert("Please select language.");
				return;
			}
			if ($scope.fromLang == $scope.toLang) {
				$scope.toLang = 0;
				alert("Language duplicated.");
			}
		} else if ("inLang" == field && $scope.inLang != 0) {
			if ($scope.inLang == $scope.langIndex + 1) {
				$scope.inLang = 0;
//				alert("Please select language.");
				return;
			}
		}
		
//		if ($scope.fromLang == $scope.toLang){
//			alert("Language duplicated");
//			$scope.fromLang = 0;
//			$scope.toLang = 0;
//			return;
//		}
		if (!($scope.fromLang == $scope.langIndex + 1 || $scope.toLang == $scope.langIndex + 1 || $scope.inLang == $scope.langIndex + 1)) {
			$scope.checkedTPRadio = 0;
			$http.post( "/trans/both/",{fromLang : $scope.fromLang, toLang : $scope.toLang, inLang : $scope.inLang, start: $scope.startForPaging}).success( function( response ) {
				if ( response.isSuccess == 1 ) {
					$scope.TPs=response.TPs;
					console.log($scope.TPs);
				}
			});
		}
	}
	$scope.getTranslation = function( field ){
		if (("fromLang" == field) && $scope.fromLang != 0){
			if ($scope.fromLang == $scope.langIndex + 1) {
				$scope.fromLang = 0;
				alert("Please select language.");
				return;
			}
			if ($scope.fromLang == $scope.toLang) {
				$scope.fromLang = 0;
				alert("Language duplicated.");
			}
		} else if ("toLang" == field && $scope.toLang != 0) {
			if ($scope.toLang == $scope.langIndex + 1) {
				$scope.toLang = 0;
				alert("Please select language.");
				return;
			}
			if ($scope.fromLang == $scope.toLang) {
				$scope.toLang = 0;
				alert("Language duplicated.");
			}
		}

		if (!($scope.fromLang == $scope.langIndex + 1 || $scope.toLang == $scope.langIndex + 1)) {
			$scope.checkedTPRadio = 1;
			$http.post( "/trans/translation/",{type : 'T', fromLang : $scope.fromLang, toLang : $scope.toLang, start: $scope.startForPaging}).success( function( response ) {
				if ( response.isSuccess == 1 ) {
					$scope.TPs=response.TPs;
					console.log($scope.TPs);
				}
			});
		}
	}
	$scope.getProofreading = function( field ){
		if ("inLang" == field && $scope.inLang != 0) {
			if ($scope.inLang == $scope.langIndex + 1) {
				$scope.inLang = 0;
				alert("Please select language.");
				return;
			}
		}

		if (!($scope.inLang == $scope.langIndex + 1)) {
			$scope.checkedTPRadio = 2;
			$http.post( "/trans/proofreading/",{type :'P', inLang : $scope.inLang, start: $scope.startForPaging}).success( function( response ) {
				if ( response.isSuccess == 1 ) {
					$scope.TPs=response.TPs;
					console.log($scope.TPs);
				}
			});
		}
	}
	$scope.vote = function($index, ref_id){
		$http.post("/questions/vote/", {type : "Q", ref_id : ref_id}).success(function(response){
			if(response.isSuccess){
				$scope.LQs[$index].like = response.like;
				$scope.LQs[$index].like_count = response.like_count;
			}
		});
	};
	$scope.pinIt = function($index, qid){
		$http.post("/questions/pin/", {qid : qid}).success(function(response){
			if(response.isSuccess){
				$scope.LQs[$index].pin_count = response.pin_count;
				$scope.LQs[$index].pin = response.pin;
			}
		});
	}
	$scope.showReportPopup = function(){
		console.log("here");
		$scope.reportPopup = true;
		setTimeout(function() {
			$(".report-textarea").focus();
		}, 0);
	}
	$scope.getMoreList = function(){
		if (false == $scope.scrollAction) {
			$scope.scrollAction = true;
			$http.post( "/trans/moreTPList/",{start : $scope.start}).success( function( response ) {
				if ( response.isSuccess == 1 ) {
					console.log(response);
					if( response.TPs.length != 0){
						$scope.start += 15;
						$scope.scrollAction = false;
					}
					$scope.TPs = $scope.TPs.concat(response.TPs);
				}
			});	
		}
	}
});


