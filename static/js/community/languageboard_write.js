tellpinApp.controller( "lbWriteController", function($scope, $http, $location, $window, $sce, $timeout) {
	$scope.initLbWrite = function(data){
		console.log(data);
		$scope.modify = false;
//		$scope.lanIndex = 1;
		$scope.learningLanIndex = 0;
		$scope.explainedLanIndex = 0;
		$scope.userInfo = data.userInfo;
		$scope.languageInfo = data.languageInfo;
		$scope.languageInfoForLearning = angular.copy($scope.languageInfo);
		$scope.languageInfoForExplained = angular.copy($scope.languageInfo);
//		console.log($scope.userInfo.learning1);
//		console.log($scope.userInfo.native1_id);
		
		if ($scope.userInfo.lang1_id != null && $scope.userInfo.lang1_id != 0) {
//			$scope.languageInfoForLearning.splice(0, 0, {"id": 0, "lang_english": " Select Language"});
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": 0, "lang_english": $scope.languageInfo[$scope.userInfo.lang1_id -1].lang_english});
		} else {
			$scope.languageInfoForLearning.splice(0, 0, {"id": 0, "lang_english": " All languages I can speak"});
		}
		console.log($scope.languageInfoForLearning);
		if ($scope.userInfo.native1_id != null && $scope.userInfo.native1_id != 0) {
//			$scope.languageInfoForExplained.splice(0, 0, {"id": 0, "lang_english": " Select Language"});
			$scope.languageInfoForExplained.splice(0, 0, {"id": 0, "lang_english": $scope.languageInfo[$scope.userInfo.native1_id -1].lang_english});
		} else {
			$scope.languageInfoForExplained.splice(0, 0, {"id": 0, "lang_english": " All languages I can speak"});
		}
		
		$scope.makeLearningLanList();
		$scope.makeExplainedLanList();

		$scope.learning_lang_id = 0;
		$scope.explained_lang_id = 0;
		$scope.learning_lang_name = $scope.languageInfo[$scope.userInfo.lang1_id-1].lang_english;
		$scope.explained_lang_name = $scope.languageInfo[$scope.userInfo.native1_id-1].lang_english;

		$scope.languageInfoForLearning.splice($scope.learningLanIndex + 1, 0, {"id": $scope.learningLanIndex + 1, "lang_english": "-----------------------------------------"});
		$scope.languageInfoForExplained.splice($scope.explainedLanIndex + 1, 0, {"id": $scope.explainedLanIndex + 1, "lang_english": "-----------------------------------------"});

		$scope.blocks = [];
		$scope.createBlock();
		$scope.videoPopup = -1;
		$scope.filePopup = -1;
		$scope.urlPopup = -1;
		$scope.submit_empty_learning_select = false;
		$scope.submit_empty_explained_select = false;
		$scope.boardTitle = "";
		
		if (data.post != undefined) {
			console.log(data.post);
			$scope.boardTitle = data.post.title;
			$scope.blocks[0].content = data.post.contents;
			$scope.id = data.post.id;
			$scope.explained_lang_id = data.post.explained_lang;
			$scope.explained_lang_name = data.post.explained_lang_name;
			$scope.learning_lang_id = data.post.learning_lang;
			$scope.learning_lang_name = data.post.learning_lang_name;
			$scope.img_fid = data.post.main_img_fid;
			$scope.modify = true;
		}
		
		//medium-editor button("anchor") redraw
		anchorRedraw();
	}
	
	$scope.makeLearningLanList = function() {
//		console.log("$scope.makeLearningLanList");
		console.log($scope.userInfo);
//		if ($scope.userInfo.lang1_id != null && $scope.userInfo.lang1_id != 0) {
//			$scope.learningLanIndex++;
//			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.lang1_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang1_id-1].lang_english});
//		}
		if ($scope.userInfo.lang2_id != null && $scope.userInfo.lang2_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.lang2_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang2_id-1].lang_english});
		}
		if ($scope.userInfo.lang3_id != null && $scope.userInfo.lang3_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.lang3_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang3_id-1].lang_english});
		}
		if ($scope.userInfo.lang4_id != null && $scope.userInfo.lang4_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.lang4_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang4_id-1].lang_english});
		}
		if ($scope.userInfo.lang5_id != null && $scope.userInfo.lang5_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.lang5_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang5_id-1].lang_english});
		}
		if ($scope.userInfo.lang6_id != null && $scope.userInfo.lang6_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.lang6_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang6_id-1].lang_english});
		}
		if ($scope.userInfo.native1_id != null && $scope.userInfo.native1_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.native1_id, "lang_english": $scope.languageInfo[$scope.userInfo.native1_id-1].lang_english});
		}
		if ($scope.userInfo.native2_id != null && $scope.userInfo.native2_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.native2_id, "lang_english": $scope.languageInfo[$scope.userInfo.native2_id-1].lang_english});
		}
		if ($scope.userInfo.native3_id != null && $scope.userInfo.native3_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.native3_id, "lang_english": $scope.languageInfo[$scope.userInfo.native3_id-1].lang_english});
		}
	}

	$scope.makeExplainedLanList = function() {
//		console.log("$scope.makeExplainedLanList");
//		if ($scope.userInfo.native1_id != null && $scope.userInfo.native1_id != 0) {
//			$scope.explainedLanIndex++;
//			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.native1_id, "lang_english": $scope.languageInfo[$scope.userInfo.native1_id-1].lang_english});
//		}
		if ($scope.userInfo.native2_id != null && $scope.userInfo.native2_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.native2_id, "lang_english": $scope.languageInfo[$scope.userInfo.native2_id-1].lang_english});
		}
		if ($scope.userInfo.native3_id != null && $scope.userInfo.native3_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.native3_id, "lang_english": $scope.languageInfo[$scope.userInfo.native3_id-1].lang_english});
		}
		if ($scope.userInfo.lang1_id != null && $scope.userInfo.lang1_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.lang1_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang1_id-1].lang_english});
		}
		if ($scope.userInfo.lang2_id != null && $scope.userInfo.lang2_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.lang2_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang2_id-1].lang_english});
		}
		if ($scope.userInfo.lang3_id != null && $scope.userInfo.lang3_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.lang3_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang3_id-1].lang_english});
		}
		if ($scope.userInfo.lang4_id != null && $scope.userInfo.lang4_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.lang4_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang4_id-1].lang_english});
		}
		if ($scope.userInfo.lang5_id != null && $scope.userInfo.lang5_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.lang5_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang5_id-1].lang_english});
		}
		if ($scope.userInfo.lang6_id != null && $scope.userInfo.lang6_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.lang6_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang6_id-1].lang_english});
		}
	}
	
	//medium-editor
	$scope.mediumBindOptions = {
			toolbar: {
				buttons: ['bold', 'italic', 'underline', 'anchor', 'h1', 'h2']
			}
	};
	
	$scope.showData = function(){
		console.log($scope.blocks);
	}

	$scope.updatePost = function(){
		if ($scope.learning_lang_id == 0) {
			$scope.learning_lang_id = $scope.userInfo.lang1_id;
		}
		if($scope.learning_lang_id == $scope.lanIndex + 1){
			$scope.submit_empty_learning_select = true;
			$(document).scrollTop(0);
			return;
//			return alert("Please select one.");
		}
		if ($scope.explained_lang_id == 0) {
			$scope.explained_lang_id = $scope.userInfo.native1_id;
		}
		if($scope.explained_lang_id == $scope.lanIndex + 1){
			$scope.submit_empty_explained_select = true;
			$(document).scrollTop(0);
			return;
//			return alert("Please select explained.");
		}
		if($scope.boardTitle == '' || $scope.boardTitle == null){
			$(document).scrollTop(0);
			document.getElementById("title").focus();
			alert("Please fill in the title.");
			return;
		}
		var content = "";
		var img_fid = 0;
		var fid = "";
		console.log("$scope.blocks[0].fid");
		console.log($scope.blocks[0].fid);

		if($scope.blocks[0].fid == undefined) {
			img_fid = $scope.img_fid;
		} else {
			img_fid = $scope.blocks[0].fid;	
		}

		for(var index = 0 ; index < $scope.blocks.length ; index ++){
			if($scope.blocks[index].type != 0){
				if($scope.blocks[index].type == 1){
					if(img_fid == 0){
						if ($scope.img_fid != $scope.blocks[index].fid) {
							img_fid = $scope.blocks[index].fid;
						}
						else {
							img_fid = $scope.img_fid;
						}
					}
					else{
						fid += $scope.blocks[index].fid + ",";
					}
					content += "<div class='media-block'><img src='" + $scope.blocks[index].src + "'></div>";
				}
				else if($scope.blocks[index].type == 2){
					if(img_fid == 0){
						img_fid = $scope.blocks[index].fid;
					}
					else{
						fid += $scope.blocks[index].fid + ",";
					}
					content += "<div class='media-block'><iframe width='100%' height='600px' src='"+ $scope.youtubeRegex( $scope.blocks[index].src )+ "' frameborder='0' allowfullscreen></iframe></div>";
				}
			}
			content += $scope.blocks[index].content;
//			if ($scope.img_fid == 0 && $scope.img_fid != $scope.blocks[index].fid){
//				img_fid = $scope.blocks[index].fid;
//			}
		}

		fid = fid.slice(0, fid.length-1);
//		if (fid == "") {
//			fid = "NULL";
//		}
		$http.post( "/board/updatepost/",{id : $scope.id, learning_lang_id : $scope.learning_lang_id, learning_lang_name : $scope.learning_lang_name, explained_lang_id : $scope.explained_lang_id, explained_lang_name : $scope.explained_lang_name, title : $scope.boardTitle, content: content, img_fid : img_fid, fid: fid}).success( function( response ) {
			if ( response.isSuccess == 1 ) {
				$window.location.href = "/board/"+response.pid+"/";
//				$scope.modify = false;
//				alert("Success");
			}
		});
	}
	
	$scope.changeLb = function( field ){
		console.log(field);
		console.log($scope.learning_lang_id);
		if ($scope.submit_empty_learning_select == true )
			$scope.submit_empty_learning_select = false;
		else if ($scope.submit_empty_explained_select == true)
			$scope.submit_empty_explained_select = false;
		if ("learning_lang_id" == field){
			$scope.learning_lang_name = $scope.languageInfo[$scope.learning_lang_id-1].lang_english;
			if($scope.learning_lang_id == $scope.lanIndex + 1){
				$scope.learning_lang_id = 0;
				$scope.learning_lang_name = "";
//				alert("Please select language.");
				return;
			}
		} else if ("explained_lang_id" == field) {
			$scope.explained_lang_name = $scope.languageInfo[$scope.explained_lang_id-1].lang_english;
			if($scope.explained_lang_id == $scope.lanIndex + 1){
				$scope.explained_lang_id = 0;
				$scope.explained_lang_name = "";
//				alert("Please select language.");
				return;
			}
		}
	}

	$scope.submitPost = function(){
		if ($scope.learning_lang_id == 0) {
			$scope.learning_lang_id = $scope.userInfo.lang1_id;
		}

		if($scope.learning_lang_id == $scope.lanIndex + 1){
			$scope.submit_empty_learning_select = true;
			$(document).scrollTop(0);
			return;
//			return alert("Please select one.");
		}

		if($scope.explained_lang_id == 0) {
			$scope.explained_lang_id = $scope.userInfo.native1_id;
		}
		if($scope.explained_lang_id == $scope.lanIndex + 1){
			$scope.submit_empty_explained_select = true;
			$(document).scrollTop(0);
			return;
//			return alert("Please select one.");
		}
		if($scope.boardTitle == '' || $scope.boardTitle == null){
			$(document).scrollTop(0);
			document.getElementById("title").focus();
			alert("Please fill in the title.");
			return;
		}
		var content = "";
		var img_fid = 0;
		var fid = "";
		for(var index = 0 ; index < $scope.blocks.length ; index ++){
			if($scope.blocks[index].type != 0){
				if($scope.blocks[index].type == 1){
					if(img_fid == 0){
						img_fid = $scope.blocks[index].fid;
					}
					else{
						fid += $scope.blocks[index].fid + ",";
					}
					content += "<div class='media-block'><img src='" + $scope.blocks[index].src + "'></div>";
				} else if($scope.blocks[index].type == 2){
					if(img_fid == 0){
						img_fid = $scope.blocks[index].fid;
					}
					else{
						fid += $scope.blocks[index].fid + ",";
					}
					content += "<div class='media-block'><iframe width='100%' height='600px' src='"+ $scope.youtubeRegex( $scope.blocks[index].src )+ "' frameborder='0' allowfullscreen></iframe></div>";
				}
			}
			content += $scope.blocks[index].content;
		}
		console.log(content);
		fid = fid.slice(0, fid.length-1);
//		if (fid == "") {
//			fid = "NULL";
//		}
		$http.post( "/board/post/",{learning_lang_id : $scope.learning_lang_id, learning_lang_name : $scope.learning_lang_name, explained_lang_id : $scope.explained_lang_id, explained_lang_name : $scope.explained_lang_name, title : $scope.boardTitle, content: content, img_fid : img_fid, fid: fid}).success( function( response ) {
			if ( response.isSuccess == 1 ) {
				$window.location.href = "/board/"+response.pid+"/";
//				alert("Success");
			}
		});
	}
	$scope.showTools = function($index){
		if($scope.blocks[$index].mediaTools){
			$scope.blocks[$index].mediaTools = false;
		}
		else{
			$scope.blocks[$index].mediaTools = true;
		}
	}
	$scope.showUrlPopup = function() {
		console.log("showImagePopup");
		$scope.urlPopup = $scope.filePopup;
	}
	$scope.showFilePicker = function($index){
		console.log("showFilePicker $index = " + $index);
		$scope.filePopup = $index;
	}
	$scope.localFilePicker = function(){
		console.log("localFilePicker : filePopup = " + $scope.filePopup);
		$("#img-tool-"+ $scope.filePopup).click();
	}
	$scope.submitImgFile = function(files, $index){
		console.log("submitImgFile $index = " + $index);
		console.log(files);
		$scope.files = files;
		$scope.backdrop = false;
		$scope.filePopup = -1;
		var formData = new FormData();
		formData.append("media",files[0]);
		$http.post("/board/write/upload_media/", formData,{
			withCredentials: true,
	        headers: {'Content-Type': undefined },
	        transformRequest: angular.identity
		}).success(function(response){
			console.log(response);
			console.log($scope.blocks);
			$scope.blocks[$index].type = 1;
			$scope.blocks[$index].fid = response.fid;
			$scope.blocks[$index].src = response.path;
//			var img = document.getElementById('upload-img');
		});
	}
	$scope.removeImg = function($index) {
		console.log("removeImg $index = " + $index);
		console.log("$scope.filePopup = " + $scope.filePopup);
		document.getElementById('img-tool-'+ $index).value = "";
		$scope.blocks[$index].mediaTools = false;
		$scope.blocks[$index].type = 0;
		$scope.blocks[$index].fid = null;
		$scope.blocks[$index].src = null;
//		console.log($scope.blocks);
	}
	$scope.removeVideo = function($index) {
		console.log("$index = " + $index);
		$scope.blocks[$index].mediaTools = false;
		$scope.blocks[$index].type = 0;
		$scope.blocks[$index].src = "";
		$scope.blocks[$index].fid = null;
	}
	$scope.showVidoPicker = function($index){
		console.log("showVidoPicker = " + $index);
		$scope.videoPopup = $index;
	}
	$scope.createBlock= function(){
		$scope.blocks.push({"type": 0, "content": "", "mediaTools": false});
	}
	$scope.addBlock = function($index){
		console.log($scope.blocks);
		console.log($index);
		$scope.createBlock();
		setTimeout(function() { //			textarea focus on
			$(".editor-block-"+ ($scope.blocks.length-1)).focus();
		}, 0);
	}
	$scope.removeBlock = function($index) {
		console.log("removeBlock");	
		$scope.blocks.splice($index, $index);
//		$scope.blocks[$index-1].focus();
	}
	$scope.submitImg = function(){
		console.log("$scope.submitImg");
		$scope.blocks[$scope.filePopup].type = 1;
		$scope.blocks[$scope.filePopup].src = $scope.imgURL;

		$http.post("/board/write/upload_video_preview/", {url: $scope.imgURL}).success(function(response){
			console.log(response);
			$scope.blocks[$scope.filePopup].fid = response.fid;
			$scope.filePopup = -1;
			$scope.urlPopup = -1;
			$scope.imgURL = "";
		});
	}
	$scope.submitVideo = function(){
		$scope.blocks[$scope.videoPopup].type = 2;
		$scope.blocks[$scope.videoPopup].src = $scope.videoURL;
		$http.post("/board/write/upload_video_preview/", {url: $scope.youtubePreviewImg($scope.videoURL)}).success(function(response){
			$scope.blocks[$scope.videoPopup].fid = response.fid;
			$scope.videoPopup = -1;
			$scope.videoURL = "";
		});
	}
	$scope.youtubeRegex = function( url ) {
		var regExp = /(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|playlist\?|watch\?v=|watch\?.+(?:&|&#38;);v=))([a-zA-Z0-9\-_]{11})?(?:(?:\?|&|&#38;)index=((?:\d){1,3}))?(?:(?:\?|&|&#38;)?list=([a-zA-Z\-_0-9]{34}))?(?:\S+)?/g;
		var result = regExp.exec( url );
		var src = "https://www.youtube.com/embed/" + result[1] + "?wmode=opaque";
		return $sce.trustAsResourceUrl( src );
	}
	$scope.youtubePreviewImg = function(url){
		var regExp = /(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|playlist\?|watch\?v=|watch\?.+(?:&|&#38;);v=))([a-zA-Z0-9\-_]{11})?(?:(?:\?|&|&#38;)index=((?:\d){1,3}))?(?:(?:\?|&|&#38;)?list=([a-zA-Z\-_0-9]{34}))?(?:\S+)?/g;
		var result = regExp.exec( url );
		var src = "http://img.youtube.com/vi/" + result[1] + "/hqdefault.jpg";
		return src;
	}
	$scope.goBack = function(){
//		console.log($scope.blocks[0].content);
//		console.log($scope.boardTitle);
		if (($scope.boardTitle == "" || $scope.boardTitle == undefined) && ($scope.blocks[0].content == "<p></p>")) {
			$window.history.back();
		} else {
			if(confirm("If you leave before saving, your changes will be lost. \nAre you sure want to leave?")){
				$window.history.back();
			}
		}
	}
	$scope.hideVideoPopup = function(){
		$scope.videoPopup = -1;
		$scope.videoURL = "";
	}
	
	$scope.hidefilePopup = function(){
		$scope.filePopup = -1;
//		$scope.videoURL = "";
	}
	$scope.hideurlPopup = function(){
		$scope.urlPopup = -1;
		$scope.filePopup = -1;
		$scope.imgURL = "";
	}
});






/* Anchor Form Options Redraw*/
function anchorRedraw() {
    'use strict';

    var AnchorForm = MediumEditor.extensions.form.extend({
        /* Anchor Form Options */

        /* customClassOption: [string]  (previously options.anchorButton + options.anchorButtonClass)
         * Custom class name the user can optionally have added to their created links (ie 'button').
         * If passed as a non-empty string, a checkbox will be displayed allowing the user to choose
         * whether to have the class added to the created link or not.
         */
        customClassOption: null,

        /* customClassOptionText: [string]
         * text to be shown in the checkbox when the __customClassOption__ is being used.
         */
        customClassOptionText: 'Button',

        /* linkValidation: [boolean]  (previously options.checkLinkFormat)
         * enables/disables check for common URL protocols on anchor links.
         */
        linkValidation: false,

        /* placeholderText: [string]  (previously options.anchorInputPlaceholder)
         * text to be shown as placeholder of the anchor input.
         */
        placeholderText: 'Paste or type a link',

        /* targetCheckbox: [boolean]  (previously options.anchorTarget)
         * enables/disables displaying a "Open in new window" checkbox, which when checked
         * changes the `target` attribute of the created link.
         */
        targetCheckbox: false,

        /* targetCheckboxText: [string]  (previously options.anchorInputCheckboxLabel)
         * text to be shown in the checkbox enabled via the __targetCheckbox__ option.
         */
        targetCheckboxText: 'Open in new window',

        // Options for the Button base class
        name: 'anchor',
        action: 'createLink',
        aria: 'link',
        tagNames: ['a'],
        contentDefault: '<b><img style="width:13px;height:13px;opacity:0.6;" src="/static/img/community/button_link.png"></b>',
        contentFA: '<i class="fa fa-link"></i>',

        init: function () {
            MediumEditor.extensions.form.prototype.init.apply(this, arguments);

            this.subscribe('editableKeydown', this.handleKeydown.bind(this));
        },

        // Called when the button the toolbar is clicked
        // Overrides ButtonExtension.handleClick
        handleClick: function (event) {
            event.preventDefault();
            event.stopPropagation();

            var range = MediumEditor.selection.getSelectionRange(this.document);

            if (range.startContainer.nodeName.toLowerCase() === 'a' ||
                range.endContainer.nodeName.toLowerCase() === 'a' ||
                MediumEditor.util.getClosestTag(MediumEditor.selection.getSelectedParentElement(range), 'a')) {
                return this.execAction('unlink');
            }

            if (!this.isDisplayed()) {
                this.showForm();
            }

            return false;
        },

        // Called when user hits the defined shortcut (CTRL / COMMAND + K)
        handleKeydown: function (event) {
            if (MediumEditor.util.isKey(event, MediumEditor.util.keyCode.K) && MediumEditor.util.isMetaCtrlKey(event) && !event.shiftKey) {
                this.handleClick(event);
            }
        },

        // Called by medium-editor to append form to the toolbar
        getForm: function () {
            if (!this.form) {
                this.form = this.createForm();
            }
            return this.form;
        },

        getTemplate: function () {
            var template = [
                '<input type="text" class="medium-editor-toolbar-input" placeholder="', this.placeholderText, '">'
            ];

            template.push(
                '<a href="#" class="medium-editor-toolbar-save">',
                this.getEditorOption('buttonLabels') === 'fontawesome' ? '<i class="fa fa-check"></i>' : this.formSaveLabel,
                '</a>'
            );

            template.push('<a href="#" class="medium-editor-toolbar-close">',
                this.getEditorOption('buttonLabels') === 'fontawesome' ? '<i class="fa fa-times"></i>' : this.formCloseLabel,
                '</a>');

            // both of these options are slightly moot with the ability to
            // override the various form buildup/serialize functions.

            if (this.targetCheckbox) {
                // fixme: ideally, this targetCheckboxText would be a formLabel too,
                // figure out how to deprecate? also consider `fa-` icon default implcations.
                template.push(
                    '<div class="medium-editor-toolbar-form-row">',
                    '<input type="checkbox" class="medium-editor-toolbar-anchor-target">',
                    '<label>',
                    this.targetCheckboxText,
                    '</label>',
                    '</div>'
                );
            }

            if (this.customClassOption) {
                // fixme: expose this `Button` text as a formLabel property, too
                // and provide similar access to a `fa-` icon default.
                template.push(
                    '<div class="medium-editor-toolbar-form-row">',
                    '<input type="checkbox" class="medium-editor-toolbar-anchor-button">',
                    '<label>',
                    this.customClassOptionText,
                    '</label>',
                    '</div>'
                );
            }

            return template.join('');

        },

        // Used by medium-editor when the default toolbar is to be displayed
        isDisplayed: function () {
            return this.getForm().style.display === 'block';
        },

        hideForm: function () {
            this.getForm().style.display = 'none';
            this.getInput().value = '';
        },

        showForm: function (opts) {
            var input = this.getInput(),
                targetCheckbox = this.getAnchorTargetCheckbox(),
                buttonCheckbox = this.getAnchorButtonCheckbox();

            opts = opts || { url: '' };
            // TODO: This is for backwards compatability
            // We don't need to support the 'string' argument in 6.0.0
            if (typeof opts === 'string') {
                opts = {
                    url: opts
                };
            }

            this.base.saveSelection();
            this.hideToolbarDefaultActions();
            this.getForm().style.display = 'block';
            this.setToolbarPosition();

            input.value = opts.url;
            input.focus();

            // If we have a target checkbox, we want it to be checked/unchecked
            // based on whether the existing link has target=_blank
            if (targetCheckbox) {
                targetCheckbox.checked = opts.target === '_blank';
            }

            // If we have a custom class checkbox, we want it to be checked/unchecked
            // based on whether an existing link already has the class
            if (buttonCheckbox) {
                var classList = opts.buttonClass ? opts.buttonClass.split(' ') : [];
                buttonCheckbox.checked = (classList.indexOf(this.customClassOption) !== -1);
            }
        },

        // Called by core when tearing down medium-editor (destroy)
        destroy: function () {
            if (!this.form) {
                return false;
            }

            if (this.form.parentNode) {
                this.form.parentNode.removeChild(this.form);
            }

            delete this.form;
        },

        // core methods

        getFormOpts: function () {
            // no notion of private functions? wanted `_getFormOpts`
            var targetCheckbox = this.getAnchorTargetCheckbox(),
                buttonCheckbox = this.getAnchorButtonCheckbox(),
                opts = {
                    url: this.getInput().value
                };

            if (this.linkValidation) {
                opts.url = this.checkLinkFormat(opts.url);
            }

            opts.target = '_self';
            if (targetCheckbox && targetCheckbox.checked) {
                opts.target = '_blank';
            }

            if (buttonCheckbox && buttonCheckbox.checked) {
                opts.buttonClass = this.customClassOption;
            }

            return opts;
        },

        doFormSave: function () {
            var opts = this.getFormOpts();
            this.completeFormSave(opts);
        },

        completeFormSave: function (opts) {
            this.base.restoreSelection();
            this.execAction(this.action, opts);
            this.base.checkSelection();
        },

        checkLinkFormat: function (value) {
            var re = /^(https?|ftps?|rtmpt?):\/\/|mailto:/;
            return (re.test(value) ? '' : 'http://') + value;
        },

        doFormCancel: function () {
            this.base.restoreSelection();
            this.base.checkSelection();
        },

        // form creation and event handling
        attachFormEvents: function (form) {
            var close = form.querySelector('.medium-editor-toolbar-close'),
                save = form.querySelector('.medium-editor-toolbar-save'),
                input = form.querySelector('.medium-editor-toolbar-input');

            // Handle clicks on the form itself
            this.on(form, 'click', this.handleFormClick.bind(this));

            // Handle typing in the textbox
            this.on(input, 'keyup', this.handleTextboxKeyup.bind(this));

            // Handle close button clicks
            this.on(close, 'click', this.handleCloseClick.bind(this));

            // Handle save button clicks (capture)
            this.on(save, 'click', this.handleSaveClick.bind(this), true);

        },

        createForm: function () {
            var doc = this.document,
                form = doc.createElement('div');

            // Anchor Form (div)
            form.className = 'medium-editor-toolbar-form';
            form.id = 'medium-editor-toolbar-form-anchor-' + this.getEditorId();
            form.innerHTML = this.getTemplate();
            this.attachFormEvents(form);

            return form;
        },

        getInput: function () {
            return this.getForm().querySelector('input.medium-editor-toolbar-input');
        },

        getAnchorTargetCheckbox: function () {
            return this.getForm().querySelector('.medium-editor-toolbar-anchor-target');
        },

        getAnchorButtonCheckbox: function () {
            return this.getForm().querySelector('.medium-editor-toolbar-anchor-button');
        },

        handleTextboxKeyup: function (event) {
            // For ENTER -> create the anchor
            if (event.keyCode === MediumEditor.util.keyCode.ENTER) {
                event.preventDefault();
                this.doFormSave();
                return;
            }

            // For ESCAPE -> close the form
            if (event.keyCode === MediumEditor.util.keyCode.ESCAPE) {
                event.preventDefault();
                this.doFormCancel();
            }
        },

        handleFormClick: function (event) {
            // make sure not to hide form when clicking inside the form
            event.stopPropagation();
        },

        handleSaveClick: function (event) {
            // Clicking Save -> create the anchor
            event.preventDefault();
            this.doFormSave();
        },

        handleCloseClick: function (event) {
            // Click Close -> close the form
            event.preventDefault();
            this.doFormCancel();
        }
    });
    
    MediumEditor.extensions.anchor = AnchorForm;
}
