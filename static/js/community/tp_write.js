tellpinApp.controller( "tpWriteController", function($scope, $http, $location, $window, $sce, $timeout) {
	$scope.initWrite = function(data){
		console.log(data);
		$scope.modify = false;
		$scope.editText = false;

		$scope.languageInfo = data.languageInfo;
		$scope.languageInfoTrans = angular.copy($scope.languageInfo);
		$scope.coin = data.availablecoin;
		$scope.learningLanIndex = 0;
		$scope.explainedLanIndex = 0;
		$scope.languageInfoForLearning = angular.copy($scope.languageInfo);
		$scope.languageInfoForExplained = angular.copy($scope.languageInfo);
//		$scope.writtenLanInfo = JSON.parse( JSON.stringify( data.languageInfo ) );
//		$scope.transLanInfo = JSON.parse( JSON.stringify( data.languageInfo ) );

		$scope.bestanswercreditInfo = data.bestanswercreditInfo;
		$scope.userInfo = data.userInfo;

//		$scope.languageInfoTrans.splice(0, 0, {"id": 0, "name": "Select language"});
		if ($scope.userInfo.lang1_id != null && $scope.userInfo.lang1_id != 0) {
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": 0, "lang_english": $scope.languageInfo[$scope.userInfo.lang1_id -1].lang_english});
		} else {
			$scope.languageInfoForLearning.splice($scope.explainedLanIndex, 0, {"id": 0, "lang_english": " All languages I can speak"});
		}
		
		if ($scope.userInfo.native1_id != null && $scope.userInfo.native1_id != 0) {
			$scope.languageInfoForExplained.splice(0, 0, {"id": 0, "lang_english": $scope.languageInfo[$scope.userInfo.native1_id -1].lang_english});
		} else {
			$scope.languageInfoForExplained.splice(0, 0, {"id": 0, "lang_english": " All languages I can speak"});
		}
		
		$scope.makeLearningLanList();
		$scope.makeExplainedLanList();
		
		$scope.writtenLang_id = 0;
		$scope.translatedLang_id = 0;
		$scope.writtenLang_name = $scope.languageInfo[$scope.userInfo.lang1_id-1].lang_english;
		$scope.translatedLang_name = $scope.languageInfo[$scope.userInfo.native1_id-1].lang_english;

		$scope.selectedType = 0;

		$scope.languageInfoForLearning.splice($scope.learningLanIndex + 1, 0, {"id": $scope.learningLanIndex + 1, "lang_english": "-----------------------------------------"});
		$scope.languageInfoForExplained.splice($scope.explainedLanIndex + 1, 0, {"id": $scope.explainedLanIndex + 1, "lang_english": "-----------------------------------------"});

		$scope.best_answer_credit=0;
		$scope.inputCoinCheck = true;
		$scope.maxLen = 0;
		$scope.title = "";
		$scope.fileCheck = false;

		if (data.data != undefined) {
			if (data.data.question.type == "P") {
				$scope.selectedType = 1;
			}
			$scope.title = data.data.question.title;
			$scope.writeContent = data.data.question.contents;
			$scope.id = data.data.question.id;
			$scope.translatedLang_id = data.data.question.translated_lang;
			$scope.writtenLang_id = data.data.question.written_lang;
			$scope.best_answer_credit = data.data.question.best_answer_credit;
			$scope.initPoint = data.data.question.best_answer_credit;
			$scope.modify = true;
		}
	};

	$scope.makeLearningLanList = function() {
//		console.log("$scope.makeLearningLanList");
		if ($scope.userInfo.lang2_id != null && $scope.userInfo.lang2_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.lang2_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang2_id-1].lang_english});
		}
		if ($scope.userInfo.lang3_id != null && $scope.userInfo.lang3_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.lang3_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang3_id-1].lang_english});
		}
		if ($scope.userInfo.lang4_id != null && $scope.userInfo.lang4_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.lang4_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang4_id-1].lang_english});
		}
		if ($scope.userInfo.lang5_id != null && $scope.userInfo.lang5_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.lang5_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang5_id-1].lang_english});
		}
		if ($scope.userInfo.lang6_id != null && $scope.userInfo.lang6_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.lang6_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang6_id-1].lang_english});
		}
		if ($scope.userInfo.native1_id != null && $scope.userInfo.native1_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.native1_id, "lang_english": $scope.languageInfo[$scope.userInfo.native1_id-1].lang_english});
		}
		if ($scope.userInfo.native2_id != null && $scope.userInfo.native2_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.native2_id, "lang_english": $scope.languageInfo[$scope.userInfo.native2_id-1].lang_english});
		}
		if ($scope.userInfo.native3_id != null && $scope.userInfo.native3_id != 0) {
			$scope.learningLanIndex++;
			$scope.languageInfoForLearning.splice($scope.learningLanIndex, 0, {"id": $scope.userInfo.native3_id, "lang_english": $scope.languageInfo[$scope.userInfo.native3_id-1].lang_english});
		}
	}
	
	$scope.makeExplainedLanList = function() {
//		console.log("$scope.makeExplainedLanList");
		if ($scope.userInfo.native2_id != null && $scope.userInfo.native2_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.native2_id, "lang_english": $scope.languageInfo[$scope.userInfo.native2_id-1].lang_english});
		}
		if ($scope.userInfo.native3_id != null && $scope.userInfo.native3_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.native3_id, "lang_english": $scope.languageInfo[$scope.userInfo.native3_id-1].lang_english});
		}
		if ($scope.userInfo.lang1_id != null && $scope.userInfo.lang1_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.lang1_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang1_id-1].lang_english});
		}
		if ($scope.userInfo.lang2_id != null && $scope.userInfo.lang2_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.lang2_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang2_id-1].lang_english});
		}
		if ($scope.userInfo.lang3_id != null && $scope.userInfo.lang3_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.lang3_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang3_id-1].lang_english});
		}
		if ($scope.userInfo.lang4_id != null && $scope.userInfo.lang4_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.lang4_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang4_id-1].lang_english});
		}
		if ($scope.userInfo.lang5_id != null && $scope.userInfo.lang5_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.lang5_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang5_id-1].lang_english});
		}
		if ($scope.userInfo.lang6_id != null && $scope.userInfo.lang6_id != 0) {
			$scope.explainedLanIndex++;
			$scope.languageInfoForExplained.splice($scope.explainedLanIndex, 0, {"id": $scope.userInfo.lang6_id, "lang_english": $scope.languageInfo[$scope.userInfo.lang6_id-1].lang_english});
		}
	}

//	$scope.nativeLanList = function() {
//		if ($scope.userInfo.native1 != null && $scope.userInfo.native1 != 0) {
//			$scope.lanMakeList("native1", $scope.lanIndex);
//		}
//		if ($scope.userInfo.native2 != null && $scope.userInfo.native2 != 0) {
//			$scope.lanIndex++;
//			$scope.lanMakeList("native2", $scope.lanIndex);
//		}
//		if ($scope.userInfo.native3 != null && $scope.userInfo.native3 != 0) {
//			$scope.lanIndex++;
//			$scope.lanMakeList("native3", $scope.lanIndex);
//		}
//	}
//	$scope.learningLanList = function() {
//		if ($scope.userInfo.learning1 != null && $scope.userInfo.learning1 != 0) {
//			$scope.lanIndex++;
//			$scope.lanMakeList("learning1", $scope.lanIndex);
//		}
//		if ($scope.userInfo.learning2 != null && $scope.userInfo.learning2 != 0) {
//			$scope.lanIndex++;
//			$scope.lanMakeList("learning2", $scope.lanIndex);
//		}
//		if ($scope.userInfo.learning3 != null && $scope.userInfo.learning3 != 0) {
//			$scope.lanIndex++;
//			$scope.lanMakeList("learning3", $scope.lanIndex);
//		}
//	}
//	$scope.otherLanList = function() {
//		if ($scope.userInfo.other1 != null && $scope.userInfo.other1 != 0) {
//			$scope.lanIndex++;
//			$scope.lanMakeList("other1", $scope.lanIndex);
//		}
//		if ($scope.userInfo.other2 != null && $scope.userInfo.other2 != 0) {
//			$scope.lanIndex++;
//			$scope.lanMakeList("other2", $scope.lanIndex);
//		}
//		if ($scope.userInfo.other3 != null && $scope.userInfo.other3 != 0) {
//			$scope.lanIndex++;
//			$scope.lanMakeList("other3", $scope.lanIndex);
//		}
//	}	
//	$scope.lanMakeList = function(name, index) {
//		$scope.languageInfoTrans.splice(index, 0, {"id": $scope.languageInfo[$scope.userInfo[name]-1].id, "lang_english": $scope.languageInfo[$scope.userInfo[name]-1].lang_english});
//	}

	$scope.selectLag = function(field){
		if ("write" == field){
			$scope.writtenLang_name = $scope.languageInfo[$scope.writtenLang_id-1].lang_english;
			if ($scope.writtenLang_id == $scope.lanIndex + 1){
				$scope.writtenLang_id = 0;
				$scope.writtenLang_name = "";
//				alert("Please select language.");
				return;
			}
			if ($scope.writtenLang_id == $scope.translatedLang_id) {
				$scope.writtenLang_id = 0;
				$scope.writtenLang_name = "";
				alert("Language duplicated");
				return;
			}
		} else if ("trans" == field){
			$scope.translatedLang_name = $scope.languageInfo[$scope.translatedLang_id-1].lang_english;
			if ($scope.translatedLang_id == $scope.lanIndex + 1){
				$scope.translatedLang_id = 0;
				$scope.translatedLang_name = "";
//				alert("Please select language.");
				return;
			}
			if ($scope.writtenLang_id == $scope.translatedLang_id) {
				$scope.translatedLang_id = 0;
				$scope.translatedLang_name = "";
				alert("Language duplicated");
				return;
			}
		}
	}

//	$scope.changewrittenLag = function() {
//		if ($scope.firstSelectlag == false) {
//			var index = $("#write-written-select").val();
//			console.log(index);
//			console.log(index.length);
//			index = index.split('number:');
//			console.log(index[1]);
//
//			$scope.firstSelectlag = true;
//			$scope.transLanInfo.splice(index[1], 1);
//			$scope.writtenLanInfo.splice(index[1], 0, {"id": index[1], "name": $scope.languageInfo[index[1]-1].name});
//			$scope.writtenLang_id = index[1];
//			$scope.translatedLang_id = 0;
//		}
//	}

	$scope.post = function(){
		if($scope.validation()){
			var data = new FormData();
			data.append("type", $scope.selectedType);
			data.append("title", $scope.title);
			data.append("content", tinymce.get("write-textarea").getContent());
			data.append("written_id", $scope.writtenLang_id);
			data.append("written_name", $scope.writtenLang_name);
			data.append("translated_id", $scope.translatedLang_id);
			data.append("translated_name", $scope.translatedLang_name);
			data.append("best_answer_credit", $scope.best_answer_credit);
			data.append("img", $scope.imgFile);
			
			$http.post( "/trans/post/", data, {
				withCredentials: true,
		        headers: {'Content-Type': undefined },
		        transformRequest: angular.identity
			}).success( function( response ) {
				if ( response.isSuccess == 1 ) {
					console.log(response);
					$window.location.href = (response.postURL);
				}
			});
		}
	}
	
	$scope.update = function(){
		if($scope.validation()){
			var data = new FormData();
			data.append("qid", $scope.id);
			data.append("type", $scope.selectedType);
			data.append("title", $scope.title);
			data.append("content", tinymce.get("write-textarea").getContent());
			data.append("written_id", $scope.writtenLang_id);
			data.append("written_name", $scope.writtenLang_name);
			data.append("translated_id", $scope.translatedLang_id);
			data.append("translated_name", $scope.translatedLang_name);
			data.append("best_answer_credit", $scope.best_answer_credit);
			data.append("initPoint", $scope.initPoint);
			data.append("img", $scope.imgFile);
			
			$http.post( "/trans/modify/", data, {
				withCredentials: true,
		        headers: {'Content-Type': undefined },
		        transformRequest: angular.identity
			}).success( function( response ) {
				if ( response.isSuccess == 1 ) {
					console.log(response);
					$window.location.href = (response.postURL);
				}
			});
		}
	}
	
	$scope.checkinputlen = function() {
//		console.log("checkinputlen");
		var string = tinymce.get("write-textarea").getContent()
//		console.log(string.length);
	}
	$scope.validation = function(){
		if($scope.title == "" || $scope.title == null){
			$(document).scrollTop(0);
			document.getElementById("title").focus();
			alert("Please fill in the title.");
			return false;
		}
		if(tinymce.get("write-textarea").getContent() == ""){
			alert("Please fill in the content.");
			return false;
		}
		if(getStats('write-textarea').chars > 1001 ){
//			console.log(tinymce.get("write-textarea").getContent().length);
//			console.log(getStats('write-textarea').chars);
			alert("characters limit!!!");
			return false;
		}
		if($scope.writtenLang_id == 0){
			$scope.writtenLang_id = $scope.userInfo.lang1_id;
//			alert("Please select one.");
//			return false;
		}
		if ($scope.translatedLang_id == 0) {
			$scope.translatedLang_id = $scope.userInfo.native1_id;
		}
//		if($scope.selectedType == 0){
//			alert("Please select one.");
//			return false;
//		}
		if($scope.best_answer_credit > 500) {
			alert("coin");
			return false;
		}
		return true;
	}
	$scope.getWriteFile = function(files){
//		console.log(files[0].type);
		var fileType = ["image/jpg", "image/jpeg", "image/png", "image/gif", "image/bmp"];
		var i = 0;

		for (i = 0; i < fileType.length; i++) {
			if (files[0].type == fileType[i]) {
				$scope.fileCheck = false;
				$scope.imgFile = files[0];
				break;
			}
		}
		if (i == fileType.length) {
			$scope.fileCheck = true;
			$scope.imgFile = null;
//			alert("Uploaded file is not valid image. Only JPG, JPEG, PNG, GIG and BMP files are allowed.");
			return;
		}
	}
	$scope.goBack = function(){
//		console.log($scope.title);
//		console.log(tinymce.get("write-textarea").getContent());
		if (($scope.title == "" || $scope.title == undefined) && (tinymce.get("write-textarea").getContent() == "")) {
			$window.history.back();
		} else {
			if(confirm("If you leave before saving, your changes will be lost. \nAre you sure want to leave?")){
				$window.history.back();
			}
		}
	}

	tinymce.init({
	    selector: "textarea#write-textarea",
	    plugins: [
	        "advlist autolink lists link charmap print preview anchor",
	        "searchreplace visualblocks code fullscreen",
	        "insertdatetime table contextmenu paste"
	    ],
	    menubar:false,
	    statusbar:false,
//	    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | preview",
	    toolbar: "insertfile undo redo | bold italic | alignleft aligncenter alignright | bullist numlist outdent indent | preview",
	    charLimit: 1000,
	        setup : function(ed) {
//	        	ed.on("change", function(e){
//	        		var tinymax, tinelen, htmlcount;
//	        		tinymax = this.settings.charLimit;
//	        		
//	                //grabbing the length of the curent editors content
//	                tinylen = $(ed.getBody()).text().length;
//	                $scope.maxLen = tinylen;
//	                console.log(tinylen);
//	                if (tinylen > tinymax) {
//	                	e.preventDefault();
//	                    e.stopPropagation();
//	                    return false;
//	                	tinymce.dom.Event.cancel(e);
////	                	alert("characters limit!!!");
//	                }
//	        	});
//	        	ed.on("KeyUp", function(e) {
//	        		var body = tinymce.get('write-textarea').getBody(), text = tinymce.trim(body.innerText || body.textContent);
//	        		if (this.settings.charLimit < text.length)
//	        			if(e.keyCode != 8 && e.keyCode != 46)
//	        				tinymce.dom.Event.cancel(e);
//	        		console.log(text.length);
//
//	        		if (this.settings.charLimit >= text.length)
//	        			$scope.maxLen = text.length;
//	        	});
	        	ed.on("KeyDown", function(e) {
	        		if ($scope.editText == false)
	        			$scope.editText = true;
	        			$scope.maxLen = getStats('write-textarea').chars;
	        	});
	        	ed.on("KeyUp", function(e) {
//	        		console.log(e.keyCode);
	        		$scope.maxLen = getStats('write-textarea').chars;
	        		if( !(e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 37 || e.keyCode == 39) ) {
	        			if ( $scope.maxLen > 999 ) {
	        				alert("characters limit!!!");
	        				tinymce.dom.Event.cancel(e);
	            			e.preventDefault();
	            			e.stopPropagation();
	            			return false;
	            		} 
	        		}
	        	});
	        }
	});

	function getStats(id) {
		
	    var body = tinymce.get(id).getBody(), text = tinymce.trim(body.innerText || body.textContent);

	    return {
	        chars: text.length,
	        words: text.split(/[\w\u2019\'-]+/).length,
	        string: text
	    };
	}

	$scope.inputCoinNumber = function(event) {
		e = event || window.event;
		if ( !((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode == 8) || (e.keyCode == 46) || (e.keyCode == 37) || (e.keyCode == 39)) ) {
			$scope.inputCoinCheck = false;
			window.event.returnValue = false;
			window.event.preventDefault();
		}
	}
	$scope.checkCoin = function() {
		if ($scope.best_answer_credit > 500 || $scope.best_answer_credit < 0) {
			$scope.inputCoinCheck = false;
		} else {
			$scope.inputCoinCheck = true;
		}
		
	}

	$scope.inputCoin = function(inputCoin) {
		console.log("inputCoin = " + inputCoin);
		console.log("$scope.coin = " + $scope.coin);
		return;
		if (inputCoin > $scope.coin){
			alert("no Balance");
			$scope.best_answer_credit = 0;
		}
	}
	
	
});



