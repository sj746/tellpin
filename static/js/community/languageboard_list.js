tellpinApp.controller( "lbListController", function($scope, $http, $window, $sce, clipboard) {
	$scope.initLbList = function(data){
		$scope.languageInfo = data.languageInfo;
		$scope.langIndex = 1;
		$scope.userInfo = data.userInfo;
		console.log(data);
		$scope.languageInfoForLearning = angular.copy($scope.languageInfo);
		$scope.languageInfoForExplained = angular.copy($scope.languageInfo);
		$scope.languageInfoForLearning.splice(0, 0, {"id": 0, "lang_english": " All languages I can speak"});
		$scope.languageInfoForExplained.splice(0, 0, {"id": 0, "lang_english": " All languages I can speak"});

		if( $scope.userInfo != undefined && $scope.userInfo != null) {
			$scope.nativeLanList();
			$scope.learningLanList();
		}
		
//		var langtype = ["native", "learning", "other1"]
//		for(var i = 0; i < 3; i++) {
//			for(var j = 1; j <= 3; j++) {
//				$scope.lanMakeList_nologin(1);
//			}
//			
//		}

		$scope.learning_lang = 0;
		$scope.explained_lang = 0;

		$scope.languageInfoForLearning.splice($scope.langIndex + 1, 0, {"id": $scope.langIndex + 1, "lang_english": "-----------------------------------------"});
		$scope.languageInfoForExplained.splice($scope.langIndex + 1, 0, {"id": $scope.langIndex + 1, "lang_english": "-----------------------------------------"});

		$scope.scrollTopBt = false;

		$scope.ajaxGetLbList();
	}
	
	$scope.ajaxGetLbList = function() {
		console.log("ajaxGetLbList");
		$http.post( "/board/ajaxGetLbList/").success( function( response ) {
			if ( response.isSuccess == 1 ) {
				console.log(response.boards);
				$scope.boards = response.boards;
				$scope.countryInfo = response.nationsInfo;
				$scope.start = 18;
				$scope.scrollAction = false;
			}
		});
	}

	$scope.nativeLanList = function() {
//		console.log("nativeLanList");
		if ($scope.userInfo.native1_id != null && $scope.userInfo.native1_id != 0) {
			$scope.lanMakeList("native1_id", $scope.langIndex);
		}
		if ($scope.userInfo.native2_id != null && $scope.userInfo.native2_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("native2_id", $scope.langIndex);
		}
		if ($scope.userInfo.native3_id != null && $scope.userInfo.native3_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("native3_id", $scope.langIndex);
		}
	}
	$scope.learningLanList = function() {
		if ($scope.userInfo.lang1_id != null && $scope.userInfo.lang1_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang1_id", $scope.langIndex);
		}
		if ($scope.userInfo.lang2_id != null && $scope.userInfo.lang2_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang2_id", $scope.langIndex);
		}
		if ($scope.userInfo.lang3_id != null && $scope.userInfo.lang3_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang3_id", $scope.langIndex);
		}
		if ($scope.userInfo.lang4_id != null && $scope.userInfo.lang4_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang4_id", $scope.langIndex);
		}
		if ($scope.userInfo.lang5_id != null && $scope.userInfo.lang5_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang5_id", $scope.langIndex);
		}
		if ($scope.userInfo.lang6_id != null && $scope.userInfo.lang6_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang6_id", $scope.langIndex);
		}
	}

	$scope.lanMakeList = function(name, index) {
//		console.log(name);
//		console.log($scope.userInfo[name]);
//		console.log(index);
		
		$scope.languageInfoForLearning.splice(index, 0, {"id": $scope.languageInfo[$scope.userInfo[name]-1].id, "lang_english": $scope.languageInfo[$scope.userInfo[name]-1].lang_english});
		$scope.languageInfoForExplained.splice(index, 0, {"id": $scope.languageInfo[$scope.userInfo[name]-1].id, "lang_english": $scope.languageInfo[$scope.userInfo[name]-1].lang_english});
	}
	
//	$scope.lanMakeList_nologin = function(index) {
//		$scope.languageInfoForLearning.splice(index, 0, {"id": $scope.languageInfo[0].id, "name": $scope.languageInfo[0].name});
//		$scope.languageInfoForExplained.splice(index, 0, {"id": $scope.languageInfo[0].id, "name": $scope.languageInfo[0].name});
//	}
	

	$scope.changeLb = function( field ){
		if (("learning" == field) && $scope.learning_lang != 0){
			if ($scope.learning_lang == $scope.langIndex + 1) {
				$scope.learning_lang = 0;
//				alert("Please select language.");
				return;
			}
//			if ($scope.learning_lang == $scope.explained_lang) {
//				$scope.learning_lang = 0;
//				alert("Language duplicated.");
//			}
		} else if ("explained" == field && $scope.explained_lang != 0) {
			if ($scope.explained_lang == $scope.langIndex + 1) {
				$scope.explained_lang = 0;
//				alert("Please select language.");
				return;
			}
		}

//		console.log($scope.langIndex);
		if (!($scope.learning_lang == $scope.langIndex + 1 || $scope.explained_lang == $scope.langIndex + 1)) {
			$scope.start = 18;
			$scope.searchWord = "";
			$http.post( "/board/change_lb/",{learning : $scope.learning_lang, explained : $scope.explained_lang, start : 0}).success( function( response ) {
				if ( response.isSuccess == 1 ) {
					$scope.boards = response.cards;
				}
			});
		}
	}

	$scope.myContents = function() {
		console.log("myContents");
		
		$http.post( "/board/mylb/" ,{learning : $scope.learning_lang, explained : $scope.explained_lang, start : 0}).success( function( response ) {
			if ( response.isSuccess == 1 ) {
				$scope.boards = response.cards;
			}
			console.log($scope.LQs);
		});
	}
	
	$(window).scroll(function(){
	    var position = $scope.getScrollLocation();
	    if (position.Y > 200)
	    	$scope.scrollTopBt = true;
	    else
	    	$scope.scrollTopBt = false;
	});

	$scope.getScrollLocation = function(){
		var position = document.documentElement;
		var body = document.body;
		var now = {};
		now.X = document.all ?(!position.scrollLeft ? body.scrollLeft : position.scrollLeft) : (window.pageXOffset ? window.pageXOffset:window.scrollX);
		now.Y = document.all ?(!position.scrollTop ? body.scrollTop : position.scrollTop) : (window.pageYOffset ? window.pageYOffset:window.scrollY);

		return now;
	}
	$scope.moveScrollTop = function(){
		$(document).scrollTop(0);
	}
	$scope.searchBoards = function(){
		$scope.start = 18;
		$http.post( "/board/search_boards/",{learning_lang : $scope.learning_lang, explained_lang : $scope.explained_lang, start : 0, search: $scope.searchWord}).success( function( response ) {
			if ( response.isSuccess == 1 ) {
				console.log(response.cards);
				$scope.boards = response.cards;
			}
		});
	}
	$scope.goURL = function(url){
		$window.location.href = (url);
	}
	$scope.pinLb = function(pid, $index){
		$http.post("/board/pin/", {pid : pid}).success(function(response){
			if(response.isSuccess){
				$scope.boards[$index].pin_count = response.pin_count;
//				$scope.boards[$index].pin = response.pin;
			}
		});
	}
	$scope.voteLb = function(pid, $index){
		
		$http.post("/board/vote/", {ref_id : pid}).success(function(response){
			if(response.isSuccess){
//				$scope.boards[$index].vote = response.like;
				$scope.boards[$index].vote_count = response.like_count;
			}
		});
	}
	$scope.showShareMenu = function($event, pid){
		console.log(pid);
		$scope.lbURL = pid +"/";
		var target = angular.element($event.target);
		var top = $(target).offset().top + 26;
		var left = $(target).offset().left;
		$("#share-tool-box").css({"top": top, "left": left});
		$scope.shareMenu = true;
	};
	$scope.hideShareMenu = function(e){
		if( e.target.id != 'share-button'){
			$scope.shareMenu = false;
		}
	}
	$scope.showSharePopup = function(url, width, height){
		$window.open(url, 'title', "toolbar=no, scrollbars=no, resizeable=yes, width="+width+", height="+height);
		return false;
	}
	$scope.copyURL = function(url){
		clipboard.copyText(url);
		alert("Copy URL to clipboard.");
	}
	$scope.getMoreLb = function(){
//		$scope.scrollAction = true;
		if (false == $scope.scrollAction) {
			$scope.scrollAction = true;
			$http.post( "/board/get_more_lb/",{learning : $scope.learning_lang, explained : $scope.explained_lang, start : $scope.start, search: $scope.searchWord}).success( function( response ) {
				if ( response.isSuccess == 1 ) {
					console.log(response);
					if( response.cards.length == 12){
						$scope.start += 12;
						$scope.scrollAction = false;
					}
					$scope.boards = $scope.boards.concat(response.cards);
				}
			});
		}
	}
});

$(document).ready(function(){
	$('#search-data').keypress(function(event){
		console.log("keypress");
		  if(event.keyCode == 13){
		    angular.element($(".lb-content")).scope().searchBoards();
		  }
	});
})