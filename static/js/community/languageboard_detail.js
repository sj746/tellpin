

tellpinApp.controller( "lbDetailController", function($scope, $http, $window, $sce, clipboard) {
	$scope.initLbDetail = function(data){
		console.log(data);
		$scope.userInfo = data.userInfo;
		$scope.post = data.post;
		$scope.languageInfo = data.languageInfo;
		$scope.nationsInfo = data.nationsInfo;
		$scope.browser = checkBrowser();
		$scope.commentCount = $scope.getCommentCount($scope.post.comments);
		$scope.lbCommentTextarea = 0;
		$scope.subCommentTextarea = [];
		$scope.lbSubComment = [];
		$scope.shareMenu = false;
		$scope.reportPopup = false;
	}
	
	$scope.removelb = function(pid) {
		console.log("removelb");
		if(confirm("delete?")){
			$http.post("/board/removelb/", {pid : pid}).success(function(response){
				if(response.isSuccess){
					$window.location.href = ("/board/");
				}
			});
		}
		else{
			return;
		}
	}

	$scope.editlb = function(pid) {
		console.log("editlb");
		$("#edit-post").submit();
	}

	$scope.trustAsHtml = function(html){
		return $sce.trustAsHtml(html);
	}
	$scope.addLbComment = function($index){
		if($scope.lbComment == undefined || $scope.lbComment == ""){
			alert("empty Comment");
			return;
		}
		$http.post("/board/add_comment/", {type : "P", ref_id : $scope.post.id, content : $scope.lbComment }).success(function(response){
			$scope.post.comments = response.comments;
			$scope.lbComment = "";
			$scope.commentCount = $scope.getCommentCount($scope.post.comments);
			$scope.hideLbCommentTextarea($index);
		});
	}
	$scope.addSubComment = function($index){
		if($scope.lbSubComment[$index] == "" || $scope.lbSubComment[$index] == undefined){
			alert("empty Comment");
			return;
		}
		console.log("sssssssssss");
		console.log($scope.post.comments[$index].id);
		$http.post("/board/add_comment/", {type : "C", ref_id : $scope.post.comments[$index].id, content : $scope.lbSubComment[$index] }).success(function(response){
			console.log(response.comments);
//			$scope.post.comments[$index].sub_comments = response.comments;
			$scope.lbSubComment[$index] = "";
			$scope.commentCount = $scope.getCommentCount($scope.post.comments);
			$scope.hideSubCommentTextarea($index);
		});
	}
	$scope.getCommentCount = function(comments){
		var count = comments.length;
//		for(var idx = 0; idx < comments.length; idx++){
//			count += comments[idx].sub_comments.length;
//		}
		return count;
	}
	$scope.showLbCommentTextarea = function(){
		if($scope.lbCommentTextarea){
			$scope.lbCommentTextarea = false;
		}
		else{
			$scope.lbCommentTextarea = true;
			setTimeout(function() { //			textarea focus on
				$(".cd-comment-textarea > textarea").focus();
			}, 0);
			var height = document.documentElement.scrollHeight;
			height = height + 10;
			$(document).scrollTop(0, height);
		}
	}
	$scope.showSubCommentTextarea = function($index){
		if($scope.subCommentTextarea[$index]){
			$scope.subCommentTextarea[$index] = false;
			
		}
		else{
			$scope.subCommentTextarea[$index] = true;
			setTimeout(function() { //			textarea focus on
				$(".sub-comment-textarea-"+$index).focus();
			}, 0);
			var height = document.documentElement.scrollHeight;
			height = height + 10;
			$(document).scrollTop(0, height);
		}
	}
	$scope.hideLbCommentTextarea = function($index){
		$scope.lbCommentTextarea= false;
		$scope.lbComment = "";
	}
	$scope.hideSubCommentTextarea = function($index){
		$scope.subCommentTextarea[$index] = false;
		$scope.lbSubComment[$index] = "";
	}
	$scope.deleteComment = function(cid){
		if(confirm("delete?")){
			$http.post("/board/delete_comment/", {cid : cid}).success(function(response){
				$scope.post.comments = response.comments;
				$scope.commentCount = $scope.getCommentCount($scope.post.comments);
			});
		}
		else{
			return;
		}
	}
	$scope.deleteSubComment = function(cid, $index){
		console.log($index);
		if(confirm("delete?")){
			$http.post("/board/delete_comment/", {cid : cid}).success(function(response){
				console.log(response.comments);
//				$scope.post.comments[$index].sub_comments = response.comments;
				$scope.commentCount = $scope.getCommentCount($scope.post.comments);
				if(response.comments.length == 0){
					$scope.subCommentList[$index] = false;
				}
			});
		}
		else{
			return;
		}
	}
	$scope.pinLb = function(){
		$http.post("/board/pin/", {pid : $scope.post.id}).success(function(response){
			if(response.isSuccess){
				$scope.post.pin_count = response.pin_count;
				$scope.post.pin = response.pin;
			}
		});
	}
	$scope.voteLb = function(){
		$http.post("/board/vote/", {type : "P", ref_id : $scope.post.id}).success(function(response){
			if(response.isSuccess){
				$scope.post.vote = response.like;
				$scope.post.vote_count = response.like_count;
			}
		});
	}
	$scope.showShareMenu = function(){
		$scope.shareMenu = true;
	};
	$scope.hideShareMenu = function(e){
		if( e.target.id != 'share-button'){
			$scope.shareMenu = false;
		}
	}
	$scope.showSharePopup = function(url, width, height){
		$window.open(url, 'title', "toolbar=no, scrollbars=no, resizeable=yes, width="+width+", height="+height);
		return false;
	}
	$scope.showReportPopup = function(){
		$scope.reportPopup = true;
		setTimeout(function() {
			$(".report-textarea").focus();
		}, 0);
	}
	$scope.hideReportPopup = function(){
		$scope.reportPopup = false;
		$scope.reportContent = "";
	}
	$scope.goURL = function(url){
		$window.location.href = (url);
	}
	$scope.copyURL = function(url){
		clipboard.copyText(url);
		alert("Copy URL to clipboard.");
	}
	$scope.lbViolation = function(pid){
		if($scope.reportContent == "" || $scope.reportContent == undefined){
			alert("empty content!!!");
			return;
		}
		$http.post("/board/violation/", {type:"P", ref_id : pid, content : $scope.reportContent}).success(function(response){
			console.log(response);
			if(response.isSuccess){
				$scope.reportPopup = false;
				$scope.reportContent="";
				alert("report successfully submitted");
			}
		});
	}
	
	$scope.goUrlAfterLogin = function(url) {
		
		if(url.length > 0){
			location.href = "/user_auth/ajax_nologin/?url=" + url;
		}
	}
});
