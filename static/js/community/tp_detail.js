tellpinApp.controller("tpDetailController",function($scope, $http, $window, clipboard, $sce){
	
	$scope.initQnADetail = function(data){
		console.log(data);
		$scope.queComments = false; //question's comments div
		$scope.commntTextarea = false; //question's comment textarea
		
		$scope.answerComments = []; //answer's comments div
		$scope.answerCommntTextarea = []; //answer's comment textarea
		
		$scope.shareMenu = false; //question's share menu
		$scope.reportPopup = false; //question's report popup
		$scope.reportAnswerPopup = false; //question's report popup
		$scope.reportAnswerId = 0;
		$scope.answerEditor = false; //answer editor
		$scope.modifyAnswer = false;
		$scope.modifyAnswerIdx = 0;
		
		$scope.answerCommentContent = []; //answer's comment textarea model
		$scope.subCommentContent = [];
		$scope.subCommentTextarea = [];
		$scope.answerSubCommentTextarea = [];
		$scope.answerSubCommentContent =[];
		$scope.subCommentTextarea = [];
		$scope.userInfo = data.userInfo;
		$scope.questionInfo = data.questionInfo;
		$scope.languageInfo = data.languageInfo;
		$scope.nationsInfo = data.nationsInfo;
		$scope.browser = checkBrowser();
		$scope.commentCount = $scope.getCommentCount();
		
//		if( $scope.userInfo != undefined && $scope.userInfo != null) {
//			$scope.writeAnswer = $scope.checkWriteAnswer();
//		} else {
//			$scope.writeAnswer = false;
//		}
		
//		console.log($scope.writeAnswer);
	}

	$scope.checkWriteAnswer = function() {
		if($scope.questionInfo.answers.length > 0) {
			for(var i = 0; i < $scope.questionInfo.answers.length; i++) {
				if ($scope.questionInfo.answers[i].user_id == $scope.userInfo.id)
					return true;
				else
					return false;
			}
		} else {
			return false;
		}
	}
	$scope.deleteTP = function(qid, best_answer_credit) {
		console.log("deleteTP");
		if(confirm("delete?")){
			$http.post("/trans/removetp/", {qid : qid, best_answer_credit : best_answer_credit}).success(function(response){
				if(response.isSuccess){
					$window.location.href = ("/trans/");
				}
			});
		}
		else{
			return;
		}
	}

	$scope.editTP = function(qid) {
		console.log("editTP");
		$("#edit-tp").submit();
//		console.log($scope.questionInfo.question.user_id);
		
//		$http.post("/trans/editTP/", {user_id: $scope.questionInfo.question.user_id, qid : qid}).success(function(response){
//			console.log(response.isSuccess);
//			if(response.isSuccess){
//				console.log("CCCCCCCCCCCCCCCC");
//				$window.location.href = ("/trans/write/");
//			}
//		});
		
	}

	$scope.trustAsHtml = function(html){
		return $sce.trustAsHtml(html);
	}
	$scope.hideShareMenu = function(e){
		if( e.target.id != 'share-button'){
			$scope.shareMenu = false;
		}
	}
	
	$scope.showQueComments = function(){
		if($scope.queComments == true){
			$scope.queComments = false;
		}
		else{
			$scope.queComments = true;
		}
	};
	$scope.showSubCommentTextarea = function($index){
		if($scope.subCommentTextarea[$index] != true){
			$scope.subCommentTextarea[$index] = true;
			setTimeout(function() { //			textarea focus on
				$(".qd-sub-textarea:nth-child("+ ($index + 1) +")").focus();
			}, 0);
			window.scrollTo(0, document.body.scrollHeight);
		}
		else{
			$scope.subCommentTextarea[$index] = false;
		}
	}
	$scope.showCommentTextarea = function(){
//		console.log($scope.qna);
		if($scope.user == ''){
			alert("please login.");
		}
		else{
			if($scope.commntTextarea == true){
				$scope.commntTextarea = false;
			}
			else{
				$scope.commntTextarea = true;
				setTimeout(function() {  	//			textarea focus on
					$(".qd-comment-textarea:first-child").focus();
				}, 0);
				window.scrollTo(0, document.body.scrollHeight);
			}
		}
	};
	$scope.showAnswerSubCommentTextarea = function(answerIndex, $index, $event){
		var target = $event.target;
		if($scope.answerSubCommentTextarea[answerIndex][$index] == true){
			$scope.answerSubCommentTextarea[answerIndex][$index] = false;
		}
		else{
			$scope.answerSubCommentTextarea[answerIndex][$index] = true;
		}
		setTimeout(function() { //			textarea focus on
			$(target).parent().parent().parent().find("textarea").focus();
		}, 0);
//		var height = document.documentElement.scrollHeight;
		console.log("showAnswerSubCommentTextarea");
//		console.log(height);
//		height = height + 30;
//		$(document).scrollTop(0, height);
		window.scrollTo(0, document.body.scrollHeight);
	}
	$scope.hideAnswerSubCommentTextarea = function(answerIndex, $index){
		
		$scope.answerSubCommentTextarea[answerIndex][$index] = false;
		$scope.answerSubCommentContent[answerIndex][$index] = "";
	}
	$scope.hideCommentTextarea = function(){
		$scope.commntTextarea = false;
		$(".qd-comment-textarea").val('');
	};
	$scope.hideSubCommentTextarea = function($index){
		$scope.subCommentTextarea[$index] = false;
		$(".qd-sub-textarea:nth-child("+ ($index + 1) +")").val('');
	}
	$scope.addSubList = function(answerIndex){
		$scope.answerSubCommentTextarea[answerIndex] = [];
		$scope.answerSubCommentContent[answerIndex] = [];
	}
	$scope.showAnswerComments = function($index){
		if($scope.user == ''){
			alert("please login.");
		}
		else{
			if($scope.answerComments[$index] == true){
				$scope.answerComments[$index] = false;
			}
			else{
				$scope.answerComments[$index] = true;
			}
		}
		
	};
	$scope.showAnswerCommentTextarea = function($index, $event){
		var target = $event.target;
		console.log($(target).parent().parent().find(".qd-comment-textarea"));
		if($scope.answerCommntTextarea[$index] == true){
			$scope.answerCommntTextarea[$index] = false;
		}
		else{
			$scope.answerCommntTextarea[$index] = true;
			setTimeout(function() { //			textarea focus on
				$(target).parent().parent().find(".qd-comment-textarea").focus();
			}, 0);
//			var height = document.documentElement.scrollHeight;
//			console.log("showAnswerCommentTextarea");
//			height = height + 130;
//			$(document).scrollTop(0, height);
//			$(document).height(0, 1300);
//			window.scrollTo(0, document.body.scrollHeight);
			var height = document.documentElement.scrollHeight;
			height = height + 30;
			$(document).scrollTop(0, height);
		}
	};
	$scope.hideAnswerCommentTextarea = function($index){
		$scope.answerCommntTextarea[$index] = false;
		$(".qd-comment-textarea").val('');
	};
	
	$scope.showShareMenu = function(){
		$scope.shareMenu = true;
	};
	$scope.showReportPopup = function(){
		$scope.reportPopup = true;
		setTimeout(function() {
			$(".report-textarea").focus();
		}, 0);
	}
	$scope.hideReportPopup = function(){
		$scope.reportPopup = false;
		$scope.reportContent = "";
	}
	$scope.showReportAnswerPopup = function(aid){
		$scope.reportAnswerId = aid;
		$scope.reportAnswerPopup = true;
		setTimeout(function() {
			$(".report-answer-textarea").focus();
		}, 0);
	}
	$scope.hideReportAnswerPopup = function(){
		$scope.reportAnswerId = 0;
		$scope.reportAnswerPopup = false;
		$scope.reportAnswerContent = "";
	}
	$scope.showSharePopup = function(url, width, height){
		$window.open(url, 'title', "toolbar=no, scrollbars=no, resizeable=yes, width="+width+", height="+height);
		return false;
	}

	$scope.postComment = function(tp_id){
		if($scope.commentContent == "" || $scope.commentContent == undefined){
			alert("empty content!!!");
			return;
		}
		$http.post("/trans/addcm/", {type : "Q", tp_id : tp_id, tp_ref_id : tp_id, content : $scope.commentContent }).success(function(response){
			$scope.questionInfo.question.comments = response.comments;
			$scope.commentCount = $scope.getCommentCount();
			$scope.commentContent = "";
			$scope.queComments = true;
			$scope.hideCommentTextarea();
		});
	};

	$scope.postAnswerComment = function($index, tp_id, tp_ref_id){
		$http.post("/trans/addcm/", {type : "A", tp_id : tp_id, tp_ref_id : tp_ref_id, content : $scope.answerCommentContent[$index] }).success(function(response){
			$scope.questionInfo.answers[$index].comments = response.comments;
			$scope.answerComments[$index] = true;
			$scope.answerCommentContent[$index] = "";
			$scope.hideAnswerCommentTextarea($index);
		});
	}
//sub comment delete
//	$scope.postSubComment = function($index, ref_id){
//		if($scope.subCommentContent[$index] == "" || $scope.subCommentContent[$index] == undefined){
//			alert("empty content!!!");
//			return;
//		}
//		$http.post("/trans/addcm/", {type : "C", ref_id : ref_id, content : $scope.subCommentContent[$index] }).success(function(response){
//			console.log(response);
////			$scope.questionInfo.question.comments[$index].sub_comments = response.comments;
//			$scope.subCommentContent[$index] = "";
//			$scope.commentCount = $scope.getCommentCount();
////			$scope.hideAnswerSubCommentTextarea($index, ref_id);
//			$scope.hideSubCommentTextarea($index);
//		});
//	}
//	$scope.postAnswerSubComment = function(answerIndex, $index, ref_id){
//		if($scope.answerSubCommentContent[answerIndex][$index] == "" || $scope.answerSubCommentContent[answerIndex][$index] == undefined){
//			alert("empty content!!!");
//			return;
//		}
//		$http.post("/trans/addcm/", {type : "C", ref_id : ref_id, content : $scope.answerSubCommentContent[answerIndex][$index] }).success(function(response){
//			console.log(response);
////			$scope.questionInfo.answers[answerIndex].comments[$index].sub_comments = response.comments;
//			$scope.answerSubCommentContent[answerIndex][$index] = "";
//			$scope.questionInfo.answers[answerIndex].commentCount = $scope.getAnswerCommentCount(answerIndex);
//			$scope.hideAnswerSubCommentTextarea(answerIndex, $index);
//		});
//	}

	$scope.deleteAnswer = function(qid, aid){
		if(confirm("Are you sure you want to delete the answer?")){
			$http.post("/trans/removeanswer/", {qid : qid, aid : aid}).success(function(response){
				if(response.isSuccess){
					$scope.questionInfo.answers = response.answers;
					$scope.writeAnswer = false;
//					alert("Success");
				}
			});
		}
		else{
			return;
		}
	}

	$scope.deleteComment = function(cid){
		if(confirm("Are you sure you want to delete the comment?")){
			$http.post("/trans/removecm/", {cid : cid}).success(function(response){
				$scope.questionInfo.question.comments = response.comments;
				$scope.commentCount = $scope.getCommentCount();
				if(response.comments.length == 0){
					$scope.queComments = false;
				}
			});
		}
		else{
			return;
		}
	}
	$scope.deleteAnswerComment = function($index, cid){
		if(confirm("delete?")){
			$http.post("/trans/removecm/", {cid : cid}).success(function(response){
				$scope.questionInfo.answers[$index].comments = response.comments;
				$scope.questionInfo.answers[$index].commentCount = $scope.getAnswerCommentCount($index);
				if(response.comments.length == 0){
					$scope.answerComments[$index] = false;
				}
			});
		}
		else{
			return;
		}
	}
	$scope.deleteSubComment = function($index, cid){
		if(confirm("delete?")){
			$http.post("/trans/removecm/", {cid : cid}).success(function(response){
//				$scope.questionInfo.question.comments[$index].sub_comments = response.comments;
				$scope.commentCount = $scope.getCommentCount();
			});
		}
		else{
			return;
		}
	}
	$scope.deleteAnswerSubComment = function(answerIndex, $index, cid){
		console.log(answerIndex);
		if(confirm("delete?")){
			$http.post("/trans/removecm/", {cid : cid}).success(function(response){
//				$scope.questionInfo.answers[answerIndex].comments[$index].sub_comments = response.comments;
				$scope.questionInfo.answers[answerIndex].commentCount = $scope.getAnswerCommentCount(answerIndex);
			});
		}
		else{
			return;
		}
	}
	$scope.getCommentCount = function(){
		var count = $scope.questionInfo.question.comments.length;
//		for(var idx = 0 ; idx < $scope.questionInfo.question.comments.length; idx++){
//			count += $scope.questionInfo.question.comments[idx].sub_comments.length;
//		}
		return count;
	}
	$scope.getAnswerCommentCount = function($index){
		var count = $scope.questionInfo.answers[$index].comments.length;
//		for(var idx = 0 ; idx < $scope.questionInfo.answers[$index].comments.length; idx++){
//			count += $scope.questionInfo.answers[$index].comments[idx].sub_comments.length;
//		}
		return count;
	}

	
	$scope.showAnswerEditor = function(){
		if($scope.user == ''){
			alert("please login.");
		}
		else{
			$scope.answerEditor = true;
			window.scrollTo(0, document.body.scrollHeight);
		}
	};

	$scope.modifyAnswerEditor = function($index, aid){
		if($scope.user == ''){
			alert("please login.");
		}
		else{
			console.log("modifyAnswerEditor");
			console.log(aid);
			console.log($scope.questionInfo.answers[$index].content);
			$scope.answerEditor = true;
			$scope.modifyAnswer = true;
			$scope.modifyAnswerIdx = aid;
			tinymce.get("answer_editor").setContent($scope.questionInfo.answers[$index].content);
		}
		
	};

	$scope.hideAnswerEditor = function(){
		$scope.answerEditor = false;
		tinymce.get("answer_editor").setContent("");
	};
	$scope.postAnswer = function(id){
		var content = tinymce.get("answer_editor").getContent();
			$http.post("/trans/answer/", {id : id, content : content}).success(function(response){
				console.log(response);
				if(response.isSuccess){
					$scope.questionInfo.answers = response.answers;
					$scope.hideAnswerEditor();
					$scope.writeAnswer = true;
//					alert("Success");
				}
			});			
	}

	$scope.editAnswer = function(qid){
		console.log("<<<< editAnswer <<<");
		console.log($scope.modifyAnswerIdx);
		console.log(tinymce.get("answer_editor").getContent());
		
		var content = tinymce.get("answer_editor").getContent();
		$http.post("/trans/editanswer/", {id : id, aid : $scope.modifyAnswerIdx, content : content}).success(function(response){
			if(response.isSuccess){
				$scope.questionInfo.answers = response.answers;
				$scope.hideAnswerEditor();
				alert("Success");
				$scope.modifyAnswer = false;
				$scope.modifyAnswerIdx = 0;
			}
		});
	}

	$scope.vote = function(ref_id){
		$http.post("/trans/vote/", {type : "Q", ref_id : ref_id, ref_aid : ref_id}).success(function(response){
			if(response.isSuccess){
				$scope.questionInfo.question.like = response.like;
				$scope.questionInfo.question.like_count = response.like_count;
			}
		});
	};
	
	$scope.voteAnswer = function($index, ref_id, ref_aid){
		$http.post("/trans/vote/", {type : "A", ref_id : ref_id, ref_aid : ref_aid}).success(function(response){
			if(response.isSuccess){
				$scope.questionInfo.answers[$index].like = response.like;
				$scope.questionInfo.answers[$index].like_count = response.like_count;
			}
		});
	}

	$scope.pinIt = function(qid){
		$http.post("/trans/pin/", {id : qid}).success(function(response){
			console.log(response);
			if(response.isSuccess){
				$scope.questionInfo.question.pin_count = response.pin_count;
				$scope.questionInfo.question.pin = response.pin;
			}
		});
	}
	$scope.tpViolation = function(qid){
		if($scope.reportContent == "" || $scope.reportContent == undefined){
			alert("empty content!!!");
			return;
		}

		$http.post("/trans/violation/", {type:"Q", tp_id : qid, ref_id : qid, content : $scope.reportContent}).success(function(response){
			console.log(response);
			if(response.isSuccess){
				$scope.reportPopup = false;
				$scope.reportContent="";
				alert("report successfully submitted");
			}
		});
	}
	
	$scope.tpAnswerViolation = function(aid){
		console.log("tpAnswerViolation");
		console.log(aid);
		if($scope.reportAnswerContent == "" || $scope.reportAnswerContent == undefined){
			alert("empty content!!!");
			return;
		}

		$http.post("/trans/answerViolation/", {type:"A", tp_id : $scope.questionInfo.question.id, ref_id : aid, content : $scope.reportAnswerContent}).success(function(response){
			console.log(response);
			if(response.isSuccess){
				$scope.reportAnswerPopup = false;
				$scope.reportAnswerContent="";
				$scope.reportAnswerId = 0;
				alert("report successfully submitted");
			}
		});
	}
	
	$scope.adoptAnswer = function(qid, aid, best_answer_credit){
		if(confirm("Will you choose this answer as the best answer?")){
			$http.post("/trans/bestanswer/", {id : qid, aid : aid, best_answer_credit : best_answer_credit}).success(function(response){
				console.log(response);
				$scope.questionInfo.answers = response.answers;
				$scope.questionInfo.question.best_answer_id=aid;
			});
		}
		else{
			return;
		}
	}
	$scope.goURL = function(url){
		$window.location.href = (url);
	}
	$scope.showData = function(){
		console.log($scope.questionInfo);
	}
	$scope.copyURL = function(url){
		clipboard.copyText(url);
		alert("Copy URL to clipboard.");
	}
	
	$scope.goUrlAfterLogin = function(url) {
		
		if(url.length > 0){
			location.href = "/user_auth/ajax_nologin/?url=" + url;
		}
	}
});
