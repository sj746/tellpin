tellpinApp.controller( "lqListController", function($scope, $http, $location, $window, $sce, $timeout, clipboard) {
	$scope.initLQ = function(data){
//		console.log(data);
		$scope.tab_num=1;
		$scope.languageInfo = data.languageInfo;

		$scope.langIndex = 1;
		$scope.userInfo = data.userInfo;

		$scope.languageInfoForWritten = angular.copy($scope.languageInfo);
		$scope.languageInfoForQuestion = angular.copy($scope.languageInfo);

		$scope.languageInfoForWritten.splice(0, 0, {"id": 0, "lang_english": "All languages I speak"});
		$scope.languageInfoForQuestion.splice(0, 0, {"id": 0, "lang_english": "All languages I speak"});
		
		if( $scope.userInfo != undefined && $scope.userInfo != null) {
			$scope.nativeLanList();
			$scope.learningLanList();
//			$scope.otherLanList();
		}

		$scope.writtenLang = 0;
		$scope.questionLang = 0;

		$scope.languageInfoForWritten.splice($scope.langIndex + 1, 0, {"id": $scope.langIndex + 1, "lang_english": "-----------------------------------------"});
		$scope.languageInfoForQuestion.splice($scope.langIndex + 1, 0, {"id": $scope.langIndex + 1, "lang_english": "-----------------------------------------"});
		
		$scope.browser = checkBrowser();
		$scope.scrollTopBt = false;
		
		$scope.ajaxGetLqList();
	}

	$scope.ajaxGetLqList = function() {
		console.log("ajaxGetLqList");
		$http.post( "/questions/ajaxGetLqList/").success( function( response ) {
			if ( response.isSuccess == 1 ) {
				console.log(response.LQs);
				$scope.start = 15;
				$scope.LQs = response.LQs;
				$scope.nationsInfo = response.nationsInfo;
				$scope.scrollAction = false;
			}
		});
	}

	$scope.nativeLanList = function() {
//		console.log("nativeLanList");
		if ($scope.userInfo.native1_id != null && $scope.userInfo.native1_id != 0) {
			$scope.lanMakeList("native1_id", $scope.langIndex);
		}
		if ($scope.userInfo.native2_id != null && $scope.userInfo.native2_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("native2_id", $scope.langIndex);
		}
		if ($scope.userInfo.native3_id != null && $scope.userInfo.native3_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("native3_id", $scope.langIndex);
		}
	}
	$scope.learningLanList = function() {
		if ($scope.userInfo.lang1_id != null && $scope.userInfo.lang1_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang1_id", $scope.langIndex);
		}
		if ($scope.userInfo.lang2_id != null && $scope.userInfo.lang2_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang2_id", $scope.langIndex);
		}
		if ($scope.userInfo.lang3_id != null && $scope.userInfo.lang3_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang3_id", $scope.langIndex);
		}
		if ($scope.userInfo.lang4_id != null && $scope.userInfo.lang4_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang4_id", $scope.langIndex);
		}
		if ($scope.userInfo.lang5_id != null && $scope.userInfo.lang5_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang5_id", $scope.langIndex);
		}
		if ($scope.userInfo.lang6_id != null && $scope.userInfo.lang6_id != 0) {
			$scope.langIndex++;
			$scope.lanMakeList("lang6_id", $scope.langIndex);
		}
	}
//	$scope.otherLanList = function() {
//		if ($scope.userInfo.other1 != null && $scope.userInfo.other1 != 0) {
//			$scope.langIndex++;
//			$scope.lanMakeList("other1", $scope.langIndex);
//		}
//		if ($scope.userInfo.other2 != null && $scope.userInfo.other2 != 0) {
//			$scope.langIndex++;
//			$scope.lanMakeList("other2", $scope.langIndex);
//		}
//		if ($scope.userInfo.other3 != null && $scope.userInfo.other3 != 0) {
//			$scope.langIndex++;
//			$scope.lanMakeList("other3", $scope.langIndex);
//		}
//	}		
	$scope.lanMakeList = function(name, index) {
//		console.log(name);
//		console.log($scope.userInfo[name]);
//		console.log(index);
		
		$scope.languageInfoForWritten.splice(index, 0, {"id": $scope.languageInfo[$scope.userInfo[name]-1].id, "lang_english": $scope.languageInfo[$scope.userInfo[name]-1].lang_english});
		$scope.languageInfoForQuestion.splice(index, 0, {"id": $scope.languageInfo[$scope.userInfo[name]-1].id, "lang_english": $scope.languageInfo[$scope.userInfo[name]-1].lang_english});
	}

	$(window).scroll(function(){
	    var position = $scope.getScrollLocation();
	    if (position.Y > 200)
	    	$scope.scrollTopBt = true;
	    else
	    	$scope.scrollTopBt = false;
	});

	$scope.getScrollLocation = function(){
		var position = document.documentElement;
		var body = document.body;
		var now = {};
		now.X = document.all ?(!position.scrollLeft ? body.scrollLeft : position.scrollLeft) : (window.pageXOffset ? window.pageXOffset:window.scrollX);
		now.Y = document.all ?(!position.scrollTop ? body.scrollTop : position.scrollTop) : (window.pageYOffset ? window.pageYOffset:window.scrollY);

		return now;
	}

	$scope.moveScrollTop = function(){
		$(document).scrollTop(0);
	}

	$scope.share = function($event){
		var target = $event.target;
		var top = $(target).offset().top + 25 ;
		var left = $(target).offset().left;
		$("#share").css('left', left);
		$("#share").css('top', top);
		$("#share").css('display', 'block');
	}
	$scope.showSharePopup = function(url, width, height){
		$window.open(url, 'title', "toolbar=no, scrollbars=no, resizeable=yes, width="+width+", height="+height);
		return false;
	}
	$scope.copyURL = function(url){
		clipboard.copyText(url);
		alert("Copy URL to clipboard.");
	}
	$scope.close = function(e){
		if( e.target.id != 'show_pop'){
			$("#share").css('display', 'none');
		}
	}

	$scope.myContents = function() {
		console.log("myContents");
		$http.post( "/questions/myqna/" ).success( function( response ) {
			if ( response.isSuccess == 1 ) {
				$scope.LQs=response.LQs;
			}
			console.log($scope.LQs);
		});
	}

	$scope.tab_click = function(num){
		$scope.tab_num = num;
		if(num == 1){
			$http.post( "/questions/allquestion/" ).success( function( response ) {
				if ( response.isSuccess == 1 ) {
					$scope.LQs=response.LQs;
				}
				console.log($scope.LQs);
			});
		}else if(num == 2){
			$http.post( "/questions/myquestion/" ).success( function( response ) {
				if ( response.isSuccess == 1 ) {
					$scope.LQs=response.LQs;
				}
				console.log($scope.LQs);
			});
			
		}else if(num == 3){
			$http.post( "/questions/myanswer/" ).success( function( response ) {
				if ( response.isSuccess == 1 ) {
					$scope.LQs=response.LQs;
				}
				console.log($scope.LQs);
			});
		}
	}
	$scope.changeLQs = function( field ){
		if (("writtenLang" == field) && $scope.writtenLang != 0){
			if ($scope.writtenLang == $scope.langIndex + 1) {
				$scope.writtenLang = 0;
//				alert("Please select language.");
				return;
			}
			if ($scope.writtenLang == $scope.questionLang) {
				$scope.writtenLang = 0;
				alert("Language duplicated.");
			}
		} else if ("questionLang" == field && $scope.questionLang != 0) {
			if ($scope.questionLang == $scope.langIndex + 1) {
				$scope.questionLang = 0;
//				alert("Please select language.");
				return;
			}
			if ($scope.writtenLang == $scope.questionLang) {
				$scope.questionLang = 0;
				alert("Language duplicated.");
			}
		}
		if (!($scope.writtenLang == $scope.langIndex + 1 || $scope.questionLang == $scope.langIndex + 1)) {
			$http.post( "/questions/search_question/",{written : $scope.writtenLang, translated : $scope.questionLang }).success( function( response ) {
				if ( response.isSuccess == 1 ) {
					$scope.LQs=response.LQs;
					console.log($scope.LQs);
				}
			});
		}
	}
	$scope.vote = function($index, ref_id){
		$http.post("/questions/vote/", {type : "Q", ref_id : ref_id}).success(function(response){
			if(response.isSuccess){
				$scope.LQs[$index].like = response.like;
				$scope.LQs[$index].like_count = response.like_count;
			}
		});
	};
	$scope.pinIt = function($index, qid){
		$http.post("/questions/pin/", {qid : qid}).success(function(response){
			if(response.isSuccess){
				$scope.LQs[$index].pin_count = response.pin_count;
				$scope.LQs[$index].pin = response.pin;
			}
		});
	}
	$scope.showReportPopup = function(){
		console.log("here");
		$scope.reportPopup = true;
		setTimeout(function() {
			$(".report-textarea").focus();
		}, 0);
	}
	$scope.getMoreList = function(){
		if (false == $scope.scrollAction) {
			$scope.scrollAction = true;
			$http.post( "/questions/moreqnaList/",{start : $scope.start}).success( function( response ) {
				if ( response.isSuccess == 1 ) {
					if( response.LQs.length != 0){
						$scope.start += 15;
						$scope.scrollAction = false;
					}
					
					$scope.LQs = $scope.LQs.concat(response.LQs);
				}
			});
		}
	}
	
	$scope.goURL = function(url){
		$window.location.href = (url);
		event.stopPropagation();
	}
});

