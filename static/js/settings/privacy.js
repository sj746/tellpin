tellpinApp.controller( "privacyController", function( $scope, $http, $timeout) {
	angular.element(document).ready(function(){

	});
	$scope.init = function(){
		console.log( 'init() ');
		console.log( $scope.data );
	}
	$scope.snsCheck = function(){
		console.log( 'check SNS' );
		$http.post("/settings/privacy/checkSNS", { data : $scope.data }).success( function( result ) {
			alert('save success');
		});
	}
	$scope.savePrivacy = function(){
		console.log("savePrivacy");
		console.log( $scope.data );
		$http.post("/settings/privacy/save/", { data : $scope.data }).success( function( result ) {
			alert('save success');
		});
	}
	$scope.showChangePWPopup = function(src){
		event.stopPropagation();
		$scope.introPopup = true;
		//$scope.introSrc = src;
	}
	$scope.closeChangePWPopup = function(){
		$scope.introPopup = false;
	}
	$scope.changePassword = function(){

		//event.preventDefault();
		var current_p = $("#change-current-password").val();
		var new_p = $("#change-new-password").val();
		var new_again = $("#change-new-password-again").val();

		console.log( current_p );
		console.log( new_p );
		console.log( new_again );
		//TODO
		//current password check
		//success: return true, fail: return false.
		obj = {
				'pwd': current_p
		}
		$http.post("/user_auth/ajax_check_password/", {obj:obj}).success(function(response) {
			if(response.isSuccess){
				console.log('success');
				$(".current-password-error").text('');
				if( check_password() ){
					console.log('check_password() success');
					//$("#form-change-password").submit();
					$http.post("/settings/privacy/change_password/", {change_new_password: new_p, change_new_password_again:new_again}).success(function(result){
						if( result.isSuccess ){
							alert( 'success' );
							$scope.closeChangePWPopup();
						}else{
							alert( result.message );
						}
					});
					
				}else{
					console.log('check_passwrod() fail');
				}
			}else{
				console.log('fail');
				$(".current-password-error").text(response.message);
			}
		});
		
		//NEW PASSWORD CHECK ( include check password equal )
		//success: return true, fail: return false.
		
	}
	/*
	$scope.check_current_password = function(){
		var current_p = $("#change-current-password").val();
		//TODO
		//current password check
		//success: return true, fail: return false.
		obj = {
				  'pwd': current_p
		}
		return $q(function(resolve, reject){
			
			$http.post("/user_auth/ajax_check_password/", {obj:obj}).success(function(response) {
				if(response.isSuccess){
					console.log('success');
					$(".current-password-error").text('');
					resolve(response);
					//return check_password();
				}else{
					console.log('fail');
					$(".current-password-error").text(response.message);
					reject(response);
					//return check_password;
				}
			});
			
		});
		
	}
	*/
});

function check_password(){
	  
	console.log('check_password()');
	pwd = $("#change-new-password").val();
	pwd2 = $("#change-new-password-again").val();
	
	console.log( pwd );
	console.log( pwd2 );
	if( pwd.length<8 || pwd.length>20){
		console.log('test1');
        console.log("비밀번호는 문자, 숫자, 특수문자의 조합으로 8~20자리로 입력해주세요.");
        $(".new-password-error").text("8 to 20 characters, case-sensitive,\n"+
        		"Must contain at least one alpha character\n"+
        		"[a-z,A-Z] and one numeric character[0-9], \n"+
        		"Can contain following special characters: "+
				"!,@,#,$,%,^,&,*,?,_,~");
		//$(".div-msg").css("display", "block");
        return false;
    }
    if(pwd.search(/\s/) != -1){
		console.log('test2');
    	console.log("비밀번호는 공백업이 입력해주세요.");
    	$(".new-password-error").text("8 to 20 characters, case-sensitive,\n"+
        		"Must contain at least one alpha character\n"+
        		"[a-z,A-Z] and one numeric character[0-9], \n"+
        		"Can contain following special characters: "+
				"!,@,#,$,%,^,&,*,?,_,~");
		//$(".div-msg").css("display", "block");
		return false;
    }
    //if(!pwd.match( /([a-zA-Z].*[!,@,#,$,%,^,&,*,?,_,~].*[0-9])|([a-zA-Z].*[0-9].*[!,@,#,$,%,^,&,*,?,_,~])|([0-9].*[a-zA-Z].*[!,@,#,$,%,^,&,*,?,_,~])|([0-9].*[!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z])|([!,@,#,$,%,^,&,*,?,_,~].*[0-9].*[a-zA-Z])|([!,@,#,$,%,^,&,*,?,_,~].*[a-zA-Z].*[0-9])/ )) 	            
    if(!pwd.match(/([0-9].*[a-zA-Z])|([a-zA-Z].*[0-9])/))
    {
		console.log('test3');
    	console.log("비밀번호는 문자, 숫자의 조합으로 8~20자리로 입력해주세요.");
    	$(".new-password-error").text("8 to 20 characters, case-sensitive,\n"+
        		"Must contain at least one alpha character\n"+
        		"[a-z,A-Z] and one numeric character[0-9], \n"+
        		"Can contain following special characters: "+
				"!,@,#,$,%,^,&,*,?,_,~");
        return false;
    }    
    $(".new-password-error").text("");
	if( $("#change-new-password-again").val() != pwd ){
		console.log('test4');
		$(".new-password-again-error").text("Passwords do not match");
		return false;
	}
	console.log('test5');
	$(".new-password-again-error").text("");
    /*
    if(/(\w)\1\1/.test(pwd)){
    	console.log('비밀번호에 같은 문자를3번 이상 사용하실 수 없습니다.'); 
    	alert('비밀번호에 같은 문자를 3번 이상 사용하실 수 없습니다.'); 
        return false;
    }
    if( pwd.search(uid)>-1){
    	console.log('ID가 포함된 비밀번호는 사용하실 수 없습니다.');
    	alert('ID가 포함된 비밀번호는 사용하실 수 없습니다.');
        return false;
    }
    */
	return true;
}