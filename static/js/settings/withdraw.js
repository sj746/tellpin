tellpinApp.controller( "withdrawCtrl", ["$scope", "$window", "$http", "$timeout", function( $scope, $window, $http, $timeout ) {
	$scope.init = function( obj ) {
		console.log( obj );
		$scope.payment = obj.payment;
		$scope.nations = obj.nation;
		$scope.checkPw = 1;
	}
	
	$scope.showPaymentData = function() {
		console.log( $scope.payment );
	}
	
	$scope.openWithdrawalModal = function( id ) {
		angular.element( '#' + id ).modal();
	}
	
	$scope.hideWithdrawalModal = function( id ) {
		angular.element( '#' + id ).modal( 'hide' );
	}
	
	$scope.paymentSave = function() {
		console.log("paymentSave");
		console.log($scope.payment);
		$http.post( '/settings/withdraw/save/', { 'payment' : $scope.payment } )
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				$scope.payment = result.payment;
				alert( 'Success' );
			}
			else {
				alert( 'Save Failed' );
			}
		});
	}
	
	$scope.submitWithdrawalPaypal = function() {
		console.log("submitWithdrawalPaypal");
		console.log($scope.pw);
		if ( !$scope.pw ) {
			$scope.checkPw = 0;
			return;
		}
		$http.post( '/settings/withdraw/modify/', { 'paypal' : $scope.payment.paypal, 'pw' : $scope.pw } )
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				$scope.checkPw = result.checkPw;
				if ( $scope.checkPw == 1 ) {
					alert( 'Success!' );
					$window.location.href = '/settings/withdraw/';
				}
				else {
					$scope.checkPw = 0;
				}
			}
		});
	}
}]);