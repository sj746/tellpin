tellpinApp.controller( "notificationController", function( $scope, $http, $timeout ) {
	angular.element(document).ready(function(){

	});
	$scope.init = function(){
		console.log( 'init() ');
		console.log( $scope.data );
		$scope.checkNoti();
	}
	$scope.checkNoti = function(){
		//lesson
		if( $scope.data.noti_by_tellpin){
			$("#lesson_tellpin_all").prop("checked", true);
			$("#lesson_tellpin_all").attr("checked", true);
		}else{
			$("#lesson_tellpin_all").prop("checked", false);
			$("#lesson_tellpin_all").attr("checked", false);
		}
		//lesson
		if( $scope.data.noti_by_email){
			$("#lesson_email_all").prop("checked", true);
			$("#lesson_email_all").attr("checked", true);
		}else{
			$("#lesson_email_all").prop("checked", false);
			$("#lesson_email_all").attr("checked", false);
		}
		//club
		if( $scope.data.t_c_comment && $scope.data.t_c_upvotes && $scope.data.t_c_writes ){
			$("#club_tellpin_all").prop("checked", true);
			$("#club_tellpin_all").attr("checked", true);
		}else{
			$("#club_tellpin_all").prop("checked", false);
			$("#club_tellpin_all").attr("checked", false);
		}
		//club
		if( $scope.data.e_c_comment && $scope.data.e_c_upvotes && $scope.data.e_c_writes ){
			$("#club_email_all").prop("checked", true);
			$("#club_email_all").attr("checked", true);
		}else{
			$("#club_email_all").prop("checked", false);
			$("#club_email_all").attr("checked", false);
		}
		//trans
		if( $scope.data.t_t_comment && $scope.data.t_t_request_answer && $scope.data.t_t_upvotes && $scope.data.t_t_pinned_answer && $scope.data.t_t_writes ){
			$("#trans_tellpin_all").prop("checked", true);
			$("#trans_tellpin_all").attr("checked", true);
		}
		else{
			$("#trans_tellpin_all").prop("checked", false);
			$("#trans_tellpin_all").attr("checked", false);
		}
		//trans
		if( $scope.data.e_t_comment && $scope.data.e_t_request_answer && $scope.data.e_t_upvotes && $scope.data.e_t_pinned_answer && $scope.data.e_t_writes ){
			$("#trans_email_all").prop("checked", true);
			$("#trans_email_all").attr("checked", true);
		}
		else{
			$("#trans_email_all").prop("checked", false);
			$("#trans_email_all").attr("checked", false);
		}
		//qa
		if( $scope.data.t_q_comment && $scope.data.t_q_request_answer && $scope.data.t_q_upvotes && $scope.data.t_q_pinned_answer && $scope.data.t_q_writes )
		{		
			$("#qa_tellpin_all").prop("checked", true);
			$("#qa_tellpin_all").attr("checked", true);
		}
		else{
			$("#qa_tellpin_all").prop("checked", false);
			$("#qa_tellpin_all").attr("checked", false);
		}
		//qa
		if( $scope.data.e_q_comment && $scope.data.e_q_request_answer && $scope.data.e_q_upvotes && $scope.data.e_q_pinned_answer && $scope.data.e_q_writes )
		{		
			$("#qa_email_all").prop("checked", true);
			$("#qa_email_all").attr("checked", true);
		}
		else{
			$("#qa_email_all").prop("checked", false);
			$("#qa_email_all").attr("checked", false);
		}
		//ex
		if( $scope.data.t_x_approve )
		{		
			$("#ex_tellpin_all").prop("checked", true);
			$("#ex_tellpin_all").attr("checked", true);
		}
		else{
			$("#ex_tellpin_all").prop("checked", false);
			$("#ex_tellpin_all").attr("checked", false);
		}
		//ex
		if( $scope.data.e_x_approve )
		{		
			$("#ex_email_all").prop("checked", true);
			$("#ex_email_all").attr("checked", true);
		}
		else{
			$("#ex_email_all").prop("checked", false);
			$("#ex_email_all").attr("checked", false);
		}
		//news
		if( $scope.data.t_news_announce && $scope.data.t_news_weekly )
		{		
			$("#news_tellpin_all").prop("checked", true);
			$("#news_tellpin_all").attr("checked", true);
		}
		else{
			$("#news_tellpin_all").prop("checked", false);
			$("#news_tellpin_all").attr("checked", false);
		}
		//news
		if( $scope.data.e_news_announce && $scope.data.e_news_weekly )
		{		
			$("#news_email_all").prop("checked", true);
			$("#news_email_all").attr("checked", true);
		}
		else{
			$("#news_email_all").prop("checked", false);
			$("#news_email_all").attr("checked", false);
		}
		//msg
		if( $scope.data.e_m_new_message )
		{	
			$("#msg_email_all").prop("checked", true);
			$("#msg_email_all").attr("checked", true);
		}
		else{
			$("#msg_email_all").prop("checked", false);
			$("#msg_email_all").attr("checked", false);
		}
		
	}
	
	$scope.saveNoti = function(){
		console.log( $scope.data );
		$http.post("/settings/notification/save/", { data : $scope.data }).success( function( result ) {
			if(result.isSuccess == 1){
				console.log(result);
			}
		});
		$scope.checkNoti();
		alert('save complete');
	}
	
	
});


