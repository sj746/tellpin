

tellpinApp.config( function( $interpolateProvider) {
	$interpolateProvider.startSymbol('[[');
	$interpolateProvider.endSymbol(']]');
});

tellpinApp.controller( 'buyCtrl', ['$scope', '$window', '$http', function( $scope, $window, $http ) {
	$scope.init = function( data ) {
		console.log( data );
		$scope.creditList = data.creditInfo;
		$scope.currency = data.user_currency[0];
		$scope.currency_user = data.user_currency[1];
		 $scope.seller = "tellpinadmin@unichal.com";
		// test seller
//		$scope.seller = "tellpinadmin-facilitator@unichal.com";
	}
	
	$scope.showCredit = function() {
		console.log( $scope.credit );
	}
	
	$scope.totalFunds = function() {
		if ( !$scope.payType || !$scope.credit ) return;
		if ( $scope.payType == 'paypal' ) {
			$scope.totalFund = $scope.credit.usd + $scope.credit.paypal_fee;
		}
		else {
			$scope.totalFund = $scope.credit.usd + $scope.credit.creditcard_fee;
		}
	}
	
	$scope.submit = function() {
		$scope.validSeller = $scope.seller;
		$scope.validtotalFund = $scope.totalFund;
		$scope.validItem = $scope.credit.tc;
	}
	
}]);