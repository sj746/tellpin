
tellpinApp.controller( "CreditCtrl", ["$scope", "$locale", "$interval",  function($scope, $locale, $interval) {
    $scope.currentYear = new Date().getFullYear();
    $scope.currentMonth = new Date().getMonth() + 1;
    $scope.months = $locale.DATETIME_FORMATS.MONTH;

    $scope.ccinfo = {type:undefined};
    $scope.ccinfo.number = "";

    $scope.submit = function(data){
      if ($scope.paymentForm.$valid){
    	  $scope.ccinfo.month = data.month.slice(0,1);
    	  $scope.ccinfo.year = data.year.slice(0,4);
    	  console.log($scope.ccinfo);
      }
    }
}]);

tellpinApp.directive( 'creditCardType', function(){
    var directive =
      { require: 'ngModel'
      , link: function(scope, elm, attrs, ctrl){
          ctrl.$parsers.unshift(function(value){
            scope.ccinfo.type =
              (/^5[1-5]/.test(value)) ? "mastercard"
              : (/^4/.test(value)) ? "visa"
              : (/^3[47]/.test(value)) ? "amex"
              : (/^6011|65|64[4-9]|622(1(2[6-9]|[3-9]\d)|[2-8]\d{2}|9([01]\d|2[0-5]))/.test(value)) ? "discover"
              : undefined
            ctrl.$setValidity('invalid',!!scope.ccinfo.type)
            return value
          })
        }
      }
    return directive
});

tellpinApp.directive( 'cardExpiration', function(){
    var directive =
      { require: 'ngModel'
      , link: function(scope, elm, attrs, ctrl){
          scope.$watch('[ccinfo.month,ccinfo.year]',function(value){
            ctrl.$setValidity('invalid',true)
            if ( scope.ccinfo.year == scope.currentYear
                 && scope.ccinfo.month <= scope.currentMonth
               ) {
              ctrl.$setValidity('invalid',false)
            }
            return value
          },true)
        }
      }
    return directive
});

tellpinApp.filter( 'range', function() {
    var filter = 
      function(arr, lower, upper) {
        for (var i = lower; i <= upper; i++) arr.push(i)
        return arr
      }
    return filter
});

tellpinApp.filter('validate', [function () {  
    return function (ccnumber) {
      if (!ccnumber) { return ''; }
      var len = ccnumber.length;
      var cardType, valid;
      mul = 0,
      prodArr = [[0, 1, 2, 3, 4, 5, 6, 7, 8, 9], [0, 2, 4, 6, 8, 1, 3, 5, 7, 9]],
      sum = 0;

      while (len--) {
          sum += prodArr[mul][parseInt(ccnumber.charAt(len), 10)];
          mul ^= 1;
      }

      if (sum % 10 === 0 && sum > 0) {
        valid = "valid"
      } else {
        valid = "not valid"
      }
      ccnumber = ccnumber.toString().replace(/\s+/g, '');

      if(/^(34)|^(37)/.test(ccnumber)) {
        cardType = "American Express";
      }
      if(/^(62)|^(88)/.test(ccnumber)) {
        cardType = "China UnionPay";
      }
      if(/^30[0-5]/.test(ccnumber)) {
        cardType = "Diners Club Carte Blanche";
      }
      if(/^(2014)|^(2149)/.test(ccnumber)) {
        cardType = "Diners Club enRoute";
      }
      if(/^36/.test(ccnumber)) {
        cardType = "Diners Club International";
      }
      if(/^(6011)|^(622(1(2[6-9]|[3-9][0-9])|[2-8][0-9]{2}|9([01][0-9]|2[0-5])))|^(64[4-9])|^65/.test(ccnumber)) {
        cardType = "Discover Card";
      }
      if(/^35(2[89]|[3-8][0-9])/.test(ccnumber)) {
        cardType = "JCB";
      }
      if(/^(6304)|^(6706)|^(6771)|^(6709)/.test(ccnumber)) {
        cardType = "Laser";
      }
      if(/^(5018)|^(5020)|^(5038)|^(5893)|^(6304)|^(6759)|^(6761)|^(6762)|^(6763)|^(0604)/.test(ccnumber)) {
        cardType = "Maestro";
      }
      if(/^5[1-5]/.test(ccnumber)) {
        cardType = "MasterCard";
      }
      if (/^4/.test(ccnumber)) {
        cardType = "Visa"
      }
      if (/^(4026)|^(417500)|^(4405)|^(4508)|^(4844)|^(4913)|^(4917)/.test(ccnumber)) {
        cardType = "Visa Electron"
      }
      return cardType + " and it's " + valid;
    };
}]);

tellpinApp.directive('maskInput', function() {//mask-input
    return {
        	require: "ngModel",
            restrict: "AE",
            scope: {
                ngModel: '=',
             },
            link: function(scope, elem, attrs) {
            	var orig = scope.ngModel;
            	var edited = orig + "";
                scope.ngModel = edited.slice(4).replace(/\d/g, 'x') + edited.slice(-4);
            	
            	elem.bind("blur", function() {
                	var temp;
                	orig  = elem.val();
                	temp = elem.val();
                    elem.val(temp.slice(4).replace(/\d/g, 'x') + temp.slice(-4));
                });
            	 
            	elem.bind("focus", function() {
                    elem.val(orig);
               });	
            }
       };
});


//tellpinApp.config( function( $interpolateProvider) {
//$interpolateProvider.startSymbol('[[');
//$interpolateProvider.endSymbol(']]');
//});