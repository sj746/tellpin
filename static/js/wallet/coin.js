/**
 * tutor_info.js
 */

var tutorApp = angular.module( "coinApp", [] );
tutorApp.run(function($rootScope, $location){
    
});

tutorApp.config( function( $interpolateProvider) {
	$interpolateProvider.startSymbol('[[');
	$interpolateProvider.endSymbol(']]');
});

tutorApp.filter('slice', function() {
	return function(arr){
		
		if ( typeof arr != 'undefined' ){
			return arr.slice(0,10);
		}
		
	}
});

tutorApp.controller( "coinController", ["$scope", "$http", "$sce", function( $scope, $http, $sce ) {
	
	
	angular.element(document).ready(function(){
		$http.post("/coin/get_coin_history/").success( function( result ) {
			if(result.isSuccess == 1){
				console.log(result);
				$scope.coin_history = result.coin_history[0].coin_list;
				$scope.balance_history = result.balance_history[0];
			}
		});
	});
	$scope.loading = function(){
		
	};
	//table filter
	$scope.fieldFilter = function(_ordering) {
		$scope.ordering = _ordering;
		
	};
	
	
}]);
