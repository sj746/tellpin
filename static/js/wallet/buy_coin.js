/**
 * tutor_info.js
 */

var tutorApp = angular.module( "buyCoinApp", [] );
tutorApp.run(function($rootScope, $location){
    
});

tutorApp.config( function( $interpolateProvider) {
	$interpolateProvider.startSymbol('[[');
	$interpolateProvider.endSymbol(']]');
});


tutorApp.controller( "buyCoinController", ["$scope", "$http", "$sce", function( $scope, $http, $sce ) {
	
	
	angular.element(document).ready(function(){
		$http.post("/coin/get_coin_history/").success( function( result ) {
			if(result.isSuccess == 1){
				$scope.coin_history = result.coin_history[0].coin_list;
				$scope.refresh_data();
			}
			
		});
	});
	$scope.refresh_data = function(){
		var coin = $("#id_coin").val();
		$("#buy_coin").text(coin+" C");
		var prev = $scope.coin_history[0].available_balance;
		var after = $("#after_coin").text();
		$("#after_coin").text( parseInt(prev) + parseInt(coin) + " C");
		console.log(coin+', '+prev+', '+after);
		$("#purchase_amount").text( parseInt(coin)/10 + " USD" );
		$("#total_amount").text( parseInt(coin)/10.0 + 0.34 + " USD");
	}
	$scope.loading = function(){
		
	};
	//table filter
	$scope.fieldFilter = function(_ordering) {
		$scope.ordering = _ordering;
		
	};
	$scope.buy_coin=function(){
		var coin = $("#id_coin").val();
		console.log(coin);
		
		$http.post("/coin/buy_coin/", {coin: coin}).success( function( result ) {
			if(result.isSuccess == 1){
				
				//데이터 갱신
				$http.post("/coin/get_coin_history/").success( function( result ) {
					if(result.isSuccess == 1){
						console.log('refresh()');
						$scope.coin_history = result.coin_history[0].coin_list;
						$scope.refresh_data();
						window.location.assign("coin/");
					}
				});				
			}
		});
	}
	
}]);
$(document).ready(function(){
	
});

function change_coin(){
	var coin = $("#id_coin").val();
	$("#buy_coin").text(coin+" C");
	//var prev = $scope.coin_history[0].available_balance;
	var prev = $("#prev_coin").text();
	var after = $("#after_coin").text();
	console.log(coin+', '+prev+', '+after);
	$("#after_coin").text( parseInt(prev) + parseInt(coin) + " C");
	$("#purchase_amount").text( parseInt(coin)/10 + " USD" );
	$("#total_amount").text( parseInt(coin)/10.0 + 0.34 + " USD");
}

