tellpinApp.controller( "accountCtrl", ["$scope", "$window", "$http", "$timeout", function( $scope, $window, $http, $timeout ) {
	$scope.init = function( obj ) {
		console.log( obj );
		$scope.checkTutor = obj.is_tutor;
		$scope.tutor = obj.is_tutor;
		$scope.tutorAccount = obj.tutorAccount;
		$scope.tuteeAccount = obj.tuteeAccount;
		$scope.history = obj.history;
		$scope.paypal = obj.tutorAccount.paypal;
		$scope.currency = obj.user_currency[0];
		$scope.currency_user = obj.user_currency[1];
		$scope.start = new Date( $scope.history.startDate );
		$scope.end = new Date( $scope.history.endDate );
		$scope.startDateOptions = {
				formatYear: 'yy',
				maxDate: new Date( $scope.history.endDate ) 
		}
		$scope.endDateOptions = {
				formatYear : 'yy',
				minDate: $scope.start
		}
		$scope.checkWithdraw = 0;
		$scope.checkPw = 1;
		$scope.isTutor = obj.is_tutor;
		$scope.withdraw = 0;
		$scope.transferCredit = 1;
		$scope.withdrawCredit = 0;
		$scope.morewithDraw = 0;
		$scope.lesswithDraw = 0;
		$scope.billEdit = 0;
		$scope.setDateType = 'one';
	}
	
	$scope.moveBuyCredits = function() {
		$window.location.href = "/wallet/buy_credits/";
	}

	$scope.openCalendarStartDate = function() {
		$scope.startDateOptions.maxDate = $scope.end;
		$scope.startDate = true;
	}
	
	$scope.openCalendarEndDate = function() {
		$scope.endDateOptions.minDate = $scope.start;
		$scope.endDate = true;
	}
	
	$scope.setDateDuration = function( type ) {
		$scope.setDateType = type;
		$scope.end = new Date();
		$scope.start = new Date();
		if ( type == 'one' ) {
			$scope.start.setMonth( $scope.start.getMonth() - 1 );
		}
		else if ( type == 'three' ) {
			$scope.start.setMonth( $scope.start.getMonth() - 3 );
		}
		else if ( type == 'six' ) {
			$scope.start.setMonth( $scope.start.getMonth() - 6 );
		}
		else {
			$scope.start.setMonth( $scope.start.getMonth() - 12 );
		}
		$scope.getCustomSearchDate();
		console.log( $scope.end );
		console.log( $scope.start );
	}
	
	$scope.getWalletHistory = function( tutor, withdraw ) {
		if ( $scope.checkTutor == tutor && $scope.checkWithdraw == withdraw ) {
			return;
		}
		$scope.isTutor = tutor;
		$scope.withdraw = withdraw;
		$scope.getCustomSearchDate();
	}
	
	$scope.setSearchDate = function() {
		$scope.setDateType = 'userCustom';
		$scope.getCustomSearchDate();
	}


	
	$scope.getCustomSearchDate = function( p ) {
		var page;
		if ( !p ) {
			page = 1;
		}
		else {
			page = p;
		}
		
		var startDay = $scope.start.getFullYear()
						+ '-'
						+ ( $scope.start.getMonth() + 1 )
						+ '-'
						+ $scope.start.getDate();
		var endDay = $scope.end.getFullYear()
						+ '-'
						+ ( $scope.end.getMonth() + 1 )
						+ '-'
						+ $scope.end.getDate();
		console.log( page );
		if ( $scope.isTutor == 1 ) {
			$http.post( '/wallet/tutor_account/page/', { 'start' : startDay, 'end' : endDay, 'page' : page, 'isType' : $scope.withdraw })
			.success( function( result ) {
				console.log( result.history );
				$scope.history = result.history;
				$scope.start = new Date( $scope.history.startDate );
				$scope.end = new Date( $scope.history.endDate );
				$scope.pageType = 2;
				$scope.checkTutor = result.checkTutor;
				$scope.checkWithdraw = result.checkWithdraw;
			});			
		}
		else {
			$http.post( '/wallet/tutee_account/page/', { 'start' : startDay, 'end' : endDay, 'page' : page, 'isType' : $scope.withdraw })
			.success( function( result ) {
				console.log( result.history );
				$scope.history = result.history;
				$scope.start = new Date( $scope.history.startDate );
				$scope.end = new Date( $scope.history.endDate );
				$scope.pageType = 2;
				$scope.checkTutor = result.checkTutor;
				$scope.checkWithdraw = result.checkWithdraw;
			});	
		}
		
	}

	$scope.openTransferModal = function() {
		$scope.transChecked = false;
		$scope.pw = '';
		$scope.checkPw = 1;
		$http.post( '/wallet/tutor_account/tuteetc/' )
		.success( function( result ) {
			console.log( result );
			$scope.tuteeTc = result.tuteeTc;
			$scope.transferCredit = 1;
			angular.element( '#transferModal' ).modal();
		});
	}
	
	$scope.openWithdrawModal = function( type, obj ) {
		if ( type == 'edit' ) {
			$scope.withdrawType = type;
			$scope.oldWithdraw = obj;
		}
		else {
			$scope.withdrawType = 'normal';
		}	
		$scope.withdrawChecked = false;
		$scope.pw = '';
		$scope.checkPw = 1;
		$scope.withdrawCredit = 0;
		angular.element( '#withdrawModal' ).modal();
	}

	$scope.buyGiftCard = function() {
		$window.location.href = "/wallet/tutee_account/gift/";
	}

	$scope.openRedeemCodeModal = function() {
		$scope.giftCode = '';
		angular.element( '#codeModal' ).modal();
		$timeout( function() {
			$( '#gift' ).focus();
		}, 460);
	}

	$scope.checkCode = function() {
		if ( !$scope.giftCode ) {
			alert( 'Input gift code' );
		}
		else {
			$http.post( '/wallet/tutee_account/gift/check/', { 'code' : $scope.giftCode } )
			.success( function( result ) {
				console.log( result );
				if ( result.isSuccess == 1 ) {
					$window.location.href = '/wallet/';
				}
				else {
					//alert( 'Invalid Code' );
					$scope.giftValid = true;
					//$( '#gift' ).focus();
					//angular.element( '#codeModal' ).modal( 'hide' );
				}
			});
		}
	}

	$scope.transCredits = function() {
		console.log( $scope.transferCredit );
	
		if ( $scope.transferCredit == 0 ) {
			alert( 'Not enough TC' );
		}
		else {
			$http.post( '/wallet/tutor_account/transfer/',
				{ 'tc' : $scope.transferCredit, 'pw': $scope.pw } ).success( function( result ) {
					console.log(result);
					if ( result.isSuccess == 0) {
						if ( result.checkPw == 0 ) {
							$scope.checkPw = 0;
						}
					} else {
						$scope.checkPw = 1;
						angular.element( '#transferModal' ).modal( 'hide' );
						$window.location.href = "/wallet/";
					}
				});
		}
	}
	
	$scope.sendWithdraw = function() {
		console.log ( $scope.withdrawType );
		if ( $scope.withdrawType == 'edit' ) {
			$scope.modifyWithdraw( $scope.oldWithdraw.teac_id, $scope.oldWithdraw.a_change, $scope.oldWithdraw.more_id );
		}
		else {
			$http.post( '/wallet/tutor_account/withdraw/',
					{ 'withdraw' : $scope.withdrawCredit, 'pw' : $scope.pw, 'paypal' : $scope.paypal } )
			.success( function( result ) {
				if ( result.isSuccess == 1 ) {
					if ( result.checkPw == 0 ) {
						$scope.checkPw = 0;
					}
					else {
						$scope.checkPw = 1;
						angular.element( '#withdrawModal' ).modal( 'hide' );
						$window.location.href = "/wallet/";
					}
				}
			});
		}
	}
	
	$scope.cancelWithdraw = function( teac_id, withdraw, paypal ) {
		$http.post( '/wallet/withdraw/delete/', { 'teac_id' : teac_id, 'withdraw' : withdraw, 'paypal' : paypal } )
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				$window.location.href = "/wallet/account/";
			} 
		});
	}
	
	$scope.modifyWithdraw = function( teac_id, old, paypal ) {
		$http.post( '/wallet/withdraw/modify/', { 'teac_id' : teac_id, 'oldwithdraw' : old, 'newwithdraw' : $scope.withdrawCredit, 'paypal' : paypal } )
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				$window.location.href = "/wallet/account/";
			} 
		});
	}

	$scope.openGiftModal = function( teac_id, type ) {
		console.log("GiftModal");
		$scope.teac_id = teac_id;
		$scope.type = type;
		$http.post( '/wallet/tutee_account/gift/giftcard/', { 'teac_id' : teac_id } )
		.success( function( result ) {
			console.log( result );
			$scope.gift = result.gift.gift;
			$scope.teac_id = teac_id;
			angular.element( '#giftModal' ).modal();
		});
	}

	$scope.openReceipt = function( teac_id ) {
		console.log( teac_id );
		$scope.billEdit = 0;
		$scope.bill = '';
		$http.post( '/wallet/tutee_account/receipt/', { 'teac_id' : teac_id } )
		.success( function( result ) {
			console.log( result );
			$scope.receipt = result.receipt;
			angular.element( '#receiptModal' ).modal();
		});
	}

	$scope.printReceipt = function() {
		var innerContents = angular.element( '#receiptModal' ).html();
        var popupWinindow = $window.open('', '_blank', 'scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/static/css/wallet/account.css" /><link rel="stylesheet" type="text/css" href="/static/css/bootstrap/css/bootstrap.min.css" id="medium-editor-theme"></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();
	}
	
	$scope.openReserveModal = function( rvid, teac_id, type, status, a_change ) {
		console.log( rvid );
		if (type == 'tutor')
			var is_Tutor = 1;
		else
			var is_Tutor = 0;

		$http.post( '/wallet/reservation/', { 'rvid' : rvid, 'teac_id' : teac_id, 'type' : is_Tutor } )
		.success( function( result ) {
			console.log( result );
			$scope.lesson = result.lesson;
			$scope.lessonStatus = status;
			if ( status == 'Refunded' ) {
				if ( type == 'tutor' ) {
					$scope.tutorRefund = a_change;
					$scope.tuteeRefund = $scope.lesson.lesson_credit - a_change;
				}
				else {
					$scope.tuteeRefund = a_change;
					$scope.tutorRefund = $scope.lesson.lesson_credit - a_change;
				}
			}
			angular.element( '#lessonModal' ).modal();
		});

	}

	$scope.closeReceiptModal = function() {
		angular.element( '#receiptModal' ).modal( 'hide' );
	}
	
	$scope.editWithdraw = function( obj ) {
		$scope.editWithdraw( 'edit', obj );
		console.log( obj );
		$scope.withdrawCredit = -( obj.a_change );
		$scope.tutorAccount.paypal = obj.more_id;
	}
	
	
	$scope.goUrl = function( url ) {
		$window.location.href = url;
	}
	
	$scope.openCommunityPost = function( post, type ) {
		var temp;
		if ( type == 8 ) {
			temp = 'questions';
		}
		else if ( type == 7 ) {
			temp = 'trans';
		}
		else {
			temp = 'board';
		}
		var postUrl = '/' + temp + '/' + post + '/';
		$window.open( postUrl, '_blank' );
	}
}]);