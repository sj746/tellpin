

tellpinApp.config( function( $interpolateProvider) {
	$interpolateProvider.startSymbol('[[');
	$interpolateProvider.endSymbol(']]');
});

tellpinApp.controller( 'giftCtrl', ['$scope', '$window', '$http', '$timeout', function( $scope, $window, $http, $timeout ) {
	$scope.init = function( obj ) {
		console.log( obj );
		$scope.giftCard = {};
		$scope.creditList = obj.creditInfo;
		$scope.giftCard.tc = obj.creditInfo[3].tc;
		$scope.giftCard.type = 13;
		$scope.availableCredit = obj.availableCredit;
		$scope.giftValid = false;
		$scope.currency = obj.user_currency[0];
		$scope.currency_user = obj.user_currency[1];
	}
	
	$scope.openPw = function() {
		$scope.pw = '';
		if ( $scope.giftCard.tc > $scope.availableCredit ) {
			alert( 'Unforutnately, there is not enough balance\nto carry out the action requested.' );
			return;
		}
		
		if ( $scope.giftCard.type == 13 && !$scope.giftCard.to_email ) {
			alert( 'require send by e-mail address' );
		}
		else if ( !$scope.giftCard.to_name ) {
			alert( 'require to name' );
		}
		else if ( !$scope.giftCard.content ) {
			alert( 'require content' );
		}
		else if ( !$scope.giftCard.from_name ) {
			alert( 'require from name' );
		}
		else {			
			angular.element( '#pwModal' ).modal();
		}
	}
	
	$scope.sendGift = function() {
		if ( !$scope.pw ) {
			alert( 'Input password' );
		}
		else {
			console.log( $scope.giftCard );
			if ( !$scope.giftCard.to_name ) {
				$scope.giftCard.to_name = '';
			}
			if ( !$scope.giftCard.content ) {
				$scope.giftCard.content = '';
			}
			if ( !$scope.giftCard.from_name ) {
				$scope.giftCard.from_name = '';
			}
			$http.post( '/wallet/tutee_account/gift/send/', { 'gift' : $scope.giftCard, 'pw' : $scope.pw } )
			.success( function( result ) {
				if ( result.isSuccess == 1 ) {
					$window.location.href = "/wallet/";
				}
				else {
					alert( 'Invalid password' );
				}
			});
		}
	}

}]);