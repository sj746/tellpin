tellpinApp.filter('timeago', function() {
    return function(input, p_allowFuture) {
        var substitute = function (stringOrFunction, number, strings) {
                var string = $.isFunction(stringOrFunction) ? stringOrFunction(number, dateDifference) : stringOrFunction;
                var value = (strings.numbers && strings.numbers[number]) || number;
                return string.replace(/%d/i, value);
            },
            nowTime = (new Date()).getTime(),
            date = (new Date(input)).getTime(),
            //refreshMillis= 6e4, //A minute
            allowFuture = p_allowFuture || false,
            strings= {
                prefixAgo: null,
                prefixFromNow: null,
                suffixAgo: "ago",
                suffixFromNow: "from now",
                seconds: "less than a minute",
                minute: "about a minute",
                minutes: "%d minutes",
                hour: "about an hour",
                hours: "about %d hours",
                day: "a day",
                days: "%d days",
                month: "about a month",
                months: "%d months",
                year: "about a year",
                years: "%d years"
            },
            dateDifference = nowTime - date,
            words,
            seconds = Math.abs(dateDifference) / 1000,
            minutes = seconds / 60,
            hours = minutes / 60,
            days = hours / 24,
            years = days / 365,
            separator = strings.wordSeparator === undefined ?  " " : strings.wordSeparator,
        
            // var strings = this.settings.strings;
            prefix = strings.prefixAgo,
            suffix = strings.suffixAgo;
            
        if (allowFuture) {
            if (dateDifference < 0) {
                prefix = strings.prefixFromNow;
                suffix = strings.suffixFromNow;
            }
        }

        words = seconds < 45 && substitute(strings.seconds, Math.round(seconds), strings) ||
        seconds < 90 && substitute(strings.minute, 1, strings) ||
        minutes < 45 && substitute(strings.minutes, Math.round(minutes), strings) ||
        minutes < 90 && substitute(strings.hour, 1, strings) ||
        hours < 24 && substitute(strings.hours, Math.round(hours), strings) ||
        hours < 42 && substitute(strings.day, 1, strings) ||
        days < 30 && substitute(strings.days, Math.round(days), strings) ||
        days < 45 && substitute(strings.month, 1, strings) ||
        days < 365 && substitute(strings.months, Math.round(days / 30), strings) ||
        years < 1.5 && substitute(strings.year, 1, strings) ||
        substitute(strings.years, Math.round(years), strings);

        return $.trim([prefix, words, suffix].join(separator));
        // conditional based on optional argument
        // if (somethingElse) {
        //     out = out.toUpperCase();
        // }
        // return out;
    }
});

tellpinApp.controller( 'rootController', ['$scope', '$window', '$http', '$timeout', '$interval', function( $scope, $window, $http, $timeout, $interval ) {
	$scope.$on('messageEventEmit', function (event, args) {
		$scope.$broadcast('messageEventBroadcast', { type: args.type, user_id: args.user_id });
	});
	$scope.friendMessage = "I'd like to be your language exchange partner.";
	$scope.popFlag = 0;
	$scope.msgCount = 0;
	$scope.hoverFlag = 0;
	$scope.userPop = {};
	$scope.langPop = [];
	$scope.popLoading = false;
	$scope.userPopover = function( event, id, type ) {
		$scope.bshowup = true;
		
		$http.post( '/common/user/popover/', { 'id' : id } )
		.success( function( result ) {
			
			if(!$scope.bshowup) {
				return;
			}
			
			if ( result.isSuccess == 3 ) {
				return;
			}
			else {
				var elem = $( event.currentTarget );
				var img = elem.find( 'img' );
				elem.append( $( '#popWrap' ) );
				$scope.popFlag = 1;
				var obj = elem.offset();
				if ( type == 'left' ) {
					angular.element( '#userPopover' ).addClass( 'right-arrow' );
					console.log( 'left', obj.left, 'top', obj.top );
					$( '#popWrap' ).css( {"right" : ( img.outerWidth( true ) ) + "px", "top" : 0 }  );
				}
				else {
					angular.element( '#userPopover' ).addClass( 'left-arrow' );
					console.log( 'left', obj.left, 'top', obj.top );
					$( '#popWrap' ).css( {"left" : ( img.outerWidth( true ) ) + "px", "top" : 0 }  );
				}
				if ( $scope.userPop && $scope.userPop.id != id ) {
					$scope.popLoading = true;
					$http.post( '/common/user/popover/', { 'id' : id } )
					.success( function( result ) {
						if ( result.isSuccess == 1 ) {
							$scope.popLoading = false;
							console.log( result );
							$scope.userPop = result.userPop;
							$scope.speaksPop = $scope.userPop.speaks;
							$scope.learnsPop = $scope.userPop.learns;
							if ($scope.userPop.is_tutor) {
								$scope.teachPop = $scope.userPop.teaches;	
							}
							$scope.friend = result.is_friend;
						}
					});			
				}
			}
		});	
		
	}
	
	$scope.popOverBodyAppend = function() {
		angular.element( 'body' ).append( $( '#popWrap' ) );
	}
	
	$scope.goLessonList = function(_type){
		console.log('go lesson list   ()');
		//selectedAsTab == 0 ? '/tutorlessons/' : '/tuteelessons/'
//		if( $scope.selectedAsTab == 0 ){
//			$window.location.href = '/tutorlessons/';
//		}else{
//			$window.location.href = '/tuteelessons/';
//		}
		var form = document.createElement("form");
		var input = document.createElement("input");
		var i = document.createElement("input");
		
		if( _type == 'tutor' ){
			form.action = "/tutorlessons/";	
		}
		else{
			form.action = "/tuteelessons/";
		}
		form.method = "post";
		
		i.name = "search_name";
		i.value = 'New Message';

		form.appendChild(i);
		document.body.appendChild(form);
		form.submit();
	}
	// 개인 메세지창	
	$scope.messageHandler = function (msg_type, id) {
		console.log("messageHandlermessageHandler");
		$scope.$emit('messageEventEmit', { type: msg_type, user_id: id });
		$window.event.stopPropagation();
	};

	$scope.goURL = function(url){
		$window.location.href = (url);
		$window.event.stopPropagation();
	}

	// 유저 언어정보 배열로 만듬
	/*
		obj =  {
					language : abc,
					level: 7,
					type: 3 
				}
	*/
	$scope.learnList = function( profile ) {
		console.log("learnList");
		var learningList = [];
		if(profile.lang1_learning == true){
			var learnObj = {};
			learnObj.language = profile.lang1;
			learnObj.level = profile.lang1_level;
			learningList.push(learnObj);
		}
		if(profile.lang2_learning == true){
			var learnObj = {};
			learnObj.language = profile.lang2;
			learnObj.level = profile.lang2_level;
			learningList.push(learnObj);
		}
		if(profile.lang3_learning == true){
			var learnObj = {};
			learnObj.language = profile.lang3;
			learnObj.level = profile.lang3_level;
			learningList.push(learnObj);
		}
		if(profile.lang4_learning == true){
			var learnObj = {};
			learnObj.language = profile.lang4;
			learnObj.level = profile.lang4_level;
			learningList.push(learnObj);
		}
		if(profile.lang5_learning == true){
			var learnObj = {};
			learnObj.language = profile.lang5;
			learnObj.level = profile.lang5_level;
			learningList.push(learnObj);
		}
		if(profile.lang6_learning == true){
			var learnObj = {};
			learnObj.language = profile.lang6;
			learnObj.level = profile.lang6_level;
			learningList.push(learnObj);
		}
		return learningList;
	}
	
	$scope.teachLanguage = function( obj ) {
		console.log("teachLanguage");
		var result = [];
		if ( obj.tea1 ) {
			var lang = {};
			lang.language = angular.copy( obj.tea1 );
			lang.level = 0;
			lang.type = 4;
			result.push( lang );
		}
		if ( obj.tea2 ) {
			var lang = {};
			lang.language = angular.copy( obj.tea2 );
			lang.level = 0;
			lang.type = 4;
			result.push( lang );
		}
		if ( obj.tea3 ) {
			var lang = {};
			lang.language = angular.copy( obj.tea3 );
			lang.level = 0;
			lang.type = 4;
			result.push( lang );
		}
		return result;
	}
	
	$scope.getScrollbarWidth = function() {
	    var outer = document.createElement("div");
	    outer.style.visibility = "hidden";
	    outer.style.width = "100px";
	    outer.style.msOverflowStyle = "scrollbar"; // needed for WinJS apps

	    document.body.appendChild(outer);

	    var widthNoScroll = outer.offsetWidth;
	    // force scrollbars
	    outer.style.overflow = "scroll";

	    // add innerdiv
	    var inner = document.createElement("div");
	    inner.style.width = "100%";
	    outer.appendChild(inner);        

	    var widthWithScroll = inner.offsetWidth;

	    // remove divs
	    outer.parentNode.removeChild(outer);

	    return widthNoScroll - widthWithScroll;
	}
	
	$scope.headerPaddingOn = function() {
		var scrollWidth = $scope.getScrollbarWidth();
		$scope.originalMarginLeft = Number( angular.element( '.header-main-menu' ).css( 'margin-left' )
											.replace( 'px', '' ) );
		var padding = $scope.originalMarginLeft - scrollWidth;
		console.log( 'padding', padding );
		//angular.element( '.tellpin-user-menu' ).css( 'margin-left', 
		//											padding + 'px');
		angular.element( '.header-main-menu' ).css( 'margin',
													'0px ' + $scope.originalMarginLeft + 'px' );
		angular.element( '.notification' ).css( 'padding-right',
												scrollWidth + 'px' );
	}
	
	$scope.headerPaddingOff = function() {
		//angular.element( '.tellpin-user-menu' ).css( 'margin-left', 
		//									$scope.originalMarginLeft + 'px' );
		angular.element( '.header-main-menu' ).css( 'margin', 'auto' );
		angular.element( '.notification' ).css( 'padding-right', '0px' );
		
	}
	
	$scope.hidePopover = function() {
		console.log( 'hide' );
		$scope.bshowup = false;
		$scope.popFlag = 0;
		$scope.hoverFlag = 0;
	}
	
	$scope.showPopover = function() {
		console.log( 'show' );
		$scope.popFlag = true;
	}
	
	$scope.openFriendModal = function() {
		angular.element( '#friendModal' ).modal();
	}
	
	$scope.sendFriendMessage = function() {
		$http.post( '/exchange/newfriend/', { 'response_id' : $scope.userPop.user, 'message' : $scope.friendMessage })
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				//alert( "request success!" );
				$scope.isFriend = result.isSuccess;
				$scope.friend = 1;
				angular.element( '#friendModal' ).modal( 'hide' ); 
			}
		});
	}
	
	$scope.intervalMessage = function() {
		$scope.messagePoll = $interval( function() {
//			$http.post( '/mypage/message/check/' )	modified by sch
			$http.post('/dashboard/message/check/')
			.success( function( result ) {
				$scope.msgCount = result.msgCount;
			});
			
			angular.element($("#tellpin-header")).scope().get_noti();
			
		}, 60000);
		
	}
	
	$scope.checkMessage = function() {
//		console.log(' calll check message () ');
//		$http.post( '/mypage/message/check/' )	modified by sch
		$http.post( '/dashboard/message/check/' )
		.success( function( result ) {
//			console.log(' calll check message 2() ');
//			console.log( $scope.msgCount );
			$scope.msgCount = result.msgCount;
//			console.log( $scope.msgCount );
		});	
	}
	
	 $scope.checkMessage(); // message polling
	 $scope.intervalMessage();
}]);

tellpinApp.controller( 'tutorFindRootController', ['$scope', '$window', '$http', '$timeout', function( $scope, $window, $http, $timeout ) {
	$scope.$on('messageEventEmit', function (event, args) {
		$scope.$broadcast('messageEventBroadcast', { type: args.type, user_id: args.user_id });
	});
}]);

tellpinApp.controller( 'msgCtrl', ['$scope', '$window', '$http', '$timeout', '$filter', '$interval', function( $scope, $window, $http, $timeout, $filter, $interval ) {

	$scope.$on('messageEventBroadcast', function (event, args) {
		console.log("messageEventBroadcast");
		console.log(args);
		$scope.openMessage();
		$scope.messageType(args.type,args.user_id);
	});
	
	$scope.openMessage = function() {
		$scope.type = 'list';
		$scope.oldType = 'list';
//		$http.post( '/mypage/messages/' )	modified by sch
		$http.post('/dashboard/messages/')
		.success( function( result ) {
			console.log( result );
			$scope.message = result.message;
			$scope.rvMessage = result.rvMessage;
			$scope.$parent.headerPaddingOn();
			angular.element( '#allMessageModal' ).on( 'hidden.bs.modal', function( e ) {
				$scope.$parent.headerPaddingOff();
				
				// 창이 닫히면 실시간 채팅 기능도 종료
				$scope.stopNewMessagePoll();
			});
			angular.element( '#allMessageModal' ).modal( {
				backdrop: 'static'
			});
		});
	}
	
	$scope.messageType = function( type, user_id ) {
		console.log( "####", type, user_id);
		if ( type == 'detail' ) {
			$scope.to_id = user_id;
			$scope.msg = '';
//			$http.post( '/mypage/message/user/', { 'user_id' : $scope.to_id } )	modified by sch
			$http.post( '/dashboard/message/user/', { 'user_id' : $scope.to_id } )
			.success( function( result ) {
				console.log( result );
				$scope.detail = result.userMessage;
				for ( var i = 0; i < $scope.detail.messageList.length; i ++ ) {
					$scope.detail.messageList[i].message = ( $scope.detail.messageList[i].message ).replace( /\n/gi, '<br />' );
				}
				$scope.oldType = $scope.type;
				$scope.type = type;
				//var elem = angular.element( '#detailMessage' );
				$filter('displayTime');
				$timeout(function(){
					$("#detailMessage").scrollTop($("#detailMessage")[0].scrollHeight);
					
				},450);
				$scope.$parent.checkMessage();
			});
			
			// 실시간 채팅을 위한 message poll 시작
			$scope.startNewMessagePoll();
			
		}
		else if ( type == 'list' ) {
//			$http.post( '/mypage/messages/' )	modified by sch
			$http.post( '/dashboard/messages/' )
			.success( function( result ) {
				console.log( result );
				$scope.message = result.message;
				$scope.oldType = $scope.type;
				$scope.type = type;
			});
			
			// 메세지 리스트로 이동하면 실시간 채팅 기능도 종료
			$scope.stopNewMessagePoll();
		}
		else {
//			$http.post( '/mypage/message/user/list/' )	modified by sch
			$http.post( '/dashboard/message/user/list/' )
			.success( function( result ) {
				console.log( result );
				$scope.userList = result.userList;
				$scope.oldType = $scope.type;
				$scope.type = type;
			});
			
			// 메세지 보낼 사람 선택하는 화면으로 이동하면 실시간 채팅 기능도 종료
			$scope.stopNewMessagePoll();
		}
	}
	
	$scope.enterCheck = function( event ) {
		if ( event.keyCode == 13 ) {
			if ( $scope.msg ) {
				angular.element( '#allMessageModal' ).modal( 'hide' );
				$scope.sendMessage();				
			}
			else {
				$scope.msg = $scope.msg.replace( /\n+/gi, '' );
			}
		}
	}
	
	$scope.sendMessage = function() {
		console.log( $scope.msg );
		console.log( $scope.to_id );
		if ( !$scope.msg || $scope.msg == '' ) {
			return;
		}
//		$http.post( '/mypage/message/send/', { 'msg' : $scope.msg, 'to_id' : $scope.to_id } )	modified by sch
		$http.post( '/dashboard/message/send/', { 'msg' : $scope.msg, 'to_id' : $scope.to_id } )
		.success( function( result ) {
			$scope.msg = '';
//			$http.post( '/mypage/message/user/', { 'user_id' : $scope.to_id } )	modified by sch
			$http.post( '/dashboard/message/user/', { 'user_id' : $scope.to_id } )
			.success( function( result ) {
				console.log( result );
				$scope.detail = result.userMessage;
				for ( var i = 0; i < $scope.detail.messageList.length; i ++ ) {
					$scope.detail.messageList[i].message = ( $scope.detail.messageList[i].message ).replace( /\n/gi, '<br />' );
				}
				//$scope.oldType = $scope.type;
				//$scope.type = type;
				var elem = angular.element( '#detailMessage' );
				$timeout(function(){
					$("#detailMessage").scrollTop($("#detailMessage")[0].scrollHeight);
					
				},450);
			});
		});
	}
	
	$scope.deleteMessage = function() {
//		$http.post( '/mypage/message/delete/', { 'mid' : $scope.deleteMid , 'you_id' : $scope.deleteYouid } )	modified by sch
		$http.post( '/dashboard/message/delete/', { 'mid' : $scope.deleteMid , 'you_id' : $scope.deleteYouid } )
		.success( function( result ) {
			if ( result.isSuccess == 1 ) {
				$scope.hideDelete();
				$scope.message = result.message;
			}
		});
	}
	
	$scope.clickDeleteMsgModal = function( mid, you_id ) {
		$scope.deleteMid = mid;
		$scope.deleteYouid = you_id;
		$( '#messageDelete' ).modal();
		
	}
	
	$scope.hideDelete = function() {
		$( '#messageDelete' ).modal( 'hide' );
	}
	
	// 실시간 채팅을 위한 message poll 변수
	var newMessagePoll;
	
	// 실시간 채팅 시작 함수
	$scope.startNewMessagePoll = function() {
		if (angular.isDefined(newMessagePoll)) { return; }
		
		newMessagePoll = $interval( function() {
			console.log($scope.to_id);
			$http.post('/dashboard/message/check_new/', {'you_id':$scope.to_id})
			.success( function (result ) {
				console.log(result.newMsg);
				if (result.newMsg) {
					//$scope.msg = '';
					$http.post( '/dashboard/message/user/', { 'user_id' : $scope.to_id } )
					.success( function( result ) {
						console.log( result );
						$scope.detail = result.userMessage;
						for ( var i = 0; i < $scope.detail.messageList.length; i ++ ) {
							$scope.detail.messageList[i].message = ( $scope.detail.messageList[i].message ).replace( /\n/gi, '<br />' );
						}
						$scope.oldType = $scope.type;
						$scope.type = 'detail';
						//var elem = angular.element( '#detailMessage' );
						$filter('displayTime');
						$timeout(function(){
							$("#detailMessage").scrollTop($("#detailMessage")[0].scrollHeight);
							
						},450);
						$scope.$parent.checkMessage();
					});
				}
			})
				
		}, 3000);
	}
	//
	
	// 실시간 채팅 종료 함수
	$scope.stopNewMessagePoll = function() {
		if (!angular.isDefined(newMessagePoll)) return;
		
		$interval.cancel(newMessagePoll)
		newMessagePoll = undefined;
	}
	
}]);