//var dashboardApp = angular.module('dashboardApp', ['ngSanitize']);

//dashboardApp.config(function($interpolateProvider) {
//    $interpolateProvider.startSymbol('[[');
//    $interpolateProvider.endSymbol(']]');
//});

function play_video(){
	/*
	$("#video_left").css("display","block");
	$("#video_right").css("display","block");
	$(".slide3-video-left").get(0).play();
	$(".slide3-video-right").get(0).play();
	*/
	console.log('play_video');
	$(".introPopup").css("display", "block");
	$("#i-player").load();
	$("#i-player").get(0).play();
}
function end_video(){
	console.log('ended');
	$(".introPopup").css("display", "none");
	//$("#i-player")[0].contentWindow.postMessage('{"event":"command", "func":"pauseVideo", "args":""}', '*');
	$("#i-player").get(0).pause();
	console.log('ended2');
	//$("#video_left").css("display","none");
//	$("#video_right").css("display","none");
}
var timer;

tellpinApp.controller("dashboardController", function($scope, $http, $location, $window, $sce, $timeout){
	$scope.initDashboard = function(language){
		$scope.language = language;
		$scope.setData();
	}

	$scope.loading = function(){
		$scope.loading = false;
	}
	$scope.setData = function(){
		// $scope.loading = true;
		$scope.loading = false;
		$http.post( "/dashboard/ajax_dashboard/").success( function( result ) {
			console.log('result: ');
			console.log(result);
			if ( result.isSuccess == 1 ){
				var data = result;
				$scope.profile = data.profile;
				$scope.reservationInfo = data.reservationInfo;
				$scope.nationsInfo = data.nationsInfo;
				$scope.languageInfo = data.languageInfo;
				$scope.myTutors = data.myTutors;
				$scope.myTutees = data.myTutees;
				$scope.myFriends = data.myFriends;
				$scope.all = data.all;
				$scope.allShowMore = data.allShowMore;
				$scope.LQs = data.LQs;
				$scope.LQShowMore = data.LQShowMore;
				$scope.TPs = data.TPs;
				$scope.TPShowMore = data.TPShowMore;
				$scope.LBs = data.LBs;
				$scope.LBShowMore = data.LBShowMore;
				$scope.FAQs = data.faqs;
				$scope.allHistory;
				$scope.selectedHistoryIndex;
				$scope.selectedHistory;
				
				$scope.getReservationNum();

				$http.post( "/tools/gethistory/").success( function( result ) {
					if ( result.isSuccess == 1 ) {
						console.log(result);
						$scope.allHistory = result.historylist;
						$scope.selectedHistoryIndex = 0;
						$scope.selectedHistory = $scope.allHistory[$scope.selectedHistoryIndex];
					}
				});
				
				if ($scope.allHistory) {
					timer = setInterval(function() {
						if($scope.selectedHistoryIndex == $scope.allHistory.length - 1){
							$scope.selectedHistoryIndex = 0;
						} else {
							$scope.selectedHistoryIndex += 1;
						}
						$scope.selectedHistory = $scope.allHistory[$scope.selectedHistoryIndex];
					}, 3000);					
				}
				
//				var city_list = [];
//				if(!($scope.profile.livein == null || $scope.profile.livein == 0)){
//					$scope.profile.livein_name = $scope.nationsInfo[$scope.profile.livein - 1].name;
//					city_list =  $scope.nationsInfo[$scope.profile.livein - 1].city_list;
//				}
//				for(var i = 0; i < city_list.length; i++){
//					if(city_list[i].id ==  $scope.profile.livein_city){
//						$scope.profile.livein_city_name = city_list[i].name;
//						break;
//					}
//				}
				$scope.skillImages = ["gauge_000.png", "gauge_001.png", "gauge_002.png", "gauge_003.png", "gauge_004.png", "gauge_005.png", "gauge_005_red.png", ]
				$scope.ratingImages = ["icon_star_001.png", "icon_star_002.png", "icon_star_003.png", "icon_star_004.png", "icon_star_005.png", "icon_star_006.png", "icon_star_007.png", "icon_star_008.png", "icon_star_009.png", "icon_star_010.png", "icon_star_011.png"]
				
				$scope.suggestTutorStart = 0;
				$scope.suggestLEStart = 0;
				if($scope.profile.is_tutor == 0){
					$(".dash-as-tabs .dash-last-tab").css("width","270px");
					$scope.selectedAsTab = 1;
				}
				else{
					$scope.selectedAsTab = 0;
				}
				$scope.selectedMyTab = 0;
				for(var idx = 0; idx < $scope.myFriends.length; idx++){
					$scope.myFriends[idx].learningList = $scope.getLearningList($scope.myFriends[idx]);
				}
				for(var idx = 0; idx < $scope.myTutors.length; idx++){
					$scope.myTutors[idx].teachesList = $scope.getTeachesList($scope.myTutors[idx]);
				}
				for(var idx = 0; idx < $scope.myTutees.length; idx++){
					$scope.myTutees[idx].learningList = $scope.getLearningList($scope.myTutees[idx]);
				}
				$scope.selectedCommunityTab = 0;
				$scope.loading = false;
				
				$scope.getSuggestAll();
			}
		}).error(function(result){
			console.log( 'erorr');
		});
		console.log("end");
		
	}

	$scope.getReservationNum = function() {
		$scope.tutorReservationNum = 0;
		$scope.tuteeReservationNum = 0;
		if ($scope.reservationInfo.tutor)
			$scope.tutorReservationNum = $scope.reservationInfo.tutor.action_required + $scope.reservationInfo.tutor.waiting_for_action + $scope.reservationInfo.tutor.not_scheduled +  $scope.reservationInfo.tutor.in_problem;
		if ($scope.reservationInfo.tutee)
			$scope.tuteeReservationNum = $scope.reservationInfo.tutee.action_required + $scope.reservationInfo.tutee.waiting_for_action + $scope.reservationInfo.tutee.not_scheduled + $scope.reservationInfo.tutee.in_problem;
	}
	$scope.tellpinSearchNext = function(direction){
		console.log("tellpinSearchNext");
		clearInterval(timer);
		timer = setInterval(function() {
			if($scope.selectedHistoryIndex == $scope.allHistory.length - 1){
				$scope.selectedHistoryIndex = 0;
			} else {
				$scope.selectedHistoryIndex += 1;
			}
			$scope.selectedHistory = $scope.allHistory[$scope.selectedHistoryIndex];
		}, 3000);
		
		if(direction == 'right'){
			if($scope.selectedHistoryIndex == $scope.allHistory.length - 1){
				$scope.selectedHistoryIndex = 0;
			} else {
				$scope.selectedHistoryIndex += 1;
			}
		} else if(direction == 'left'){
			if($scope.selectedHistoryIndex == 0){
				$scope.selectedHistoryIndex = $scope.allHistory.length - 1;
			} else {
				$scope.selectedHistoryIndex -= 1;
			}
		}
		$scope.selectedHistory = $scope.allHistory[$scope.selectedHistoryIndex];
	}
	$scope.searchWord = function(){
		urlArray = $scope.selectedHistory.result_url1.split("@^tskw^@");
		search_url = "";
		if(urlArray.length == 2){
			search_url = urlArray[0] + encodeURIComponent($scope.selectedHistory.word) + urlArray[1];
		} else if(urlArray.length == 1) {
			search_url = urlArray + encodeURIComponent($scope.selectedHistory.word);
		}
		search = $window.open(search_url, 'search', 'width=900, height=600');
	}
	// $scope.initLanguage = function(){
	// 	$scope.setData();
	// }
	$scope.goLessonList = function(_filter){
		console.log('goLessonList');
		//selectedAsTab == 0 ? '/tutorlessons/' : '/tuteelessons/'
//		if( $scope.selectedAsTab == 0 ){
//			$window.location.href = '/tutorlessons/';
//		}else{
//			$window.location.href = '/tuteelessons/';
//		}
		var form = document.createElement("form");
		var input = document.createElement("input");
		var i = document.createElement("input");
		var count = 0;
		
		if (_filter == "Action required") {
			if( $scope.selectedAsTab == 0 ){
				count = $scope.reservationInfo.tutor.action_required;
			} else {
				count = $scope.reservationInfo.tutee.action_required;
			}
		} else if (_filter == "Waiting for action") {
			if( $scope.selectedAsTab == 0 ){
				count = $scope.reservationInfo.tutor.waiting_for_action;
			} else {
				count = $scope.reservationInfo.tutee.waiting_for_action;
			}
		} else if (_filter == "Not scheduled") {
			if( $scope.selectedAsTab == 0 ){
				count = $scope.reservationInfo.tutor.not_scheduled;
			} else {
				count = $scope.reservationInfo.tutee.not_scheduled;
			}
		} else if (_filter == "In Problem") {
			if( $scope.selectedAsTab == 0 ){
				count = $scope.reservationInfo.tutor.in_problem;
			} else {
				count = $scope.reservationInfo.tutee.in_problem;
			}
		}
		if( $scope.selectedAsTab == 0 ){
			if (count > 0) {
				form.action = "/tutorlessons/";
			} else {
				return;
			}
		}
		else{
			if (count > 0) {
				form.action = "/tuteelessons/";
			} else {
				return;
			}
		}
		form.method = "post";
		
		i.name = "search_name";
		i.value = _filter;

		form.appendChild(i);
		document.body.appendChild(form);
		form.submit();
	}
	
	$scope.getLearningList = function(profile){
		var learningList = [];
		if(profile.lang1_learning == true){
			var learnObj = {};
			learnObj.language = profile.lang1;
			learnObj.level = profile.lang1_level;
			learningList.push(learnObj);
		}
		if(profile.lang2_learning == true){
			var learnObj = {};
			learnObj.language = profile.lang2;
			learnObj.level = profile.lang2_level;
			learningList.push(learnObj);
		}
		if(profile.lang3_learning == true){
			var learnObj = {};
			learnObj.language = profile.lang3;
			learnObj.level = profile.lang3_level;
			learningList.push(learnObj);
		}
		if(profile.lang4_learning == true){
			var learnObj = {};
			learnObj.language = profile.lang4;
			learnObj.level = profile.lang4_level;
			learningList.push(learnObj);
		}
		if(profile.lang5_learning == true){
			var learnObj = {};
			learnObj.language = profile.lang5;
			learnObj.level = profile.lang5_level;
			learningList.push(learnObj);
		}
		if(profile.lang6_learning == true){
			var learnObj = {};
			learnObj.language = profile.lang6;
			learnObj.level = profile.lang6_level;
			learningList.push(learnObj);
		}
		return learningList;
	};
	$scope.getTeachesList = function(profile){
		var teachesList = [];
		if(profile.teaching1 != null){
			var teachObj = {};
			teachObj.language = profile.teaching1;
			teachesList.push(teachObj);
		}
		if(profile.teaching2 != null){
			var teachObj = {};
			teachObj.language = profile.teaching2;
			teachesList.push(teachObj);
		}
		if(profile.teaching3 != null){
			var teachObj = {};
			teachObj.language = profile.teaching3;
			teachesList.push(teachObj);
		}
		return teachesList;
	}
	$scope.getSpeaksList = function(profile){
		var speaksList = [];
		if(profile.native1 != null && profile.native1 != 0){
			var speakObj = {};
			speakObj.language = profile.native1;
			speakObj.level = 7;
			speaksList.push(speakObj);
		}
		if(profile.native2 != null && profile.native2 != 0){
			var speakObj = {};
			speakObj.language = profile.native2;
			speakObj.level = 7;
			speaksList.push(speakObj);
		}
		if(profile.native3 != null && profile.native3 != 0){
			var speakObj = {};
			speakObj.language = profile.native3;
			speakObj.level = 7;
			speaksList.push(speakObj);
		}
		if(profile.lang1 != null){
			var speakObj = {};
			speakObj.language = profile.lang1;
			speakObj.level = profile.lang1_level;
			speaksList.push(speakObj);
		}
		if(profile.lang2 != null){
			var speakObj = {};
			speakObj.language = profile.lang2;
			speakObj.level = profile.lang2_level;
			speaksList.push(speakObj);
		}
		if(profile.lang3 != null){
			var speakObj = {};
			speakObj.language = profile.lang3;
			speakObj.level = profile.lang3_level;
			speaksList.push(speakObj);
		}
		if(profile.lang4 != null){
			var speakObj = {};
			speakObj.language = profile.lang4;
			speakObj.level = profile.lang4_level;
			speaksList.push(speakObj);
		}
		if(profile.lang5 != null){
			var speakObj = {};
			speakObj.language = profile.lang5;
			speakObj.level = profile.lang5_level;
			speaksList.push(speakObj);
		}
		if(profile.lang6 != null){
			var speakObj = {};
			speakObj.language = profile.lang6;
			speakObj.level = profile.lang6_level;
			speaksList.push(speakObj);
		}
		return speaksList;
	}
	$scope.getSuggestAll = function(){
//		console.log('getSuggestAll');
		$scope.getSuggestTutor();
		$scope.getSuggestLE();
	}
	$scope.getSuggestTutor = function(){
		console.log('getSuggestTutor');
//		console.log($scope.profile.learns);
		$scope.$parent.popOverBodyAppend();
		var learnings = [];
		for(i=0; i < $scope.profile.learns.length; i++) {
			learnings.push($scope.profile.learns[i].id);
		}
		$http.post("/dashboard/get_suggest_tutor/",{learnings: learnings, start: $scope.suggestTutorStart}).success(function(response){
			console.log(response.suggestTutor);
	        $scope.suggestTutor = response.suggestTutor;
	        for(var idx = 0; idx < $scope.suggestTutor.length ; idx++){
	        	$scope.suggestTutor[idx].speaksList = $scope.getSpeaksList($scope.suggestTutor[idx]);
	        	$scope.suggestTutor[idx].teachesList = $scope.getTeachesList($scope.suggestTutor[idx]);
	        }
	        $scope.suggestTutorStart = response.suggestTutorStart;
		});
	}
	$scope.getSuggestLE = function(){
		console.log('getSuggestLE');
		var learnings = [];
		$scope.$parent.popOverBodyAppend();
		for(i=0; i < $scope.profile.learns.length; i++) {
			learnings.push($scope.profile.learns[i].id);
		}
		$http.post("/dashboard/get_suggest_le/",{learnings: learnings, start: $scope.suggestLEStart}).success(function(response){
			console.log(response.suggestLE);
	        $scope.suggestLE = response.suggestLE;
	        for(var idx = 0; idx < $scope.suggestLE.length ; idx++){
	        	$scope.suggestLE[idx].learningList = $scope.getLearningList($scope.suggestLE[idx]);
	        	$scope.suggestLE[idx].speaksList = $scope.getSpeaksList($scope.suggestLE[idx]);
	        }
	        $scope.suggestLEStart = response.suggestLEStart;
		});
	}
	$scope.showData = function(){
		console.log($scope.profile);
	}
	$scope.goURL = function(url){
		$window.location.href = (url);
		$window.event.stopPropagation();
	}
	$scope.goCommunityDetail = function(category, id){
		
		if(category == 'Board')
			$window.location.href = ('/board/'+ id);
		else if(category == 'TranslationProofreading')
			$window.location.href = ('/trans/'+ id);
		else if(category == 'Question')
			$window.location.href = ('/questions/'+ id);
		$window.event.stopPropagation();
	}
	$scope.sendMessage = function (msg_type, id) {
		$scope.$emit('messageEventEmit', { type: msg_type, user_id: id });
		$window.event.stopPropagation();
	};
	/*2016.04.01 khyun*/
	//step 1
	$scope.get_lang = function(){
		console.log('get_lang()');
		var native = $("#selected_native").chosen().val();
		var learning = $("#selected_learning").chosen().val();

		//TODO
		//최소 1개 입력 check code
		if( native == null || learning == null){
			if( native == null ){
				$(".warning-native").text('Please select at least one language');
			}
			if( learning == null ){
				$(".warning-learning").text('Please select at least one language');
			}
			return;
		}
		if( native != null ){
			for( var i=0; i < native.length ; i++){
				console.log( 'native: ' + native[i] + ', type:'+typeof(native[i]));
			}
		}
		if( learning != null ){
			for( var i=0; i < learning.length ; i++){
				console.log( 'learning: ' + learning[i] + ', type:'+typeof(learning[i]));
			}
		}
		
		$scope.native = native;
		$scope.learning = learning;

		//tab2
		$(".profile #tab1").removeClass("selected_tab");
		$(".profile #tab1").addClass("unselected_tab");
		$(".profile #tab2").removeClass("unselected_tab");
		$(".profile #tab2").addClass("selected_tab");

		var utc_time = minutesToUTC(-(new Date().getTimezoneOffset()));
		console.log(utc_time);
	};
	$scope.change_native = function(native, learning){
		console.log('change_native()');
		$scope.native_list = native;
		
		//console.log( $("#selected_learning").val(2).val() );
		
		//$("#selected_learning option[value='2']").removeClass("active-result");
		//$("#selected_learning option[value='2']").addClass("result-selected");
		
		
		//$("#selected_learning").empty();
	
		//$("#selected_learning").trigger('chosen:updated');
	}
	$scope.exclude_native = function (obj){
		/*console.log('exclude_native()');
		if( $scope.native_list ){
			console.log( 'if ' );
			for(var i=0; i<$scope.native_list; i++){
				if( $scope.native_list[0].parseInt() == obj.id ){
					return false;
				}else{
					return true;
				}
			}
		}else{
			return true;
		}*/
		return true;
	};
	$scope.exclude_learning = function (id){
		//console.log( $scope.learning_list );
		return true;
	};
	//step 2
	$scope.done = function(){
		console.log('done()');
		var utc_time = minutesToUTC(-(new Date().getTimezoneOffset()));

		$(".profile #tab2").addClass("unselected_tab");
		$("#pop-profile-template").css('background', 'none');
		$("#pop-profile-template").append("<img id='loading_bar' src='/static/img/ajax_loading.gif' style='position:absolute; top:calc(50% - 48px); left:calc(50% - 48px);' >");
		
		var obj = {
					'native': $scope.native,
					'timezone': utc_time,
					'learning': $scope.learning,
					'interest_teaching': $("#chk-teaching").is(":checked"),
					'interest_learning': $("#chk-learning").is(":checked"),
					'interest_finding': false
				  }

		//TODO
		//language save
		//해당함수에서 레벨 2로 설정
		$http.post("/user_auth/profile/", { obj : obj }).success(function(result) {
			if( result.isSuccess ==1 ){
				//window.location.href = '/main/';
				$("#pop-profile-template").css('background', '');
				$(".profile #tab3").addClass("selected_tab");
				$(".profile #tab3").removeClass("unselected_tab");
				$("#pop-profile-template #loading_bar").remove();
				
			}
		});
		
	}
	//step 3
	$scope.confirm_email = function(){
		console.log('confirm_email()');
		
		//TODO 
		//send email
		//post email 보내고 종료. 해당 함수에서 redirect
		
		$("#pop-profile-template").css("z-index", "1");
		$("#tutor-schedule-template").css("display", "block");
		window.location.href = "/user_auth/complete/" + $("#user_email").text().trim();
		
		
		
		
		/*$http.post("/user_auth/complete/", { obj }).success(function(result){
			if( result.isSuccess == 1 ){
				
			}
		});*/
		
	}
	//resend
	$scope.resend_email = function(){
		console.log('resend_email()');
		var obj = {
				'email':  $("#user_email").text().trim()
		}
		
		console.log(obj);
		$http.post("/user_auth/resend_email/", {obj:obj}).success(function(response){
	       alert("resend_success");
		});
	}
	$scope.click_didnt = function(){
		console.log('didnt');
		$(".span-didnt-receive").toggle();
		$(".span-success-receive").toggle();
	}
	//show change input 
	$scope.show_change = function(){
		console.log('show_change()');
		$(".div-submit-email").toggle('display');
		$(".span-didnt-receive").css('display', 'block');
		$(".span-success-receive").css('display', 'none');
	}
	//click submit button
	$scope.change_email = function(){
		
		console.log('change_email');
		if( !email_check() ){
			return;
		}
		//TODO
		//user email validation check
		
		//TODO
		//user email save - post 
		var obj = {
					'email': $("#change_email").val()
		}
		
		$http.post("/user_auth/ajax_check_email/", { obj:obj }).success(function(response){
			result = response;
			console.log(result);
			console.log(result.isSuccess);
			if( result.isSuccess ){
				$("#user_email").text( $("#change_email").val() );
				$(".div-submit-email").toggle();
				$(".span-didnt-receive").toggle();
				$(".span-success-receive").css('display', 'block');
				$scope.resend_email();
			}else{
				alert(result.message);
			}
		});
		
	}

	$http.post("/tutor/apply/getApplyStatus/").success(function(response) {
		if ( response.isSuccess == 1) {
			console.log("apply status");
			console.log(response.status);
			$scope.applyStatus = response.status;
			
		}
	});
	
});
String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};
Date.prototype.format = function(f) {
    if (!this.valueOf()) return " ";
 
    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;
     
    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).zf(2);
            case "MM": return (d.getMonth() + 1).zf(2);
            case "dd": return d.getDate().zf(2);
            case "E": return weekName[d.getDay()];
            case "HH": return d.getHours().zf(2);
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case "mm": return d.getMinutes().zf(2);
            case "ss": return d.getSeconds().zf(2);
            case "a/p": return d.getHours() < 12 ? "오전" : "오후";
            default: return $1;
        }
    });
};