from django.conf.urls import url, include

import views
import exchange.views
from mypage.messageService import getUserMessage

urlpatterns = [
    url(r'^view/profile/$', views.view_profile),
    url(r'^upload/$', views.upload),
    url('', include('social.apps.django_app.urls', namespace='social')),
#     url(r'^messages/$', views.getMessage),
#     url(r'^message/user/$', views.getUserMessage),
#     url(r'^message/user/list/$', views.getMessageUserList),
#     url(r'^message/send/$', views.sendMessage),
#     url(r'^message/delete/$', views.deleteMessage),
#     url(r'^message/check/$', views.checkMessage),
]
