#-*- coding: utf-8 -*-

###############################################################################
# filename    : mypage > messageService.py
# description : 메세지 처리 관련 py
# author      : msjang@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 msjang 최초 작성
#
#
###############################################################################

from django.db import connection
from common import common
# from common.models import Message
# from common.models import TellpinUser
from datetime import datetime

PROFILE_IMAGE_FILE_DIR = "/static/img/upload/profile"

def getMessage( user_id ):
    """
    @summary: 해당 유저와 상대방들과의 메세지 목록
    @author: msjang
    @param user_id: 해당 유저 id 번호
    @return: result( list )
    """
    result = []
    try :
        cursor = connection.cursor()
        # 전체목록 쿼리
        sql = '''
            SELECT T.*
            , ( COUNT(*) - ( 
                            SELECT COUNT(*) AS rcount 
                            FROM message 
                            WHERE from_user_id = %s
                            AND to_user_id = ( CASE WHEN t.to_user_id = %s THEN t.from_user_id ELSE t.to_user_id END )
                            AND ( from_del_yn =  ( CASE WHEN from_user_id = %s THEN 0 ELSE 1 END ) OR to_del_yn = ( CASE WHEN to_user_id = %s THEN 0 ELSE 1 END )  )
                           ) ) - ( 
                            SELECT COUNT(*) AS rcount 
                            FROM message 
                            WHERE to_user_id = %s
                            AND from_user_id = ( CASE WHEN t.from_user_id = %s THEN t.to_user_id ELSE t.from_user_id END )
                            AND read_yn = 1
                            AND ( from_del_yn =  ( CASE WHEN from_user_id = %s THEN 0 ELSE 1 END ) OR to_del_yn = ( CASE WHEN to_user_id = %s THEN 0 ELSE 1 END )  )
                            ) as read_count        
            FROM (
                    SELECT *, 
                           CASE WHEN to_user_id = %s
                           THEN ( SELECT NAME FROM user_auth_tellpinuser WHERE id = from_user_id )
                           ELSE ( SELECT NAME FROM user_auth_tellpinuser WHERE id = to_user_id )
                           END AS name,
                           CASE WHEN to_user_id = %s
                           THEN ( SELECT profile_photo FROM user_auth_tellpinuser WHERE id = from_user_id )
                           ELSE ( SELECT profile_photo FROM user_auth_tellpinuser WHERE id = to_user_id )
                           END AS photo,
                           CASE WHEN to_user_id = %s
                           THEN ( SELECT id FROM user_auth_tellpinuser WHERE id = from_user_id )
                           ELSE ( SELECT id FROM user_auth_tellpinuser WHERE id = to_user_id )
                           END AS you_id,
                           ( SELECT id FROM user_auth_tellpinuser WHERE id = %s ) AS my_id
                    FROM message
                    WHERE (to_user_id = %s OR from_user_id = %s )
                      AND ( from_del_yn =  ( CASE WHEN from_user_id = %s THEN 0 ELSE 1 END ) OR to_del_yn = ( CASE WHEN to_user_id = %s THEN 0 ELSE 1 END )  ) 
                    ORDER BY created_time DESC
                ) AS T
            GROUP BY you_id
            ORDER BY created_time DESC;
            '''
        cursor.execute( sql, [ user_id, user_id, user_id, user_id,
                               user_id, user_id, user_id, user_id,
                               user_id, user_id, user_id, user_id, user_id, user_id, user_id, user_id ] )
        for i in common.dictfetchall( cursor ) :
            i['photo'] = PROFILE_IMAGE_FILE_DIR + '/' + i['photo']
            i['created_time'] = str( i['created_time'] )
            result.append( i )
    except Exception as e :
        print str( e )
    finally :
        cursor.close()
        return result


def getUserMessage( my_id, you_id ):
    """
    @summary: 해당유저와 상대방 유저의 상세 메세지 목록
    @author: msjang
    @param my_id: 내 유저 id 번호
    @param you_id: 상대방 유저 id 번호
    @return: obj( object )
    """
    obj = {}
    try :
        result = []
        cursor = connection.cursor()
        # 메세지 모두 읽음 처리
        sql = '''
            UPDATE message
            SET read_yn = 1
            WHERE to_user_id = %s
            AND from_user_id = %s
            AND read_yn = 0
        '''
        cursor.execute( sql, [ my_id, you_id ] )
        # 메세지 목록 가져오기
        sql = '''
                SELECT *,
                        ( SELECT name FROM user_auth_tellpinuser WHERE id = from_user_id ) AS name,
                        ( SELECT profile_photo FROM user_auth_tellpinuser WHERE id = from_user_id ) AS photo,
                        ( SELECT id FROM user_auth_tellpinuser WHERE id = %s ) AS my_id,
                        ( SELECT name FROM user_auth_tellpinuser WHERE id = %s ) AS you_name
                FROM message
                WHERE to_user_id IN ( %s, %s )
                AND from_user_id IN ( %s, %s )
                AND ( from_del_yn =  ( CASE WHEN from_user_id = %s THEN 0 ELSE 1 END ) OR to_del_yn = ( CASE WHEN to_user_id = %s THEN 0 ELSE 1 END )  )
                ORDER BY created_time;
            '''
        cursor.execute( sql, [ my_id, you_id,
                               my_id, you_id,
                               my_id, you_id,
                               my_id, my_id ] )
        for i in common.dictfetchall( cursor ) :
            i['photo'] = PROFILE_IMAGE_FILE_DIR + '/' + i['photo']
            i['created_time'] = str( i['created_time'] )
            result.append( i )
        
        obj['name'] = TellpinUser.objects.get( id = you_id ).name
        obj['messageList'] = result
    except Exception as e :
        print str( e )
    finally :
        cursor.close()
        return obj


def getMessageUserList( user_id ):
    """
    @summary: 해당유저가 메세지 보낼수 있는 유저 목록
    @author: msjang
    @param user_id: 해당 유저 id 번호
    @return: result( list )
    """
    result = []
    try :
        cursor = connection.cursor()
        # 내 친구, 튜터, 튜티 목록
        # 나중에 친구 수락 상황 생길경우
        # le_friend where 조건절에 state=3 추가할것!!! 
        # 현재는 그냥 친구 신청한 상대도 가져오는상태
        sql = '''
        
            SELECT t.id, t.name, n.name_en, CONCAT( '/static/img/upload/profile/', t.profile_photo ) AS photo, 1 AS type
            FROM user_auth_tellpinuser t, nation_info n
            WHERE t.from = n.nid
            AND   t.id IN ( SELECT CASE WHEN request_id = %s AND req_del = 0
                                   THEN response_id
                                   WHEN response_id = %s AND res_del = 0
                                   THEN request_id
                                   ELSE 0
                                   END AS user_id
                            FROM le_friend
                            WHERE ( request_id = %s OR response_id = %s )
                              AND state = 3
                            UNION
                            DISTINCT 
                            SELECT CASE WHEN tutor_id = %s
                                        THEN tutee_id
                                        ELSE tutor_id
                                        END AS user_id
                            FROM reservation
                            WHERE tutor_id = %s OR tutee_id = %s
                            UNION
                            SELECT DISTINCT followee_id
                            FROM follow
                            WHERE follower_id = %s
                            AND division = 1
                            UNION
                            SELECT DISTINCT to_user_id
                            FROM message
                            WHERE from_user_id = %s
                           )
        '''
        cursor.execute( sql, [ user_id, user_id, user_id, user_id, user_id, user_id, user_id, user_id, user_id ] )
        result = common.dictfetchall( cursor )
    except Exception as e :
        print str( e )
    finally :
        cursor.close()
        return result


def sendMessage( to_id, from_id, msg ):
    """
    @summary: 메세지 보내는 함수
    @author: msjang
    @param to_id: 받는 사용자 id 번호
    @param from_id: 보내는 사용자 id 번호
    @param msg: 메세지 내용
    @return: 1( success ), 0( fail )
    """
    result = 1
    # to 받는이
    # from 보내는이
    try :
        obj = Message( 
            to_user_id = to_id,
            from_user_id = from_id,
            message = msg
        )
        obj.save()
    except Exception as e :
        print str( e )
        result = 0
    finally :
        return result, obj


def deleteMessage( my_id, you_id, mid ):
    """
    @summary: 특정 유저와의 메세지 목록 삭제( 직접 삭제하지 않고 삭제표시 )
    @author: msjang
    @param my_id: 해당 유저 id 번호
    @param you_id: 특정 유저 id 번호
    @param mid: Message table pk
    @return: 1( success ), 0( fail )
    """
    result = 1
    try :
        cursor = connection.cursor()
        sql = '''
            UPDATE message
            SET to_del_yn = ( CASE WHEN to_user_id = %s THEN 1 ELSE 0 END )
              , from_del_yn = ( CASE WHEN from_user_id = %s THEN 1 ELSE 0 END )
            WHERE to_user_id IN ( %s, %s )
            AND from_user_id IN ( %s, %s )
            AND MID <= %s; 
        '''
        cursor.execute( sql, [ my_id, my_id,
                               my_id, you_id,
                               my_id, you_id, mid ])
    except Exception as e :
        print str( e )
        result = 0
    finally :
        cursor.close()
        return result


def checkReceiveMessage( user_id ):
    """
    @summary: 안 읽은 메세지 유무
    @author: msjang
    @param id: 유저 id 번호
    @return: count( int )
    """
    obj = {}
    try:
        count = Message.objects.filter( to_user_id = user_id, read_yn = 0 ).count()
        cursor = connection.cursor()
        sql = '''
            SELECT ( SELECT COUNT(*) FROM reservation r, rv_message rm WHERE r.rvid = rm.rvid AND tutor_id = %s AND tutor_status = 0 ) AS from_tutee,
                   ( SELECT COUNT(*) FROM reservation r, rv_message rm WHERE r.rvid = rm.rvid AND tutee_id = %s AND tutee_status = 0 ) AS from_tutor
            FROM DUAL
        '''
        cursor.execute( sql, [ user_id, user_id ] )
        obj = common.dictfetchall( cursor )[0]
        obj['msgCount'] = count
    except Exception as e:
        print str( e )
        obj['err'] = str( e )
    finally:
        cursor.close()
        return obj
