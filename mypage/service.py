# -*- coding: utf-8 -*-

###############################################################################
# filename    : mypage > service.py
# description : mypage 관련 service py
# author      : msjang@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 msjang 최초 작성
#
#
###############################################################################

from datetime import datetime, date, timedelta
import time

from _mysql import NULL
from django.forms.models import model_to_dict

# from common.models import NationInfo, CityInfo, Profile, Tools, CurrencyInfo, Resume, Files
# from common.models import TellpinUser, Timezone
from tutor.service4registration import TutorService4Registration
from common import common
from user_auth.models import TellpinAuthUser, TellpinUser
from tutor.models import Tutor, Tutee
from common.models import CountryInfo, CityInfo, TimezoneInfo

ts4r = TutorService4Registration(object)


class MyPageService(object):
    def __init__(self, params):
        '''
        Constructor
        '''



#     def save_user_profile(self, _profile):
#         """
#         @summary: 유저정보 저장 함수
#         @author: msjang
#         @param _profile: 해당 유저 정보 객체
#         @return: user_model.name, user_model.profile_photo, timezone.utc_time, timezone.utc_location
#         """
#         user_model = TellpinUser.objects.get(id=_profile["id"])
#         for field in _profile:
#             if field == 'birthDate':
#                 if _profile[field]["year"] == None:
#                     user_model.birth = ""
#                 else:
#                     birth_date = date(_profile[field]["year"], _profile[field]["month"], _profile[field]["day"])
#                     user_model.birth = birth_date.strftime("%Y%m%d")
#             else:
#                 setattr(user_model, field, _profile[field])
#         user_model.save()
#         timezone = Timezone.objects.get(id=user_model.timezone)
# 
#         return user_model.name, "/static/img/upload/profile/" + user_model.profile_photo, timezone.utc_time, timezone.utc_location

    def get_timezone_info(self):
        """
        @summary: timezone 정보
        @author: msjang
        @param none
        @return: timezone_list( list )
        """
        timezone_list = [];
        for timezone in TimezoneInfo.objects.all():
            timezone_list.append(model_to_dict(timezone))
        return timezone_list

    def get_nations_info(self):
        """
        @summary: nation 정보
        @author: msjang
        @param none
        @return: nation_info( list )
        """
        nations = CountryInfo.objects.all()
        cities = CityInfo.objects.all()

        nation_info = []
        for nation in nations:
            nat_obj = {}
            nat_obj["name"] = nation.country
            nat_obj["id"] = nation.id
            nat_obj["image"] = nation.flag_filename
            nat_obj["city_list"] = []
            for city in cities:
                if city.country_id == nation.id:
                    city_obj = {}
                    city_obj["id"] = city.id
                    city_obj["name"] = city.city
                    nat_obj["city_list"].append(city_obj)
            nation_info.append(nat_obj)
        return nation_info

    def get_tutor_profile(self, user_id):
        """
        @summary: 유저 tutor profile
        @author: msjang
        @param user_id: 유저 id 번호
        @return: profile( object )
        """
        user_model = TellpinUser.objects.get(id=user_id)
        profile = model_to_dict(user_model)
        resumeFile = {}
        index = 0
        del profile["reg_date"]
        del profile["updated_time"]
        del profile["last_login"]
        del profile['quick_intro']
        del profile['long_intro']
        profile["server_year"] = datetime.today().year
        tutorIntro = Profile.objects.get(user_id=user_id)
        print tutorIntro
        profile['quick_intro'] = tutorIntro.quick_intro
        profile['long_intro'] = tutorIntro.long_intro

        resume_info = Resume.objects.get(user_id=user_id)

        if (resume_info.fid):
            resume_info.fid = resume_info.fid.split(',')
            resume_info.fid = map(int, resume_info.fid);

            if (resume_info.pending != 1):
                while (index < 9):
                    if (resume_info.fid[index] != 0):
                        resumeFile["filename" + str(index)] = Files.objects.get(fid=resume_info.fid[index]).filename
                        index += 1
                    else:
                        resumeFile["filename" + str(index)] = "no file selected"
                        index += 1

                profile['resumefilename'] = resumeFile

        profile["education"] = resume_info.education
        profile["work_exp"] = resume_info.work_exp
        profile["certification"] = resume_info.certification
        profile["fid"] = resume_info.fid

        # 생일 정보
        birthDate = {}
        if profile["birth"] == "":
            birthDate["year"] = None
            birthDate["month"] = None
            birthDate["day"] = None
        else:
            birthDate["year"] = int(profile["birth"][0:4])
            birthDate["month"] = int(profile["birth"][4:6])
            birthDate["day"] = int(profile["birth"][6:8])
        del profile["birth"]
        profile["birthDate"] = birthDate

        # 성별 정보
        if profile["gender"] == None:
            profile["gender"] = 0

        # tools list
        tool_info = Tools.objects.filter(user_id=user_id)
        if tool_info:
            tool_info = Tools.objects.get(user_id=user_id)
            profile["offline"] = tool_info.offline
            profile["online"] = tool_info.online
            profile["audio"] = tool_info.audio
            profile["tools"] = [];
            if tool_info.skype == 1:
                profile["tools"].append({"name": "Skype", "account": tool_info.skype_id})
            if tool_info.hangout == 1:
                profile["tools"].append({"name": "HangOut", "account": tool_info.hangout_id})
            if tool_info.facetime == 1:
                profile["tools"].append({"name": "FaceTime", "account": tool_info.facetime_id})
            if tool_info.qq == 1:
                profile["tools"].append({"name": "QQ", "account": tool_info.qq_id})
        else:
            profile["offline"] = None
            profile["online"] = None
            profile["audio"] = None

        # currency
        if profile["currency"] == None:
            profile["currency"] = 0

        return profile
