# -*- encoding: utf-8 -*-

###############################################################################
# filename    : mypage > views.py
# description : mypage app views.py
# author      : msjang@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 msjang 최초 작성
#
#
###############################################################################


from datetime import datetime
from encodings.base64_codec import base64_decode
import json
import json
import logging
import os
import time
import urllib2

from django.forms.models import model_to_dict
from django.http import JsonResponse
from django.http.response import HttpResponse, HttpResponseRedirect
from django.http.response import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, render_to_response, redirect
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt

from common.common import tellpin_login_required
from exchange.views import detailUser
from mypage.service import MyPageService
# from common.models import Tellpin_Notecast, Tellpin_form_style, \
#     Tellpin_Notecast_form_style
# from tellpin.settings import BASE_DIR
# from tellpin.settings import INTEREST_IMG_URL
# from tutor.service import TutorService
# from tutor.service4registration import TutorService4Registration
# from common.models import TellpinUser, Interest, MyInterest, Language, \
#     MyLanguage, Timezone
from user_auth.utils import auth_email
import exchange
from mypage import messageService
from community.service import Community

# ts = TutorService(object)
mypage_service = MyPageService(object)
# ts4r = TutorService4Registration(object)
DATA_ROOT = '/static/img/upload'
logger = logging.getLogger('mylogger')


# Create your views here.
@csrf_exempt
def upload(request):
    """
    @summary: file upload page
    @author: msjang
    @param request
    @return: HttpResponse
    """
    # 하나의 파일 처리는 가능, 여러개의 파일을 전송했을 때 처리를 해야함
    print "fileupload"
    try:
        image = request.FILES[request.FILES.keys()[0]]

        image_path = upload_image(image)
        logger.debug("image_path" + image_path)

        return HttpResponse(image_path)
    except Exception as err:
        print("upload Exception : " + str(err))
        logger.debug(err)

        return HttpResponse('')


def upload_image(image):
    """
    @summary: image upload
    @author: msjang
    @param image
    @return: image_path( string )
    """
    image_name = "%d&&%s" % (int(round(time.time() * 1000)), image.name)
    image_path = '%s/%s' % (DATA_ROOT, image_name)
    full_path = '%s%s' % (BASE_DIR, image_path)
    try:
        with open(full_path, 'wb') as fp:
            for chunk in image.chunks():
                fp.write(chunk)
    except Exception as err:
        print full_path

    return image_path


@tellpin_login_required
def view_profile(request):
    """
    @summary: mynotecast page
    @author: msjang
    @param request
    @return: exchange.views.detailUser()
    """
    user_id = ts.get_user_id(request.session["email"])

    return redirect('detailUser', user_id=user_id)

