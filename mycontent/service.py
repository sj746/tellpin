# -*- encoding: utf-8 -*-

###############################################################################
# filename    : mycontent > service.py
# description : my content list service
# author      : hsrjmk@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20161004 hsrjmk 최초 작성
# 
#
###############################################################################
from community.models import (
    LB, LBPin, LBVote, LBComment,
    TP, TPAnswer, TPPin, TPVote, TPComment,
    LQ, LQAnswer, LQPin, LQVote, LQComment
)
from datetime import timedelta
from tutor.models import Tutee
from common.service import CommonService
from django.core.paginator import Paginator
from django.db.models.aggregates import Max, Count
from django.db.models.expressions import Value
from django.db.models.fields import CharField
from itertools import chain
from operator import attrgetter


class MyContentService(object):
    def __init__(self):
        pass
    
    PER_PAGE = 10
    
    ####################################################################
    #    Language Board 관련 QuerySet
    ####################################################################
    @staticmethod
    def get_lb_ids_related_pin(user_id):
        """
        @summary: language board 관련 pin한 게시 글 id
        @author: hsrjmk
        @param user_id: user
        @return result: LB ids
        """
        # Get LB ids (group by lb_id)
        result = LBPin.objects.filter(user_id=user_id).values('lb')\
                            .annotate(lastest_created_time=Max('created_time'))\
                            .values_list('lb', flat=True)
        return result
    
    @staticmethod
    def get_lb_ids_related_upvote(user_id):
        """
        @summary: language board 관련 upvote한 게시 글 id
        @author: hsrjmk
        @param user_id: user
        @return result: LB ids
        """
        # Get LB ids (group by lb_id)
        result = LBVote.objects.filter(user_id=user_id, like=True).values('lb')\
                            .annotate(lastest_created_time=Max('created_time'))\
                            .values_list('lb', flat=True)
        return result
    
    @staticmethod
    def get_lb_ids_related_comment(user_id):
        """
        @summary: language board 관련 comment한 게시 글 id
        @author: hsrjmk
        @param user_id: user
        @return result: LB ids
        """
        # Get LB ids (group by lb_id)
        result = LBComment.objects.filter(user_id=user_id).values('lb')\
                            .annotate(lastest_created_time=Max('created_time'))\
                            .values_list('lb', flat=True)
        return result
    
    @staticmethod
    def get_lb_objs_post(user_id):
        """
        @summary: language board 관련 작성한 게시 글
        @author: hsrjmk
        @param user_id: user
        @return result: LB objects
        """
        result = LB.objects.filter(user_id=user_id)\
                            .select_related('user__tutee')\
                            .annotate(lastest_created_time=Max('created_time'),
                                      comment_count=Count('lbcomment', distinct=True),
                                      type=Value('post', CharField()))\
                            .order_by('-lastest_created_time')
        return result
    
    @staticmethod
    def get_lb_objs_related(ids, from_model, lb_type):
        """
        @summary: language board 관련 게시 글
        @author: hsrjmk
        @param ids: LB ids
        @param from_model: model name where from created_time field
        @param lb_type: Type related LB
        @return result: LB objects
        """
        result = LB.objects.filter(id__in=ids)\
                            .select_related('user__tutee')\
                            .annotate(lastest_created_time=Max(''.join([from_model, '__created_time'])),
                                      comment_count=Count('lbcomment', distinct=True),
                                      type=Value(lb_type, CharField()))\
                            .order_by('-lastest_created_time')
        return result

    @staticmethod
    def make_lb_list(user_info, lb_objs):
        """
        @summary: language board 관련 게시 글 목록 만들기
        @author: hsrjmk
        @param user_info: User who created LB
        @param lb_objs: LB model object
        @return result: LB list
        """
        result = []
        for lb in lb_objs:
            li = {} # List item
            # LB data
            li['pid'] = lb.id
            li['title'] = lb.title
            li['content'] = lb.contents
            li['learning'] = lb.learning_lang_name
            li['explained'] = lb.explained_lang_name
            # Tutee data
            li['user_id'] = lb.user.user_id
            li['name'] = getattr(lb.user, 'name', None) or 'No name'
            li['profile_photo'] = ''.join([CommonService.PROFILE_IMAGE_FILE_DIR, '/', getattr(lb.user.tutee, 'photo_filename', None) or 'icon_person.png'])
            # LBVote data
            li['updated_time'] = lb.lastest_created_time + timedelta(hours=getattr(user_info.timezone_id, 'utc_delta', None) or 0)
            li['utc_zero'] = lb.lastest_created_time
            # Meta data (not in DB)
            li['comment_count'] = getattr(lb, 'comment_count', None) or 0
            li['TYPE'] = lb.type
 
            result.append(li)
        return result
    
    #########################################################################################
    #    Language Board Service API
    #########################################################################################
    @staticmethod
    def get_lb_all_count(user_id):
        """
        @summary: language board 관련 전체 게시 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = MyContentService.get_lb_post_count(user_id)
        result += MyContentService.get_lb_pin_count(user_id)
        result += MyContentService.get_lb_upvote_count(user_id)
        result += MyContentService.get_lb_comment_count(user_id)
        return result
    
    @staticmethod
    def get_lb_post_count(user_id):
        """
        @summary: language board 관련 게시 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = LB.objects.filter(user_id=user_id).count()
        return result

    @staticmethod
    def get_lb_pin_count(user_id):
        """
        @summary: language board 관련 pin한 게시 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = MyContentService.get_lb_ids_related_pin(user_id).count()
        return result

    @staticmethod
    def get_lb_upvote_count(user_id):
        """
        @summary: language board 관련 upvote한 게시 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = MyContentService.get_lb_ids_related_upvote(user_id).count()
        return result
        
    @staticmethod
    def get_lb_comment_count(user_id):
        """
        @summary: language board 관련  comment한 게시 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = MyContentService.get_lb_ids_related_comment(user_id).count()
        return result
    
    @staticmethod
    def get_lb_all(user_id, page=1):
        """
        @summary: Language board에 내가 쓴 글, pin한  글, upvote한 글, comment한 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return: obj: 요청 리스트
        """
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        lb_ids_related_pin = MyContentService.get_lb_ids_related_pin(user_id)
        lb_ids_related_upvote = MyContentService.get_lb_ids_related_upvote(user_id)
        lb_ids_related_comment = MyContentService.get_lb_ids_related_comment(user_id)
        
        lb_objs_post = MyContentService.get_lb_objs_post(user_id)
        lb_objs_related_pin = MyContentService.get_lb_objs_related(lb_ids_related_pin, 'lbpin', 'pin')
        lb_objs_related_vote = MyContentService.get_lb_objs_related(lb_ids_related_upvote, 'lbvote', 'vote')
        lb_objs_related_comment = MyContentService.get_lb_objs_related(lb_ids_related_comment, 'lbcomment', 'comment')
        lq_objs = sorted(chain(lb_objs_post, lb_objs_related_pin, lb_objs_related_vote, lb_objs_related_comment),
                            key=attrgetter('lastest_created_time'),
                            reverse=True)

        paginator = Paginator(lq_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_lb_list(user_info, current_page)
        return obj
    
    @staticmethod
    def get_lb_post(user_id, page=1):
        """
        @summary: Language board에 내가 쓴 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return: obj: 요청 리스트
        """
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        lb_objs = MyContentService.get_lb_objs_post(user_id)

        paginator = Paginator(lb_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_lb_list(user_info, current_page)
        return obj
    
    @staticmethod
    def get_lb_pin(user_id, page=1):
        """
        @summary: Language board에 내가 pin한  글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return: obj: 요청 리스트
        """
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        lb_ids = MyContentService.get_lb_ids_related_pin(user_id)
        lb_objs = MyContentService.get_lb_objs_related(lb_ids, 'lbpin', 'pin')

        paginator = Paginator(lb_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_lb_list(user_info, current_page)

        return obj
    
    @staticmethod
    def get_lb_upvote(user_id, page=1):
        """
        @summary: Language board에 내가 upvote한 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return: obj: 요청 리스트
        """
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        lb_ids = MyContentService.get_lb_ids_related_upvote(user_id)
        lb_objs = MyContentService.get_lb_objs_related(lb_ids, 'lbvote', 'vote')

        paginator = Paginator(lb_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_lb_list(user_info, current_page)
        return obj
    
    @staticmethod
    def get_lb_comment(user_id, page=1):
        """
        @summary: Language board에 내가 comment한 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return obj: 요청 리스트
        """
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        lb_ids = MyContentService.get_lb_ids_related_comment(user_id)
        lb_objs = MyContentService.get_lb_objs_related(lb_ids, 'lbcomment', 'comment')

        paginator = Paginator(lb_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_lb_list(user_info, current_page)
        return obj

    ####################################################################
    #    Translation & Proofreading 관련 QuerySet
    ####################################################################
    @staticmethod
    def get_tp_ids_related_answer(user_id):
        """
        @summary: Translation & Proofreading 관련 답변한 질문 글 id
        @author: hsrjmk
        @param user_id: user
        @return result: TP ids
        """
        # Get TP ids (group by tp_id)
        result = TPAnswer.objects.filter(user_id=user_id).values('tp')\
                            .annotate(lastest_created_time=Max('created_time'))\
                            .values_list('tp', flat=True)
        return result
    
    @staticmethod
    def get_tp_ids_related_pin(user_id):
        """
        @summary: Translation & Proofreading 관련  pin한 질문 글 id
        @author: hsrjmk
        @param user_id: user
        @return result: TP ids
        """
        # Get TP ids (group by tp_id)
        result = TPPin.objects.filter(user_id=user_id).values('tp')\
                            .annotate(lastest_created_time=Max('created_time'))\
                            .values_list('tp', flat=True)
        return result
    
    @staticmethod
    def get_tp_ids_related_upvote(user_id):
        """
        @summary: Translation & Proofreading 관련 upvote한 질문 글 id
        @author: hsrjmk
        @param user_id: user
        @return result: TP ids
        """
        # Get TP ids (group by tp_id)
        result = TPVote.objects.filter(user_id=user_id, like=True).values('tp')\
                            .annotate(lastest_created_time=Max('created_time'))\
                            .values_list('tp', flat=True)
        return result
    
    @staticmethod
    def get_tp_ids_related_comment(user_id):
        """
        @summary: Translation & Proofreading 관련  comment한 질문 글 id
        @author: hsrjmk
        @param user_id: user
        @return result: TP ids
        """
        # Get TP ids (group by tp_id)
        result = TPComment.objects.filter(user_id=user_id).values('tp')\
                            .annotate(lastest_created_time=Max('created_time'))\
                            .values_list('tp', flat=True)
        return result
    
    @staticmethod
    def get_tp_objs_question(user_id):
        """
        @summary: Translation & Proofreading 관련 작성한 질문 글
        @author: hsrjmk
        @param user_id: user
        @return result: TP objects
        """
        result = TP.objects.filter(user_id=user_id)\
                            .select_related('user__tutee')\
                            .annotate(lastest_created_time=Max('created_time'),
                                      answer_count=Count('tpanswer', distinct=True),
                                      comment_count=Count('tpcomment', distinct=True),
                                      kind=Value('question', CharField()))\
                            .order_by('-lastest_created_time')
        return result
    
    @staticmethod
    def get_tp_objs_related(ids, from_model, tp_kind):
        """
        @summary: Translation & Proofreading 관련 질문 글
        @author: hsrjmk
        @param ids: TP ids
        @param from_model: model name where from created_time field
        @param tp_kind: Kind related TP
        @return result: TP objects
        """
        result = TP.objects.filter(id__in=ids)\
                            .select_related('user__tutee')\
                            .annotate(lastest_created_time=Max(''.join([from_model, '__created_time'])),
                                      answer_count=Count('tpanswer', distinct=True),
                                      comment_count=Count('tpcomment', distinct=True),
                                      kind=Value(tp_kind, CharField()))\
                            .order_by('-lastest_created_time')
        return result

    @staticmethod
    def make_tp_list(user_info, tp_objs):
        """
        @summary: Translation & Proofreading 관련 질문 글 목록 만들기
        @author: hsrjmk
        @param user_info: User who created TP
        @param tp_objs: TP model object
        @return result: TP list
        """
        result = []
        for tp in tp_objs:
            li = {} # List item
            # TP data
            li['qid'] = tp.id
            li['title'] = tp.title
            li['content'] = tp.contents
            li['translated'] = tp.translated_lang_name
            li['written'] = tp.written_lang_name
            li['bestanswer'] = tp.best_answer_id
            li['point'] = tp.best_answer_credit
            li['type'] = tp.type
            # Tutee data
            li['user_id'] = tp.user.user_id
            li['name'] = getattr(tp.user, 'name', None) or 'No name'
            li['profile_photo'] = ''.join([CommonService.PROFILE_IMAGE_FILE_DIR, '/', getattr(tp.user.tutee, 'photo_filename', None) or 'icon_person.png'])
            # TPVote data
            li['updated_time'] = tp.lastest_created_time + timedelta(hours=user_info.timezone_id.utc_delta)
            li['utc_zero'] = tp.lastest_created_time
            # Meta data (not in DB)
            li['comment_count'] = getattr(tp, 'comment_count', None) or 0
            li['answer_count'] = getattr(tp, 'answer_count', None) or 0
            li['kind'] = tp.kind
 
            result.append(li)
        return result

    #########################################################################################
    #    Translation & Proofreading Service API
    #########################################################################################
    @staticmethod
    def get_tp_all_count(user_id):
        """
        @summary: Translation & Proofreading 관련  전체 질문 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = MyContentService.get_tp_question_count(user_id)
        result += MyContentService.get_tp_answer_count(user_id)
        result += MyContentService.get_tp_pin_count(user_id)
        result += MyContentService.get_tp_upvote_count(user_id)
        result += MyContentService.get_tp_comment_count(user_id)
        return result
        
    @staticmethod
    def get_tp_question_count(user_id):
        """
        @summary: Translation & Proofreading 관련  질문 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = TP.objects.filter(user_id=user_id).count()
        return result
    
    @staticmethod
    def get_tp_answer_count(user_id):
        """
        @summary: Translation & Proofreading 관련  답변한 질문 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = MyContentService.get_tp_ids_related_answer(user_id).count()
        return result

    @staticmethod
    def get_tp_pin_count(user_id):
        """
        @summary: Translation & Proofreading 관련  pin한 질문 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = MyContentService.get_tp_ids_related_pin(user_id).count()
        return result
        
    @staticmethod
    def get_tp_upvote_count(user_id):
        """
        @summary: Translation & Proofreading 관련  upvote한 질문 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = MyContentService.get_tp_ids_related_upvote(user_id).count()
        return result
        
    @staticmethod
    def get_tp_comment_count(user_id):
        """
        @summary: Translation & Proofreading 관련  comment한 질문 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = MyContentService.get_tp_ids_related_comment(user_id).count()
        return result
    
    @staticmethod
    def get_tp_all(user_id, page=1):
        """
        @summary: Translation & Proofreading에 내가 질문한 글, 답변한 글, pin한  글, upvote한 글, comment한 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return obj: 요청 리스트
        """
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        tp_answer_ids = MyContentService.get_tp_ids_related_answer(user_id)
        tp_pin_ids = MyContentService.get_tp_ids_related_pin(user_id)
        tp_vote_ids = MyContentService.get_tp_ids_related_upvote(user_id)
        tp_comment_ids = MyContentService.get_tp_ids_related_comment(user_id)
        tps_question = MyContentService.get_tp_objs_question(user_id)
        tps_related_answer = MyContentService.get_tp_objs_related(tp_answer_ids, 'tpanswer', 'answer')
        tps_related_pin = MyContentService.get_tp_objs_related(tp_pin_ids, 'tppin', 'pin')
        tps_related_vote = MyContentService.get_tp_objs_related(tp_vote_ids, 'tpvote', 'vote')
        tps_related_comment = MyContentService.get_tp_objs_related(tp_comment_ids, 'tpcomment', 'comment')
        tp_objs = sorted(chain(tps_question, tps_related_answer, tps_related_pin, tps_related_vote, tps_related_comment),
                            key=attrgetter('lastest_created_time'),
                            reverse=True)

        paginator = Paginator(tp_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_tp_list(user_info, current_page)
        return obj
    
    @staticmethod
    def get_tp_question(user_id, page=1):
        """
        @summary: Translation & Proofreading에 내가 질문한 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return obj: 요청 리스트
        """
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        tp_objs = MyContentService.get_tp_objs_question(user_id)
        
        paginator = Paginator(tp_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_tp_list(user_info, current_page)
        return obj
    
    @staticmethod
    def get_tp_answer(user_id, page=1):
        """
        @summary: Translation & Proofreading에 내가 답변한 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return obj: 요청 리스트
        """     
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        tp_ids = MyContentService.get_tp_ids_related_answer(user_id)
        tp_objs = MyContentService.get_tp_objs_related(tp_ids, 'tpanswer', 'answer')
        
        paginator = Paginator(tp_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_tp_list(user_info, current_page)
        return obj
    
    @staticmethod
    def get_tp_pin(user_id, page=1):
        """
        @summary: Translation & Proofreading에 내가 pin한 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return obj: 요청 리스트
        """
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        tp_ids = MyContentService.get_tp_ids_related_pin(user_id)
        tp_objs = MyContentService.get_tp_objs_related(tp_ids, 'tppin', 'pin')
        
        paginator = Paginator(tp_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_tp_list(user_info, current_page)
        return obj
    
    @staticmethod
    def get_tp_upvote(user_id, page=1):
        """
        @summary: Translation & Proofreading에 내가 upvote한 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return obj: 요청 리스트
        """
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        tp_ids = MyContentService.get_tp_ids_related_upvote(user_id)
        tp_objs = MyContentService.get_tp_objs_related(tp_ids, 'tpvote', 'vote')
        
        paginator = Paginator(tp_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_tp_list(user_info, current_page)
        return obj
    
    @staticmethod
    def get_tp_comment(user_id, page=1):
        """
        @summary: Translation & Proofreading에 내가 comment한 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return obj: 요청 리스트
        """
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        tp_ids = MyContentService.get_tp_ids_related_comment(user_id)
        tp_objs = MyContentService.get_tp_objs_related(tp_ids, 'tpcomment', 'comment')

        paginator = Paginator(tp_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_tp_list(user_info, current_page)
        return obj

    ####################################################################
    #    Language Questions 관련 QuerySet
    ####################################################################
    @staticmethod
    def get_lq_ids_related_answer(user_id):
        """
        @summary: Language Questions 관련  답변한 질문 글 id
        @author: hsrjmk
        @param user_id: user
        @return result: LQ ids
        """
        # Get LQ ids (group by lq_id)
        result = LQAnswer.objects.filter(user_id=user_id).values('lq')\
                            .annotate(lastest_created_time=Max('created_time'))\
                            .values_list('lq', flat=True)
        return result
    
    @staticmethod
    def get_lq_ids_related_pin(user_id):
        """
        @summary: Language Questions 관련  pin한 질문 글 id
        @author: hsrjmk
        @param user_id: user
        @return result: LQ ids
        """
        # Get LQ ids (group by lq_id)
        result = LQPin.objects.filter(user_id=user_id).values('lq')\
                            .annotate(lastest_created_time=Max('created_time'))\
                            .values_list('lq', flat=True)
        return result
    
    @staticmethod
    def get_lq_ids_related_upvote(user_id):
        """
        @summary: Language Questions 관련  upvote한 질문 글 id
        @author: hsrjmk
        @param user_id: user
        @return result: LQ ids
        """
        # Get LQ ids (group by lq_id)
        result = LQVote.objects.filter(user_id=user_id, like=True).values('lq')\
                            .annotate(lastest_created_time=Max('created_time'))\
                            .values_list('lq', flat=True)
        return result
    
    @staticmethod
    def get_lq_ids_related_comment(user_id):
        """
        @summary: Language Questions 관련  comment한 질문 글 id
        @author: hsrjmk
        @param user_id: user
        @return result: LQ ids
        """
        # Get LQ ids (group by lq_id)
        result = LQComment.objects.filter(user_id=user_id).values('lq')\
                            .annotate(lastest_created_time=Max('created_time'))\
                            .values_list('lq', flat=True)
        return result
    
    @staticmethod
    def get_lq_objs_question(user_id):
        """
        @summary: Language Questions 관련 작성한 질문 글
        @author: hsrjmk
        @param user_id: user
        @return result: LQ objects
        """
        result = LQ.objects.filter(user_id=user_id)\
                            .select_related('user__tutee')\
                            .annotate(lastest_created_time=Max('created_time'),
                                      answer_count=Count('lqanswer', distinct=True),
                                      comment_count=Count('lqcomment', distinct=True),
                                      kind=Value('question', CharField()))\
                            .order_by('-lastest_created_time')
        return result
    
    @staticmethod
    def get_lq_objs_related(ids, from_model, lq_kind):
        """
        @summary: Language Questions 관련 질문 글
        @author: hsrjmk
        @param ids: LQ ids
        @param from_model: model name where from created_time field
        @param lq_kind: Kind related LQ
        @return result: LQ objects
        """
        result = LQ.objects.filter(id__in=ids)\
                            .select_related('user__tutee')\
                            .annotate(lastest_created_time=Max(''.join([from_model, '__created_time'])),
                                      answer_count=Count('lqanswer', distinct=True),
                                      comment_count=Count('lqcomment', distinct=True),
                                      kind=Value(lq_kind, CharField()))\
                            .order_by('-lastest_created_time')
        return result
    
    @staticmethod
    def make_lq_list(user_info, lq_objs):
        """
        @summary: Language Questions 관련 질문 글 목록 만들기
        @author: hsrjmk
        @param user_info: User who created LQ
        @param lq_objs: LQ model object
        @return result: LQ list
        """
        result = []
        for lq in lq_objs:
            li = {} # List item
            # LQ data
            li['qid'] = lq.id
            li['title'] = lq.title
            li['content'] = lq.contents
            li['translated'] = lq.question_lang_name
            li['written'] = lq.written_lang_name
            li['bestanswer'] = lq.best_answer_id
            li['point'] = lq.best_answer_credit
            # Tutee data
            li['user_id'] = lq.user.user_id
            li['name'] = getattr(lq.user, 'name', None) or 'No name'
            li['profile_photo'] = ''.join([CommonService.PROFILE_IMAGE_FILE_DIR, '/', getattr(lq.user.tutee, 'photo_filename', None) or 'icon_person.png'])
            # LQVote data
            li['updated_time'] = lq.lastest_created_time + timedelta(hours=user_info.timezone_id.utc_delta)
            li['utc_zero'] = lq.lastest_created_time
            # Meta data (not in DB)
            li['comment_count'] = getattr(lq, 'comment_count', None) or 0
            li['answer_count'] = getattr(lq, 'answer_count', None) or 0
            li['kind'] = lq.kind
 
            result.append(li)
        return result
        
    #########################################################################################
    #    Language Questions Service API
    #########################################################################################
    @staticmethod
    def get_lq_all_count(user_id):
        """
        @summary: Language Questions 관련  전체 질문 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = MyContentService.get_lq_question_count(user_id)
        result += MyContentService.get_lq_answer_count(user_id)
        result += MyContentService.get_lq_pin_count(user_id)
        result += MyContentService.get_lq_upvote_count(user_id)
        result += MyContentService.get_lq_comment_count(user_id)
        return result
    
    @staticmethod
    def get_lq_question_count(user_id):
        """
        @summary: Language Questions 관련  질문 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = LQ.objects.filter(user_id=user_id).count()
        return result

    @staticmethod
    def get_lq_answer_count(user_id):
        """
        @summary: Language Questions 관련  답변한 질문 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = MyContentService.get_lq_ids_related_answer(user_id).count()
        return result

    @staticmethod
    def get_lq_pin_count(user_id):
        """
        @summary: Language Questions 관련  pin한 질문 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = MyContentService.get_lq_ids_related_pin(user_id).count()
        return result

    @staticmethod
    def get_lq_upvote_count(user_id):
        """
        @summary: Language Questions 관련  upvote한 질문 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = MyContentService.get_lq_ids_related_upvote(user_id).count()
        return result

    @staticmethod
    def get_lq_comment_count(user_id):
        """
        @summary: Language Questions 관련  comment한 질문 글 개수
        @author: hsrjmk
        @param user_id: user
        @return result: count
        """
        result = MyContentService.get_lq_ids_related_comment(user_id).count()
        return result

    @staticmethod
    def get_lq_all(user_id, page=1):
        """
        @summary: Language Questions에 내가 질문한 글, 답변한 글, pin한  글, upvote한 글, comment한 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return obj: 요청 리스트
        """
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        lq_answer_ids = MyContentService.get_lq_ids_related_answer(user_id)
        lq_pin_ids = MyContentService.get_lq_ids_related_pin(user_id)
        lq_vote_ids = MyContentService.get_lq_ids_related_upvote(user_id)
        lq_comment_ids = MyContentService.get_lq_ids_related_comment(user_id)
        lqs_question = MyContentService.get_lq_objs_question(user_id)
        lqs_related_answer = MyContentService.get_lq_objs_related(lq_answer_ids, 'lqanswer', 'answer')
        lqs_related_pin = MyContentService.get_lq_objs_related(lq_pin_ids, 'lqpin', 'pin')
        lqs_related_vote = MyContentService.get_lq_objs_related(lq_vote_ids, 'lqvote', 'vote')
        lqs_related_comment = MyContentService.get_lq_objs_related(lq_comment_ids, 'lqcomment', 'comment')
        lq_objs = sorted(chain(lqs_question, lqs_related_answer, lqs_related_pin, lqs_related_vote, lqs_related_comment),
                            key=attrgetter('lastest_created_time'),
                            reverse=True)

        paginator = Paginator(lq_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_lq_list(user_info, current_page)
        return obj
    
    @staticmethod
    def get_lq_question(user_id, page=1):
        """
        @summary: Language Questions에 내가 질문한 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return obj: 요청 리스트
        """   
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        lq_objs = MyContentService.get_lq_objs_question(user_id)
        
        paginator = Paginator(lq_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_lq_list(user_info, current_page)
        return obj
    
    @staticmethod
    def get_lq_answer(user_id, page=1):
        """
        @summary: Language Questions에 내가 답변한 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return obj: 요청 리스트
        """  
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        lq_ids = MyContentService.get_lq_ids_related_answer(user_id)
        lq_objs = MyContentService.get_lq_objs_related(lq_ids, 'lqanswer', 'answer')
        
        paginator = Paginator(lq_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_lq_list(user_info, current_page)
        return obj
    
    @staticmethod
    def get_lq_pin(user_id, page=1):
        """
        @summary: Language Questions에 내가 pin한 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return obj: 요청 리스트
        """
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        lq_ids = MyContentService.get_lq_ids_related_pin(user_id)
        lq_objs = MyContentService.get_lq_objs_related(lq_ids, 'lqpin', 'pin')
        
        paginator = Paginator(lq_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_lq_list(user_info, current_page)
        return obj
    
    @staticmethod
    def get_lq_upvote(user_id, page=1):
        """
        @summary: Language Questions에 내가 upvote한 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return obj: 요청 리스트
        """   
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        lq_ids = MyContentService.get_lq_ids_related_upvote(user_id)
        lq_objs = MyContentService.get_lq_objs_related(lq_ids, 'lqvote', 'vote')
        
        paginator = Paginator(lq_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_lq_list(user_info, current_page)
        return obj

    @staticmethod
    def get_lq_comment(user_id, page=1):
        """
        @summary: Language Questions에 내가 comment한 글 목록 조회
        @author: hsrjmk
        @param user_id: user id
        @param page: 요청 page 번호
        @return obj: 요청 리스트
        """
        user_info = Tutee.objects.select_related('timezone_id').get(user_id=user_id)
        lq_ids = MyContentService.get_lq_ids_related_comment(user_id)
        lq_objs = MyContentService.get_lq_objs_related(lq_ids, 'lqcomment', 'comment')
        
        paginator = Paginator(lq_objs, MyContentService.PER_PAGE)
        current_page = paginator.page(page)
        
        obj = {}
        obj['page'] = page
        obj['has_next'] = current_page.has_next()
        obj['list'] = MyContentService.make_lq_list(user_info, current_page)
        return obj
