from django.conf.urls import url
import views

urlpatterns = [
    url( r'^$', views.getMyContentList ),
    url( r'^list/$', views.ajax_getMyContentList ),
]