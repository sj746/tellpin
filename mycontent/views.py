# -*- encoding: utf-8 -*-

###############################################################################
# filename    : mycontent > views.py
# description : mycontent app views.py
# author      : msjang@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 msjang 최초 작성
# 
#
###############################################################################

import json

from django.core.serializers.json import DjangoJSONEncoder
from django.http.response import JsonResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt

from common.common import tellpin_login_required
from .service import MyContentService


# Create your views here.
@tellpin_login_required
def getMyContentList(request):
    """
    @summary: mycontent page
    @author: msjang
    @param request
    @return: url - mycontent/mycontent.html
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    
    resData['all'] = MyContentService.get_lb_all(user_id, 1) 
    lcObj = {}
    lcObj['add'] = MyContentService.get_lb_post_count(user_id)
    lcObj['comment'] = MyContentService.get_lb_comment_count(user_id)
    lcObj['vote'] = MyContentService.get_lb_upvote_count(user_id)
    lcObj['pin'] = MyContentService.get_lb_pin_count(user_id)
    resData['lb'] = lcObj
    tpObj = {}
    tpObj['question'] = MyContentService.get_tp_question_count(user_id)
    tpObj['answer'] = MyContentService.get_tp_answer_count(user_id)
    tpObj['comment'] = MyContentService.get_tp_comment_count(user_id)
    tpObj['vote'] = MyContentService.get_tp_upvote_count(user_id)
    tpObj['pin'] = MyContentService.get_tp_pin_count(user_id)
    resData['tp'] = tpObj
    lqObj = {}
    lqObj['question'] = MyContentService.get_lq_question_count(user_id)
    lqObj['answer'] = MyContentService.get_lq_answer_count(user_id)
    lqObj['comment'] = MyContentService.get_lq_comment_count(user_id)
    lqObj['vote'] = MyContentService.get_lq_upvote_count(user_id)
    lqObj['pin'] = MyContentService.get_lq_pin_count(user_id)
    resData['lq'] = lqObj
    print resData

    return render_to_response('mycontent/mycontent.html', {'data': json.dumps(resData, cls=DjangoJSONEncoder)}, RequestContext(request))


@csrf_exempt
@tellpin_login_required
def ajax_getMyContentList(request):
    """
    @summary: mycontent page
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    
    post = json.loads(request.body)
    type = post['type']
    page = post['page']

    if type == 'lbAll':
        resData['item'] = MyContentService.get_lb_all(user_id, page)
    elif type == 'lbAdd':
        resData['item'] = MyContentService.get_lb_post(user_id, page)
    elif type == 'lbComment':
        resData['item'] = MyContentService.get_lb_comment(user_id, page)
    elif type == 'lbVote':
        resData['item'] = MyContentService.get_lb_upvote(user_id, page)
    elif type == 'lbPin':
        resData['item'] = MyContentService.get_lb_pin(user_id, page)
    elif type == 'tpAll':
        resData['item'] = MyContentService.get_tp_all(user_id, page)
    elif type == 'tpQuestion':
        resData['item'] = MyContentService.get_tp_question(user_id, page)
    elif type == 'tpAnswer':
        resData['item'] = MyContentService.get_tp_answer(user_id, page)
    elif type == 'tpComment':
        resData['item'] = MyContentService.get_tp_comment(user_id, page)
    elif type == 'tpVote':
        resData['item'] = MyContentService.get_tp_upvote(user_id, page)
    elif type == 'tpPin':
        resData['item'] = MyContentService.get_tp_pin(user_id, page)
    elif type == 'lqAll':
        resData['item'] = MyContentService.get_lq_all(user_id, page)
    elif type == 'lqQuestion':
        resData['item'] = MyContentService.get_lq_question(user_id, page)
    elif type == 'lqAnswer':
        resData['item'] = MyContentService.get_lq_answer(user_id, page)
    elif type == 'lqComment':
        resData['item'] = MyContentService.get_lq_comment(user_id, page)
    elif type == 'lqVote':
        resData['item'] = MyContentService.get_lq_upvote(user_id, page)
    elif type == 'lqPin':
        resData['item'] = MyContentService.get_lq_pin(user_id, page)
    else:
        resData['item'] = []

    return JsonResponse(resData)
