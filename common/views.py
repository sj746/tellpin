import json
import calendar
from django.http.response import JsonResponse
from django.shortcuts import render

from django.views.decorators.csrf import csrf_exempt
from .common import tellpin_login_required

from .service import CommonService
from tutor.service import TutorService
from people.service import PeopleService


@csrf_exempt
def get_city(request):
    result = {}
    result["isSuccess"] = 1

    param = json.loads(request.body)
    country_id = param['country_id']
    result['city'] = CommonService.city_list(country_id=country_id)

    return JsonResponse(result)

@csrf_exempt
def get_max_date(request):
    """
    @summary: get max date
    @author: msjang
    @param request
    @return: json
    """
    post_dict = json.loads(request.body)
    month_dates = calendar.monthrange(post_dict["year"], post_dict["month"])[1]

    return JsonResponse({"lastDate": month_dates})

@csrf_exempt
@tellpin_login_required
def getUserInfo(request):
    """
    @summary: exchange get user information function
    @author: khyun
    @param request
    @return: json
    """
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    post = json.loads(request.body)
    pop_id = post['id']

    user_detail = TutorService.get_user_detail(pop_id, 0)

    if user_id == id:
        resData['isSuccess'] = 3
    else:
        resData['userPop'] = user_detail['profile']
        resData['is_friend'] = PeopleService.isRequestFriend(user_id, pop_id)

    return JsonResponse(resData)
