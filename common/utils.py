# -*- coding: utf-8 -*-
###############################################################################
# filename    : common > utils.py
# description : 공통 기능 함수 정의
# author      : hsrjmk@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160921 hsrjmk 최조 작성
#
###############################################################################
from django.utils import timezone
from datetime import datetime, timedelta


def get_expiration_datetime(days=30):
    now = timezone.make_aware(datetime.now()+timedelta(days=days), timezone.get_default_timezone())
    return now.astimezone(timezone.utc)
