#-*- coding: utf-8 -*-

###############################################################################
# filename    : common > common.py
# description : 공통으로 쓰이는 함수들
# author      : msjang@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 msjang 최초 작성
#
#
###############################################################################

from datetime import datetime, timedelta, time
import random
import string
from time import strftime
from tellpin.settings import TELLPIN_HOME
from django.contrib.sessions.models import Session
from django.core.mail.message import EmailMessage
from django.db import connection
from django.http.response import HttpResponseRedirect
from django.template.context import RequestContext
from user_auth.models import TellpinUser
from models import LanguageInfo, CountryInfo, CityInfo
# from models import TuteeAccount, TutorAccount
# from tellpin.settings import TELLPIN_HOME
# from models import NationInfo, CityInfo
# from models import TellpinUser, Language


USER_IMG = '/static/img/upload/profile/'

def send_noti_email(to_user, _title, _context, from_user=None, _footer=None):
    """
    @summary: 알림 email 보내는 함수
    @author: khyun
    @param to_user: email 받는 사람
    @param _title: email 제목
    @param _context: email 내용
    @param from_user: email 보내는 사람
    @return: none
    """
    print 'send_noti_email'
    #email setting
    url_location = "user_auth/register_confirm/"
    subject = _title
    img_message = None

    if from_user:

        print 'test3'
        print from_user
        #user = TellpinUser.objects.get(user_id=from_user.user_id)
        user = from_user
        name = user.name
        flag = None
        _image = None
        from_in = None
        if user.from_country_id:
            flag = CountryInfo.objects.get(id=user.from_country_id_id).flag_filename
            #from_in = CountryInfo.objects.get(id=user.from_country_id).name_en +', ' + CityInfo.objects.get(city_id=user.from_city).name
            from_in = CountryInfo.objects.get(id=user.from_country_id_id).country
        if user.photo_filename:
            _image = user.photo_filename

        speaks = ""
        learnings = ""
        teaches = ""

        print 'test2'
        if user.native1:
            speaks = user.native1 #LanguageInfo.objects.get(id=user.native1).language1
        if user.native2:
            speaks += ", " + user.native2 #LanguageInfo.objects.get( id = user.native2 ).language1
        if user.native3:
            speaks += ", " + user.native3 #LanguageInfo.objects.get( id = user.native3 ).language1
        if user.lang1_id:
            speaks += ", " + user.lang1 #LanguageInfo.objects.get( id = user.other1 ).language1
        if user.lang2_id:
            speaks += ", " + user.lang2 #LanguageInfo.objects.get( id = user.other2 ).language1
        if user.lang3_id:
            speaks += ", " + user.lang3 #LanguageInfo.objects.get( id = user.other3 ).language1
            
        if user.lang1_learning:
            learnings = user.lang1 #LanguageInfo.objects.get( id = user.learning1 ).language1
        if user.lang2_learning:
            learnings += ", " + user.lang2 #LanguageInfo.objects.get( id = user.learning2 ).language1
        if user.lang3_learning:
            learnings += ", " + user.lang3 #LanguageInfo.objects.get( id = user.learning3 ).language1
        
#         if user.teaching1:
#             teaches = LanguageInfo.objects.get( id = user.teaching1 ).language1
#         if user.teaching2:
#             teaches += ", " + LanguageInfo.objects.get( id = user.teaching2 ).language1
#         if user.teaching3:
#             teaches += ", " + LanguageInfo.objects.get( id = user.teaching3 ).language1
        
        print 'test1'
        img_message = ''\
        '<table>'\
        '    <tr>'
        print 'user.profile_photo:'
        if user.photo_filename:
            img_message += '        <td rowspan=\"5\" style=\"width:120px;\"><img style=\"width:100px;border-radius:30%%;border: 1px solid #DBEFF1 !important; \" src=\"%sstatic/img/upload/profile/%s\" onerror=\"javascript:this.src=\'%sstatic/img/user_auth/icon_person.png\'\"></td>' % ( TELLPIN_HOME, _image, TELLPIN_HOME)
        else:    
            img_message += '        <td rowspan=\"5\" style=\"width:120px;\"><img style=\"width:100px;border-radius:30%%;border: 1px solid #DBEFF1 !important;\" src=\"%sstatic/img/user_auth/icon_person.png\" onerror=\"javascript:this.src=\'%sstatic/img/user_auth/icon_person.png\'\"></td>' % ( TELLPIN_HOME, TELLPIN_HOME)
        
        if flag:
            img_message += '        <td><a style=\"font-weight:bold;\" href=\"%s\"> %s </a> <img width="20" height="15" style=\"margin-left:10px; width: 20px; height: 15px;\" src=\"%sstatic/img/nations/%s\">' % (TELLPIN_HOME+'user/'+str(from_user.user_id)+'/', name, TELLPIN_HOME, flag)
        else:
            img_message += '        <td><a style=\"font-weight:bold;\" href=\"%s\"> %s </a>' % (TELLPIN_HOME+'user/'+str(from_user.user_id)+'/' ,name)
        
        img_message += '        </td>'\
        '    </tr>'\
        '    <tr>'\
        '        <td><span>Speaks</span><span style=\"margin-left: 10px;\">%s</span>'\
        '        </td>'\
        '    </tr>'\
        '    <tr>'\
        '        <td><span>Learning</span><span style=\"margin-left: 10px;\">%s</span>'\
        '        </td>'\
        '    </tr>'\
        '    <tr>' % ( speaks, learnings )
        print 'test6'
#         if user.teaching1:
#             img_message += '        <td><span>Teaches</span><span style=\"margin-left: 10px;\">%s</span></td> ' % ( teaches )
        print 'test7'
        img_message += '    </tr>'\
        '    <tr>'
        if user.from_country:
            img_message += '        <td><span style=\"float: left;\"><img src=\"%sstatic/img/tutor/icon_live_in.png\" ></span><span style=\"float: left;line-height:28px\">%s</span> ' % ( TELLPIN_HOME, from_in )
        img_message += '    </tr></table>' \
        ' <br>%s ' % ( _context )
        print 'test8'
        img_message += '<p style=\"margin-top:50px; font-size:12px !important;\"> '\
                       '<span style=\"font-size:12px !important;\">'\
                       'You received this email because you are a registered member on Tellpin. '\
                       'If you would like to change your email notification preferences, '\
                       'please click <a href="%s">here</a> . '\
                       'Replies to this email address will not be read. '\
                       'Do not hesitate to <a href=\"%s\">contact us</a> '\
                       'if you have any further questions.</span></p>' % ( TELLPIN_HOME + 'settings/notification/', TELLPIN_HOME + 'help/' )
            
        print 'test4'
        print img_message
        _context = img_message
        print 'test5'
    
    if _footer:
        _context += '<p style=\"margin-top:50px; font-size:12px !important;\"> '\
                       '<span style=\"font-size:12px !important;\">'\
                       'You received this email because you are a registered member on Tellpin. '\
                       'If you would like to change your email notification preferences, '\
                       'please click <a href="%s">here</a> . '\
                       'Replies to this email address will not be read. '\
                       'Do not hesitate to <a href=\"%s\">contact us</a> '\
                       'if you have any further questions.</span></p>' % ( TELLPIN_HOME + 'settings/notification/', TELLPIN_HOME + 'help/' )
    
    print '?'
    print str(_context)
    message = '' \
    ' <table width="100%%" style="height: 450px;" >'\
    '    <tr style=\"background-color: #ECECEC;height: 450px;width:100%%\"> ' \
    '        <td align="center"> ' \
    '            <table align="center;" style=\"width: 770px;height:450px;\"> ' \
    '            <tr style=\"background-color: white; height: 450px;\"> ' \
    '                <td style=\"padding:0 50px;font-size:16px !important;\" > ' \
    '                    <p style=\"border-bottom: 1px solid #ECECEC; padding-top: 20px; padding-bottom: 20px;\"><a href=\"%s\"><img width=\"100\"  src=\"%sstatic/img/mainpage/tellpin_header_logo_title_beta.png\"></a></p>'\
    '                    <p style=\"margin-top:20px;\">%s</p> ' \
    '                    <p></p> ' \
    '                </td> ' \
    '            </tr> ' \
    '            </table> '\
    '        </td>'\
    '    </tr>'\
    '</table>' % ( TELLPIN_HOME, TELLPIN_HOME, _context )
    #'</table>' % ( TELLPIN_HOME, str(user.name), TELLPIN_HOME+url_location+activation_key, TELLPIN_HOME, TELLPIN_HOME+url_location+activation_key, TELLPIN_HOME, TELLPIN_HOME, TELLPIN_HOME, TELLPIN_HOME, TELLPIN_HOME )
    
    from_email = 'info'
    print '---------test----------'
    print subject
    print message
    print from_email
    print '---------test----------'
    
    if subject and message and from_email:
        try:
            #send_mail(subject, '', 'tellpinadmin@unichal.com', [user.email], html_message=message)#userprofile db update
            msg = EmailMessage(subject, message, 'tellpinadmin@unichal.com', [to_user])
            print 'to_user :'
            print to_user
            msg.content_subtype = 'html'
            msg.send()
            print 'message send end--'
        except Exception as e:
            e.message = 'send_email error'
            #return HttpResponse('Invalid header found.')
        #return HttpResponseRedirect('/user_auth/register.html/', )
    else:
        # In reality we'd use a form class
        # to get proper validation errors.
        print 'send_email error'
        #return HttpResponse('Make sure all fields are entered and valid.')


def getTuteeTotalAvailable( user_id ):
    """
    @summary: 현재 사용가능한 tutee 코인
    @author: msjang
    @param user_id: 해당 유저 id
    @return: result( int )
    """
    result = 0
    try :
        tutee = TuteeAccount.objects.filter( user_id = user_id ).latest('created_date')
        result = tutee.total_available
    except Exception as e :
        result = 0
    finally:
        return result
    
    
def getTuteeTotalBalance( user_id ):
    """
    @summary: total tutee 코인
    @author: msjang
    @param user_id: 해당 유저 id
    @return: result( int )
    """
    result = 0
    try :
        tutee = TuteeAccount.objects.filter( user_id = user_id ).latest('created_date')
        result = tutee.total_balance
    except Exception as e :
        result = 0
    finally:
        return result
    
def getTuteeTotalPending( user_id ):
    """
    @summary: total pending 된 tutee 코인
    @author: msjang
    @param user_id: 해당 유저 id
    @return: result( int )
    """
    result = 0
    try :
        tutee = TuteeAccount.objects.filter( user_id = user_id ).latest('created_date')
        result = tutee.total_pending
    except Exception as e :
        result = 0
    finally:
        return result
    
    
def getTutorTotalAvailable( user_id ):
    """
    @summary: 현재 사용가능한 tutor 코인
    @author: msjang
    @param user_id: 해당 유저 id
    @return: result( int )
    """
    result = 0
    try :
        tutor = TutorAccount.objects.filter( user_id = user_id ).latest('created_date')
        result = tutor.total_available
    except Exception as e :
        result = 0
    finally:
        return result
    
    
def getTutorTotalBalance( user_id ):
    """
    @summary: 토탈 tutor 코인
    @author: msjang
    @param user_id: 해당 유저 id
    @return: result( int )
    """
    result = 0
    try :
        tutor = TutorAccount.objects.filter( user_id = user_id ).latest('created_date')
        result = tutor.total_balance
    except Exception as e :
        result = 0
    finally:
        return result
    

    
def getTutorTotalPending( user_id ):
    """
    @summary: total pending 된 tutor 코인
    @author: msjang
    @param user_id: 해당 유저 id
    @return: result( int )
    """
    result = 0
    try :
        tutor = TutorAccount.objects.filter( user_id = user_id ).latest('created_date')
        result = tutor.total_pending
    except Exception as e :
        result = 0
    finally:
        return result

def getTutorTotalWithPending( user_id ):
    """
    @summary: total withdraw pending 된 tutor 코인
    @author: msjang
    @param user_id: 해당 유저 id
    @return: result( int )
    """
    result = 0
    try :
        tutor = TutorAccount.objects.filter( user_id = user_id ).latest('created_date')
        result = tutor.total_withdraw_pending
    except Exception as e :
        result = 0
    finally:
        return result

def isTutor(user_id):
    """
    @summary: tutor 여부
    @author: msjang
    @param user_id: 해당 유저 id
    @return: 0( tutee ), 1( tutor )
    """
    return TellpinUser.objects.get( id = user_id ).is_tutor

# 16자리 시리얼 랜덤 생성
def serialGenerator( size = 16, chars = string.ascii_uppercase + string.digits ) :
    """
    @summary: gift code 생성 함수
    @author: msjang
    @param size: 생성할 문자 길이
    @param chars: 문자 list
    @return: char
    """
    return ''.join( random.choice( chars ) for _ in range( size ) )

# cursor to dict
def dictfetchall( cursor ):
    """
    @summary: cursor -> dict 변환 함수
    @author: msjang
    @param cursor: sql 실행 결과 cursor
    @return: dictionary
    """
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
    
def insertTuteeAccount( user_id, tc, type, more_type, more_id ):
    """
    @summary: community 쪽 질문 답변 코인 처리 함수
    @author: msjang
    @param user_id: 해당 유저 id
    @param tc: Tellpin coin
    @param type: 기능 분류( 11 : 질문시 코인 걸기 12 : 베스트답변채택(질문자) 13 : 베스트답변채택(답변자) 14: 베스트답변채택 없음 )
    @param more_type: 상세 분류( 6 : LQ, 7 : TP, 8 : LC )
    @param more_id: 해당 게시글 id
    @return: result( int )
    """
    result = 1
    try : 
        available = getTuteeTotalAvailable( user_id )
        pending = getTuteeTotalPending( user_id )
        balance = getTuteeTotalBalance( user_id )
        # 11 : 질문시 코인 걸기 12 : 베스트답변채택(질문자) 13 : 베스트답변채택(답변자) 14: 베스트답변채택 없음
        if type == 11 :
            TuteeAccount(
                user_id = user_id,
                a_change = -( tc ),
                p_change = tc,
                t_change = 0,
                total_available = available - tc,
                total_pending = pending + tc,
                total_balance = balance,
                type = type,
                more_type = more_type,
                more_id = more_id,
                payment_method = 0
            ).save()
        elif type == 12 :
            TuteeAccount(
                user_id = user_id,
                a_change = 0,
                p_change = -( tc ),
                t_change = -( tc ),
                total_available = available,
                total_pending = pending - tc,
                total_balance = balance - tc,
                type = type,
                more_type = more_type,
                more_id = more_id,
                payment_method = 0
            ).save()
        elif type == 13 :
            TuteeAccount(
                user_id = user_id,
                a_change = tc,
                p_change = 0,
                t_change = tc,
                total_available = available + tc,
                total_pending = pending,
                total_balance = balance + tc,
                type = type,
                more_type = more_type,
                more_id = more_id,
                payment_method = 0
            ).save()
        else :
            TuteeAccount(
                user_id = user_id,
                a_change = tc,
                p_change = 0,
                t_change = tc,
                total_available = available + tc,
                total_pending = pending,
                total_balance = balance + tc,
                type = type,
                more_type = more_type,
                more_id = more_id,
                payment_method = 0
            ).save()
    except Exception as e :
        print str( e )
        result = 0
    finally :
        return result


def applyUserUTCTime(server_list, utc_time):
    """
    @summary: 사용자가 설정한 utc 적용
    @author: 
    @param server_list: 서버저장 시간
    @param utc_time: utc 정보
    @return: client_list( list )
    """
    client_list = []
    utc_index = utcToIndex(utc_time) 
    for dayIndex, list_item in enumerate(server_list) :#for index(dayIndex : 0~6), value in enumerate(list)
        schedule_obj = {}
        for idx in range(48):
            index = idx - utc_index #utc_index = 10
            if index < 0:
                if dayIndex - 1 < 0 :
                    schedule_obj[index_to_time(idx)] = server_list[6][index_to_time(48 + index)]
                else :
                    schedule_obj[index_to_time(idx)] = server_list[dayIndex - 1][index_to_time(48 + index)]
            elif index > 47 :
                if dayIndex + 1 > 6 :
                    schedule_obj[index_to_time(idx)] = server_list[0][index_to_time(index - 48)]
                else :
                    schedule_obj[index_to_time(idx)] = server_list[dayIndex + 1][index_to_time(index - 48)]
            else :
                schedule_obj[index_to_time(idx)] = server_list[dayIndex][index_to_time(index)]
        schedule_obj["day_of_week"] = server_list[dayIndex]["day_of_week"]
        client_list.append(schedule_obj)
    return client_list

def user_utc_rv_time(rv_time, user_utc):
    """
    @summary: 사용자 utc 적용 예약 함수
    @author: 
    @param rv_time: 예약 시간
    @param user_utc: 사용자 utc 시간
    @return: datetime
    """
    utc_index = utcToIndex(user_utc)
    time = datetime.strptime(rv_time[0:8]+rv_time[9:13], "%Y%m%d%H%M")
    if utc_index % 2 == 0 :
        utc_timedelta = timedelta(hours = (utc_index/2))
    else :
        if utc_index < 0 :
            utc_timedelta = timedelta(hours = (utc_index/2), minutes=-30)
        else : 
            utc_timedelta = timedelta(hours = (utc_index/2), minutes=30)
    utc_rv_time = time + utc_timedelta
    time_string = strftime("%Y%m%d%H%M", utc_rv_time.timetuple())
    return time_string[0:8] + dayindex_to_alphabet(utc_rv_time.weekday()) + time_string[8:12]

def server_rv_time(rv_time, user_utc):
    """
    @summary: 서버 강의 예약 시간
    @author: 
    @param rv_time: 예약 시간
    @param user_utc: 사용자 utc 시간
    @return: datetime
    """
    utc_index = utcToIndex(user_utc)
    time = datetime.strptime(rv_time[0:8]+rv_time[9:13], "%Y%m%d%H%M")
    if utc_index % 2 == 0 :
        utc_timedelta = timedelta(hours = -(utc_index/2))
    else :
        if utc_index < 0 :
            utc_timedelta = timedelta(hours = -(utc_index/2), minutes=30)
        else : 
            utc_timedelta = timedelta(hours = -(utc_index/2), minutes=-30)
    utc_rv_time = time + utc_timedelta
    time_string = strftime("%Y%m%d%H%M", utc_rv_time.timetuple())
    return time_string[0:8] + dayindex_to_alphabet(utc_rv_time.weekday()) + time_string[8:12]

def apply_time_list(day_of_week, from_time, to_time, utc_time, user_id):
    """
    @summary: utc 적용 list
    @author: 
    @param day_of_week: 요일정보
    @param from_time: 시작날짜
    @param to_time: 종료날짜
    @param utc_time: utc 시간
    @param user_id: 해당 유저 id 번호
    @return: time_list( list )
    """
    time_list = []
    utc_index = utcToIndex(utc_time) 
    for time_index in range(from_time, to_time) :
        time_obj = {}
        index = time_index - utc_index
        if index < 0 :
            if day_of_week - 1 < 1 :
                time_obj["day_of_week"] = 7
            else :
                time_obj["day_of_week"] = day_of_week - 1
            time_obj["time"] = index_to_time(48 + index)
        elif index > 47 :
            if day_of_week + 1 > 7 :
                time_obj["day_of_week"] = 1
            else :
                time_obj["day_of_week"] = day_of_week + 1
            time_obj["time"] = index_to_time(index + 47)
        
        else :
            time_obj["day_of_week"] = day_of_week
            time_obj["time"] = index_to_time(index)
        time_list.append(time_obj)
    return time_list
    
def set_utc_time_list(date, time_list, utc_time):
    """
    @summary: time list utc 적용
    @author: 
    @param date: 날짜정보
    @param time_list: 시간 정보
    @param utc_time: utc 시간
    @return: utc_time_list( list )
    """
    utc_time_list = []
    utc_index = utcToIndex(utc_time)
    if utc_index % 2 == 0 :
        utc_timedelta = timedelta(hours = -(utc_index/2))
    else :
        if utc_index < 0 :
            utc_timedelta = timedelta(hours = -(utc_index/2), minutes=30)
        else : 
            utc_timedelta = timedelta(hours = -(utc_index/2), minutes=-30)
    for column in time_list :
        utc_time = datetime.strptime(date[0:8]+column[4:8], "%Y%m%d%H%M") + utc_timedelta
        time_string = strftime("%Y%m%d%H%M", utc_time.timetuple())
        utc_time_list.append(time_string[0:8] + dayindex_to_alphabet(utc_time.weekday()) + time_string[8:12])
        
    return utc_time_list

def apply_utc_month_schedule(delta_time, utc_time):
    """
    @summary: utc 적용 월별 스케줄
    @author: 
    @param delta_time: 시간정보
    @param utc_time: utc 시간
    @return: string
    """
    utc_index = utcToIndex(utc_time)
    if utc_index % 2 == 0 :
        utc_timedelta = timedelta(hours = (utc_index/2))
    else :
        if utc_index < 0 :
            utc_timedelta = timedelta(hours = (utc_index/2), minutes=-30)
        else : 
            utc_timedelta = timedelta(hours = (utc_index/2), minutes=30)
    utc_time = datetime.strptime(delta_time[0:8]+delta_time[9:13], "%Y%m%d%H%M") + utc_timedelta #deltatime =20160623D0100
    time_string = strftime("%Y%m%d%H%M", utc_time.timetuple())
    return time_string[0:8] + dayindex_to_alphabet(utc_time.weekday()) + time_string[8:12]
    
def get_utc_date_range(from_date, to_date, utc_time):
    """
    @summary: utc 적용 날짜범위
    @author: 
    @param from_date: 시작날짜
    @param to_date: 종료날짜
    @param utc_time: utc 시간
    @return: range_list( list )
    """
    try:
        utc_index = utcToIndex(utc_time)
        if utc_index % 2 == 0 :
            utc_timedelta = timedelta(hours = -(utc_index/2))
        else :
            if utc_index < 0 :
                utc_timedelta = timedelta(hours = -(utc_index/2), minutes=30)
            else : 
                utc_timedelta = timedelta(hours = -(utc_index/2), minutes=-30)
        range_list = []
        start_date = datetime.strptime(from_date, "%Y%m%d")
        end_date = datetime.strptime(to_date, "%Y%m%d")
        for single_day in range(int((end_date - start_date).days)+1) :
            for idx in range(48) :
                single_date = start_date + timedelta(single_day)
                single_date_string = strftime("%Y%m%d", single_date.timetuple()) + index_to_time(idx)[4:8]
                utc_time = datetime.strptime(single_date_string, "%Y%m%d%H%M") + utc_timedelta
                time_string = strftime("%Y%m%d%H%M", utc_time.timetuple())
                range_list.append(time_string[0:8] + dayindex_to_alphabet(utc_time.weekday()) + time_string[8:12])
        return range_list
    except Exception as e :
        print str( e )
        
def time_to_user_timezone(date, user_utc):
    """
    @summary: 사용자 시간 정보 변경
    @author: 
    @param date: 시간정보
    @param user_utc: user timezone
    @return: string
    """
    utc_index = utcToIndex(user_utc)
    if utc_index % 2 == 0 :
        utc_timedelta = timedelta(hours = (utc_index/2))
    else :
        if utc_index < 0 :
            utc_timedelta = timedelta(hours = (utc_index/2), minutes=-30)
        else : 
            utc_timedelta = timedelta(hours = (utc_index/2), minutes=30)
    utc_date = date + utc_timedelta
    return str(utc_date)[0:19]
    
  
def utcToIndex(utc_time):
    """
    @summary: utc -> index
    @author: 
    @param utc_time: utc 시간
    @return: int
    """
    utc_index = int(utc_time[1:3]) * 2 #utc_time : +05:00
    
    if int(utc_time[4:6]) == 30 :
        utc_index += 1
    
    if utc_time[0] == "-" :
        utc_index = -utc_index
    return utc_index

def index_to_time(index):
    """
    @summary: index -> time
    @author: 
    @param index: index정보
    @return: string
    """
    time_string = "time"
    if (index % 2) == 0:
        if index/2 < 10 :
            time_string += "0"+str(index/2)+"00"
        else :
            time_string += str(index/2)+"00"
    else:
        if(index-1) / 2 < 10 :
            time_string += "0"+str((index-1)/2)+"30"
        else :
            time_string += str((index-1)/2)+"30"
    return time_string # time1900


def get_login_users():
    """
    @summary: 현재 접속중인 유저 list
    @author: 
    @param none
    @return: login_users( list )
    """
    login_users = []
    _time = datetime.utcnow()
    
    for item in Session.objects.filter(expire_date__gt=_time):
        uid = item.get_decoded().get('email')
        if uid:
            login_users.append(uid)
    return list(set(login_users))
        
def time_to_index(time):
    """
    @summary: time -> index
    @author: 
    @param time: 시간정보
    @return: string
    """
    if time[6:8] == "00" :
        return int(time[0:2])*2
    else :
        return int(time[0:2]) * 2 +1

def dayindex_to_alphabet(index):
    """
    @summary: 요일정보 -> alphabet
    @author: 
    @param index: 요일정보
    @return: string
    """
    alphabet_list = ['A', 'B', 'C', 'D', 'E', 'F', 'G']
    return alphabet_list[index]

def getUserInfo( id ): 
    """
    @summary: id에 해당하는 유저 정보
    @author: msjang
    @param id: user id 번호
    @return: obj( object )
    """
    obj = {}
    try:
        cursor = connection.cursor()
        sql = '''
            SELECT id, name, learning1
                 , ( SELECT language1 FROM user_auth_language WHERE id = learning1 ) AS lang1
                 , learning1_skill_level
                 , learning2
                 , ( SELECT language1 FROM user_auth_language WHERE id = learning2 ) AS lang2     
                 , learning2_skill_level
                 , learning3
                 , ( SELECT language1 FROM user_auth_language WHERE id = learning3 ) AS lang3
                 , learning3_skill_level
                 , other1, other1_skill_level
                 , ( SELECT language1 FROM user_auth_language WHERE id = other1 ) AS oth1
                 , other2, other2_skill_level
                 , ( SELECT language1 FROM user_auth_language WHERE id = other2 ) AS oth2
                 , other3, other3_skill_level
                 , ( SELECT language1 FROM user_auth_language WHERE id = other3 ) AS oth3
                 , ( SELECT language1 FROM user_auth_language WHERE id = native1 ) AS na1
                 , ( SELECT language1 FROM user_auth_language WHERE id = native2 ) AS na2
                 , ( SELECT language1 FROM user_auth_language WHERE id = native3 ) AS na3
                 , teaching1, teaching2, teaching3
                 , ( SELECT language1 FROM user_auth_language WHERE id = teaching1 ) AS tea1
                 , ( SELECT language1 FROM user_auth_language WHERE id = teaching2 ) AS tea2
                 , ( SELECT language1 FROM user_auth_language WHERE id = teaching3 ) AS tea3
                 , CONCAT( %s, profile_photo) AS profile
                 , ( SELECT name_en FROM nation_info WHERE nid = u.from  ) AS nation
                 , ( SELECT CONCAT( '/static/img/nations/', image ) FROM nation_info WHERE nid = u.from ) AS nation_img
                 , ( SELECT NAME FROM city_info WHERE city_id = from_city ) AS city
                 , ( SELECT CONCAT(AVG(NULLIF( rating, 0 ))) FROM ratings WHERE lid IN ( 
                                                                   SELECT lid 
                                                                   FROM lesson 
                                                                   WHERE user_id = id
                                                                  ) ) AS rate
                 , 1 AS TYPE
            FROM user_auth_tellpinuser u
            WHERE id = %s
        '''
        cursor.execute( sql, [ USER_IMG, id ] )
        obj = dictfetchall( cursor )[0]
    except Exception as e:
        print str( e )
        obj['err'] = str( e )
    finally:
        cursor.close()
        return obj



#login check decorater 
'''
def tellpin_login_required(functor):
    def decorated(args):
        print "decorater"
        if not args.session.get('email', False):
            print "no session key 'email'"
            return HttpResponseRedirect("/user_auth/login_form/")
        return functor(args)
    return decorated    
'''


"""
@summary: login check decorator
@author: mcpark
@return: decorator
"""
def tellpin_login_required(view_func, login_url='/user_auth/login_form/'):
    def decorator(request, *args, **kwargs):
        request.session['previous_url'] = request.get_full_path()
        #request.session['previous_post'] = request.POST.dict()

        # 로그인된 상태이므로 원래 view 함수를 실행한다
        if request.user:
            print 'tellpin_login_required if'
            return view_func(request, *args, **kwargs)
        # 로그인을 하지 않았으므로, 로그인페이지로 redirect
        else:
            print 'tellpin_login_required else' 
            return HttpResponseRedirect(login_url, RequestContext(request))
    return decorator

