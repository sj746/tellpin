#-*- coding: utf-8 -*-

###############################################################################
# filename    : common > service.py
# description : 공통으로 쓰이는 함수들
# author      : msjang@unichal.com
#
# history
# -----------------------------------------------------------------------------
# 20160907 msjang 최초 작성
#
#
###############################################################################
import os
import shutil
import time
import ctypes
import urllib2
import json
from ctypes import windll
import locale
from django.utils import translation

from datetime import datetime

from os.path import isfile

from django.contrib.sessions.models import Session
from django.http.response import JsonResponse
from django.utils import timezone

from dashboard.models import Message
from tellpin.settings import BASE_DIR
from user_auth.models import File
from .models import LanguageInfo, CountryInfo, SkillLevelInfo, TimezoneInfo, CityInfo, CurrencyInfo


class CommonService(object):
    PROFILE_IMAGE_FILE_DIR = "/static/img/upload/profile"
    PROBLEM_IMAGE_FILE_DIR = "/static/img/upload/problem"
    RESUME_FILE_DIR = "/static/img/upload/resume"
    RESUME_TEMP_FILE_DIR = "/static/img/upload/resume/temp"
    TEMP_FILE_DIR = "/static/img/upload/temp"
    NATION_FILE_DIR = "/static/img/nations"

    DISPLAY_LANGUAGE = {
        0: 'en-us',
        1: 'ko',
        2: 'ja'
    }

    def __init__(self):
        pass

    @staticmethod
    def make_result(success=1, msg=''):
        result = {
            "isSuccess": success,
            "message": msg
        }

        return result

    @staticmethod
    def language_list():
        queryset = LanguageInfo.objects.all().values('id', 'lang_english', 'lang_local')

        return list(queryset)

    @staticmethod
    def country_list():
        queryset = CountryInfo.objects.all().values('id', 'country', 'flag_filename')

        return list(queryset)

    @staticmethod
    def city_list(country_id):
        queryset = CityInfo.objects.filter(country_id=country_id).values('id', 'city')

        return list(queryset)

    @staticmethod
    def tool_list():
        tools = [
            {
                "id": 0,
                "name": "Select tools"
            },
            {
                "id": 1,
                "name": "Skype"
            },
            {
                "id": 2,
                "name": "Hangout"
            },
            {
                "id": 3,
                "name": "Facetime"
            },
            {
                "id": 4,
                "name": "QQ"
            }
        ]

        return tools

    @staticmethod
    def skill_level_list():
        queryset = SkillLevelInfo.objects.all().values('id', 'level')

        return list(queryset)

    @staticmethod
    def skill_level_list_exclude_native():
        queryset = SkillLevelInfo.objects.all().exclude(id=7).values('id', 'level')

        return list(queryset)

    @staticmethod
    def timezone_list():
        queryset = TimezoneInfo.objects.all().values('id', 'utc_location', 'utc_time', 'utc_delta')

        return list(queryset)

    @staticmethod
    def currency_list():
        queryset = CurrencyInfo.objects.all().values('id', 'code', 'symbol')

        return list(queryset)

    @staticmethod
    def upload_image(image, user_email, upload_type=None):
        """
        @summary: 이미지 업로드
        @author: msjang
        @param image: image 파일
        @param user_email: user email address
        @param upload_type: 이미지 업로드 종류
        @return: image_path, image_name( string )
        """
        if upload_type == 'profile':
            dir_type = CommonService.TEMP_FILE_DIR
        elif upload_type == 'resume':
            dir_type = CommonService.RESUME_FILE_DIR
        elif upload_type == 'resume_temp':
            dir_type = CommonService.RESUME_TEMP_FILE_DIR
        elif upload_type == 'problem':
            dir_type = CommonService.PROBLEM_IMAGE_FILE_DIR
        else:
            dir_type = CommonService.TEMP_FILE_DIR

        image_name = "%d&&%s" % (int(round(time.time() * 1000)), image.name)
        repo_path, image_path = CommonService.make_user_directory(dir_type, user_email)
        full_path = '%s/%s' % (repo_path, image_name)
        with open(full_path, 'wb') as fp:
            for chunk in image.chunks():
                fp.write(chunk)
        print full_path

        return image_path, image_name

    @staticmethod
    def remove_image(image_path, image_name):
        result = True
        full_path = '%s%s/%s' % (BASE_DIR, image_path, image_name)
        try:
            if isfile(full_path):
                os.remove(full_path)
        except Exception as err:
            print str(err)
            result = False
        finally:
            return result

    @staticmethod
    def copy_image(src, dst_type, email=None):
        result = True
        dst = ""
        try:
            if dst_type == 'profile':
                dst = CommonService.PROFILE_IMAGE_FILE_DIR
            elif dst_type == 'problem':
                dst = CommonService.PROBLEM_IMAGE_FILE_DIR
            elif dst_type == 'resume':
                dst = CommonService.RESUME_FILE_DIR
            else:
                if email:
                    dst = '%s/%s' % (CommonService.RESUME_TEMP_FILE_DIR, email)

            src_file = '%s%s/%s' % (BASE_DIR, CommonService.TEMP_FILE_DIR, src)
            if not os.path.exists('%s/%s' % (dst, src)):
                shutil.copy(src_file, '%s%s' % (BASE_DIR, dst))
        except Exception as err:
            print str(err)
            result = False
        finally:
            return result

    @staticmethod
    def make_user_directory(dir_type, email=""):
        """
        @summary: 유저 폴더 생성
        @author: msjang
        @param dir_type: 디렉토리 타입
        @param email: 유저 email
        @return: directory, image_file_path( string )
        """
        directory = '%s%s' % (BASE_DIR, dir_type)

        if not os.path.exists(directory):
            os.makedirs(directory)

        if dir_type == "/static/img/upload/resume/temp":
            user_dir = '%s/%s' % (directory, email)
            if not os.path.exists(user_dir):
                os.makedirs(user_dir)

                return user_dir, '%s/%s' % (dir_type, email)

        return directory, dir_type

    @staticmethod
    def file_insert(file_path, file_name, file_type, fid=None):
        result = 0
        try:
            if not fid:
                file_obj = File(
                    type=file_type,
                    filename=file_name,
                    filepath=file_path,
                    ref_id=0
                )
                file_obj.save()
                result = file_obj.pk
            else:
                file_obj = File.objects.get(fid_id=fid)
                remove_filepath = file_obj.filepath
                remove_filename = file_obj.filename
                CommonService.remove_image(remove_filepath, remove_filename)
                file_obj.filepath = file_path
                file_obj.filename = file_name
                file_obj.save()
                result = file_obj.pk
        except Exception as err:
            result = 0
            print str(err)
        finally:
            return result

    @staticmethod
    def file_copy_update(file_name, file_type, email=None):
        result = True
        try:
            if File.objects.filter(filename=file_name).exists():
                file_obj = File.objects.get(filename=file_name)
                if file_type == 'profile':
                    file_obj.filepath = CommonService.PROFILE_IMAGE_FILE_DIR
                elif file_type == 'problem':
                    file_obj.filepath = CommonService.PROBLEM_IMAGE_FILE_DIR
                elif file_type == 'resume':
                    file_obj.filepath = CommonService.RESUME_FILE_DIR
                else:
                    file_obj.filepath = '%s/%s' % (CommonService.RESUME_TEMP_FILE_DIR, email)
                file_obj.save()
        except Exception as err:
            print str(err)
            result = False
        finally:
            return result

    @staticmethod
    def get_login_users():
        """
        @summary: 현재 접속중인 유저 list
        @author:
        @param none
        @return: login_users( list )
        """
        login_users = []
        _time = timezone.now()

        for item in Session.objects.filter(expire_date__gt=_time):
            uid = item.get_decoded().get('email')
            if uid:
                login_users.append(uid)
        return list(set(login_users))

    @staticmethod
    def paging_list(page_range, cur_page, per_page):
        """
        @summary: 페이징 처리 함수
        @param page_range: 전체 페이지 범위 ex. [1, 2, 3, 4, 5, 6, ...]
        @param cur_page: 현재 페이지 ex. 1페이지일 경우 [1 ~ 10], 22페이지일 경우[20 ~ 30]
        @param per_page: 한번에 보여줄 페이지 단위
        @return: list
        """
        page_list = []
        try:
            max_page = max(page_range)
            if cur_page > max_page:
                page_list = []
            else:
                if not (cur_page-1) / per_page:
                    start = 0
                else:
                    start = (per_page * ((cur_page-1)/per_page))
                end = start + per_page
                page_list = page_range[start:end]
        except Exception as err:
            print '@@@@@paging_list : ', str(err)
            page_list = []
        finally:
            return page_list

    @staticmethod
    def check_post_data(post_data, key, no_value=True):
        """
        @summary: check post parameter
        @param post_data: post data list
        @param key: post data key
        @param no_value: existed value
        @return: boolean, string
        """
        result = {}

        if key in post_data:
            value = post_data[key]
            if no_value:
                if not value or value == 'undefined':
                    result['isSuccess'] = False
                    result['message'] = 'no value, check the parameter by post'

                    return False, JsonResponse(result)
        else:
            result['isSuccess'] = False
            result['message'] = 'no key("' + key + '"), check the parameter by post'

            return False, JsonResponse(result)

        return True, value

    @staticmethod
    def change_local_language(request, display_language):
        print "== change_local_language =="
        cur_language = translation.get_language_from_request(request)
        print "requestlanguage = " + cur_language
#         print type(display_language)
        user_lang = CommonService.DISPLAY_LANGUAGE[display_language]
        print "display_language = " + user_lang
        if cur_language != user_lang:
            translation.deactivate_all()
            translation.activate(user_lang)
            request.LANGUAGE_CODE = translation.get_language()
            request.session[translation.LANGUAGE_SESSION_KEY] = request.LANGUAGE_CODE
            print "request.LANGUAGE_CODE = " + request.LANGUAGE_CODE

    @staticmethod
    def get_local_language():
        print "get_local_languageget_local_language"
        windll = ctypes.windll.kernel32
        lang1 = windll.GetUserDefaultUILanguage()
        lang2 = locale.windows_locale[windll.GetUserDefaultUILanguage()]
        print lang1
        print lang2







    @staticmethod
    def bitbucket_get_tellpin():
        print "bitbucket_get_tellpin"
#         currency_base = 'USD'
#         currency_user = 'USD'
#         currency = urllib2.urlopen('http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s='+currency_base+currency_user+'=X').read()
#         print currency

        data = urllib2.urlopen('https://api.bitbucket.org/2.0/repositories/sj746/tellpin/').read()
        print data

    @staticmethod
    def bitbucket_get_tellpin_branches():
        print "bitbucket_get_tellpin_branches"
#         currency_base = 'USD'
#         currency_user = 'USD'
#         currency = urllib2.urlopen('http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s='+currency_base+currency_user+'=X').read()
#         print currency

        data = urllib2.urlopen('https://api.bitbucket.org/2.0/repositories/sj746/tellpin/refs/branches').read()
        jsonData = json.dumps(data)
        print jsonData
















class MessageService(object):
    def __init__(self):
        pass

    @staticmethod
    def send_message(sender_id, receiver_id, msg):
        """
        @summary: 메세지 보내는 함수
        @param sender_id: 보내는 이
        @param receiver_id: 받는 이
        @param msg: 메세지 내용
        @return: dict()
        """
        result = {
            'isSuccess': True,
            'message': 'Success'
        }
        try:
            message = Message(
                sender_id=sender_id,
                receiver_id=receiver_id,
                message=msg
            )
            message.save()
        except Exception as err:
            print '@@@@@send_message : ', str(err)
            result['isSuccess'] = False
            result['message'] = str(err)
        finally:
            return result
