#-*- coding: utf-8 -*-
'''
Created on 2016. 4. 18.

@author: khyun
'''
###############################################################################
# filename    : common > Scheduler.py
# description : Scheduler ( backend )
# author      : khyun@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 khyun 최초 작성
#
#
###############################################################################

from datetime import datetime, timedelta
import time

from apscheduler.schedulers.background import BackgroundScheduler
from tutor.models import Tutee, Tutor
from common import send_noti_email
from settings.models import SettingsNotification
from dashboard.models import Notification
from tellpin.settings import TELLPIN_HOME
from tutoring.models import LessonReservation, Lesson, LessonClaim
from user_auth.models import TellpinUser
from tutor.service import TutorService

# websocket
from django.http import HttpResponse
from django.views.generic.base import TemplateView
from django.views.decorators.csrf import csrf_exempt
from ws4redis.redis_store import RedisMessage
from ws4redis.publisher import RedisPublisher
from chatserver.views import UserChatView
from xmlrpclib import _datetime

class Notification_Scheduler(object):
    """
    @summary: Scheduler class
    @author: yjkim
    @param none
    @return: url - none
    """
    
    LESSON_TITLE = [
                    "[Reminder] Upcoming lesson with \'%s\' at %s",
                    ]
    LESSON_CONTEXT = [
                      "<p> Don't forget that there is an upcoming lesson with \'%s\' in %s </p>",
                      "<p> Don't forget to be ready for the upcoming lesson with \'%s\' in %s </p>",
                      "Your lesson request has been declined."
                      ]
    LESSON_CONTEXT2 = [
                       "Tutor : <a href=\'%s\'>\'%s\'</a> (User ID : %s) <br>"\
                        "Tutee : <a href=\'%s\'>\'%s\'</a> (User ID : %s) <br>"\
                        "Course name : \'%s\' <br>"\
                        "Lesson time : %s <br>"\
                        "Duration : \'%s\' mins <br>"\
                        "Reservation ID : %s <br>",
                        "<p>The tutor declined your lesson request. <a href=\'%s\'>View details</a></p>"
                       ]
    def __init__(self):
        """
        @summary: Scheduler init function
        @author: yjkim
        @param none
        @return: url - none
        """
        print '__init__()'
        self.sched = BackgroundScheduler()
        #rv = Reservation.objects.get(id=145)
        #self.send_mail(rv, rv.tutor_id, rv.tutee_id , '1')
        self.sched.start()
        self.sched.add_job(self.set_noti, 'interval', seconds=1)
        self.sched.add_job(self.check_noti, 'interval', seconds=3)
        
    def __call__(self):
        print '__init__()'
        self.sched = BackgroundScheduler()
        #rv = Reservation.objects.get(id=145)
        #self.send_mail(rv, rv.tutor_id, rv.tutee_id , '1')
        self.sched.start()
        self.sched.add_job(self.set_noti, 'interval', seconds=1)
        self.sched.add_job(self.check_noti, 'interval', seconds=3)
    
    def __del(self):
        """
        @summary: Scheduler destroy function
        @author: khyun
        @param none
        @return: url - none
        """
        self.shutdown()
        
    def shutdown(self):
        """
        @summary: Scheduler shutdown function
        @author: yjkim
        @param none
        @return: url - none
        """
        self.sched.shutdown()
        
    def check_noti(self):
        current_time = datetime.utcnow()
        before_time = current_time-timedelta(seconds=3)
        
        noti_obj = Notification.objects.filter(created_time__gte=before_time,
                                created_time__lt=current_time)
        user_list = []
        for noti in noti_obj:
            user_list.append(TellpinUser.objects.get(user_id=noti.receiver_id).email)
        
        UserChatView.post_noti(user_list, 'notification')
        
        
    def set_noti(self):
        """
        @summary: Scheduler ( notification, send-mail ) function
        @author: yjkim
        @param none
        @return: url - none
        """
        print 'set_noti()'
        #all reservation check
        #rv_status = 1 예약상태.
        rvs = LessonReservation.objects.filter(lesson_status=1, rv_time__isnull = False)
        rvps = LessonReservation.objects.filter(lesson_status=6)
        
        for rv in rvs:
            print rv.id
            #print r.tutor_id, r.tutee_id
            _datetime = datetime( int(rv.rv_time[:4]), int(rv.rv_time[4:6]), int(rv.rv_time[6:8]), int(rv.rv_time[9:11]), int(rv.rv_time[11:13]) )
            print str(_datetime)
            try:
                #print datetime(_datetime - datetime.utcnow())
                #print _datetime.strftime("%H:%M")
                #print datetime.utcnow().strftime("%H:%M")
                #print _datetime - datetime.utcnow().replace(microsecond=0)
                redirect_url = "/tutorlessons/status/"+str(rv.id)
                if _datetime - datetime.utcnow().replace(microsecond=0) == timedelta(hours=1):
                    
                    print 'before 1 hours'
                    if SettingsNotification.objects.get(user_id=rv.tutor_id).e_l_1hours == 1:
                        #TODO
                        #send_mail()
                        print 'send_tutor_1hour_mail()'
                        self.send_mail(rv, rv.tutor_id, rv.tutee_id , '1')
                    if SettingsNotification.objects.get(user_id=rv.tutee_id).e_l_1hours == 1:
                        #TODO
                        #send_mail()
                        print 'send_tutee_1hour_mail()'
                        self.send_mail(rv, rv.tutee_id, rv.tutor_id , '1')
                    Notification(sender_id=rv.tutor_id, receiver_id=rv.tutee_id,redirect_url = redirect_url, type=1,  noti_text=1).save()
                    Notification(sender_id=rv.tutee_id, receiver_id=rv.tutor_id,redirect_url = redirect_url, type=1,  noti_text=1).save()
                    #self.sched.shutdown()
                if _datetime - datetime.utcnow().replace(microsecond=0) == timedelta(hours=3):
                    print 'before 3 hours'
                    if SettingsNotification.objects.get(user_id=rv.tutor_id).e_l_3hours == 1:
                        #TODO
                        #send_mail()
                        print 'send_3hour_mail()'
                        self.send_mail(rv, rv.tutor_id, rv.tutee_id , '3')
                    if SettingsNotification.objects.get(user_id=rv.tutee_id).e_l_3hours == 1:
                        #TODO
                        #send_mail()
                        print 'send_3hour_mail()'
                        self.send_mail(rv, rv.tutee_id, rv.tutor_id , '3')
                    Notification(sender_id=rv.tutor_id, receiver_id=rv.tutee_id,redirect_url = redirect_url, type=1,  noti_text=1).save()
                    Notification(sender_id=rv.tutee_id, receiver_id=rv.tutor_id,redirect_url = redirect_url, type=1, noti_text=1).save()
                    #self.sched.shutdown()
                if _datetime - datetime.utcnow().replace(microsecond=0) == timedelta(hours=24):
                    print 'before 24 hours'
                    if SettingsNotification.objects.get(user_id=rv.tutor_id).e_24_1hours == 1:
                        #TODO
                        #send_mail()
                        print 'send_24hour_mail()'
                        self.send_mail(rv, rv.tutor_id, rv.tutee_id , '24')
                    if SettingsNotification.objects.get(user_id=rv.tutee_id).e_24_1hours == 1:
                        #TODO
                        #send_mail()
                        print 'send_24hour_mail()'
                        self.send_mail(rv, rv.tutee_id, rv.tutor_id , '24')
                    Notification(sender_id=rv.tutor_id, receiver_id=rv.tutee_id,redirect_url = redirect_url, type=1, noti_text=1).save()
                    Notification(sender_id=rv.tutee_id, receiver_id=rv.tutor_id,redirect_url = redirect_url, type=1,  noti_text=1).save()
                    #self.sched.shutdown()
                    #settings check
                print rv.created_time
                #자동 decline
                if rv.created_time:    
                    print "auto decline"
                    cr_date = datetime.strftime(rv.created_time, '%Y-%m-%d %H:%M:%S')
                    cr_date = datetime.strptime(cr_date, '%Y-%m-%d %H:%M:%S')
                    #강의 시작 시간이 현재시간을 지남 or request한지 36시간이 지남.
                    if _datetime < datetime.utcnow().replace(microsecond=0) or datetime.utcnow() - cr_date > timedelta(hours=36):
                        #print u'예약시간이 현재 시간을 지남  or 강의예약일로 부터 36시간 지남, 자동 decline 처리'
                        #print _datetime > datetime.utcnow().replace(microsecond=0)
                        #print datetime.utcnow() - cr_date
                        #자동 decline 처리
                        
                        #print rv.id, rv.rv_time
                        
                        #decline 처리.
                        rv.lesson_status = 3
                        rv.save()
                        
                        print rv.id
                        #decline 메일 전송
                        self.send_mail(rv, rv.tutor_id, rv.tutee_id, '36')
                        #decline 알림 전송
                        Notification(sender_id=rv.tutor_id, receiver_id=rv.tutee_id, ref_id = rv.id, redirect_url = redirect_url, type=1,  noti_text=1).save()
                        
                        
                    
                    
            except Exception as e:
                print 'exception'
                print e
                self.sched.shutdown()
                #email 전송. . . . . 
        
        #problem 자동 처리
        for rvp in rvps:
            print 'auto problem solve'
            #reply 없는 경우
            try:
                problem = LessonClaim.objects.filter(lesson_reservation_id=rvp.id)
                if problem and problem.count() < 2:
                    #Tutee (튜티)가 신고
                    pr_date = datetime.strftime(problem[0].reg_date, '%Y-%m-%d %H:%M:%S')
                    pr_date = datetime.strptime(pr_date, '%Y-%m-%d %H:%M:%S')
                    if problem[0].problem_resolve_type == "Return lesson coins to the tutee" and problem[0].is_accepted == 0 and datetime.utcnow() - pr_date > timedelta(hours=36):
                        #튜티에게 전액 환불
                        print 'return tutee'
                        ts = TutorService(object)
                        rvp.rv_status = 7
                        rvp.save()
                        ts.pay_refund(problem[0].lesson_reservation_id)
                    #Tutor (튜티)가 신고
                    if problem[0].problem_resolve_type == "Transfer the lesson coins to the tutor" and problem[0].is_accepted == 0 and datetime.utcnow() - pr_date > timedelta(hours=36):
                        #튜터에게 전액 환불
                        print 'return tutor'
                        ts = TutorService(object)
                        rvp.rv_status = 5
                        rvp.save()
                        ts.pay_confirm(problem[0].lesson_reservation_id)
            except Exception as e:
                print e
                
    def send_mail(self, rv, to_user, from_user, hours):
        """
        @summary: Scheduler send_mail function
        @author: khyun
        @param rv : reservation object
        @param to_user : to user param
        @param from_user : from user param
        @param hours : time param
        @return: url - none
        """
        print 'send_mail()'
        to_user_email = TellpinUser.objects.get(user_id=to_user).email
        to_user_name = TellpinUser.objects.get(user_id=to_user).name
        from_user_name = TellpinUser.objects.get(user_id=from_user).name
        tutor_name = TellpinUser.objects.get(user_id=rv.tutor_id).name
        tutee_name = TellpinUser.objects.get(user_id=rv.tutee_id).name
        lesson = Lesson.objects.select_related('course').get(id=rv.lesson_id)
        _title = self.LESSON_TITLE[0] % ( from_user_name, rv.rv_time )
        if hours == '1':
            _context = self.LESSON_CONTEXT[1] % ( from_user_name, "1 hour.")
        if hours == '3':
            _context = self.LESSON_CONTEXT[1] % ( from_user_name, "3 hours.")
        if hours == '24':
            _context = self.LESSON_CONTEXT[1] % ( from_user_name, "24 hours.")
        if hours == '36':
            _context = self.LESSON_CONTEXT[2]
        if lesson.pkg_times == 1:
            duration = lesson.sg_min
        else:
            duration = lesson.pkg_min
        _context2 = self.LESSON_CONTEXT2[0] % ( TELLPIN_HOME+'tutor/'+str(rv.tutee_id), tutor_name, rv.tutor_id, TELLPIN_HOME+'user/'+str(rv.tutee_id), tutee_name, rv.tutee_id, lesson.course.course_name, rv.rv_time, duration, rv.id)
        if hours == '36':
            _context2 = self.LESSON_CONTEXT2[1] % ( TELLPIN_HOME+'tuteelessons/status/'+str(rv.id)+'/' )
        _context += _context2
        send_noti_email(to_user_email, _title, _context)
    
        
if __name__ == '__main__':
    """
    @summary: Scheduler main
    @author: khyun
    @param none
    @return: url - none
    """
    print '__main__test'
    sch = Notification_Scheduler()
    
    #while True: time.sleep(1)        
    for i in xrange(30) : time.sleep(1)
    
    #sch.shutdown()
    print 'Scheduler is over'    
    
def run():
    """
    @summary: Scheduler main
    @author: khyun
    @param none
    @return: url - none
    """
    print '__main__test'
    sch = Notification_Scheduler()
    
    while True: time.sleep(1)        
    #for i in xrange(30) : time.sleep(1)
    
    #sch.shutdown()
    print 'Scheduler is over'