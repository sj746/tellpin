# -*- coding: utf-8 -*-
###############################################################################
# filename    : common > models.py
# description : Info 모델 정의
# author      : hsrjmk@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160831 hsrjmk 최조 작성
#
###############################################################################
from django.db import models

class LanguageInfo(models.Model):
    lang_english = models.CharField("Language name in English", max_length=100)
    lang_local = models.CharField("Language name in local language", max_length=100)

    class Meta:
        db_table = u'language_info'


class CountryInfo(models.Model):
    country = models.CharField("Country name in English", max_length=50)
    flag_filename = models.CharField("Filename of flag image", max_length=100)

    class Meta:
        db_table = u'country_info'


class CityInfo(models.Model):
    city = models.CharField("City name", max_length=50)
    country = models.ForeignKey('CountryInfo')

    class Meta:
        db_table = u'city_info'


class CurrencyInfo(models.Model):
    code = models.CharField("Currency code", max_length=3)
    symbol = models.CharField("Currency symbol", max_length=10)

    class Meta:
        db_table = u'currency_info'


class TimezoneInfo(models.Model):
    utc_location = models.CharField("location name", max_length=100)
    utc_time = models.CharField("Time offset", max_length=6)
    utc_delta = models.FloatField("Difference time from UTC")

    class Meta:
        db_table = u'timezone_info'


class SkillLevelInfo(models.Model):
    level = models.CharField("Skill level name", max_length=50)

    class Meta:
        db_table = u'skill_level_info'


class BuyCreditsFeeInfo(models.Model):
    tc = models.IntegerField("Tellpin credit to buy")
    usd = models.IntegerField("Prices in USD")
    creditcard_fee = models.FloatField("Credit card fees")
    paypal_fee = models.FloatField("Paypal fees")

    class Meta:
        db_table = u'buy_credits_fee_info'
