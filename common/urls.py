from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^city/', views.get_city, name='city'),
    url(r'^get_max_date/$', views.get_max_date),
    url(r'^user/popover/$', views.getUserInfo),
]
