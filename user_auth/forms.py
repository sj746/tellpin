# -*- encoding: utf-8 -*-
'''
Created on 2015. 7. 30.

@author: khyun
'''

from django import forms


class UserRegisterForm(forms.Form):
    email = forms.EmailField(label='', widget=forms.TextInput(attrs={'placeholder': 'Enter E-mail address',
                                                                     'style': 'ime-mode:disabled;background: white url(/static/img/user_auth/icon_mail.png) left no-repeat;padding-left: 30px;background-position-x:5px;'}))
    pwd = forms.CharField(label='', min_length=8, widget=forms.PasswordInput(attrs={'placeholder': 'Password',
                                                                                    'style': 'ime-mode:disabled;background: white url(/static/img/user_auth/icon_lock.png) left no-repeat;padding-left: 30px;background-position-x:5px;'}))
    name = forms.CharField(label='', min_length=3, max_length=50, widget=forms.TextInput(
        attrs={'placeholder': 'Full Name',
               'style': 'ime-mode:disabled;background: white url(/static/img/user_auth/icon_people_new.png) left no-repeat;padding-left: 30px;background-position-x:5px;'}))


class UserLoginForm(forms.Form):
    email = forms.EmailField(label='', widget=forms.TextInput(attrs={'placeholder': 'Enter E-mail address',
                                                                     'style': 'ime-mode:disabled;background: white url(/static/img/user_auth/icon_mail.png) left no-repeat;padding-left: 30px;background-position-x:5px;'}))
    pwd = forms.CharField(label='', min_length=4, widget=forms.PasswordInput(attrs={'placeholder': 'Password',
                                                                                    'style': 'ime-mode:disabled;background: white url(/static/img/user_auth/icon_lock.png) left no-repeat;padding-left: 30px;background-position-x:5px;'}))
