# -*- encoding: utf-8 -*-
import re

from django.contrib.auth import authenticate
from django.contrib.auth.hashers import make_password
from django.db import transaction

from common.models import LanguageInfo, TimezoneInfo
from common.service import CommonService
from settings.models import SettingsNotification
from tutor.models import Tutee, Tutor
from tutor.service import TutorService
from user_auth.models import TellpinAuthUser, TellpinUser
from user_auth.utils import auth_email


# from tutor.service import TutorService
class UserAuthService(object):
    def __init__(self):
        pass

    @staticmethod
    def encrypt_password(pw):
        salt = "OlbLswqIKhhZ"
        return make_password(pw, salt, 'pbkdf2_sha256')

    @staticmethod
    def check_password(user, pw):
        """
        @summary: 비밀번호 확인
        @param user: 현재 로그인된 유저
        @param pw: 비밀번호
        @return: True, False
        """
        return user.check_password(pw)

    @staticmethod
    def check_user(email, pw):
        """
        @summary: 유저 패스워드를 암호화 하는 함수
        @author: msjang
        @param email: login user
        @param pw: user password
        @return: hashed password data
        """
        user = authenticate(username=email, password=pw)

        return user

    @staticmethod
    @transaction.atomic
    def sign_up(email, pw, name, signup_step, interest_teaching, interest_learning,
                is_tutor=0, facebook_uid="", twitter_uid="", google_uid=""):
        """
        @summary: 회원가입
        @author: msjang
        @param: email, pw, name, signup_step, interest_teaching, interest_learning,
                is_tutor, facebook_uid, twitter_uid, google_uid
        @return True, False
        """
        result = True
        try:
            if (not facebook_uid) and (not twitter_uid) and (not google_uid):
                new_user = TellpinAuthUser.objects.create_user(email, pw)
            else:
                new_user = TellpinAuthUser.objects.get(email=email)
            new_user.set_password(pw)#9999
            new_user.save()
            TellpinUser(
                user_id=new_user.id,
                email=email,
                pw=new_user.password,
                name=name,
                signup_step=signup_step,
                interest_teaching=interest_teaching,
                interest_learning=interest_learning,
                is_tutor=is_tutor,
                facebook_uid=facebook_uid,
                twitter_uid=twitter_uid,
                google_uid=google_uid,
                del_by_user=0,
                del_by_admin=0
            ).save()
            Tutee(
                user_id=new_user.id,
                name=name,
                timezone_id_id=35,
                timezone='(UTC) Dublin, Edinburgh, Lisbon, London'
            ).save()
            SettingsNotification(user_id = new_user.id).save()
        except Exception as err:
            print "@@@@@@sign_up error " + str(err)
            print str(err)
            result = False
        finally:
            return result

    @staticmethod
    def setLoginSession(request, user):
        """
        @summary: 로그인
        @author: msjang
        @param request: request
        @param user: user
        @return True, False
        """
        print "setLoginSession"

        for key in request.session.keys():
            del request.session[key]
        tellpin_user = user.tellpinuser
        print tellpin_user.signup_step
        request.session['email'] = tellpin_user.email
        request.session['id'] = tellpin_user.pk
        request.session['name'] = tellpin_user.name
        if Tutor.objects.filter(user=tellpin_user.user_id).exists():
            if Tutor.objects.get(user=tellpin_user.user_id).photo_filename is not None:
                user_obj = Tutor.objects.get(user=tellpin_user.user_id)
                request.session['photo'] = user_obj.photo_filename
                request.session['name'] = user_obj.name
        elif Tutee.objects.get(user=tellpin_user.user_id).photo_filename is not None:
            user_obj = Tutee.objects.get(user=tellpin_user.user_id)
            request.session['photo'] = user_obj.photo_filename
            request.session['name'] = user_obj.name
        else:
            request.session['photo'] = None
        request.session['level'] = tellpin_user.signup_step
        request.session['is_tutor'] = int(tellpin_user.is_tutor)
        request.session['utc_time'] = tellpin_user.tutee.timezone_id.utc_time
        request.session['utc_delta'] = tellpin_user.tutee.timezone_id.utc_delta
        request.session['utc_location'] = tellpin_user.tutee.timezone_id.utc_location
        request.session['currency_exchange'] = TutorService.get_currency_exchange(tellpin_user.user_id)

    @staticmethod
    def email_check(email):
        """
        @summary: 이메일 중복 체크
        @author: msjang
        @param email : user email
        @return True, False
        """
        pass

    @staticmethod
    def signup_validation(email, pw, name, social_user):
        """
        @summary: 회원가입 전 validation 체크
        @author: msjang
        @param email: user email
        @param pw: user password
        @param name: user name
        @parma social_user
        @return json
        """
        print "signup_validation"
        result = {
            "success": False,
            "email": False,
            "password": False,
            "message": "No state"
        }

        try:
            email_flag = re.search("^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$",
                                   email)
            if not email_flag:
                result["success"] = False
                result["email"] = True
                result["password"] = False
                result["name"] = False
                result["message"] = "Please provide a valid email address"

                return result

            pw_flag = re.search("\s", pw)  # 공백 체크

            if pw_flag:
                result["success"] = False
                result["email"] = False
                result["password"] = True
                result["name"] = False
                result["message"] = "8 to 20 characters, case-sensitive,\n" \
                                    "Must contain at least one alpha character\n" \
                                    "[a-z,A-Z] and one numeric character[0-9], \n" \
                                    "Can contain following special characters:" \
                                    "!,@,#,$,%,^,&,*,?,_,~"

                return result

            pw_format_flag = re.search("([a-zA-Z].*[0-9])|([0-9].*[a-zA-Z])", pw)

            if not pw_format_flag:
                result["success"] = False
                result["email"] = False
                result["password"] = True
                result["name"] = False
                result["message"] = "8 to 20 characters, case-sensitive,\n" \
                                    "Must contain at least one alpha character\n" \
                                    "[a-z,A-Z] and one numeric character[0-9], \n" \
                                    "Can contain following special characters:" \
                                    "!,@,#,$,%,^,&,*,?,_,~"

                return result

            pw_email_flag = re.search(email, pw)  # 비밀번호가 이메일과 같은지 검사

            if pw_email_flag:
                result["success"] = False
                result["email"] = False
                result["password"] = True
                result["name"] = False
                result["message"] = "8 to 20 characters, case-sensitive,\n" \
                                    "Must contain at least one alpha character\n" \
                                    "[a-z,A-Z] and one numeric character[0-9], \n" \
                                    "Can contain following special characters:" \
                                    "!,@,#,$,%,^,&,*,?,_,~"

                return result

            pw_len = len(pw)
            if pw_len < 8 or pw_len > 20:
                result["success"] = False
                result["email"] = False
                result["password"] = True
                result["name"] = False
                result["message"] = "8 to 20 characters, case-sensitive,\n" \
                                    "Must contain at least one alpha character\n" \
                                    "[a-z,A-Z] and one numeric character[0-9], \n" \
                                    "Can contain following special characters:" \
                                    "!,@,#,$,%,^,&,*,?,_,~"

                return result

            name_len = len(name)
            if name_len < 3 or name_len > 50:
                result["success"] = False
                result["email"] = False
                result["password"] = False
                result["name"] = True
                result["message"] = "Between 3 and 50 characters without special characters"

                return result

            if TellpinUser.objects.filter(email=email).exists():
                result["success"] = False
                result["email"] = True
                result["password"] = False
                result["name"] = False
                result["message"] = "The email address is already being used."

                return result

            if social_user is not None: #소셜 회원가입 유효성 체크
                if social_user.user.email.count('@') >= 1 and social_user.user.email != email:
                    # 트위터가 아닌 경우 이메일 변경을 허용하지 않음
                    result["success"] = False
                    result["email"] = True
                    result["password"] = False
                    result["name"] = False

                    # 소셜 계정에 따라 메세지를 다르게 출력
                    verify_email_str = "The email address is not verified by ";
                    if social_user.provider == 'google-oauth2':
                        verify_email_str = verify_email_str + 'google'
                    elif social_user.provider == 'facebook':
                        verify_email_str = verify_email_str + 'facebook'
                    elif social_user.provider == 'twitter':
                        verify_email_str = verify_email_str + 'twitter'

                    result["message"] = verify_email_str;

                    return result

                if social_user.user.email.count('@') < 1 and TellpinAuthUser.objects.filter(email=email).exists():
                    # 트위터인 경우 이미 존재하는 이메일로 변경을 허용하지 않음
                    result["success"] = False
                    result["email"] = True
                    result["password"] = False
                    result["name"] = False
                    result["message"] = "The email address is already in use"

                    return result

            result["success"] = True
            result["message"] = "Valid"

            return result

        except Exception as err:
            print "@@@@@@signup_validation " + str(err)
            result["success"] = False
            result["message"] = "An error occurred. Please ask for support."

            return result

    @staticmethod
    def update_profile(user, tutee, interest_teaching, interest_learning, learning, native, timezone):
        result = CommonService.make_result()
        try:
            tutee.interest_teaching = interest_teaching
            tutee.terest_learning = interest_learning
            native_list = LanguageInfo.objects.filter(id__in=native)
            learn_list = LanguageInfo.objects.filter(id__in=learning)

            for i, native in enumerate(native_list):
                setattr(tutee, 'native' + str(i+1) + '_id', native)
                setattr(tutee, 'native' + str(i+1), native.lang_english)

            for i, learn in enumerate(learn_list):
                setattr(tutee, 'lang' + str(i+1) + '_id', learn)
                setattr(tutee, 'lang' + str(i+1), learn.lang_english)
                setattr(tutee, 'lang' + str(i+1) + '_learning', True)

            user_timezone = TimezoneInfo.objects.filter(utc_time=timezone).first()
            tutee.timezone_id = user_timezone
            tutee.timezone = user_timezone.utc_location
            tutee.save()
            user.signup_step = 3
            user.name = user.name.encode('utf8')
            user.save()

            auth_email(user)
        except Exception as err:
            print "@@@@@@update_profile " + str(err)
            result = CommonService.make_result(0, str(err))
        finally:
            return result

    @staticmethod
    def check_exsiting_user(email):
        return TellpinUser.objects.get(email=email)

    @staticmethod
    def check_auth_user(email):
        return TellpinAuthUser.objects.get(email=email)

    @staticmethod
    def check_social_user(provider, uid):
        print "##########check_social_user"
        if provider == 'facebook':
            user = TellpinUser.objects.filter(facebook_uid=uid)
        elif provider == 'twitter':
            user = TellpinUser.objects.filter(twitter_uid=uid)
        elif provider == 'google-oauth2':
            user = TellpinUser.objects.filter(google_uid=uid)
        else:
            return None

        if user:
            return user[0]
        else:
            return None

    @staticmethod
    def update_social_uid(user, *uids):
        """
        @summary 사용자의 소셜 계정 id 를 업데이트
        @param user: 사용자
        @param uids: 업데이트할 소셜 계정 id 딕셔너리. ex) uids = { 'facebook_uid':101023, 'twitter_uid':123543, ... }
        @return result
        """

        facebook_uid = uids.get('facebook_uid')
        twitter_uid = uids.get('twitter_uid')
        google_uid = uids.get('google_uid')

        result = CommonService.make_result()

        try:
            if not facebook_uid:
                user.facebook_uid = facebook_uid

            if not twitter_uid:
                user.twitter_uid = twitter_uid

            if not google_uid:
                user.google_uid = google_uid

            user.save()
        except Exception as err:
            print "@@@@@@update_social_uid " + str(err)
            result = CommonService.make_result(0, str(err))
        finally:
            return result

    @staticmethod
    def update_auth_user_email(auth_user, email):
        """
        @summary auth_user 테이블의 email 필드를 업데이트
        @param auth_user: 업데이트 할 auth_user
        @param email: 업데이트 할 이메일
        @return result
        """
        result = CommonService.make_result()

        try:
            auth_user.email = email
            auth_user.save()
        except Exception as err:
            print "@@@@@@update_auth_user_email " + str(err)
            result = CommonService.make_result(0, str(err))
        finally:
            return result

