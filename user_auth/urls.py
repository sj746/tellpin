# -*- encoding: utf-8 -*-

from django.conf.urls import include, url
import views

# retry_email.html 을 호출하여 이메일 인증메일 재전송.
# register_confirm:
# retry_activation:
# register/success:

# 패스워드 리셋 
# reset: 패스워드 변경 페이지 이동.
# reset_confirm: 패스워드 변경이메일 확인
# retry_password_activation: 패스워드 변경 이메일 재전송
# reset/success/: 패스워드 변경 성공페이지 이동


urlpatterns = [
    url(r'^$', views.landing_page),
    url(r'^home/$', views.home),
    url(r'^login/$', views.login_tellpin),
    url(r'^complete/(?P<uid>.*)$', views.complete),
    url(r'^complete2/(?P<uid>.*)$', views.complete2),
    url(r'^profile/$', views.profile),
    url(r'^resend_email/$', views.resend_email),
    url(r'^mobile_login/$', views.mobile_login),
    url(r'^logout/$', views.logout),
    url(r'^ajax_logout/$', views.ajax_logout),
    url(r'^signup/$', views.signup),
    url(r'^ajax_signup/$', views.ajax_signup),
    url(r'^ajax_social_signup/$', views.ajax_social_signup),
    url(r'^ajax_login/$', views.ajax_login),
    url(r'^ajax_check_password/$', views.ajax_check_password),
    url(r'^ajax_check_email/$', views.ajax_check_email),
    url(r'^social_login/$', views.social_login),
    url(r'^signup_page/$', views.signup_page),
    url(r'^login_form/$', views.login_form),
    url(r'^login_page/$', views.login_page),
    url(r'^signup_form/$', views.signup_form),
    url(r'^reset/$', views.reset_page, name='reset_page'),
    url(r'^reset_password/$', views.reset_password, name='reset_password'),
    url(r'^register_confirm/(?P<activation_key>.*)$', views.register_confirm),
    url(r'^reset_confirm/(?P<activation_key>.*)$', views.reset_confirm),
    url(r'^retry_activation/$', views.retry_activation),
    url(r'^retry_password_activation/$', views.retry_password_activation),
    url(r'^register/success/$', views.register_success),
    url(r'^reset/success/$', views.reset_success, name='reset_page'),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^language/$', views.language_select),
    url(r'^tellpinsearch_login/$', views.tellpin_search_login),
    url(r'^tellpinsearch_login/main/$', views.tellpin_search_login_main),
    url(r'^get_notification/$', views.ajax_get_notification),
    url(r'^set_notification_status/$', views.ajax_set_notification_status),
    url(r'^set_noti_lesson/$', views.landing_page),
    url(r'^set_noti_post/$', views.landing_page),
    url(r'^set_noti_comment/$', views.landing_page),
    url(r'^set_noti_comment2/$', views.landing_page),
    url(r'^set_noti_upvote/$', views.landing_page),

    url(r'^ajax_nologin/$', views.ajax_nologin),
]
