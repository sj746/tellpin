# -*- encoding: utf-8 -*-

from datetime import datetime, timedelta
import json
import logging
import urllib2
import re

from django import forms
from django.contrib.auth import login as auth_login, authenticate
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.views import login
from django.contrib.sessions.models import Session
from django.db.backends.oracle.creation import PASSWORD
from django.http.response import HttpResponse, HttpResponseRedirect, \
    JsonResponse
from django.shortcuts import render, render_to_response, get_object_or_404, redirect
from django.template.context import RequestContext
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from openid.consumer.consumer import Response
from social.pipeline.social_auth import social_user

from common.common import tellpin_login_required
from common.service import CommonService
# from exchange.views import ts
from common.models import LanguageInfo
from tellpin.settings import TELLPIN_HOME, INTEREST_IMG_URL
from user_auth.forms import UserLoginForm, UserRegisterForm
# from common.models import TellpinUser, Language, Interest, UserProfile, \
#     ResetPwd, DjangoSession, Timezone
from user_auth.models import TellpinUser, UserAuth, UserResetPW, TellpinAuthUser
from user_auth.service import UserAuthService
from user_auth.utils import auth_email, reset_email, check_number, check_letter, \
    check_specialletter, get_notification, set_notification_status, encrypt_password, \
    get_social_user_from_request
from tutor.models import Tutee, Tutor

logger = logging.getLogger('mylogger')


# Create your views here.
# 로그인 페이지 (랜딩페이지)
def landing_page(request):
    """
    @summary: 랜딩페이지 호출 함수
    @author: khyun
    @param none
    @return: url - user_auth/landingpage.html
    """
    print 'landing_page()'

    obj = []
    try:
        if request.session['email']:
            level = request.session['level']
            print level
            # TODO level check
            # level - 0:세부입력 미완료 1:세부입력완료->관심사페이지이동 2:관심사선택완료->도움말페이지로(이메일 인증 미완료 상태.) 3:이메일인증완료(이메일클릭시처리)
            if level == 0:
                print 'level 0'
                # 0: 세부입력 페이지 이동

                language = LanguageInfo.objects.filter()
                for lang in language:
                    data = {}
                    data['id'] = lang.id
                    #data['language1'] = lang.language1
                    #data['language2'] = lang.language2
                    data['language1'] = lang.lang_english
                    data['language2'] = lang.lang_local
                    obj.append(data)

                language = json.dumps(obj)
                # return render_to_response('mypage/user_profile.html', {'languages':language}, RequestContext(request))
                # return HttpResponseRedirect('/main/', {'language': language}, RequestContext(request))
                return HttpResponseRedirect('/dashboard/', {'language': language}, RequestContext(request))
                # return render_to_response('tutor/tutor_find.html', RequestContext(request))
            elif level == 2:
                print 'level 2'
                # 1: 세부입력 완료 관심사 페이지 이동
                # interests = Interest.objects.filter()
                # url = INTEREST_IMG_URL
                # return render_to_response('mypage/my_interest.html', {'interests':interests, 'url':url}, RequestContext(request))
                language = LanguageInfo.objects.filter()
                for lang in language:
                    data = {}
                    data['id'] = lang.id
                    #data['language1'] = lang.language1
                    #data['language2'] = lang.language2
                    data['language1'] = lang.lang_english
                    data['language2'] = lang.lang_local
                    obj.append(data)

                language = json.dumps(obj)
                # email confirm
                # return HttpResponseRedirect('/main/', {'language': language}, RequestContext(request))
                return HttpResponseRedirect('/dashboard/', {'language': language}, RequestContext(request))
                # return render_to_response('tutor/tutor_find.html', RequestContext(request))
            else:
                print level
                # user = TellpinUser.objects.get(email = request.session['email'])
                # print 'test'
                # auth_login(request, user)
                # print 'session'
                # print request.session
                # return HttpResponseRedirect('/main/', RequestContext(request))
                return HttpResponseRedirect('/dashboard/', RequestContext(request))

        form = UserLoginForm()
        language = LanguageInfo.objects.filter()
        CommonService.change_local_language(request, 1)
        return render_to_response("user_auth/landingpage.html", {'form': form, 'languages': language},
                                  RequestContext(request))

    except Exception as e:
        print "landing_page exception"
        print e
        form = UserLoginForm()
        language = LanguageInfo.objects.filter()
        CommonService.change_local_language(request, 1)
        return render_to_response("user_auth/landingpage.html", {'form': form, 'languages': language},
                                  RequestContext(request))


def home(request):
    """
         사용하지 않는 함수
    """
    print "home"
    return render_to_response('user_auth/landingpage.html')


# 회원가입 페이지
def signup_page(request):
    """
    @summary: 회원가입 페이지 호출 함수
    @author: khyun
    @param request
    @return: url - user_auth/registration.html
    """
    print "signup_page"
    message = ""
    form = UserRegisterForm()
    home = TELLPIN_HOME

    # twitter 가 아닌 소셜 회원가입 진행 중이면 이메일 필드를 readonly 로 바꿔야 함
    social_user = get_social_user_from_request(request)
    if social_user is not None and social_user.user.email.count('@') >= 1:
        form.fields['email'].widget.attrs['readonly'] = True

    return render_to_response("user_auth/registration.html", {'message': message, 'form': form, 'home': home},
                              RequestContext(request))


# 로그인 페이지
def login_page(request):
    """
    @summary: 모바일 로그인 페이지 호출 함수
    @author: khyun
    @param none
    @return: url - user_auth/login_page.html
    """
    print "login_page"
    message = ""
    form = UserLoginForm()
    home = TELLPIN_HOME

    return render_to_response("user_auth/login_page.html", {'message': message, 'form': form}, RequestContext(request))


# 소셜 로그인 및 회원가입 페이지 호출
def signup_form(request):
    """
    @summary: 소셜 버튼 클릭 후 회원가입 페이지 호출 함수
    @author: khyun, chsin
    @param none
    @return: url - user_auth/resgistration.html
    """
    print "signup_form"

    if str(request.user) == 'AnonymousUser':
        url = TELLPIN_HOME
        form = UserRegisterForm()
        return render_to_response("user_auth/registration.html", {'url': url, 'form': form}, RequestContext(request))

    social_user = get_social_user_from_request(request)

    provider = social_user.provider
    extra_data = social_user.extra_data
    access_token = extra_data.get('access_token')

    uids = {}
    if provider == 'facebook':
        uids['facebook_uid'] = social_user.uid
    elif provider == 'twitter':
        uids['twitter_uid'] = social_user.uid
    elif provider == 'google-oauth2':
        uids['google_uid'] = social_user.uid
    else:
        uids = None

    #기존 회원이 소셜 아이디 연동할 때 DB 데이터 업데이트
    #현재 미구현 상태임
    if request.session.has_key('email'):
        existing_user = UserAuthService.check_exsiting_user(request.session['email'])
        result = UserAuthService.update_social_uid(existing_user, uids)
        return render_to_response("mypage/friends_list.html", RequestContext(request))

    #소셜 아이디로 로그인 하기
    login_social_user = UserAuthService.check_social_user(provider, social_user.uid)
    if login_social_user is not None:
        message = "아이디 또는 비밀번호를 확인해 주세요."
        login_user = UserAuthService.check_user(login_social_user.email, login_social_user.pw)    #향후 비밀번호 처리 필요9999
        if login_user is not None:
            UserAuthService.setLoginSession(request, login_user)
            auth_login(request, login_user)
            return redirect('/dashboard/')
        else:
            form = UserLoginForm()
            language = LanguageInfo.objects.filter()
            return render_to_response('user_auth/login_form.html', 
                                      {'message': message, 'form': form, 'languages': language},
                                      RequestContext(request))

    # 회원아님->가입화면
    url = TELLPIN_HOME
    form = UserRegisterForm()

    # twitter 가 아닌 소셜 회원가입 진행 중이면 이메일 필드를 readonly 로 바꿔야 함
    social_user = get_social_user_from_request(request)
    if social_user is not None and social_user.user.email.count('@') >= 1:
        form.fields['email'].widget.attrs['readonly'] = True

    return render_to_response("user_auth/registration.html", {'url': url, 'form': form}, RequestContext(request))



def login_form(request):
    """
    @summary: 로그인 페이지 호출
    @author: khyun
    @param none
    @return: url - user_auth/login_form.html
    """
    print "login_formlogin_formlogin_formlogin_form"
    form = UserLoginForm()
    language = LanguageInfo.objects.all()
    return render_to_response("user_auth/login_form.html", {'form': form, 'languages': language},
                              RequestContext(request))


# 회원가입 중복 체크
@csrf_exempt
def ajax_signup(request):
    """
    @summary: 회원가입 전 validation 체크 함수.
    @author: khyun
    @param none
    @return: json
    """
    # TODO
    # EMAIL 중복 체크
    # 닉네임 중복체크
    # 패스워드 길이 체크 (유효성없음, 클라이언트 동시 체크)
    print "ajax_signup"


    # 회원 이메일, 패스워드, 이름의 중복성 체크를 위해 변수에 저장
    email = request.POST['email']
    pwd = request.POST['pwd']
    name = request.POST['name']

    #소셜 가입 시 auth_user 이메일 체크를 위해 변수에 저장
    social_user = get_social_user_from_request(request)

    result = UserAuthService.signup_validation(email, pwd, name, social_user)

    return JsonResponse(result)


        # message = ""
        # result = {"success": True, 'message': ""}
        # json_result = ""

        # 이메일 형식 체크
    #     email_format = re.search("^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$",
    #                              email)
    #     print bool(email_format)  # true ok email format
    #     email_format = bool(email_format)
    #     if email_format == False:
    #         print "ajax_signup email"
    #         result['success'] = False
    #         result['email'] = True
    #         result['password'] = False
    #         result['name'] = False
    #         result['message'] = "Please provide a valid email address"
    #         json_result = json.dumps(result)
    #         return HttpResponse(json_result, RequestContext(request))
    #
    #     # 패스워드 유효성 체크
    #     pwd_format1 = re.search("\s", pwd)
    #     print bool(pwd_format1)  # true -> 공백존재
    #     pwd_format1 = bool(pwd_format1)
    #     if pwd_format1:
    #         print "ajax_signup pwd"
    #         result['success'] = False
    #         result['email'] = False
    #         result['password'] = True
    #         result['name'] = False
    #         result['message'] = "8 to 20 characters, case-sensitive,\n" \
    #                             "Must contain at least one alpha character\n" \
    #                             "[a-z,A-Z] and one numeric character[0-9], \n" \
    #                             "Can contain following special characters:" \
    #                             "!,@,#,$,%,^,&,*,?,_,~"
    #         json_result = json.dumps(result)
    #         return HttpResponse(json_result, RequestContext(request))
    #
    #     pwd_format2 = re.search("([a-zA-Z].*[0-9])|([0-9].*[a-zA-Z])", pwd)
    #     print bool(pwd_format2)  # true -> format ok.
    #     pwd_format2 = bool(pwd_format2)
    #     if pwd_format2 == False:
    #         print "ajax_signup pwd"
    #         result['success'] = False
    #         result['email'] = False
    #         result['password'] = True
    #         result['name'] = False
    #         result['message'] = "8 to 20 characters, case-sensitive,\n" \
    #                             "Must contain at least one alpha character\n" \
    #                             "[a-z,A-Z] and one numeric character[0-9], \n" \
    #                             "Can contain following special characters:" \
    #                             "!,@,#,$,%,^,&,*,?,_,~"
    #         json_result = json.dumps(result)
    #         return HttpResponse(json_result, RequestContext(request))
    #
    #     # 이메일 주소 = 패스워드 x
    #     pwd_format3 = re.search(email, pwd)
    #     print bool(pwd_format3)  # true -> 일치. 금지.
    #     pwd_format3 = bool(pwd_format3)
    #     if pwd_format3:
    #         print "ajax_signup pwd"
    #         result['success'] = False
    #         result['email'] = False
    #         result['password'] = True
    #         result['name'] = False
    #         result['message'] = "8 to 20 characters, case-sensitive,\n" \
    #                             "Must contain at least one alpha character\n" \
    #                             "[a-z,A-Z] and one numeric character[0-9], \n" \
    #                             "Can contain following special characters:" \
    #                             "!,@,#,$,%,^,&,*,?,_,~"
    #         json_result = json.dumps(result)
    #         return HttpResponse(json_result, RequestContext(request))
    #     '''
    #     pwd_format4 = re.search("(\w)\1\1", pwd)
    #     print pwd
    #     print bool(pwd_format4)
    #      '''
    #     # 이메일이 중복되는지 확인
    #     user = TellpinUser.objects.filter(email=email)
    #     if user:
    #         # 이메일 중복 처리
    #         print "ajax_signup email"
    #         result['success'] = False
    #         result['email'] = True
    #         result['password'] = False
    #         result['name'] = False
    #         result['message'] = "The email address is already being used."
    #         json_result = json.dumps(result)
    #         return HttpResponse(json_result, RequestContext(request))
    #
    #     # 비밀번호 길이 확인
    #     if len(pwd) < 8 or len(pwd) > 20:
    #         # 비밀번호 4자리 이상 처리
    #         print "ajax_signup password"
    #         result['success'] = False
    #         result['email'] = False
    #         result['password'] = True
    #         result['name'] = False
    #         result['message'] = "8 to 20 characters, case-sensitive,\n" \
    #                             "Must contain at least one alpha character\n" \
    #                             "[a-z,A-Z] and one numeric character[0-9], \n" \
    #                             "Can contain following special characters:" \
    #                             "!,@,#,$,%,^,&,*,?,_,~"
    #         json_result = json.dumps(result)
    #         return HttpResponse(json_result, RequestContext(request))
    #
    #     if len(name) < 3 or len(name) > 50:
    #         print "ajax_signup name length"
    #         result['success'] = False
    #         result['email'] = False
    #         result['password'] = False
    #         result['name'] = True
    #         result['message'] = "Between 3 and 50 characters without special characters"
    #         json_result = json.dumps(result)
    #         return HttpResponse(json_result, RequestContext(request))
    #
    #     '''
    #     #닉네임 중복 확인
    #     user = TellpinUser.objects.filter(name = name)
    #     if user:
    #         #닉네임 중복 처리
    #         print "ajax_signup name"
    #         result['success'] = False
    #         result['email'] = False
    #         result['password'] = False
    #         result['name'] = True
    #         result['message'] = "이미 사용 중인 닉네임 입니다."
    #         json_result = json.dumps(result)
    #         return HttpResponse(json_result, RequestContext(request))
    #     '''
    #
    #     # 회원가입 처리
    #     print "ajax_signup success"
    #     result['success'] = True
    #     result['message'] = "valid"
    #     json_result = json.dumps(result)
    #     return HttpResponse(json_result, RequestContext(request))
    #
    # except Exception as e:
    #     print e
    #     result['success'] = False
    #     result['message'] = "An error occurred. Please ask for support."
    #     json_result = json.dumps(result)
    #     return HttpResponse(json_result, {'message': message}, RequestContext(request))
    #
    # return message

# 소셜 회원가입 중복 체크
@csrf_exempt
def ajax_social_signup(request):
    """
    @summary: 소셜 회원가입 validation check 함수
    @author: khyun
    @param none
    @return: json
    """
    # TODO
    # EMAIL 중복 체크
    # 닉네임 중복체크
    # 패스워드 길이 체크 (유효성없음, 클라이언트 동시 체크)
    print "ajax_social_signup"

    try:
        # 회원 이메일, 패스워드, 이름의 중복성 체크를 위해 변수에 저장
        email = request.POST['email']
        pwd = request.POST['pwd']
        name = request.POST['name']
        message = ""
        result = {"success": True, 'email': "", 'pwd': "", 'name': ""}
        json_result = ""

        # 이메일이 중복되는지 확인
        user = TellpinUser.objects.filter(email=email)
        if user:
            # 이메일 중복 처리
            print "ajax_social_signup email"
            result['success'] = False
            result['email'] = "이미 사용 중인 이메일 주소 입니다."

        # 비밀번호 길이 확인
        if len(pwd) <= 3:
            # 비밀번호 4자리 이상 처리
            print "ajax_social_signup password"
            result['success'] = False
            result['pwd'] = "비밀번호는 4자리 이상이어야 합니다."

        if len(name) <= 0:
            print "ajax_social_signup name length"
            result['success'] = False
            result['name'] = "닉네임을 입력해 주세요."

        # 닉네임 중복 확인
        user = TellpinUser.objects.filter(name=name)
        if user:
            # 닉네임 중복 처리
            print "ajax_social_signup name"
            result['success'] = False
            result['name'] = "이미 사용 중인 닉네임 입니다."

        if not result['success']:
            json_result = json.dumps(result)
            return HttpResponse(json_result, RequestContext(request))

        # 회원가입 처리
        print "ajax_social_signup success"
        result['success'] = True
        result['message'] = "valid"
        json_result = json.dumps(result)
        return HttpResponse(json_result, RequestContext(request))

    except Exception as e:
        print e
        result['success'] = False
        result['message'] = "예외. 관리자에게 문의해 주세요.  : tellpinadmin@unichal.com"
        json_result = json.dumps(result)
        return HttpResponse(json_result, {'message': message}, RequestContext(request))

    return message


# 회원가입
@csrf_exempt
def signup(request):
    """
    @summary: 회원가입 처리 함수.
    @author: khyun
    @param none
    @return: url - /dashboard/
    """
    print "signup"
    # TODO
    # 일반회원 회원가입 구현.
    email = request.POST['email']
    pwd = request.POST['pwd']
    name = request.POST['name']
    level = 0
#    facebook_flag = False
#    twitter_flag = False
#    google_flag = False
    facebook_uid = ''
    twitter_uid = ''
    google_uid = ''

    #소셜 회원가입 진행 시
    social_user = get_social_user_from_request(request)
    if social_user and social_user.user:
        provider = social_user.provider

        # uid 얻어 오기
        if provider == 'facebook':
            facebook_uid = social_user.uid
        elif provider == 'twitter':
            twitter_uid = social_user.uid
        elif provider == 'google-oauth2':
            google_uid = social_user.uid

        # 소셜 인증한 이메일
        social_email = social_user.user.email

        # 예외처리. 인증한 이메일과 가입 이메일이 다른 경우
        # 트위터 외에는 이메일 변경 불허
        if email != social_email:
            if social_email.count('@') < 1:   #트위터
                auth_user = UserAuthService.check_auth_user(social_email)

                if auth_user is None:
                    return HttpResponse("관리자 문의 바람! : tellpinadmin@unichal.com")

                result = UserAuthService.update_auth_user_email(auth_user, email)

                if result['isSuccess'] != 1:    #업데이트 실패
                    return HttpResponse("관리자 문의바람!! : tellpinadmin@unichal.com")

            else:   #페이스북, 구글
                return HttpResponse("관리자 문의바람!!! : tellpinadmin@unichal.com")

    result = UserAuthService.sign_up(email, pwd, name, level, 0, 0, 0, facebook_uid, twitter_uid, google_uid)
    user = UserAuthService.check_user(email, pwd)
    if user and result:
        UserAuthService.setLoginSession(request, user)
        auth_login(request, user)
        CommonService.change_local_language(request, 0)

        return redirect('/dashboard/')
    else:
        return HttpResponse("관리자 문의바람!!!! : tellpinadmin@unichal.com")

    # if str(request.user) != 'AnonymousUser':
    #
    #     social_user = request.user.social_auth.get()
    #     provider = social_user.provider
    #     access_token = social_user.extra_data.get('access_token')
    #     uid = social_user.uid
    #
    #     # TODO
    #     # 기존 소셜회원가입 유저 -> 로그인 ( 이메일, flag 값 비교 )
    #
    #
    #     """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    #
    #     if provider == 'facebook':
    #         print 'facebook signup'
    #         facebook_flag = True
    #         facebook_uid = uid
    #     elif provider == 'twitter':
    #         print 'twitter signup'
    #         twitter_flag = True
    #         twitter_uid = uid
    #     else:
    #         print 'google signup'
    #         google_flag = True
    #         google_uid = uid
    #
    # try:
    #     user = TellpinUser.objects.filter(email=email)
    #     print user
    #     if user:
    #         # 이메일 회원 존재, 로그인 페이지 이동
    #         # TODO, 회원이 존재한다는 알림을 보여주도록 변경 ( 현재 정해진 사항 없음 )
    #         print "signup fail, " + user.email
    #         return render_to_response("/", RequestContext(request))
    #     else:
    #         # 회원가입 진행
    #         print "signup tellpin"
    #         print pwd
    #         user = TellpinUser(
    #             email=email,
    #             password=pwd,
    #             name=name,
    #             coin=0,
    #             exp=0,
    #             profile_photo="",
    #             facebook=facebook_flag,
    #             twitter=twitter_flag,
    #             google=google_flag,
    #             facebook_uid=facebook_uid,
    #             twitter_uid=twitter_uid,
    #             google_uid=google_uid,
    #             mailing=True,
    #             reg_date=datetime.now(),
    #             last_login=datetime.now(),
    #             display_language=1,
    #             level=level
    #         )
    #         user.save()
    #         # 로그인 페이지 이동
    #         # TODO
    #         # 자동 로그인 처리 -> 메인 페이지로 이동하도록 변경
    #         # 세션 처리.
    #         #             request.session['email'] = user.email
    #         #             request.session['name'] = user.name
    #         #             request.session['level'] = user.level
    #         #             request.session['photo'] = "/static/img/upload/profile/" + user.profile_photo;
    #         #             request.session['coin'] = user.coin;
    #         #             request.session['exp'] = user.exp;
    #         #             request.session['is_tutor'] = user.is_tutor;
    #         #             request.session['id'] = user.id;
    #         set_user_session(request, user)
    #
    #         language = Language.objects.filter()
    #         # return render_to_response('mypage/my_profile.html', {'languages': language}, RequestContext(request))
    #         return HttpResponseRedirect('/dashboard/', RequestContext(request))



    # except Exception as e:
    #     print "exception"
    #     print e
    #     return HttpResponse("signup error 관리자에게 문의해 주세요.")


# 로그인 중복 체크
@csrf_exempt
def ajax_login(request):
    """
    @summary: 로그인 validation check 함수
    @author: khyun
    @param none
    @return: json
    """
    # TODO
    # 패스워드 체크 (유효성없음, 클라이언트 동시 체크)
    print "ajax_login"
    # TODO
    # 일반회원 로그인 구현.
    email = request.POST['email']
    pwd = request.POST['pwd']

    form = UserLoginForm()
    try:
        # email check
        user = UserAuthService.check_user(email, pwd)
        # 이메일 회원 존재
        print user
        if user is not None:
            if user.is_active:
                resData = dict()
                resData["success"] = True
                jsonData = json.dumps(resData)

                return HttpResponse(jsonData, RequestContext(request))
            else:
                print "password invalid"
                message = "You entered an invalid email or password"

                resData = {}
                resData["success"] = False
                resData["message"] = message
                jsonData = json.dumps(resData)
                return HttpResponse(jsonData, RequestContext(request))
                # 권한 여부
        else:
            # 이메일 존재안함.
            print "email invalid"
            message = "You entered an invalid email or password"

            resData = {}
            resData["success"] = False
            resData["message"] = message
            jsonData = json.dumps(resData)

            return HttpResponse(jsonData, RequestContext(request))
            # return render_to_response('user_auth/landingpage.html', {'message':message, 'form':form}, RequestContext(request))
            # return render_to_response('이메일 없음 (페이지구현 or 안내말 ). 로그인 실패')
            # return render_to_response('user_auth/login_form.html', {'message': message, 'form': form, 'languages':language}, RequestContext(request))

    except Exception as e:
        print '@@@@@@@@@@@ajax_login error', str(e)
        message = "login error, please ask to administrator"
        # return HttpResponse("Login error 관리자에게 문의하세요")
        resData = {}
        resData["success"] = False
        resData["message"] = message
        jsonData = json.dumps(resData)

        return HttpResponse(jsonData, RequestContext(request))
    '''
    try:
        #회원 이메일, 패스워드, 이름의 중복성 체크를 위해 변수에 저장
        email = request.POST['email']
        pwd = encrypt_password(request.POST['pwd'])
        message = ""
        result = {"success": True, 'message':""}
        json_result = ""

        #비밀번호 체크
        user = TellpinUser.objects.filter(email = email)
        #비밀번호 확인
        if user :
            #패스워드 일치
            if user[0].password == pwd :
                #로그인 처리
                print "ajax_login password valid"
                result['success'] = True
                result['message'] = "로그인 성공"
                json_result = json.dumps(result)
                return HttpResponse(json_result, RequestContext(request))
            #패스워드 불일치
            else:
                print "ajax_login password invalid"
                result['success'] = False
                result['message'] = "아이디 또는 패스워드를 다시 확인해 주세요."
                json_result = json.dumps(result)
                return HttpResponse(json_result, RequestContext(request))
        #사용자 존재하지 않음
        else:
            print "ajax_login email not found"
            result['success'] = False
            result['message'] = "아이디 또는 패스워드를 다시 확인해 주세요."
            json_result = json.dumps(result)
            return HttpResponse(json_result, RequestContext(request))

    except Exception as e:
        print e
        result['success'] = False
        result['message'] = "예외. 관리자에게 문의해 주세요."
        json_result = json.dumps(result)
        return HttpResponse(json_result, {'message':message}, RequestContext(request))

    return message
    '''


# profile
@csrf_exempt
@tellpin_login_required
def profile(request):
    """
         삭제예정 함수
    """
    print 'user_auth/profile'
    # get request parameter
    post_dict = json.loads(request.body)
    data = post_dict['obj']
    result = UserAuthService.update_profile(request.user.tellpinuser,
                                            request.user.tellpinuser.tutee,
                                            data['interest_teaching'],
                                            data['interest_learning'],
                                            data['learning'],
                                            data['native'],
                                            data['timezone'])

    return JsonResponse(result)


@csrf_exempt
def resend_email(request):
    """
    @summary: 인증이메일 재전송 함수
    @author: khyun
    @param none
    @return: json
    """
    print 'resend_email()'

    post_dict = json.loads(request.body)
    data = post_dict['obj']
    # TODO
    # 일반회원 로그인 구현.
    email = data['email']
    print email

    try:
        user_id = request.user.id

        user = TellpinUser.objects.get(user=user_id)

        user.signup_step = 3
        user.email = email
        user.save()

        auth_user = TellpinAuthUser.objects.get(email=user.email)
        auth_user.backend = 'django.contrib.auth.backends.ModelBackend'
#         check_user = UserAuthService.check_user(user.email, user.pw)
        obj = {}
        if auth_user is not None:
            UserAuthService.setLoginSession(request, auth_user)
            auth_login(request, auth_user)
            auth_email(user)
            obj["isSuccess"] = True
            return JsonResponse(obj)
        else:
            obj["isSuccess"] = False
            return JsonResponse(obj)
    except Exception as e:
        print "@@@@@@@@resend_email", str(e)

    resData = {}
    resData["isSuccess"] = True
    jsonData = json.dumps(resData)

    return HttpResponse(jsonData, RequestContext(request))


# email complete
@csrf_exempt
def complete(request, uid):
    """
    @summary: 회원가입후  1단계:언어선택, 2단계:관심사 선택, 3단계 이메일 확인 후 complete 버튼을 누르면 호출되는 함수
    @author: khyun
    @param request
    @param uid : user id
    @return: json
    """
    print "user_auth/views/complete"
    try:
        # user_profile에서 user정보를 다시 가져온다.
        print uid
        print uid.strip()
        print request
        user = request.user.tellpinuser
        request.session['level'] = user.signup_step
        # activation code 재전송.
        #         request.session['email']=uid
        #         request.session['level']=3
        # set_user_session(request, user)
        # request.session.clear()

        # 인증메일 재전송 페이지
        obj = []
        language = CommonService.language_list()

        return HttpResponseRedirect('/dashboard/', {'language': json.dumps(language)}, RequestContext(request))
    except Exception as e:
        # 인증코드 오류
        print '@@@@@@@@@complete', str(e)
        return render_to_response('user_auth/activation_error.html')


        # email complete


@csrf_exempt
# activation code 재전송.
def complete2(request, uid):
    """
    @summary: 이메일 미인증시 헤더 위쪽의 이메일을 인증해 달라는 문구를 누르면 나오는 이메일 재인증 팝업의 complete 버튼을 누르면 호출되는 함수
    @author: khyun
    @param uid : user id
    @return: json
    """
    print "user_auth/views/complete2"

    try:
        # user_profile에서 user정보를 다시 가져온다.
        print uid
        print uid.strip()
        print request.session['email']
        user = get_object_or_404(TellpinUser, email=request.session['email'])

        user.signup_step = 3
        user.email = uid
        user.save()

        # activation code 재전송.
        # auth_email(user)

        # request.session['email']=uid
        # request.session['level']=3
        # request.session.clear()
#         user = UserAuthService.check_user(request.session['email'], user.pw)
        auth_user = TellpinAuthUser.objects.get(email=user.email)
        auth_user.backend = 'django.contrib.auth.backends.ModelBackend'
        if auth_user is not None:
            obj = {}
            UserAuthService.setLoginSession(request, auth_user)
            auth_login(request, auth_user)
            obj["isSuccess"] = True
            return JsonResponse(obj)
#             return HttpResponseRedirect('/dashboard/', {'isSuccess': True}, RequestContext(request))
#             return redirect('/dashboard/')
        else:
            return HttpResponse("관리자 문의바람!!!!!  : tellpinadmin@unichal.com")
#         set_user_session(request, user)
        # 인증메일 재전송 페이지
        obj = []
#         language = Language.objects.filter()
#         for lang in language:
#             data = {}
#             data['id'] = lang.id
#             data['language1'] = lang.language1
#             data['language2'] = lang.language2
#             obj.append(data)
# 
#         language = json.dumps(obj)

        obj2 = {}
        obj2["isSuccess"] = True
        # return render_to_response('mypage/user_profile.html', {'languages':language}, RequestContext(request))
        # return HttpResponseRedirect('/dashboard/', {'language': language}, RequestContext(request))
        return JsonResponse(obj2)

    except Exception as e:
        # 인증코드 오류
        print '@@@@@complete2 : ', str(e)
        return render_to_response('user_auth/activation_error.html')

        # 로그인


def login_tellpin(request):
    """
    @summary: 로그인 함수
    @author: khyun
    @param request
    @return: url - /dashboard/
    """
    print "login_tellpin"
    # TODO
    # 일반회원 로그인 구현.
    email = request.POST['email']
    pwd = request.POST['pwd']
    message = "아이디 또는 비밀번호를 확인해 주세요."
    form = UserLoginForm()

    user = UserAuthService.check_user(email, pwd)
    if user is not None:
        UserAuthService.setLoginSession(request, user)
        auth_login(request, user)
        if Tutor.objects.filter(user=user.tellpinuser.user_id).exists():
            if Tutor.objects.get(user=user.tellpinuser.user_id).display_language is not None:
                display_language = Tutor.objects.get(user=user.tellpinuser.user_id).display_language
                CommonService.change_local_language(request, display_language)
            else:
                CommonService.change_local_language(request, 0)
        else:
            if Tutee.objects.get(user=user.tellpinuser.user_id).display_language is not None:
                display_language = Tutee.objects.get(user=user.tellpinuser.user_id).display_language
                CommonService.change_local_language(request, display_language)
            else:
                CommonService.change_local_language(request, 0)
        return redirect('/dashboard/')
    else:
        # message = "Please provide a valid email address. "
        form = UserLoginForm()
        language = LanguageInfo.objects.filter()
        return render_to_response('user_auth/login_form.html',
                                  {'message': message, 'form': form, 'languages': language},
                                  RequestContext(request))

    # nextUrl = '/dashboard/'
    # if 'previous_url' in request.session.keys():
    #     nextUrl = request.session['previous_url']
    #     print nextUrl
    #     del request.session['previous_url']
    #
    # try:
    #     # email check
    #     user = TellpinUser.objects.filter(email=email)
    #
    #     # 이메일 회원 존재
    #     if user:
    #         if user[0].password == pwd:
    #             # 패스워드 일치
    #             #                 request.session['email'] = user[0].email
    #             #                 request.session['name'] = user[0].name
    #             #                 request.session['level'] = user[0].level
    #             #                 request.session['photo'] = "/static/img/upload/profile/" + user[0].profile_photo;
    #             #                 request.session['coin'] = user[0].coin;
    #             #                 request.session['exp'] = user[0].exp;
    #             #                 request.session['is_tutor'] = user[0].is_tutor;
    #             #                 request.session['id'] = user[0].id;
    #             #                 if user[0].timezone != None :
    #             #                     timezone = Timezone.objects.get(id = user[0].timezone)
    #             #                     request.session['utc_time'] = timezone.utc_time;
    #             #                     request.session['utc_location'] = timezone.utc_location;
    #             set_user_session(request, user[0])
    #
    #             level = user[0].level
    #             obj = []
    #             if level == 0:
    #                 # 0: 세부입력 페이지 이동
    #                 language = Language.objects.filter()
    #                 for lang in language:
    #                     data = {}
    #                     data['id'] = lang.id
    #                     data['language1'] = lang.language1
    #                     data['language2'] = lang.language2
    #                     obj.append(data)
    #
    #                 language = json.dumps(obj)
    #                 return HttpResponseRedirect(nextUrl, {'language': language}, RequestContext(request))
    #             elif level == 2:
    #                 # 1: 세부입력 완료 관심사 페이지 이동
    #                 # interests = Interest.objects.filter()
    #                 # url = INTEREST_IMG_URL
    #                 # return render_to_response('mypage/my_interest.html', {'interests':interests, 'url':url}, RequestContext(request))
    #                 language = Language.objects.filter()
    #                 for lang in language:
    #                     data = {}
    #                     data['id'] = lang.id
    #                     data['language1'] = lang.language1
    #                     data['language2'] = lang.language2
    #                     obj.append(data)
    #
    #                 language = json.dumps(obj)
    #                 return HttpResponseRedirect(nextUrl, {'language': language}, RequestContext(request))
    #             else:
    #                 print level
    #                 return HttpResponseRedirect(nextUrl, RequestContext(request))
    #
    #         else:
    #             # 패스워드 틀림
    #             print "password invalid"
    #             message = "Please provide a valid email address."
    #             form = UserLoginForm()
    #             language = Language.objects.filter()
    #             # return render_to_response('user_auth/landingpage.html', {'message':message, 'form':form}, RequestContext(request))
    #             # return render_to_response('비밀번호 다름 (페이지구현 or 안내말). 로그인 실패')
    #             return render_to_response('user_auth/login_form.html',
    #                                       {'message': message, 'form': form, 'languages': language},
    #                                       RequestContext(request))
    #     else:
    #         # 이메일 존재안함.
    #         print "email invalid"
    #         message = "Please provide a valid email address. "
    #         form = UserLoginForm()
    #         language = Language.objects.filter()
    #         # return render_to_response('user_auth/landingpage.html', {'message':message, 'form':form}, RequestContext(request))
    #         # return render_to_response('이메일 없음 (페이지구현 or 안내말 ). 로그인 실패')
    #         return render_to_response('user_auth/login_form.html',
    #                                   {'message': message, 'form': form, 'languages': language},
    #                                   RequestContext(request))

    # except Exception as e:
    #     print 'exception'
    #     print e
    #     return HttpResponse("Login error 관리자에게 문의하세요")


# 모바일 로그인
def mobile_login(request):
    """
    @summary: 모바일 로그인 함수
    @author: khyun
    @param none
    @return: url - /dashboard/
    """
    print "login"
    # TODO
    # 일반회원 로그인 구현.
    email = request.POST['email']
    pwd = encrypt_password(request.POST['pwd'])
    message = "You entered an invalid email or password. "
    form = UserLoginForm()
    try:
        # email check
        userinfo = TellpinUser.objects.get(email=email)
        auth_user = TellpinAuthUser.objects.get(email=email)

        # 이메일 회원 존재
        if userinfo:
            if UserAuthService.check_password(auth_user, pwd):
            # 패스워드 일치
                print "password valid"
                login_user = UserAuthService.check_user(email, pwd)    #향후 비밀번호 처리 필요9999
                if login_user is not None:
                    UserAuthService.setLoginSession(request, login_user)
                    auth_login(request, login_user)

                level = user[0].level
                obj = []
                if level == 0:
                    # 0: 세부입력 페이지 이동
                    language = LanguageInfo.objects.filter()
                    for lang in language:
                        data = {}
                        data['id'] = lang.id
                        data['language1'] = lang.lang_english
                        data['language2'] = lang.lang_local
                        obj.append(data)

                    language = json.dumps(obj)
                    return HttpResponseRedirect('/dashboard/', {'language': language}, RequestContext(request))
                elif level == 2:
                    # 1: 세부입력 완료 관심사 페이지 이동
                    # interests = Interest.objects.filter()
                    # url = INTEREST_IMG_URL
                    # return render_to_response('mypage/my_interest.html', {'interests':interests, 'url':url}, RequestContext(request))
                    language = LanguageInfo.objects.filter()
                    for lang in language:
                        data = {}
                        data['id'] = lang.id
                        data['language1'] = lang.lang_english
                        data['language2'] = lang.lang_local
                        obj.append(data)

                    language = json.dumps(obj)
                    return HttpResponseRedirect('/dashboard/', {'language': language}, RequestContext(request))
                else:
                    print level
                    return HttpResponseRedirect('/dashboard/', RequestContext(request))

            else:
                # 패스워드 틀림
                print "password invalid"
                message = "You entered an invalid email or password. "
                form = UserLoginForm()
                language = LanguageInfo.objects.filter()
                # return render_to_response('user_auth/landingpage.html', {'message':message, 'form':form}, RequestContext(request))
                # return render_to_response('비밀번호 다름 (페이지구현 or 안내말). 로그인 실패')
                return render_to_response('user_auth/login_page.html',
                                          {'message': message, 'form': form, 'languages': language},
                                          RequestContext(request))
        else:
            # 이메일 존재안함.
            print "email invalid"
            message = "You entered an invalid email or password. "
            form = UserLoginForm()
            language = LanguageInfo.objects.filter()
            # return render_to_response('user_auth/landingpage.html', {'message':message, 'form':form}, RequestContext(request))
            # return render_to_response('이메일 없음 (페이지구현 or 안내말 ). 로그인 실패')
            return render_to_response('user_auth/login_page.html',
                                      {'message': message, 'form': form, 'languages': language},
                                      RequestContext(request))

    except Exception as e:
        print '@@@@@mobile_login : ', str(e)
        return HttpResponse("Login error 관리자에게 문의하세요  : tellpinadmin@unichal.com")

        # 로그아웃


def logout(request):
    """
    @summary: 로그아웃 함수
    @author: khyun
    @param none
    @return: url - /
    """
    auth_logout(request)
    return HttpResponseRedirect('/')


# 로그아웃
@csrf_exempt
def ajax_logout(request):
    """
    @summary: ajax 로그아웃 함수
    @author: khyun
    @param none
    @return: json
    """
    auth_logout(request)

    resData = {}
    resData["success"] = True
    jsonData = json.dumps(resData)

    return HttpResponse(jsonData, RequestContext(request))


# 소셜로그인 및 회원가입
def social_login(request):
    """
         삭제예정 함수
    """
    print "social_login"
    # TODO
    # 소셜 로그인 및 회원가입 구현
    # 로그인 및 회원가입은 같은 방식으로 동작
    # 소셜아이디를 통해 로그인 시도 -> 로그인 처리. 단, 최초 로그인 시도시 자동회원가입을 진행한다.
    # 페이스북, 트위터, 구글플러스를 통해 최초 로그인 시도 시, 회원정보에 이메일이 없는 경우 또는 트위터의 경우 처럼 이메일을 불러올 수 없는 경우 이메일을 추가로 요청.
    # 이메일이 중복 되는 경우, 중복되지 않는 이메일을 요청 ( 요청 방식은 시안을 참조할 것 )
    # 회원이 닉네임은 소셜 회원의 이름을 가져와서 자동완성 ( 한글인 경우, 재입력 요청 or 랜덤 닉네임 ), ( 닉네임이 중복되는 경우 재입력 요청 )

    return HttpResponse("this is social login")


############################################이메일 인증#####################################################################
# activation code 재전송.
def retry_activation(request):
    """
    @summary: 인증메일 재전송 함수
    @author: khyun
    @param none
    @return: url - user_auth/retry_activation.html
    """
    print "user_auth/views/retry_activation"
    try:
        # user_profile에서 user정보를 다시 가져온다.
        user = get_object_or_404(TellpinUser, email=request.session['email'])
        # activation code 재전송.
        auth_email(user)
        request.session.clear()
        # 인증메일 재전송 페이지
        return render_to_response('user_auth/retry_activation.html')
    except Exception as e:
        # 인증코드 오류
        return render_to_response('user_auth/activation_error.html')


# 회원가입. 이메일 activation code 인증  . level3
def register_confirm(request, activation_key):
    """
    @summary: 인증메일 확인후 호출되는 함수.
    @author: khyun
    @param none
    @return: url - user_auth/register_confirm.html
    """
    try:
        # 사용자가 이미 로그인한 상태인 경우.
        if request.user.is_authenticated():
            HttpResponseRedirect('/')

        # activation key와 userprofile이 맞는 경우  #UserProfile model 새로 정의함.
        user_profile = get_object_or_404(UserAuth, activation_key=activation_key)
        user = get_object_or_404(TellpinUser, user=user_profile.user_id)

        print "user_profile.key_expired_date"
        print user_profile.key_expired_date
        print "timezone.now()"
        print timezone.now().date()
        # activation key 만료된경우.
        if user_profile.key_expired_date < timezone.now().date():
            # key가 만료된경우 키가 만료된 페이지로 이동
            request.session['email'] = user.email
            return render_to_response('user_auth/confirm_expired.html', RequestContext(request))

        # activation key가 정상이면 is_active=true

        #         request.session['email']=user.email
        #         request.session['level']=user.level
        #         request.session['name']=user.name
        #         request.session['id'] = user.id
        auth_user = TellpinAuthUser.objects.get(email=user.email)
        auth_user.backend = 'django.contrib.auth.backends.ModelBackend'
#         print user
        if auth_user is not None:
            user = user
            user.signup_step = 4
            user.save()
            UserAuthService.setLoginSession(request, auth_user)
            auth_login(request, auth_user)
            # 유저의 is_active를 True로 변경한 후 확인화면으로 이동.
            return render_to_response('user_auth/register_confirm.html')
    except Exception as e:
        print "@@@@@register_confirm = ", str(e)
        return render_to_response('user_auth/activation_error.html')


# 회원가입 성공
def register_success(request):
    """
         삭제예정함수
    """
    # TODO
    # 팝업 화면이 나오도록 수정
    return render_to_response('user_auth/register_success.html')


########################################################################################################################

################################################ 패스워드 리셋 ##############################################################

# reset page
def reset_page(request):
    """
    @summary: password reset 페이지 호출 함수
    @author: khyun
    @param none
    @return: url - user_auth/reset.html
    """
    print 'reset_page'
    form = UserRegisterForm()
    return render_to_response('user_auth/reset.html', {'form': form}, RequestContext(request))


@csrf_exempt
@require_http_methods(["POST"])
# 패스워드다시설정 이메일 전송
def reset_password(request):
    """
    @summary: password 재설정 메일 전송 함수
    @author: khyun
    @param none
    @return: url - user_auth/mail_send.html
    """
    email = request.POST['email']

    user = TellpinUser.objects.filter(email=email)
    if user:
        # user에게 이메일 전송
        user = TellpinUser.objects.get(email=email)
        reset_email(user)
        message = 'ok'
        return render_to_response('user_auth/mail_send.html', {'message': message}, RequestContext(request))
    else:
        message = 'no'
        return render_to_response('user_auth/mail_send.html', {'message': message}, RequestContext(request))


# password change. 사용자에게 패스워드를 입력받아서 처리한다.
def reset_confirm(request, activation_key):
    """
    @summary: password reset 인증메일 클릭후 호출되는 함수. 패스워드 재설정 가능한 화면으로 이동
    @author: khyun
    @param none
    @return: url - user_auth/reset_confirm.html
    """
    try:
        # 사용자가 이미 로그인한 상태인 경우.
        #     if request.user.is_authenticated():
        #         HttpResponseRedirect('/')

        # activation key와 userprofile이 맞는 경우  #UserProfile model 새로 정의함.
        user_profile = get_object_or_404(UserResetPW, activation_key=activation_key)
        user = get_object_or_404(TellpinUser, user_id=user_profile.user_id)
        request.session['email'] = user.email
        # activation key 만료된경우.
        if user_profile.key_expired_date < timezone.now().date():
            # key가 만료된경우 키가 만료된 페이지로 이동
            request.session['email'] = user.email
            return render_to_response('user_auth/confirm_password_expired.html', RequestContext(request))
            # return HttpResponse('키가 만료되었습니다. (임시, 재발급 받을수 있는 화면으로 이동하도록 수정)')

        # activation key가 정상이면 is_active=true
        #     user = user_profile.user
        #     user.mailing = True
        #     user.save()

        print '4'
        # 유저의 is_active를 True로 변경한 후 확인화면으로 이동.
        return render_to_response('user_auth/reset_confirm.html', RequestContext(request))
        print '5'
    except Exception as e:
        print '6'
        print e
        return render_to_response('user_auth/activation_error.html')


# 패스워드 재전송 (인증코드가 만료된경우 해당 페이지에서 클릭)
def retry_password_activation(request):
    """
    @summary: password reset 인증메일이기간이 만료된경우 reset메일 재전송.
    @author: khyun
    @param none
    @return: url - user_auth/retry_password_activation.html
    """
    try:
        # user_profile에서 user정보를 다시 가져온다.
        print 're_act'
        user = get_object_or_404(TellpinUser, email=request.session['email'])
        # activation code 재전송.
        reset_email(user)
        request.session.clear()

        return render_to_response('user_auth/retry_password_activation.html')
    except Exception as e:
        print "@@@@@@@@@@@@retry_password_activation : ", str(e)
        return render_to_response('user_auth/activation_error.html')


# password change_success
def reset_success(request):
    """
    @summary: password reset 시도시 호출되는 함수. 패스워드 변경 성공후 로그인 페이지로 이동.
    @author: khyun
    @param none
    @return: url - user_auth/login_form.html
    """
    print 'reset_success'
    password1 = request.POST['password1']
    password2 = request.POST['password2']

    print password1
    print password2
    # 비밀번호가 동일한지 확인
    if password1 and password2 and password1 != password2:
        # 비밀번호가 다를경우 validationerror 발생 exception으로 ..
        raise forms.ValidationError(
            message="password mismatch",
            code="password mismatch"
        )

    # 비밀번호 문자.숫자.특수문자 체크
    password1 = request.POST['password1']
    print password1
    numcheck = check_number(password1)
    charcheck = check_letter(password1)
#     specialletter = check_specialletter(password1)

    email = request.session["email"]
    user = TellpinUser.objects.get(email=email)

#     numcheck = True
#     charcheck = True
#     specialletter = True
    # password 문자. 숫자. 특수문자 check
    if not numcheck or not charcheck:
        # 비밀번호 저장실패
        message = "8 to 20 characters, case-sensitive, Must contain at least one alpha character [a-z,A-Z] and one numeric character[0-9] case-sensitive, Must contain at least one alpha character [a-z,A-Z] and one numeric character[0-9]."
        language = LanguageInfo.objects.filter()
        form = UserLoginForm()
        return render_to_response('user_auth/landingpage.html',
                                  {'message': message, 'form': form, 'languages': language}, RequestContext(request))

    else:
        # 비밀번호 저장성공
        auth_user = TellpinAuthUser.objects.get(email=email)
        auth_user.set_password(password1)
        auth_user.save()
        user = TellpinUser.objects.get(user=user.user)
#        user.pw = encrypt_password(password1)
        user.pw = auth_user.password
        user.save()
        key_expires = datetime.today() - timedelta(3)
        # 패스워드 변경코드값 기간만료.
        password_session = UserResetPW.objects.get(user_id=user.user)
        password_session.key_expires = key_expires
        password_session.save()

        # return render_to_response('user_auth/reset_success.html', RequestContext(request))
        # 2016-04-29 kihyun update
        form = UserLoginForm()
        language = LanguageInfo.objects.filter()
        return render_to_response("user_auth/login_form.html", {'form': form, 'languages': language},
                                  RequestContext(request))

    ########################################################################################################################


def language_select(request):
    """
         삭제예정함수
    """
    return render_to_response("user_auth/language_select.html", RequestContext(request))


@csrf_exempt
def tellpin_search_login(request):
    """
    @summary: 텔핀써치에서 텔핀닷컴 마이페이지로 이동시 호출되는 함수
    @author: khyun
    @param none
    @return: url - /mypage/
    """
    print "tellpin_search_login"
    # 사용자 정보
    if request.method == 'GET':
        qd = request.GET
    elif request.method == 'POST':
        qd = request.POST

    if qd.has_key('user_id'):
        rec_email = qd['user_id']
    else:
        return HttpResponse("parameter user_id error")

    if qd.has_key('user_pw'):
        # pwd = encrypt_password(qd['user_pw'])
        pwd = qd['user_pw']
    else:
        return HttpResponse("parameter user_id error")

    # email check
    userinfo = TellpinUser.objects.get(email=rec_email)
    auth_user = TellpinAuthUser.objects.get(email=rec_email)
    if userinfo:
        # 아이디 일치
        if UserAuthService.check_password(auth_user, pwd):
            # 패스워드 일치
            print "password valid"
            login_user = UserAuthService.check_user(rec_email, pwd)
            if login_user is not None:
                UserAuthService.setLoginSession(request, login_user)
                auth_login(request, login_user)
        else:
            # 패스워드 틀림
            print "password invalid"
            message = "You entered an invalid email or password. "
            form = UserLoginForm()
            language = LanguageInfo.objects.filter()
            # return render_to_response('user_auth/landingpage.html', {'message':message, 'form':form}, RequestContext(request))
            # return render_to_response('비밀번호 다름 (페이지구현 or 안내말). 로그인 실패')
            return render_to_response('user_auth/landingpage.html',
                                      {'message': message, 'form': form, 'languages': language},
                                      RequestContext(request))
    else:
        # 이메일 존재안함.
        print "email invalid"
        message = "You entered an invalid email or password. "
        form = UserLoginForm()
        language = LanguageInfo.objects.filter()
        # return render_to_response('user_auth/landingpage.html', {'message':message, 'form':form}, RequestContext(request))
        # return render_to_response('이메일 없음 (페이지구현 or 안내말 ). 로그인 실패')

        return render_to_response('user_auth/landingpage.html',
                                  {'message': message, 'form': form, 'languages': language}, RequestContext(request))

    return HttpResponseRedirect("/mypage")


@csrf_exempt
def tellpin_search_login_main(request):
    """
    @summary: 텔핀써치에서 텔핀닷컴 메인으로 이동시 호출되는 함수.
    @author: khyun
    @param none
    @return: url - /dashboard/
    """
    print "tellpin_search_login_main"
    # 사용자 정보
    if request.method == 'GET':
        qd = request.GET
    elif request.method == 'POST':
        qd = request.POST

    if qd.has_key('user_id'):
        rec_email = qd['user_id']
    else:
        return HttpResponse("parameter user_id error")

    if qd.has_key('user_pw'):
        pwd = qd['user_pw']
        # pwd = encrypt_password(qd['user_pw'])
    else:
        return HttpResponse("parameter user_id error")

    # email check
    userinfo = TellpinUser.objects.get(email=rec_email)
    auth_user = TellpinAuthUser.objects.get(email=rec_email)
    if userinfo:
        # 아이디 일치
        if UserAuthService.check_password(auth_user, pwd):
            # 패스워드 일치
            print "password valid"
            login_user = UserAuthService.check_user(rec_email, pwd)
            if login_user is not None:
                UserAuthService.setLoginSession(request, login_user)
                auth_login(request, login_user)
        else:
            # 패스워드 틀림
            print "password invalid"
            message = "You entered an invalid email or password. "
            form = UserLoginForm()
            language = LanguageInfo.objects.filter()
            # return render_to_response('user_auth/landingpage.html', {'message':message, 'form':form}, RequestContext(request))
            # return render_to_response('비밀번호 다름 (페이지구현 or 안내말). 로그인 실패')
            return render_to_response('user_auth/landingpage.html',
                                      {'message': message, 'form': form, 'languages': language},
                                      RequestContext(request))
    else:
        # 이메일 존재안함.
        print "email invalid"
        message = "You entered an invalid email or password. "
        form = UserLoginForm()
        language = LanguageInfo.objects.filter()
        # return render_to_response('user_auth/landingpage.html', {'message':message, 'form':form}, RequestContext(request))
        # return render_to_response('이메일 없음 (페이지구현 or 안내말 ). 로그인 실패')
        return render_to_response('user_auth/landingpage.html',
                                  {'message': message, 'form': form, 'languages': language}, RequestContext(request))

    return HttpResponseRedirect("/dashboard/")


@csrf_exempt
def ajax_get_notification(request):
    """
    @summary: 헤더의 알람 data 호출 함수
    @author: khyun
    @param none
    @return: json
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    
    print 'views.get_notification'
    # get request parameter
    # post_dict = json.loads(request.body)
    # resData["tutor_list"], resData["page_info"] = ts.ajax_listup_tutors(user_id,post_dict)
    try:
        resData["res_data"] = get_notification(user_id)
        jsonData = json.dumps(resData)
    except Exception as e:
        print e

    print jsonData
    return JsonResponse(resData)


@csrf_exempt
def ajax_set_notification_status(request):
    """
    @summary: 헤더의 알람 list 클릭시 읽음으로 처리하는 함수.
    @author: khyun
    @param none
    @return: json
    """
    # resoponse json
    resData = {}
    resData["isSuccess"] = 1

    print request.session['email']
    if request.session.get('email', False):
        # ('email' 라는 key 가 있다면 value 를 return 하고, 아니면 False 를 return 합니다.)
        user_id = TellpinUser.objects.get(email=request.session["email"]).user_id
    else:
        resData["isSuccess"] = 0
        resData['message'] = "no login"
        jsonData = json.dumps(resData)
        return HttpResponse(jsonData, content_type='application/json')

    # get request parameter
    post_dict = json.loads(request.body)
    print post_dict
    # resData["tutor_list"], resData["page_info"] = ts.ajax_listup_tutors(user_id,post_dict)
    try:
        resData["res_data"] = set_notification_status(post_dict['noid'])
        jsonData = json.dumps(resData)
    except Exception as e:
        print "@@@@@@@@@@@@ajax_set_notification_status : ", str(e)

    return JsonResponse(resData)


# 로그인 중복 체크
@csrf_exempt
def ajax_check_email(request):
    """
    @summary: 이메일이 중복되는지 확인하는 함수
    @author: khyun
    @param request
    @return: json
    """
    # TODO
    # 패스워드 체크 (유효성없음, 클라이언트 동시 체크)
    print "ajax_check_email"
    print request  # get request parameter
    post_dict = json.loads(request.body)
    data = post_dict['obj']
    # TODO
    # 일반회원 로그인 구현.
    email = data['email']
    print email
    try:
        # email check
        user = TellpinUser.objects.filter(email=email)
        print user
        # 이메일 회원 존재
        if user:
            resData = {}
            resData["isSuccess"] = False
            resData["message"] = "The email address is already being used"
            jsonData = json.dumps(resData)

            return HttpResponse(jsonData, content_type='application/json')

        else:
            message = "valid"

            resData = {}
            resData["isSuccess"] = True
            resData["message"] = message
            jsonData = json.dumps(resData)
            return HttpResponse(jsonData, content_type='application/json')

    except Exception as e:
        print "@@@@@@@@@@@@ajax_check_email : ", str(e)
        message = "login error, please ask to administrator"
        # return HttpResponse("Login error 관리자에게 문의하세요")
        resData = {}
        resData["isSuccess"] = False
        resData["message"] = message
        jsonData = json.dumps(resData)

        return HttpResponse(jsonData, content_type='application/json')


# 로그인 중복 체크
@csrf_exempt
def ajax_check_password(request):
    """
    @summary: 비밀번호가 맞는지 체크하는 함수
    @author: khyun
    @param none
    @return: json
    """
    # TODO
    # 패스워드 체크 (유효성없음, 클라이언트 동시 체크)
    print "ajax_check_password"
#     print request  # get request parameter
    post_dict = json.loads(request.body)
    data = post_dict['obj']
    # TODO
    # 일반회원 로그인 구현.
    email = request.session["email"]
    pwd = data['pwd']
    print email
    try:
        # email check
        user = TellpinUser.objects.get(email=email)
        new_user = TellpinAuthUser.objects.get(email=email)
        # 이메일 회원 존재
        if user:
            if UserAuthService.check_password(new_user, pwd):
                resData = {}
                resData["isSuccess"] = True
                resData["message"] = "valid"

                return JsonResponse(resData)

        message = "Invalid password"

        resData = {}
        resData["isSuccess"] = False
        resData["message"] = message
        jsonData = json.dumps(resData)

        return JsonResponse(resData)

    except Exception as e:
        print "@@@@@@@@@@@@ajax_check_password : ", str(e)
        message = "login error, please ask to administrator"
        # return HttpResponse("Login error 관리자에게 문의하세요")
        resData = {}
        resData["isSuccess"] = False
        resData["message"] = message

        return JsonResponse(resData)


def ajax_nologin(request):
    """
    @summary: login이 필요한 페이지에서 로그인이 안된경우 호출하는 함수.
    @author: khyun
    @param none
    @return: url - /user_auth/login_form/
    """
    print "ajax_nologin"
    request.session['previous_url'] = request.GET['url']
    return HttpResponseRedirect("/user_auth/login_form/")
