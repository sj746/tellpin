# -*- coding: utf-8 -*-
###############################################################################
# filename    : user_auth > tellpin_social.py
# description : 소셜 로그인 및 회원가입 시 동작하는 callback methods 를 모아 놓은 모듈. 
#               auth_config.py 의 pipeline 에 등록하여 사용
# author      : chsin@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20161111 chsin 최조 작성
#
###############################################################################


def social_pipeline(strategy, details, user=None, *args, **kwargs):
    """
    @summary 소셜 로그인 및 회원가입 시 동작
    @param strategy: pipeline 을 따라 넘어오는 객체
    @param details: oAuth2 인증 정보
    @param user: 인증할 사용자 객체
    @param args: 비어 있음
    @param kargs: 기타 정보
    @return {isNew: 새로 생성 여부(True, False), user: 인증한 사용자 객체}
    """
    print "@@@@@@@@@@@@ social_pipeline"
    print(details)
    print(args)
    print(kwargs)

    if user:
        return {'is_new': False}

    email = details.get('email')
    username = details.get('username')
    fullname = details.get('fullname')
    uid = kwargs.get('uid')

    if not email:
        if username:
            email = username

#    email = email + "###" + str(uid);

    fields = {'email': email}

    if not fields:
        return

    return {
        'is_new': True,
        'user': strategy.create_user(**fields)
    }