# -*- coding: utf-8 -*-
###############################################################################
# filename    : user_auth > models.py
# description : System 모델 정의
# author      : hsrjmk@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160905 hsrjmk 최조 작성
#
###############################################################################
from django.conf import settings
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)


FILE_TYPES = (
    ('LB-P', 'Language Board Post'),
    ('TP-Q', 'Translation and Proofreading Question'),
    ('TP-A', 'Translation and Proofreading Answer'),
    ('LQ-Q', 'Language Questions Question'),
    ('LQ-A', 'Language Questions Answer'),
    ('FAQ', 'Help FAQ'),
    ('QNA_Q', 'Help Question'),
    ('QNA_A', 'Help Answer'),
    ('QNA_R', 'Help Reply'),
    ('PROFILE', 'Profile'),
)


class TellpinAuthUserManager(BaseUserManager):
    def create_user(self, email, password=None, username=None, fullname=None):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            username=fullname
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(email,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class TellpinAuthUser(AbstractBaseUser):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    username = models.CharField('User name', max_length=255, default='', null=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = TellpinAuthUserManager()

    USERNAME_FIELD = 'email'

    def get_full_name(self):
        # The user is identified by their email address
        return self.username

    def get_short_name(self):
        # The user is identified by their email address
        return self.username

    def __unicode__(self):  # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    class Meta:
        db_table = u'auth_user'


class TellpinUser(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, primary_key=True)
    email = models.EmailField("User email")
    pw = models.CharField("Account password", max_length=100)
    name = models.CharField("User full name", max_length=100, null=True)

    signup_step = models.IntegerField("Step after sign up", default=0)
    interest_teaching = models.BooleanField("Check user interested in teaching", default=False)
    interest_learning = models.BooleanField("Check user interested in learning", default=False)

    is_tutor = models.BooleanField("Check user is tutor", default=False)

    facebook_uid = models.CharField("Facebook uid", max_length=100, null=True)
    twitter_uid = models.CharField("Twitter uid", max_length=100, null=True)
    google_uid = models.CharField("Google uid", max_length=100, null=True)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    del_by_user = models.BooleanField("Deleted by user", default=False)
    del_by_admin = models.BooleanField("Deleted by administrator", default=False)

    class Meta:
        db_table = u'user'


class UserLoginLog(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    ip = models.GenericIPAddressField("User ip address")
    login_time = models.DateTimeField("User login time")

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'user_login_log'


class UserResetPW(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    activation_key = models.CharField("Activation key for reset password", max_length=40)
    key_expired_date = models.DateField("Key expired date")

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'user_reset_pw'


class UserAuth(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    activation_key = models.CharField("Activation key for user authentication", max_length=40)
    key_expired_date = models.DateField("Key expired date")

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'user_auth'


class File(models.Model):
    fid = models.AutoField("File id", primary_key=True)
    ref_id = models.IntegerField("Reference model id", null=True)
    type = models.CharField("File usage type", max_length=10, choices=FILE_TYPES)
    filename = models.CharField("Filename", max_length=255)
    filepath = models.FilePathField("File path", max_length=255)

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'file'
