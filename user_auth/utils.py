# -*- encoding: utf-8 -*-
###############################################################################
# filename    : exchange > views.py
# description : exchange views ( language exchange )
# author      : khyun@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 khyun 최초 작성
#
#
###############################################################################
'''
Created on 2015. 5. 15.

@author: kihyun 
'''
# from common.models import LcPost, LcComment, TpQuestion, TpComment, \
#     LqQuestion, LqComment, LqAnswer, TpAnswer
#

from datetime import datetime as datetime_2, timedelta
import datetime
from email.mime.text import MIMEText
import hashlib
import logging
import random

from django.core.mail import send_mail, send_mass_mail, BadHeaderError
from django.core.mail.message import EmailMessage
from django.db import connection
from django.db.models.query_utils import Q
from django.utils import timezone

from community.models import LB, LBComment, TP, TPComment, TPAnswer, LQ, \
    LQComment, LQAnswer
from dashboard.models import Notification, NotificationText
from tellpin.settings import TELLPIN_HOME
from tutor.models import Tutee
from tutoring.models import LessonHistory, LessonHistoryText, LessonReservation, \
    Lesson
from user_auth.models import TellpinUser
from user_auth.models import UserAuth, UserResetPW
import user_auth.smtplib
from wallet.models import Giftcard


logger = logging.getLogger('user_auth')
# # from user_auth.models import UserProfile
# from common.models import UserProfile, ResetPwd, TellpinUser, Timezone
# import string, re
# from common.models import Notification, HistoryInfo, Lesson, Reservation, ClubInfo, TransInfo, QaInfo, ExchangeInfo, \
#     Friend, \
#     Message, MessageInfo, RV_Message, GiftcardIssue
#
# from common.models import UserLanguage, SearchCategory, MyCategory, \
#     RecommendedSite, SearchSite, MySite, SkillLevel, Searchwordlog, MyPin1, \
#     MyPin2, MyPin3, MyPin, DefaultCategory, DefaultSite, MyStatus


# email 전송
def auth_email(user):
    """
    @summary: email 인증 확인 이메일을 전송하는 함수.
    @author: khyun
    @param user : user object
    @return: none
    """
    # activation code 생성
    salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
    activation_key = hashlib.sha1(salt + user.email).hexdigest()
    key_expires = datetime.datetime.today() + datetime.timedelta(2)

    # email setting
    url_location = "user_auth/register_confirm/"
    url_help_center = "help/"
    subject = '(Tellpin) Please check email.'

    message = '' \
              ' <table width="100%%" style="height: 320px; font-size:16px !important;" align="center">' \
              '    <tr style=\"background-color: #ECECEC;height: 300px;width:100%%; font-size:16px !important;\"> ' \
              '        <td> ' \
              '            <table width="770" align="center" style=\"height:300px;\"> ' \
              '            <tr style=\"background-color: white; height: 300px;\"> ' \
              '                <td style=\"padding: 30px 50px; font-size:16px !important;\"> ' \
              '                    <p style=\"border-bottom: 1px solid #ECECEC; padding-bottom: 20px;\"><a href=\"%s\"><img width=\"100\"  src=\"%sstatic/img/mainpage/tellpin_header_logo_title_beta.png\"></a></p>' \
              '                    <p style=\"padding-top: 20px;font-weight: 600;\"><span> [Welcome to Tellpin] Please verify your email address. </span></p> ' \
              '                    <p style=\"padding-top: 20px;\"><span> Dear %s ! </span></p> ' \
              '                    <p><span> Thank you for registering on Tellpin.com </span></p> ' \
              '                    <p><span> To finish setting up Tellpin Account, Please click the following link to verify your email address. </span></p> ' \
              '                    <p style=\"text-align: center;margin:5%% 0;\"><a  href=\"%s\" style=\"color:blue; text-decoration:underline; ' \
              '                    line-height:34px; border-radius:8px; margin: auto; \">Verify your email address</a></p> ' \
              '                    <p></p> ' \
              '                    <p><span> If the link doesn\'t work, Please follow URL below: </span></p> ' \
              '                    <p><span>%s</span></p> ' \
              '                    <p><span> For more information, visit Tellpin <a  href=\"%s\" style=\"color:blue; text-decoration:underline; ' \
              '                    line-height:34px; border-radius:8px; margin: auto; \">Help Center</a> </span></p> ' \
              '                    <p><span> Thanks. </span></p> ' \
              '                    <p><span> Tellpin Team </span></p> ' \
              '                </td> ' \
              '            </tr> ' \
              '            <tr style=\"height: 1%%\"> ' \
              '                <td> ' \
              '                </td> ' \
              '            </tr> ' \
              '            </table> ' \
              '        </td>' \
              '    </tr>' \
              '</table>' % (TELLPIN_HOME, TELLPIN_HOME, str(user.name), TELLPIN_HOME + url_location + activation_key,
                            TELLPIN_HOME + url_location + activation_key, TELLPIN_HOME + url_help_center)
    # '</table>' % ( TELLPIN_HOME, str(user.name), TELLPIN_HOME+url_location+activation_key, TELLPIN_HOME, TELLPIN_HOME+url_location+activation_key, TELLPIN_HOME, TELLPIN_HOME, TELLPIN_HOME, TELLPIN_HOME, TELLPIN_HOME )

    logger.debug('send_email()->' + user.email)
    from_email = 'info'
    if subject and message and from_email:
        try:
            # send_mail(subject, '', 'tellpinadmin@unichal.com', [user.email], html_message=message)#userprofile db update
            msg = EmailMessage(subject, message, 'tellpinadmin@unichal.com', [user.email])
            print user.email
            msg.content_subtype = 'html'
            msg.send()
            save_userProfile(user, activation_key, key_expires)

        except BadHeaderError as e:
            print '@@@@@auth_email : ', str(e)
            e.message = 'send_email error'
            logger.debug('send_email error')
            # return HttpResponse('Invalid header found.')
            # return HttpResponseRedirect('/user_auth/register.html/', )
    else:
        # In reality we'd use a form class
        # to get proper validation errors.
        logger.debug('send_email error')
        # return HttpResponse('Make sure all fields are entered and valid.')


# email 전송. password reset
def reset_email(user):
    """
    @summary: password reset 이메일을 전송하는 함수.
    @author: khyun
    @param user : user object
    @return: none
    """
    print 'reset_email()'
    # activation code 생성
    salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
    activation_key = hashlib.sha1(salt + user.email).hexdigest()
    key_expired_date = datetime.datetime.today() + datetime.timedelta(2)

    # email setting
    url_location = "user_auth/reset_confirm/"
    subject = 'Reset your Tellpin Password'
    print user.name + ', ' + user.email + ', ' + url_location + ', ' + activation_key
    #     message = 'Hi %s.\n\n ' \
    #                'Please follow the link below to change your password.The link will remain valid for 48hours \n\n' \
    #                '<a href="%s/%s">click here</a> \n\n' % ( user.name, url_location, activation_key )


    #     message = ''\
    #     '<div style=\"width: 500px; height: 300px; margin:auto; \"> '  \
    #     '<div style=\"width:450px; height: 250px; margin:auto; padding:20px; background-color: #f5f5f5\"> ' \
    #         '<div style=\"width: 450px; margin: auto; height: 200px; background-color: white; display:table-cell; vertical-align:middle;\"> ' \
    #             '<div style=\"text-align:center;vertical-align: middle;font-size:30px;color:#555555\"> ' \
    #                 '<span style=\"display:block;\">안녕하세요.</span> ' \
    #             '</div> ' \
    #             '<div style=\"text-align:center;vertical-align: middle;font-size:15px;line-height:30px;color:#999999;\"> ' \
    #                 '<span style=\"display:block;\">비밀번호는 암호화 되어있어 임시 비밀번호를 전달드립니다.</span> ' \
    #                 '<span style=\"display:block;\">하단의 E-mail인증 버튼을 눌러 비밀번호를 변경해 주세요.</span> ' \
    #                 '<a style=\"width:207px;height:45px;\" href=\"%s/%s\"><img alt=\"확인\" src=\"/static/user_auth/img/btn_confirm.png\"></a> ' \
    #             '</div> ' \
    #         '</div> ' \
    #         '<div style=\"text-align: center;width:450px;\"> ' \
    #                 '<span style=\"display:block;color:#999999;\"> Copyrightⓒ유니챌(주) All Rights Reserved. </span> ' \
    #         '</div> ' \
    #     '</div> ' \
    #     '</div> '   % ( url_location, activation_key )



    #     '</body>' \
    #     '</x-meta>' % ( UNICHAL_HOME, url_location, activation_key, UNICHAL_HOME, 'static/user_auth/img/btn_pwd.png')
    #
    # print message.decode('utf-8').encode('utf-8')
    # message = message.decode('utf-8').encode('utf-8')

    logger.debug('send_email()->' + user.email)
    # from_email = 'info@unichal.com'
    from_email = 'info'

    message = '' \
              '<table style="width:100%%; height: 300px">' \
              '    <tr style=\"background-color: #ECECEC;height: 270px; width: 100%%;\"> ' \
              '        <td> ' \
              '            <table align="center" style=\"width: 770px;height:270px;\"> ' \
              '            <tr style=\"background-color: white; height: 270px;\"> ' \
              '            <td style=\"padding: 0 50px; font-size:16px !important;\">' \
              '                    <p style=\"border-bottom: 1px solid #ECECEC; padding-bottom: 20px;\"><a href=\"%s\"><img width=\"100\"  src=\"%sstatic/img/mainpage/tellpin_header_logo_title_beta.png\"></a></p>' \
              '                    <p style=\"padding-top:20px;\"><span> Please click the button below to reset your password. </span></p> ' \
              '                    <p style=\"text-align: center;margin:5%% 0;\"><a href=\"%s\" style=\" text-decoration:none; color: blue;line-height:34px; border-radius:8px;margin:auto;\">Reset Password</a></p> ' \
              '                    <p></p> ' \
              '            </td>' \
              '            </tr> ' \
              '            <tr style=\"height: 1%%\"> ' \
              '                <td> ' \
              '                </td> ' \
              '            </tr> ' \
              '            </table> ' \
              '        </td>' \
              '    </tr>' \
              '</table>' % (TELLPIN_HOME, TELLPIN_HOME, TELLPIN_HOME + url_location + activation_key)
    # '</table>' % ( TELLPIN_HOME, str(user.name), TELLPIN_HOME+url_location+activation_key, TELLPIN_HOME, TELLPIN_HOME, TELLPIN_HOME, TELLPIN_HOME, TELLPIN_HOME, TELLPIN_HOME )

    print message
    if subject and message and from_email:
        try:
            # userprofile 에서 reset email로 수정....
            # py_send_email(subject, message, user.email)
            # send_mail(subject, '', 'tellpinadmin@unichal.com' , [user.email], html_message=message )#userprofile db update
            msg = EmailMessage(subject, message, 'tellpinadmin@unichal.com', [user.email])
            msg.content_subtype = 'html'
            msg.send()
            save_pwd(user, activation_key, key_expired_date)
        except BadHeaderError as e:
            print '@@@@@reset_email : ', str(e)
            e.message = 'send_email error'
            logger.debug('send_email error')
            # return HttpResponse('Invalid header found.')
            # return HttpResponseRedirect('/user_auth/register.html/', )
    else:
        # In reality we'd use a form class
        # to get proper validation errors.
        logger.debug('send_email error')
        # return HttpResponse('Make sure all fields are entered and valid.')


# password_session activation코드 저장
def save_pwd(user, activation_key, key_expired_date):
    """
    @summary: password 변경 이메일 전송시 activation key값과 key_expires date(유효기간 만료일)을 저장하는 함수.
    @author: khyun
    @param activation_key : activation key value
    @param key_expires : key expire date ( 키값 만료일 )
    @return: none
    """
    """
    new_profile = UserProfile(user=user, activation_key=activation_key, 
    key_expires=key_expires)
    new_profile.save()
    """
    try:
        print('1')
        update_profile = UserResetPW.objects.get(user_id=user.user_id)
        print('2')
        update_profile.activation_key = activation_key
        update_profile.key_expired_date = key_expired_date
        update_profile.save()
    except UserResetPW.DoesNotExist as e:
        print '@@@@@save_pwd : ', str(e)
        new_profile = UserResetPW(user_id=user.user_id, activation_key=activation_key, key_expired_date=key_expired_date)
        new_profile.save()



        # userprofile activation코드 저장


def save_userProfile(user, activation_key, key_expires):
    """
    @summary: 인증 이메일의 activation key값과  만료일을 저장하는 함수.
    @author: khyun
    @param activation_key : activation key value
    @param key_expires : key expire date ( 키값 만료일 )
    @return: none
    """
    """
    new_profile = UserProfile(user=user, activation_key=activation_key, 
    key_expires=key_expires)
    new_profile.save()
    """
    print "save_userProfile"

    try:
        update_profile = UserAuth.objects.get(user_id=user.pk)
        update_profile.activation_key = activation_key
        update_profile.key_expired_date = key_expires
        update_profile.save()
    except UserAuth.DoesNotExist:
        new_profile = UserAuth(user_id=user.pk, activation_key=activation_key, key_expired_date=key_expires)
        new_profile.save()


# 문자포함체크
def check_letter(s):
    """
    @summary: 문자가 포함되어 있는지 체크하는 함수
    @author: khyun
    @param s : string
    @return: none
    """
    characters = list(s)
    logger.debug('check_letter')
    for char in s:
        if char in characters:
            return True
    return False


# 숫자포함체크
def check_number(s):
    """
    @summary: 숫자가 포함되어 있는지 체크하는 함수
    @author: khyun
    @param s : string
    @return: none
    """
    numbers = [str(i) for i in range(10)]
    logger.debug('check_number')
    for char in s:
        if char in numbers:
            return True
    return False


# 특수문자포함체크
def check_specialletter(s):
    """
    @summary: 특수문자가 포함되어 있는지 체크하는 함수
    @author: khyun
    @param s : string
    @return: none
    """
    invalidChars = set(s.punctuation.replace("_", ""))
    logger.debug('check_specialletter')
    if any(char in invalidChars for char in s):
        return True
    else:
        return False


def get_notification(user_id):
    """
    @summary: 알림내용을 가져오는 함수 
    @author: khyun
    @param user_id : user id
    @return: object data
    """
    print 'utils.get_notification()---------------'
    # from community.service import CommunityService
    # cs = CommunityService(object)
    # cs.set_notification_club(user_id, 1)
    notification = Notification.objects.filter(receiver_id=user_id).order_by('-created_time')
    print len(notification)

    obj = {}
    lesson_list = []
    community_list = []
    club_list = []
    trans_list = []
    qa_list = []
    ex_list = []
    msg_list = []
    gift_list = []
    obj["alarm_state"] = 0
    obj["lesson_state"] = 0
    obj["community_state"] = 0

    try:
        for noti in notification:

            obj2 = {}
            obj2["noti_id"] = noti.id
            obj2["from_user"] = noti.sender_id
            from_user = Tutee.objects.select_related('user').get(user_id=noti.sender_id)
            obj2["from_user_name"] = from_user.name
            if from_user.photo_filename:
                obj2["from_user_photo"] = "/static/img/upload/profile/" + from_user.photo_filename
            else:    
                obj2["from_user_photo"] = ""
            if noti.type == 7:
                obj2["from_user_photo"] = "/static/img/tellpin_logo_alarm.png"
            obj2["to_user"] = noti.receiver_id
            obj2["to_user_name"] = TellpinUser.objects.get(user_id=noti.receiver_id).name
            obj2["type"] = noti.type
            if noti.type == 1:
                obj2["type_name"] = 'lesson'
                obj2["type_msg_text"] = LessonHistoryText.objects.get(id=noti.noti_text).history_english
                print "noti ref id"
                print noti.ref_id
                rv = LessonReservation.objects.select_related('lesson').get(id=noti.ref_id)
                ls = Lesson.objects.select_related('course').get(id=rv.lesson_id)
                obj2["lesson_name"] = ls.course.course_name
                obj2["from_time"] = str(rv.rv_time)[9:11] + ":" + str(rv.rv_time)[11:13]
                _time = datetime_2.strptime(obj2['from_time'], "%H:%M")
                if rv.lesson.pkg_times == 1:
                    duration = rv.lesson.sg_min
                else:
                    duration = rv.lesson.pkg_min
                _delta = timedelta(minutes=duration)
                obj2["to_time"] = str((_time + _delta).time())[:5]
                _datetime = datetime_2(int(rv.rv_time[:4]), int(rv.rv_time[4:6]), int(rv.rv_time[6:8]),
                                       int(rv.rv_time[9:11]), int(rv.rv_time[11:13]))
                # _timezone = timezone( int(rv.rv_time[:4]), int(rv.rv_time[4:6]), int(rv.rv_time[6:8]), int(rv.rv_time[9:11]), int(rv.rv_time[11:13]) )
                obj2["from_date_time"] = str(_datetime)
            elif noti.type == 2:
                obj2["type_name"] = 'club'
                obj2["type_msg_text"] = NotificationText.objects.get(id=noti.noti_text).notification #"club text" #ClubInfo.objects.get(id=noti.type_msg).comment
            elif noti.type == 3:
                obj2["type_name"] = 'trans'
                obj2["type_msg_text"] = NotificationText.objects.get(id=noti.noti_text).notification #"trans text" #TransInfo.objects.get(id=noti.type_msg).comment
            elif noti.type == 4:
                obj2["type_name"] = 'qa'
                obj2["type_msg_text"] = NotificationText.objects.get(id=noti.noti_text).notification #"qa text" #QaInfo.objects.get(id=noti.type_msg).comment
            elif noti.type == 5:
                obj2["type_name"] = 'ex'
                obj2["type_msg_text"] = NotificationText.objects.get(id=noti.noti_text).notification #"ex text" #ExchangeInfo.objects.get(id=noti.type_msg).comment
            #             elif noti.type == 6:
            #                 obj2["type_name"] = 'msg'
            #                 obj2["type_msg_text"] = MessageInfo.objects.get(id=noti.type_msg).comment + ' ' + obj2["from_user_name"]
            elif noti.type == 7:
                obj2["type_name"] = 'gift'
                #                 print noti.ntid
                #                 print GiftcardIssue.objects.get(issue_id=noti.ntid)
                obj2["type_msg_text"] = str(Giftcard.objects.get(
                    id=noti.ref_id).tc) + 'TC worth of gift card is successfully sent to ' + str(
                    Giftcard.objects.select_related('user').get(id=noti.ref_id).user.email)
            obj2["ntid"] = noti.ref_id
            obj2["type_msg"] = noti.noti_text
            obj2["msg_type"] = noti.type
            obj2["msg_id"] = noti.type

            # print noti.id, noti.ntid, noti.from_user, noti.to_user
            # club
#             print '-----------------'
#             print noti.community_type
#             print noti.type
#             print '-----------------'
            if noti.type == 2:
                # post
                if noti.community_type == 1:
                    obj2["msg_text"] = LB.objects.get(id=noti.ref_id).title
                # comment
                elif noti.community_type == 2 or noti.community_type == 3:
                    obj2["msg_text"] = LBComment.objects.get(id=noti.ref_id).contents
#                     print '-------msg text----------'
#                     print obj2["msg_text"]
            # trans
            elif noti.type == 3:
                if noti.community_type == 1:
                    obj2["msg_text"] = TP.objects.get(id=noti.ref_id).title
                elif noti.community_type == 2 or noti.community_type == 3:
                    obj2["msg_text"] = TPComment.objects.get(id=noti.ref_id).contents
                elif noti.community_type == 5:
                    obj2["msg_text"] = TPAnswer.objects.get(id=noti.ref_id).contents
            # qa
            elif noti.type == 4:
                if noti.community_type == 1:
                    obj2["msg_text"] = LQ.objects.get(id=noti.ref_id).title
                elif noti.community_type == 2 or noti.community_type == 3:
                    obj2["msg_text"] = LQComment.objects.get(id=noti.ref_id).contents
                elif noti.community_type == 5:
                    obj2["msg_text"] = LQAnswer.objects.get(id=noti.ref_id).contents
            # ex
            elif noti.type == 5:
                if noti.community_type == 1:
                    obj2["msg_text"] = "ex" #Friend.objects.get(fr_id=noti.msg_id).message
                if noti.community_type == 2:
                    obj2["msg_text"] = "ex"  # Friend.objects.get(fr_id = noti.msg_id).message
                    # message
                #             elif noti.type == 6:
                #                 if noti.msg_type == 1:
                #                     #community
                #                     obj2["msg_text"] = Message.objects.get(mid = noti.msg_id).message
                #                     print obj2["msg_text"]
                #                 if noti.msg_type == 2:
                #                     obj2["msg_text"] = RV_Message.objects.get(mid = noti.msg_id).message
                #                     print obj2["msg_text"]
            # gift
            elif noti.type == 7:
                obj2["msg_text"] = 'The code in your gift card was ' + Giftcard.objects.get(
                    issue_id=noti.ref_id).serial[0:4] + '-' + Giftcard.objects.get(id=noti.ref_id).gift_code[
                                                            4:8] + '-' + Giftcard.objects.get(
                    id=noti.ref_id).gift_code[8:12] + '-' + Giftcard.objects.get(id=noti.ref_id).gift_code[12:16]

            obj2["redirect_url"] = noti.redirect_url
            obj2["state"] = noti.is_read
            if noti.is_read == 0 and noti.type == 1:
                print noti.id, noti.is_read, noti.type, 'first'
                obj["lesson_state"] = 1
                obj["alarm_state"] = 1
            #             elif noti.state == 0 and noti.type == 6 and noti.msg_type == 2:
            #                 print noti.id, noti.state, noti.type, 'second'
            #                 obj["lesson_state"] = 1
            #                 obj["alarm_state"] = 1
            elif noti.is_read == 0 and noti.type != 1 and noti.type != 6:
                print noti.id, noti.is_read, noti.type, 'third'
                obj["community_state"] = 1
                obj["alarm_state"] = 1

            obj2["created"] = str(noti.created_time)
            if noti.type == 1:
                lesson_list.append(obj2)
                print 'lesson_list:'
                #                 print lesson_list
                print 'lesson_list end'
            elif noti.type == 2:
                club_list.append(obj2)
                community_list.append(obj2)
            elif noti.type == 3:
                trans_list.append(obj2)
                community_list.append(obj2)
            elif noti.type == 4:
                qa_list.append(obj2)
                community_list.append(obj2)
            elif noti.type == 5:
                ex_list.append(obj2)
                community_list.append(obj2)
            #             elif noti.type == 6 and noti.msg_type == 1:
            #                 msg_list.append(obj2)
            #                 community_list.append(obj2)
            #             elif noti.type == 6 and noti.msg_type == 2:
            #                 msg_list.append(obj2)
            #                 lesson_list.append(obj2)
            elif noti.type == 7:
                gift_list.append(obj2)
                community_list.append(obj2)
    except Exception as e:
        print 'get notification error --- --- ---'
        print e

    obj["lesson_list"] = lesson_list
    obj["club_list"] = club_list
    obj["trans_list"] = trans_list
    obj["community_list"] = community_list
    obj["qa_list"] = qa_list
    obj["gift_list"] = gift_list

    #     print obj["lesson_state"]
    #     print obj["alarm_state"]
    return obj


def set_notification_status(noti_id):
    """
    @summary: 읽은 알림의 상태를 '읽음'으로 변경하는 함수
    @author: khyun
    @param noti_id : 해당 알림 id
    @return: Boolean
    """
    print noti_id
    try:
        notification = Notification.objects.get(id=noti_id)
        notification.is_read = 1
        notification.save()

    except Exception as e:
        print e
        return False

    return True


def encrypt_password(password):
    """
    @summary: 유저 패스워드를 암호화 하는 함수
    @author: khyun
    @param password : user password
    @return: hashed password data
    """
    # print password, hashlib.sha256(password).hexdigest()
    return hashlib.sha256(password).hexdigest()
    # return password


def get_social_user_from_request(request):
    """
    @summary request 에서 현재 인증한 소셜 유저 객체를 추출하는 함수
    @author: chsin
    @param request
    @return social user object
    """
    
    if str(request.user) == 'AnonymousUser' or request.user.social_auth is None:
        return None

    social_users = request.user.social_auth.all()
    provider = request.session['social_auth_last_login_backend']

    if len(social_users) > 1:
        for user in social_users:
            if provider == user.provider:
                social_user = user
                break
    else:
        social_user = social_users[0]

    return social_user

