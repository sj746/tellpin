from django.conf.urls import url
import views

urlpatterns = [
               url(r'^$', views.dashboard),
               url(r'ajax_dashboard/$', views.ajax_dashboard),
               url(r'^get_suggest_tutor/$', views.get_suggest_tutor),
               url(r'^get_suggest_le/$', views.get_suggest_le),
               url(r'^messages/$', views.getMessage),
               url(r'^message/user/$', views.getUserMessage),
               url(r'^message/user/list/$', views.getMessageUserList),
               url(r'^message/send/$', views.sendMessage),
               url(r'^message/delete/$', views.deleteMessage),
               url(r'^message/check/$', views.checkMessage),
               url(r'^message/check_new/$', views.checkNewMessage)
               ]