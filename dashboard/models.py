# -*- coding: utf-8 -*-
###############################################################################
# filename    : dashboard > models.py
# description : Message 모델 정의
# author      : hsrjmk@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160922 hsrjmk 최조 작성
#
###############################################################################
from django.db import models
from user_auth.models import TellpinUser


NOTIFICATION_TYPE = (
    (1, 'Lesson'),
    (2, 'Language Board'),
    (3, 'Translation and Proofreading'),
    (4, 'Language Question'),
    (5, 'Language Exchange'),
    (6, 'Message'),
    (7, 'Gift'),
)
COMMUNITY_TYPE = (
    (0, None),
    (1, 'Post'),
    (2, 'Answer'),
    (3, 'Comment'),
    (4, 'Reply to comment'),
    (5, 'Upvote'),
)

class Message(models.Model):
    sender = models.ForeignKey(TellpinUser, related_name='+')
    receiver = models.ForeignKey(TellpinUser, related_name='+')
    message = models.CharField("Message", max_length=1000, null=True)
    is_read = models.BooleanField("Whether message read", default=False)
    del_by_sender = models.BooleanField("Whether message deleted by sender", default=False)
    del_by_receiver = models.BooleanField("Whether message deleted by receiver", default=False)

    created_time = models.DateTimeField("Created time", auto_now_add=True)
    updated_time = models.DateTimeField("Updated time", auto_now=True)

    class Meta:
        db_table = u'message'


class Notification(models.Model):
    sender = models.ForeignKey(TellpinUser, related_name='+')
    receiver = models.ForeignKey(TellpinUser, related_name='+')
    type = models.PositiveSmallIntegerField("Notification type", choices=NOTIFICATION_TYPE, default=1, null=True)
    community_type = models.PositiveSmallIntegerField("Community type", choices=COMMUNITY_TYPE, default=1, null=True)
    noti_text = models.IntegerField(default=1, null=True, db_column='noti_text_id')
    
    redirect_url = models.CharField('Redirect URL', max_length=1000, null=True)
    ref_id = models.IntegerField("Reference id: Lesson id, LB id, TP id, LQ id, Message id", null=True)
    is_read = models.BooleanField("Whether message read", default=False)

    created_time = models.DateTimeField("Created time", auto_now_add=True)

    class Meta:
        db_table = u'notification'


class NotificationText(models.Model):
    notification = models.CharField("Message", max_length=200, null=True)

    class Meta:
        db_table = u'notification_text'
