# -*- coding: utf-8 -*-

###############################################################################
# filename    : dashboard > service.py
# description : dashboard 화면을 구성하는데 필요한 정보를 얻기 위한 view file
# author      : sj746@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 j746 최초 작성
#
#
###############################################################################

import json

from django.contrib.auth.decorators import login_required
from django.core.serializers.json import DjangoJSONEncoder
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render, render_to_response
from django.template.context import RequestContext
from django.views.decorators.csrf import csrf_exempt

from common.common import tellpin_login_required
from common.models import LanguageInfo
from common.service import CommonService
from community.service import Community
from dashboard.service import DashboardService
from mypage.service import MyPageService
from people.service import PeopleService
from support.service import SupportService
from tutor.service import TutorService
from tutor.service4registration import TutorService4Registration
from wallet.service import WalletService


ms = MyPageService(object)
ts4r = TutorService4Registration(object)


# Create your views here.
@tellpin_login_required
def dashboard(request):
    """
    @summary: dashboard 화면 진입을 위한 url
    @author: sj746
    @param request
    @return: url : dashboard/dashboard.html
    """
    resData = dict()
    resData["isSuccess"] = 1
    language = CommonService.language_list()
    resData['language'] = json.dumps(language)

    return render(request, "dashboard/dashboard.html", resData)


@csrf_exempt
@tellpin_login_required
def ajax_dashboard(request):
    """
    @summary: dashboard 처음 진입 시 많은 시간이 소요되므로 html 파일만 로드한 후 데이터 구성은 나중에 해주는 함수
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    print "ajax_dashboard()"
    resData = {}
    resData['isSuccess'] = 1
    user_id = request.user.id
    isTutor = TutorService.is_tutor(user_id)

    if "ml" in request.session.keys():
        ml = request.session.get("ml")
        # TODO: 다국어에 맞는 Language id로 변환
        if ml == 2:
            lang_id = 1
        else:
            lang_id = 2
    else:
        lang_id = 2

    try:
        # 기본 정보
        cur_page = 1
        if isTutor:
            user_utc = request.session.get('utc_delta', 0)
            tutor_detail = TutorService.get_tutor_detail(user_id, user_id, user_utc)
            resData["profile"] = tutor_detail['profile']
            resData["profile"]["is_tutor"] = 1
            resData["profile"]["tuteeTC"] = WalletService.get_tutee_available_balance(user_id)
        else:
            user_detail = TutorService.get_user_detail(user_id, isTutor)
            resData["profile"] = user_detail["profile"]
            resData["profile"]["is_tutor"] = 0
            resData["profile"]["tuteeTC"] = WalletService.get_tutee_available_balance(user_id)

        resData["reservationInfo"] = DashboardService.get_reservation_info(user_id, isTutor)
        resData["myFriends"] = PeopleService.getFriends(user_id)
        resData["myTutors"] = PeopleService.getTutorList(user_id)
        resData["myTutees"] = PeopleService.getTutees(user_id)
        resData["LQs"], resData["LQShowMore"] = DashboardService.get_language_question(cur_page, None, 8)
        resData["TPs"], resData["TPShowMore"] = DashboardService.get_translation_proofreading(cur_page, None, 8)
        resData["LBs"], resData["LBShowMore"] = DashboardService.get_language_board(cur_page, None, 8)
        resData["all"], resData["allShowMore"] = DashboardService.get_all_community(cur_page, None, 8)
        resData["faqs"] = SupportService.get_top_questions(lang_id)
        resData["nationsInfo"] = CommonService.country_list()
        resData["languageInfo"] = CommonService.language_list()
#         resData["skillLevelInfo"] = CommonService.skill_level_list()

        return JsonResponse(resData)
    except Exception as e:
        print 'ajax_dashboard error', str(e)

        return JsonResponse(e)

@csrf_exempt
@tellpin_login_required
def get_suggest_tutor(request):
    """
    @summary: 로그인한 사용자의 정보 중 learning 언어에 해당하는 언어를 가르치는 튜터를 추천해주는 함수
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)
    resData["suggestTutor"], resData["suggestTutorStart"] = DashboardService.get_suggest_tutor(user_id,
                                                                                 post_dict["learnings"],
                                                                                 post_dict["start"])

    return JsonResponse(resData)


@csrf_exempt
@tellpin_login_required
def get_suggest_le(request):
    """
    @summary: 로그인한 사용자의 정보 중 learning 언어에 해당하는 언어가 native 인 사용자를 추천해주는 함수
    @author: sj746
    @param request
    @return: JsonResponse(resData)
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
    post_dict = json.loads(request.body)
    resData["suggestLE"], resData["suggestLEStart"] = DashboardService.get_suggest_le(user_id,
                                                                        post_dict["learnings"],
                                                                        post_dict["start"])

    return JsonResponse(resData)

# ====================message 관련=============================
# 전체목록
@csrf_exempt
@tellpin_login_required
def getMessage(request):
    """
    @summary: get message list
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
#    user_id = ts.get_user_id(request.session["email"])
    resData['message'] = DashboardService.getMessage(user_id)
    resData['rvMessage'] = DashboardService.checkReceiveMessage(user_id)

    return JsonResponse(resData)

# 개별 메세지
@csrf_exempt
@tellpin_login_required
def getUserMessage(request):
    """
    @summary: get user detail message
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
#    user_id = ts.get_user_id(request.session["email"])
    post = json.loads(request.body)
    you_id = post['user_id']
    resData['userMessage'] = DashboardService.getUserMessage(user_id, you_id)

    return JsonResponse(resData)

# 메세지 보낼대상
@csrf_exempt
@tellpin_login_required
def getMessageUserList(request):
    """
    @summary: 메세지 보낼수 있는 유저 리스트
    @author: msjang
    @param none
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
#    user_id = ts.get_user_id(request.session["email"])
    resData['userList'] = DashboardService.getMessageUserList(user_id)

    return JsonResponse(resData)

# 메세지 보내기
@csrf_exempt
@tellpin_login_required
def sendMessage(request):
    """
    @summary: send message
    @author: msjang
    @param none
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
#    user_id = ts.get_user_id(request.session["email"])
    post = json.loads(request.body)
    to_id = post['to_id']
    msg = post['msg']
    resData['isSuccess'], obj = DashboardService.sendMessage(to_id, user_id, msg)
    Community.set_notification_message(user_id, to_id, obj)
    resData['message'] = DashboardService.getUserMessage(user_id, to_id)

    return JsonResponse(resData)

# 메세지 삭제
@csrf_exempt
@tellpin_login_required
def deleteMessage(request):
    """
    @summary: delete message
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
#    user_id = ts.get_user_id(request.session["email"])
    post = json.loads(request.body)
    my_id = user_id
    you_id = post['you_id']
    mid = post['mid']
    resData['isSuccess'] = DashboardService.deleteMessage(my_id, you_id, mid)

    if resData['isSuccess'] == 1:
        resData['message'] = DashboardService.getMessage(user_id)

    return JsonResponse(resData)

# 메세지 체크
@csrf_exempt
@tellpin_login_required
def checkMessage(request):
    """
    @summary: check message
    @author: msjang
    @param request
    @return: json
    """
    resData = {}
    resData["isSuccess"] = 1
    user_id = request.user.id
#    user_id = ts.get_user_id(request.session["email"])
    resData['msgCount'] = DashboardService.checkReceiveMessage(user_id)

    return JsonResponse(resData)

# 새 메세지 체크
@csrf_exempt
@tellpin_login_required
def checkNewMessage(request):
    """
    @summary: check new message
    @author: chsin
    @param: request
    @return json
    """

    resData = {}
    resData['isSuccess'] = 1
    my_id = request.user.id

    post = json.loads(request.body)
    you_id = post['you_id']

    resData['newMsg'] = DashboardService.checkNewMessage(my_id, you_id)

    return JsonResponse(resData)
