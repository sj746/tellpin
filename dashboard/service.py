#-*- coding: utf-8 -*-

###############################################################################
# filename    : sample > view.py
# description : sample code
# author      : whcho@unichal.com  
#
# history
# -----------------------------------------------------------------------------
# 20160603 whcho 최초 작성
# 20160603 kihyun landingpage2 sample 추가
#
#
###############################################################################

from datetime import datetime, timedelta
from operator import itemgetter

from django.contrib.sessions.models import Session
from django.core.paginator import Paginator
from django.db.models.aggregates import Count
from django.db.models.query_utils import Q
from django.forms.models import model_to_dict

# from common.models import LcPost, LcComment
# from common.models import LqQuestion, LqAnswer, LqComment
# from common.models import TpQuestion, TpAnswer, TpComment
# from people import peopleService
# from common.models import Ratings, Lesson, Reservation, Follow, Message, Friend, TutorHide
# from common.models import Language, TellpinUser, DjangoSession, Feedback
# from common.common import get_login_users
from common.service import CommonService
from people.service import PeopleService
from community.models import LB, LBComment, LBPin, LBVote, \
                            TP, TPAnswer, TPComment, TPPin, TPVote, \
                            LQ, LQAnswer, LQComment, LQPin, LQVote
from tutor.models import Tutee, Friend, Tutor
from tutoring.models import Lesson, LessonFeedback
from user_auth.models import TellpinUser
from dashboard.models import Message
from tutoring.models import LessonMessage, LessonReservation
from tutor.service import TutorService

PROFILE_IMAGE_FILE_DIR = "/static/img/upload/profile"
PROFILE_IMAGE_FILE_DEFAULT_PATH = "/static/img/user_auth/icon_person.png"


class DashboardService(object):
    def __init__(self):
        pass

    @staticmethod
    def get_language_board(page, user_id, count):
        """
        @summary: 내가 작성한 language board 항목
        @param page: 페이지 번호
        @param user_id: 글 작성자 id
        @return: 내가 작성한 language board 항목, 다음 페이지 유무
        """
        lb_list = []
        show_more = False
        try:
            if user_id is not None:
                queryset = LB.objects.select_related('user__tutee')\
                    .filter(user_id=user_id,
                            del_by_user=False,
                            del_by_admin=False)\
                    .annotate(comment_count=Count('lbcomment'))\
                    .order_by('-updated_time')
            else:
                queryset = LB.objects.select_related('user__tutee')\
                    .filter(del_by_user=False,
                            del_by_admin=False)\
                    .annotate(comment_count=Count('lbcomment'))\
                    .order_by('-updated_time')
            p = Paginator(queryset, count)
            page_item = p.page(page)
            for lb in page_item.object_list:
                lb_dict = model_to_dict(lb,
                                        exclude=[
                                            'learning_lang', 'explained_lang',
                                            'main_img_fid', 'fid', 'del_by_user',
                                            'del_by_admin'
                                        ])
                lb_dict['updated_time'] = lb.updated_time.strftime('%Y-%m-%d %H:%M:%S')
                if TutorService.is_tutor(lb_dict["user"]):
                    lb_dict['photo'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                  Tutor.objects.get(user=lb_dict["user"]).photo_filename)
                    lb_dict['name'] = lb.user.tutor.name
                else:
                    lb_dict['photo'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                  Tutee.objects.get(user=lb_dict["user"]).photo_filename)
                    lb_dict['name'] = lb.user.tutee.name
                lb_dict['comment_count'] = lb.comment_count
                lb_dict['category'] = 'Board'
                lb_list.append(lb_dict)
            show_more = page_item.has_next()
        except Exception as err:
            print '@@@@@get_language_board : ', str(err)
            lb_list = []
            show_more = False
        finally:
            return lb_list, show_more

    @staticmethod
    def get_translation_proofreading(page, user_id, count):
        """
        @summary: 사용자가 작성한 tp 리스트
        @param page: 페이지 번호
        @param user_id: 작성자 id
        @return: tp list, 다음 페이지 유무
        """
        tp_list = []
        show_more = False
        try:
            if user_id is not None:
                queryset = TP.objects.select_related('user__tutee')\
                    .filter(user_id=user_id,
                            del_by_user=False,
                            del_by_admin=False)\
                    .annotate(comment_count=Count('tpcomment'),
                              answer_count=Count('tpanswer'))\
                    .order_by('-updated_time')
            else:
                queryset = TP.objects.select_related('user__tutee')\
                    .filter(del_by_user=False,
                            del_by_admin=False)\
                    .annotate(comment_count=Count('tpcomment'),
                              answer_count=Count('tpanswer'))\
                    .order_by('-updated_time')
            p = Paginator(queryset, count)
            page_item = p.page(page)
            for tp in page_item.object_list:
                tp_dict = model_to_dict(tp,
                                        exclude=[
                                            'written_lang', 'translated_lang',
                                            'fid', 'del_by_user', 'del_by_admin'
                                        ])
                if TutorService.is_tutor(tp_dict["user"]):
                    tp_dict['photo'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                  Tutor.objects.get(user=tp_dict["user"]).photo_filename)
                    tp_dict['name'] = tp.user.tutor.name
                else:
                    tp_dict['photo'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                  Tutee.objects.get(user=tp_dict["user"]).photo_filename)
                    tp_dict['name'] = tp.user.tutee.name
                tp_dict['updated_time'] = tp.updated_time.strftime('%Y-%m-%d %H:%M:%S')
                tp_dict['answer_count'] = tp.answer_count
                tp_dict['comment_count'] = tp.comment_count
                tp_dict['category'] = 'TranslationProofreading'
                tp_list.append(tp_dict)
            show_more = page_item.has_next()
        except Exception as err:
            print '@@@@@get_translation_proofreading : ', str(err)
            tp_list = []
            show_more = False
        finally:
            return tp_list, show_more

    @staticmethod
    def get_language_question(page, user_id, count):
        """
        @summary: 유저가 작성한 language question 목록
        @param page: 페이지 번호
        @param user_id: 작성 유저 id
        @return: language question 목록, 다음 페이지 유무
        """
        lq_list = []
        show_more = False
        try:
            if user_id is not None:
                queryset = LQ.objects.select_related('user__tutee')\
                    .filter(user_id=user_id,
                            del_by_user=False,
                            del_by_admin=False)\
                    .annotate(comment_count=Count('lqcomment'),
                              answer_count=Count('lqanswer'))\
                    .order_by('-updated_time')
            else:
                queryset = LQ.objects.select_related('user__tutee')\
                    .filter(del_by_user=False,
                            del_by_admin=False)\
                    .annotate(comment_count=Count('lqcomment'),
                              answer_count=Count('lqanswer'))\
                    .order_by('-updated_time')
            p = Paginator(queryset, count)
            page_item = p.page(page)
            for lq in page_item.object_list:
                lq_dict = model_to_dict(lq,
                                        exclude=[
                                            'written_lang', 'question_lang',
                                            'fid', 'del_by_user', 'del_by_admin'
                                        ])
                if TutorService.is_tutor(lq_dict["user"]):
                    lq_dict['photo'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                  Tutor.objects.get(user=lq_dict["user"]).photo_filename)
                    lq_dict['name'] = lq.user.tutor.name
                else:
                    lq_dict['photo'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                  Tutee.objects.get(user=lq_dict["user"]).photo_filename)
                    lq_dict['name'] = lq.user.tutee.name
                lq_dict['updated_time'] = lq.updated_time.strftime('%Y-%m-%d %H:%M:%S')
                lq_dict['category'] = 'Question'
                lq_dict['answer_count'] = lq.answer_count
                lq_dict['comment_count'] = lq.comment_count
                lq_list.append(lq_dict)
            show_more = page_item.has_next()
        except Exception as err:
            print '@@@@@get_language_question : ', str(err)
            lq_list = []
            show_more = False
        finally:
            return lq_list, show_more

    @staticmethod
    def get_all_community(page, user_id, count):
        """
        @summary: community 전체 항목
        @param page: 페이지 번호
        @param user_id: 유저 id
        @return: community list, 다음 페이지 유무
        """
        community_list = []
        show_more = False
        try:
            if user_id is not None:
                user_obj = TellpinUser.objects.select_related('tutee').get(user_id=user_id)
                lb_list = user_obj.lb_set.filter(del_by_user=False,
                                                 del_by_admin=False)\
                    .annotate(comment_count=Count('lbcomment'))
                tp_list = user_obj.tp_set.filter(del_by_user=False,
                                                 del_by_admin=False)\
                    .annotate(comment_count=Count('tpcomment'),
                              answer_count=Count('tpanswer'))
                lq_list = user_obj.lq_set.filter(del_by_user=False,
                                                 del_by_admin=False)\
                    .annotate(comment_count=Count('lqcomment'),
                              answer_count=Count('lqanswer'))
            else:
                lb_list = LB.objects.select_related('user__tutee')\
                    .filter(del_by_user=False,
                            del_by_admin=False)\
                    .annotate(comment_count=Count('lbcomment'))\
                    .order_by('-updated_time') 

                tp_list = TP.objects.select_related('user__tutee')\
                    .filter(del_by_user=False,
                            del_by_admin=False)\
                    .annotate(comment_count=Count('tpcomment'),
                              answer_count=Count('tpanswer'))\
                    .order_by('-updated_time')
                lq_list = LQ.objects.select_related('user__tutee')\
                    .filter(del_by_user=False,
                            del_by_admin=False)\
                    .annotate(comment_count=Count('lqcomment'),
                              answer_count=Count('lqanswer'))\
                    .order_by('-updated_time')

            all_list = list(lb_list) + list(tp_list) + list(lq_list)
            all_list.sort(key=lambda x: x.updated_time, reverse=True)
            p = Paginator(all_list, count)
            page_item = p.page(page)
            for community in page_item.object_list:
                community_dict = model_to_dict(community,
                                               exclude=[
                                                   'fid', 'del_by_user',
                                                   'del_by_admin'
                                               ])
                if type(community) == LB:
                    community_dict['category'] = 'Board'
                else:
                    if type(community) == TP:
                        community_dict['category'] = 'TranslationProofreading'
                    else:
                        community_dict['category'] = 'Question'
                    community_dict['answer_count'] = community.answer_count
                community_dict['comment_count'] = community.comment_count
                if TutorService.is_tutor(community_dict["user"]):
                    community_dict['photo'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                  Tutor.objects.get(user=community_dict["user"]).photo_filename)
                    community_dict['name'] = community.user.tutor.name
                else:
                    community_dict['photo'] = '%s/%s' % (CommonService.PROFILE_IMAGE_FILE_DIR,
                                                  Tutee.objects.get(user=community_dict["user"]).photo_filename)
                    community_dict['name'] = community.user.tutee.name
                community_dict['updated_time'] = community.updated_time.strftime('%Y-%m-%d %H:%M:%S')
                community_list.append(community_dict)
            show_more = page_item.has_next()
        except Exception as err:
            print '@@@@@get_all_contents : ', str(err)
            community_list = []
            show_more = False
        finally:
            return community_list, show_more

# 메세지 관련 서비스들

    @staticmethod
    def getMessage(user_id):
        """
            @summary: 해당 유저와 상대방들과의 메세지 목록
            @author: msjang, chsin
            @param: user_id: 해당 유저 id 번호
            @return: result(list)
            """

        result = []

        try:
            # 사용자가 보내거나 받은 메세지 중 삭제하지 않은 메세지를 모두 읽어옴
            messages_qs = Message.objects.filter(
                (Q(sender_id=user_id, del_by_sender=0) |
                 Q(receiver_id=user_id, del_by_receiver=0))
            ).order_by('-created_time')

            # 상대방과 가장 최근에 한 대화를 읽어옴
            up_to_date_messages = []
            you_id_list = []
            for message in messages_qs.iterator():

                # 상대방 id 를 읽어옴
                my_id = user_id
                you_id = 0
                you_name = ''
                if message.sender_id == user_id:
                    # 해당 유저가 보낸 사람이면
                    you_id = message.receiver_id
                    you_name = message.receiver.name
                else:
                    # 해당 유저가 받은 사람이면
                    you_id = message.sender_id
                    you_name = message.sender.name

                cur_read_count = 0

                if message.receiver_id == my_id:
                    cur_read_count = abs(message.is_read - 1)
                else:
                    cur_read_count = 0

                if you_id in you_id_list:
                    # 최신 메세지가 아니면 read_count 만 계산
                    if message.receiver_id == my_id:
                        up_to_date_messages[
                            you_id_list.index(you_id)]['read_count'] += cur_read_count
                    continue

                up_to_date_messages.append({
                    'mid': message.id,
                    'to_user_id': message.receiver_id,
                    'from_user_id': message.sender_id,
                    'message': message.message,
                    'created_time': message.created_time,
                    'read_yn': message.is_read,
                    'to_del_yn': message.del_by_receiver,
                    'from_del_yn': message.del_by_sender,
                    'read_count': cur_read_count,
                    'you_id': you_id,
                    'name': you_name,
                    'photo': PROFILE_IMAGE_FILE_DEFAULT_PATH
                })

                you_id_list.append(you_id)

            # 사용자 프로필 사진을 읽어옴

            # 튜터로 부터 읽어옴
            tutors = Tutor.objects.filter(user_id__in=you_id_list)
            # 튜티 체크를 위한 임시 값
            tutee_id_list = []
            tutee_id_list.extend(you_id_list)

            for tutor in tutors.iterator():
                idx = you_id_list.index(tutor.user_id)
                if tutor.photo_filename is not None:
                    up_to_date_messages[idx]['photo'] = tutor.photo_filename
                tutee_id_list.remove(tutor.user_id)

            if len(tutee_id_list) > 0:
                # 튜티로 부터 읽어옴
                tutees = Tutee.objects.filter(user_id__in=tutee_id_list)

                for tutee in tutees.iterator():
                    idx = you_id_list.index(tutee.user_id)
                    if tutee.photo_filename is not None:
                        up_to_date_messages[idx]['photo'] = tutee.photo_filename

            result = up_to_date_messages

        except Exception as e:
            print str(e)
        finally:
            return result

    @staticmethod
    def checkReceiveMessage(user_id):
        """
            @summary: 안 읽은 메세지 유무
            @author: msjang, chsin
            @param: id: 유저 id 번호
            @return: count(int)
            """

        result = {}
        try:
            # 일반 메세지
            count_from_message = Message.objects.filter(
                receiver_id=user_id, is_read=0).count()

            # 레슨 관련 메세지
            lesson_messages_qs = LessonMessage.objects.filter(
                receiver_id=user_id, is_read=0).select_related(
                'lesson_reservation_id')

            # 튜터로부터 받은 메세지
            count_from_tutor = lesson_messages_qs.filter(
                lesson_reservation__tutee_id=user_id).count()

            # 튜티로부터 받은 메세지
            count_from_tutee = lesson_messages_qs.filter(
                lesson_reservation__tutor_id=user_id).count()

            result['from_tutee'] = count_from_tutee
            result['from_tutor'] = count_from_tutor
            result['msgCount'] = count_from_message

        except Exception as e:
            print str(e)
            result['err'] = str(e)
        finally:
            return result

    @staticmethod
    def checkNewMessage(my_id, you_id):
        """
        @summary: 해당 유저와 상대방 유저 간에 새로운 메세지가 있는지 체크하는 함수
        @author: chsin
        @param: my_id: 내 유저  id 번호
        @param you_id: 상대방 유저 id 번호
        @return: True, False
        """

        result = False

        try:
            new_msg_qs = Message.objects.filter(
                receiver_id=my_id, sender_id=you_id, is_read=0).aggregate(new_msg_cnt=Count('id'))

            if new_msg_qs['new_msg_cnt'] > 0:
                result = True
            else:
                result = False
        except Exception as e:
            result = False
        finally:
            return result

    @staticmethod
    def getUserMessage(my_id, you_id):
        """
        @summary: 해당유저와 상대방 유저의 상세 메세지 목록
        @author: msjang, chsin
        @param my_id: 내 유저 id 번호
        @param you_id: 상대방 유저 id 번호
        @return: obj(object)
        """
        obj = {}

        try:
            result = []

            # 메세지 모두 읽음 처리
            Message.objects.filter(
                receiver_id=my_id, sender_id=you_id).update(is_read=1)

            # 해당 유저와 상대방 간에 주고 받은 메세지를 모두 읽어옴. 해당 유저가 삭제한 메세지는 제외
            messages = Message.objects.filter(
                Q(sender_id__in=[my_id, you_id],
                  receiver_id__in=[my_id, you_id]),
                (Q(sender_id=my_id, del_by_sender=0) |
                 Q(receiver_id=my_id, del_by_receiver=0))
            ).order_by('created_time')

            # 해당 유저와 상대방의 프로필 이미지를 읽어옴
            tutors = Tutor.objects.filter(user_id__in=[my_id, you_id])

            my_photo_filename = PROFILE_IMAGE_FILE_DEFAULT_PATH
            you_photo_filename = PROFILE_IMAGE_FILE_DEFAULT_PATH

            tutee_list = [my_id, you_id]

            for tutor in tutors.iterator():
                if tutor.user_id == my_id:
                    tutee_list.remove(my_id)
                    if tutor.photo_filename is not None:
                        my_photo_filename = (PROFILE_IMAGE_FILE_DIR + "/" + tutor.photo_filename)   
                elif tutor.user_id == you_id:
                    tutee_list.remove(you_id)
                    if tutor.photo_filename is not None:
                        you_photo_filename = (PROFILE_IMAGE_FILE_DIR + "/" + tutor.photo_filename)

            if len(tutee_list) > 0:
                tutees = Tutee.objects.filter(user_id__in=tutee_list)

                for tutee in tutees.iterator():
                    if tutee.photo_filename is None:
                        continue
                    if tutee.user_id == my_id :
                        my_photo_filename = (PROFILE_IMAGE_FILE_DIR + "/" + tutee.photo_filename)   
                    elif tutee.user_id == you_id:
                        you_photo_filename = (PROFILE_IMAGE_FILE_DIR + "/" + tutee.photo_filename)

            # 결과를 매핑
            for message in messages.iterator():
                data = {}
                data['mid'] = message.id
                data['to_user_id'] = message.receiver_id
                data['from_user_id'] = message.sender_id
                data['message'] = message.message
                data['created_time'] = message.created_time
                data['read_yn'] = message.is_read
                data['to_del_yn'] = message.del_by_receiver
                data['from_del_yn'] = message.del_by_sender
                data['name'] = message.sender.name

                if message.sender_id == my_id:
                    data['photo'] = my_photo_filename
                    data['you_name'] = message.receiver.name
                else:
                    data['photo'] = you_photo_filename
                    data['you_name'] = message.sender.name

                data['my_id'] = my_id

                result.append(data)

            # 메세지를 한 번도 주고 받지 않은 경우에도 디스플레이할 이름을 읽어옴
            obj['name'] = TellpinUser.objects.get(user_id=you_id).name
            obj['messageList'] = result

        except Exception as e:
            print str(e)
        finally:
            return obj

    @staticmethod
    def sendMessage(receiver_id, sender_id, message):
        """
            @summary: 메세지 보내는 함수
            @author: msjang, chsin
            @param receiver_id: 받는 사용자 id 번호
            @param sender_id: 보내는 사용자 id 번호
            @param msg: 메세지 내용
            @return: 1( success ), 0( fail )
            """

        result = 1

        try:
            obj = Message(
                receiver_id=receiver_id,
                sender_id=sender_id,
                message=message
            )
            obj.save()

        except Exception as e:
            print str(e)
            result = 0
        finally:
            return result, obj

    @staticmethod
    def deleteMessage(my_id, you_id, mid):
        """
            @summary: 특정 유저와의 메세지 목록 삭제( 직접 삭제하지 않고 삭제표시 )
            @author: msjang
            @param my_id: 해당 유저 id 번호
            @param you_id: 특정 유저 id 번호
            @param mid: Message table pk
            @return: 1( success ), 0( fail )
            """

        result = 1
        try:
            # 보낸 메세지 삭제
            result_sender = Message.objects.filter(
                sender_id=my_id, receiver_id=you_id, id__lte=mid).update(del_by_sender=1)

            # 받은 메세지 삭제
            result_receiver = Message.objects.filter(
                receiver_id=my_id, sender_id=you_id, id__lte=mid).update(del_by_receiver=1)

            if (result_sender >= 0 and result_receiver >= 0):
                result = 1
            else:
                result = 0

        except Exception as e:
            print str(e)
            result = 0
        finally:
            return result

    @staticmethod
    def getMessageUserList(user_id):
        """
            @summary: 해당유저가 메세지 보낼수 있는 유저 목록
            @author: msjang
            @param user_id: 해당 유저 id 번호
            @return: result( list )
            """
        result = []

        try:
            # 친구 목록
            friend_qs = Friend.objects.filter(
                (Q(req_user_id=user_id) | Q(res_user_id=user_id)), status=2)

            # 튜터 또는 튜티 목록
            reservation_qs = LessonReservation.objects.filter(
                Q(tutor_id=user_id) | Q(tutee_id=user_id))

            # 메세지 목록
            message_qs = Message.objects.filter(sender_id=user_id)

            # 읽어올 유저 id 목록
            id_list = []

            # 친구 id 목록 읽어오기
            friends = friend_qs.values(
                'req_user_id', 'res_user_id').annotate(Count('req_user_id'))
            for friend in friends:
                if friend['req_user_id'] == user_id:
                    friend_user_id = friend['res_user_id']
                else:
                    friend_user_id = friend['req_user_id']

                if friend_user_id in id_list:
                    continue

                id_list.append(friend_user_id)

            # 튜터 또는 튜티 id 목록 읽어오기
            reservations = reservation_qs.values(
                'tutor_id', 'tutee_id').annotate(Count('tutor_id'))
            for reservation in reservations:
                if reservation['tutor_id'] == user_id:
                    reservation_user_id = reservation['tutee_id']
                else:
                    reservation_user_id = reservation['tutor_id']

                if reservation_user_id in id_list:
                    continue

                id_list.append(reservation_user_id)

            # 메세지 수신자 id 목록 읽어오기
            messages = message_qs.values(
                'receiver_id').annotate(Count('receiver_id'))
            for message in messages:
                message_user_id = message['receiver_id']

                if message_user_id in id_list:
                    continue

                id_list.append(message_user_id)

            # 튜터 읽어오기
            tutor_qs = Tutor.objects.filter(user_id__in=id_list)
            for tutor in tutor_qs.iterator():
                id_list.remove(tutor.user_id)

                photo = PROFILE_IMAGE_FILE_DEFAULT_PATH
                if tutor.photo_filename is not None:
                    photo = PROFILE_IMAGE_FILE_DIR + tutor.photo_filename

                result.append({
                    'id': tutor.user_id,
                    'name': tutor.name,
                    'name_en': tutor.livingin_country,
                    'photo': photo,
                    'type': 1
                })

            if len(id_list) > 0:

                # 튜티 읽어오기
                tutee_qs = Tutee.objects.filter(user_id__in=id_list)

                # 튜티에서 데이터 읽어오기
                for tutee in tutee_qs.iterator():
                    photo = PROFILE_IMAGE_FILE_DEFAULT_PATH
                    if tutee.photo_filename is not None:
                        photo = PROFILE_IMAGE_FILE_DIR + tutee.photo_filename

                    result.append({
                        'id': tutee.user_id,
                        'name': tutee.name,
                        'name_en': tutee.livingin_country,
                        'photo': photo,
                        'type': 1
                    })

        except Exception as e:
            print str(e)
        finally:
            return result

    @staticmethod
    def get_suggest_tutor(_user_id, _learnings, _start):
        """
        @summary: 로그인한 사용자의 정보 중 learning 언어에 해당하는 언어를 가르치는 튜터를 추천해주는 함수
        @author: sj746
        @param param1: _user_id : 사용자 id
        @param param2: _learnings : 사용자의 learning 언어
        @param param3: _start : 보여줄 리스트 인덱스
        @return: suggest_tutor_list : 추천 강사 리스트, next_start : 다음 보여줄 강사 리스트 시작 인덱스
        """

        suggest_tutors = []
        tutors = Tutor.objects.filter(Q(teaching1_id__in=_learnings) | Q(teaching2_id__in=_learnings) | Q(teaching3_id__in=_learnings)).exclude(user=_user_id)

#         print "get_suggest_tutor"
#         print tutors
#         if tutors 
#             print "nonenonenone"
#             tutors = Tutor.objects.all().exclude(user=_user_id)
        my_tutors = PeopleService.getTutorList(_user_id)
        for tutor in tutors:
            # 이미 My Tutors 목록에 있는 지 확인
            exist = False
            for my_tutor in my_tutors:
                if my_tutor["user"] == tutor.user_id:
                    exist = True
                    break

            if not exist:
                # learning = teaching
                feedback = LessonFeedback.objects.filter(tutor=tutor.user_id)
                rating = 0
                for rt in feedback:
                    if rt.ratings is not None:
                        rating += rt.ratings

                tutor.teaching_level = 0
                if tutor.teaching1_id is not None:
                    if tutor.teaching1_id in _learnings:
                        if tutor.native1_id == tutor.teaching1_id or tutor.native2_id == tutor.teaching1_id or tutor.native3_id == tutor.teaching1_id:
                            tutor.teaching_level = 7# 7 : native, 6 : upper itermediate
                if tutor.teaching2_id is not None:
                    if tutor.teaching2_id in _learnings:
                        if tutor.native1_id == tutor.teaching2_id or tutor.native2_id == tutor.teaching2_id or tutor.native3_id == tutor.teaching2_id:
                            tutor.teaching_level = 7
                if tutor.teaching3_id is not None:
                    if tutor.teaching3_id in _learnings:
                        if tutor.native1_id == tutor.teaching3_id or tutor.native2_id == tutor.teaching3_id or tutor.native3_id == tutor.teaching3_id:
                            tutor.teaching_level = 7

                tutor_dict = model_to_dict(tutor, exclude=[
                            'created_time', 'updated_time', 'del_by_user',
                            'del_by_admin', 'how_to_meet', 'audio_only',
                            'skype_id', 'facetime_id', 'hangout_id', 'qq_id',
                            'video_intro', 'short_intro', 'long_intro', 'work1',
                            'work2', 'work3', 'education1', 'education2',
                            'education3', 'certificate1', 'certificate2',
                            'certificate3', 'paymentinfo', 'has_trial', 'trial_price'
                            'available_balance', 'birthdate', 'currency', 'currency_id',
                            'gender', 'has_course', 'has_schedule', 'is_available', 'pending_balance',
                            'timezone', 'timezone_id', 'total_balance', 'withdrawal_pending'
                                                            ])
                tutor_dict["teaching_level"] = tutor.teaching_level
                try:
                    tutor_dict["total_rating"] = rating/len(feedback)
                except ZeroDivisionError:
                    tutor_dict["total_rating"] = 0
                suggest_tutors.append(tutor_dict)

        suggest_tutors = sorted(suggest_tutors, key=itemgetter("teaching_level"))

        login_users = CommonService.get_login_users()
        for suggest_tutor in suggest_tutors[_start:_start+2]:
            if TellpinUser.objects.get(user=suggest_tutor["user"]).email in login_users:
                suggest_tutor["is_login"] = 1
            else:
                suggest_tutor["is_login"] = 0

        suggest_tutor_list = []
        if len(suggest_tutors[_start:_start+2]) == 1:
            suggest_tutor_list.append(suggest_tutors[-1])
            suggest_tutor_list.append(suggest_tutors[0])
            next_start = 1
        elif len(suggest_tutors[_start:_start+2]) == 0:
            suggest_tutor_list = suggest_tutors[0:2]
            next_start = 2
        else:
            suggest_tutor_list = suggest_tutors[_start:_start+2]
            next_start = _start + 2

        return suggest_tutor_list, next_start

    @staticmethod
    def get_suggest_le(_user_id, _learnings, _start):
        """
        @summary: 로그인한 사용자의 정보 중 learning 언어에 해당하는 언어가 native 인 사용자를 추천해주는 함수
        @author: sj746
        @param param1: _user_id : 사용자 id
        @param param2: _learnings : 사용자의 learning 언어
        @param param3: _start : 보여줄 리스트 인덱스
        @return: suggest_LE_list : 추천  language exchange 리스트, next_start : 다음 보여줄  리스트 시작 인덱스
        """

        suggest_LEs = []
        LEs = Tutee.objects.filter(Q(native1_id__in=_learnings) or
                                   Q(native2_id__in=_learnings) or
                                   Q(native3_id__in=_learnings)).exclude(
                                    user=_user_id).order_by("-updated_time")

        for LE in LEs:
            LE_dict = model_to_dict(LE, exclude=[
                            'available_balance', 'birthdate', 'currency', 'currency_id',
                            'gender', 'has_course', 'has_schedule', 'is_available', 'pending_balance',
                            'timezone', 'timezone_id', 'total_balance', 'withdrawal_pending',
                            'created_time', 'updated_time', 'del_by_user',
                            'del_by_admin', 'skype_id', 'facetime_id', 'hangout_id', 'qq_id'
                        ])
            suggest_LEs.append(LE_dict)

        login_users = CommonService.get_login_users()
        for suggest_LE in suggest_LEs[_start:_start+2]:
            if TellpinUser.objects.get(user=suggest_LE["user"]).email in login_users:
                suggest_LE["is_login"] = 1
            else:
                suggest_LE["is_login"] = 0

        suggest_LE_list = []
        if len(suggest_LEs[_start:_start+2]) == 1:
            suggest_LE_list.append(suggest_LEs[-1])
            suggest_LE_list.append(suggest_LEs[0])
            next_start = 1
        elif len(suggest_LEs[_start:_start+2]) == 0:
            suggest_LE_list = suggest_LEs[0:2]
            next_start = 2
        else:
            suggest_LE_list = suggest_LEs[_start:_start+2]
            next_start = _start + 2

        return suggest_LE_list, next_start

    @staticmethod
    def get_reservation_info(_user_id, _is_tutor):
        """
        @summary: 튜터인 사용자의 예약된 강의 정보를 구성하는 함수
        @author: sj746
        @param param1: _user_id : 사용자 id
        @param param2: _is_tutor : 튜터인지 여부
        @return: reservation_info : 예약된 강의의 항목
        """

        reservation_info = {}
        reservation_info["tutee"] = {}
        reservation_info["tutee"]["action_required"] = LessonReservation.objects.filter(Q(tutee_id=_user_id) & Q(lesson_status__in=[2, 5, 6, 9999])).count()
        reservation_info["tutee"]["waiting_for_action"] = LessonReservation.objects.filter(tutee_id=_user_id, lesson_status__in=[1, 3]).count()
        reservation_info["tutee"]["not_scheduled"] = LessonReservation.objects.filter(tutee_id=_user_id, lesson_status=9999).count()
        reservation_info["tutee"]["in_problem"] = LessonReservation.objects.filter(tutee_id=_user_id, lesson_status=5).count()

        # reservation status 가 정상 적용되면 수행
        tutee_upcomming_lessons = LessonReservation.objects.select_related('lesson', 'course', 'tutee', 'tutor').filter(tutee_id=_user_id, lesson_status=2)

        now = datetime.now()

        upcoming_list = []
        for reservation in tutee_upcomming_lessons:
            rv_date = datetime.strptime(reservation.rv_time[0:8] + reservation.rv_time[9:], "%Y%m%d%H%M")
            if now < rv_date:
                lesson_dict = {}
                lesson_dict["tutor_id"] = reservation.tutor_id
                lesson_dict["tutor_name"] = reservation.tutor.name
                lesson_dict["tutor_photo"] = reservation.tutor.photo_filename
                lesson_dict["tutor_from"] = reservation.tutor.from_city
                lesson_dict["lesson_id"] = reservation.id
                lesson_dict["lesson_title"] = reservation.course.course_name
                rv_time = reservation.rv_time
                if reservation.lesson.pkg_times == 1:
                        rv_duration = reservation.lesson.sg_min
                else:
                    rv_duration = reservation.lesson.pkg_min
                _date = datetime.strptime(str(rv_time)[:8]+str(rv_time)[9:], "%Y%m%d%H%M")
                f_time = datetime.strptime(str(_date)[11:13]+":"+str(_date)[14:16], "%H:%M")
                f_time2 = datetime.strptime(str(_date+timedelta(minutes=rv_duration))[11:13]+str( _date+timedelta(minutes=rv_duration) )[14:16], "%H%M" )
                lesson_dict["rv_date_time"] = str(_date)[:4] +'/'+ str(_date)[5:7] +'/'+ str(_date)[8:10] + ', ' + f_time.strftime("%I:%M%p") + "-" + f_time2.strftime("%I:%M%p")
                lesson_dict["rv_time"] = reservation.rv_time
                lesson_dict["rv_date"] = rv_date
                upcoming_list.append(lesson_dict)

        upcoming_list = sorted(upcoming_list, key=itemgetter("rv_date"))

        for lesson in upcoming_list:
            del lesson["rv_date"]

        reservation_info["tutee"]["approved_upcoming"] = len(upcoming_list)
        reservation_info["tutee"]["upcoming_lessons"] = upcoming_list[:2]

        if _is_tutor == 1:
            reservation_info["tutor"] = {}
            action_count = 0
            for rv in LessonReservation.objects.filter(Q(tutor_id=_user_id) & Q(lesson_status__in=[1, 2, 5])):
                if rv.lesson_status == 5:
                    try:
                        fb = LessonFeedback.objects.get(id=rv.lesson_id)
                        continue
                    except Exception as e:
                        print e
                action_count += 1

            reservation_info["tutor"]["action_required"] = action_count
            reservation_info["tutor"]["waiting_for_action"] = LessonReservation.objects.filter(tutor_id = _user_id, lesson_status__in = [3, 6]).count()
            reservation_info["tutor"]["not_scheduled"] = LessonReservation.objects.filter(tutor_id = _user_id, lesson_status = 9999).count()
            reservation_info["tutor"]["in_problem"] = LessonReservation.objects.filter(tutor_id = _user_id, lesson_status = 5).count()

            tutor_upcomming_lessons = LessonReservation.objects.select_related('lesson', 'course', 'tutee', 'tutor').filter(tutor_id=_user_id, lesson_status=2)

            tutor_upcoming_list = []
            for reservation in tutor_upcomming_lessons:
                rv_date = datetime.strptime(reservation.rv_time[0:8] + reservation.rv_time[9:], "%Y%m%d%H%M")
                if now < rv_date:
                    lesson_dict = {}
                    lesson_dict["tutee_id"] = reservation.tutee_id
                    lesson_dict["tutee_name"] = reservation.tutee.name
                    lesson_dict["tutee_photo"] = reservation.tutee.photo_filename
                    lesson_dict["tutee_from"] = reservation.tutee.from_city
                    lesson_dict["lesson_id"] = reservation.id
                    lesson_dict["lesson_title"] = reservation.course.course_name
                    rv_time = reservation.rv_time
                    if reservation.lesson.pkg_times == 1:
                            rv_duration = reservation.lesson.sg_min
                    else:
                        rv_duration = reservation.lesson.pkg_min
                    _date =  datetime.strptime(str(rv_time)[:8]+str(rv_time)[9:], "%Y%m%d%H%M")
                    f_time = datetime.strptime(str(_date)[11:13]+":"+str(_date)[14:16], "%H:%M")
                    f_time2 = datetime.strptime( str( _date+timedelta(minutes=rv_duration) )[11:13]+str( _date+timedelta(minutes=rv_duration) )[14:16], "%H%M" )
                    lesson_dict["rv_date_time"] = str(_date)[:4] +'/'+ str(_date)[5:7] +'/'+ str(_date)[8:10] + ', ' + f_time.strftime("%I:%M%p") + "-" + f_time2.strftime("%I:%M%p")
                    lesson_dict["rv_time"] = reservation.rv_time
                    lesson_dict["rv_date"] = rv_date
                    tutor_upcoming_list.append(lesson_dict)

            tutor_upcoming_list = sorted(tutor_upcoming_list, key = itemgetter("rv_date"))

            for lesson in tutor_upcoming_list:
                del lesson["rv_date"]

            reservation_info["tutor"]["upcoming_lessons"] = tutor_upcoming_list[:2]

        return reservation_info

#     @staticmethod
#     def get_all(_LCs, _PTs, _LQs, _start):
#         """
#         @summary: community의 모든 항목(club, trans&proof, language question)을 구성하는 함수
#         @author: sj746
#         @param param1: _LCs : language learning club 항목
#         @param param1: _PTs : trans&proof
#         @param param1: _LQs : language question
#         @param param1: _start : 처음 보여줄 항목의 개수
#         @return: all_list : 커뮤니티 모든 항목, show_more : 한 화면에 보여준 후 다음 보여줄 개수의 여부
#         """
#
#         all = []
#         show_more = True
#         all.extend(_LCs)
#         all.extend(_PTs)
#         all.extend(_LQs)
#         for post in all :
#             post["created_time"] = datetime.strptime(post["created_time"],"%Y-%m-%d %H:%M:%S")
#
#         all = sorted(all, key=itemgetter("created_time"), reverse=True)
#
#         for post in all :
#             post["created_time"] = str(post["created_time"])
#
#         all_list = []
#         item_count = 4
#         start = (item_count*_start) + 4
#         if _start == 0:
#             all_list = all[0:8]
#             if len(all) <= 8:
#                 show_more = False
#         else:
#             all_list = all[start: start+4]
#             if len(all) <= start+4:
#                 show_more = False
#         login_users = [] #get_login_users()
#
#         for post in all_list:
#             if post["email"] in login_users:
#                 post["is_login"] = 1
#             else:
#                 post["is_login"] = 0
#
#         return all_list, show_more
#
#     @staticmethod
#     def get_language_Question(_start, _user_id):
#         """
#         @summary: 내가 작성한 language question 항목을 가져오는 함수
#         @author: sj746
#         @param param1: _start : 처음 보여줄 항목의 개수
#         @param param1: _user_id : 게시글 작성 사용자 id
#         @return: LQ_list : 내가 작성한  language question 항목, show_more : 한 화면에 보여준 후 다음 보여줄 개수의 여부
#         """
#
#         LQ_list = []
#         try:
#             show_more = True
#             item_count = 4
#             start = (item_count * _start) + 4
#             if (_user_id != -1):
#                 LQ_model = LQ.objects.filter(user_id=_user_id).order_by( '-created_time' )
#             else:
#                 LQ_model = LQ.objects.all().order_by("-created_time")
#             if _start == 0:
#                 LQs = LQ_model[0:8]
#                 if LQ_model.count() <= 8:
#                     show_more = False
#             else:
#                 LQs = LQ_model[start: start+4]
#                 if LQ_model.count() <= start+4:
#                     show_more = False
#             login_users = [] #get_login_users()
#             print LQs
#             for LQ in LQs:
#                 writer = TellpinUser.objects.get(id=LQ.user_id)
#                 LQ_model = model_to_dict(LQ, exclude=['updated_time', 'created_time'])
#                 LQ_model['email'] = writer.email
#                 if LQ_model['email'] in login_users:
#                     LQ_model["is_login"] = 1
#                 else:
#                     LQ_model["is_login"] = 0
#                 #LQ_model['created_time'] = str(LQ.created_time)
#                 LQ_model['created_time'] = (LQ.created_time).strftime("%Y-%m-%d %H:%M:%S")
#                 LQ_model['name'] = writer.name
#                 LQ_model['photo'] = writer.profile_photo
#                 LQ_model['category'] = "Question"
#                 LQ_model['answer_count'] = LQAnswer.objects.filter(id=LQ.qid).count()
#                 LQ_model['comment_count'] = LQAnswer.objects.filter(type='Q', ref_id=LQ.qid).count()
#                 LQ_list.append(LQ_model)
#         except Exception as e:
#             print str(e)
#         finally:
#             return LQ_list, show_more
#
#     @staticmethod
#     def get_translation_and_proofreading(_start, _user_id):
#         """
#         @summary: 내가 작성한 trans&proof 항목을 가져오는 함수
#         @author: sj746
#         @param param1: _start : 처음 보여줄 항목의 개수
#         @param param1: _user_id : 게시글 작성 사용자 id
#         @return: TP_list : 내가 작성한  trans&proof 항목, show_more : 한 화면에 보여준 후 다음 보여줄 개수의 여부
#         """
#
#         TP_list = []
#         show_more = True
#         item_count = 4
#         start = (item_count * _start) + 4
#         if (_user_id != -1):
#             TP_model = TP.objects.filter(user_id=_user_id).order_by( '-created_time' )
#         else:
#             TP_model = TP.objects.all().order_by("-created_time")
#
#         if _start == 0:
#             TPs = TP_model[0:8]
#             if TP_model.count() <= 8:
#                 show_more = False
#         else:
#             TPs = TP_model[start: start+4]
#             if TP_model.count() <= start+4:
#                 show_more = False
#         login_users = [] #get_login_users()
#         for TP in TPs:
#             writer = TellpinUser.objects.get(id = TP.user_id)
#             TP_model = model_to_dict(TP, exclude=['updated_time', 'created_time'])
#             TP_model['email'] = writer.email
#             if TP_model['email'] in login_users:
#                 TP_model["is_login"] = 1
#             else:
#                 TP_model["is_login"] = 0
#
#             TP_model['created_time'] = (TP.created_time).strftime("%Y-%m-%d %H:%M:%S")
#             TP_model['name'] = writer.name
#             TP_model['photo'] = writer.profile_photo
#             if TP_model['type'] == 'T':
#                 TP_model['category'] = "Translation"
#             elif TP_model['type'] == 'P':
#                 TP_model['category'] = "Proofreading"
#             TP_model['answer_count'] = TPAnswer.objects.filter(qid=TP.qid).count()
#             TP_model['comment_count'] = TPComment.objects.filter(type='Q', ref_id=TP.qid).count()
#             TP_list.append(TP_model)
#
#         return TP_list, show_more
#
#     @staticmethod
#     def get_language_club(_start, _user_id):
#         """
#         @summary: 내가 작성한 language learning club 항목을 가져오는 함수
#         @author: sj746
#         @param param1: _start : 처음 보여줄 항목의 개수
#         @param param1: _user_id : 게시글 작성 사용자 id
#         @return: LC_list : 내가 작성한  language learning club 항목, show_more : 한 화면에 보여준 후 다음 보여줄 개수의 여부
#         """
#
#         LC_list = []
#         try:
#             print 'userid', _user_id
#             show_more = True
#             item_count = 4
#             start = (item_count * _start) + 4
#             end = start + 4
#             if (_user_id != -1):
#                 LC_model = LB.objects.filter(user_id=_user_id).order_by( '-created_time' )
#             else:
#                 LC_model = LB.objects.all().order_by("-created_time")
#             if _start == 0:
#                 LCs = LC_model[0:8]
#                 if LC_model.count() <= 8:
#                     show_more = False
#             else:
#                 print LC_model
#                 LCs = LC_model[start:end]
#                 if LC_model.count() <= start+4:
#                     show_more = False
#             print LCs
#             login_users = [] #get_login_users()
#             for LC in LCs:
#                 writer = TellpinUser.objects.get(id=LC.user_id)
#                 LC_model = model_to_dict(LC, exclude=['updated_time', 'created_time'])
#                 LC_model['email'] = writer.email
#                 if LC_model['email'] in login_users :
#                     LC_model["is_login"] = 1
#                 else:
#                     LC_model["is_login"] = 0
#
#                 LC_model['created_time'] = (LC.created_time).strftime("%Y-%m-%d %H:%M:%S")
#                 LC_model['name'] = writer.name
#                 LC_model['photo'] = writer.profile_photo
#                 LC_model['category'] = "Club"
#                 LC_model['comment_count'] = LBComment.objects.filter(ref_id=LC.pid).count()
#                 LC_list.append(LC_model)
#         except Exception as e:
#             print str(e)
#         finally:
#             return LC_list, show_more
